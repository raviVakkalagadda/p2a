List<cscfga__Product_Definition__c> prodDefs = [select Id, csordtelcoa__Screen_Flow_For_Service_Changes__c, cscfga__Opportunity_Mapping__c from cscfga__Product_Definition__c where csordtelcoa__Screen_Flow_For_Service_Changes__r.cscfga__Predicate__c = null and csordtelcoa__Screen_Flow_For_Service_Changes__c != null];
//List<cscfga__Product_Definition__c> prodDefs = [select Id, csordtelcoa__Screen_Flow_For_Service_Changes__c, cscfga__Opportunity_Mapping__c from cscfga__Product_Definition__c where Id = 'a26O0000000ej2T'];
List<Id> objectMappingIds = new List<Id>();
List<Id> screenFlowIds = new List<Id>();
Map<Id, ProdDefWrapper> prodDefWrappers = new Map<Id, ProdDefWrapper>();

for(cscfga__Product_Definition__c prodDef : prodDefs){
    objectMappingIds.add(prodDef.cscfga__Opportunity_Mapping__c);
    screenFlowIds.add(prodDef.csordtelcoa__Screen_Flow_For_Service_Changes__c);

    ProdDefWrapper wrap = new ProdDefWrapper();
    wrap.screenFlowId = prodDef.csordtelcoa__Screen_Flow_For_Service_Changes__c;
    prodDefWrappers.put(prodDef.Id, wrap);
}

List<cscfga__Object_Mapping__c> objectMappings = [select Id, (select Id, cscfga__From_Field__c from cscfga__Field_Mappings__r) from cscfga__Object_Mapping__c where Id in :objectMappingIds];

Boolean found = false;
List<cscfga__Field_Mapping__c> fieldMappingsToInsert = new List<cscfga__Field_Mapping__c>();
for(cscfga__Object_Mapping__c objMap : objectMappings){
    found = false;
    for(cscfga__Field_Mapping__c fm : objMap.cscfga__Field_Mappings__r){
        if(fm.cscfga__From_Field__c == 'StageName'){
            found = true;
        }
    }
    if(!found){
        fieldMappingsToInsert.add(new cscfga__Field_Mapping__c(cscfga__Object_Mapping__c = objMap.Id,
                                                                   Name = 'StageName',
                                                                   cscfga__From_Field__c = 'StageName',
                                                                   cscfga__To_Field__c = 'StageName'));
    }
}

insert fieldMappingsToInsert;

List<cscfga__Attribute_Definition__c> attributesToInsert = new List<cscfga__Attribute_Definition__c>();


for(cscfga__Product_Definition__c prodDef : prodDefs){
  cscfga__Attribute_Definition__c attDef = new cscfga__Attribute_Definition__c(Name = 'StageName',
                                                               cscfga__Label__c = 'StageName',
                                                               cscfga__Type__c  = 'Display Value',
                                                               cscfga__Data_Type__c  = 'String',
                                                               cscfga__Default_Value__c = 'Closed Sales',
                                                               cscfga__Enable_null_option__c  = true,
                                                               cscfga__Product_Definition__c = prodDef.Id);
  attributesToInsert.add(attDef);
  
  prodDefWrappers.get(prodDef.Id).prodDef = prodDef;
  prodDefWrappers.get(prodDef.Id).attDef = attDef;  
}

insert attributesToInsert;

List<cscfga__Predicate__c> predicatesToInsert = new List<cscfga__Predicate__c>();
for(cscfga__Product_Definition__c prodDef : prodDefs){
  
 cscfga__Predicate__c pred = new cscfga__Predicate__c(cscfga__Subject__c = prodDefWrappers.get(prodDef.Id).attDef.Id,
                                                  cscfga__Product_Definition__c = prodDef.Id,
                                                  cscfga__Evaluate__c = 'Attribute Value',
                                                  cscfga__Operator__c = 'equals',
                                                  cscfga__Value__c = '{$Opportunity:StageName}',
                                                  cscfga__Value_is_expression__c = true);
  predicatesToInsert.add(pred);
  prodDefWrappers.get(prodDef.Id).predicate = pred;
}

insert predicatesToInsert;

String defIdString = '';
Map<Id, cscfga__Screen_Flow__c> screenFlowsToUpdate = new Map<Id, cscfga__Screen_Flow__c>();
for(ProdDefWrapper wrap : prodDefWrappers.values()){
  screenFlowsToUpdate.put(wrap.screenFlowId, new cscfga__Screen_Flow__c(Id = wrap.screenFlowId, cscfga__Predicate__c = wrap.predicate.Id));
  defIdString += wrap.prodDef.Id + ' ';
}

update screenFlowsToUpdate.values();

System.debug('Definition Ids: ' + defIdString);

class ProdDefWrapper{
    cscfga__Product_Definition__c prodDef;
    cscfga__Attribute_Definition__c attDef;
    cscfga__Predicate__c predicate;
    Id screenFlowId;
}
