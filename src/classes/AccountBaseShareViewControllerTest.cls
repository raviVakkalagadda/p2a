@isTest(SeeAllData = false)
public class AccountBaseShareViewControllerTest{

    static testmethod void TestMethodAccountBaseShareViewController1() {
        
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<User> usrList = P2A_TestFactoryCls.get_Users(1);
        profile pr = [select id from profile where name='System administrator'];
        User u = new User(Alias = 'stand', Email='standardugf12gser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing000', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standargfgfduser12345096@testorg.com',EmployeeNumber='1230056');
                
                insert u;
        System.debug('acclist ........'+acclist );
        acclist[0].ownerid = usrList[0].id;
        update acclist;
        List<AccountShare> accountShareList = new List<AccountShare>();
        Group grp = new Group();
        grp.Name = 'Test Account Sharing';
        insert grp;
        System.assert(grp!=null);
        
        for(Account a:acclist){
        AccountShare ashare = new AccountShare();
        ashare.AccountId = a.id;
        ashare.UserOrGroupId = u.id;
        ashare.AccountAccessLevel = 'edit';
        ashare.OpportunityAccessLevel = 'Read';
        ashare.CaseAccessLevel = 'None';
        ashare.Rowcause = 'Manual';
        accountShareList.add(ashare);
        
       }
       insert accountShareList;
        
        Test.startTest();
        PageReference pageRef = new PageReference('https://telstra--p2aval.cs57.my.salesforce.com/a3R0k0000004C9x');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',acclist[0].Id);
        ApexPages.currentPage().getParameters().put('recordIdToDelete',acclist[0].Id);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(acclist[0]);
        AccountBaseShareViewController obj = new AccountBaseShareViewController();
        obj.getManualSharedAccounts();
        obj.addSharing();
        obj.editRequest();
        obj.deleteRequest();
        Test.stopTest();
    }
}