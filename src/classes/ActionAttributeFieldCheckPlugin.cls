global class ActionAttributeFieldCheckPlugin extends cscfglint.AbstractSObjectCheckPlugin {
    public override Map<String, Object> getPluginInformation() {
        return new Map<String, Object> {
            cscfglint.Constants.INFO_NAME => 'Action Attribute Field Check Plugin',
            cscfglint.Constants.INFO_DESCRIPTION => 'Checks whether Actions actually reference Attribute Fields of their target Attributes'
        };
    }

    public override String getSObjectName() {
        return 'cscfga__Action__c';
    }

    public override String[] getSObjectFields() {
        return new String[] {
            'cscfga__target__c',
            'cscfga__target__r.name',
            'cscfga__attribute_field__c',
            'cscfga__attribute_field__r.name',
            'cscfga__attribute_field__r.cscfga__attribute_definition__c'
        };
    }

    public override String getCustomFilters() {
        return 'cscfga__target__c != null and cscfga__attribute_field__c != null';
    }

    public override void runCheckOnObject(SObject o, List<Map<String, Object>> failedRecordsList) {
        Id actionAttDefId = (Id) o.get('cscfga__target__c');

        SObject attField = o.getSObject('cscfga__attribute_field__r');
        Id attFieldAttDefId = (Id) attField.get('cscfga__attribute_definition__c');

        if (actionAttDefId != attFieldAttDefId) {
            failedRecordsList.add(cscfglint.FailedCheckMessage.getAsMap(
                (Id) o.get('Id'),
                cscfglint.FailedCheckMessage.MESSAGE_TYPE_WARNING,
                o.get('Name') + ': Referenced attribute field ' + attField.get('Name') + ' (' + attField.get('Id') + ') does not belong to the target attribute definition'
            ));
        }
    }
}