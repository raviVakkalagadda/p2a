/**
        @author - Accenture
        @date - 12-JUNE-2012
        @description - This class contains the Business Logic for the ActionItem triggers. 
    */  
public class ActionItemBusinessClass extends BaseTriggerHandler{

   public ActionItemBusinessClass() 
    {
        super();
        allowRecursion();
    }
     public static void updateOpportunity(){
        Map<ID,ID> ActionOppMapping = new Map<ID,ID>();
        Map<Id,String> OppMap = new Map<Id,String>();
        Map<String,String> rtMap = new Map<String, String>();       
        Map<ID,Opportunity> objOppMap = new Map<ID,Opportunity>();
        
       
        Opportunity objOpportunity; 
        
        for(Action_Item__c ObjActionItem : (List<Action_Item__c>)trigger.new){    
            ActionOppMapping.put(ObjActionItem.Id,ObjActionItem.Opportunity__c); 
        }
        
        if(CheckRecursive.ActionItemBusinessClass() && ActionOppMapping.size() > 0)
        {    
            for(Opportunity opp : [select Id,Name from Opportunity where Id IN : ActionOppMapping.Values()])
            {
                OppMap.put(opp.Id,opp.Name);            
            }
        }
        
        for(Action_Item__c ObjActionItem : (List<Action_Item__c>)trigger.new){
             if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Pre Provisioning') == ObjActionItem.RecordTypeId){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null && ObjActionItem.Is_senior_sales_approved__c && !ObjActionItem.Is_senior_delivery_approved__c && ObjActionItem.Feedback__c!='Rejected'){ 
                    //update the fields of Opportunity with Action Item values
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Service_Delivery_Approver__c=UserInfo.getUserId(),Approved_Date__c=ObjActionItem.Completed_By_Service_delivery_date__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                }
                if(OppMap.get(ObjActionItem.Opportunity__c) != null && ObjActionItem.Is_senior_delivery_approved__c ){ 
                    //update the fields of Opportunity with Action Item values
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Approved_By__c=ObjActionItem.Completed_By_Service_delivery__c,Service_Delivery_Approved_Date__c=ObjActionItem.Completion_Date__c,Email_Approval_Uploaded__c=true);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                }    
            }
            
            if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Pre Provisioning') == ObjActionItem.RecordTypeId && ObjActionItem.Feedback__c=='Rejected' ){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    //update the fields of Opportunity with Action Item values
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Email_Approval_Uploaded__c=false,Pre_Contract_Provisioning_Required__c='No');
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);    
                    }
                } 
            }  
            if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Credit Check') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Assigned'){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    //update the fields of Opportunity with Action Item values
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Credit_Check_Requested__c=true);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                }
            }
            if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Credit Check') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                System.debug('entering IF1Har'+objOppMap+'hhhhh'+ObjActionItem.Opportunity__c);
                
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                system.debug('@@@@ObjActionItem.Status__c;'+ObjActionItem.Status__c);
                Opportunity ObjOpp1 = new Opportunity(Id = ObjActionItem.Opportunity__c);
                    objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp1);
                    System.debug('ddddd Har'+objOppMap);
                    //update the fields of Opportunity with Action Item values
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) { 
                System.debug('entering IF2 Har');
                                
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,CreditCheckStatus__c=ObjActionItem.Status__c,CreditCheckValidity__c=ObjActionItem.CreditLimitValidity__c,SecurityDeposit__c=ObjActionItem.SecurityDeposit__c,SecurityDepositHoldingPeriod__c=ObjActionItem.SecurityDepositHoldingPeriod__c,CreditCheckDoneBy__c=ObjActionItem.Completed_By__c,CreditCheckCompletedDate__c=ObjActionItem.Completion_Date__c,Credit_Check_Feedback__c =ObjActionItem.Feedback__c,Credit_Check_Completed__c=true);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    else{
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.CreditCheckStatus__c=ObjActionItem.Status__c;
                        opp.CreditCheckValidity__c=ObjActionItem.CreditLimitValidity__c;
                        opp.SecurityDeposit__c=ObjActionItem.SecurityDeposit__c;
                        opp.SecurityDepositHoldingPeriod__c=ObjActionItem.SecurityDepositHoldingPeriod__c;
                        if((ObjActionItem.Status__c == 'Completed') && (opp.CreditCheckDoneBy__c=='' || ObjActionItem.Completed_By__c == opp.CreditCheckDoneBy__c) && (opp.CreditCheckCompletedDate__c==null || ObjActionItem.Completion_Date__c == opp.CreditCheckCompletedDate__c))
                        {
                        System.debug('entering if 55555'+opp.CreditCheckDoneBy__c);
                        opp.CreditCheckDoneBy__c=userinfo.getFirstName()+' ' +userinfo.getLastName();
                        opp.CreditCheckCompletedDate__c=System.Today();
                        }
                        opp.Credit_Check_Feedback__c =ObjActionItem.Feedback__c;
                        opp.Credit_Check_Completed__c=true;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        System.debug('entering IF3 Har'+opp.CreditCheckDoneBy__c);
                        System.debug('entering IF3 Har1'+ObjActionItem.Completed_By__c);
                    }
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Order Support') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed' ){
                system.debug('Entered 110');
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values 
                    system.debug('Entered 113');
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Order_Support_AI_completed__c=true);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    
                    else{
                        system.debug('Entered 119');
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.Order_Support_AI_completed__c=true;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }    
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Feasibility Study') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c!='Completed'){
                system.debug('Entered 110');
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                // update the fields of Opportunity with Action Item values 
                    system.debug('Entered 113');
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){
                    Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Feasibility_Study_Requested__c=true);
                    objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    
                else{
                    system.debug('Entered 119');
                    Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                    // Now Set the Opp Values
                    opp.Feasibility_Study_Requested__c=true;
                    objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }    
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Feasibility Study') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values  
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                 
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,FeasibilityStatus__c=ObjActionItem.Status__c,FeasibilityDoneBy__c=ObjActionItem.Completed_By__c,FeasibilityCompletedDate__c=ObjActionItem.Completion_Date__c, F_Expiration_Date__c=ObjActionItem.F_Expiration_Date__c,Feasibility_Result__c =ObjActionItem.FeasibilityResult__c,Feasibility_Result_Details__c=ObjActionItem.FeasibilityResultDetails__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    else{
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.FeasibilityStatus__c=ObjActionItem.Status__c;
                        opp.FeasibilityDoneBy__c=ObjActionItem.Completed_By__c;
                        opp.FeasibilityCompletedDate__c=ObjActionItem.Completion_Date__c;
                        opp.F_Expiration_Date__c=ObjActionItem.F_Expiration_Date__c;
                        opp.Feasibility_Result__c =ObjActionItem.FeasibilityResult__c;
                        opp.Feasibility_Result_Details__c=ObjActionItem.FeasibilityResultDetails__c;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Pricing Approval') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c!='Completed'){
                system.debug('Entered 110');
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values 
                    system.debug('Entered 113');
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Pricing_Approval_Requested__c=true);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }  
                    else{
                        system.debug('Entered 119');
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.Pricing_Approval_Requested__c=true;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }    
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Pricing Approval') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values 
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                     
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,PricingApprovalStatus__c=ObjActionItem.Status__c,PricingApprovalDoneBy__c=ObjActionItem.Completed_By__c,PricingApprovalCompletedDate__c=ObjActionItem.Completion_Date__c,Pricing_Approval_Feedback__c =ObjActionItem.Pricing_Feedback__c,Pricing_Comments__c=objActionItem.Pricing_Approval_Comments__c,Additional_information_notes__c =objActionItem.Additional_information_notes__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    else{
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.PricingApprovalStatus__c=ObjActionItem.Status__c;
                        opp.PricingApprovalDoneBy__c=ObjActionItem.Completed_By__c;
                        opp.PricingApprovalCompletedDate__c=ObjActionItem.Completion_Date__c;
                        opp.Pricing_Approval_Feedback__c =ObjActionItem.Pricing_Feedback__c;
                        opp.Pricing_Comments__c=objActionItem.Pricing_Approval_Comments__c;
                        opp.Additional_information_notes__c =objActionItem.Additional_information_notes__c;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Contract') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                // update the fields of Opportunity with Action Item values 
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) {                     
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Notes_to_Legal_Filled_in_by_Sales__c =ObjActionItem.Special_Terms_and_Conditions__c,Legal_Comments__c =ObjActionItem.LegalComments__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    
                    else{
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.Notes_to_Legal_Filled_in_by_Sales__c =ObjActionItem.Special_Terms_and_Conditions__c;
                        opp.Legal_Comments__c =ObjActionItem.LegalComments__c;
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Request Service Details') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                if(OppMap.get(ObjActionItem.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values 
                    if (!objOppMap.containskey(ObjActionItem.Opportunity__c)) { 
                        //system.debug('Action_Item_Created_SD__c '+ 'gng here****');                    
                        Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Action_Item_Created_SD__c = false);
                        system.debug('Action_Item_Created_SD__c*****'+ObjOpp.Action_Item_Created_SD__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                    }
                    else{
                        Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                        // Now Set the Opp Values
                        opp.Action_Item_Created_SD__c = false;
                        // system.debug('Action_Item_Created_SD__c2*****'+ opp.Action_Item_Created_SD__c);
                        objOppMap.put(ObjActionItem.Opportunity__c,opp);
                    }
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Stage Gate Review Request') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Completed'){
                if(OppMap.get(ObjActionItem.Opportunity__c)!= null){
                    // update the fields of Opportunity with Action Item values  
                    if(ObjActionItem.Stage_Gate_Number__c == 1){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){  
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_1_Review_Completed__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_1_Review_Completed__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 2){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){      
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_2_Review_Completed__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_2_Review_Completed__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 3){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){  
                        
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_3_Review_Completed__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_3_Review_Completed__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 4){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){
                    
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_4_Review_Completed__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_4_Review_Completed__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }                   
                }
            }
            else if(RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SobjectType, 'Stage Gate Review Request') == ObjActionItem.RecordTypeId && ObjActionItem.Status__c=='Assigned'){
                if(OppMap.get(ObjActionItem.Opportunity__c)!= null){
                    // update the fields of Opportunity with Action Item values  
                    if(ObjActionItem.Stage_Gate_Number__c == 1){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){  
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_1_Action_Created__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_1_Action_Created__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 2){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){      
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_2_Action_Created__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_2_Action_Created__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 3){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){  
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_3_Action_Created__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_3_Action_Created__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }
                    else if(ObjActionItem.Stage_Gate_Number__c == 4){
                        if (!objOppMap.containskey(ObjActionItem.Opportunity__c)){
                            Opportunity ObjOpp = new Opportunity(Id = ObjActionItem.Opportunity__c,Stage_Gate_4_Action_Created__c = true);
                            objOppMap.put(ObjActionItem.Opportunity__c,ObjOpp);
                        }
                        else{
                            Opportunity opp = objOppMap.get(ObjActionItem.Opportunity__c);
                            opp.Stage_Gate_4_Action_Created__c = true;
                            objOppMap.put(ObjActionItem.Opportunity__c,opp);
                        }
                    }                   
                }
            }
        }
        
        List<Opportunity> oppValues = objOppMap.values();

        if(oppValues.size() > 0 ) {
            try{
                system.debug('oppvalues-----'+oppValues);
                update oppValues;
            }
            catch(Exception excp){
                CreateApexErrorLog.InsertHandleException('Trigger','ActionItemAfterInsert',excp.getMessage(),'Action_Item__c',Userinfo.getName());
            }
        }
    }
}