@isTest(seeAllData= false)

public class ActionItemBusinessClassTest extends BaseTriggerHandler{
    
    private static List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    private static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    private static List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1);    
    private static List<Site__c> cityList = P2A_TestFactoryCls.getsites(1, accList,  countrylist);
    private static List<Contact> contactList = P2A_TestFactoryCls.getContact(1, accList);
    private static Account acc = accList[0];
    private static Opportunity opp = oppList[0];
    private static Country_Lookup__c cl = countryList[0];    
    private static Site__c s = cityList[0];
    private static Contact con = contactList[0];
    private static City_Lookup__c ci;
    private static Action_Item__c at1;
    private static list<Action_Item__c> acList;
    private static list<Action_Item__c> acList1;
    private static Map<ID,ID> ActionOppMapping = new Map<ID,ID>();
    private static Map<Id,String> OppMap = new Map<Id,String>();
    private static Map<String,String> rtMap = new Map<String, String>();       
    private static Map<ID,Opportunity> objOppMap = new Map<ID,Opportunity>();
    
    private static Action_Item__c getRecordType(){
        if(at1 == null){ 
            at1 = new Action_Item__c();
            //at1.RecordTypeId = 'SG';
            
            insert at1;
            system.assert(at1!=null);
        }
        return at1;
    }
    
    private static Id getRecordTypeRPP(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Pre Provisioning').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRCC(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo.get('Request Credit Check').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeROS(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Order Support').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRFS(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Feasibility Study').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRPA(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Pricing Approval').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRC(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Contract').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeRSD(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Request Service Details').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static Id getRecordTypeSGRR(){
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Action_Item__c; 
        Map<String,Schema.RecordTypeInfo> AccountRecordTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = AccountRecordTypeInfo .get('Stage Gate Review Request').getRecordTypeId();
        system.assert(cfrSchema !=null);
        return rtId;
    }
    
    private static void initTestData1(){        
        
        acList = new List<Action_Item__c>();
        
        Action_Item__c ai1 = new Action_Item__c();
        Action_Item__c ai2 = new Action_Item__c();
        Action_Item__c ai3 = new Action_Item__c();
        Action_Item__c ai4 = new Action_Item__c();
        Action_Item__c ai5 = new Action_Item__c();        
        Action_Item__c ai6 = new Action_Item__c();
        Action_Item__c ai7 = new Action_Item__c();
        Action_Item__c ai8 = new Action_Item__c();
        Action_Item__c ai9 = new Action_Item__c();
        Action_Item__c ai10 = new Action_Item__c();        
        Action_Item__c ai11 = new Action_Item__c();
        Action_Item__c ai12 = new Action_Item__c();
        Action_Item__c ai13 = new Action_Item__c();
        Action_Item__c ai14 = new Action_Item__c();
        Action_Item__c ai15 = new Action_Item__c();
        Action_Item__c ai16 = new Action_Item__c();
        Action_Item__c ai17 = new Action_Item__c();
        Action_Item__c ai18 = new Action_Item__c();
        Action_Item__c ai19 = new Action_Item__c();
        Action_Item__c ai20 = new Action_Item__c();
        Action_Item__c ai21 = new Action_Item__c();
        Action_Item__c ai22 = new Action_Item__c();
        Action_Item__c ai23 = new Action_Item__c();
        Action_Item__c ai24 = new Action_Item__c();
        Action_Item__c ai25 = new Action_Item__c();
        Action_Item__c ai26 = new Action_Item__c();
        Action_Item__c ai5_1 = new Action_Item__c();
        Action_Item__c ai10_1 = new Action_Item__c();
        Action_Item__c ai19_1 = new Action_Item__c();
        Action_Item__c ai20_1 = new Action_Item__c();
        Action_Item__c ai21_1 = new Action_Item__c();
        Action_Item__c ai22_1 = new Action_Item__c();
        Action_Item__c ai23_1 = new Action_Item__c();
        Action_Item__c ai24_1 = new Action_Item__c();
        Action_Item__c ai25_1 = new Action_Item__c();
        Action_Item__c ai26_1 = new Action_Item__c();
        
        // data inputs
        ai1.RecordTypeId = getRecordTypeRPP();
        //System.debug('sumit Id'+getOpportunity().Id);
        ai1.Opportunity__c = opp.id;
        //ai1.Service_Delivery_Approver__c=UserInfo.getUserId()
        ai1.Completed_By_Service_delivery_date__c = System.today();
        ai1.Is_senior_sales_approved__c =true;
        ai1.Is_senior_delivery_approved__c = false;
        ai1.Feedback__c='Approved';
        acList.add(ai1);
        
        ai2.RecordTypeId = getRecordTypeRPP();
        ai2.Opportunity__c = opp.Id;
        //ai2.Completed_By_Service_delivery__c = ;
        ai2.Completion_Date__c = System.today();
        //ai2.Email_Approval_Uploaded__c=true
        //ai1.Is_senior_sales_approved__c =true;
        ai2.Is_senior_delivery_approved__c = true;
        //ai2.Feedback__c='Approved';
        acList.add(ai2);
        
        ai3.RecordTypeId = getRecordTypeRPP();
        ai3.Opportunity__c = opp.Id;
        //ai3.Email_Approval_Uploaded__c=false
        //ai3.Pre_Contract_Provisioning_Required__c='No'
        ai3.Feedback__c='Rejected';
        acList.add(ai3);
        
        ai4.RecordTypeId = getRecordTypeRCC();
        ai4.Opportunity__c = opp.Id;
        //ai4.Credit_Check_Requested__c = true4
        ai4.Status__c = 'Assigned';
        ai4.Feedback__c='Rejected';
        acList.add(ai4);
        
        ai5_1.Opportunity__c = opp.Id;
        ai5_1.RecordTypeId = getRecordTypeRCC();
        ai5_1.Status__c = 'Assigned';
        ai5_1.Feedback__c='Approved';
        ai5_1.CreditLimitValidity__c=System.today();
        ai5_1.SecurityDeposit__c= 100.00;
        ai5_1.SecurityDepositHoldingPeriod__c=System.today();
        ai5_1.Completed_By__c= '';
        ai5_1.Completion_Date__c=null;
        ai5_1.Completed_By_Service_delivery_date__c=System.today();
        acList.add(ai5_1);
        
        ai6.RecordTypeId = getRecordTypeROS();
        ai6.Opportunity__c = opp.Id;
        ai6.Status__c = 'Assigned';
        ai6.Feedback_Order__c = 'Approved';
        ai6.CreditLimitValidity__c = System.today();
        ai6.SecurityDeposit__c = 100.00;
        ai6.SecurityDepositHoldingPeriod__c =System.today();
        ai6.Completion_Date__c = System.today();
        acList.add(ai6);
        
        Id oppId_ai7 = opp.id;
        ai7.Opportunity__c = oppId_ai7;
        ai7.RecordTypeId = getRecordTypeROS();
        ai7.Status__c = 'Assigned';
        ai7.Feedback_Order__c = 'Approved';
        ai7.CreditLimitValidity__c = System.today();
        ai7.SecurityDeposit__c = 100.00;
        ai7.SecurityDepositHoldingPeriod__c =System.today();
        ai7.Completion_Date__c = System.today();
        acList.add(ai7);
        Opportunity opp_ai7 = new Opportunity(Id = oppId_ai7);
        objOppMap.put(oppId_ai7, opp_ai7);
        
        ai8.RecordTypeId = getRecordTypeRFS();
        ai8.Opportunity__c = opp.Id;
        //ai8.Feasibility_Study_Requested__c=true
        ai8.Status__c = 'New';
        acList.add(ai8);
        
        ai9.Opportunity__c = opp.Id;
        ai9.RecordTypeId = getRecordTypeRFS();
        ai9.Status__c = 'Assigned';
        //ai9.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai9.Completion_Date__c = System.today();
        ai9.F_Expiration_Date__c = System.today();
        ai9.FeasibilityResult__c = 'Feasible';
        ai9.FeasibilityResultDetails__c = 'Feasible';
        //objOppMap.remove(ai9.Opportunity__c);
        acList.add(ai9);
        
        ai10.Opportunity__c = opp.Id;
        ai10.RecordTypeId = getRecordTypeRFS();
        ai10.Status__c = 'Assigned';
        ai10.Completed_By__c = 'kutta';
        ai10.Completion_Date__c = System.today();
        ai10.F_Expiration_Date__c = System.today();
        ai10.FeasibilityResult__c = 'Feasible';
        ai10.FeasibilityResultDetails__c = 'Feasible';
        acList.add(ai10);
        
        ai10_1.Opportunity__c = opp.Id;
        ai10_1.RecordTypeId = getRecordTypeRFS();
        ai10_1.Status__c = 'Assigned';
        //ai10_1.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai10_1.Completion_Date__c = System.today();
        ai10_1.F_Expiration_Date__c = System.today();
        ai10_1.FeasibilityResult__c = 'Feasible';
        ai10_1.FeasibilityResultDetails__c = 'Feasible';
        acList.add(ai10_1);
        
        ai11.Opportunity__c = opp.Id;
        ai11.RecordTypeId = getRecordTypeRPA();
        ai11.Status__c = 'New';
        acList.add(ai11);
        
        ai12.Opportunity__c = opp.Id;
        ai12.RecordTypeId = getRecordTypeRPA();
        ai12.Status__c = 'New';
        //objOppMap.remove(ai12.Opportunity__c);
        acList.add(ai12);   
        
        ai13.Opportunity__c = opp.Id;
        ai13.RecordTypeId = getRecordTypeRPA();
        ai13.Status__c = 'Assigned';
        //ai13.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai13.Completion_Date__c = System.today();
        ai13.Pricing_Feedback__c = 'Approved';
        ai13.Pricing_Approval_Comments__c = 'Approved';
        ai13.Additional_information_notes__c = 'Approved';
        acList.add(ai13);
        
        ai14.Opportunity__c = opp.Id;
        ai14.RecordTypeId = getRecordTypeRPA();
        ai14.Status__c = 'Assigned';
        //ai14.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai14.Completion_Date__c = System.today();
        ai14.Pricing_Feedback__c = 'Approved';
        ai14.Pricing_Approval_Comments__c = 'Approved';
        ai14.Additional_information_notes__c = 'Approved';
        acList.add(ai14);           
        
        ai15.Opportunity__c = opp.Id;
        ai15.RecordTypeId = getRecordTypeRC();
        ai15.Status__c ='Assigned';
        ai15.Special_Terms_and_Conditions__c = 'xxxx';
        ai15.LegalComments__c = 'xxxx';
        acList.add(ai15);
        
        ai16.Opportunity__c = opp.Id;
        ai16.Status__c ='Assigned';
        ai16.RecordTypeId = getRecordTypeRC();
        ai16.Special_Terms_and_Conditions__c = 'xxxx';
        ai16.LegalComments__c = 'xxxx';
        acList.add(ai16);           
        
        ai17.Opportunity__c = opp.Id;
        ai17.RecordTypeId = getRecordTypeRSD();
        ai17.Status__c ='Assigned';
        acList.add(ai17);
        
        ai18.Opportunity__c = opp.Id;
        ai18.RecordTypeId = getRecordTypeRSD();
        ai18.Status__c ='Assigned';
        acList.add(ai18);
        
        ai19.Opportunity__c = opp.Id;
        ai19.RecordTypeId = getRecordTypeSGRR();
        ai19.Status__c ='Assigned';
        ai19.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai19);
        
        ai19_1.Opportunity__c = opp.Id;
        ai19_1.RecordTypeId = getRecordTypeSGRR();
        ai19_1.Status__c ='Assigned';
        ai19_1.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai19_1);
        
        ai20.Opportunity__c = opp.Id;
        ai20.RecordTypeId = getRecordTypeSGRR();
        ai20.Status__c ='Assigned';
        ai20.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai20);
        
        ai20_1.Opportunity__c = opp.Id;
        ai20_1.RecordTypeId = getRecordTypeSGRR();
        ai20_1.Status__c ='Assigned';
        ai20_1.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai20_1);
        
        ai21.Opportunity__c = opp.Id;
        ai21.RecordTypeId = getRecordTypeSGRR();
        ai21.Status__c ='Assigned';
        ai21.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai21);
        
        ai21_1.Opportunity__c = opp.Id;
        ai21_1.RecordTypeId = getRecordTypeSGRR();
        ai21_1.Status__c ='Assigned';
        ai21_1.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList.add(ai21_1);
        
        ai22.Opportunity__c = opp.Id;
        ai22.RecordTypeId = getRecordTypeSGRR();
        ai22.Status__c ='Assigned';
        ai22.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true
        acList.add(ai22);
        
        ai22_1.Opportunity__c = opp.Id;
        ai22_1.RecordTypeId = getRecordTypeSGRR();
        ai22_1.Status__c ='Assigned';
        ai22_1.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true;
        acList.add(ai22_1);
     
        
        ai23.Opportunity__c = opp.Id;
        ai23.RecordTypeId = getRecordTypeSGRR();
        ai23.Status__c ='Assigned';
        ai23.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList.add(ai23);
        
        ai23_1.Opportunity__c = opp.Id;
        ai23_1.RecordTypeId = getRecordTypeSGRR();
        ai23_1.Status__c ='Assigned';
        ai23_1.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList.add(ai23_1);
        
        ai24.Opportunity__c = opp.Id;
        ai24.RecordTypeId = getRecordTypeSGRR();
        ai24.Status__c ='Assigned';
        ai24.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList.add(ai24);
        
        ai24_1.Opportunity__c = opp.Id;
        ai24_1.RecordTypeId = getRecordTypeSGRR();
        ai24_1.Status__c ='Assigned';
        ai24_1.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList.add(ai24_1);
        
        ai25.Opportunity__c = opp.Id;
        ai25.RecordTypeId = getRecordTypeSGRR();
        ai25.Status__c ='Assigned';
        ai25.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList.add(ai25);
        
        ai25_1.Opportunity__c = opp.Id;
        ai25_1.RecordTypeId = getRecordTypeSGRR();
        ai25_1.Status__c ='Assigned';
        ai25_1.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList.add(ai25_1);
        
        ai26.Opportunity__c = opp.Id;
        ai26.RecordTypeId = getRecordTypeSGRR();
        ai26.Status__c ='Assigned';
        ai26.Stage_Gate_Number__c = 4;
        //ai21.Stage_Gate_4_Action_Created__c = true
        acList.add(ai26);
        
        ai26_1.Opportunity__c = opp.Id;
        ai26_1.RecordTypeId = getRecordTypeSGRR();
        ai26_1.Status__c ='Assigned';
        ai26_1.Stage_Gate_Number__c = 4;
       // ai26_1.Assigned_To__c=UserInfo.getUserId();
       // ai26_1.Assigned_To_OD__c=UserInfo.getUserId();
        //ai21.Stage_Gate_4_Action_Created__c = true
        acList.add(ai26_1);
                
            insert acList; 
            system.assert(acList!=null);
       
    }
    private static void initTestData2(){
        acList1 = new List<Action_Item__c>();
        
        Action_Item__c ai1 = new Action_Item__c();
        Action_Item__c ai2 = new Action_Item__c();
        Action_Item__c ai3 = new Action_Item__c();
        Action_Item__c ai4 = new Action_Item__c();
        Action_Item__c ai5 = new Action_Item__c();        
        Action_Item__c ai6 = new Action_Item__c();
        Action_Item__c ai7 = new Action_Item__c();
        Action_Item__c ai8 = new Action_Item__c();
        Action_Item__c ai9 = new Action_Item__c();
        Action_Item__c ai10 = new Action_Item__c();        
        Action_Item__c ai11 = new Action_Item__c();
        Action_Item__c ai12 = new Action_Item__c();
        Action_Item__c ai13 = new Action_Item__c();
        Action_Item__c ai14 = new Action_Item__c();
        Action_Item__c ai15 = new Action_Item__c();
        Action_Item__c ai16 = new Action_Item__c();
        Action_Item__c ai17 = new Action_Item__c();
        Action_Item__c ai18 = new Action_Item__c();
        Action_Item__c ai19 = new Action_Item__c();
        Action_Item__c ai20 = new Action_Item__c();
        Action_Item__c ai21 = new Action_Item__c();
        Action_Item__c ai22 = new Action_Item__c();
        Action_Item__c ai23 = new Action_Item__c();
        Action_Item__c ai24 = new Action_Item__c();
        Action_Item__c ai25 = new Action_Item__c();
        Action_Item__c ai26 = new Action_Item__c();
        Action_Item__c ai5_1 = new Action_Item__c();
        Action_Item__c ai10_1 = new Action_Item__c();
        Action_Item__c ai19_1 = new Action_Item__c();
        Action_Item__c ai20_1 = new Action_Item__c();
        Action_Item__c ai21_1 = new Action_Item__c();
        Action_Item__c ai22_1 = new Action_Item__c();
        Action_Item__c ai23_1 = new Action_Item__c();
        Action_Item__c ai24_1 = new Action_Item__c();
        Action_Item__c ai25_1 = new Action_Item__c();
        Action_Item__c ai26_1 = new Action_Item__c();
        
        // data inputs
        ai1.RecordTypeId = getRecordTypeRPP();
        //System.debug('sumit Id'+getOpportunity().Id);
        ai1.Opportunity__c = getOpportunity().Id;
        //ai1.Service_Delivery_Approver__c=UserInfo.getUserId()
        ai1.Completed_By_Service_delivery_date__c = System.today();
        ai1.Is_senior_sales_approved__c =true;
        ai1.Is_senior_delivery_approved__c = false;
        ai1.Feedback__c='Approved';
        acList1.add(ai1);
        
        ai2.RecordTypeId = getRecordTypeRPP();
        ai2.Opportunity__c = getOpportunity().Id;
        //ai2.Completed_By_Service_delivery__c = ;
        ai2.Completion_Date__c = System.today();
        //ai2.Email_Approval_Uploaded__c=true
        //ai1.Is_senior_sales_approved__c =true;
        ai2.Is_senior_delivery_approved__c = true;
        //ai2.Feedback__c='Approved';
        acList1.add(ai2);
        
        ai3.RecordTypeId = getRecordTypeRPP();
        ai3.Opportunity__c = getOpportunity().Id;
        //ai3.Email_Approval_Uploaded__c=false
        //ai3.Pre_Contract_Provisioning_Required__c='No'
        ai3.Feedback__c='Rejected';
        acList1.add(ai3);
        
        ai4.RecordTypeId = getRecordTypeRCC();
        ai4.Opportunity__c = getOpportunity().Id;
        //ai4.Credit_Check_Requested__c = true4
        ai4.Status__c = 'Assigned';
        ai4.Feedback__c='Rejected';
        acList1.add(ai4);
        
        ai5_1.Opportunity__c = getOpportunity().Id;
        ai5_1.RecordTypeId = getRecordTypeRCC();
        ai5_1.Status__c = 'Completed';
        ai5_1.Feedback__c='Approved';
        ai5_1.CreditLimitValidity__c=System.today();
        ai5_1.SecurityDeposit__c= 100.00;
        ai5_1.SecurityDepositHoldingPeriod__c=System.today();
        ai5_1.Completed_By__c= '';
        ai5_1.Completion_Date__c=null;
        ai5_1.Completed_By_Service_delivery_date__c=System.today();
        acList1.add(ai5_1);
        
        ai6.RecordTypeId = getRecordTypeROS();
        ai6.Opportunity__c = getOpportunity().Id;
        ai6.Status__c = 'Completed';
        ai6.Feedback_Order__c = 'Approved';
        ai6.CreditLimitValidity__c = System.today();
        ai6.SecurityDeposit__c = 100.00;
        ai6.SecurityDepositHoldingPeriod__c =System.today();
        ai6.Completion_Date__c = System.today();
        acList1.add(ai6);
        
        Id oppId_ai7 = getOpportunity().Id;
        ai7.Opportunity__c = oppId_ai7;
        ai7.RecordTypeId = getRecordTypeROS();
        ai7.Status__c = 'Completed';
        ai7.Feedback_Order__c = 'Approved';
        ai7.CreditLimitValidity__c = System.today();
        ai7.SecurityDeposit__c = 100.00;
        ai7.SecurityDepositHoldingPeriod__c =System.today();
        ai7.Completion_Date__c = System.today();
        acList1.add(ai7);
        Opportunity opp_ai7 = new Opportunity(Id = oppId_ai7);
        objOppMap.put(oppId_ai7, opp_ai7);
        
        ai8.RecordTypeId = getRecordTypeRFS();
        ai8.Opportunity__c = getOpportunity().Id;
        //ai8.Feasibility_Study_Requested__c=true
        ai8.Status__c = 'New';
        acList1.add(ai8);
        
        ai9.Opportunity__c = getOpportunity().Id;
        ai9.RecordTypeId = getRecordTypeRFS();
        ai9.Status__c = 'Completed';
        //ai9.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai9.Completion_Date__c = System.today();
        ai9.F_Expiration_Date__c = System.today();
        ai9.FeasibilityResult__c = 'Feasible';
        ai9.FeasibilityResultDetails__c = 'Feasible';
        //objOppMap.remove(ai9.Opportunity__c);
        acList1.add(ai9);
        
        ai10.Opportunity__c = getOpportunity().Id;
        ai10.RecordTypeId = getRecordTypeRFS();
        ai10.Status__c = 'Completed';
        ai10.Completed_By__c = 'kutta';
        ai10.Completion_Date__c = System.today();
        ai10.F_Expiration_Date__c = System.today();
        ai10.FeasibilityResult__c = 'Feasible';
        ai10.FeasibilityResultDetails__c = 'Feasible';
        acList1.add(ai10);
        
        ai10_1.Opportunity__c = getOpportunity().Id;
        ai10_1.RecordTypeId = getRecordTypeRFS();
        ai10_1.Status__c = 'Completed';
        //ai10_1.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai10_1.Completion_Date__c = System.today();
        ai10_1.F_Expiration_Date__c = System.today();
        ai10_1.FeasibilityResult__c = 'Feasible';
        ai10_1.FeasibilityResultDetails__c = 'Feasible';
        acList1.add(ai10_1);
        
        ai11.Opportunity__c = getOpportunity().Id;
        ai11.RecordTypeId = getRecordTypeRPA();
        ai11.Status__c = 'New';
        acList1.add(ai11);
        
        ai12.Opportunity__c = getOpportunity().Id;
        ai12.RecordTypeId = getRecordTypeRPA();
        ai12.Status__c = 'New';
        //objOppMap.remove(ai12.Opportunity__c);
        acList1.add(ai12);   
        
        ai13.Opportunity__c = getOpportunity().Id;
        ai13.RecordTypeId = getRecordTypeRPA();
        ai13.Status__c = 'Completed';
        //ai13.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai13.Completion_Date__c = System.today();
        ai13.Pricing_Feedback__c = 'Approved';
        ai13.Pricing_Approval_Comments__c = 'Approved';
        ai13.Additional_information_notes__c = 'Approved';
        acList1.add(ai13);
        
        ai14.Opportunity__c = getOpportunity().Id;
        ai14.RecordTypeId = getRecordTypeRPA();
        ai14.Status__c = 'Completed';
        //ai14.Completed_By__c = getOpportunity().CreditCheckDoneBy__c;
        ai14.Completion_Date__c = System.today();
        ai14.Pricing_Feedback__c = 'Approved';
        ai14.Pricing_Approval_Comments__c = 'Approved';
        ai14.Additional_information_notes__c = 'Approved';
        acList1.add(ai14);           
        
        ai15.Opportunity__c = getOpportunity().Id;
        ai15.RecordTypeId = getRecordTypeRC();
        ai15.Status__c ='Completed';
        ai15.Special_Terms_and_Conditions__c = 'xxxx';
        ai15.LegalComments__c = 'xxxx';
        acList1.add(ai15);
        
        ai16.Opportunity__c = getOpportunity().Id;
        ai16.Status__c ='Completed';
        ai16.RecordTypeId = getRecordTypeRC();
        ai16.Special_Terms_and_Conditions__c = 'xxxx';
        ai16.LegalComments__c = 'xxxx';
        acList1.add(ai16);           
        
        ai17.Opportunity__c = getOpportunity().Id;
        ai17.RecordTypeId = getRecordTypeRSD();
        ai17.Status__c ='Completed';
        acList1.add(ai17);
        
        ai18.Opportunity__c = getOpportunity().Id;
        ai18.RecordTypeId = getRecordTypeRSD();
        ai18.Status__c ='Completed';
        acList1.add(ai18);
        
        ai19.Opportunity__c = getOpportunity().Id;
        ai19.RecordTypeId = getRecordTypeSGRR();
        ai19.Status__c ='Completed';
        ai19.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai19);
        
        ai19_1.Opportunity__c = getOpportunity().Id;
        ai19_1.RecordTypeId = getRecordTypeSGRR();
        ai19_1.Status__c ='Completed';
        ai19_1.Stage_Gate_Number__c = 1;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai19_1);
        
        ai20.Opportunity__c = getOpportunity().Id;
        ai20.RecordTypeId = getRecordTypeSGRR();
        ai20.Status__c ='Completed';
        ai20.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai20);
        
        ai20_1.Opportunity__c = getOpportunity().Id;
        ai20_1.RecordTypeId = getRecordTypeSGRR();
        ai20_1.Status__c ='Completed';
        ai20_1.Stage_Gate_Number__c = 2;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai20_1);
        
        ai21.Opportunity__c = getOpportunity().Id;
        ai21.RecordTypeId = getRecordTypeSGRR();
        ai21.Status__c ='Completed';
        ai21.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai21);
        
        ai21_1.Opportunity__c = getOpportunity().Id;
        ai21_1.RecordTypeId = getRecordTypeSGRR();
        ai21_1.Status__c ='Completed';
        ai21_1.Stage_Gate_Number__c = 3;
        //ai15.Stage_Gate_3_Review_Completed__c = true
        acList1.add(ai21_1);
        
        ai22.Opportunity__c = getOpportunity().Id;
        ai22.RecordTypeId = getRecordTypeSGRR();
        ai22.Status__c ='Completed';
        ai22.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true
        acList1.add(ai22);
        
        ai22_1.Opportunity__c = getOpportunity().Id;
        ai22_1.RecordTypeId = getRecordTypeSGRR();
        ai22_1.Status__c ='Completed';
        ai22_1.Stage_Gate_Number__c = 4;
        //ai16.Stage_Gate_4_Review_Completed__c = true;
        acList1.add(ai22_1);
     
        
        ai23.Opportunity__c = getOpportunity().Id;
        ai23.RecordTypeId = getRecordTypeSGRR();
        ai23.Status__c ='Assigned';
        ai23.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList1.add(ai23);
        
        ai23_1.Opportunity__c = getOpportunity().Id;
        ai23_1.RecordTypeId = getRecordTypeSGRR();
        ai23_1.Status__c ='Assigned';
        ai23_1.Stage_Gate_Number__c = 1;
        //ai17.Stage_Gate_1_Action_Created__c = true
        acList1.add(ai23_1);
        
        ai24.Opportunity__c = getOpportunity().Id;
        ai24.RecordTypeId = getRecordTypeSGRR();
        ai24.Status__c ='Assigned';
        ai24.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList1.add(ai24);
        
        ai24_1.Opportunity__c = getOpportunity().Id;
        ai24_1.RecordTypeId = getRecordTypeSGRR();
        ai24_1.Status__c ='Assigned';
        ai24_1.Stage_Gate_Number__c = 2;
        //ai18.Stage_Gate_2_Action_Created__c = true
        acList1.add(ai24_1);
        
        ai25.Opportunity__c = getOpportunity().Id;
        ai25.RecordTypeId = getRecordTypeSGRR();
        ai25.Status__c ='Assigned';
        ai25.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList1.add(ai25);
        
        ai25_1.Opportunity__c = getOpportunity().Id;
        ai25_1.RecordTypeId = getRecordTypeSGRR();
        ai25_1.Status__c ='Assigned';
        ai25_1.Stage_Gate_Number__c = 3;
        //ai19.Stage_Gate_3_Action_Created__c = true
        acList1.add(ai25_1);
        
        ai26.Opportunity__c = getOpportunity().Id;
        ai26.RecordTypeId = getRecordTypeSGRR();
        ai26.Status__c ='Assigned';
        ai26.Stage_Gate_Number__c = 4;
        //ai21.Stage_Gate_4_Action_Created__c = true
        acList1.add(ai26);
        
        ai26_1.Opportunity__c = getOpportunity().Id;
        ai26_1.RecordTypeId = getRecordTypeSGRR();
        ai26_1.Status__c ='Assigned';
        ai26_1.Stage_Gate_Number__c = 4;
        //ai21.Stage_Gate_4_Action_Created__c = true
        acList1.add(ai26_1);

            insert acList1;
            system.assert(acList1 !=null);        
    }
    
    
    private static Opportunity getOpportunity(){
        //if(opp == null) {
        opp = new Opportunity();
        //Account acc = new Account()
        //acc =getAccount();
        //opp.Id= getRecordType().id;
        opp.Name = 'test';
        opp.Opportunity_Type__c = 'Simple';
        opp.Sales_Status__c = 'Open';
        opp.AccountId = getAccount().Id;
        opp.Order_Type__c = 'New';
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        opp.Approved_By__c=UserInfo.getUserId();
        opp.CreditCheckDoneBy__c='Sumit Suman';

            insert opp;
            system.assert(opp!=null);
        //}
        return opp;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            cl = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_Status__c = 'Active';
            acc.Billing_Status__c = 'Active';
            acc.Account_ID__c = '9090';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
        }
        return acc;
    }

    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    }   
       
    
    static testMethod void updateOpportunityMethod1(){
        
        Test.startTest();
            ActionItemBusinessClass bCLass= new ActionItemBusinessClass();
            system.assert(bCLass!=null);
            initTestData1();
        Test.stopTest();
        
    } 
  /*  @istest
    public static void unittest1()
    {
      List<user> userlist = new List<user>();
      profile pr = [select id from profile where name=' TI Pricing User'];
      user u1 = new user();
     // u1.name='Testuserdemo';
      u1.Alias = 'standt';
      u1.Email='standarduser1@testorg.com';
      u1.EmailEncodingKey='UTF-8';
      u1.LastName='Testing1';
      u1.LanguageLocaleKey='en_US';
      u1.EmployeeNumber = '2233233';
      u1.LocaleSidKey='en_US';
      u1.TimeZoneSidKey='America/Los_Angeles';
      u1.UserName='standarduser1231456@testorg.com';
      u1.ProfileId = pr.Id;
      userlist.add(u1);
      insert userlist;
      List<Account> accList1 = P2A_TestFactoryCls.getAccounts(1);
      
      
   List<Opportunity>  OppList1 = new List<Opportunity>();
    Opportunity opp1   = new Opportunity();  
    opp1.AccountId = accList1[0].id;
    opp1.Service_Delivery_Approver__c = userlist[0].id;
    opp1.Approved_Date__c = system.today();
     opp1.name          = 'Test Opp ';
     opp1.stagename     = 'Identify & Define';
                opp1.CloseDate     = system.today() + 10;
                opp1.Pre_Contract_Provisioning_Required__c = 'No';
                opp1.QuoteStatus__c = 'Draft';
                opp1.Product_Type__c = 'IPL';
                opp1.Estimated_MRC__c = 100.00;
                opp1.Estimated_NRC__c = 100.00;
                opp1.Opportunity_Type__c = 'Simple';
                opp1.Sales_Status__c = 'Open';
                opp1.Order_Type__c = 'New';
                opp1.Stage__c='Identify & Define';
                opp1.ContractTerm__c='10';
                opp1.Approved_By__c=UserInfo.getUserId();
                opp1.CreditCheckDoneBy__c='Sumit Suman';
          OppList1.add(opp1);
          insert  OppList1;
          
                  
      id recordtype = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Request Pre Provisioning');        
      List<Action_Item__c> newActionItems =new List<Action_Item__c>();
      Action_item__c ai = new Action_item__c();
      ai.Opportunity__c = OppList1[0].id;
      ai.Status__c = 'Approved';
      ai.recordtypeid = recordtype;
      ai.Opportunity_Email__c = 'test@gmail.com';
      ai.Quote_Subject__c = 'Pricing Validation';
      //ai.Pricing_Case_Approval__c = 'Pricing';
      ai.Assigned_User__c = 'test@gmail.com';
      ai.Approved_by_Email__c = 'test@gmail.com';
      ai.Commercial_Impact__c = 'yes';
      ai.Due_Date__c = system.today()+ 5;
      ai.Assigned_To__c = userlist[0].id;
    //  ai.Rejection_Reason__c = 'Insufficient';
      ai.Order_Approval_Comments__c = 'Testtest';
      
      newActionItems.add(ai);
      insert newActionItems ; 
      
      Test.starttest();
     // ActionItemBusinessClass bCLass1 = new ActionItemBusinessClass();
      ActionItemBusinessClass.updateOpportunity();
      //bCLass1.updateOpportunity(OppList1);
      Test.Stoptest();    
      
    } */

    /*static testMethod void updateOpportunityMethod2(){
        
        
        ActionItemBusinessClass bCLass= new ActionItemBusinessClass();
        Test.startTest();
            initTestData2();
        Test.stopTest();
        
    } 
    */
      
}