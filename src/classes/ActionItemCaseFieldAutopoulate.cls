//This class has been written for CR 52 to populate the case object based on the pricing validation action item

public without sharing class ActionItemCaseFieldAutopoulate
{

    public static void Fieldpopulate(List<Action_Item__c> newActionItems)
    {
    try{
    list<case> caselist=new list<case>();
    map<id,Action_Item__c> acitemmap=new map<id,Action_Item__c>();
    for(Action_Item__c ac:newActionItems)
    {
    if(ac.Opportunity__c!=null && ac.Quote_Subject__c=='Pricing Validation' && (ac.status__c == 'Approved' || ac.status__c == 'Closed - Non Commercial')){
    acitemmap.put(ac.Opportunity__c,ac);
    }
    }
   
   /* for(Action_Item__c acitem:[select id,Recordtype.name,Pricing_Case_Approval__c,Opportunity__r.id from Action_Item__c where id in:AIIDlist and Recordtype.name='Pricing Validation' and Pricing_Case_Approval__c='Order Desk'])
    {
    acitemmap.put(acitem.Opportunity__r.id,acitem);
    }*/
    if(acitemmap.size()>0){
    for(case c:[select id,Subject,Accountid,Opportunity_Name__c ,Pricing_Approval_Case__c from case where Opportunity_Name__c in:acitemmap.keyset() and subject like 'Pricing approval request for Quote%'])
    {
    if(acitemmap.get(c.Opportunity_Name__c).Status__c!='Approved' || acitemmap.get(c.Opportunity_Name__c).Status__c=='Closed - Non Commercial')
          {
            c.Pricing_Approval_Case__c= 'Pricing';
            
          }else
          {
            c.Pricing_Approval_Case__c=acitemmap.get(c.Opportunity_Name__c).Pricing_Case_Approval__c; 
          }
    caselist.add(c);
    }
   } 
    if(!caselist.isempty())
    {
    update caselist;
    }
    }catch(Exception ex){
    system.debug('exception ocurred==>>'+ex);
    }
    }
}