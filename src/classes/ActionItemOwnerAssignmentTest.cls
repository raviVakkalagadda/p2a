@isTest(seealldata=false)
public class ActionItemOwnerAssignmentTest{

    static Account a;
    static Country_Lookup__c cl;
    static Lead ld;
    
    Static Account acc1 = getAccount1();

    static opportunity opp1=getOpportunity();
    static Lead lead1=getLead();
    
    
      private static Account getAccount1(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc; 
        List<Account> a = [select id,name from account where name ='Test Account']; 
        System.assertequals(acc.name, a[0].name);  
        system.assert(acc!=null);
        return acc;    
   } 
   private static Country_Lookup__c getCountry1(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country; 
           system.assert(country!=null);
           List<Country_Lookup__c > c = [Select id,Country_Code__c from Country_Lookup__c where Country_Code__c= 'HK']; 
           system.assertequals(country.Country_Code__c, c[0].Country_Code__c);   
           system.assert(country!=null);  
           return country;    
         } 
           private static Opportunity getOpportunity(){                 
            Opportunity opp = new Opportunity();            
            //Account acc = getAccount();           
            opp.Name = 'Test Opportunity';            
            opp.AccountId = acc1.Id;   
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
            opp.StageName = 'Identify & Define' ;  
            opp.Stage__c= 'Identify & Define' ;          
            opp.CloseDate = System.today();   
            opp.Opportunity_Type__c = 'Complex';
            opp.Estimated_MRC__c=8;
                opp.Estimated_NRC__c=2;
                opp.ContractTerm__c='1';
                opp.Order_Type__c='New';
                system.assert(opp!=null);
            //opp.Stage_Gate_1_Action_Created__c = true;         
            //opp.Approx_Deal_Size__c = 9000;                 
            return opp;    
           } 
           private static cscfga__Product_Basket__c  getProductbasket(){
              cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
              pb.cscfga__Opportunity__c =opp1.id;
              pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
              pb.csbb__Synchronised_With_Opportunity__c =true;
             
             return pb;  
           }
       
    
    private static Account getAccount(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        if(a == null){    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account1995';
            a.Customer_Type__c = 'GSP';
            //a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INCN';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Region__c='US';
            a.Account_ID__c='123456abcd1';
            a.Customer_Legal_Entity_Name__c='Test';
            insert a;
            
             List<Account> a1 = [select id,name from account where name ='Test Account1995']; 
            System.assertequals(a.name, a1[0].name);  
            system.assert(a!=null);
            
        }
        return a;
    }
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            List<Country_Lookup__c> c2 = [Select id,CCMS_Country_Code__c from Country_Lookup__c where CCMS_Country_Code__c= 'SG'];
          //  system.assertequals(cl.CCMS_Country_Code__,c2[0].cCCMS_Country_Code__c);
            system.assert(cl!=null);
        }
        return cl;
    }
    
    private static Lead  getLead(){
            ld = new Lead();
            ld.Region__c = 'US';
            ld.Company = 'Telstra International12345';
            ld.LastName = 'Adp123';
            ld.Status = 'Marketing Qualified Lead (MQL)';
            ld.LeadSource = 'Direct Mail';
            ld.Email='test@email.com';
            ld.Industry='Telecommunications';
            ld.Country_Picklist__c = 'BANGLADESH';
            insert ld;
            system.assert(ld!=null);
            return ld;
    }
    
  
    static testmethod void testAssignActionItemOwner(){
        P2A_TestFactoryCls.sampletestdata();
        String accountId = acc1.id;
        String leadId = lead1.id;
       
       // ActionItemOwnerAssignment  actionItemObj = new ActionItemOwnerAssignment();
       // ActionItemOwnerAssignment.assignActionItemOwner(accountId);
        
        try{
      
        ActionItemOwnerAssignment  actionItemObj = new ActionItemOwnerAssignment();
        ActionItemOwnerAssignment.assignActionItemOwner(accountId);
        ActionItemOwnerAssignment.assignActionItemOwnertoLead(leadId); 
        system.assert(leadId!=null);
        } Catch (Exception e){
        System.debug('The following exception has occurred: ' + e.getMessage());
        
        }
        
   
    } 
       static testmethod void testAssignActionItemOwner1(){ 
           P2A_TestFactoryCls.sampletestdata();
           String accountId1= getAccount().id;
           ActionItemOwnerAssignment  actionItemObj = new ActionItemOwnerAssignment();
           system.assert(accountId1!=null);
           try{
           ActionItemOwnerAssignment.assignActionItemOwner(accountId1);
           }catch(Exception e){
           }
       }

}