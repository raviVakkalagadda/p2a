/* Type   : Managed Apex Sharing for Action_Item__c
// Author : Siddharth Sinha
// Date   : 02-Jun-2016
// The AI object's OWD has been made as Private
// This is to extend the Manual Sharing of the AI to AI Account Owner, AI Created By, AI Last Modified By and AI Opportunity Owner
*/ 

public without sharing class ActionItem_Share{
List<Action_Item__Share> AIShares = new List<Action_Item__Share>();

public void CustomShare(List<Action_Item__c> olist){
for(Action_Item__c t:olist){
 /** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
 Action_Item__Share AI_CreatedBy = new Action_Item__Share();
 Action_Item__Share AI_AccountOwner = new Action_Item__Share();
 Action_Item__Share AI_OppOwner = new Action_Item__Share();
  Action_Item__Share AI_LastModBy = new Action_Item__Share();
 /** Populate the Action_Item_Share record with the ID of the record to be shared. **/
  AI_CreatedBy.ParentId = t.Id;
  AI_AccountOwner.ParentId = t.Id;
  AI_OppOwner.ParentId = t.Id;
  AI_LastModBy.ParentId = t.Id;

/* Debug Testing
system.debug('Sid.OwnerId'+t.CreatedById);
system.debug('Sid.Account__r.OwnerId'+t.Account__r.OwnerId);
system.debug('Sid.Account__r.OwnerId'+t.Opportunity__r.Owner.Id);
system.debug('Sid.AW'+t.Account_Owner_Id__c);
system.debug('Sid.OW'+t.Opportunity_Owner_Id__c);
*/


 /** Share to - AI Creator **/ 
 If(t.CreatedById !=null)
   {
        AI_CreatedBy.UserOrGroupId = t.CreatedById;
        System.debug('New Logo:::'+t.CreatedById);
        /** Specify that the Apex sharing reason **/
        AI_CreatedBy.RowCause = Schema.Action_Item__Share.RowCause.AI_Creator__c;
   }
    /** Share to - AI Last Modified By **/ 
  If(t.lastModifiedById !=null)
   {
        AI_LastModBy.UserOrGroupId = t.LastModifiedById;
        /** Specify that the Apex sharing reason **/
        AI_LastModBy.RowCause = Schema.Action_Item__Share.RowCause.Last_Modified_By__c;
   }
  /** Share to - AI Account Owner **/ 
 If(t.Account_Owner_Id__c !=null)
   {
       AI_AccountOwner.UserOrGroupId = t.Account_Owner_Id__c;
       /** Specify that the Apex sharing reason **/
       AI_AccountOwner.RowCause = Schema.Action_Item__Share.RowCause.Account_Owner__c;
   }

  /** Share to - AI Opportunity Owner **/ 
 If(t.Opportunity_Owner_Id__c !=null)
   {
       AI_OppOwner.UserOrGroupId = t.Opportunity_Owner_Id__c;
       /** Specify that the Apex sharing reason **/
       AI_OppOwner.RowCause = Schema.Action_Item__Share.RowCause.Opportunity_Owner__c;
   }
   
 /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
  AI_CreatedBy.AccessLevel = 'Read';
  AI_AccountOwner.AccessLevel = 'Read';
  AI_OppOwner.AccessLevel = 'Read';
  AI_LastModBy.AccessLevel = 'Read';

 /** Add the new Share record to the list of new Share records. **/
 AIShares.add(AI_CreatedBy);
 AIShares.add(AI_AccountOwner);
 AIShares.add(AI_OppOwner);
 AIShares.add(AI_LastModBy);
 }

 /** Insert all of the newly created Share records and capture save result **/
 Database.SaveResult[] jobShareInsertResult = Database.insert(AIShares,false);
        /* This section be enabled to know debug testing in case the manual sharing by this class is not working.
           for (Database.SaveResult sr : jobShareInsertResult) {
            if (sr.isSuccess()) {
            // Operation was successful, so get the ID of the record that was processed
                System.debug('Successfully inserted AIID:' + sr.getId());
            }
           else {
                 for(Database.Error err : sr.getErrors()) {
                    System.debug('The following error has occurred.');                   
                    System.debug(err.getStatusCode() + ': ' + err.getMessage());
                    System.debug('Account fields that affected this error: ' + err.getFields());
                    }
                 }
           }*/
  }
}