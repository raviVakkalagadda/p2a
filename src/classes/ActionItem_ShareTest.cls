//Test Class for trigger : ActionItem_Share
//Author : Siddharth Sinha 

@isTest
private class ActionItem_ShareTest {
    public static List<user> usrList = P2A_TestFactoryCls.get_Users(1);
    public static List<Account> acctlist = P2A_TestFactoryCls.getAccounts(1);
    public static List<Opportunity> opptylist = P2A_TestFactoryCls.getOpportunitys(1,acctlist);
    public static List<Action_Item__c> actionItemList = P2A_TestFactoryCls.getActIteList(1);
    
    
static testMethod void testAddShares()
 {
     //SELECT Id, ParentId, UserOrGroupId, AccessLevel, RowCause, LastModifiedDate, LastModifiedById, IsDeleted FROM Action_Item__Share
     
     actionItemList[0].Account__c = acctlist[0].id;
     actionItemList[0].Opportunity__c = opptylist[0].id;
     update actionItemList;
     System.assert(actionItemList!=null);
        system.assertEquals(actionItemList[0].Account__c , acctlist[0].id);
     
    Action_Item__Share actionitemshare = new Action_Item__Share();
    actionitemshare.ParentId = actionItemList[0].id;
    actionitemshare.UserOrGroupId = usrList[0].id;
    actionitemshare.AccessLevel ='edit';
    actionitemshare.RowCause = 'Manual';
    
    Test.startTest();
    insert actionitemshare;
    
     
 ActionItem_Share accshare = new ActionItem_Share();
 accshare.CustomShare(actionItemList);
  Test.stopTest();

 }
}