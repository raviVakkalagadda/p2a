public class AddEVPLPort{
    
    static boolean flag = true;
    
    /**
     * Add EVP ports automatically in the basket for the EVPL port.
     */
    @testvisible
    public static void EVPPorts(List<cscfga__Product_Configuration__c> newProductConfigs){
        Set<Id> bsktIdList = new Set<Id>();
        for(cscfga__Product_Configuration__c pc :newProductConfigs){
            if(pc.cscfga__Product_Basket__c != null && pc.Basket_Quote_Status__c != 'Approved'){
                bsktIdList.add(pc.cscfga__Product_Basket__c); 
            }
        }

        if(bsktIdList.Size() > 0){
                AddEVPLPort.addEVPPorts(bsktIdList);
        }
    }   
    
    public static void addEVPPorts(Set<Id> bsktIDList){
        
            List<csbb__Product_Configuration_Request__c> toInsert = new List<csbb__Product_Configuration_Request__c>();
            Map<Id, cscfga__Product_Configuration__c> evplPClistMap = new Map<Id, cscfga__Product_Configuration__c>();
            List<cscfga__Product_Configuration__c> evpPC = new List<cscfga__Product_Configuration__c>();
            cscfga__Product_Configuration__c aEndPC = new cscfga__Product_Configuration__c();
            cscfga__Product_Configuration__c zEndPC = new cscfga__Product_Configuration__c();
            Map<Id, Id> oldEVPLAEndPort = new Map<Id, Id>();
            Map<Id, Id> oldEVPLZEndPort = new Map<Id, Id>();
            Map<Id, Id> evplAEndPort = new Map<Id, Id>();
            Map<Id, Id> evplZEndPort = new Map<Id, Id>();
        
            List<cscfga__Product_Configuration__c> listPC = [SELECT id,Name,EvplId__c,cscfga__Product_Basket__c,CurrencyIsoCode,RecordID__c,EvplVlan_Transparent__c,AddVlan__c,EVPLAEndPop__c,EVPLZEndPop__c,csordtelcoa__Replaced_Product_Configuration__c,csordtelcoa__Replaced_Product_Configuration__r.EvplId__c,cscfga__Contract_Term__c,Customer_Required_Date__c,NNI_bandwidth__c,Add_NID__c 
                From cscfga__Product_Configuration__c 
                    Where cscfga__Product_Basket__c IN :bsktIDList];

        system.debug('listPC sap'+listPC);

        if(listPC.Size() > 0 && bsktIDList.Size() > 0){
            
            for(cscfga__Product_Configuration__c prodId : listPC){
                if(prodId.Name == 'A End Port' && prodId.EvplId__c != null){
                    evplAEndPort.put(prodId.EvplId__c, prodId.Id);
                }
                if(prodId.Name == 'Z End Port' && prodId.EvplId__c != null){
                    evplZEndPort.put(prodId.EvplId__c, prodId.Id);
                }
                if(prodId.Name == 'EVPL'){
                    evplPClistMap.put(prodId.Id, prodId);                   
                }
                if(prodId.csordtelcoa__Replaced_Product_Configuration__r.EvplId__c != null && prodId.Name == 'A End Port'){
                    oldEVPLAEndPort.put(prodId.csordtelcoa__Replaced_Product_Configuration__r.EvplId__c, prodId.Id);
                }
                if(prodId.csordtelcoa__Replaced_Product_Configuration__r.EvplId__c != null && prodId.Name == 'Z End Port'){
                    oldEVPLZEndPort.put(prodId.csordtelcoa__Replaced_Product_Configuration__r.EvplId__c, prodId.Id);
                }
            }
            
            for(cscfga__Product_Configuration__c prodId : listPC){
                
                if(prodId.AddVlan__c != null && prodId.Name == 'EVPL' && flag == true && prodId.csordtelcoa__Replaced_Product_Configuration__c == null){
                    
                    //A End EVP Port
                     if(prodId.AddVlan__c != 'Yes' && evplAEndPort.get(prodId.Id) == null){
                        aEndPC.Name = 'A End Port';
                        aEndPC.cscfga__Product_Definition__c = System.Label.EVPLPortProdLabel;
                        aEndPC.cscfga__Description__c = 'A End Port';
                        aEndPC.cscfga__Configuration_Status__c = 'Incomplete';                    
                        aEndPC.cscfga__Product_Basket__c = prodId.cscfga__Product_Basket__c;
                        aEndPC.cscfga__Quantity__c = 1;
                        aEndPC.cscfga__originating_offer__c = System.Label.AEndPortOfferlabel;
                        aEndPC.EvplId__c = prodId.RecordID__c;  
                        aEndPC.EVPLAEndPop__c = prodId.EVPLAEndPop__c;
                        aEndPC.CurrencyIsoCode = prodId.CurrencyIsoCode;
                        evpPC.add(aEndPC);
                    }

                    //Z End EVP Port                    
                    if(prodId.AddVlan__c != 'Yes' && evplZEndPort.get(prodId.Id) == null){
                        zEndPC.Name = 'Z End Port';
                        zEndPC.cscfga__Product_Definition__c = System.Label.EVPLPortProdLabel;
                        zEndPC.cscfga__Description__c = 'Z End Port';
                        zEndPC.cscfga__Configuration_Status__c = 'Incomplete';                    
                        zEndPC.cscfga__Product_Basket__c = prodId.cscfga__Product_Basket__c;
                        zEndPC.cscfga__Quantity__c = 1;
                        zEndPC.cscfga__originating_offer__c = System.Label.ZEndPortOfferlabel;
                        zEndPC.EvplId__c = prodId.RecordID__c;     
                        zEndPC.EVPLAEndPop__c = prodId.EVPLZEndPop__c;
                        zEndPC.CurrencyIsoCode = prodId.CurrencyIsoCode;
                        evpPC.add(zEndPC);
                    }

                    if(evpPC.size() > 0){
						insertEvpPort(evpPC);
                        //insert evpPC;
                        flag = false;
                    }
                    
                    if(aEndPC.id != null ){
                        evpPortPOP(aEndPC.id, aEndPC.Name, aEndPC.EVPLAEndPop__c, aEndPC.cscfga__Product_Definition__c, prodId.cscfga__Contract_Term__c, prodId.Customer_Required_Date__c, prodId.NNI_bandwidth__c, prodId.Add_NID__c,prodId.RecordID__c);
                        toInsert.add (new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = prodId.cscfga__Product_Basket__c
                            , csbb__Product_Category__c = System.Label.EVPLPortLabel
                            , csbb__Product_Configuration__c = aEndPC.id
                            , csbb__Optionals__c = '{}'));                      
                    }

                    if(zEndPC.id != null){
                        evpPortPOP(zEndPC.id, zEndPC.Name, zEndPC.EVPLAEndPop__c, zEndPC.cscfga__Product_Definition__c, prodId.cscfga__Contract_Term__c, prodId.Customer_Required_Date__c, prodId.NNI_bandwidth__c, prodId.Add_NID__c,prodId.RecordID__c);
                        toInsert.add (new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = prodId.cscfga__Product_Basket__c
                            , csbb__Product_Category__c = System.Label.EVPLPortLabel
                            , csbb__Product_Configuration__c = zEndPC.id
                            , csbb__Optionals__c = '{}'));                      
                    }
                                
                    if(toInsert.size() > 0){
                        //insert toInsert;
                        flag = false;
                    }

                }
            }
       
            if(toInsert.size() > 0){
                        insert toInsert;
                        //flag = false;
                    }
		
        if((oldEVPLAEndPort.size() > 0 || oldEVPLZEndPort.size() > 0)){
            rplcOldEVPLPC(listPC, oldEVPLAEndPort, oldEVPLZEndPort);
        }
        
        if(evplPClistMap.size() > 0){
            evpPortsAttrbt(evplPClistMap, evplAEndPort, evplZEndPort);
            system.debug('evplPClistMap sap'+evplPClistMap);
        }
        
        }
    }
	
	 public static void insertEvpPort(List<cscfga__Product_Configuration__c> addEvpPort)
	 {
		 insert addEvpPort;
	 }
    
    public static void evpPortPOP(Id pcId, String portType, String EVPLAEndPop, Id attProdDefId, Decimal contractTerm, Date CRD, String bandwidth, String AddNID, Id EVPLPCId){
        
        List<cscfga__Attribute__c> atrbtList = new List<cscfga__Attribute__c>();
        cscfga__Attribute__c attIds = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds1 = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds2 = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds3 = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds4 = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds5 = new cscfga__Attribute__c();
        cscfga__Attribute__c attIds6 = new cscfga__Attribute__c();
                
        if(pcId != null){
            attIds.Name = 'POP__c';
            attIds.cscfga__Value__c = EVPLAEndPop;
            attIds.cscfga__Display_Value__c = EVPLAEndPop;
            attIds.cscfga__Product_Configuration__c = pcId;
            attIds.cscfga__Attribute_Definition__c = attId('POP__c', attProdDefId);
            atrbtList.add(attIds);

            attIds1.Name = 'Contract Term';
            attIds1.cscfga__Value__c = System.Label.Contract_Term.Contains(String.ValueOf(contractTerm)) ? String.ValueOf(contractTerm) : 'Other';
            attIds1.cscfga__Display_Value__c = System.Label.Contract_Term.Contains(String.ValueOf(contractTerm)) ? String.ValueOf(contractTerm) : 'Other';
            attIds1.cscfga__Product_Configuration__c = pcId;
            attIds1.cscfga__Attribute_Definition__c = attId('Contract Term', attProdDefId);
            atrbtList.add(attIds1);                     
            
            if(!System.Label.Contract_Term.Contains(String.ValueOf(contractTerm))){
                attIds2.Name = 'NonStandardContractTerm';
                attIds2.cscfga__Value__c = String.ValueOf(contractTerm);
                attIds2.cscfga__Display_Value__c = String.ValueOf(contractTerm);
                attIds2.cscfga__Product_Configuration__c = pcId;
                attIds2.cscfga__Attribute_Definition__c = attId('NonStandardContractTerm', attProdDefId);
                atrbtList.add(attIds2);
            }

            attIds3.Name = 'Bandwidth';
            attIds3.cscfga__Value__c = bandwidth;
            attIds3.cscfga__Display_Value__c = bandwidth;
            attIds3.cscfga__Product_Configuration__c = pcId;
            attIds3.cscfga__Attribute_Definition__c = attId('Bandwidth', attProdDefId);
            atrbtList.add(attIds3);

            attIds4.Name = 'Add NID';
            attIds4.cscfga__Value__c = AddNID;
            attIds4.cscfga__Display_Value__c = AddNID;
            attIds4.cscfga__Product_Configuration__c = pcId;
            attIds4.cscfga__Attribute_Definition__c = attId('Add NID', attProdDefId);
            atrbtList.add(attIds4);
            
            attIds5.Name = 'EVP Port Type';
            attIds5.cscfga__Value__c = portType;
            attIds5.cscfga__Display_Value__c = portType;
            attIds5.cscfga__Product_Configuration__c = pcId;
            attIds5.cscfga__Attribute_Definition__c = attId('EVP Port Type', attProdDefId);
            atrbtList.add(attIds5);
            
            attIds6.Name = 'EVPL PC Id';
            attIds6.cscfga__Value__c = EVPLPCId;
            attIds6.cscfga__Display_Value__c = EVPLPCId;
            attIds6.cscfga__Product_Configuration__c = pcId;
            attIds6.cscfga__Attribute_Definition__c = attId('EVPL PC Id', attProdDefId);
            atrbtList.add(attIds6);
        }
                
        if(atrbtList.size() > 0){
            insert atrbtList;
        }
    }

    public static Id attId(String attName, Id attProdDefId){
        cscfga__Attribute_Definition__c attId = [select id from cscfga__Attribute_Definition__c where Name =:attName and cscfga__Product_Definition__c =:attProdDefId];
        return attId.Id;
    }

    public static void rplcOldEVPLPC(List<cscfga__Product_Configuration__c> listPC, Map<Id, Id> oldEVPLAEndPort, Map<Id, Id> oldEVPLZEndPort){
        try{
            List<cscfga__Product_Configuration__c> evpPC = new List<cscfga__Product_Configuration__c>();
            Map<Id, Id> rplcdEVPLMapList = new Map<Id, Id>();
                
            for(cscfga__Product_Configuration__c pc :listPC){
                
                if(pc.csordtelcoa__Replaced_Product_Configuration__c != null && pc.Name == 'EVPL'){
                    rplcdEVPLMapList.put(oldEVPLAEndPort.get(pc.csordtelcoa__Replaced_Product_Configuration__c), pc.Id);
                    rplcdEVPLMapList.put(oldEVPLZEndPort.get(pc.csordtelcoa__Replaced_Product_Configuration__c), pc.Id);
                }
            }

            for(cscfga__Product_Configuration__c pc :listPC){
                
                if(pc.EvplId__c != null && oldEVPLAEndPort.get(pc.EvplId__c) == pc.Id){
                    pc.EvplId__c = rplcdEVPLMapList.get(pc.Id);
                    evpPC.add(pc);
                }
                
                if(pc.EvplId__c != null && oldEVPLZEndPort.get(pc.EvplId__c) == pc.Id){
                    pc.EvplId__c = rplcdEVPLMapList.get(pc.Id);
                    evpPC.add(pc);
                }

            }

            if(evpPC.size() > 0){
                update evpPC;
            }
        } catch(Exception e){
            System.debug('Error: "AddEVPLPort; rplcOldEVPLPC" method update failure - ' +e);
          }
    }
    
    public static void evpPortsAttrbt(Map<Id, cscfga__Product_Configuration__c> evplPortList, Map<Id, Id> evplAEndPort, Map<Id, Id> evplZEndPort){
        try{    
            List<cscfga__Attribute__c> atrbtList = new List<cscfga__Attribute__c>();
            
            //List<cscfga__Attribute__c> atList = [SELECT Id,Name,cscfga__Value__c,cscfga__Product_Configuration__r.EvplId__c,cscfga__Product_Configuration__r.Name From cscfga__Attribute__c Where cscfga__Product_Configuration__c in :evplAEndPort.Values() or cscfga__Product_Configuration__c in :evplZEndPort.Values()];
            
            for(cscfga__Attribute__c atrbt :[SELECT Id,Name,cscfga__Value__c,cscfga__Product_Configuration__r.EvplId__c,cscfga__Product_Configuration__r.Name From cscfga__Attribute__c Where cscfga__Product_Configuration__c in :evplAEndPort.Values() or cscfga__Product_Configuration__c in :evplZEndPort.Values()]){
                
                if(atrbt.cscfga__Product_Configuration__r.EvplId__c != null && atrbt.cscfga__Product_Configuration__r.Name != null){

                    if(atrbt.Name == 'POP__c' && atrbt.cscfga__Product_Configuration__r.Name == 'A End Port'){
                        atrbt.cscfga__Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).EVPLAEndPop__c;
                        atrbt.cscfga__Display_Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).EVPLAEndPop__c;
                        atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'POP__c' && atrbt.cscfga__Product_Configuration__r.Name == 'Z End Port'){
                            atrbt.cscfga__Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).EVPLZEndPop__c;
                            atrbt.cscfga__Display_Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).EVPLZEndPop__c;
                            atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'Bandwidth'){
                            atrbt.cscfga__Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).NNI_bandwidth__c;
                            atrbt.cscfga__Display_Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).NNI_bandwidth__c;
                            atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'Add NID'){
                            atrbt.cscfga__Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).Add_NID__c;
                            atrbt.cscfga__Display_Value__c = evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).Add_NID__c;
                            atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'Contract Term'){
                            atrbt.cscfga__Value__c = System.Label.Contract_Term.Contains(String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c)) ? String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c) : 'Other';
                            atrbt.cscfga__Display_Value__c = System.Label.Contract_Term.Contains(String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c)) ? String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c) : 'Other';
                            atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'NonStandardContractTerm' && !System.Label.Contract_Term.Contains(String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c))){
                            atrbt.cscfga__Value__c = String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c);
                            atrbt.cscfga__Display_Value__c = String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).cscfga__Contract_Term__c);
                            atrbtList.add(atrbt);
                    } 
                        else if(atrbt.Name == 'Customer_Required_Date_c'){
                            atrbt.cscfga__Value__c = String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).Customer_Required_Date__c);
                            atrbt.cscfga__Display_Value__c = String.ValueOf(evplPortList.get(atrbt.cscfga__Product_Configuration__r.EvplId__c).Customer_Required_Date__c);
                            atrbtList.add(atrbt);
                    }           
                }
            }
                   
            if(atrbtList.size() > 0){
                update atrbtList;
            }
        } catch(Exception e){
            System.debug('Error: "AddEVPLPort; evpPortsAttrbt" method update failure - ' +e);
          }     
    }
}