global class AddOnWidgetController {
	
	@RemoteAction
	global static String getAvaialbleAddOns(Id priceItem, String category, Id configId) {
		String retVal = '';
		try {
			List<cspmb__Price_Item_Add_On_Price_Item_Association__c> addOnAssociations = [
				select Id, cspmb__Add_On_Price_Item__c, cspmb__Price_Item__c
				from cspmb__Price_Item_Add_On_Price_Item_Association__c
				where cspmb__Price_Item__c = :priceItem
			];
			if (!addOnAssociations.isEmpty()) {
				Set<Id> addOnIds = new Set<Id>();
				
				for (cspmb__Price_Item_Add_On_Price_Item_Association__c addOnAssoc : addOnAssociations) {
					addOnIds.add(addOnAssoc.cspmb__Add_On_Price_Item__c);
				}
				//Building query for Add On Price Items
				String queryString = 'select ';
				queryString += 'Id, Name,';
				Add_On_Widget_Settings__c settings = Add_On_Widget_Settings__c.getValues(category);
				if (settings != null && settings.Add_On_Fields__c != null) {
					queryString += settings.Add_On_Fields__c;
				}
				if (settings != null && settings.Image__c != null && settings.Image__c != '') { // ZD
				    queryString += ',' + settings.Image__c;
				}
				queryString += ' from cspmb__Add_On_Price_Item__c';
				queryString += ' where Id in :addOnIds';
				if (settings != null && settings.Product_Definition__c != null) {
					queryString += ' and cspmb__Product_Definition_Name__c = \'' + settings.Product_Definition__c + '\'';
				}
				if (settings != null && settings.Record_Type_Name__c != null) {
					Id recordTypeId = RecordTypeUtil.getRecordTypeIdByName(cspmb__Add_On_Price_Item__c.SObjectType, settings.Record_Type_Name__c);
					queryString += ' and RecordTypeId = :recordTypeId';
				}

				//Fetching already selected Add Ons
				Map<String,String> mapSelectedAddOns = new Map<String,String>();
				String selectedAddOnsQuery;
				if (settings != null && settings.Custom_Object__c != null) {
					selectedAddOnsQuery = 'SELECT Name nameOfAddOn, COUNT(Id) cnt FROM ' + settings.Custom_Object__c + ' WHERE Product_Configuration__c =: configId GROUP BY Name';
				} else if (settings != null && settings.Add_On_Definition__c != null) {
					selectedAddOnsQuery = 'SELECT Add_On__c nameOfAddOn, COUNT(Id) cnt ' +
										  'FROM cscfga__Product_Configuration__c ' +
										  'WHERE cscfga__Root_Configuration__c =: configId AND cscfga__Product_Definition__r.Name = \'' + settings.Add_On_Definition__c + '\' GROUP BY Add_On__c';
				}

				System.debug(selectedAddOnsQuery);

				List<AggregateResult> selectedAddOns = Database.query(selectedAddOnsQuery);
				for(sObject sAddOns : selectedAddOns) {
					mapSelectedAddOns.put((String) sAddOns.get('nameOfAddOn'), String.valueOf((Integer) sAddOns.get('cnt')));
				}
				

				//Building JSON response from queried Add On Price Items
				List<Object> retList = new List<Object>();
				List<cspmb__Add_On_Price_Item__c> addOns = Database.query(queryString);
				for (cspmb__Add_On_Price_Item__c addon : addOns) {
					Map<String, String> addOnValues = new Map<String, String>();
					if (settings != null && settings.Add_On_Fields__c != null) {
						List<String> flds = settings.Add_On_Fields__c.split(',');
						for (String fld : flds) {
							String label = cspmb__Add_On_Price_Item__c.SObjectType.getDescribe().fields.getMap().get(fld).getDescribe().getLabel();
							String value = addon.get(fld) != null ? String.valueOf(addon.get(fld)) : '';
							addOnValues.put(label, value);
						}
					}
					if (settings != null && settings.Quantity__c) {
						addOnValues.put('Quantity', String.valueOf(true));
					} else {
						addOnValues.put('Quantity', String.valueOf(false));
					}
					if (settings != null && settings.Image__c != null && settings.Image__c != '') {
						String imageUrl = addOn.get(settings.Image__c) != null && addOn.get(settings.Image__c) != ''? String.valueOf(addOn.get(settings.Image__c)) : '';
						addOnValues.put('Image', imageUrl);
					} else {
						addOnValues.put('Image', '');
					}
					addOnValues.put('category', category);
					if (!mapSelectedAddOns.isEmpty()) {
						addOnValues.put('Selected', mapSelectedAddOns.containsKey(addon.Name) ? mapSelectedAddOns.get(addon.Name) : '0');
					} else {
						addOnValues.put('Selected', '0');
					}

					addOnValues.put('Name', addon.Name);
					addOnValues.put('Id', addon.Id);
					retList.add(addOnValues);
				}
				retVal = JSON.serialize(retList);
			}
		} catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='AddOnWidgetController:getAvaialbleAddOns';      
            ErrorHandlerException.sendException(e);
			retVal = 'Error - ' + e.getTypeName() + ': ' + e.getMessage() + ' : Line number: ' + e.getLineNumber();
		}
		System.debug(retVal);
		return retVal;
	}

	@RemoteAction
	global static void deleteAddOns(Id configId, String category) {
		try {
			if (category != null && category != '') {
				Add_On_Widget_Settings__c settings = Add_On_Widget_Settings__c.getValues(category);
				if (settings != null && settings.Add_On_Definition__c != null && settings.Add_On_Definition__c != '') {
					String definitionName = settings.Add_On_Definition__c;
					List<cscfga__Product_Configuration__c> configList = new List<cscfga__Product_Configuration__c>();
					configList = [
						select Id
						from cscfga__Product_Configuration__c
						where cscfga__Product_Definition__c = :definitionName
						and cscfga__Root_Configuration__c = :configId
					];
					if (!configList.isEmpty()) {
						delete configList;
					}
				}
				if (settings != null && settings.Custom_Object__c != null && settings.Custom_Object__c != '') {
					String objName = settings.Custom_Object__c;
					List<sObject> objList = Database.query('select id from ' + objName + ' where Product_Configuration__c = :configId');
					if (!objList.isEmpty()) {
						delete objList;
					}
				}
			}
		} catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='AddOnWidgetController:deleteAddOns';      
            ErrorHandlerException.sendException(e);
			system.debug(Logginglevel.Error, e.getMessage());
		}
	}
	
	@RemoteAction
	global static String saveAddOns(Id configId, List<Id> addOnIds, String category) {
		List<Id> newObjects = new List<Id>();
		try {
			cscfga__Product_Configuration__c config = [
				select id, cscfga__Product_Basket__c
				from cscfga__Product_Configuration__c
				where id = :configId
				limit 1
			];
			Id basketId = config.cscfga__Product_Basket__c;
			
			String queryString = 'select ';
			queryString += 'name,';
			
			Add_On_Widget_Settings__c settings = Add_On_Widget_Settings__c.getValues(category);
			
			if (settings != null && settings.Add_On_Fields__c != null) {
				queryString += settings.Add_On_Fields__c;
			}

			queryString += ' from cspmb__Add_On_Price_Item__c';
			queryString += ' where Id in :addOnIds';
			List<cspmb__Add_On_Price_Item__c> addOns = Database.query(queryString);
			Map<Id, cspmb__Add_On_Price_Item__c> addOnsMap = new Map<Id, cspmb__Add_On_Price_Item__c>(addOns);
			if (settings != null && settings.Custom_Object__c != null) {
				
				//Creating a list of custom objects fields which will be used for Add Ons
				List<String> objectFields = new List<String>();
				objectFields.add('name');
				if (settings != null && settings.Custom_Object_Fields__c != null) {
					for (String fld : settings.Custom_Object_Fields__c.split(',')) {
						objectFields.add(fld.toLowerCase());
					}
				}

				//Creating custom objects for Add Ons 
				List<sObject> addOnObjects = new List<sObject>();
				for (Id addOnId : addOnIds) {
					cspmb__Add_On_Price_Item__c addOn = addOnsMap.get(addOnId);
					Schema.SObjectType targetType = Schema.getGlobalDescribe().get(settings.Custom_Object__c);
					sObject so = targetType.newSObject();
					Set<String> sObjectFields = Schema.SObjectType.cspmb__Add_On_Price_Item__c.fields.getMap().keySet();
					so.put('Product_Configuration__c', configId);
					for (String fieldName : objectFields) {
						if (sObjectFields.contains(fieldName)) {
							so.put(fieldName, addOn.get(fieldName));
						} else if (sObjectFields.contains('cspmb__' + fieldName)) {
							so.put(fieldName, addOn.get('cspmb__' + fieldName));
						}
					}
					addOnObjects.add(so);
				}

				//Insertion of created custom objects
				if (!addOnObjects.isEmpty()) {
					insert addOnObjects;
					for (sObject so : addOnObjects) {
						newObjects.add(so.Id);
					}
				}
			} else if (settings != null && settings.Add_On_Definition__c != null) {
				Map<String, Id> addOnMap = new Map<String, Id>();
				cscfga__Product_Definition__c productDefinition = [
					select Id, Name
					from cscfga__Product_Definition__c
					where Name = :settings.Add_On_Definition__c
					and cscfga__Active__c = true
					limit 1
				];
				cscfga__Attribute__c parentAttribute;
				if (settings != null && settings.Parent_Attribute__c != null) {
					parentAttribute = [
						select Id,cscfga__Value__c
						from cscfga__Attribute__c
						where cscfga__Product_Configuration__c = :configId
						and Name = :settings.Parent_Attribute__c
						limit 1
					];
				}
				List<cscfga__Product_Configuration__c> newConfigs = new List<cscfga__Product_Configuration__c>();
				List<cscfga__Attribute__c> newAttributes = new List<cscfga__Attribute__c>();
				cscfga.API_1.ApiSession session = cscfga.Api_1.getApiSession(productDefinition);
				cscfga.ProductConfiguration pc = session.getRootConfiguration();
				List<cscfga.Attribute> attributes = pc.getAttributes();
				system.debug(attributes);
				for (Id addOnId : addOnIds) {
					cspmb__Add_On_Price_Item__c addOn = addOnsMap.get(addOnId);
					cscfga__Product_Configuration__c pc2 = pc.getSObject().clone(false, false);
					pc2.cscfga__Root_Configuration__c = configId;
					pc2.cscfga__Parent_Configuration__c = configId;
					pc2.cscfga__Product_Basket__c = basketId;
					pc2.Name = addOn.Name;
					pc2.Add_On__c = addOn.Name;
					pc2.Id = null;
					if (settings != null && settings.Parent_Attribute__c != null) {
						pc2.cscfga__Attribute_Name__c = settings.Parent_Attribute__c;
					}
					newConfigs.add(pc2);
					addOnMap.put(addOn.Name, addOn.Id);
				}
				Set<Id> configIds = new Set<Id>();
				if (!newConfigs.isEmpty()) {
					insert newConfigs;
					for (cscfga__Product_Configuration__c pconf : newConfigs) {
						newObjects.add(pconf.Id);
						for (cscfga.Attribute attr : attributes) {
							cscfga__Attribute__c att = attr.getSObject().clone(false, false);
							if (settings.Add_On_Attribute__c != null && settings.Add_On_Attribute__c == att.Name) {
								att.cscfga__Value__c = addOnMap.get(pconf.Name);
							}
							att.cscfga__Product_Configuration__c = pconf.Id;
							newAttributes.add(att);
						}
					}
					system.debug(JSON.serialize(newAttributes));
					if (!newAttributes.isEmpty()) {
						insert newAttributes;
					}
				}
				if (parentAttribute != null) {
					String val = parentAttribute.cscfga__Value__c != null &&  parentAttribute.cscfga__Value__c != '' ? parentAttribute.cscfga__Value__c : '';
					
					for (Id confId : newObjects) {
						if (val != '') {
							val += ',' + confId;
						} else {
							val += confId;
						}
					}
					parentAttribute.cscfga__Value__c = val;
					update parentAttribute;
				}
				cscfga.ProductConfigurationBulkActions.revalidateConfigurationsAsync(new Set<Id>(newObjects));
			}
		} catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='AddOnWidgetController:saveAddOns';      
            ErrorHandlerException.objectId=String.valueOf(addOnIds[0]);
            ErrorHandlerException.sendException(e);
			system.debug('Error - ' + e.getMessage());
		}
		return JSON.serialize(newObjects);
	}

}