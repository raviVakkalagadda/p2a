global with sharing class AddressCheck {

	webservice static String getAddresses (String city, String country) {
		system.debug('****** AddressCheck City ******** :' + city);
		String cityCondition = (city == '' ) ? '' : ' and CS_CityName__c like \'%' + city +'%\'';
		String countryCondition = (country == '') ? '' : ' and CS_CountryName__c like \'%' + country +'%\'';
		String orderBy = ' ORDER BY CS_CountryName__c, CS_CityName__c';
		system.debug('****** AddressCheck cityCondition ******** :' + cityCondition +':'+orderBy);
		String query = 'select Id, Name, Connectivity__c, CS_City__c, CS_Country__c, CS_CountryName__c, CS_CityName__c from CS_POP__c where IsIPVPN__c= true ' + cityCondition + countryCondition + orderBy;
		system.debug('****** AddressCheck query ******** :' + query);
		List<CS_POP__c> cities = Database.query(query);
		system.debug('****** AddressCheck cities ******** ' + cities);
		return JSON.serialize(cities);
	}
}