global with sharing class AddressCheckInternet {

  webservice static String getAddresses_Internet (String city, String country) {
    system.debug('****** AddressCheck City ******** :' + city);
    String cityCondition = (city == '' ) ? '' : ' CS_CityName__c like \'%' + city +'%\'';
    String countryCondition = (country == '') ? '' : ' CS_CountryName__c like \'%' + country +'%\'';
    if(cityCondition != ''){
        countryCondition = 'AND '+countryCondition;
    }
    String orderBy = ' ORDER BY CS_CountryName__c, CS_CityName__c';
    //String ConnectivityCondition = (connectivity == '' ) ? '' : ' CS_CityName__c like \'%' + city +'%\'';
    system.debug('****** AddressCheck cityCondition ******** :' + cityCondition +':'+orderBy);
    String query = 'select Id, Name, Connectivity__c, CS_City__c, CS_Country__c, CS_CountryName__c, CS_CityName__c from CS_POP__c where Connectivity__c=\'Onnet\' AND ' + cityCondition + countryCondition + orderBy;
    system.debug('****** AddressCheck query ******** :' + query);
    List<CS_POP__c> cities = Database.query(query);
    system.debug('****** AddressCheck cities ******** ' + cities);
    return JSON.serialize(cities);
  }
}