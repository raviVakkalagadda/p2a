@isTest(SeeAllData = false)
private class AddressCheckInternetTest {

    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_POP__c> popList;
    
    private static String city;
    private static String country;
    
    private static void initTestData(){
            countryList = new List<CS_Country__c>{
                new CS_Country__c(Name = 'Croatia'),
                new CS_Country__c(Name = 'Hong Kong')
            };
    
      insert countryList;
      list<CS_Country__c> con = [select id,Name from CS_Country__c where Name = 'Croatia'];
                                system.assertEquals(countryList[0].name , con[0].name);
                                System.assert(countryList!=null);
       
       
      cityList = new List<CS_City__c>{
                new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
                new CS_City__c(Name = 'Hong Kong', CS_Country__c = countryList[1].Id)
            };
            
     insert cityList;
     list<CS_City__c> city = [select id,Name from CS_City__c where Name = 'Croatia'];
                                system.assertEquals(cityList[0].name , city[0].name);
                                System.assert(cityList!=null);
     
 
     popList = new List<CS_POP__c>{
                new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id, CS_City__c = cityList[0].Id, IPVPN_Critical_Data__c = true),
                new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id, CS_City__c = cityList[1].Id, IPVPN_Critical_Data__c = true)
            };
    
     insert popList;
       list<CS_POP__c> pop = [select id,Name from CS_POP__c where Name = 'Croatia'];
                                system.assertEquals(popList[0].name , pop[0].name);
                                System.assert(popList!=null);
     
 
 }
         private static testMethod void getAddressesInternetTest() {
              
              Exception ee = null;
              integer userid = 0;
              try{
              CS_TestUtil.disableAll(UserInfo.getUserId());
              system.assertEquals(true,userid!=null); 
              Test.startTest();
              initTestData();           
                       
              
              }catch(Exception e){
              String iaddresses = AddressCheckInternet.getAddresses_Internet(city,country);
              ErrorHandlerException.ExecutingClassName='AddressCheckInternetTest:getAddressesInternetTest';         
                ErrorHandlerException.sendException(e); 
                         ee = e;
                         System.debug('**** catchexception : ' + ee);
                    }
            Test.stopTest();
            }
            
}