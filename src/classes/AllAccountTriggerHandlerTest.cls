@isTest
 private with Sharing class AllAccountTriggerHandlerTest{
     static Account acc;
     static Country_Lookup__c cl;
     static Global_Account__c ga;
     private static List<Account> accountList = new List<Account>();
      private static List<NewLogo__c> NewLogolist;
     
    
   
     
     public static testMethod void AllAccountTriggerHandlerMethod() {
     
        Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
                                       
     
        //User owner = [SELECT Id from USER WHERE Name = 'Siddharth Sinha' LIMIT 1][0];
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        
        Profile p=[SELECT Id From Profile WHERE Name='System Administrator'];
        User u2 =new User( Alias = 'newUser1' ,
                            Email ='newuser1231@testorg.com',
                            EmailEncodingKey = 'UTF-8',
                            EmployeeNumber = '132',
                            LastName = 'Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', // changed for to avoid: INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, Locale: bad value for restricted picklist field: America/Los_Angeles
                            UserName='newuser1231@testorg.com',
                            ProfileId=p.Id,
                            TimeZoneSidKey    = 'America/Los_Angeles');
        insert u2;
       
        System.runAs(u2) {
        Country_Lookup__c country = TestDataFactory.createCountry();
        Account accobj= TestDataFactory.createAccount('AccountTest',country,true); 
        Contact conobj = TestDataFactory.createContact('Bill','Profile',accobj,'Billprofile',country,true);
        
        List<Global_Constant_Data__c> listGCD = new List<Global_Constant_Data__c>();
        Global_Constant_Data__c  authTokenSetting =  new Global_Constant_Data__c(name ='Action Item Status',value__c = 'Assigned');
        listGCD.add(authTokenSetting); 
        Global_Constant_Data__c  authTokenSetting1 =  new Global_Constant_Data__c(name ='Action Item Priority',value__c = 'High');
        listGCD.add(authTokenSetting1);
        insert listGCD;
   system.assert(listGCD!=null);
   
                                       
     Action_Item__c objActionItem1= new Action_Item__c();
            //objActionItem9.OwnerID = BProfObjlist[0].OwnerID;
            // objActionItem1.Bill_Profile__c = BProfObjlist[0].Id;
               // objActionItem1.Account__c =  BProfObjlist[0].Account__c;
                objActionItem1.Subject_For_NewLogo__c = 'New Customer';
                objActionItem1.Due_Date__c = System.Today() + 2;  
                 objActionItem1.Status__c = 'Assigned';
                //objActionItem9.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.sObjectType, 'Adhoc Request');
                objActionItem1.Subject__c = 'Update Contract Documents';
                //objActionItem9.Status__c =  'High';
                //objActionItem9.Priority__c = listGCD[1].value__c;
                // Hard Coded Comments , need to Put them in Herirachial Custom Settings
                objActionItem1.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
                Insert objActionItem1;
                system.assert(objActionItem1!=null); 
                
       list<Action_Item__c> al = new list<Action_Item__c> ();  
       al.add(objActionItem1);                                                    
        
       
       
        Test.startTest();
        
        AllAccountTriggerHandler altrg = new AllAccountTriggerHandler ();
        altrg.afterGlobalAccountUndelete(accountList);
        system.assert(altrg!=null);
        //altrg.flagSentToTibco(accountList,accMap);
        //altrg.manageNewLogo(accountList,accMap);
        Test.stopTest();     
      } 
     } 
     
    static testMethod void getAccountDetails() {
    test.starttest();
     
        String accountOwner = Userinfo.getFirstName() + ' ' +Userinfo.getLastName();
         acc = new Account();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            cl = getCountry();
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Account_Manager__c = null;
            acc.Postal_Code__c='123456789';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            ga = new Global_Account__c();
            ga.Account__c = acc.id;
            ga.Name = 'test1243';
            ga.Country__c =cl.id;
            insert ga;  
            system.assert(ga!=null);  
            acc.Name = 'test2234';
            ga.name ='test2234';
            update ga;
            ga=[select id,Account__c from Global_Account__c where account__c =: acc.id limit 1];   
            delete ga; 
           // ga=[select Account__c from Global_Account__c where id =: ga.id and account__c =: acc.id limit 1];   
           Undelete ga;    
            update acc;
            delete acc;
           undelete acc;
            
            Map<Id, Account> accMap = new Map<Id, Account>{acc.Id => acc}; 
           
            test.stopTest();  
        AllAccountTriggerHandler altrg1 = new AllAccountTriggerHandler ();
        altrg1.beforeGlobalAccountDelete(accMap); 
        altrg1.beforeDelete(accMap);
        altrg1.afterUndelete(accountList,accMap);
    }
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'US';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'US';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    }   
    
     private static void initTestData(){
        
        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC',New_Logo__c = true,
             Win_Back__c = false),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC', Win_Back__c = false,New_Logo__c = true)
        };
        
        insert accountList;  
        system.assert(accountList!=null);
     
   }   
   
    public static testMethod  void unittest() {
          
       NewLogolist = new List<NewLogo__c>{
                    new NewLogo__c(name = 'ActionItem',Action_Item__c = 'Active'),
                    new NewLogo__c(name = 'ActionItem1',Action_Item__c = 'Action_Item__c'),
                    new NewLogo__c(name = 'ActionItem2',Action_Item__c = 'Logo_Approved'),
                    new NewLogo__c(name = 'ActionItem6',Action_Item__c = 'Logo_Customer'),
                    new NewLogo__c(name = 'ActionItem7',Action_Item__c = 'Logo_Former_Customer'),
                    new NewLogo__c(name = 'ActionItem8',Action_Item__c = 'Logo_New_Customer'),
                    new NewLogo__c(name = 'ActionItem9',Action_Item__c = 'Logo_New_Logo'),
                    new NewLogo__c(name = 'ActionItem10',Action_Item__c = 'Logo_Prospect'),
                    new NewLogo__c(name = 'ActionItem11',Action_Item__c = 'Logo_Rejected')
                              
       };
       Insert NewLogolist;
       system.assert(NewLogolist!=null);
       
     }  
}