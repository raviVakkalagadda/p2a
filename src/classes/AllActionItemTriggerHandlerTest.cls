@isTest
 private with Sharing class AllActionItemTriggerHandlerTest{
     
    @testSetup
    public static void setupTestData() 
   {
     }
     public static void AllActionItemTriggerHandlerMethod() {
     
        //User owner = [SELECT Id from USER WHERE Name = 'Siddharth Sinha' LIMIT 1][0];
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        
        Profile p=[SELECT Id From Profile WHERE Name='System Administrator'];
        User u2 =new User( Alias = 'newUser1' ,
                            Email ='newuser113523@testorg.com',
                            EmailEncodingKey = 'UTF-8',
                            EmployeeNumber = '132',
                            LastName = 'Testing',
                            LanguageLocaleKey='en_US',
                            LocaleSidKey='en_US', // changed for to avoid: INVALID_OR_NULL_FOR_RESTRICTED_PICKLIST, Locale: bad value for restricted picklist field: America/Los_Angeles
                            UserName='newuser0123456123@testorg.com',
                            ProfileId=p.Id,
                            TimeZoneSidKey    = 'America/Los_Angeles');
        insert u2;
       
        System.runAs(u2) {
        Country_Lookup__c country = TestDataFactory.createCountry();
        Account accobj= TestDataFactory.createAccount('AccountTest',country,true);
        accobj.Account_ID__c = '1234';
         List<account> accounts = new List<account>();
         accounts.add(accobj);
         upsert accounts;  
        Contact conobj = TestDataFactory.createContact('Bill','Profile',accobj,'Billprofile',country,true);
        List<Country_Lookup__c> countrys = P2A_TestFactoryCls.getcountry(1);
      List<Site__c> siteslist = P2A_TestFactoryCls.getsites(1,accounts,countrys);
        
        List<Global_Constant_Data__c> listGCD = new List<Global_Constant_Data__c>();
        Global_Constant_Data__c  authTokenSetting =  new Global_Constant_Data__c(name ='Action Item Status',value__c = 'Assigned');
        listGCD.add(authTokenSetting); 
        Global_Constant_Data__c  authTokenSetting1 =  new Global_Constant_Data__c(name ='Action Item Priority',value__c = 'High');
        listGCD.add(authTokenSetting1);
        insert listGCD;
        
        List<BillProfile__c> BillProfList = new List<BillProfile__c>();
        BillProfile__c BProfObj = new BillProfile__c();
        //BProfObj.OwnerID = u2.Id;
        BProfObj.Account__c = accobj.Id;
        BProfObj.Billing_Entity__c = 'Austrila';
        BProfObj.Payment_Terms__c = '45 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = conobj.id;
        BProfObj.Activated__c = true;
        BprofObj.Bill_Profile_Site__c= siteslist[0].id;
        BillProfList.add(BProfObj);
        insert BillProfList; 
        
        list<BillProfile__c> BProfObjlist = new list<BillProfile__c>();
        list<BillProfile__c> BProfObj2 = [select id,name,Account__c,Payment_Terms__c,Status__c,Billing_Frequency__c, 
                                             Activated__c from BillProfile__c where id=:BillProfList[0].id limit 1];
        for(BillProfile__c bprf:BProfObj2 ){
            bprf.Billing_Frequency__c  = 'North Asia Monthly Data';
            bprf.Account__c = accobj.Id;
            bprf.Payment_Terms__c = '60 Days';
            BProfObjlist.add(bprf);
        }                                  
        Update BProfObjlist;                                     
        
        if(BillProfList[0].Payment_Terms__c!= BProfObjlist[0].Payment_Terms__c || BillProfList[0].Billing_Frequency__c!= BProfObjlist[0].Billing_Frequency__c)
          {
          try{
            System.runAs(u2){
            Action_Item__c objActionItem9= new Action_Item__c();
            //objActionItem9.OwnerID = BProfObjlist[0].OwnerID;
            objActionItem9.Bill_Profile__c = BProfObjlist[0].Id;
                objActionItem9.Account__c =  BProfObjlist[0].Account__c;
                objActionItem9.Due_Date__c = System.Today() + 2;  
                //objActionItem9.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.sObjectType, 'Adhoc Request');
                objActionItem9.Subject__c = 'Update Contract Documents';
                //objActionItem9.Status__c =  'High';
                //objActionItem9.Priority__c = listGCD[1].value__c;
                // Hard Coded Comments , need to Put them in Herirachial Custom Settings
                objActionItem9.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
                Insert objActionItem9;
                }    
         } catch (Exception e) {
        }
      } 
   
      list<BillProfile__c> BilProfile = [select id, name,Account__c,Status__c,Payment_Terms__c,Activated__c,Billing_Contact__c,Billing_Entity__c 
                                             from BillProfile__c where id=:BillProfList[0].id limit 1];    
                                       
     Action_Item__c objActionItem1= new Action_Item__c();
            //objActionItem9.OwnerID = BProfObjlist[0].OwnerID;
            objActionItem1.Bill_Profile__c = BProfObjlist[0].Id;
                objActionItem1.Account__c =  BProfObjlist[0].Account__c;
                objActionItem1.Due_Date__c = System.Today() + 2;  
                //objActionItem9.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.sObjectType, 'Adhoc Request');
                //objActionItem1.Subject__c = 'Update Contract Documents';
                objActionItem1.Quote_Subject__c = 'Pricing Validation';
                //objActionItem9.Status__c =  'High';
                //objActionItem9.Priority__c = listGCD[1].value__c;
                // Hard Coded Comments , need to Put them in Herirachial Custom Settings
                objActionItem1.Comments__c = 'Payment Terms and Billing Frequency has changed, please update the contract documents (if any).';
                Insert objActionItem1; 
                
       list<Action_Item__c> al = new list<Action_Item__c> ();  
       al.add(objActionItem1); 
       
       list<Action_Item__c> al1 = new list<Action_Item__c> ();  
       al.add(objActionItem1); 
        
      Map<Id,BillProfile__c> BillProfNewMap = new Map<Id,BillProfile__c>();
        if(BillProfNewMap.isEmpty()) {
        BillProfNewMap.put(BillProfList[0].id,BillProfList[0]);
        }
        
    Map<Id,Action_Item__c> actMap = new Map<Id,Action_Item__c>();
        if(actMap.isEmpty()) {
            actMap.put(al[0].id,al[0]);
        }
    /*Map<Id,Action_Item__c> actMapOld = new Map<Id,Action_Item__c>();
        if(actMapOld.isEmpty()) {
            actMapOld.put(al1[0].id,al1[0]);
        }*/
         
        Test.startTest();
        //Util.assignOwnersToRegionalQueue(usr,objActionItem1,'Bill Profile');
        AllActionItemTriggerHandler  allAction = new AllActionItemTriggerHandler(); 
        allAction.runManagedSharing(al);        
        
        System.debug('al--------'+al);
        System.debug('actMap--------'+actMap);
        System.debug('actMap--------'+actMap);
        
        allAction.updatingcommercialimpactinpricingvalidationAI(al);
       // allAction.beforeUpdate(al,actMap,actMap);
       // allAction.afterUpdate(al,actMap,actMap);
       // allAction.updateCrossCountryAppAI(al,actMap,actMap);
        allAction.updateCompletedByAndDate(al,actMap);
        allAction.requesttomodifySplitAI(al);
            
         
        Test.stopTest();     
      } 
     }
     
    private static testMethod void doDynamicLookupSearchTest() {
       Exception ee = null;
        
        try{
        
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        
        AllActionItemTriggerHandlerMethod();                          
       
       } catch(Exception e){
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    } 
     
   /*  public static testmethod void testbeforeUpdate() {
       
        Country_Lookup__c countrynew1 = TestDataFactory.createCountry();
        Account accnew1 = TestDataFactory.createAccount('Account1', countrynew1, true); 
        Contact connew2 = TestDataFactory.createContact('Bill','Profile',accnew1,'Billprofile',countrynew1,true);
        
        List<BillProfile__c> BillPrfList = new List<BillProfile__c>();
        BillProfile__c BProfObj = new BillProfile__c();
        BProfObj.Name = 'BillProfile';
        BProfObj.Account__c = accnew1.Id;
        BProfObj.Billing_Entity__c = 'INDIA';
        BProfObj.Payment_Terms__c = '30 Days';
        BProfObj.Billing_Frequency__c = 'Hong Kong Annual Voice';
        BProfObj.Billing_Contact__c = connew2.id;
        BProfObj.Activated__c = true;
        insert BProfObj;

        Map<Id, BillProfile__c> oldMap = new Map<Id, BillProfile__c>{BProfObj.Id => BProfObj};
        Map<Id, BillProfile__c> newdMap = new Map<Id, BillProfile__c>{BProfObj.Id => BProfObj};
        
        //Start Test
        Test.startTest();
        AllActionItemTriggerHandler  allActionrHdlr = new AllActionItemTriggerHandler  ();     
        allActionrHdlr .beforeUpdate(new List<BillProfile__c>{BProfObj},newdMap,oldMap);
        Test.stopTest();    
    } */
    
    
     
}