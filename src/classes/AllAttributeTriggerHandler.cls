public with sharing class AllAttributeTriggerHandler extends BaseTriggerHandler{
    public override Boolean isDisabled(){
      if (TriggerFlags.NoAttributeTriggers) {
                return true;
            }
            return triggerDisabled;
        }

    /** Before Insert trigger handler **/
    public override void beforeInsert(List<SObject> newAttributes){
        copyCurrencyFromProductConfiguration((List<cscfga__Attribute__c>)newAttributes);
        AttributeValueIsChanged((List<cscfga__Attribute__c>)newAttributes, null);
        clearAttributeValue((List<cscfga__Attribute__c>)newAttributes);     
    }  
   

    /** Before Update trigger handler **/
    public override void beforeUpdate(List<SObject> newAttributes, Map<Id, SObject> newAttributesMap, Map<Id, SObject> oldAttributesMap){
        updateDisplayValue((List<cscfga__Attribute__c>)newAttributes, (Map<Id, cscfga__Attribute__c>)newAttributesMap, (Map<Id, cscfga__Attribute__c>)oldAttributesMap);
        clearAttributeValue((List<cscfga__Attribute__c>)newAttributes);
        LinkedChilds((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
        LinkedChildsForCage((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
        AttributeValueIsChanged((List<cscfga__Attribute__c>)newAttributes, (Map<Id, cscfga__Attribute__c>)oldAttributesMap);
    }

    /** After Update trigger handler **/
    public override void afterUpdate(List<SObject> newAttributes, Map<Id, SObject> newAttributesMap, Map<Id, SObject> oldAttributesMap){
        LinkIPCWithPorts((List<cscfga__Attribute__c>)newAttributes, (Map<Id, cscfga__Attribute__c>)oldAttributesMap);
        if(checkRecursive.AttributeAfterUpdate()){  
            copyNniNumberToProductBasket((Map<Id, cscfga__Attribute__c>)newAttributesMap, (Map<Id, cscfga__Attribute__c>)oldAttributesMap);
            runMACDUpdate((Map<Id, cscfga__Attribute__c>)newAttributesMap, (Map<Id, cscfga__Attribute__c>)oldAttributesMap);
        }
        updateColoId((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
        updateCageId((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/        
        updatePortwithMultihomeId((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
        PowerForCage((List<cscfga__Attribute__c>) newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
        mapValuesToServices(newAttributesMap, oldAttributesMap);
        setRowCountOnPC((List<cscfga__Attribute__c>)newAttributes,(Map<Id, cscfga__Attribute__c>)oldAttributesMap); /** SOQL optimised **/
    }
    
     /**
      * Output map child configuration values to services
      * @param Map<Id, SObject> newAttributesMap 
      * @param Map<Id, SObject> oldAttributesMap
      */
    public void mapValuesToServices(Map<Id, SObject> newAttributesMap, Map<Id, SObject> oldAttributesMap) {
      List<Id> attsToUpdate = new List<Id>();
      for (Id attId : newAttributesMap.keySet()) {
        cscfga__Attribute__c newAtt = (cscfga__Attribute__c) newAttributesMap.get(attId);
        cscfga__Attribute__c oldAtt = (cscfga__Attribute__c) oldAttributesMap.get(attId);
        if (newAtt.cscfga__Value__c != oldAtt.cscfga__Value__c) {
          attsToUpdate.add(attId);
        }
      }
      if (!attsToUpdate.isEmpty()) {
        CS_ConfigChangesHelper.mapConfigChangesToService(attsToUpdate);
      }
    }

    /** Update display value of lookup attributes - for VPN ports **/
    @testvisible
    private Map<String,String> AttributeNameToSObject = new Map<String,String> { 'Pop City' => 'CS_City__c',
                                                                                 'POP' => 'CS_POP__c',
                                                                                 'Port Country' => 'CS_Country__c'};
    @testvisible
    private void updateDisplayValue(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> newAttributesMap, Map<Id, cscfga__Attribute__c> oldAttributesMap){

        Map<String, List<cscfga__Attribute__c>> csAttTosObject = new Map<String, List<cscfga__Attribute__c>>();
        Map<String, List<cscfga__Attribute__c>> csAttValueToAtt = new Map<String, List<cscfga__Attribute__c>>();
        
        for(cscfga__Attribute__c att :newAttributes){
            if(AttributeNameToSObject.containsKey(att.Name) 
                && att.cscfga__Value__c != null 
                && att.cscfga__Value__c != '' 
                && att.Attribute_Type__c == 'Lookup'
                && att.cscfga__Value__c != oldAttributesMap.get(att.Id).cscfga__Value__c){
                
                if(csAttTosObject.containsKey(AttributeNameToSObject.get(att.cscfga__Value__c))){
                    csAttTosObject.get(AttributeNameToSObject.get(att.Name)).add(att);
                } else{
                    List<cscfga__Attribute__c> list1 = new List<cscfga__Attribute__c>();
                    list1.add(att);
                    csAttTosObject.put(AttributeNameToSObject.get(att.Name), list1);
                }

                if(csAttValueToAtt.containsKey(att.cscfga__Value__c)){
                    csAttValueToAtt.get(att.cscfga__Value__c).add(att);
                } else{
                    List<cscfga__Attribute__c> list1 = new List<cscfga__Attribute__c>();
                    list1.add(att);
                    csAttValueToAtt.put(att.cscfga__Value__c, list1);
                }
            } 
        }

        system.debug('>>>>>>> Map csAttTosObject beforeUpdate1: '+csAttTosObject);
        for(String sObjectName : csAttTosObject.keySet()){
            List<Id> idList = new List<Id>();
            for(cscfga__Attribute__c att : csAttTosObject.get(sObjectName)){
                idList.add(att.cscfga__Value__c);
            }

            List<SObject> result  = Database.query(getQuery(sObjectName, idList));
            for(SObject obj : result){
                String sObjectId = (String)obj.get('Id');

                for(cscfga__Attribute__c attTo : csAttValueToAtt.get(sObjectId)){
                    attTo.cscfga__Display_Value__c = (String)obj.get('Name');
                }
            }
        }
        system.debug('>>>>>>> Map csAttTosObject beforeUpdate2: '+csAttTosObject);  
    }

    @testvisible
    private String getQuery(String sObjectName, List<Id> idList){
        String query ='SELECT Id, Name FROM '+sObjectName+' WHERE Id IN :idList';
        return query;
    }
    /** Update display value of lookup attributes - for VPN ports -------END------- **/


    /** Flag a service when it has attributes which have been changed as part of a MACD update **/
    @testvisible
    private void runMACDUpdate(Map<Id, cscfga__Attribute__c> newAttributeMap, Map<Id, cscfga__Attribute__c> oldAttributeMap) {
        if(CheckRecursive.runMACDupdate()){
            Set<Id> prodConfigIdsMACD = new Set<Id>();
        
            for(cscfga__Attribute__c attNew : newAttributeMap.values()){
                if(attNew.Attribute_Screen_Config__c > 0 
                    && attNew.Attribute_Type__c != 'Related Product' 
                    && (attNew.Name != 'Customer_Required_Date__c' 
                    && attNew.Name != 'ScreenFlow' 
                    && attNew.Name != 'ScreenFlowName' 
                    && attNew.Name != 'Product_config_ID__c' 
                    && attNew.Name != 'BasketId')){
                        if((attNew.cscfga__Value__c != oldAttributeMap.get(attNew.Id).cscfga__Value__c)){
                            prodConfigIdsMACD.add(attNew.cscfga__Product_Configuration__c);
                        }              
                }
            }
            
            if(!prodConfigIdsMACD.IsEmpty()){
                List<csord__Service__c> serviceupdate = [Select Id, Updated__c, Ordertype__c from csord__Service__c where csordtelcoa__Product_Configuration__c IN :prodConfigIdsMACD and Ordertype__c != 'PROVIDE'];
                if(!serviceupdate.IsEmpty()){
                    for(csord__Service__c supdate :serviceupdate){
                        supdate.Updated__c = true;
                    }
                    update serviceupdate;
                }
            }
        }
    }

    /**
     * Set Currency ISO Code & Exchange Rate From Product Basket.
     * 
     * @param newAttributes the Attributes to update
     * Refactored the logic to update value of the Currency ISO Code & Exchange Rate From Product Basket. //Sapan
     * Removed SOQL query and added formula fields on the Attribute object to fetch the value from Product Basket and assign it to the Attributes.
     */ 
    @testvisible
    private void copyCurrencyFromProductConfiguration(List<cscfga__Attribute__c> newAttributes) {

        for(cscfga__Attribute__c att: newAttributes) {
            att.CurrencyIsoCode = att.Product_Basket_CurrencyISOCode__c;
            if(att.Name == 'Product Basket Currency'){
                att.cscfga__Value__c = att.Product_Basket_CurrencyISOCode__c;
                att.cscfga__Display_Value__c = att.Product_Basket_CurrencyISOCode__c;
            }
            else if(att.Name == 'Product Basket Currency Ratio'){
                att.cscfga__Value__c = att.Product_Basket_Exchange_Rate__c;
                att.cscfga__Display_Value__c = att.Product_Basket_Exchange_Rate__c;
            }
        }
    }

    /**
     * AttributeValueIsChanged.
     * 
     * @param newAttributes the Attributes to update
     */ 
    @testvisible
    public static void AttributeValueIsChanged(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributeMap){
        try{
            for(cscfga__Attribute__c att :newAttributes){
                if(!CheckRecursive.AttributeValueIsChangedDontExecute 
                    && att.Is_Required_For_Pricing__c == 'Yes' 
                    && (att.AttDef_ScreenSectionName__c != 'Price' || att.AttDef_ScreenSectionName__c != 'Pricing' || att.AttDef_ScreenSectionName__c != 'Pricing Summary')){

                    if(att.Id == null){
                        att.Attribute_Value_IsChanged__c = true;
                    }               
                    else if(att.Id != null 
                        && oldAttributeMap.get(att.Id).cscfga__Value__c != null 
                        && oldAttributeMap.get(att.Id).cscfga__Value__c != att.cscfga__Value__c){
                            att.Attribute_Value_IsChanged__c = true;
                    }
                }
            }
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:AttributeValueIsChanged';
            ErrorHandlerException.objectList=newAttributes;
            ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; AttributeValueIsChanged" method update failure - ' +e);
          } 
    }

    @testvisible
    private void copyNniNumberToProductBasket(Map<Id, cscfga__Attribute__c> newAttributeMap, Map<Id, cscfga__Attribute__c> oldAttributeMap){
        /** Collect the nni number attributes **/
        if(CheckRecursive.runCopyNniNumberToProductBasket()){
            List<Id> attributeIds = new List<Id>();
            for(cscfga__Attribute__c newAttribute: newAttributeMap.values()){
                if(newAttribute.Name == 'NNI Number Input' 
                    && newAttribute.cscfga__Value__c != oldAttributeMap.get(newAttribute.Id).cscfga__Value__c){ 
                        attributeIds.add(newAttribute.Id);
                }
            }
        
            /** Get the product baskets **/
            if(attributeIds.Size() > 0){          
                List<cscfga__Product_Basket__c> toUpdate = new List<cscfga__Product_Basket__c>();
                for(cscfga__Attribute__c newAttribute :[Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c from cscfga__Attribute__c where Id IN :attributeIds]){
                    toUpdate.add(new cscfga__Product_Basket__c(Id = newAttribute.cscfga__Product_Configuration__r.cscfga__Product_Basket__c, NNI_Number__c = Decimal.valueOf(newAttribute.cscfga__Value__c)));        
                }
                /** Update baskets NNI number fields **/
                if(toUpdate.Size() > 0){
                    update toUpdate;
                }
            }
        }
    }
    
    /** Matija CS 13.02.2017 **/
    @TestVisible
    private void clearAttributeValue(List<cscfga__Attribute__c> newAttributes){
        for(cscfga__Attribute__c item :newAttributes){
            if(item.cscfga__is_active__c == false){
                item.cscfga__Value__c = null;
                item.cscfga__Display_Value__c = null;
            } 
        }
    }

    @testvisible
    private static void setRowCountOnPC(List<cscfga__Attribute__c> newAttributes,Map<Id, cscfga__Attribute__c> oldAttributeMap){
        try{
            Map<Id, Decimal> attributeToPcMap = new Map<Id, Decimal>();
            List<cscfga__Attribute__c>RelatedAttrList=new List<cscfga__Attribute__c>();
            
            for(cscfga__Attribute__c attr:newAttributes){
                if(attr.cscfga__Value__c!=null 
                && attr.cscfga__Value__c!=oldAttributeMap.get(attr.id).cscfga__Value__c
                && attr.Attribute_Type__c=='Related Product'){
                    RelatedAttrList.add(attr);
                }
            }
            if(RelatedAttrList.size()>0){           
            
            for(cscfga__Attribute__c atr :[Select Id, cscfga__Value__c, cscfga__Attribute_Definition__c, cscfga__Attribute_Definition__r.cscfga__Type__c, cscfga__Attribute_Definition__r.cscfga__Row__c From cscfga__Attribute__c Where ID IN:newAttributes AND cscfga__Attribute_Definition__r.cscfga__Type__c='Related Product' AND cscfga__Attribute_Definition__r.cscfga__Row__c > 0]){
                
                    List<Id> tempList = new List<Id>();                
                    if(atr.cscfga__Value__c != null){
                        try{tempList = atr.cscfga__Value__c.split(',');}catch(Exception e){}
                        for(Id temp :tempList){
                            if(temp != null && !attributeToPcMap.containsKey(temp)){
                                attributeToPcMap.put(temp, atr.cscfga__Attribute_Definition__r.cscfga__Row__c);
                            }  
                        }
                    }                
                } 
            
                if(attributeToPcMap.Size() > 0){
                    List<cscfga__Product_Configuration__c> updatePCList = [Select Id, Row_Sequence__c from cscfga__Product_Configuration__c where ID IN :attributeToPcMap.KeySet()];
                    for(cscfga__Product_Configuration__c pc :updatePCList){
                        pc.Row_Sequence__c = attributeToPcMap.get(pc.Id);
                    }
                    if(updatePCList.Size() > 0){update updatePCList;}
                }
            }
            
        } catch(exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:setRowCountOnPC';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; setRowCountOnPC" method update failure - ' +e);
          }
    }
   
    /**
     * Desc: Updating the ids of multihome on ports
     * Date: 11/5/2017
     * Name: Sowmya
     */
    @testvisible
    private static void updatePortwithMultihomeId(List<cscfga__Attribute__c> newAttributes,Map<Id,cscfga__Attribute__c>oldAttributesMap){
        try{
            List<cscfga__Attribute__c> attId = new List<cscfga__Attribute__c>();
            List<cscfga__Attribute__c> attPortsAdded = new List<cscfga__Attribute__c>();
            Map<Id, Id> PortandMultihome = new Map<Id, Id>();
            Set<Id> alreadyExecutedIds = new Set<Id>();
            for(cscfga__Attribute__c att :newAttributes){
                if(att.name == 'Ports Added' 
                    && att.cscfga__Value__c != null 
                    && CheckRecursive.canExecuteUpdate(alreadyExecutedIds) 
                    && Label.Internet_Product_Codes.contains(att.Product_Code__c)){
                        attId.add(att);
                }
            }
            if(attId.Size() > 0){
                attPortsAdded = [Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c from cscfga__Attribute__c where Id IN :attId and Name ='Ports Added'];
            }
        
            if(attPortsAdded.size()>0){
                for(cscfga__Attribute__c a : attPortsAdded){
                    if(a.cscfga__Value__c != null){
                        List<String> str = a.cscfga__Value__c.split(',');
                        for(String s : str){
                            PortandMultihome.put(s,a.cscfga__Product_Configuration__c);
                        }
                    }
                }
            }
            List<cscfga__Product_Configuration__c> pcids = new List<cscfga__Product_Configuration__c>();
            if(PortandMultihome.keyset().size() > 0){
                pcids = [Select id,name,Parent_Configuration_For_Sequencing__c from cscfga__Product_Configuration__c where id in: PortandMultihome.keyset()];
                for(cscfga__Product_Configuration__c pc : pcids){
                    pc.Parent_Configuration_For_Sequencing__c = PortandMultihome.get(pc.id);
                }
            }
            if(pcids.Size() > 0){
                update(pcids);
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:updatePortwithMultihomeId';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; updatePortwithMultihomeId" method update failure - ' +e);
          }  
    }

    /**
     * Desc: Updating the Id of CAGE in Rack
     * Date: 20/6/2017
     * Name: Sowmya
     */
    @testvisible
    private static void updateCageId(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributeMap){
        try{
            System.debug('Control here');
            List<cscfga__Attribute__c> attId = new List<cscfga__Attribute__c>();
            Map<Id, Id> RackandCage = new Map<Id, Id>();
            for(cscfga__Attribute__c att :newAttributes){
                if(att.Name == 'Select Cage Master' 
                    && att.cscfga__Value__c != oldAttributeMap.get(att.Id).cscfga__Value__c){
                        attId.add(att);
                }
            }
            
            if(attId.Size() > 0){
                List<cscfga__Attribute__c> attPortsAdded = [Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c from cscfga__Attribute__c where Id IN :attId and Name = 'Select Cage Master'];
                System.debug('Att list =====> '+attPortsAdded);
                if(attPortsAdded.Size() > 0){
                    for(cscfga__Attribute__c a :attPortsAdded){
                        if(a.cscfga__Value__c != null){
                            RackandCage.put(a.cscfga__Product_Configuration__c,a.cscfga__Value__c);
                        }
                    }
                }           
                
                List<cscfga__Product_Configuration__c> pcIds = [Select Id, Name, Parent_Configuration_For_Sequencing__c from cscfga__Product_Configuration__c where Id IN :RackandCage.KeySet()];
                for(cscfga__Product_Configuration__c pc :pcIds){
                    pc.Parent_Configuration_For_Sequencing__c = RackandCage.get(pc.Id);
                }
                if(pcIds.Size() > 0){
                    update(pcIds);
                }
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:updateCageId';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; updateCageId" method update failure - ' +e);
          }
    }
    
    /**
     * Date: 29-June-2017
     * Developer: Harsha
     * CR: CR50
     * Description: The below method will populate all the child pc ids in colo master
     * Is called in after update
     */
    @testvisible     
    private static void LinkedChilds(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributesMap){
        try{
            Map<Id, Id> ChildAttMap = new Map<Id, Id>();
            Map<Id, cscfga__Attribute__c> ParentAttMap = new Map<Id, cscfga__Attribute__c>();
            List<cscfga__Attribute__c> updateList  = new List<cscfga__Attribute__c>();
            //List <cscfga__Attribute__c> ParentAttList = new List <cscfga__Attribute__c>();

            for(cscfga__Attribute__c att :newAttributes){
                if(att.Name == 'Select Colo Master' 
                    && oldAttributesMap.get(att.Id).cscfga__Value__c != att.cscfga__Value__c){
                        ChildAttMap.put(att.cscfga__Value__c, att.cscfga__Product_Configuration__c);
                }
            }

            if(ChildAttMap.IsEmpty() == false){
                //ParentAttList = [Select Id, Name, cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :ChildAttMap.KeySet() and Name = 'LinkedChilds'];
        for(cscfga__Attribute__c att :[Select Id, Name, cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :ChildAttMap.KeySet() and Name = 'LinkedChilds']){
                ParentAttMap.put(att.cscfga__Product_Configuration__c, att);
            }
      }
            
            for(Id parentkey :ParentAttMap.KeySet()){
                for(Id childkey :ChildAttMap.KeySet()){
                    if(parentkey == childkey){
                        string s1 = '' + ParentAttMap.get(parentkey).cscfga__Value__c;
                        string s2 = '' + ChildAttMap.get(childkey);
                        if(!s1.contains(s2)){
                            if(ParentAttMap.get(parentkey).cscfga__Value__c == '' || ParentAttMap.get(parentkey).cscfga__Value__c == null){
                                ParentAttMap.get(parentkey).cscfga__Value__c = ChildAttMap.get(childkey);
                            }
                            else if(ParentAttMap.get(parentkey).cscfga__Value__c != '' || ParentAttMap.get(parentkey).cscfga__Value__c != null){
                                ParentAttMap.get(parentkey).cscfga__Value__c = ParentAttMap.get(parentkey).cscfga__Value__c +','+ ChildAttMap.get(childkey);
                            }
                        }
                    }
                }
            }

            if(ParentAttMap.KeySet().Size() > 0){
                List<cscfga__Product_Configuration__c> pc = [Select Id, Name, Added_Racks__c from cscfga__Product_Configuration__c where Id =:ParentAttMap.KeySet()];
                for(cscfga__Product_Configuration__c p :pc){
                    if(ParentAttMap.get(p.Id).cscfga__Value__c != null || ParentAttMap.get(p.Id).cscfga__Value__c != ''){
                        p.Added_Racks__c = ParentAttMap.get(p.Id).cscfga__Value__c;
                    }
                }
            
                if(pc.Size() > 0){
                    update pc;
                }
            }

            if(ParentAttMap.size()>0){
                updateList = ParentAttMap.values();
                update(updateList);
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:LinkedChilds';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; LinkedChilds" method update failure - ' +e);
          }
    }

    /**
     * Date: 30-June-2017
     * Developer: Harsha
     * CR: CR50
     * Description: The below method will populate all the child pc ids in cage master
     * Is called in after update
     */
    @testvisible
    private static void LinkedChildsForCage(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributesMap){
        try{
            Map<Id, Id> ChildAttMap = new Map<Id, Id>();
            Map<Id, cscfga__Attribute__c> ParentAttMap = new Map<Id, cscfga__Attribute__c>();
            List<cscfga__Attribute__c> updateList  = new List<cscfga__Attribute__c>();
            //List <cscfga__Attribute__c> ParentAttList = new List <cscfga__Attribute__c>();
            for(cscfga__Attribute__c att :newAttributes){
                if(att.Name == 'Select Cage Master' 
                    && oldAttributesMap.get(att.Id).cscfga__Value__c != att.cscfga__Value__c){
                        ChildAttMap.put(att.cscfga__Value__c, att.cscfga__Product_Configuration__c);
                }
            }

            if(ChildAttMap.IsEmpty() == false){
                //ParentAttList = [Select Id, Name, cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :ChildAttMap.KeySet() and Name = 'LinkedChilds'];
            for(cscfga__Attribute__c att :[Select Id, Name, cscfga__Product_Configuration__c, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :ChildAttMap.KeySet() and Name = 'LinkedChilds']){
                ParentAttMap.put(att.cscfga__Product_Configuration__c, att);
            }
      }
            
            for(Id parentkey :ParentAttMap.KeySet()){
                for(Id childkey :ChildAttMap.KeySet()){
                    if(parentkey == childkey){
                        string s1 = '' + ParentAttMap.get(parentkey).cscfga__Value__c;
                        string s2 = '' + ChildAttMap.get(childkey);
                        if(!s1.contains(s2)){
                            if(ParentAttMap.get(parentkey).cscfga__Value__c == '' || ParentAttMap.get(parentkey).cscfga__Value__c == null){
                                ParentAttMap.get(parentkey).cscfga__Value__c = ChildAttMap.get(childkey);
                            }
                            else if(ParentAttMap.get(parentkey).cscfga__Value__c != '' || ParentAttMap.get(parentkey).cscfga__Value__c != null){
                                ParentAttMap.get(parentkey).cscfga__Value__c = ParentAttMap.get(parentkey).cscfga__Value__c +','+ ChildAttMap.get(childkey);
                            }
                        }
                    }
                }
            }

            if(ParentAttMap.KeySet().Size() > 0){
                List<cscfga__Product_Configuration__c> pc = [Select Id, Name, Added_Racks__c from cscfga__Product_Configuration__c where Id =:ParentAttMap.KeySet()];
                for(cscfga__Product_Configuration__c p :pc){
                    if(ParentAttMap.get(p.Id).cscfga__Value__c != null || ParentAttMap.get(p.Id).cscfga__Value__c != ''){
                        p.Added_Racks__c = ParentAttMap.get(p.Id).cscfga__Value__c;
                    }
                }
                if(pc.Size() > 0){
                    update pc;
                }
            }

            if(ParentAttMap.Size() > 0){
                updateList = ParentAttMap.values();
                update(updateList);
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:LinkedChildsForCage';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; LinkedChildsForCage" method update failure - ' +e);
          }
    }

    public void LinkIPCWithPorts(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributesMap){
        Map<Id, String> NewMap = new Map<Id, String>();
        Map<Id, String> OldMap = new Map<Id, String>();
        Map<Id, String> PCMap = new Map<Id, String>();

        for(cscfga__Attribute__c att :newAttributes){
            if(att.Name == 'Ports Added'){
                System.debug('att.cscfga__Value__c'+att.cscfga__Value__c);
                System.debug('oldAttributesMap.get(att.Id).cscfga__Value__c'+oldAttributesMap.get(att.Id).cscfga__Value__c);
            }
            if(att.Name == 'Ports Added' && att.cscfga__Value__c != oldAttributesMap.get(att.Id).cscfga__Value__c){            
                NewMap.put(att.cscfga__Product_Configuration__c, att.cscfga__Value__c);
                OldMap.put(att.cscfga__Product_Configuration__c, oldAttributesMap.get(att.Id).cscfga__Value__c);
            }
        }
        if(NewMap.Size() > 0){      
            ProductConfigurationSequence.updateIPCtoSingleHome(NewMap,OldMap);
        }
    }
        
    /**
     * Desc: Updating the Id of COLO in Cage, Rack, Misc and Cross Connect
     * Date: 20/6/2017
     * Name: Sowmya
     */
    @testvisible
    private static void updateColoId(List<cscfga__Attribute__c> newAttributes, Map<Id, cscfga__Attribute__c> oldAttributesMap){
        try{
            System.debug('Control here');
            List<cscfga__Attribute__c> attId = new List<cscfga__Attribute__c>();
            Map<Id, Id> ChildandColo = new Map<Id, Id>();
            for(cscfga__Attribute__c att :newAttributes){
                if(att.Name == 'Select Colo Master' && att.cscfga__Value__c != oldAttributesMap.get(att.Id).cscfga__Value__c) {
                  attId.add(att);
                }
            }
            if(attId.size()>0){
                List<cscfga__Attribute__c> attPortsAdded = [Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__c from cscfga__Attribute__c where Id IN :attId and Name = 'Select Colo Master'];
                System.debug('Att list =====> '+attPortsAdded);
                if(attPortsAdded.Size() > 0){
                    for(cscfga__Attribute__c a :attPortsAdded){
                        if(a.cscfga__Value__c != null){
                            ChildandColo.put(a.cscfga__Product_Configuration__c, a.cscfga__Value__c);
                        }
                    }
                }
                List<cscfga__Product_Configuration__c> pcIds = [Select Id, Name, Parent_Configuration_For_Sequencing__c from cscfga__Product_Configuration__c where Id IN :ChildandColo.KeySet()];          
                for(cscfga__Product_Configuration__c pc :pcIds){
                    pc.Parent_Configuration_For_Sequencing__c = ChildandColo.get(pc.Id);
                }
                if(pcIds.Size() > 0){
                    update(pcIds);
                }
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:updateColoId';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
            System.debug('Error: "AllAttributeTriggerHandler; updateColoId" method update failure - ' +e);
          }
    }

    @testvisible
    private static void PowerForCage(List<cscfga__Attribute__c> newAttributes,Map<Id, cscfga__Attribute__c> oldAttributeMap){
        List<cscfga__Attribute__c> LinkChildList = new List<cscfga__Attribute__c>();
        List<String> RackIdList = new List<String>();
        //List<cscfga__Attribute__c> RackPowerList = new List<cscfga__Attribute__c>();
        List<Decimal> RackPowerListWithoutkVA = new List<Decimal>();
        List<cscfga__Attribute__c> Attributes = new List<cscfga__Attribute__c>();
        List<cscfga__Attribute__c> powerforrack = new List<cscfga__Attribute__c>();
        List<cscfga__Attribute__c> CageUtilityModel = new List<cscfga__Attribute__c>();
        Boolean flagColo = false;
        String RackUtilityModelForCage;
        try{
            Id BasketId;
            Id PcId;
            Decimal FinalPower = 0.0;
            for(cscfga__Attribute__c ar :newAttributes){
                if(ar.Name == 'Utility_Model' 
                    && ar.cscfga__Value__c != oldAttributeMap.get(ar.Id).cscfga__Value__c){
                        flagColo = true;
                }
            }
            if(flagColo == true){
                Attributes = [Select Id, Name, cscfga__Value__c, cscfga__Product_Configuration__r.Name, cscfga__Product_Configuration__r.cscfga__Product_Basket__c, cscfga__Product_Configuration__c from cscfga__Attribute__c where Id IN :newAttributes];
                for(cscfga__Attribute__c att :Attributes){
                    if(att.cscfga__Product_Configuration__r.Name == 'COLO-CAGE-RACK'){
                        system.debug('+++++Enter IF++++');
                        BasketId = att.cscfga__Product_Configuration__r.cscfga__Product_Basket__c;
                        PcId = att.cscfga__Product_Configuration__c;
                    }
                    if((att.Name == 'Utility_Model') && (att.cscfga__Product_Configuration__r.Name == 'COLO-CAGE-RACK')){
                        RackUtilityModelForCage = att.cscfga__Value__c;
                    }
                }
            }
            system.debug('+++++BasketId++++'+BasketId);
            system.debug('+++++PcId++++'+PcId);
            system.debug('+++++RackUtilityModelForCage++++'+RackUtilityModelForCage);
            if(BasketId != null){
                system.debug('+++++Enter IF 1++++');
                LinkChildList = [Select Id, Name, cscfga__Value__c, cscfga__Display_Value__c, cscfga__Product_Configuration__c from cscfga__Attribute__c where cscfga__Product_Configuration__r.cscfga__Product_Basket__c =: BasketId AND cscfga__Product_Configuration__r.Name = 'Cage/Private Room' AND cscfga__Attribute_Definition__r.Name = 'LinkedChilds'];
            }
            Map<cscfga__Attribute__c, Id> cageMap = new Map<cscfga__Attribute__c, Id>();
            for(cscfga__Attribute__c att :LinkChildList){
                cageMap.put(att,att.cscfga__Product_Configuration__c);
            }
            system.debug('+++++LinkChildList++++'+LinkChildList);
            for(cscfga__Attribute__c lc : LinkChildList){
                if(lc.cscfga__Value__c.contains(PcId)){
                    system.debug('+++++Enter IF 2++++');
                    RackIdList = lc.cscfga__Value__c.split(',');
                }
            }
            
            system.debug('+++++RackIdList++++'+RackIdList);         
            if(RackIdList.Size() > 0){
                //RackPowerList = [Select Id, Name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :RackIdList AND Name =:'Power Pack Final'];
            for(cscfga__Attribute__c  rp :[Select Id, Name, cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c IN :RackIdList AND Name =:'Power Pack Final']){
                string temp = rp.cscfga__Value__c.remove('kVA');
                temp = temp.trim();
                system.debug('++++temp++++'+temp);
                Decimal power = Decimal.valueof(temp);
                RackPowerListWithoutkVA.add(power);
            }
      }
            //system.debug('+++++RackPowerList++++'+RackPowerList);
            
            //system.debug('+++++RackPowerListWithoutkVA++++'+RackPowerListWithoutkVA);
            for(Decimal rpw :RackPowerListWithoutkVA){
                FinalPower = FinalPower+rpw;
            }
            system.debug('+++++FinalPower++++'+FinalPower);
            List<Id> cageId = new List<Id>();
            for(cscfga__Attribute__c  a2 :cageMap.keySet()){
                if(a2.cscfga__Value__c.contains(PcId)){
                    cageId.add(cageMap.get(a2));
                }
            }

            if(cageId.Size() > 0){    
                powerforrack = [Select Id, Name, cscfga__Value__c, cscfga__Display_Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c=: cageId and name='Power_for_cage_private_room'];
                CageUtilityModel = [Select Id, Name, cscfga__Value__c, cscfga__Display_Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c=: cageId and name='Utility_Model'];
            }
            for(cscfga__Attribute__c lc1 :powerforrack){
                system.debug('++++lc1++++'+lc1);
                system.debug('++++PcId++++'+PcId);
                system.debug('++++lc1.cscfga__Value__c.contains(PcId)++++'+lc1.cscfga__Value__c.contains(PcId));
                system.debug('++++lc1.cscfga__Display_Value__c++++'+lc1.cscfga__Display_Value__c);
                if(lc1.Name=='Power_for_cage_private_room'){
                    system.debug('+++++Enter IF 3++++');
                    lc1.cscfga__Value__c = String.valueof(FinalPower)+'kVA';
                    lc1.cscfga__Display_Value__c = String.valueof(FinalPower)+'kVA';
                }
            }
            system.debug('+++++CageUtilityModel++++'+CageUtilityModel);
            for(cscfga__Attribute__c ut :CageUtilityModel){    
                if((ut.cscfga__Value__c == '') || (ut.cscfga__Value__c == null)){
                    system.debug('+++++Enter IF 5++++');
                    ut.cscfga__Value__c = RackUtilityModelForCage;
                }
            }
            update(CageUtilityModel);

            if(FinalPower != 0){
                system.debug('+++++Enter IF 4++++');
                update(powerforrack);
            }
        } catch(Exception e){
                ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandler:PowerForCage';
                ErrorHandlerException.objectList=newAttributes;
                ErrorHandlerException.sendException(e);
                System.debug('Error: "AllAttributeTriggerHandler; PowerForCage" method update failure - ' +e);
          }
    }
}
