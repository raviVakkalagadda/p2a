@isTest
private class AllAttributeTriggerHandlerTest
{

private static void disableAll(Id userId) 
 {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
              
              /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
              
    
    public static List<CS_Provider__c> providers;     
    public static testmethod void AllAttributeTriggerHandlerunittest() 
    { 
        id userId=UserInfo.getUserId();
                  //disableAll(userId);
        
        P2A_TestFactoryCls.sampletestdata();
            createProviders(2);
        
        Map<Id, cscfga__Attribute__c> newAttributeMap = new Map<Id, cscfga__Attribute__c>();
        Map<Id, cscfga__Attribute__c> oldAttributeMap = new Map<Id, cscfga__Attribute__c>();
        Map<Id, cscfga__Attribute__c> newAttributeMap1 = new Map<Id, cscfga__Attribute__c>();
        Map<Id, cscfga__Attribute__c> oldAttributeMap1 = new Map<Id, cscfga__Attribute__c>();
        Map<Id, cscfga__Attribute__c> newAttributeMap2 = new Map<Id, cscfga__Attribute__c>();
        Map<Id, cscfga__Attribute__c> oldAttributeMap2 = new Map<Id, cscfga__Attribute__c>();          
                             
        Test.startTest();
                                                                               
        List<User> usrlist=P2A_TestFactoryCls.get_Users(1);
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
        List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        
        List<cscfga__Product_Configuration__c> proconfigs =  P2A_TestFactoryCls.getProductonfig(8,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
        
           proconfigs[1].name = 'COLO-CAGE-RACK';
           update proconfigs;
                             //inserting product configuration
        List<cscfga__Product_Configuration__c> proconfigs1= new List<cscfga__Product_Configuration__c>();
            
                                               cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
                pb.cscfga__Product_basket__c =prodbasktlist[0].id ;
                pb.cscfga__Product_Definition__c = ProductDeflist[0].id ;
                pb.name ='IPL';
                pb.cscfga__contract_term_period__c = 12;
                pb.Is_Offnet__c = 'yes';
                pb.cscfga_Offer_Price_MRC__c = 200;
                pb.cscfga_Offer_Price_NRC__c = 300;
                pb.cscfga__Product_Bundle__c =Pbundle[0].id;
                pb.Rate_Card_NRC__c = 200;
                pb.Rate_Card_RC__c = 300;
                pb.cscfga__total_contract_value__c = 1000;
                pb.cscfga__Contract_Term__c = 12;
               pb.CurrencyIsoCode = 'USD';
                pb.cscfga_Offer_Price_MRC__c = 100;
                pb.cscfga_Offer_Price_NRC__c = 100;
                pb.Child_COst__c = 100;
                pb.Cost_NRC__c = 100;
                pb.Cost_MRC__c = 100;
                pb.Product_Name__c = 'test product';
                pb.Added_Ports__c = null ;
                Pb.cscfga__Product_Family__c = 'Point to Point';
                //Pb.csordtelcoa__Replaced_Product_Configuration__c = Pb.csordtelcoa__Replaced_Product_Configuration__c;
                                               Pb.Product_Code__c='IPC';
                                                          proconfigs1.add(pb);
                insert proconfigs1;
                system.assert(proconfigs1!=null);
        List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Attribute_Definition__c> AttDef = P2A_TestFactoryCls.getAttributesdef(2, proconfigs, ProductDeflist, ConScr, ScrSec);
        AttDef[0].cscfga__Type__c = 'Lookup' ;
        //AttDef[0].Is_required_for_pricing__c = 'Yes' ;
        upsert attdef;
                             
                             
        List<cscfga__Attribute_Definition__c> AttDef1 = P2A_TestFactoryCls.getAttributesdef(2, proconfigs, ProductDeflist, ConScr, ScrSec);
        AttDef1[0].cscfga__Type__c = 'Related Product' ;        
        upsert AttDef1; 
        
        //List<cscfga__Attribute__c> Attrblistold = P2A_TestFactoryCls.getAttributes(1, proconfigs, AttDef);
        List<cscfga__Attribute__c> UpdateAllAttrblist=new List<cscfga__Attribute__c>();
                             
        List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(10, proconfigs, AttDef);
        attrblist[0].Name = 'Pop City';
        attrblist[1].Name = 'Select Cage Master';   
        attrblist[1].cscfga__Value__c ='Select Cage Master'; 
        attrblist[2].Name = 'LinkedChilds';
        attrblist[3].Name = 'Product Basket Currency';
        attrblist[4].Name = 'Product Basket Currency Ratio';
        attrblist[5].Name = 'Ports Added';
        attrblist[6].Name = 'Utility_Model';
        attrblist[6].cscfga__is_active__c=false;
        //attrblist[6].cscfga__Product_Configuration__c = proconfigs[1].id;
        attrblist[7].Name = 'NNI Number Input';     
        attrblist[8].cscfga__Value__c = 'Test';
        attrblist[9].Name = 'Select Colo Master';
        attrblist[9].cscfga__Value__c = 'Test1';
        update attrblist;
                             //UpdateAllAttrblist.add(attrblist]);        
        
        cscfga__Attribute__c att = new cscfga__Attribute__c();
        att.id=attrblist[6].id;
        att.name='Ports Added';
        
        cscfga__Attribute__c att1 = new cscfga__Attribute__c();
        att1.id=attrblist[9].id;
        att1.cscfga__Value__c='Ports Added';
        
        cscfga__Attribute__c att2 = new cscfga__Attribute__c();
        att2.id=attrblist[1].id;
        att2.cscfga__Value__c='Ports Added';
        
        cscfga__Attribute__c att3 = new cscfga__Attribute__c();
        att3.id=attrblist[7].id;
        att3.cscfga__Value__c='Ports';
        
        cscfga__Attribute__c att4 = new cscfga__Attribute__c();
        att4.id=attrblist[5].id;
        att4.cscfga__Value__c='Ports';
        
        
        Map<Id, cscfga__Attribute__c> newAttributeMap3 = new Map<Id, cscfga__Attribute__c>();
        newAttributeMap3.put(att.id,att);       
        Map<Id, cscfga__Attribute__c> oldAttributeMap4 = new Map<Id, cscfga__Attribute__c>();
        oldAttributeMap4.put(att1.id,att1);
        Map<Id, cscfga__Attribute__c> oldAttributeMap5 = new Map<Id, cscfga__Attribute__c>();
        oldAttributeMap5.put(att2.id,att2);
        Map<Id, cscfga__Attribute__c> oldAttributeMap6 = new Map<Id, cscfga__Attribute__c>();
        oldAttributeMap6.put(att3.id,att3);
        Map<Id, cscfga__Attribute__c> oldAttributeMap7 = new Map<Id, cscfga__Attribute__c>();
        oldAttributeMap7.put(att4.id,att4);
        
        cscfga__Attribute__c Atts = new cscfga__Attribute__c(name = 'LinkedChilds',cscfga__Product_Configuration__c=proconfigs[1].id,
                        cscfga__Value__c = 'Test',cscfga__Attribute_Definition__c=AttDef1[0].id);
         insert Atts;          
            
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        
        List<csord__Order_Request__c> OrdReqList1 = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList1);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList1,SUBList);
        servlist[0].Updated__c = true ; 
        servlist[0].csordtelcoa__Product_Configuration__c = proconfigs[0].id;
        update servlist ; 
        system.assert(servlist!=null);
        List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists = P2A_TestFactoryCls.getOrchestrationProcess(1);
        List<CSPOFA__Orchestration_Process__c> orchprocesslist = P2A_TestFactoryCls.getOrchestrationProcesss(1,ProcessTemplatesLists);
        List<CSPOFA__Orchestration_Step__c> orchprocessteplist = P2A_TestFactoryCls.getOrchestrationStep(1,orchprocesslist);
        List<CSPOFA__Orchestration_Step_Template__c> OrchStepTemplate =  P2A_TestFactoryCls.getOOrchestration_Step_Template(1,ProcessTemplatesLists);
        
        List<Id> Ids = new List<Id>();
        
        for(cscfga__Attribute__c attr :Attrblist){
            Ids.add(attr.Id);   
         }
             
        newAttributeMap.put(Attrblist[0].id,Attrblist[0]);
        oldAttributeMap.put(Attrblist[0].id,Attrblist[0]);
        
        /*newAttributeMap1.put(Attrblist8[0].id,Attrblist[0]);
        oldAttributeMap1.put(Attrblist5[0].id,Attrblist[0]);
        newAttributeMap2.put(Attrblist9[0].id,Attrblist[0]);*/
       
        newAttributeMap1.put(attrblist[1].id,attrblist[1]);
        oldAttributeMap1.put(attrblist[1].id,attrblist[1]);
        
        newAttributeMap2.put(attrblist[7].id,attrblist[7]);
        oldAttributeMap2.put(attrblist[1].id,attrblist[1]);
                             //enableAll(userid);
        
        Exception ee = null;
                             
        try{
            //disableAll(UserInfo.getUserId());
            
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());            
             
            AllAttributeTriggerHandler Handler = new AllAttributeTriggerHandler();
            Handler.runMACDUpdate(newAttributeMap1,oldAttributeMap1);
            //AllAttributeTriggerHandler.updateaddedRack(Attrblist1);
            //AllAttributeTriggerHandler.updateaddedRack(Attrblist2);
            AllAttributeTriggerHandler.setRowCountOnPC(attrblist,oldAttributeMap1);
            AllAttributeTriggerHandler.updatePortwithMultihomeId(attrblist,oldAttributeMap1);
                      
            AllAttributeTriggerHandler.updateColoId(attrblist,oldAttributeMap4);
            Handler.copyCurrencyFromProductConfiguration(attrblist);
            Handler.copyCurrencyFromProductConfiguration(attrblist);       
            Handler.getQuery('cscfga__Attribute__c',Ids);       
            Handler.updateDisplayValue(attrblist,newAttributeMap,oldAttributeMap); 
            //AllAttributeTriggerHandler.AttributeValueIsChanged(Attrblist7,oldAttributeMap);            
            AllAttributeTriggerHandler.PowerForCage(attrblist,newAttributeMap3);             
           
            AllAttributeTriggerHandler.LinkedChildsForCage(attrblist,oldAttributeMap5);
             AllAttributeTriggerHandler.LinkedChilds(attrblist,oldAttributeMap4);            
             
             Handler.copyNniNumberToProductBasket(newAttributeMap2,oldAttributeMap6); 
             Handler.clearAttributeValue(attrblist);
             AllAttributeTriggerHandler.updateCageId(attrblist,oldAttributeMap5);
            Handler.LinkIPCWithPorts(attrblist,oldAttributeMap7);   
            
        } catch(Exception e){
           ee = e;
           ErrorHandlerException.ExecutingClassName='AllAttributeTriggerHandlerTest:AllAttributeTriggerHandlerunittest';         
           ErrorHandlerException.sendException(e);
        } finally {
            Test.stopTest();
            //enableAll(UserInfo.getUserId());
             P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
       }
    } 

    //create Offnet Providers
    private static void createProviders(Integer numOfRecords){
        
        P2A_TestFactoryCls.sampleTestData();
        providers = new List<CS_Provider__c>();
        //Test.starttest();
        if(numOfRecords > 0){
            for(Integer i = 0; i < numOfRecords; i++){
                
                if(Math.mod(i, 2) == 0){
                    providers.add(new CS_Provider__c(Name = 'Test Provider ' + i
                    , NAS_Product__c = Boolean.valueOf('true')
                    , Offnet_Provider__c = Boolean.valueOf('true')
                    , Onnet__c = Boolean.valueOf('false')
                    )
                    );
                    } else {
                    providers.add(new CS_Provider__c(Name = 'Test Provider ' + i
                    , NAS_Product__c = Boolean.valueOf('false')
                    , Offnet_Provider__c = Boolean.valueOf('true')
                    , Onnet__c = Boolean.valueOf('false')
                    )
                    );
                }
            }
            insert providers;
            system.assert(providers!=null);
            System.debug('**** created Providers: ' + providers);
        }
        // Test.stoptest();
        //return providers;
    }
    
    public static testmethod void LinkedChildunittest() {
        
        P2A_TestFactoryCls.sampletestdata();
        List<User> usrlist=P2A_TestFactoryCls.get_Users(1);
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
        List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        
        List<cscfga__Product_Configuration__c> proconfigs =  P2A_TestFactoryCls.getProductonfig(8,prodbasktlist,ProductDeflist,Pbundle,Offerlists);
        
           proconfigs[1].name = 'COLO-CAGE-RACK';
           update proconfigs;
                             //inserting product configuration
        List<cscfga__Product_Configuration__c> proconfigs1= new List<cscfga__Product_Configuration__c>();
            
                                               cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
                pb.cscfga__Product_basket__c =prodbasktlist[0].id ;
                pb.cscfga__Product_Definition__c = ProductDeflist[0].id ;
                pb.name ='IPL';
                pb.cscfga__contract_term_period__c = 12;
                pb.Is_Offnet__c = 'yes';
                pb.cscfga_Offer_Price_MRC__c = 200;
                pb.cscfga_Offer_Price_NRC__c = 300;
                pb.cscfga__Product_Bundle__c =Pbundle[0].id;
                pb.Rate_Card_NRC__c = 200;
                pb.Rate_Card_RC__c = 300;
                pb.cscfga__total_contract_value__c = 1000;
                pb.cscfga__Contract_Term__c = 12;
               pb.CurrencyIsoCode = 'USD';
                pb.cscfga_Offer_Price_MRC__c = 100;
                pb.cscfga_Offer_Price_NRC__c = 100;
                pb.Child_COst__c = 100;
                pb.Cost_NRC__c = 100;
                pb.Cost_MRC__c = 100;
                pb.Product_Name__c = 'test product';
                pb.Added_Ports__c = null ;
                Pb.cscfga__Product_Family__c = 'Point to Point';
                //Pb.csordtelcoa__Replaced_Product_Configuration__c = Pb.csordtelcoa__Replaced_Product_Configuration__c;
                                               Pb.Product_Code__c='IPC';
                                                          proconfigs1.add(pb);
                insert proconfigs1;
                system.assert(proconfigs1!=null);
        List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Attribute_Definition__c> AttDef = P2A_TestFactoryCls.getAttributesdef(2, proconfigs, ProductDeflist, ConScr, ScrSec);
        AttDef[0].cscfga__Type__c = 'Lookup' ;
        //AttDef[0].Is_required_for_pricing__c = 'Yes' ;
        upsert attdef;
                             
                             
        List<cscfga__Attribute_Definition__c> AttDef1 = P2A_TestFactoryCls.getAttributesdef(2, proconfigs, ProductDeflist, ConScr, ScrSec);
        AttDef1[0].cscfga__Type__c = 'Related Product' ;        
        upsert AttDef1; 
        
        List<cscfga__Attribute__c> UpdateAllAttrblist=new List<cscfga__Attribute__c>();
                             
        List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(10, proconfigs, AttDef);
        attrblist[0].Name = 'LinkedChilds';
        attrblist[0].cscfga__Value__c = 'Test';
        
        attrblist[1].Name = 'Select Colo Master';
        attrblist[1].cscfga__Value__c = 'Test1';
        update attrblist;
        
        List<cscfga__Attribute__c> newAttributes = new List<cscfga__Attribute__c>{attrblist[0]};
        
        cscfga__Attribute__c Atts = new cscfga__Attribute__c(name = 'LinkedChilds',cscfga__Product_Configuration__c=proconfigs[0].id,
                        cscfga__Value__c = 'Test2',cscfga__Attribute_Definition__c=AttDef1[0].id);
         insert Atts;  
        
        List<cscfga__Attribute__c> newAttributes1 = new List<cscfga__Attribute__c>{Atts};
        
        
        Map<Id, cscfga__Attribute__c> oldAttributesMap = new Map<Id, cscfga__Attribute__c>();
        oldAttributesMap.put(attrblist[1].id,attrblist[1]); 
        
        
        AllAttributeTriggerHandler.LinkedChilds(newAttributes,oldAttributesMap);
        AllAttributeTriggerHandler.LinkedChilds(newAttributes1,oldAttributesMap);
    }
    
    
    @istest
    public static void testcatchblock()
    {
    AllAttributeTriggerHandler Handler1 = new AllAttributeTriggerHandler();
    try
    {
     Handler1.beforeInsert(null);
    }catch(Exception e){}
    try
    {
     Handler1.beforeUpdate(null,null,null);
    }catch(Exception e){}
     try
    {
     Handler1.afterUpdate(null,null,null);
    }catch(Exception e){}
    try
    {
     Handler1.mapValuesToServices(null,null);
    }catch(Exception e){}
    
    
    try
    {
     Handler1.runMACDUpdate(null,null);
    }catch(Exception e){}
    try
    {
       AllAttributeTriggerHandler.setRowCountOnPC(null,null);
    }catch(Exception e){}
    try
    {
      AllAttributeTriggerHandler.updatePortwithMultihomeId(null,null);
    }catch(Exception e){}
    try
    {
      AllAttributeTriggerHandler.updateCageId(null,null);
    }catch(Exception e){}
    try
    {
       AllAttributeTriggerHandler.LinkedChilds(null,null);
    }catch(Exception e){}
    try
    {
     AllAttributeTriggerHandler.updateColoId(null,null);
    }catch(Exception e){}
    try
    {
        Handler1.copyCurrencyFromProductConfiguration(null);
    }catch(Exception e){}
    try
    {
        Handler1.getQuery(null,null);
    }catch(Exception e){}
     try
    {
        Handler1.updateDisplayValue(null,null,null);
    }catch(Exception e){}
    try
    {
      Handler1.copyNniNumberToProductBasket(null,null); 
    }catch(Exception e){}
    try
    {
      AllAttributeTriggerHandler.PowerForCage(null,null);
    }catch(Exception e){}
    try
    {
      AllAttributeTriggerHandler.AttributeValueIsChanged(null,null);
    }catch(Exception e){}
    try
    {
      Handler1.clearAttributeValue(null);
    }catch(Exception e){}
    try
    {
      AllAttributeTriggerHandler.LinkedChildsForCage(null,null);
    }catch(Exception e){}
    try
    {
      Handler1.LinkIPCWithPorts(null,null);
    }catch(Exception e){}
    
      
    }
    
    
}