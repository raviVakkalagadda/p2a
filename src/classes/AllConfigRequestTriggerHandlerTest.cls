@isTest(SeeAllData = false)
private class AllConfigRequestTriggerHandlerTest {

private static List<cscfga__Product_Category__c> prodCategoryList;
private static List<cscfga__Product_Definition__c> prodDefList;
private static List<cscfga__Product_Configuration__c> pcList;
private static List<csbb__Product_Configuration_Request__c> pcrList;
private static List<csbb__Callout_Product_Result__c> calloutPrResList;

    private static void initTestData(){
        
        prodCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Test Produc Category')
        };
        
        insert prodCategoryList;
        system.assert(prodCategoryList!=null);
        
        prodDefList = new List<cscfga__Product_Definition__c>{
            new cscfga__Product_Definition__c(Name = 'Test Product definition', cscfga__Product_Category__c = prodCategoryList[0].Id)
        };
        
        pcList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'Test PC 1'),
            new cscfga__Product_Configuration__c(Name = 'Test PC 2')
        };
        
      //  insert pcList;
        
        pcrList = new List<csbb__Product_Configuration_Request__c>{
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[0].Id),
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[0].Id),
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[0].Id),
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[1].Id),
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[1].Id),
            new csbb__Product_Configuration_Request__c(csbb__Product_Category__c = prodCategoryList[0].Id, CreatedDate = system.now(), csbb__Product_Configuration__c = pcList[1].Id)            
        };
        
        insert pcrList;
        system.assert(pcrList!=null);
        
        calloutPrResList = new List<csbb__Callout_Product_Result__c>{
            new csbb__Callout_Product_Result__c(csbb__Product_Configuration_Request__c = pcrList[0].Id)
        };
        
        insert calloutPrResList;
        system.assert(calloutPrResList!=null);
    }

    static testMethod void allPCRTriggerTest() {
            Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            Integer numOfPCRs = pcrList.size();
            system.debug('Number of PCRs: ' + numOfPCRs);
            system.assert(numOfPCRs!=null);
            List<SObject> pcrSobjectList = new List<SObject>((List<SObject>)pcrList);
            Map<Id, csbb__Product_Configuration_Request__c> newPcrsMap = new Map<Id, csbb__Product_Configuration_Request__c>();
            List<Id> pcIds = new List<Id>();
            
            for(cscfga__Product_Configuration__c pc : pcList){
                pcIds.add(pc.Id);
            }
            
            for(csbb__Product_Configuration_Request__c pcr : pcrList){
                newPcrsMap.put(pcr.Id, pcr);
            }
            
            AllConfigurationRequestTriggerHandler pcrTriggerHandler = new AllConfigurationRequestTriggerHandler();
            pcrTriggerHandler.afterInsert(pcrSobjectList, newPcrsMap);
            
            List<csbb__Product_Configuration_Request__c> newPCRList = [SELECT Id, Name, csbb__Product_Configuration__c FROM csbb__Product_Configuration_Request__c
                                                                    WHERE csbb__Product_Configuration__c IN :pcIds];
            system.assert(newPCRList!=null);
            Integer newNumOfPCRs = newPCRList.size();
            system.debug('New Number of PCRs: ' + newNumOfPCRs);
            
           // System.assert((newNumOfPCRs > 0) && (newNumOfPCRs < numOfPCRs), 'PCRs not deleted');
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='AllConfigRequestTriggerHandlerTest :allPCRTriggerTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}