public with sharing class AllConfigurationRequestTriggerHandler extends BaseTriggerHandler{
    public override Boolean isDisabled(){
      if (TriggerFlags.NoConfigurationRequestTriggerHandler){
                return true;
            }
            return triggerDisabled;
        }
    public override void afterInsert(List<SObject> newConfigRequests, Map<Id, SObject> newBasketsMap) {
        System.debug('**** AllConfigurationRequestTriggerHandler - afterInsert'+newConfigRequests);
        reduceDuplicates((List<csbb__Product_Configuration_Request__c>)newConfigRequests, (Map<Id, csbb__Product_Configuration_Request__c>)newBasketsMap, null);
    }
    
    public override void afterUpdate(List<SObject> newConfigRequests, Map<Id, SObject> newBasketsMap, Map<Id, SObject> oldObjectsMap) {
        System.debug('**** AllConfigurationRequestTriggerHandler - afterUpdate'+newConfigRequests);
        reduceDuplicates((List<csbb__Product_Configuration_Request__c>)newConfigRequests, (Map<Id, csbb__Product_Configuration_Request__c>)newBasketsMap, null);
    }
    
    private void reduceDuplicates(List<csbb__Product_Configuration_Request__c> newConfigRequests, Map<Id, csbb__Product_Configuration_Request__c> newMap, Map<Id, csbb__Product_Configuration_Request__c> oldMap) {
        System.debug('++++ Reduce duplicates ++++');
        
        List<csbb__Callout_Product_Result__c> calloutPrResList = new List<csbb__Callout_Product_Result__c>();
        List<csbb__Callout_Product_Result__c> tempCalloutPrResList;
        Map<Id, List<csbb__Callout_Product_Result__c>> calloutPrResMap = new Map<Id, List<csbb__Callout_Product_Result__c>>();
        
        calloutPrResList = [SELECT Id, Name, csbb__Product_Configuration_Request__c 
                            FROM csbb__Callout_Product_Result__c
                            WHERE csbb__Product_Configuration_Request__c IN :newMap.keySet()];
        
        if(!calloutPrResList.isEmpty()){
            for(csbb__Callout_Product_Result__c cpr : calloutPrResList){
                tempCalloutPrResList = calloutPrResMap.get(cpr.Id);
                
                if(tempCalloutPrResList == null){
                    tempCalloutPrResList = new List<csbb__Callout_Product_Result__c>();
                } 
                    
                tempCalloutPrResList.add(cpr);
                calloutPrResMap.put(cpr.csbb__Product_Configuration_Request__c, tempCalloutPrResList);
            }
        }

        
        List<Id> productConfigIds = new List<Id>();
        for (csbb__Product_Configuration_Request__c req : newConfigRequests) {   
            if(req.csbb__Product_Configuration__c != null){
                productConfigIds.add(req.csbb__Product_Configuration__c);    
            }
        }
        
        
        System.debug('++++ Product config ids: ' + productConfigIds);
        
        if(!productConfigIds.isEmpty()){
            List<csbb__Product_Configuration_Request__c> pcrList = new List<csbb__Product_Configuration_Request__c>();
            List<csbb__Product_Configuration_Request__c> toDelete = new List<csbb__Product_Configuration_Request__c>();
            Boolean isAdvancedClone = false;
            
            /*List<cscfga__Product_Configuration__c> pcList = [SELECT Id, Name, (SELECT Id, CreatedDate FROM csbb__Product_Configuration_Requests__r ORDER BY CreatedDate DESC)
                                                            FROM  cscfga__Product_Configuration__c
                                                            WHERE Id in :productConfigIds];*/
                 
            for(cscfga__Product_Configuration__c pc : [SELECT Id, Name, (SELECT Id, CreatedDate FROM csbb__Product_Configuration_Requests__r ORDER BY CreatedDate DESC)
                                                            FROM  cscfga__Product_Configuration__c
                                                            WHERE Id in :productConfigIds]){
                if(pc.csbb__Product_Configuration_Requests__r != null && pc.csbb__Product_Configuration_Requests__r.size() > 1){    
                    for(Integer i = 0; i< pc.csbb__Product_Configuration_Requests__r.size(); i++){
                        pcrList.add(pc.csbb__Product_Configuration_Requests__r.get(i));
                    }
                    
                }
                
                if(!pcrList.isEmpty()){
                    
                    for(csbb__Product_Configuration_Request__c pcr : pcrList){
                        if(calloutPrResMap.get(pcr.Id) == null){
                            isAdvancedClone = false;
                        } else {
                            isAdvancedClone = true;
                        }
                    }   
                    
                    if(isAdvancedClone){
                        for(csbb__Product_Configuration_Request__c pcr : pcrList){
                            if(calloutPrResMap.get(pcr.Id) == null){
                                toDelete.add(pcr);
                            }
                        }
                    } else {
                        for(Integer i = 1; i < pcrList.size(); i++){
                            toDelete.add(pcrList.get(i));
                        }
                    }
                    
                }   
                
                pcrList.clear();
            }
     
            if(!toDelete.isEmpty()){
                System.debug('++++ Deleting PCRs: ' + toDelete);
                delete toDelete; 
            }     

        }
        
    } 
}