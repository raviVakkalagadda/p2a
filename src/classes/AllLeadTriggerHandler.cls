/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class AllLeadTriggerHandler extends BaseTriggerHandler {
public class AllLeadTriggerHandler extends BaseTriggerHandler {

    @TestVisible
    private Boolean triggerDisabled = false;

    public override Boolean isDisabled(){
        if(Util.muteAllTriggers()){
            return true;
        }
        return triggerDisabled;
    }

    public override void beforeInsert(List<SObject> newLead) {
        //IR 168 Lead Management Changes
        LeadBeforeTrgHandler leadBefTrgHdlr = new LeadBeforeTrgHandler();
        leadBefTrgHdlr.ctryPickTrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.ctryValidTrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.postcodeFrmtforUSATrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.existLeadTrigHandler(trigger.new,trigger.isInsert,trigger.isBefore);
    }

    public override void afterInsert(List<SObject> newLead, Map<Id, SObject> newLeadMap) {
        // IR 168 Lead Management Changes
        LeadAfterTrgHandler leadAftTrgHandler = new LeadAfterTrgHandler();
        leadAftTrgHandler.existLeadTrigHandler(trigger.new,trigger.isInsert,trigger.isAfter);        
    }

    //public override void beforeUpdate(List<SObject> newActionItems, Map<Id,SObject> newActionItemsMap, Map<Id,SObject> oldActionItemsMap) {
    public override void beforeUpdate(List<SObject> newLead, Map<Id, SObject> newLeadMap, Map<Id, SObject> oldLeadMap) {
        //IR 168 Lead Management Changes
        LeadBeforeTrgHandler leadBefTrgHdlr = new LeadBeforeTrgHandler();
        leadBefTrgHdlr.ctryPickTrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.ctryValidTrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.postcodeFrmtforUSATrgHandler(trigger.new,trigger.isInsert,trigger.isUpdate, trigger.isBefore);
        leadBefTrgHdlr.existLeadTrigHandler(trigger.new,trigger.isInsert,trigger.isBefore);
        leadBefTrgHdlr.leadQueueChangeAction(trigger.new, trigger.isUpdate, trigger.isBefore, Trigger.old);
        
        
        //public void leadQueueChangeAction(List<Lead> trigNew,boolean isUpdate, boolean isBefore,map<id,Lead> oldMap)
    
    }
    
    public override void afterUpdate(List<SObject> newLead, Map<Id, SObject> newLeadMap, Map<Id, SObject>  oldLeadMap) {
        LeadAfterTrgHandler leadAftTrgHandler = new LeadAfterTrgHandler();
         leadAftTrgHandler.existLeadTrigHandler(trigger.new,trigger.isInsert,trigger.isAfter);
    }
     
    public override void beforeDelete(Map<Id, SObject> oldLeadMap) {   
        //IR 168 Lead Management Changes
        profPermToDelete(trigger.old, (Map<Id, Lead>) oldLeadMap);
    }
    
     @TestVisible
    private void profPermToDelete(List<Lead> trigOld, Map<Id, Lead> oldLeadMap)
	{
		if(oldLeadMap.values() != null)
		{
		
		for(Profile profObj : [SELECT id, profile.name from profile where name != 'TI Marketing' And name != 'System Administrator'])
        {    
        	for(Lead leadObj : trigOld) 
    		{
           		try{
            	 	String uid=UserInfo.getProfileId();
                    if(uid == profObj.Id)
                	{
                    	leadObj.addError('Only Ti_Marketing users have Permission to delete Lead record');
                	}
             	}catch(Exception e){
                 ErrorHandlerException.ExecutingClassName='AllLeadTriggerHandler:profPermToDelete';
                 ErrorHandlerException.objectList=trigOld;
                 ErrorHandlerException.sendException(e);
                 System.debug('The following exception has occurred: ' + e.getMessage());
            	}    
          	}
        }   
    	}
	} 
}