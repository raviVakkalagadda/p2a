@isTest
class AllOpportunityLineItemTriggersTest{

static testmethod void AllOpportunityLineItem () {

  Country_Lookup__c country = new Country_Lookup__c();            
    country.CCMS_Country_Code__c = 'HK';            
    country.CCMS_Country_Name__c = 'India';            
    country.Country_Code__c = 'HK';            
    insert country;  
    system.assert(country!=null);
    List<Country_Lookup__c> coun = [Select id,CCMS_Country_Name__c from Country_Lookup__c where CCMS_Country_Name__c = 'India'];
    system.assertequals(country.CCMS_Country_Name__c,coun[0].CCMS_Country_Name__c);
    Account acc = new Account();           
    acc.Name = 'Test Account';            
    acc.Customer_Type__c = 'MNC';            
    acc.Country__c = country.Id;            
    acc.Selling_Entity__c = 'Telstra INC';  
    acc.Activated__c= true;  
    acc.Account_ID__c = '9090';  
    acc.Customer_Legal_Entity_Name__c = 'Test' ;     
    insert acc;       
    Opportunity opp = new Opportunity();          
    opp.Name = 'Test Opportunity';            
    opp.AccountId = acc.Id;   
    //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
    opp.StageName = 'Identify & Define' ;  
    opp.Stage__c= 'Identify & Define' ;          
    opp.CloseDate = System.today();   
    opp.Opportunity_Type__c = 'Complex';
    opp.Estimated_MRC__c=8;
    opp.Estimated_NRC__c=2;
    opp.ContractTerm__c='1';  
    insert opp;    
    system.assert(opp!=null);
       Product2 prod = new Product2(Name = 'Laptop X200', 
            Family = 'Hardware');
        insert prod;
    
           Id pricebookId = Test.getStandardPricebookId();
           
           
          PricebookEntry standardPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 10000, IsActive = true);
        insert standardPrice;
        //Nitin: 10/04/2017
        opp.Pricebook2Id = pricebookId;
                                update opp;
                                //Nitin: 10/04/2017
        OpportunityLineItem oli = new OpportunityLineItem (PricebookEntryId=standardPrice.id,OpportunityId=opp.id,unitPrice=10,Quantity = 5);
          
        
        
        insert oli;
        system.assert(oli!=null);
        }
}