public with sharing class AllOpportunityProductTypeHandler extends BaseTriggerHandler{
    
    /*public AllOpportunityProductTypeHandler(){
        
        super();
        allowRecursion();
        
    }*/
    
       /**
         * Before update handler
         */
        public override void beforeUpdate(List<SObject> newOppProdType, Map<Id, SObject> newOppProdTypeMap, Map<Id, SObject> oldOppProdTypeMap) {
            updateReportedSOV((List<Opp_Product_Type__c>) newOppProdType,(Map<Id, Opp_Product_Type__c> )newOppProdTypeMap,(Map<Id, Opp_Product_Type__c> )oldOppProdTypeMap);
                
                    
        }
        
        public override void afterUpdate(List<SObject> newOppProdType, Map<Id, SObject> newOppProdTypeMap, Map<Id, SObject> oldOppProdTypeMap) {
            updateSSCategory((List<Opp_Product_Type__c>) newOppProdType,(Map<Id, Opp_Product_Type__c> )newOppProdTypeMap,(Map<Id, Opp_Product_Type__c> )oldOppProdTypeMap);
        }
        
       public void updateSSCategory(List<Opp_Product_Type__c> newOppProdType,Map<Id, Opp_Product_Type__c> newOppProdTypeMap,Map<Id, Opp_Product_Type__c> oldOppProdTypeMap){
             Set<Id> OppIdSet=new Set<Id>();
         
          for(Opp_Product_Type__c oppt:newOppProdType){
      
       if(newOppProdTypeMap.get(oppt.Id).Product_SOV__c!=oldOppProdTypeMap.get(oppt.Id).Product_SOV__c){
          
         OppIdSet.add(oppt.opportunity__c); 
       }  
        
          }
        
        if (OppIdSet.size()>0) {
         List<OpportunitySplit> OppSplitList=[select id,Opportunity.Sales_Status__c,Opportunity.StageName,SplitAmount_Product_SOV__c,SplitPercentage_Product_SOV__c,Team_Role__c,Total_Product_SOV__c,OpportunityId from OpportunitySplit where OppSplitType__c=:true and OpportunityId In:OppIdSet];
        if(OppSplitList!=null && OppSplitList.size()>0){
        for(Opp_Product_Type__c oppt:newOppProdType){
         for(OpportunitySplit oppSplitObj:OppSplitList){
            if(oppSplitObj.SplitPercentage_Product_SOV__c!=null && oppSplitObj.Total_Product_SOV__c!=null && oppSplitObj.Team_Role__c!=null && oppt.Sales_Specialist_Category__c!=null && oppSplitObj.Team_Role__c.contains(oppt.Sales_Specialist_Category__c) && oppt.Opportunity__c==oppSplitObj.OpportunityId){
                oppSplitObj.Total_Product_SOV__c=oppt.Product_SOV__c;
                oppSplitObj.SplitAmount_Product_SOV__c=(oppSplitObj.SplitPercentage_Product_SOV__c*oppSplitObj.Total_Product_SOV__c)/100;
                
            }
            
         }
        }
        
         if(OppSplitList.size()>0){//Util.OppoProductTypeFlag=false; 
		 TriggerFlags.NoOpportunitySplitValError=true;
            update OppSplitList;
            }
            }
            }
        
        
        
       }
    /**
      * This method updates Reported New SOV and Reported Renewal SOV with Calculated New SOV and Calculated Renewal SOV respectively
      */
    
    public void updateReportedSOV(List<Opp_Product_Type__c> newOppProdType, Map<Id, Opp_Product_Type__c> newOppProdTypeMap, Map<Id, Opp_Product_Type__c> oldOppProdTypeMap){
       //171
    Set<Id> OppIdSet=new Set<Id>();
    Map<Id,OpportunitySplit> mapIdOppSplit=new Map<Id,OpportunitySplit>();
     List<Product_Type_with_Role__c> ListRoleMappingObj = Product_Type_with_Role__c.getAll().values();
    Map<String,String> MapOppType_Role=new Map<String,String>();
      for(Product_Type_with_Role__c prodRoleObj:ListRoleMappingObj){
        
        MapOppType_Role.put(prodRoleObj.Product_Type__c,prodRoleObj.Role__c);
       
    }
    //171
   
   
   
    for(Opp_Product_Type__c oppt:newOppProdType){
    
            if(oppt.Calculated_New_SOV__c!= oldOppProdTypeMap.get(oppt.Id).Calculated_New_SOV__c /*&& oppt.Reported_New_SOV__c== oldOppProdTypeMap.get(oppt.Id).Reported_New_SOV__c*/){
                oppt.Reported_New_SOV__c = oppt.Calculated_New_SOV__c ;
                system.debug('OPT1'+ oppt.Reported_New_SOV__c );              
            }
            if(oppt.Calculated_Renewal_SOV__c!= oldOppProdTypeMap.get(oppt.Id).Calculated_Renewal_SOV__c /*&& oppt.Reported_Renewal_SOV__c== oldOppProdTypeMap.get(oppt.Id).Reported_Renewal_SOV__c*/){
                oppt.Reported_Renewal_SOV__c = oppt.Calculated_Renewal_SOV__c;
                system.debug('OPT2'+ oppt.Reported_New_SOV__c );             
            }
    
                //171
            //if(label.MuteSPT != 'TRUE'){
           if(oppt.Product_Type_Name__c=='Global ATM' || oppt.Product_Type_Name__c == 'Conferencing and Collaboration (UC)' || oppt.Product_Type_Name__c == 'GVoIP'|| oppt.Product_Type_Name__c == 'Global Cloud Collaboration' || oppt.Product_Type_Name__c == 'Global Hosting Services - DRaaS' || oppt.Product_Type_Name__c == 'Global Hosting Services - IaaS' || oppt.Product_Type_Name__c == 'Global Hosting Services - SaaS' || oppt.Product_Type_Name__c == 'IP Telephony' || oppt.Product_Type_Name__c == 'Virtual Contact Centre'|| oppt.Product_Type_Name__c == 'Global Hosting Services - SaaS' || oppt.Product_Type_Name__c == 'Contact Centre Solutions (UC)' || oppt.Product_Type_Name__c == 'Cloud Collaboration' || oppt.Product_Type_Name__c == 'PEN' || oppt.Product_Type_Name__c == 'Global Hosting Services - Prof Svc'|| oppt.Product_Type_Name__c =='Global Hosting Services-On-Premise Cloud'){
            oppt.Product_SOV__c=oppt.Reported_Total_SOV__c;
          } 
          if(oppt.Quote_Status__c!='Approved' && oppt.Quote_Status__c!='Accepted'){
            
          oppt.Product_SOV__c=oppt.Reported_Total_SOV__c;   
          }
         
          //171
           oppt.Sales_Specialist_Category__c=MapOppType_Role.containsKey(oppt.Product_Type_Name__c)?MapOppType_Role.get(oppt.Product_Type_Name__c):null; 
         //}
    
    }
        
        
        
    }
    
    
    
}