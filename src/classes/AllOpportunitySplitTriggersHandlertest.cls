@isTest (SeeAllData=false)
public class AllOpportunitySplitTriggersHandlertest
{
   static testMethod void AllOpportunitySplitTriggers() {

        Test.startTest();
       
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        
       /* OpportunitySplit os1 = new OpportunitySplit();  
        //os1.Description = 'TestOppotunity split';
        os1.OpportunityId = oppList[0].id  ;  
        os1.SplitOwnerId = userinfo.getuserid();
        //os1.SplitPercentage = 
        //insert os1;
        
        OpportunitySplitType oppSplitType = new OpportunitySplitType();
        //oppSplitType.DeveloperName = userinfo.getuserId();
        oppSplitType.isactive = true;
        */
        

       List<OpportunitySplit> newOpportunitySplit = new List<OpportunitySplit>();
       list<OpportunitySplitType> newost = new list<OpportunitySplitType>() ;
       OpportunitySplitType ost = new OpportunitySplitType();
       system.assert(oppList!=null);
       
       //ost.DeveloperName='Test';
       //ost.isactive=True;
        newost.add(ost);
       Map<Id, OpportunitySplit> newOpportunitySplitMap = new Map<Id, OpportunitySplit>();
       Map<Id, OpportunitySplit> oldnewOpportunitySplitMap = new Map<Id, OpportunitySplit>();
      // Map<Id,SObject> oldnewOpportunitySplitMap1 = new Map<Id,SObject>();
       Map<id,OpportunitySplit> mapOppSplit1 =new Map<id,OpportunitySplit>();
       Map<id,OpportunitySplit> mapOppSplit=new Map<id,OpportunitySplit>();
       List<OpportunitySplit> oppSplit1 = new List<OpportunitySplit>([select id,CurrUserProfileName__c,Opportunity.Owner.userRoleId,Opportunity.StageName from Opportunitysplit]);
       AllOpportunitySplitTriggersHandler app=new AllOpportunitySplitTriggersHandler();
       app.createOpportunitySplit(newOpportunitySplit);
       app.validationOnOpportunitydeletion(newOpportunitySplit,newOpportunitySplitMap,oldnewOpportunitySplitMap);
       newOpportunitySplit=oldnewOpportunitySplitMap.values();
       mapOppSplit=oldnewOpportunitySplitMap;
        list<UserRole> r = new list<UserRole>([select Id,parentRoleId from UserRole]);
       UserRole r1 = new UserRole(DeveloperName = 'MyCustomRole', Name = 'My Role');
       r.add(r1);
       system.assert(r1!=null);
       Set<Id> ownerRoleSet=new Set<Id>();
       string str1 = system.label.ProfileNotForSplitOpp;
        for(OpportunitySplit opp:oppSplit1)
        {
            opp.Opportunity.StageName='Closed Won';
            str1=opp.CurrUserProfileName__c;
          ownerRoleSet.add(opp.Opportunity.Owner.userRoleId);
          mapOppSplit.get(opp.Id);  
        }
     // newOpportunitySplit = mapOppSplit.values();
     
     app.beforeInsert(newOpportunitySplit);
     app.beforeUpdate(newOpportunitySplit,mapOppSplit1,mapOppSplit);
       app.beforeDelete(mapOppSplit1 );
       app.afterInsert(newOpportunitySplit,mapOppSplit1);
       app.afterUpdate(newOpportunitySplit,mapOppSplit1,mapOppSplit);
      Test.stopTest();  
}
 @isTest static void testExceptions(){
         AllOpportunitySplitTriggersHandler alls=new AllOpportunitySplitTriggersHandler();
        
         try{alls.validationOnOpportunitydeletion(null,null,null);}catch(Exception e){}
          system.assertEquals(true,alls!=null);    
         
         
     }


}