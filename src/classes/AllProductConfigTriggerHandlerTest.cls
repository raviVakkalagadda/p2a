@isTest(SeeAllData=false) 
public class AllProductConfigTriggerHandlerTest
{
    static testMethod void initTestData1()
    {
        Set<ID> ids = new Set<ID>();
         P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1 , accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<cscfga__Product_Definition__c>ListProductdef =  P2A_TestFactoryCls.getProductdef(1);
         List<cscfga__Product_Bundle__c> pbundlelist =  P2A_TestFactoryCls.getProductBundleHdlr(1,oppList);
         List<cscfga__Configuration_Offer__c> Listconfigoffer = P2A_TestFactoryCls.getOffers(1);
         List<cscfga__Product_Configuration__c> Listconfig = P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ListProductdef ,pbundlelist,Listconfigoffer);
            ids.add(Listconfig[0].id);
            
          Listconfig[0].name = 'Colocation';
          
          update Listconfig ;
          
          Listconfig[0].name = 'IPT Singlehome';
          update Listconfig ;         
          
          Listconfig[0].GIE_Selected_ASBR__c = 'sfdfsdf';
          update Listconfig ;
          List<cscfga__Product_Configuration__c> pcon = [Select id,name from cscfga__Product_Configuration__c where name ='Colocation' limit 10];
          //system.assertequals(Colocation,pcon[0].name);
          
          system.assert(Listconfig!=null);
          
         AllProductConfigurationTriggerHandler allProdConf = new AllProductConfigurationTriggerHandler();
         system.assert(allProdConf!=null);
         Map<Id,cscfga__Product_Configuration__c> prodConfigMap = new Map<Id,cscfga__Product_Configuration__c>();
         for(cscfga__Product_Configuration__c prodConfig : Listconfig)
         {
             prodConfigMap.put(prodConfig.Id,prodConfig);
         }
         
         //cscfga__Product_Configuration__c prodconf = new cscfga__Product_Configuration__c(name ='Colocation',cscfga__Product_Basket__c=prodBaskList[0].id);
         //insert prodconf;
         
         Test.startTest();
         
         allProdConf.isDisabled();
         allProdConf.beforeInsert(Listconfig);
         allProdConf.afterInsert(Listconfig,prodConfigMap);
         allProdConf.beforeUpdate(Listconfig,prodConfigMap,prodConfigMap);
         allProdConf.afterUpdate(Listconfig,prodConfigMap,prodConfigMap);         
         //allProdConf.calQuoteRequest(Listconfig);
         //allProdConf.calculateDiscountsSapan(Listconfig,prodConfigMap,prodConfigMap);
         allProdConf.updateScreenFlowField(Listconfig);
         allProdConf.countOffnetProducts(prodConfigMap,prodConfigMap);
         allProdConf.runManagedSharing(Listconfig);
         allProdConf.afterDelete(prodConfigMap);
         allProdConf.calculateBasketTCV(prodConfigMap);
         //allProdConf.clearMasterIPVPNIds(Listconfig);
         //AllProductConfigurationTriggerHandler.updateIPTSinglehome(Listconfig);
         
         AllProductConfigurationTriggerHandler.changeConfigOwner(ids);
         //AllProductConfigurationTriggerHandler.updateProductInformationFromPC(prodConfigMap);
         
         //Util.muteAllTriggers = false;
        Test.stopTest();
        
        //public void countOffnetProducts(Map<Id,cscfga__Product_Configuration__c> mapNewProductConfigs, Map<Id,cscfga__Product_Configuration__c> mapOldProductConfigs){
        
        
        //calculateBasketTCV(Map<Id, cscfga__Product_Configuration__c> oldProductConfigsMap)
    }

    @isTest static void UpdateProductBasketIsColoTest(){
         P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1 , accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<cscfga__Product_Definition__c>ListProductdef =  P2A_TestFactoryCls.getProductdef(1);
         List<cscfga__Product_Bundle__c> pbundlelist =  P2A_TestFactoryCls.getProductBundleHdlr(1,oppList);
         List<cscfga__Configuration_Offer__c> Listconfigoffer = P2A_TestFactoryCls.getOffers(1);
         List<cscfga__Product_Configuration__c> Listconfig = P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ListProductdef ,pbundlelist,Listconfigoffer);
         Listconfig[0].product_id__c='COLO-CNTDCOLO';
         update Listconfig;
        
        cscfga__Product_Configuration__c oldPc=new cscfga__Product_Configuration__c();
        oldPc.id=Listconfig[0].id;
        oldpc.cscfga__Product_Basket__c=null;
        
        Map<Id,cscfga__Product_Configuration__c>oldPcMap=new Map<Id,cscfga__Product_Configuration__c>();
        oldPcMap.put(oldpc.id,oldpc);
        AllproductConfigurationTriggerHandler.UpdateProductBasketIsColo(Listconfig,oldPcMap);
        AllproductConfigurationTriggerHandler.UpdateProductBasketIsColo(Listconfig,null);

        List<cscfga__product_basket__c>baskList=[select Is_Colo_Product__c  from cscfga__product_basket__c where Id=:Listconfig[0].cscfga__Product_Basket__c];      
        System.assertEquals('Yes',baskList[0].Is_Colo_Product__c);
    }
    
    @isTest static void HierarchySortingTest(){
         P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1 , accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
       
         
         cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c(name='Custom PTP',cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf');
         insert pb;
           
         cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
         cscfga__Product_basket__c =prodBaskList[0].id,Hierarchy_Sequence_Key__c='',cscfga__Product_Definition__c=pb.id/*,Hierarchy__c='1.02.003.0004'*/);
         
          cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
         cscfga__Product_basket__c =prodBaskList[0].id,Hierarchy_Sequence_Key__c='',cscfga__Product_Definition__c=pb.id,Hierarchy__c='1');
         
          cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
         cscfga__Product_basket__c =prodBaskList[0].id,Hierarchy_Sequence_Key__c='',cscfga__Product_Definition__c=pb.id,Hierarchy__c='02');
         
          cscfga__Product_Configuration__c pc3 = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
         cscfga__Product_basket__c =prodBaskList[0].id,Hierarchy_Sequence_Key__c='',cscfga__Product_Definition__c=pb.id,Hierarchy__c='003');
         
         cscfga__Product_Configuration__c pc4 = new cscfga__Product_Configuration__c(cscfga__Product_Family__c ='Custom PTP',
         cscfga__Product_basket__c =prodBaskList[0].id,Hierarchy_Sequence_Key__c='',cscfga__Product_Definition__c=pb.id,Hierarchy__c='0004');
         
         List<cscfga__Product_Configuration__c>pcList=new List<cscfga__Product_Configuration__c>{pc,pc1,pc2,pc3,pc4};
         insert pcList;
        
        
        AllproductConfigurationTriggerHandler.hierarchySorting(pcList);
        Map<Id,cscfga__Product_Configuration__c>resPcMap=new Map<Id,cscfga__Product_Configuration__c>([select id,Hierarchy_Sequence_Key__c  from cscfga__Product_Configuration__c where Id IN: pcList]);
        //System.assertEquals(resPcMap.get(pc.id).Hierarchy_Sequence_Key__c,'0001.0002.0003.0004');
        //System.assertEquals(resPcMap.get(pc1.id).Hierarchy_Sequence_Key__c,'0001');
        //System.assertEquals(resPcMap.get(pc2.id).Hierarchy_Sequence_Key__c,'0002');
        //System.assertEquals(resPcMap.get(pc3.id).Hierarchy_Sequence_Key__c,'0003');
        //System.assertEquals(resPcMap.get(pc4.id).Hierarchy_Sequence_Key__c,'0004');
    }

    
    @istest
    public static void testcatchblock()
    {
    AllProductConfigurationTriggerHandler allProdConf1 = new AllProductConfigurationTriggerHandler();
        
    try
    {
     allProdConf1.countOffnetProducts(null,null);
    }catch(Exception e){}
    try
    {
      AllProductConfigurationTriggerHandler.UpdateProductBasketHasOffnetExecute(null);
    }catch(Exception e){}
    
    try
    {
     AllProductConfigurationTriggerHandler.changeConfigOwner(null); 
    }catch(Exception e){}
    
    try
    {
      //AllProductConfigurationTriggerHandler.UpdateProductInfoFuture(null);
    }catch(Exception e){}
    try
    {
     allProdConf1.UpdateGIEAsbr(null);
 
    }catch(Exception e){}
    try
    {
      allProdConf1.updateProductConfigStatusMLE(null);
    }catch(Exception e){}
    try
    {
     allProdConf1.calculateBasketTCV(null);  
    }catch(Exception e){}

    
    } 
    
  
}
