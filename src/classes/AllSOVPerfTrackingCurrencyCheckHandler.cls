public without sharing class AllSOVPerfTrackingCurrencyCheckHandler extends BaseTriggerHandler{
        /**
         * Before Insert handler
         */
         
       public override void beforeInsert(List<SObject> newSOVPerf) {
         updateCurrencybeforeInsert((List<Performance_Tracking__c>)newSOVPerf);
         
         }
         
         
       /**
         * Before update handler
         */
     
        public override void beforeUpdate(List<SObject> newSOVPerf, Map<Id, SObject> newSOVPerfMap, Map<Id, SObject> oldnewSOVPerfMap) {
             updateCurrencybeforeUpdate((List<Performance_Tracking__c>)newSOVPerf,(Map<Id, Performance_Tracking__c>)newSOVPerfMap,(Map<Id, Performance_Tracking__c>)oldnewSOVPerfMap);  
                    
        }
        
 public void updateCurrencybeforeUpdate(List<Performance_Tracking__c> newSOVPerf, Map<Id, Performance_Tracking__c> newSOVPerfMap, Map<Id, Performance_Tracking__c> oldnewSOVPerfMap){
           //List<CurrencyType> currencyTypeList = new List<CurrencyType>();
         Map<String,CurrencyType> isoCodeCurrencyTypeObjMap = new Map<String,CurrencyType>();
         //currencyTypeList = [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType];
                                
         for(CurrencyType currencyTypeObj : [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType]){
         isoCodeCurrencyTypeObjMap.put(currencyTypeObj.IsoCode,currencyTypeObj);
         }
         
         Set<Id> employeeIdSet=new Set<Id>();
         Map<String,Performance_Tracking__c> perfMap=new  Map<String,Performance_Tracking__c>();
         system.debug('======trigger.new===='+trigger.new);
         List<Performance_Tracking__c> perfList=new List<Performance_Tracking__c>();
          
          
  
  
        for(Performance_Tracking__c  perfObj:newSOVPerf){
        	
        if(oldnewSOVPerfMap!=null && oldnewSOVPerfMap.get(perfObj.id).Currency__c!=perfObj.Currency__c && perfObj.SOV_Type__c=='Performance'){
         perfMap.put(perfObj.Employee__c+perfObj.Fiscal_year__c+perfObj.SOV_Type__c,perfObj);
         employeeIdSet.add(perfObj.Employee__c);
        }
    
    
        }
    PerformanceSOVRollupController perfSOVobj=new PerformanceSOVRollupController();
    String fiscalYear=(perfSOVobj.getQuarter(system.today())).fiscalyear;
 if(employeeIdSet.size()>0 && fiscalYear!=''){
perfList=[select Opportunity_Currency__c,Currency__c,Q1_Individual_Performance_Opp__c,Q1_Rolled_Up_Opp__c,Q2_Individual_Performance_Opp__c,Q2_Rolled_Up_Opp__c,Q3_Individual_Performance_Opp__c,Q3_Rolled_Up_Opp__c,Q4_Individual_Performance_Opp__c,Q4_Rolled_Up_Opp__c,CurrencyIsoCode,OwnerId,Credit_Debit_Date__c,Q1_SOV_Credit__c,Q2_SOV_Credit__c,Q3_SOV_Credit__c,Q4_SOV_Credit__c,Opportunity_Look_Up__c,SOV_Type__c,UserRoleId__c,Employee__c,Employee__r.UserRoleId,Q1_End_date__c,Q1_Start_date__c,Q2_End_date__c,Q2_Start_date__c,Fiscal_year__c,Q3_End_date__c,Q3_Start_date__c,Q4_End_date__c,Q4_Start_date__c,Employee__r.SIP_User__c,FY_End_Date__c,FY_Start_date__c,Q1_Individual_Performance__c,Q1_Rolled_Up__c,Q1_Target__c,Q2_Individual_Performance__c,Q2_Rolled_Up__c,Q2_Target__c,Q3_Individual_Performance__c,Q3_Rolled_Up__c,Q3_Target__c,Q4_Individual_Performance__c,Q4_Rolled_Up__c,Q4_Target__c,Q1_GAM_Roll_Up__c,Q2_GAM_Roll_Up__c,Q3_GAM_Roll_Up__c,Q4_GAM_Roll_Up__c from Performance_Tracking__c where  /*Opportunity_Look_Up__c =: oppSplitList[0].OpportunityId*/ Fiscal_year__c=:fiscalYear and Employee__c IN:employeeIdSet and SOV_Type__c!='Performance' and Employee__r.SIP_User__c=:true limit 10000];
 }
system.debug('=====perfList======'+perfList.size());
if(perfList!=null && perfList.size()>0){
  for(Performance_Tracking__c  perfObj:perfList){
    
    if(perfObj.SOV_Type__c!='Performance' && perfMap!=null && perfMap.containskey(perfObj.Employee__c+perfObj.Fiscal_year__c+'Performance') && perfMap.get(perfObj.Employee__c+perfObj.Fiscal_year__c+'Performance').Employee__c==perfObj.Employee__c && perfMap.get(perfObj.Employee__c+perfObj.Fiscal_year__c+'Performance').Fiscal_year__c==perfObj.Fiscal_year__c){
        
       perfObj.Currency__c=perfMap.get(perfObj.Employee__c+perfObj.Fiscal_year__c+'Performance').Currency__c;
    }
    
  }
}
  for(Performance_Tracking__c  perfObj:newSOVPerf){
 //Double currencyExchangeRateValue = isoCodeCurrencyTypeObjMap.get(perfObj.CurrencyIsoCode).ConversionRate;
 if(perfObj.Currency__c==null || perfObj.Currency__c==''){perfObj.Currency__c='AUD';}
 
 Double currencyExchangeRateValue1 = isoCodeCurrencyTypeObjMap.get(perfObj.Currency__c).ConversionRate;
 Double currencyExchangeRateValueAUD1 = isoCodeCurrencyTypeObjMap.get(perfObj.CurrencyIsoCode).ConversionRate;
            system.debug('====perfObj.Currency__c'+perfObj.Currency__c);
            perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //record currency to USD
               if(perfObj.SOV_Type__c!='Performance' && perfObj.Opportunity_Currency__c!=null){
                Double currencyExchangeRateValueAUD = isoCodeCurrencyTypeObjMap.get(perfObj.Opportunity_Currency__c).ConversionRate;

                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up_Opp__c!=null)?(((perfObj.Q1_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance_Opp__c!=null)?(((perfObj.Q1_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up_Opp__c!=null)?(((perfObj.Q2_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance_Opp__c!=null)?(((perfObj.Q2_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up_Opp__c!=null)?(((perfObj.Q3_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance_Opp__c!=null)?(((perfObj.Q3_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up_Opp__c!=null)?(((perfObj.Q4_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance_Opp__c!=null)?(((perfObj.Q4_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

               }else{
               
               system.debug('=====perfObj.Q2_Individual_Performance__c===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up__c!=null)?(((perfObj.Q1_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance__c!=null)?(((perfObj.Q1_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up__c!=null)?(((perfObj.Q2_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance__c!=null)?(((perfObj.Q2_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up__c!=null)?(((perfObj.Q3_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance__c!=null)?(((perfObj.Q3_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up__c!=null)?(((perfObj.Q4_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance__c!=null)?(((perfObj.Q4_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
 system.debug('=====perfObj.Q2_Individual_Performance__c222222===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Target__c=(perfObj.Q1_Target__c!=null)?(((perfObj.Q1_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q2_Target__c=(perfObj.Q2_Target__c!=null)?(((perfObj.Q2_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q3_Target__c=(perfObj.Q3_Target__c!=null)?(((perfObj.Q3_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q4_Target__c=(perfObj.Q4_Target__c!=null)?(((perfObj.Q4_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
         

            }
            
            
 
 }
 
  for(Performance_Tracking__c  perfObj:perfList){
 if(perfObj.Currency__c==null || perfObj.Currency__c==''){perfObj.Currency__c='AUD';}
 
 Double currencyExchangeRateValue1 = isoCodeCurrencyTypeObjMap.get(perfObj.Currency__c).ConversionRate;
 Double currencyExchangeRateValueAUD1 = isoCodeCurrencyTypeObjMap.get(perfObj.CurrencyIsoCode).ConversionRate;
           
            perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //record currency to USD
               if(perfObj.SOV_Type__c!='Performance' && perfObj.Opportunity_Currency__c!=null){
                Double currencyExchangeRateValueAUD = isoCodeCurrencyTypeObjMap.get(perfObj.Opportunity_Currency__c).ConversionRate;

                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up_Opp__c!=null)?(((perfObj.Q1_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance_Opp__c!=null)?(((perfObj.Q1_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up_Opp__c!=null)?(((perfObj.Q2_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance_Opp__c!=null)?(((perfObj.Q2_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up_Opp__c!=null)?(((perfObj.Q3_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance_Opp__c!=null)?(((perfObj.Q3_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up_Opp__c!=null)?(((perfObj.Q4_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance_Opp__c!=null)?(((perfObj.Q4_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

               }else{
               
               system.debug('=====perfObj.Q2_Individual_Performance__c===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up__c!=null)?(((perfObj.Q1_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance__c!=null)?(((perfObj.Q1_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up__c!=null)?(((perfObj.Q2_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance__c!=null)?(((perfObj.Q2_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up__c!=null)?(((perfObj.Q3_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance__c!=null)?(((perfObj.Q3_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up__c!=null)?(((perfObj.Q4_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance__c!=null)?(((perfObj.Q4_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
 system.debug('=====perfObj.Q2_Individual_Performance__c222222===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Target__c=(perfObj.Q1_Target__c!=null)?(((perfObj.Q1_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q2_Target__c=(perfObj.Q2_Target__c!=null)?(((perfObj.Q2_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q3_Target__c=(perfObj.Q3_Target__c!=null)?(((perfObj.Q3_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q4_Target__c=(perfObj.Q4_Target__c!=null)?(((perfObj.Q4_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
         

            }
  }
 List<Performance_Tracking__c> finalList=new List<Performance_Tracking__c>();
 for(Performance_Tracking__c perfObj:perfList){
    if(!trigger.newmap.containskey(perfObj.Id)){
        finalList.add(perfObj);
    }
    
    
 }
if(finalList.size()>0){update finalList; }
  
            
 }
public void updateCurrencybeforeInsert(List<Performance_Tracking__c> newSOVPerf){
    try{
 //List<CurrencyType> currencyTypeList = new List<CurrencyType>();
 Map<String,CurrencyType> isoCodeCurrencyTypeObjMap = new Map<String,CurrencyType>();
 //currencyTypeList = [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType];
                        
 for(CurrencyType currencyTypeObj : [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType]){
 isoCodeCurrencyTypeObjMap.put(currencyTypeObj.IsoCode,currencyTypeObj);
 }
 
 Set<Id> employeeIdSet=new Set<Id>();
 Map<String,Performance_Tracking__c> perfMap=new  Map<String,Performance_Tracking__c>();
 List<Performance_Tracking__c> perfList=new List<Performance_Tracking__c>();
 for(Performance_Tracking__c  perfObj:newSOVPerf){
 //Double currencyExchangeRateValue = isoCodeCurrencyTypeObjMap.get(perfObj.CurrencyIsoCode).ConversionRate;
 if(perfObj.Currency__c==null || perfObj.Currency__c==''){perfObj.Currency__c='AUD';}
 
 Double currencyExchangeRateValue1 = isoCodeCurrencyTypeObjMap.get(perfObj.Currency__c).ConversionRate;
 Double currencyExchangeRateValueAUD1 = isoCodeCurrencyTypeObjMap.get(perfObj.CurrencyIsoCode).ConversionRate;
            system.debug('====perfObj.Currency__c'+perfObj.Currency__c);
            perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //perfObj.CurrencyIsoCode=perfObj.Currency__c;
            //record currency to USD
               if(perfObj.SOV_Type__c!='Performance' && perfObj.Opportunity_Currency__c!=null){
                Double currencyExchangeRateValueAUD = isoCodeCurrencyTypeObjMap.get(perfObj.Opportunity_Currency__c).ConversionRate;

                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up_Opp__c!=null)?(((perfObj.Q1_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance_Opp__c!=null)?(((perfObj.Q1_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up_Opp__c!=null)?(((perfObj.Q2_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance_Opp__c!=null)?(((perfObj.Q2_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up_Opp__c!=null)?(((perfObj.Q3_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance_Opp__c!=null)?(((perfObj.Q3_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up_Opp__c!=null)?(((perfObj.Q4_Rolled_Up_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance_Opp__c!=null)?(((perfObj.Q4_Individual_Performance_Opp__c/currencyExchangeRateValueAUD)) * currencyExchangeRateValue1):0;

               }else{
               
               system.debug('=====perfObj.Q2_Individual_Performance__c===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Rolled_Up__c=(perfObj.Q1_Rolled_Up__c!=null)?(((perfObj.Q1_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q1_Individual_Performance__c=(perfObj.Q1_Individual_Performance__c!=null)?(((perfObj.Q1_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Rolled_Up__c=(perfObj.Q2_Rolled_Up__c!=null)?(((perfObj.Q2_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q2_Individual_Performance__c=(perfObj.Q2_Individual_Performance__c!=null)?(((perfObj.Q2_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q3_Rolled_Up__c=(perfObj.Q3_Rolled_Up__c!=null)?(((perfObj.Q3_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                
                perfObj.Q3_Individual_Performance__c=(perfObj.Q3_Individual_Performance__c!=null)?(((perfObj.Q3_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;

                perfObj.Q4_Rolled_Up__c=(perfObj.Q4_Rolled_Up__c!=null)?(((perfObj.Q4_Rolled_Up__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
                perfObj.Q4_Individual_Performance__c=(perfObj.Q4_Individual_Performance__c!=null)?(((perfObj.Q4_Individual_Performance__c/currencyExchangeRateValueAUD1)) * currencyExchangeRateValue1):0;
 system.debug('=====perfObj.Q2_Individual_Performance__c222222===='+perfObj.Q2_Individual_Performance__c);
               
                perfObj.Q1_Target__c=(perfObj.Q1_Target__c!=null)?(((perfObj.Q1_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q2_Target__c=(perfObj.Q2_Target__c!=null)?(((perfObj.Q2_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q3_Target__c=(perfObj.Q3_Target__c!=null)?(((perfObj.Q3_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
                perfObj.Q4_Target__c=(perfObj.Q4_Target__c!=null)?(((perfObj.Q4_Target__c/currencyExchangeRateValueAUD1 )) * currencyExchangeRateValue1):0;
         

            }
            
            
 
 }
 
 
            
        }catch(system.Exception e){
        
            ErrorHandlerException.ExecutingClassName='AllSOVPerfTrackingCurrencyCheckHandler:updateCurrencybeforeInsert';
            ErrorHandlerException.objectList=newSOVPerf;
            ErrorHandlerException.sendException(e);
        
     //  for (Integer i = 0; i < e.; i++) {
        // Process exception here 
        System.debug(e.getCause()+','+e.getMessage()+','+e.getLineNumber()); 
      // }
        
    } 
        }
}