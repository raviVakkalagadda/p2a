@isTest(SeeAllData=false) 
public class AllServiceLineItemTriggerTest
 {
   Public static testMethod void AllServiceLineItemTriggertest1() 
    {
    
    P2A_TestFactoryCls.sampleTestData();
    
         AllServiceLineItemTriggerHandler serLineItemTrgHdlr = new AllServiceLineItemTriggerHandler();
         Map<Id,csord__Service_Line_Item__c> serLineItemMap = new Map<Id,csord__Service_Line_Item__c>();
         
         Test.StartTest();
         serLineItemTrgHdlr.isDisabled();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
         
         
         for(csord__Service_Line_Item__c servLineItemObj : serLineItemList) {
             serLineItemMap.put(servLineItemObj.Id,servLineItemObj);
         }
                
         serLineItemTrgHdlr.afterInsert(serLineItemList, serLineItemMap);
         serLineItemTrgHdlr.beforeUpdate(serLineItemList, serLineItemMap, serLineItemMap);
         serLineItemTrgHdlr.beforeInsert(serLineItemList);
         system.assert(serLineItemList!=null);
         Test.StopTest(); 
     
    }
        
    Public static testMethod void AllServiceLineItemTriggertest2() 
    {
    
    P2A_TestFactoryCls.sampleTestData();
    
         AllServiceLineItemTriggerHandler serLineItemTrgHdlr = new AllServiceLineItemTriggerHandler();
         Map<Id,csord__Service_Line_Item__c> serLineItemMap = new Map<Id,csord__Service_Line_Item__c>();
         
         Test.StartTest();
         serLineItemTrgHdlr.isDisabled();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
         
         List<Country_Lookup__c> countryList = P2A_TestFactoryCls.getcountry(1);
         List<Contact> contactList = P2A_TestFactoryCls.getContact(1, accList);
         List<Site__c> SitesList = P2A_TestFactoryCls.getsites(1, accList, countrylist);
         List<BillProfile__c> billProfiles = P2A_TestFactoryCls.getBPs(1, accList, SitesList, contactList);
         
         serList[0].Billing_Commencement_Date__c = System.Today(); 
         serList[0].Stop_Billing_Date__c = System.Today(); 
         update serList;
         
         //serLineItemList[0].Billing_Commencement_Date__c = System.Today(); 
         //serLineItemList[0].Billing_ETC_End_Date__c = System.Today(); 
         serLineItemList[0].MISCService__c = serList[0].name;
         serLineItemList[0].Display_Line_Item_On_Invoice_Text__c = serList[0].name;
         
         
         serLineItemList[0].bill_profile__c = billProfiles[0].id;
         serLineItemList[0].Frequency_flag__c = false;
         serLineItemList[0].Is_ETC_Line_Item__c = true;
         update serLineItemList;
         
         system.assert(serLineItemList[0].bill_profile__c != null);
         //system.assert(false, serLineItemList[0].Frequency_flag__c);
         //system.assert(serList[0], serLineItemList[0].csord__service__c);
         
         for(csord__Service_Line_Item__c servLineItemObj : serLineItemList) {
             serLineItemMap.put(servLineItemObj.Id,servLineItemObj);
         }
                
         serLineItemTrgHdlr.afterInsert(serLineItemList, serLineItemMap);
         serLineItemTrgHdlr.beforeUpdate(serLineItemList, serLineItemMap, serLineItemMap);
         serLineItemTrgHdlr.beforeInsert(serLineItemList);
         system.assert(serLineItemList!=null);
         Test.StopTest(); 
     
    }
 }