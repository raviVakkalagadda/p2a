public with sharing class AllSiteTriggerHandler extends BaseTriggerHandler{
    
    
    public override void beforeInsert(List<SObject> newSites){
         UpdateGoeProperties((List<Site__c>) newSites,null);    
    }
    
    public override void afterInsert(List<SObject> newSites, Map<Id, SObject> newSitesMap) {
        
    }
    
    public override void beforeUpdate(List<SObject> newSites, Map<Id,SObject> newSitesMap, Map<Id,SObject> oldSitesMap){
      SiteBeforeUpdate((List<Site__c>) newSites, (Map<Id,Site__c>) oldSitesMap);
      UpdateGoeProperties((List<Site__c>) newSites, (Map<Id,Site__c>) oldSitesMap);
    }
    
    public override void afterUpdate(List<SObject> newSites, Map<Id, SObject> newSitesMap, Map<Id, SObject>  oldSitesMap){
        UpdateBillprofile((List<Site__c>) newSites, (Map<Id,Site__c>) oldSitesMap);
    }


/**
Trigger Logic of SiteBeforeUpdate has been moved into this method as a part of Tech Debt Changes to merge multiple triggers into one
Date:27-Sept-2017
Developer:Ritesh


@author - Accenture
@date - 28-JUNE-2012
@description -1) This Trigger is used to update the Is Updated flag when Site changes are sent to TIBCO.
              2) For CR#152 - This trigger is used to update the "Is Attached to Bill Profile" flag if site is associated to any Bill Profile.          
               CR# - 152 
*/ 
public static void SiteBeforeUpdate(List<Site__c> newSites, Map<Id,Site__c> oldSitesMap){
    if(Util.muteAllTriggers()) return;
     
    List<Id> siteList = new List<Id>(); //part of CR 414
    List<BillProfile__c> billList = new List<BillProfile__c>();//part of CR 414
    
    for(Site__c s:newSites){
    
       siteList.add(s.Id); // part of CR 414
      if (s.valid__c==true && oldSitesMap.get(s.Id).valid__c== true && s.isOppUpdate__c==true && oldSitesMap.get(s.id).isOppUpdate__c==true){
             s.Is_updated__c = true;
        }
        
     }
       
      // Checking if site is associated to bill profile or not.
          
      integer i = [Select count() from BillProfile__c where Bill_Profile_Site__c IN: siteList];      
      For(Site__c st : newSites){
        if (i > 0 ){            
        // if i is greater than 0 that means Bill Profile is associated to Site so here we are updating the Flag as True           
        st.Is_Attached_to_Bill_Profile__c = true;            
        system.debug('\n\n@@ [1] if part :Attached Bill Profile : '+st.Is_Attached_to_Bill_Profile__c);       
        }        
        else{            
        st.Is_Attached_to_Bill_Profile__c = false;            
        system.debug('\n\n@@ [1] else part :Attached Bill Profile : '+st.Is_Attached_to_Bill_Profile__c);        
        }    
    }
    
    // CR 414 code starts here
    billList = [Select Id, Name, Company__c,Status__c,Bill_Profile_Site__c from BillProfile__c where Bill_Profile_Site__c in :siteList];
    For(Site__c site : newSites){
       for(BillProfile__c bp:billList){
           if(bp.Company__c == Null && bp.Status__c =='Active'){
             site.adderror(System.label.Site_BP_Error);
           }
       }
    }
    // CR 414 Code ends here
}

/**
    @author - Accenture
    @date - 04-MAY-2012
    @version - 1.0
    @description - This is trigger to update the Bill profile's "Last_Address_Change_On_Site__c" field
                   when the Site address fields gets updated.
*/

public static void UpdateBillprofile(List<Site__c> newSites, Map<Id,Site__c> oldSitesMap){
    if(Util.muteAllTriggers()) return;
      List<Site__c> siteList = new List<Site__c>();
   
      for (Site__c newSite:newSites)
      {
         //Getting the previous details of the site object      
         Site__c oldSite = oldSitesMap.get(newSite.id);
         
         //compares the old and new values of the site object and fetches the updated site lists
         if( (oldSite.Address1__c != newSite.Address1__c)||(oldSite.Address2__c!=newSite.Address2__c)||(oldSite.Address_Type__c!=newSite.Address_Type__c)||(oldSite.City_name__c!=newSite.City_name__c)||(oldSite.Country_Formula__c!= newSite.Country_Formula__c)||(oldSite.State__c!= newSite.State__c)||(oldSite.PostalCode__c!= newSite.PostalCode__c)||(oldSite.Org_code__c!= newSite.Org_code__c) || (oldSite.Site_Code__c!= newSite.Site_Code__c) || (oldSite.Primary_Use__c!= newSite.Primary_Use__c)|| (oldSite.Name !=newSite.Name)||(oldSite.Address_3__c !=newSite.Address_3__c)||(oldSite.Address_4__c != newSite.Address_4__c))
         {           
            siteList.add(newSite);
         }                                        
                
      }
      
      //Fetching the bill profiles related to the updated site
      if(siteList.size()>0){
      List<BillProfile__c> bpList = [SELECT Id,Last_Address_Change_On_Site__c,site_updated_time__c FROM BillProfile__c WHERE Bill_Profile_Site__c  IN:siteList];
      
      system.debug('BillProfile List:****' + bpList );
      for( BillProfile__c b: bpList)
      {
        //Updating the Bill Profile date field
        b.Last_Address_Change_On_Site__c = date.today();
        b.site_updated_time__c = datetime.now();
       system.debug('DATE Field******:'+  b.Last_Address_Change_On_Site__c);
      }
      if(bpList.size()>0)update bpList;
      }
}

/**
    @author - Accenture
    @date - 28-April-2012
    @version - 1.0
    @description - This trigger updates the Google properties in site Object .
*/

public static void UpdateGoeProperties(List<Site__c> newSites, Map<Id,Site__c> oldSitesMap){
  if(Util.muteAllTriggers()) return;
  system.debug(Logginglevel.ERROR,'UpdateGoeProperties geo updates');      
  List <String> sitesToUpdate = new List<String>();
  
  // When new value is updated
  for(Site__c theSite: newSites)
  {
    boolean needsUpdate = false;
    if(oldSitesMap==null)//incase of insert trigger
    {
      needsUpdate = true;
      theSite.latitude__c = '0.000';
      theSite.longitude__c ='0.000';
    }
    else {
      // getting the value for Old ID   
      Site__c beforeUpdate = oldSitesMap.get(theSite.Id);
      needsUpdate = (
       (beforeUpdate.City_Finder__c != theSite.City_Finder__c) ||
       (beforeUpdate.State__c != theSite.State__c) ||
       (beforeUpdate.Country_Finder__c != theSite.Country_Finder__c)
      );
    }
    // If it is true, then update the new ID
    if(needsUpdate)
    {
      sitesToUpdate.add(theSite.Id);      
    } 
  }
  // Bulk Update
  if(sitesToUpdate.size() >0 )
  {
    GeoUtilities.updateSiteGeo(sitesToUpdate);
  }
}


}