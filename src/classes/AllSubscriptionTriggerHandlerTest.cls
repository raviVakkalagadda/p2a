@istest(seealldata = false)
public class AllSubscriptionTriggerHandlerTest{

/*@testSetUp static void setUp(){
    
            P2A_TestFactoryCls.sampletestdata();
            Map<Id,csord__Subscription__c> newsub = new Map<Id,csord__Subscription__c>();

            List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
            List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
            List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
            List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
            List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
            List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
            List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
            List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
            list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
            List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
            List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
            List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
            List<User> users = P2A_TestFactoryCls.get_Users(1);
            List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
            list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
            List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists = P2A_TestFactoryCls.getOrchestrationProcess(1);
            List<CSPOFA__Orchestration_Process__c>  orc = P2A_TestFactoryCls.getOrchestrationProcesss(1, ProcessTemplatesLists);
            List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList , SUBList);

}*/
   
   //first test method
   @isTest static void testUpdateOrders(){
     
     List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
     List<Opportunity> oppo= P2A_TestFactoryCls.getOpportunitys(1, accList);
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
     List<csord__Subscription__c> replSub = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
     List<csord__Order__c> Ord = P2A_TestFactoryCls.getorder(1,OrdReqList);
     
     List<CSPOFA__Orchestration_Process_Template__c> TempLists = P2A_TestFactoryCls.getOrchestrationProcess(1);  
     CSPOFA__Orchestration_Process_Template__c templ=TempLists[0];
     List<csord__Service__c> servList= P2A_TestFactoryCls.getService(1, OrdReqList , replSub);
     List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
     csord__service__c service=servList[0];
     service.csord__order__c=Orders[0].id;
     service.Opportunity__c=oppo[0].id;
     service.product_id__c='LLOOP';
     update service;
     
     List<csord__Service__c> ser = [Select id,name,product_id__c from csord__Service__c where product_id__c='LLOOP' and id =: servList[0].id];
     
     system.assertequals(servList[0].product_id__c,ser[0].product_id__c);
     system.assert(servList!=null);
     
     List<csord__Subscription__c> subinsert=new List<csord__Subscription__c>();
     for(integer i=0;i<2;i++){
         csord__Subscription__c sub=new csord__Subscription__c();
         sub.Name ='Test'; 
         sub.csord__Identification__c = 'Test-Catlyne-4238362';
         sub.csord__Order_Request__c = OrdReqList[0].id;
         sub.OrderType__c ='Parallel Upgrade';
         sub.HasOffnetService__c=true;
         sub.csord__order__c=ord[0].id;
         sub.csordtelcoa__Replaced_Subscription__c=replSub[0].id;
         //sub.Is_Inflight_Order__c=false;
         subinsert.add(sub);
     }
     
     
     test.starttest();
     insert subinsert;
     //subinsert[0].OpportunityRelatedList__c = oppo[0].id; 
     system.assertEquals(true,subinsert[0].csordtelcoa__Replaced_Subscription__c!=null);
     AllSubscriptionTriggerHandler alls=new AllSubscriptionTriggerHandler ();
     alls.getMaxRecursion();
     alls.isDisabled();
     alls.updateOrders(subinsert,null);
     //alls.setOpportunityAndAccountID(subinsert);
     alls.addOrderDetails(subinsert);
     List<CSPOFA__Orchestration_Process__c>pro=alls.newProcesses(templ,subinsert);
     CSPOFA__Orchestration_Process_Template__c tm=alls.findProcessTemplateByName(templ.name);
     test.stoptest();    
       
   }    
   
   //second test method
   @istest static void testsetOrdertype(){
     
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
      List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
      List<csord__Subscription__c> subList= P2A_TestFactoryCls.getSubscription(1,OrdReqList);       
      List<csord__Service__c> insertService=new List<csord__Service__c>();
     
      
      
      CSPOFA__Orchestration_Process_Template__c ott=new CSPOFA__Orchestration_Process_Template__c();
                ott.name='order_new';
                insert ott;
                
      csord__Order__c ord=Orders[0];
      ord.Full_Termination__c=true;
      update ord;
      
      List<CSPOFA__Orchestration_Process_Template__c>otList=new List<CSPOFA__Orchestration_Process_Template__c>();
       Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2; 
            Map<id,csord__Subscription__c>subMap=new Map<id,csord__Subscription__c>();  
            for(Integer i=0; i < 2; i++){              

                CSPOFA__Orchestration_Process_Template__c ot=new CSPOFA__Orchestration_Process_Template__c();
                ot.name='servicenew';
                otList.add(ot);
                
                
                csord__Service__c ser = new csord__Service__c();
                ser.product_id__c = 'IPVPN'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c = i<OrdReqList.size() ? OrdReqList[i].id : OrdReqList[0].id;
                ser.csord__Subscription__c = i<SUBList.size() ? SUBList[i].id : SUBList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = ''; 
                ser.Cease_Service_Flag__c=true;
                ser.csord__order__c=ord.id;
                ser.csordtelcoa__Product_Configuration__c=null;

                if(i<SUBList.size()){
                    subMap.put(SUBList[i].id,SUBList[i]);
                }               

                insertService.add(ser);
            }
            
            try{insert insertService;}
            catch(exception e){
                ErrorHandlerException.ExecutingClassName='AllCaseTriggerHandler :After Insert';         
                ErrorHandlerException.sendException(e);
            }  
            system.assert(insertService!=null);
            List<csord__Service__c> testser = [select id,name,product_id__c from csord__Service__c where product_id__c='IPVPN' Limit 1];
            //insert otList;
            system.assertequals(insertService[0].product_id__c,testser[0].product_id__c);
      test.starttest();
      AllSubscriptionTriggerHandler alls=new AllSubscriptionTriggerHandler ();
      //alls.setOrderType(subList,insertService);
      alls.afterInsert(subList,subMap);   
      test.stoptest();
   }

   //third test method
    @istest static void testsetOrdertype1(){
     
      List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
      List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
      List<csord__Subscription__c> subList= P2A_TestFactoryCls.getSubscription(1,OrdReqList);       
      List<csord__Service__c> insertService=new List<csord__Service__c>();
     
      
      CSPOFA__Orchestration_Process_Template__c ott=new CSPOFA__Orchestration_Process_Template__c();
                ott.name='order_new';
                insert ott;
                
      csord__Order__c ord=Orders[0];
      ord.Full_Termination__c=false;
      update ord;
      
      List<CSPOFA__Orchestration_Process_Template__c>otList=new List<CSPOFA__Orchestration_Process_Template__c>();
       Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2; 
            Map<id,csord__Subscription__c>subMap=new Map<id,csord__Subscription__c>();  
            for(Integer i=0; i < 2; i++){              

                CSPOFA__Orchestration_Process_Template__c ot=new CSPOFA__Orchestration_Process_Template__c();
                ot.name='servicenew';
                otList.add(ot);
                
                
                csord__Service__c ser = new csord__Service__c();
                ser.product_id__c = 'IPVPN'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c = OrdReqList[0].id;
                ser.csord__Subscription__c = SUBList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = ''; 
                ser.Cease_Service_Flag__c=false;
                ser.csord__order__c=ord.id;
                ser.csordtelcoa__Product_Configuration__c=null;

                if(i<SUBList.size()){
                    subMap.put(SUBList[0].id,SUBList[0]);
                }               

                insertService.add(ser);
                
            }
            //insert otList;
            system.assert(insertService!=null);
            
            try{insert insertService;
                List<csord__Service__c> testser = [select id,name,product_id__c from csord__Service__c where product_id__c='IPVPN' and id =: insertService[0].id];
                system.assertequals(insertService[0].product_id__c,testser[0].product_id__c);
                }
            
            catch(exception e){
                ErrorHandlerException.ExecutingClassName='AllCaseTriggerHandler :After Insert';         
                ErrorHandlerException.sendException(e);
            } 
            
      test.starttest();
      AllSubscriptionTriggerHandler alls=new AllSubscriptionTriggerHandler ();
      //alls.setOrderType(subList,insertService);
      alls.afterInsert(subList,subMap);
      alls.changeOrchestrationProcessTemplate(subList);
      test.stoptest();    
   }
}