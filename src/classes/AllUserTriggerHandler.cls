public class AllUserTriggerHandler extends BaseTriggerHandler{

        public override void afterInsert(List<SObject> usrLst, Map<Id, SObject> newUserMap){
            AssignGroupMethod((List<User>)usrLst, (Map<ID, User>)newUserMap, null);
            CloneUserToAgentMethod((List<User>)usrLst, (Map<ID, User>)newUserMap, null);
        }
        
        
        public override void afterUpdate(List<SObject> usrLst, Map<Id, SObject> newUserMap, Map<Id, SObject> oldUserMap) {         
             AssignGroupMethod((List<User>)usrLst, (Map<ID, User>)newUserMap, (Map<ID, User>)oldUserMap); 
             CloneUserToAgentMethod((List<User>)usrLst, (Map<ID, User>)newUserMap, (Map<ID, User>)oldUserMap);
         }
         
    private void AssignGroupMethod(List<User> usrLst, Map<ID, User> newUserMap, Map<ID, User> oldUserMap){
            //List<User> usrLst = Trigger.new; // New User List
        //Map<ID, User> newUserMap = Trigger.newMap; // New User Map
        //Map<ID, User> oldUserMap = Trigger.oldMap; // Old User Map
        
        Map<String, ID> GrpMap = new Map<String, ID>(); // Map for Group name, Group ID
        
        List<User_Group_Mapper__c> usrGrpMprLst = [select Group_Name__c, Profile_Name__c, User_Region__c from User_Group_Mapper__c]; //Custom Object to store the User Profile, Region to a Group
        
        List<Group> publicGrpLst = [select id, Name from Group where Type = 'Regular']; // Select all the common group list
        
        for(Group grp : publicGrpLst)
            GrpMap.put(grp.name, grp.id);
            
        if(oldUserMap == null){ // If a new User is added 
            List<GroupMember> grpMbrLst = new List<GroupMember>();
            for(User newUsr : usrLst){
                Set<String> grpNameSet = new Set<String>();
                for(User_Group_Mapper__c grpMapr : usrGrpMprLst) // Loop through the Group Mapper records
                    //if(grpMapr.Profile_Name__c == newUsr.Profile_Name__c && grpMapr.User_Region__c == newUsr.Region__c)
                    if(grpMapr.Profile_Name__c == newUsr.Profile_Name__c) // If the User profile matches, then add the user into the Group
                        grpNameSet.add(grpMapr.Group_Name__c);
                for(String grpNam : grpNameSet){
                    if(GrpMap.containsKey(grpNam))
                        grpMbrLst.add(new GroupMember(UserOrGroupId = newUsr.id, GroupId = GrpMap.get(grpNam))); //Add the user into the Group
                }
            }
            try{
                if(!grpMbrLst.isEmpty()) // Insert
                    insert grpMbrLst;
            }catch (DmlException e) {
                //System.debug('Error while assigning a group to a new User '+ e.getMessage());
                CreateApexErrorLog.InsertHandleException('ApexTrigger','AssignGroup',e.getMessage(),'GroupMember',Userinfo.getName());
                e.setMessage('Error while assigning a group to a new User');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
        
        else{ // If User is updated
            List<GroupMember> oldGrpMbrLst = [select id, GroupId, UserOrGroupId from GroupMember where UserOrGroupId in :newUserMap.keySet() and GroupId in :GrpMap.values()]; // Query old Group mappings
            List<GroupMember> delGrpMbrLst = new List<GroupMember>(); //List to delete the User Group mappings
            List<GroupMember> newGrpMbrLst = new List<GroupMember>(); //List to add the new User Group mappings
            for(User Usr : usrLst){ // Loop through the user list
               // if(usr.Profile_Name__c != oldUserMap.get(usr.id).Profile_Name__c || usr.Region__c != oldUserMap.get(usr.id).Region__c){
                if(usr.Profile_Name__c != oldUserMap.get(usr.id).Profile_Name__c){ //If the User Profile changes
                    for(GroupMember mbr : oldGrpMbrLst)
                        if(mbr.UserOrGroupId == usr.id) // Delete the User Group mapping, If user is already in a Group
                                    delGrpMbrLst.add(mbr);
                    
                    
                Set<String> grpNameSet = new Set<String>();
                for(User_Group_Mapper__c grpMapr : usrGrpMprLst) // Add the user into new Group
                    //if(grpMapr.Profile_Name__c == Usr.Profile_Name__c && grpMapr.User_Region__c == Usr.Region__c)
                    if(grpMapr.Profile_Name__c == Usr.Profile_Name__c)
                        grpNameSet.add(grpMapr.Group_Name__c);
                        
                for(String grpNam : grpNameSet){
                    if(GrpMap.containsKey(grpNam))
                        newGrpMbrLst.add(new GroupMember(UserOrGroupId = Usr.id, GroupId = GrpMap.get(grpNam)));
                }
            
                }
            }
            try{
                if(!delGrpMbrLst.isEmpty()) // delete the existing user Group mapping
                    delete delGrpMbrLst;
                if(!newGrpMbrLst.isEmpty())
                    insert newGrpMbrLst; // Create new User Group mappings
            }catch (DmlException e) {
                //System.debug('Error while assigning a group to an existing User '+ e.getMessage());
                CreateApexErrorLog.InsertHandleException('ApexTrigger','AssignGroup',e.getMessage(),'GroupMember',Userinfo.getName());
                e.setMessage('Error while assigning a group to an existing User');
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
            }   
        
        }
        
        private void CloneUserToAgentMethod(List<User> usrLst, Map<ID, User> newUserMap, Map<ID, User> oldUserMap){
            if(Util.muteAllTriggers()) return;
            
            Set<String> eligibleUserIds = new Set<String>();
            //Loop through the records and add into Agent object if User profile is Sales and Employee number is not NULL
            for(User u:usrLst){
                //System.debug(' User Profile '+u.Profile_Name__c);
                if( (u.Profile_Name__c == 'TI Account Manager' || 
                u.Profile_Name__c == 'TI Sales Manager'  ||
                u.Profile_Name__c == 'TI Sales Admin' ||
                u.Profile_Name__c == 'TI Billing' ) &&  u.EmployeeNumber!= null){
                    if (oldUserMap == null && u.isActive == True ) // create new Agent if the user is active
                        eligibleUserIds.add(u.Id);
                    else if(oldUserMap != null) // update Agent record
                        eligibleUserIds.add(u.Id);
                }
            }
            if(eligibleUserIds.size() > 0){
                CommonMapperController cm = new CommonMapperController();
                CommonMapperController.setUserAgentMapperDetails('User', 'Agent__c', eligibleUserIds);
            }
             //On ticking of SIP User checkbox,blank records should be created on SOV Performance Tracking object:IR171
         
         
        List<Period> periodList=[select Type,StartDate,EndDate from Period where Type='Year'  Order by StartDate ASC];
        Map<Id,Date> mapUserIdPeriod=new Map<Id,Date>();
        Map<Id,Date> mapUserIdPeriodE=new Map<Id,Date>();
        Set<String> fiscalyrSet=new Set<String>();
        String FYear='';
           for(User usrObj:usrLst){
            if(usrObj.isActive){
            for(Period periodObj:periodList){
                if(((oldUserMap!=null && oldUserMap.containskey(usrObj.Id) && oldUserMap.get(usrObj.Id).SIP_User__c!= usrObj.SIP_User__c && !oldUserMap.get(usrObj.Id).SIP_User__c && usrObj.SIP_User__c ) || (oldUserMap == null && usrObj.SIP_User__c)) && usrObj.LastModifiedDate>=periodObj.StartDate && usrObj.LastModifiedDate<=periodObj.EndDate){
                   FYear=periodObj.StartDate.year()+'-'+periodObj.EndDate.year();
                    mapUserIdPeriod.put(usrObj.id,periodObj.StartDate);
                    mapUserIdPeriodE.put(usrObj.id,periodObj.EndDate);
                    fiscalyrSet.add(FYear);
                           
            }   
            } 
            }
           }
           if(fiscalyrSet!=null && fiscalyrSet.size()>0){
           CommonMapperController.createSOVPerformanceTrackingforSIPUser(fiscalYrSet,mapUserIdPeriod,mapUserIdPeriodE,newUserMap.keyset());
           }

                    
    }
         
}