@isTest(SeeAllData=true)
private class ApproveCaseCtrlTest{
    
    public static cscfga__Product_Basket__c basket = null;
    
    public static list<Account> accList = P2A_TestFactoryCls.getAccounts(1);   
    public static list<User> usrList = P2A_TestFactoryCls.get_Users(1);  
    public static user usr = usrList[0];      
    Public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
    public static List<cscfga__Product_Basket__c> basketList =  P2A_TestFactoryCls.getProductBasket(1);
    public static List<csord__Order_Request__c> ordReqList =  P2A_TestFactoryCls.getorderrequest(1);
    public static List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1, OrdReqList);
    public static List<cscfga__Product_Bundle__c> bundlist=  P2A_TestFactoryCls.getProductBundleHdlr(1,oppList);
    public static List<cscfga__Product_Definition__c> prodList = P2A_TestFactoryCls.getProductdef(1);
    public static List<case> caseList= P2A_TestFactoryCls.getcases(1,AccList,oppList,basketList,ordList,usrList);  
    public static List<cscfga__Configuration_Offer__c> ofrList = P2A_TestFactoryCls.getOffers(1);
    private static List<cscfga__Product_Configuration__c> prodConfigs = P2A_TestFactoryCls.getProductonfig(1,basketList,prodList,bundlist,ofrList );
    public static case upCase = caseList[0];
    public static List<CurrencyValue__c> currVal=  P2A_TestFactoryCls.getCurVal();
    public static List<Pricing_Approval_Request_Data__c> preqdata = P2A_TestFactoryCls.getProductonfigappreq(1,basketList, prodConfigs);
    
    
    Public Static testMethod void ApproveCaseCtrlTest_Method1(){ 
        List<Profile> Profiles_Id = [Select id, Name from Profile Where id =:userinfo.getProfileid() and (Name = 'TI Commercial' Or Name = 'System Administrator') Limit 1];
        System.assertequals(Profiles_Id[0].Name,'System Administrator');
        
        User u = new User(Alias = 'standt', Email='standarduser1233@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profiles_Id[0].id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123as4561234789@testorg.com',EmployeeNumber='123');
        insert u;
        System.assert(U.id!=null);
       
       //system.runas(u);
    
   /* case casobj = new case();
        Casobj.Product_Basket__c = Products[0].id;
        Casobj.Order_type__c='Renewal';
        insert casobj;
        System.assert(Casobj != null)
    
    Pricing_Approval_Request_Data__c Par_apps = new Pricing_Approval_Request_Data__c();
        Par_apps.Product_basket__c = casobj.Product_Basket__c ;
        Par_apps.Product_Configuration__c = prodConfigs[0].id;
        Par_apps.Rate_RC__c = 123;
        Par_apps.Rate_NRC__c = 123 ;
        Par_apps.Offer_RC__c = 123;
        //Par_apps.Last_Approved_NRC__c = 456;
        Par_apps.Is_Offnet__c = true ;
        Par_apps.Product_Information__c = 'Code coverage';
        Par_apps.Contract_Term__c = 12;
        Par_apps.Rate_Burst_Rate__c = 234;
        Par_apps.Rate_China_Burst_Rate__c = 234;
        Par_apps.Target_Burst_Rate__c = 156;
        Par_apps.Target_China_Burst_Rate__c = 298;
        Par_apps.Approved_Burst_Rate__c = 234;
        Par_apps.Approved_China_Burst_Rate__c = 678;
        insert Par_apps ;
       System.assert(Par_apps.id != null);
       
    ApexPages.StandardController scon = new ApexPages.standardController(casobj);
    */
    Pricing_Role__c p= new Pricing_Role__c();   
        P.Role__c='Pricing ED';
        //P.Name=
        //P.Margin__c=
        //P.Revenue_TCV__c=
        //P.Term__c=
        //P.Owner=u.id;
        insert p;
     System.assert(p.id != null);    
    Pricing_Delegate__c p1= new Pricing_Delegate__c();          
        
        P1.Delegate_Start_Date__c=System.Today()+50;
        P1.Pricing_roles__c=p.id;       
       
        P1.Delegate_End_Date__c=System.Today()+198;
        
        insert p1;
     System.assert(p1.id != null);
    
    upCase.TCV__c = 1;   
    upCase.Approved_Offline__c = True;
    upCase.Approval_Status_Reason__c='Other';
    upsert upCase;
    ApproveCaseCtrl appCaseCtrl = new ApproveCaseCtrl(new ApexPages.StandardController(upCase));
    appCaseCtrl .displayPopup = true;
    appCaseCtrl .callfunct = 'sdfsd';
    
    
    system.RunAs(usr){appCaseCtrl.approveCase();} 
    }
    
    
    Public Static testMethod void ApproveCaseCtrlTest_Method2(){ 
    upCase.TCV__c = 1;  
    //upCase.status = 'approved';
    upCase.Approved_Offline__c = False;
    upCase.Approval_Status_Reason__c='Other';
    upsert upCase;
    system.assert(upCase!=null);
    ApproveCaseCtrl appCaseCtrl = new ApproveCaseCtrl(new ApexPages.StandardController(upCase));
    system.RunAs(usr){appCaseCtrl.approveCase();}  
    }    
    
     Public Static testMethod void ApproveCaseCtrlTest_Method3(){ 
        
        preqdata[0].Approved_NRC__c = null;
        preqdata[0].Approved_RC__c = null;
        preqdata[0].Cost_NRC__c = null;
        preqdata[0].Cost_RC__c = null;
        preqdata[0].Approved_Burst_Rate__c = null;
        preqdata[0].Approved_China_Burst_Rate__c = null;
        upsert preqdata[0];
    
    upCase.TCV__c = 0.02;  
    //upCase.status = 'approved';
    upCase.Approved_Offline__c = False;
    upCase.Approval_Status_Reason__c='Other';
    upCase.Margin_Value__c = 1.00;
    upsert upCase;
    system.assert(upCase!=null);
    
    
    List<Pricing_Role__c>Pinsert = New List<Pricing_Role__c>();
    Pricing_Role__c p= new Pricing_Role__c();   
    P.Role__c='Pricing ED';
    Pinsert.add(p); 
    
    Pricing_Role__c p1= new Pricing_Role__c();   
    P1.Role__c ='Pricing Director Group';
    Pinsert.add(p1); 
    
    Pricing_Role__c p2= new Pricing_Role__c();   
    P2.Role__c = 'Manager';
    Pinsert.add(p2); 
    
    Pricing_Role__c p3= new Pricing_Role__c();   
    P3.Role__c = 'Pricing Manager/Analyst';
    Pinsert.add(p3); 
    
    Pricing_Role__c p4= new Pricing_Role__c();   
    P4.Role__c = 'Global Price Desk Analyst';
    Pinsert.add(p4); 
    
    insert Pinsert;
    
    case casobj = new case();
    Casobj.Product_Basket__c = basketList[0].id;
    Casobj.Order_type__c='Renewal';
    Casobj.Margin_Value__c = 1.00;
    insert casobj;
    System.assert(Casobj != null);
    
    Pricing_Approval_Request_Data__c Par_apps = new Pricing_Approval_Request_Data__c();
    Par_apps.Product_basket__c = Casobj.Product_Basket__c;
    Par_apps.Product_Configuration__c = prodConfigs[0].id;
    Par_apps.Rate_RC__c = 123;
    Par_apps.Rate_NRC__c = 123 ;
    Par_apps.Offer_RC__c = 123;
    Par_apps.Last_Approved_NRC__c = 456;
    Par_apps.Is_Offnet__c = true ;
    Par_apps.Product_Information__c = 'Code coverage';
    Par_apps.Contract_Term__c = 12;
    Par_apps.Rate_Burst_Rate__c = 234;
    Par_apps.Rate_China_Burst_Rate__c = 234;
    Par_apps.Target_Burst_Rate__c = 156;
    Par_apps.Target_China_Burst_Rate__c = 298;
    Par_apps.Approved_Burst_Rate__c = 234;
    Par_apps.Approved_China_Burst_Rate__c = 678;
    insert Par_apps ;
    System.assert(Par_apps.id != null);
    
    
    Pricing_Delegate__c pRd1= new Pricing_Delegate__c();          
    pRd1.Delegate_Start_Date__c=System.Today()+5;
    pRd1.Pricing_roles__c=Pinsert[0].id;       
    pRd1.Delegate_End_Date__c=System.Today()+8;
    insert pRd1;
    
            
    PricingApprovalExtCr1 Ext = new PricingApprovalExtCr1(new ApexPages.StandardController(upCase));
    PricingApprovalExtCr1.PricingApprovalWrapper pricAppWrapObj = new PricingApprovalExtCr1.PricingApprovalWrapper(Par_apps,true);

    ApproveCaseCtrl appCaseCtrl = new ApproveCaseCtrl(new ApexPages.StandardController(upCase));
   system.RunAs(usr){appCaseCtrl.approveCase();}  
    
    }     
    @istest
    public static void approvalcasetest()
    {
        List<Account> acclist = new List<Account>();
    account a = new account();
    a.name    = 'Test Accenture ';
    a.BillingCountry  = 'GB';
    a.Activated__c  = True;
    a.Account_ID__c  = 'test';
    a.Account_Status__c = 'Active';
    a.Customer_Legal_Entity_Name__c = 'Test';
    a.Customer_Type_New__c = 'MNO';
    a.Region__c = 'Australia';
    a.Website='abc.com';
    a.Selling_Entity__c='Telstra Limited';
    a.Industry__c='BPO';
    a.Account_ORIG_ID_DM__c = 'test';
    acclist.add(a);
    insert acclist;
    system.assert(acclist[0].id!=null);  
     
     
     List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity(); 
        opp.AccountId = acclist[0].id;
        opp.name  = 'Test Opp ';
        opp.stagename = 'Identify & Define';
        opp.CloseDate= system.today()+5;
        opp.Pre_Contract_Provisioning_Required__c = 'yes';
        opp.QuoteStatus__c = 'draft';
        opp.Product_Type__c = 'IPL';
        opp.Estimated_MRC__c = 100.00;
        opp.Estimated_NRC__c = 100.00;
        opp.Opportunity_Type__c = 'Simple';
        opp.Sales_Status__c = 'Open';
        opp.Order_Type__c = 'New';
        opp.Stage__c='Identify & Define';
        opp.ContractTerm__c='10'; 
        opp.CreditCheckDoneBy__c= 'testname';
        opplist.add(opp);
        insert opplist ;
        system.assert(opplist[0].id!=null);
        
         
         List<cscfga__Product_Basket__c> ProductBasketlist = new List<cscfga__Product_Basket__c>();
          cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
                p.csordtelcoa__Synchronised_with_Opportunity__c = true;
                p.Name = 'New Basket' + system.now();
                p.csordtelcoa__Account__c = acclist[0].id;
                p.cscfga__Opportunity__c = opplist[0].id ;
                p.Quote_Status__c = 'Pending Pricing Approval';
                p.csordtelcoa__Change_Type__c = 'Upgrade';
                p.cscfga__total_contract_value__c = 34;
                p.csbb__Synchronised_With_Opportunity__c = true;
                ProductBasketlist.add(p);
                insert ProductBasketlist ;
            system.assert(ProductBasketlist[0].id!=null);  
        
     
       List<Group> groups = [select id, name from group where type = 'Queue' and Name IN  ('GDC-FS','GPD Asia ANZ')];
           case c = new case();
                c.Status ='Unassigned';
                c.ownerid = groups[0].id;
                c.Is_Feasibility_Request__c= true;
                c.Is_Resource_Reservation__c = true;
                c.TCV__c = 1; 
                c.Approved_Offline__c = False;
                c.Approval_Status_Reason__c='Other';
                c.CurrencyIsoCode = 'AUD';
                c.Approval_Status_Reason__c = 'Other';
                c.Approved_Offline__c = true;
                c.Product_Basket__c = ProductBasketlist[0].id;
                c.Margin_Value__c = 23;
                //c.Margin__c
                c.Confirmedcheck__c = true;
                c.Approval_Rejection_Date__c = System.today();
                 insert c ;
                
          List<cscfga__Product_Definition__c> ProductDeflist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c();
        pb.name = 'Master IPVPN Service'; 
        pb.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        ProductDeflist.add(pb);
        insert ProductDeflist ;
        
        
                
                  List<cscfga__Product_Configuration__c> ProductConfiglist = new List<cscfga__Product_Configuration__c>(); 
                cscfga__Product_Configuration__c pb10 = new cscfga__Product_Configuration__c();
                pb10.Name = 'Standalone ASBR';
                pb10.Product_Name__c = 'IPT';
                pb10.Approved_Offer_Price_MRC__c = 23;
                pb10.Approved_Offer_Price_NRC__c = 24;
                pb10.cscfga__Configuration_Status__c = 'Valid';
                
                pb10.cscfga__Product_Family__c ='Master IPVPN Service';
                pb10.Internet_Onnet_BillingType__c = 'Aggregated';
                pb10.cscfga__Product_Basket__c = ProductBasketlist[0].id;
                pb10.cscfga__Product_Definition__c = ProductDeflist[0].id;
                pb10.csordtelcoa__Ignore_For_Order_Decomposition__c = true;
                pb10.cscfga__Product_Family__c = 'Master IPVPN Service';
                pb10.Product_Code__c = 'ASBRB';
                ProductConfiglist.add(pb10);
                
                 cscfga__Product_Configuration__c pb11 = new cscfga__Product_Configuration__c();
                pb11.Name = 'Standalone ASBR';
                pb11.Product_Name__c = 'IPT';
                pb11.Approved_Offer_Price_MRC__c = 23;
                pb11.Approved_Offer_Price_NRC__c = 24;
                pb11.cscfga__Configuration_Status__c = 'Valid';
                pb11.cscfga__Root_Configuration__c  =ProductConfiglist[0].id; 
                pb11.cscfga__Product_Family__c ='Master IPVPN Service';
                pb11.Internet_Onnet_BillingType__c = 'Aggregated';
                pb11.cscfga__Product_Basket__c = ProductBasketlist[0].id;
                pb11.cscfga__Product_Definition__c = ProductDeflist[0].id;
                pb11.csordtelcoa__Ignore_For_Order_Decomposition__c = true;
                pb11.cscfga__Product_Family__c = 'Master IPVPN Service';
                pb11.Product_Code__c = 'ASBRB';
                ProductConfiglist.add(pb11);
                insert ProductConfiglist;
                
                
                
                List<Pricing_Approval_Request_Data__c> pard1 = new  List<Pricing_Approval_Request_Data__c>();
                Pricing_Approval_Request_Data__c pard = new Pricing_Approval_Request_Data__c();
                pard.Product_Basket__c = ProductBasketlist[0].id;
                pard.Product_Configuration__c = ProductConfiglist[0].id;
                pard.Actual_Configuration__c = 'testdata';
                pard.Approved_Configuration__c= 'testdata';
                
                pard.Rate_RC__c = 123;
                pard.Rate_NRC__c = 123 ;
                pard.Offer_RC__c = 123;
                pard.Approved_NRC__c = 23;
                pard.Approved_RC__c = 24;
                pard.Cost_NRC__c = 23;
                pard.Cost_RC__c = 34;
                pard.Last_Approved_NRC__c = 456;
                pard.Is_Offnet__c = true ;
                pard.Product_Information__c = 'Code coverage';
                pard.Contract_Term__c = 12;
                pard.Rate_Burst_Rate__c = 234;
                pard.Rate_China_Burst_Rate__c = 234;
                pard.Target_Burst_Rate__c = 156;
                pard.Target_China_Burst_Rate__c = 298;
                pard.Approved_Burst_Rate__c = null;
                pard.Approved_China_Burst_Rate__c = null;
                pard1.add(pard);
                insert pard1;
               /* Pricing_Approval_Request_Data__c pard2 = new Pricing_Approval_Request_Data__c();
                pard2.Product_Basket__c = ProductBasketlist[0].id;
                pard2.Product_Configuration__c = ProductConfiglist[0].id;
                pard2.Actual_Configuration__c = 'testdata';
                pard2.Approved_Configuration__c= 'testdata';
                pard2.Rate_RC__c = 123;
                pard2.Rate_NRC__c = 123 ;
                pard2.Offer_RC__c = 123;
                pard2.Approved_NRC__c = 23;
                pard2.Approved_RC__c = 24;
                pard2.Cost_NRC__c = 23;
                pard2.Cost_RC__c = 34;
                pard2.Last_Approved_NRC__c = 456;
                pard2.Is_Offnet__c = true ;
                pard2.Product_Information__c = 'Code coverage';
                pard2.Contract_Term__c = 12;
                pard2.Rate_Burst_Rate__c = 234;
                pard2.Rate_China_Burst_Rate__c = 234;
                pard2.Target_Burst_Rate__c = 156;
                pard2.Target_China_Burst_Rate__c = null;
                pard2.Approved_Burst_Rate__c = null;
                pard2.Approved_China_Burst_Rate__c = 678;
                pard1.add(pard2);*/
                
                
             List<Pricing_Role__c> plist = new List<Pricing_Role__c>();
                Pricing_Role__c prole= new Pricing_Role__c();
                prole.role__c='Pricing ED';
                //prole.Owner.id = UserInfo.getuserid();
                plist.add(prole);
                insert plist ; 
                
                List<Pricing_Delegate__c> pdlist = new List<Pricing_Delegate__c>();
               Pricing_Delegate__c pd= new Pricing_Delegate__c();
               pd.Pricing_Roles__c = plist[0].id;
               pd.Delegates__c= UserInfo.getuserid();
               pd.Delegate_Start_Date__c = System.today();
               pd.Delegate_End_Date__c = System.today();
                pdlist.add(pd);
                
                
                
                Test.starttest();
                
      PricingApprovalExtCr1 Ext = new PricingApprovalExtCr1(new ApexPages.StandardController(c));
    //PricingApprovalExtCr1.PricingApprovalWrapper pricAppWrapObj = new PricingApprovalExtCr1.PricingApprovalWrapper(pard1,true);

    ApproveCaseCtrl appCaseCtrl = new ApproveCaseCtrl(new ApexPages.StandardController(c));
    system.RunAs(usr)
    {
    appCaseCtrl.approveCase();
    }  
                   
                Test.stoptest();
    } 
      
}