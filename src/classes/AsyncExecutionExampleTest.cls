@isTest(SeeAllData=false)
public class AsyncExecutionExampleTest 
{
        public static testmethod void AsyncExecutionExampleTest(){
         List<Performance_Tracking__c> perfList1=new List<Performance_Tracking__c>();
             Performance_Tracking__c perfobj=null;
        
                perfobj=new Performance_Tracking__c();
               
                perfobj.Fiscal_year__c='2015-2016';
                perfobj.FY_Start_date__c=system.now();
                perfobj.FY_End_Date__c=system.now();
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                //sov in opp currency
                
                perfobj.SOV_Type__c='Performance';
                system.assert(perfobj!=null);
               
               
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                 /*Currency Conversion*/
                
                perfList1.add(perfobj);
                system.assert(perfList1!=null);
                
                perfobj=new Performance_Tracking__c();
               
                perfobj.Fiscal_year__c='2015-2016';
                perfobj.FY_Start_date__c=system.now();
                perfobj.FY_End_Date__c=system.now();
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                //sov in opp currency
                
                perfobj.SOV_Type__c='Transaction CR/DR';
              
               
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                 /*Currency Conversion*/
                
                perfList1.add(perfobj);
                
                        insert perfList1;
                        Test.StartTest();
                        AsyncExecutionExample aee = new AsyncExecutionExample(perfList1);
                        System.enqueueJob(aee);
                        Test.StopTest(); 
         
        } 
}