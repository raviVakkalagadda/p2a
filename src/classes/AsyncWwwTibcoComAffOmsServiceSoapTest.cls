@istest
public class AsyncWwwTibcoComAffOmsServiceSoapTest{
    
    static testMethod void beginSuspendOrder() {
        Long count = 1778945286;
        Boolean orderSummary = true;
        Boolean includejeopardyHeader = false;

        string action = 'action';

        string[] order = new List<String>();
        order.add('order');
        order.add('service');
        order.add('Test');

        string[] orderRef = new List<string>();
        orderRef.add('Tibco');
        orderRef.add('Template');
        orderRef.add('Service');


        AsyncWwwTibcoComAffOmsServiceSoap.AsyncOrderServicePort AsyncTibcoServiceSoap = new AsyncWwwTibcoComAffOmsServiceSoap.AsyncOrderServicePort();        
        AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture AsyncSuspEleme = new AsyncWwwTibcoComAffOrderservice.SuspendOrderResponse_elementFuture();        
        AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture AsyncAffSubmitorder = new AsyncWwwTibcoComAffOrderservice.SyncSubmitOrderResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture Asyngetorderres = new AsyncWwwTibcoComAffOrderservice.GetOrdersResponse_elementFuture();

        AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture performBulkelemfuture = new AsyncWwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_elementFuture();  
        AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture GetorderresponseEle = new AsyncWwwTibcoComAffOrderservice.GetOrderDetailsResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture AmendordResponse = new AsyncWwwTibcoComAffOrderservice.AmendOrderResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture SubmitOrder = new AsyncWwwTibcoComAffOrderservice.SubmitOrderResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture CancelOrder = new AsyncWwwTibcoComAffOrderservice.CancelOrderResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture Getorder = new AsyncWwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture Withdraw = new AsyncWwwTibcoComAffOrderservice.WithdrawOrderResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture GetEnrichPlanResponse = new AsyncWwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_elementFuture();
        AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture beginActivateOrder = new AsyncWwwTibcoComAffOrderservice.ActivateOrderResponse_elementFuture();

        wwwTibcoComAffOrderservice.sortCriteria Tibcosortcretia = new wwwTibcoComAffOrderservice.sortCriteria();
        wwwTibcoComAffOrderservice.dateRange_element dateRange = new wwwTibcoComAffOrderservice.dateRange_element();
        wwwTibcoComAffOrderservice.headerUDF_element[] headerUDF = new list<wwwTibcoComAffOrderservice.headerUDF_element>();
        wwwTibcoComAffOrderservice.orderLineUDF_element[] orderLineUDF = new List<wwwTibcoComAffOrderservice.orderLineUDF_element>();
        wwwTibcoComAffOrderservice.pagination_element pagination = new wwwTibcoComAffOrderservice.pagination_element();
                
        Test.startTest(); 
            AsyncSuspEleme = AsyncTibcoServiceSoap.beginSuspendOrder(new System.continuation(60),'orderID','orderRef');
            AsyncAffSubmitorder = AsyncTibcoServiceSoap.beginSyncSubmitOrder(new System.continuation(60));
            Asyngetorderres = AsyncTibcoServiceSoap.beginGetOrders(new System.continuation(60),Tibcosortcretia,'orderId','orderRef','customerID','subscriberID',dateRange,'status',headerUDF,orderLineUDF,count,pagination,orderSummary);
            performBulkelemfuture = AsyncTibcoServiceSoap.beginPerformBulkOrderAction(new System.continuation(60),'action',order,orderRef);   
            GetorderresponseEle  = AsyncTibcoServiceSoap.beginGetOrderDetails(new System.continuation(60),'orderId','orderRef');
            AmendordResponse = AsyncTibcoServiceSoap.beginAmendOrder(new System.continuation(60));
            SubmitOrder  = AsyncTibcoServiceSoap.beginSubmitOrder(new System.continuation(60));
            CancelOrder = AsyncTibcoServiceSoap.beginCancelOrder(new System.continuation(60),'orderID','orderRef',orderSummary);
            Getorder  = AsyncTibcoServiceSoap.beginGetOrderExecutionPlan(new System.continuation(60),'orderID','orderRef');
            Withdraw  = AsyncTibcoServiceSoap.beginWithdrawOrder(new System.continuation(60),'orderID','orderRef');
            GetEnrichPlanResponse  = AsyncTibcoServiceSoap.beginGetEnrichedExecutionPlan(new System.continuation(60),'orderID','orderRef',orderSummary,includejeopardyHeader);
            beginActivateOrder  = AsyncTibcoServiceSoap.beginActivateOrder(new System.continuation(60),'orderID','orderRef');
        Test.stopTest(); 
        
        system.assertEquals(true,AsyncSuspEleme!=null);
        system.assertEquals(true,AsyncAffSubmitorder!=null);
        system.assertEquals(true,Asyngetorderres!=null);
        system.assertEquals(true,performBulkelemfuture!=null);
        system.assertEquals(true,GetorderresponseEle!=null); 
        system.assertEquals(true,AmendordResponse!=null);
        system.assertEquals(true,SubmitOrder!=null);
        system.assertEquals(true,CancelOrder!=null);
        system.assertEquals(true,Getorder!=null);
        system.assertEquals(true,Withdraw!=null);
        system.assertEquals(true,GetEnrichPlanResponse!=null);
        system.assertEquals(true,beginActivateOrder!=null);
        

    }
    
 }