@istest(seealldata=false)
public class AsyncWwwTibcoComBillingactivateTest{
     
    static testMethod void AllOrderTriggertest() {
        
        wwwTibcoComSchemasXsdgenerationBill.Service_element[] Service = new List<wwwTibcoComSchemasXsdgenerationBill.Service_element>();
        
        AsyncWwwTibcoComBillingactivate.AsyncBillingActivatePortTypeEndpoint1 point = new AsyncWwwTibcoComBillingactivate.AsyncBillingActivatePortTypeEndpoint1();
        
        Test.startTest();
            Test.setMock(WebServiceMock.class, new MainMockClass.wwwTibcoComSchemasXsdgenerationBill());
            AsyncWwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_elementFuture future = new AsyncWwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_elementFuture();
            future = point.beginBillingActivateOperation(new System.continuation(60), Service);
            system.assertEquals(true,future!=null); 

        Test.stopTest(); 
   
    }
 }