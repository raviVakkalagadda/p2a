//Generated by wsdl2apex

public class AsyncWwwTigComWsdlGenerateidGenerateidim {
    public class AsyncIDGeneratorSOAPEventSource {
        public String endpoint_x = 'http://localhost:7123/Business%20Processes/00-InputChannels/Receive_IDGenerator_Request';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.tig.com/wsdl/GenerateID/GenerateIDImpl/Business_sp_Processes/_00-InputChannels', 'wwwTigComWsdlGenerateidGenerateidim', 'http://www.tig.com/schemas/ID_GeneratorRequest', 'wwwTigComSchemasIdGeneratorrequest', 'http://www.tibco.com/schemas/ID_Generator_Fault', 'wwwTibcoComSchemasIdGeneratorFault', 'http://www.tig.com/schemas/ID_GeneratorResponse', 'wwwTigComSchemasIdGeneratorresponse'};
        public AsyncWwwTigComSchemasIdGeneratorresponse.TSID_elementFuture beginGenerateID(System.Continuation continuation,String Product_xc,String ACity_Side,String ZCity_Side,String Selling_Entity,String Service_Type) {
            wwwTigComSchemasIdGeneratorrequest.Envelope_element request_x = new wwwTigComSchemasIdGeneratorrequest.Envelope_element();
            request_x.Product_xc = Product_xc;
            request_x.ACity_Side = ACity_Side;
            request_x.ZCity_Side = ZCity_Side;
            request_x.Selling_Entity = Selling_Entity;
            request_x.Service_Type = Service_Type;
            return (AsyncWwwTigComSchemasIdGeneratorresponse.TSID_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwTigComSchemasIdGeneratorresponse.TSID_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'IDGenerator',
              'http://www.tig.com/schemas/ID_GeneratorRequest',
              'Envelope',
              'http://www.tig.com/schemas/ID_GeneratorResponse',
              'TSID',
              'wwwTigComSchemasIdGeneratorresponse.TSID_element'}
            );
        }
    }
}