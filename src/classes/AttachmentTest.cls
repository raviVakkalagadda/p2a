/**
@author - Accenture
@date - 27-FEB-2012
@version - 1.0
@description - This is the test class on AttachmentHandler Trigger.
*/
@isTest
private with sharing class AttachmentTest{

        static testMethod void generateOrderTest() {
        Account acc = OpportunityHelp.getAccount();
        Blob b = Blob.valueOf('abcd');
        Attachment att = new Attachment(Name='Test att1', Description = 'Test att1', ParentId=acc.id, Body = b);
        insert att;
        system.assert(att!=null);
     }
}