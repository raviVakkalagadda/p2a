/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class AttachmentTriggerHandler extends BaseTriggerHandler{
public class AttachmentTriggerHandler extends BaseTriggerHandler{
    
    public override void beforeInsert(List<SObject> newAttachments) {
        updateVariationNoOnContracts((List<Attachment>)newAttachments);
		createTIDocuments((List<Attachment>)newAttachments);
    }
	
	 public override void beforeUpdate(List<SObject> newAttachments, Map<Id, SObject> newAttachmentsMap, Map<Id, SObject> oldAttachmentsMap) {
        updateVariationNoOnContracts((List<Attachment>)newAttachments);
    }


    /**
     * Different types of Attachment documents are generating in CPQ and push it to different SFDC objects.
     * This trigger moved these documents into common object called  "TI Document"
     */
    @testvisible
    private void createTIDocuments(List<Attachment> newAttachments){    
        if(Util.muteAllTriggers()) return;
        Set<ID> attIds = new Set<ID>();
        List<TI_Document__c> tiDocList = new List<TI_Document__c>();
        map<String, String> keyPrefixToName = new map<string,string>();
        for(Schema.SObjectType stype : Schema.getGlobalDescribe().values()){
            keyPrefixToName.put(stype.getDescribe().getkeyprefix(),stype.getDescribe().getname());
        }
        String objectName = '';
        for(Attachment att1 : newAttachments){
            attIds.add(att1.ParentId); //Set to hold the Attachment IDs
            objectName = keyPrefixToName.get(String.valueOf(att1.ParentId).substring(0,3));
            
            if(objectName != null && !(objectName == 'Account' || objectName == 'Opportunity' || objectName == 'Contract__c' || objectName == 'csord__Service__c')){
                return;
            }
        }

        Map<id, Account> accMap = new Map <id, Account>([select id from Account where id in :attIds ]);
        Map<id, Opportunity> oppMap = new Map <id, Opportunity>([select id,Ownerid from Opportunity where id in :attIds ]);
        Map<id, Contract__c> conMap = new Map <id, Contract__c>([select id from Contract__c where id in :attIds ]);
		Map<id, csord__Service__c> serviceMap = new Map <id, csord__Service__c>([select id from csord__Service__c where id in :attIds ]);
        Map<String, Attachment> attNameMap = new Map<String, Attachment>();
		
		
		List<Opportunity> Opplist = [select id,Ownerid from Opportunity where id in :attIds ];
		List<Account> Acclist = [select id,Ownerid from Account where id in :attIds ];
		List<Contract__c> Contlist = [select id,Account_Owner_Id__c from Contract__c where id in :attIds ];
		List<csord__Service__c> Serlist = [select id,Account_Owner__c from csord__Service__c where id in :attIds ];
		
        for(Attachment att2 : newAttachments){
            //Condition to check the current parent of the document.If its not TI Document, then proceed.
            if(accMap.containsKey(att2.ParentId) || oppMap.containsKey(att2.ParentId) || conMap.containsKey(att2.ParentId) || serviceMap.containsKey(att2.ParentId)){ 
                TI_Document__c tidoc = new TI_Document__c(Name=att2.name);
                IF(oppMap.containsKey(att2.ParentId)){
				//List<Opportunity> Opplist = [select id,Ownerid from Opportunity where id in :attIds ];
                tidoc.ownerid = Opplist[0].ownerid; 
                tidoc.Document_Type_del__c = 'Opportunity'; 
                tidoc.Related_Opportunity__c = Opplist[0].id; 
                } if(accMap.containsKey(att2.ParentId)){		
                        //List<Account> Acclist = [select id,Ownerid from Account where id in :attIds ];		
                        tidoc.Related_Account__c = Acclist[0].Id;		
                        tidoc.ownerid = Acclist[0].ownerid;		
                        tidoc.Document_Type_del__c = 'Account';		
                }		
                if(conMap.containsKey(att2.ParentId)){		
                        //List<Contract__c> Contlist = [select id,Account_Owner_Id__c from Contract__c where id in :attIds ];		
                        tidoc.Related_Contract__c = Contlist[0].Id;		
                        tidoc.Document_Type_del__c = 'Contract';		
                        tidoc.ownerid = Contlist[0].Account_Owner_Id__c;		
                }		
                if(serviceMap.containsKey(att2.ParentId)){		
                        //List<csord__Service__c> Serlist = [select id,Account_Owner__c from csord__Service__c where id in :attIds ];		
                        tidoc.Service__c = Serlist[0].id;		
                        tidoc.Document_Type_del__c = 'Service';		
                        tidoc.ownerid = Serlist[0].Account_Owner__c;		
                }
                tiDocList.add(tidoc);
                attNameMap.put(att2.name, att2);
            }
        }
    
        if(tiDocList.size()>0){
            List<TI_Document__c> updDocLst = new List<TI_Document__c>();
            Database.SaveResult[] result = Database.insert(tiDocList,false); //Create new TI Document header records for the new attachments.
            set <ID> tiDocIDs = new Set<ID>();
            for(Database.SaveResult rslt:result)
                tiDocIDs.add(rslt.getId());
            /**
            Date: 22nd Jun 2017
            Incident#: TGC0043878 
            Description: Attachment uploaded under Opportunity is not getting associated to its corresponding TI Document record
            Developer: Geethanjali
            Change: Removed from SOQL "Document_Type_del__c !='Opportunity' and"
            */
            for(TI_Document__c tidoc : [select id,name from TI_Document__c where id in :tiDocIDs]){
                if(attNameMap.containsKey(tidoc.name)){
                    Attachment tmpAtt = attNameMap.get(tidoc.name);
                    tmpAtt.ParentId = tidoc.id; //Modify the parent for the Attchement to TI Document
                }
            }
        
        try{
            if(!updDocLst.isEmpty())
                update updDocLst;
        } catch(DMLException dx){
            ErrorHandlerException.ExecutingClassName='AttachmentTriggerHandler:createTIDocuments';
            ErrorHandlerException.objectList=newAttachments;
            ErrorHandlerException.sendException(dx);
            System.debug('Exception while updating TI_Document');
        }
        }
    }
	
	private void updateVariationNoOnContracts(List<Attachment> newAttachments){    
        if(Util.muteAllTriggers()) return;
        Integer count=0;
		Set<ID> attIds = new Set<ID>();
       List<Contract__c> con = new List<Contract__c>();
	   List<Contract__c> conlist = new List<Contract__c>();
	   List<Account> Acctemplist = new List<Account>();
	   Contract__c con1 = new Contract__c();
	   Set<ID> conlistIDs = new Set<ID>();
	   Account acc = new Account();
       
	   for(Attachment att1 : newAttachments){
		   attIds.add(att1.ParentId);
	   }
	   
       //query ObjectPrefixIdentifiers to get ObjectPrefixIdentifiers Object based on request Name
       ObjectPrefixIdentifiers__c opi = ObjectPrefixIdentifiers__c.getInstance('Contract');       
       
	   conlist = [select Id, Name, Account__c, Initial_Period__c, Variation_Number__c from Contract__c where Id IN:attIds];
	   
	   for(Contract__c c : conlist){
		conlistIDs.add(c.id);
	   }
	   
	   Acctemplist = [Select Id from Account where Id IN: conlistIDs];
       // Getting the values from Contract and Account when new document is uploaded
       for(Attachment at : newAttachments)
       {
           if(opi!=null && at.ParentId!=null && opi.ObjectIdPrefix__c!=null && opi.ObjectIdPrefix__c ==(((String)(at.ParentId)).substring(0,3))){
             //query Contract to get Contract Object based on request Attachment ID
             //Contract__c con1 = [select Id, Name, Account__c, Initial_Period__c, Variation_Number__c from Contract__c where Id=:at.ParentId Limit 1];
			 
			 for( Contract__c c : conlist){
				if(at.ParentId == c.id)
				{
					con1= c;
					break;
				}
			 }
			 
			 for( Account a : Acctemplist){
				if(a.Id == con1.Account__c)
				{
					acc= a;
					break;
				}
			 }
             
             //query Account to get Account Object based on request Contract ID
             //Account acc = [Select Id from Account where Id=:con1.Account__c Limit 1];
             
             // check first three digits of Attachment ID with ObjectIDPrefix field 
             if(opi.ObjectIdPrefix__c ==(((String)(at.ParentId)).substring(0,3)))
             {
            
                 
                 if(con1.Variation_Number__c == null)
                 {
                    //System.debug('Variation Number*** '+con1.Variation_Number__c);
                     con1.Variation_Number__c = 0;
                     //System.debug('Variation Number1*** '+con1.Variation_Number__c);
                 }
                 else
                 {
                    
                    for(Integer i=0; i<=con1.Variation_Number__c; i++) 
                    {
                        
                        count++;
                    }
                    con1.Variation_Number__c = count;
                    //System.debug('Variation Number1*** '+con1.Variation_Number__c);
                 }            
             }
             con.add(con1);
        }
       }
       if(!con.IsEmpty()){
           update con;
       }
     }
}
