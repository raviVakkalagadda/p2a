@isTest
public class AttachmentTriggerHandlerTest {
  static testmethod void createTIDocuments_method1()
    {
        List<Account> accounts = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> opportunitys = P2A_TestFactoryCls.getOpportunitys(1,accounts);
        
        ObjectPrefixIdentifiers__c op=new ObjectPrefixIdentifiers__c();
        op.ObjectIdPrefix__c='a07';
        op.name='Contract';
        insert op;
        system.assertEquals(true,op!=null); 
         TI_Document__c tidoc = new TI_Document__c(Name= 'Test Attachment for Parent');
         insert tidoc;  
        system.assertEquals(true,tidoc!=null); 
        Attachment attachment = new Attachment();
        attachment.Body = Blob.valueOf('Test Data');
        attachment.Name = 'Test Attachment for Parent';
        //attachment.ParentId = opportunitys[0].id;
        attachment.ParentId = tidoc.id;        
        Profile p=[SELECT Id,name From Profile WHERE Name='System Administrator' limit 1];
        List<User> usr = P2A_TestFactoryCls.get_Users(1);//[Select id, name from User where ProfileId=:p.Id limit 1];      
        attachment.OwnerId = usr[0].id;        
        list<Attachment> attach=new list<Attachment>();
        attach.add(attachment);
        insert attach;       
        system.assertEquals(true,attach!=null); 
        AttachmentTriggerHandler ath = new AttachmentTriggerHandler ();
        ath.beforeInsert(attach);
        system.assertEquals(true,ath!=null); 
  } 
}