global class BandwidthLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        System.Debug('BandwidthLookup.apxc: doDynamicLookupSearch');
//List<CloudSense_Bandwidth__c> bandwidths = Database.query('SELECT Name FROM CloudSense_Bandwidth__c WHERE Name = "STM4"');
    String STM4 ='STM4';
    List<CloudSense_Bandwidth__c> bandwidths = Database.query('SELECT Name FROM CloudSense_Bandwidth__c WHERE Name =:STM4');  
    return bandwidths;
    }
    public override String getRequiredAttributes(){ 
        return '[ "RateCardIDTemp" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            try {
                System.Debug('BandwidthLookup.apxc');
                System.Debug('Search Fields = ' + searchFields);
                String rcId = searchFields.get('RateCardIDTemp');
                System.Debug('Using Rate Card Id = ' + rcID);
                String query = 'select id, Bandwidth__r.Name from IPL_Rate_Card_Bandwidth_Join__c where IPL_Rate_Card_Item__r.id = :rcId';
                List<String> bandwidthNames = new List<String>();
                for (IPL_Rate_Card_Bandwidth_Join__c rcbj : Database.query(query))
                {
                    bandwidthNames.add(rcbj.Bandwidth__r.Name);
                }
                String searchValue = searchFields.get('searchValue') +'%';
                query = 'SELECT Name FROM CloudSense_Bandwidth__c WHERE Name in :bandwidthNames AND Name LIKE :searchValue ORDER BY Name';
                List<CloudSense_Bandwidth__c> bandwidths = Database.query(query);
                return bandwidths;
            }
            catch (Exception e)
            {
                System.Debug('Exception: '+e);
                return null;
            }
    }

}