@isTest(SeeAllData = false)
public class BaseTriggerHandler_Test{
 public static List<Supplier__c> supplierlist;
 public static Map<Id, SObject> newObjectsMap;
 
    private static void initTestData(){        
        try{
            supplierlist = P2A_TestFactoryCls.getsuppliers(1);
            system.assertEquals(true,supplierlist!=null); 
            newObjectsMap.put(supplierlist[0].id,supplierlist[0]);
        }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='BaseTriggerHandler_Test :initTestData';         
            ErrorHandlerException.sendException(e);
        }
    }
    private static testMethod void baseTriggerHandler()
    {
            initTestData();
            Test.startTest();
            BaseTriggerHandler hndlr = new BaseTriggerHandler();
            hndlr.isDisabled();
            hndlr.getMaxRecursion();
            hndlr.beforeInsert(supplierlist);
            hndlr.afterInsert(supplierlist,newObjectsMap);
            hndlr.beforeUpdate(supplierlist,newObjectsMap,newObjectsMap);
            hndlr.afterUpdate(supplierlist,newObjectsMap,newObjectsMap);
            hndlr.beforeDelete(newObjectsMap);
            hndlr.afterDelete(newObjectsMap);
            hndlr.afterUndelete(supplierlist,newObjectsMap);
            hndlr.allowRecursion();
            hndlr.allowRecursion(1);
            hndlr.getName();
            system.assertEquals(true,hndlr!=null); 
            Test.stopTest();
                
            
        }
}