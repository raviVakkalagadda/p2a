global with sharing class BasketControllerExtension {
	
	private static cscfga__Product_Basket__c basket;

	public BasketControllerExtension(ApexPages.StandardController stdBasket) {
		basket = (cscfga__Product_Basket__c)stdBasket.getRecord();
	}

	@RemoteAction
	global static void updateProductConfigurationsRequest(Id configIdOld, Id configIdNew, Id basketId){

		system.debug('**** Basket: ' + basket);	
		if(configIdOld != configIdNew){
			List<csbb__Product_Configuration_Request__c> pcrList = [SELECT Id, csbb__Product_Configuration__c
																	FROM csbb__Product_Configuration_Request__c 
																	WHERE csbb__Product_Configuration__c = :configIdOld
																	AND csbb__Product_Basket__c = :basketId];

			system.debug('**** pcrList: ' + pcrList);	
			if(!pcrList.isEmpty()){
				csbb__Product_Configuration_Request__c pcr = pcrList.get(0);
				pcr.csbb__Product_Configuration__c = configIdNew;

				system.debug('**** pcr: ' + pcr);
				update pcr;
				Set<Id> confIds = new Set<Id>();
				confIds.add(configIdOld);
				deleteConfigs(confIds);
			}
		}
	}

	@future
	global static void deleteConfigs(Set<Id> configIds){

		cscfga.ProductConfigurationBulkActions.deleteProductConfigs(configIds);
	}
}