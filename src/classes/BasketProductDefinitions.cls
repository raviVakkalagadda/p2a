public with sharing class BasketProductDefinitions {

    public String selectListValue { 
        get {return this.selectedProductDef;}
        set {
            if (value != null)
            {
                System.Debug('Set '+value); 
                this.selectedProductDef = value;
                try
                {
                  cscfga__Screen_Flow_Product_Association__c sfpa = [select Id, cscfga__Screen_Flow__c  from cscfga__Screen_Flow_Product_Association__c WHERE cscfga__Screen_Flow__r.MLEFlow__c = True AND cscfga__Product_Definition__c  = :value];
                  this.screenFlowId = sfpa.cscfga__Screen_Flow__c;
                 
                }  
                catch (Exception e)
                {
                    ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.ERROR, 'No screenflow defined for product '+value));
                }
            }
        }
    }
    
    public PageReference setProductDefinitionName() {
        PageReference ref = ApexPages.currentPage();
        ref.getParameters().put('basketId', this.basketId);
        return ref;
    }


    /*public PageReference save() {
        System.Debug('Current value of selected product Def = ' + this.productDefinitions.get(this.selectedProductDef));
        return new PageReference(String.Format(
        URL_BASE,
            new String[] {this.basketId, this.selectedProductDef, this.basketId, this.screenFlowId}));
    }*/
    
        public PageReference save() {
        System.Debug('Current value of selected product Def = ' + this.productDefinitions.get(this.selectedProductDef));
        return new PageReference(String.Format(
        URL_BASE,
            new String[] {this.basketId, this.selectedProductDef, '/apex/c__PCRsForMLE?basketId=' + this.basketId, this.screenFlowId}));
    }
   
    
    static String URL_BASE = '/apex/csmle1__MultiLineEditor?id={0}&productDefinitionId={1}&batchSize=5&showHeader=false&sideBar=false&retURL={2}&screenFlowId={3}';
    public String selectedProductDef = '';
    public String screenFlowId = '';
    public String basketId;
    public Map<Id, String> productDefinitions = new Map<Id, String>();
    public Set<String> allowedProductDefinitions = new Set<String>{'IPVPN Port','VPLS VLAN Port', 'VPLS Transparent', 'VLAN Group', 'SMA Gateway', 'ASBR','Cage/Private Room', 'Rack', 'Cross Connect'};
    
    public BasketProductDefinitions()
    {
        this.basketId = System.currentPageReference().getParameters().get('basketId');
        System.Debug('Got basketId = ' + basketId);
        if (basketId != null)
        {
            for (cscfga__Product_Configuration__c pc : [SELECT 
                                                                Id, cscfga__Product_Definition__c,
                                                                cscfga__Product_Definition__r.Name,
                                                                OpportunityStage__c,
                                                                cscfga__Parent_Configuration__c
                                                                FROM 
                                                                cscfga__Product_Configuration__c
                                                                    WHERE 
                                                                    cscfga__Product_Basket__c = :basketId])
            {
                if(this.allowedProductDefinitions.contains(pc.cscfga__Product_Definition__r.Name)){
                    this.productDefinitions.put(pc.cscfga__Product_Definition__c,
                                                        pc.cscfga__Product_Definition__r.Name);  
                            System.Debug('******** Added ' + pc.cscfga__Product_Definition__r.Name + ' to list of PDs');
                }
            }
        }
        System.Debug(productDefinitions);
    }
    public List<SelectOption> getItems() {
        List<SelectOption> items = new List<SelectOption>();
        if (this.productDefinitions.keySet() != null)
        for (Id prodDefId : this.productDefinitions.keySet())
        {
            if (prodDefId != null)
            {
            System.Debug(prodDefId);
            System.Debug(productDefinitions);
            items.add(new SelectOption(prodDefId, productDefinitions.get(prodDefId)));
            }
        }
        return items;
    }
  
}