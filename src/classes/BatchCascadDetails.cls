public class BatchCascadDetails implements Database.Batchable<csord__order__c>, Database.Stateful { 

    public Set<Id>OrderSet{get;set;}
    public Set<Id>ServiceSet{get;set;}
    public Map<String,String>AttrToValueMap{get;set;}
    public id solutionId{get;set;}
    public solutions__c  solnObj{get;set;}
    
    
    //contructor
    public BatchCascadDetails(Set<Id>OrderSet,Set<Id>ServiceSet,Map<String,String>AttrToValueMap) {
        this.OrderSet=OrderSet;
        this.ServiceSet=ServiceSet;
        this.AttrToValueMap=AttrToValueMap;
        List<id>orderIdList=new List<Id>(OrderSet);
        csord__order__c ordertosolution=[select id,Solutions__c from csord__order__c where Id =:orderIdList[0]];
        solnObj=[select id,batch_count__c from solutions__c where id=:ordertosolution.Solutions__c];
        solnObj.batch_count__c=OrderIdList.size();
        update solnObj;//update the total batch count on soln object
        solutionId=solnObj.id;
    } 
    
    //start method: will return list of order Sobject
    public List<csord__order__c> start(Database.BatchableContext BC)
    {   

        /**
         * Setting flag value to true to start execution of the method 'updateOrderOrchestrationProcess'.
         * This flag value will be true until the batch job complete.        
         * Method 'updateOrderOrchestrationProcess' is defined in the class 'AllOrderTriggersHandler'.
         */
        CheckRecursive.updateOrderOrchestrationProcessExecute = true;
        
        //Disable Trigger
        //TriggerFlags.NoOrderTriggers = true;
        //TriggerFlags.NoServiceTriggers = true;    
        //TriggerFlags.NoAttributeTriggers = true;
        
       if(!OrderSet.isempty())
       {return [SELECT ID,name,Status__c,Billing_Team_Notified__c,SD_OM_Contact__c,SD_PM_Contact__c,Solutions__c,Technical_Specialist__c,
                        Customer_Test_Complete__c,Handover_Complete__c,Billing_Check_Complete__c,Termination_Reason__c,
                        Customer_Required_Date__c,Customer_Turn_Up_Test_Date__c,Order_Completion_Date__c,
                        Requested_Termination_Date__c,SD_OM_Assigned_Date__c,SD_PM_Assigned_Date__c,
                        Technical_Specialist_Assigned_Date__c 
                            FROM csord__Order__c 
                            WHERE Id IN:orderSet];
                            }
       return null;

    }  
    
    //execute method will process the list of order sobject and submit the order
    public void execute(Database.BatchableContext BC,List<csord__order__c>OrderList)
    {
              
       try{
           List<csord__service__c>serviceList=[select id,csordtelcoa__Product_Configuration__c,Customer_Handover_Date__c,Billing_Commencement_Date__c,Stop_Billing_Date__c,Customer_Required_Date__c from csord__service__c where id in:ServiceSet];
           set<Id>pcSet=new Set<Id>(); 
  
  
          //code to update the selected service
          
               for(csord__service__c serv:serviceList){
                    if(serv.csordtelcoa__Product_Configuration__c!=null && !pcSet.contains(serv.csordtelcoa__Product_Configuration__c)){
                        pcSet.add(serv.csordtelcoa__Product_Configuration__c);
                       }   
                       system.debug('PCSET---' +pcSet); 
                    serv.Customer_Handover_Date__c=AttrToValueMap.get('Ser_CustHandDate')!= null && AttrToValueMap.get('Ser_CustHandDate')!='' ? Date.parse(AttrToValueMap.get('Ser_CustHandDate')) : serv.Customer_Handover_Date__c;
                    serv.Billing_Commencement_Date__c=AttrToValueMap.get('Ser_BillingComDate')!= null && AttrToValueMap.get('Ser_BillingComDate')!='' ? Date.parse(AttrToValueMap.get('Ser_BillingComDate')) : serv.Billing_Commencement_Date__c;
                    serv.Stop_Billing_Date__c=AttrToValueMap.get('Ser_StopBillDate')!= null && AttrToValueMap.get('Ser_StopBillDate')!='' ? Date.parse(AttrToValueMap.get('Ser_StopBillDate')) : serv.Stop_Billing_Date__c;
                    serv.Customer_Required_Date__c=AttrToValueMap.get('Ser_CRD')!= null && AttrToValueMap.get('Ser_CRD')!='' ? Date.parse(AttrToValueMap.get('Ser_CRD')) : serv.Customer_Required_Date__c;
               }
               
              //code to update the selected orders,associated with the services 
                for(csord__Order__c ord1 : orderList){
                    ord1.Status__c = AttrToValueMap.get('Ord_Status')!=null && AttrToValueMap.get('Ord_Status')!='' ? AttrToValueMap.get('Ord_Status') : ord1.Status__c;
                    ord1.Termination_Reason__c = AttrToValueMap.get('Ord_TerReason')!=null && AttrToValueMap.get('Ord_TerReason')!='' ? AttrToValueMap.get('Ord_TerReason') : ord1.Termination_Reason__c;
                    ord1.Customer_Required_Date__c = AttrToValueMap.get('Ser_CRD')!= null && AttrToValueMap.get('Ser_CRD')!='' ? Date.parse(AttrToValueMap.get('Ser_CRD')) : ord1.Customer_Required_Date__c;
                    ord1.Customer_Test_Complete__c = AttrToValueMap.get('Ord_custTestComDate')!= null && AttrToValueMap.get('Ord_custTestComDate')!='' && !ord1.Customer_Test_Complete__c ? Boolean.valueOf(AttrToValueMap.get('Ord_custTestComDate')) : ord1.Customer_Test_Complete__c;
                    ord1.Customer_Turn_Up_Test_Date__c =  AttrToValueMap.get('Ord_TurnUp')!= null && AttrToValueMap.get('Ord_TurnUp')!='' ? Date.parse(AttrToValueMap.get('Ord_TurnUp')) : ord1.Customer_Turn_Up_Test_Date__c; 
                    ord1.Order_Completion_Date__c =  AttrToValueMap.get('Ord_ComplDate')!= null && AttrToValueMap.get('Ord_ComplDate')!='' ? Date.parse(AttrToValueMap.get('Ord_ComplDate')) : ord1.Order_Completion_Date__c; 
                    ord1.Requested_Termination_Date__c=  AttrToValueMap.get('Ord_TerDate')!= null && AttrToValueMap.get('Ord_TerDate')!='' ? Date.parse(AttrToValueMap.get('Ord_TerDate')) : ord1.Requested_Termination_Date__c;
                    ord1.Handover_Complete__c =  AttrToValueMap.get('Ord_HandComp')!= null && AttrToValueMap.get('Ord_HandComp')!='' && !ord1.Handover_Complete__c? Boolean.valueOf(AttrToValueMap.get('Ord_HandComp')) : ord1.Handover_Complete__c;
                    ord1.Billing_Team_Notified__c =  AttrToValueMap.get('Ord_BillingComp')!= null && AttrToValueMap.get('Ord_BillingComp')!='' && !ord1.Billing_Team_Notified__c ? Boolean.valueOf(AttrToValueMap.get('Ord_BillingComp')) : ord1.Billing_Team_Notified__c ;
                    ord1.SD_PM_Contact__c =  AttrToValueMap.get('Ord_SDPM')!= null && AttrToValueMap.get('Ord_SDPM')!='' ? AttrToValueMap.get('Ord_SDPM') : ord1.SD_PM_Contact__c;
                    ord1.SD_OM_Contact__c =  AttrToValueMap.get('Ord_SDOM')!= null && AttrToValueMap.get('Ord_SDOM')!='' ? AttrToValueMap.get('Ord_SDOM') : ord1.SD_OM_Contact__c;   
                    ord1.SD_PM_Assigned_Date__c =  AttrToValueMap.get('Ord_SDPMAsgDate')!= null && AttrToValueMap.get('Ord_SDPMAsgDate')!='' ? Date.parse(AttrToValueMap.get('Ord_SDPMAsgDate')) : ord1.SD_PM_Assigned_Date__c;
                    ord1.SD_OM_Assigned_Date__c =  AttrToValueMap.get('Ord_SDPMAsgDate')!= null && AttrToValueMap.get('Ord_SDPMAsgDate')!='' ? Date.parse(AttrToValueMap.get('Ord_SDPMAsgDate')) : ord1.SD_OM_Assigned_Date__c; 
                    ord1.Technical_Specialist__c = AttrToValueMap.get('Ord_TechSpl')!=null && AttrToValueMap.get('Ord_TechSpl')!='' ? AttrToValueMap.get('Ord_TechSpl') : ord1.Technical_Specialist__c; 
                    
                    ord1.Technical_Specialist_Assigned_Date__c = AttrToValueMap.get('Ord_TechSplDate')!=null && AttrToValueMap.get('Ord_TechSplDate')!='' ? Date.parse(AttrToValueMap.get('Ord_TechSplDate')) : ord1.Technical_Specialist_Assigned_Date__c;
                  }
               
              
               
               
               /*below code will update CRD on Attribute associated with PC
               *Since the CRD attribute can be any among given below three attribute names
               *So all of these three are being queries and whichever is found associated with the PC
               *will be updated, and hence will be visible in enrichment screen
               */
               List<cscfga__Attribute__c> attrList= [Select id,name,cscfga__Display_Value__c,cscfga__Value__c from cscfga__Attribute__c where name IN ('Customer_Required_Date__c','Customer_Required_Date_c','Customer_Required_Date') and cscfga__Product_Configuration__c IN :pcSet];
                 system.debug('attributeList---' +attrList);
                  String tempDate= AttrToValueMap.get('Ser_CRD')!= null && AttrToValueMap.get('Ser_CRD')!='' ? AttrToValueMap.get('Ser_CRD'):null;
                  String newTempDate='';
                   //convert date into YYYY-MM-DD format
                      if(tempDate!=null){
                         List<String>DateList=tempDate.Split('/');  
                         if(DateList[0].length()==2){ 
                        newTempDate=DateList[2]+'-'+DateList[1]+'-'+DateList[0];
                        }
                        else{
                        DateList[0]='0'+DateList[0];
                        newTempDate=DateList[2]+'-'+DateList[1]+'-'+DateList[0];
                        }
                      }              
                  for(cscfga__Attribute__c attr : attrList)
                  {                      
                      attr.cscfga__Display_Value__c= tempDate!= null ? tempDate : attr.cscfga__Value__c ;
                      attr.cscfga__Value__c =newTempDate!=''?newTempDate:attr.cscfga__Display_Value__c;
                  //attr.cscfga__Value__c = String.valueOf(date1);
                 // system.debug('value!!---' +date1);
                  system.debug('value---' +attr.cscfga__Value__c);
                  } 
                  
                     
                  
                  try{if(attrList.size()>0)update attrList;}
                  catch(Exception e)
                  {
                  ErrorHandlerException.ExecutingClassName='BatchCascadDetails:execute';
                  ErrorHandlerException.objectList=OrderList;
                  ErrorHandlerException.sendException(e);
                  System.debug('Attribute updation failed'+e);
                  }
                  try{if(serviceList.size()>0)update serviceList;}
                  catch(Exception e)
                  {
                  ErrorHandlerException.ExecutingClassName='BatchCascadDetails:execute';
                  ErrorHandlerException.objectList=OrderList;
                  ErrorHandlerException.sendException(e);
                  System.debug('service updation failed'+e);
                  
                  }
                  try{if(orderList.size()>0)update orderList;}
                  catch(Exception e)
                  {
                  ErrorHandlerException.ExecutingClassName='BatchCascadDetails:execute';
                  ErrorHandlerException.objectList=OrderList;
                  ErrorHandlerException.sendException(e);
                  
                  System.debug('order updation failed'+e);
                  }
                  //AllServiceTriggerHandler obj=new AllServiceTriggerHandler();
                  //obj.sendExceptionMail(String.valueOf(attrList));
           }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='BatchCascadDetails:execute';
            ErrorHandlerException.objectList=OrderList;
            ErrorHandlerException.sendException(e);
            System.debug(e);
             //AllServiceTriggerHandler obj1=new AllServiceTriggerHandler();
             //obj1.sendExceptionMail(String.valueOf(e));
           }     
    
     }
    //final method :will be executed when batch is finished
   public void finish(Database.BatchableContext BC)

    {
       //TriggerFlags.BatchSuccess='Batch Completed SuccessFully';
       //TriggerFlags.BatchFailed='';
       //TriggerFlags.BatchCount=0;//will help in actionPoller in solutionSummary Page
       solnObj=[select id,batch_count__c from solutions__c where id=:solutionId];
       solnObj.batch_count__c=0;
       update solnObj;//once the batch count is 0, means batch is completed
       System.debug('execution finished'); 

       /**
         * Setting flag value to false to end the execution of method 'updateOrderOrchestrationProcess' upon completion of the batch job.
         * Method 'updateOrderOrchestrationProcess' is defined in the class 'AllOrderTriggersHandler'.
         */     
        CheckRecursive.updateOrderOrchestrationProcessExecute = false;
        Map<Id,csord__Order__c> newOrders = new Map<Id,csord__Order__c>([select Id, Name, RAG_Status__c, RAG_Status_Red__c,
                                                                        RAG_Reason_Code__c, RAG_Order_Status_RED__c
                                                                        FROM csord__Order__c
                                                                        where Id IN :OrderSet]);
        AllOrderTriggersHandler.pushStatusToActiveStep(newOrders.values(), newOrders);      
    } 
    
    
    private class BatchCascadDetailsException extends Exception {}
    
}