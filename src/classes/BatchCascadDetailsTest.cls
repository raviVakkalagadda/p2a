@isTest(SeeAllData = false)
public class BatchCascadDetailsTest{
    public static Map<String,String> AttrToValueMap=new Map<String,String>();
    public static Set<Id> OrderSet = new Set<Id>();
    public static Set<Id> ServiceSet = new Set<Id>();
    
     public static List<Account> acclist =  P2A_TestFactoryCls.getAccounts(1);
        public static List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        public static List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        public static List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        public static List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
        public static List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,orderRequestList);       
        public static List<csord__Service__c> servicelist = P2A_TestFactoryCls.getService(1,orderRequestList,subList);
        public static  List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        public static List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        public static List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        //public static List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,prodBaskList,prodef,pbundlelist,Offerlists); 
         
    private static testMethod void submitOrderToFOMTest() {  
         solutions__c solObj = new solutions__c(Batch_Count__c=5,Order_Name__c=ordList[0].id,Account_Name__c=acclist[0].id);
         insert solObj;      
         system.assertEquals(true,solObj!=null); 
        
          
        ordList[0].Solutions__c=solObj.id;
        ordList[0].Order_Completion_Date__c = DateTime.now();
        ordList[0].Requested_Termination_Date__c = system.today();
        ordList[0].Handover_Complete__c = true;
        ordList[0].Billing_Team_Notified__c = true;
        ordList[0].SD_PM_Contact__c = userinfo.getuserid();
        
         
        servicelist[0].Solutions__c=solObj.id;
        update ordList;
        update servicelist; 
        system.assertEquals(true,servicelist!=null); 
        OrderSet.add(ordList[0].id);
        ServiceSet.add(servicelist[0].id);
         
         
        AttrToValueMap.put('Ord_Status','Ord_Status');
        AttrToValueMap.put('Ord_CRD','25/05/2019');
        AttrToValueMap.put('Ord_TurnUp','true');
        AttrToValueMap.put('Ord_ComplDate','true');
        AttrToValueMap.put('Ord_TerDate','25/05/2019');
        AttrToValueMap.put('Ord_custTestComDate','25/05/2019');
        AttrToValueMap.put('Ord_HandComp','true');
        //AttrToValueMap.put('Ord_SDPM','');
        //AttrToValueMap.put('Ord_SDOM','');
        AttrToValueMap.put('Ord_SDPMAsgDate','25/05/2019');
        AttrToValueMap.put('Ord_TechSplDate','25/05/2019');
        AttrToValueMap.put('Ord_SDOMAsgDate','25/05/2019');
    
     
         Test.startTest();
         BatchCascadDetails submit = new BatchCascadDetails(OrderSet, ServiceSet, AttrToValueMap);
         Database.executeBatch(submit);   
         system.assertEquals(true,submit!=null); 

         Test.stopTest();
   
        }
        
      private static testMethod void ErrorhandlerTest() {
        
        try{
            BatchCascadDetails submit = new BatchCascadDetails(null,null,null);
            Database.executeBatch(submit);
        }catch(Exception e){}      
      }  
             
}