global class BatchUpdateForPerformanceTracking implements Database.Batchable<sObject> {
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'select Credit_Debit_Date__c,Q1_SOV_Credit__c,IsOldPF__c,Q2_SOV_Credit__c,Q3_SOV_Credit__c,Q4_SOV_Credit__c,Opportunity_Look_Up__c,SOV_Type__c,UserRoleId__c,Employee__c,Employee__r.UserRoleId,Q1_End_date__c,Q1_Start_date__c,Q2_End_date__c,Q2_Start_date__c,Fiscal_year__c,Q3_End_date__c,Q3_Start_date__c,Q4_End_date__c,Q4_Start_date__c,FY_End_Date__c,FY_Start_date__c,Q1_Individual_Performance__c,Q1_Rolled_Up__c,Q1_Target__c,Q2_Individual_Performance__c,Q2_Rolled_Up__c,Q2_Target__c,Q3_Individual_Performance__c,Q3_Rolled_Up__c,Q3_Target__c,Q4_Individual_Performance__c,Q4_Rolled_Up__c,Q4_Target__c,Q1_GAM_Roll_Up__c,Q2_GAM_Roll_Up__c,Q3_GAM_Roll_Up__c,Q4_GAM_Roll_Up__c from Performance_Tracking__c'; 		
return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<Performance_Tracking__c> perfTrackingLst) {
    	Integer orgFiscalMonth = [SELECT FiscalYearStartMonth FROM Organization].FiscalYearStartMonth;
		Date orgFiscalYear = Date.newinstance(system.today().year(), orgFiscalMonth, 1);
		Date currentFYStartDate=orgFiscalYear;
		Date threeYrsOldFYStartDate=orgFiscalYear.addYears(-3);
		Date twoYrsOldFYStartDate=orgFiscalYear.addYears(-2);
		List<Performance_Tracking__c> pfOldList=new List<Performance_Tracking__c>();
		String FYear=threeYrsOldFYStartDate.year()+'-'+twoYrsOldFYStartDate.year();
		system.debug('===FYear=='+FYear);
         for(Performance_Tracking__c perfObj : perfTrackingLst)
         {
         	if(perfObj.Fiscal_year__c==FYear){
         		
         		perfObj.IsOldPF__c=true;
         		pfOldList.add(perfObj);
         	}
         }
         if(pfOldList.size()>0)update pfOldList;
    }   
    
    global void finish(Database.BatchableContext BC) {
    }
}