@isTest
    private class BatchUpdateForPerformanceTrackingTest {
    
    static testmethod void test() {
    
    // Create test Performance Tracking to be updated
    // by the batch job.
    
    List<Performance_Tracking__c> prtList = new List<Performance_Tracking__c>();
    Performance_Tracking__c perTrackObj = new Performance_Tracking__c(Credit_Debit_Date__c = System.now(),Fiscal_year__c ='2014-2015');
    
    prtList.add(perTrackObj);
    insert prtList;
    system.assertEquals(true,prtList!=null); 
    Test.startTest();
    BatchUpdateForPerformanceTracking batUpdForPerTrack = new BatchUpdateForPerformanceTracking();
    DataBase.executeBatch(batUpdForPerTrack); 
    system.assertEquals(true,batUpdForPerTrack!=null); 
    Test.stopTest();
    
    }
}