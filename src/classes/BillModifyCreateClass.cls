public with sharing class BillModifyCreateClass {
    
    Public String SubObj;
    public Boolean pageRefresh {get; Set;}
    public List<csord__Service__c> ChildServices {get; set;}
    public List<csord__Service__c> AllServices {get; set;}
    public List<csord__Subscription__c> SubDispaly {get;set;}
    public set<id> parentbundleid = new set<id>();
    public csord__Service__c parservice = new csord__Service__c();
    public set<id> parentbundleids = new set<id>();
    public csord__Service__c parservices = new csord__Service__c();
    public Boolean isItemSelected { get; set; }
    public csord__Service__c ParentBundle { get; set; }
    public csord__Service__c Bundle { get; set; }
    public string subid {get;set;}
    public Boolean checked {get;set;} 
    public set<id> selected = new set<id>();

    public BillModifyCreateClass(ApexPages.StandardController controller) {
       //pageRefresh=false;
        SubObj = ApexPages.currentPage().getParameters().get('id');  
          
        List<csord__Subscription__c> SubforOpp = [select id, OpportunityRelatedList__c,csordtelcoa__Subscription_Number__c from csord__Subscription__c where id =: SubObj];
        
        ChildServices = [select id, Name,Bundle_Default_Value__c, csord__Subscription__r.csordtelcoa__Subscription_Number__c, Primary_Service_ID__c, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c from csord__Service__c where csord__Subscription__c=: SubObj];
        system.debug('@@@@childservices'+ChildServices);
        if(SubforOpp[0].OpportunityRelatedList__c != null){
           // AllServices = [select id, Name, csord__Subscription__r.csordtelcoa__Subscription_Number__c, Primary_Service_ID__c, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c, Opportunity__c from csord__Service__c where Opportunity__c =: SubforOpp[0].OpportunityRelatedList__c];
            SubDispaly = [select id,OpportunityRelatedList__c,csordtelcoa__Subscription_Number__c from csord__Subscription__c where OpportunityRelatedList__c =: SubforOpp[0].OpportunityRelatedList__c ];
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'you can not create single bundle'));
        }
    }
    public PageReference serviceBundle()
    {
     if(selected.contains(subid))
     {
     selected.remove(subid);
     }
     else
     {
     selected.add(subid);
     }
     system.debug('@@@selected'+selected);
     return null;
    }
    public PageReference updateSerItems()
    {
        List<csord__Service__c> UpdateServices = new List<csord__Service__c>();
        if(SubObj != null)
        {
         
            for( csord__Service__c obj : ChildServices ) 
            {      
              if(obj.Parent_Bundle_Flag__c == True)  
              {
                parentbundleid.add(obj.id);
              }   
            }
            system.debug('!!!!'+parentbundleid);
            if(parentbundleid.isEmpty())
            {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Records cannot be saved without selecting Parent Bundle Flag'));
            }
             if(!parentbundleid.isEmpty())
             {
              parservice = [select id,Bundle_Label_name__c,Bundle_Default_Value__c from csord__Service__c where id in :parentbundleid];
             }
            for( csord__Service__c objs : ChildServices ) 
            {  
              objs.Bundle_Flag__c = objs.Bundle_Flag__c;
              objs.Parent_Bundle_Flag__c = objs.Parent_Bundle_Flag__c;
              if(parservice!=null && objs.Bundle_Flag__c == true)
              {
              objs.Bundle_Label_name__c = parservice.Bundle_Default_Value__c;
              }
              else
              {
              objs.Bundle_Label_name__c = objs.Bundle_Default_Value__c;
              }
              system.debug('@#$'+objs.Bundle_Label_name__c+'$$$$'+objs.Bundle_Default_Value__c);
              UpdateServices.add(objs);
            }
        }
        if(UpdateServices.size()>0)
        {
        update UpdateServices;
        parentbundleid.clear();
       
        }
        
       PageReference ReturnPage = new PageReference('/' + SubObj); 
       ReturnPage.setRedirect(true); 
       return ReturnPage;
    }
       public PageReference updateAllSerItems(){
        List<csord__Service__c> UpdateAllServices = new List<csord__Service__c>();
        system.debug('!!!!'+SubObj + '####'+AllServices.size());
        if(SubObj != null)
        {
            for(csord__Service__c objparent : AllServices) 
            {            
            system.debug('!!!!'+objparent + '####'+objparent.Parent_Bundle_Flag__c);
               if(objparent.Parent_Bundle_Flag__c == True)  
              {
                parentbundleids.add(objparent.id);
                system.debug('!!!!'+parentbundleids);
              }   
            }
            if(parentbundleids.isEmpty())
            {
             ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Records cannot be saved without selecting Parent Bundle Flag'));
            }
            if(!parentbundleids.isEmpty())
             {
              parservices = [select id,Bundle_Label_name__c,Bundle_Default_Value__c from csord__Service__c where id in :parentbundleids];
             }
            for( csord__Service__c objsparen : AllServices ) 
            {  
              objsparen.Bundle_Flag__c = true;
              objsparen.Parent_Bundle_Flag__c = objsparen.Parent_Bundle_Flag__c;
              if(parservices!=null && objsparen.Bundle_Flag__c == true)
              {
               objsparen.Bundle_Label_name__c = parservices.Bundle_Default_Value__c;
              }
              else
              {
              objsparen.Bundle_Label_name__c = objsparen.Bundle_Default_Value__c;
              }
              system.debug('@#$'+objsparen.Bundle_Label_name__c+'$$$$'+objsparen.Bundle_Default_Value__c);
              UpdateAllServices.add(objsparen);
            }
        }
        if(UpdateAllServices.size()>0)
        {
        update UpdateAllServices;
        parentbundleids.clear();
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Bundling done successfully'));
        }
       PageReference ReturnPage = new PageReference('/' + SubObj); 
       ReturnPage.setRedirect(true); 
       return ReturnPage;
    } 
    public PageReference updateAllSerItemsUnbundle(){
        List<csord__Service__c> UpdateAllServicesUnbundle = new List<csord__Service__c>();
        system.debug('!!!!'+SubObj + '####'+AllServices.size());
        if(SubObj != null)
        {
            for( csord__Service__c objsparenunbundle : AllServices ) 
            {  
              objsparenunbundle.Bundle_Flag__c = false;
              objsparenunbundle.Parent_Bundle_Flag__c = false;
              objsparenunbundle.Bundle_Label_name__c = objsparenunbundle.Bundle_Default_Value__c;
              system.debug('@#$'+objsparenunbundle.Bundle_Label_name__c+'$$$$'+objsparenunbundle.Bundle_Default_Value__c);
              UpdateAllServicesUnbundle.add(objsparenunbundle);
            }
        }
        if(UpdateAllServicesUnbundle.size()>0)
        {
        update UpdateAllServicesUnbundle;
        }
       PageReference ReturnPage = new PageReference('/' + SubObj); 
       ReturnPage.setRedirect(true); 
       return ReturnPage;
     } 
public void SetParentBundle()
    {
        ParentBundle = null;
          
        for (csord__Service__c con : AllServices)
        {
            if (con.Parent_Bundle_Flag__c)
            ParentBundle = con;
        }
    }
    
public void SetBundle()
    {
        Bundle = null;
          
        for (csord__Service__c cons : ChildServices)
        {
            if (cons.Parent_Bundle_Flag__c)
            Bundle = cons;
        }
    }
    public PageReference ServiceBundleCall()
    {
     AllServices = [select id, Name, csord__Subscription__r.csordtelcoa__Subscription_Number__c, Primary_Service_ID__c, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c, Opportunity__c from csord__Service__c where csord__Subscription__c in : selected];
     system.debug('@@@@'+AllServices.size());
     PageReference pageRef = new PageReference('/apex/BillModifyCreateParent');
     pageRef.setRedirect(false);
     return pageRef;
    }
}