@isTest (seealldata = false)
public class BillModifyCreateClassTest {

  public static void initTestData(){
  
        P2A_TestFactoryCls.sampletestdata();
        
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        
        List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);
       
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
       // List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist);
       // list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist, configLst, ScreenSec);
        list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,proconfigs);
        list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
        List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        system.assertEquals(true,services!=null); 
        Test.startTest();
        
        PageReference pageRef = Page.BillModifyCreate;
        Test.setCurrentPage(pageRef);
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(SUBList[0]);
        ApexPages.currentPage().getParameters().put('Id',SUBList[0].id);
        
       // Apexpages.StandardController stdController = new Apexpages.StandardController(SUBList[0]);
        BillModifyCreateClass bmc = new BillModifyCreateClass(sc);
        
        bmc.AllServices = services;
       // BillModifyCreateClass bmc = new BillModifyCreateClass();
        bmc.serviceBundle();
        bmc.updateSerItems();
        bmc.updateAllSerItems();
       
        bmc.updateAllSerItemsUnbundle();
       
        bmc.SetParentBundle();
        bmc.SetBundle();
        bmc.ServiceBundleCall();
        system.assertEquals(true,bmc!=null); 
        Test.stopTest();
        }
      private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        integer userid = 0;
        try{
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            initTestData();
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='BillModifyCreateClassTest :doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }
        
   }