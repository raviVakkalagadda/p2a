/**
    @author - Accenture
    @date - 19- Jul-2012
    @version - 1.0
    @description - This is the test class for BillProfileInsertUpdate Trigger.
*/
@isTest(SeeAllData = false)
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
private class BillProfileInsertUpdateTest
//End
{
static Account acc;
static Country_Lookup__c cl;
static BillProfile__c b;
static BillProfile__c b1;   
static Site__c s;
static City_Lookup__c ci;
static Contact con;

    static testMethod void testUpdateBillProfile(){
        b = getBillProfile();
        
       // b1 = getBillProfile1();
        b.Bill_Profile_ORIG_ID_DM__c='1234';
        update b;
        system.assert(b!=null);
   
    
    }
private static Account getAccount(){
    if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Name = 'Test Account1111111';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Customer_Legal_Entity_Name__c='Test';
        insert acc;
        system.assert(acc!=null);
    }
    return acc;
}
private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
        system.assert(cl!=null);
    }
    return cl;
} 
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
        system.assert(ci!=null);
    }
    return ci;
}
        
private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        con = getContact();
        acc = getAccount();
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Incorporated';
       // b.Account__c = s.AccountId__c;
        b.Account__c= acc.Id;
        
        //b.Bill_Profile_Site__c = s.Id;
       // b.Operating_Unit__c = 'TI_HK_I52S'; 
        b.First_Period_Date__c = date.newinstance(2012, 12, 11);
        b.Invoice_Breakdown__c='Cost Centre';
        b.Start_Date__c =  System.today();
        b.Bill_Profile_Site__c = s.id;
        b.Status__c ='Active';
        b.Activated__c = True;
            b.Approved__c = True;
            //b.Approved_By__c = ac.ownerId;
       // b.Country__c = 'US';
        
        b.Billing_Contact__c = con.id;
       // b.Customer_Ref__c = 'Test';
       b.Bill_Profile_ORIG_ID_DM__c= '123445';
       insert b;
       system.assert(b!=null);
           
    }
        return b;
}

private static BillProfile__c getBillProfile1(){
    if(b1 == null) {
        b1 = new BillProfile__c();
        s = getSite();
        con = getContact();
        acc = getAccount();
       // b.Bill_Profile_Number__c = 'Test Bill Profile';
        b1.Billing_Entity__c = 'Telstra Limited';
        b1.Account__c= acc.Id;
        b1.Bill_Profile_Site__c = acc.Id;
       // b.Operating_Unit__c = 'TI_HK_I52S'; 
        b1.First_Period_Date__c = date.newinstance(2012, 12, 11);
        b1.Invoice_Breakdown__c='Cost Centre';
        b1.Start_Date__c =  System.today();
        b1.Bill_Profile_Site__c = s.id;
        
        b1.Billing_Contact__c = con.id;
       // b.Customer_Ref__c = 'Test';
       b1.Bill_Profile_ORIG_ID_DM__c= '4567';
       insert b1;
       system.assert(b1!=null);
           
    }
        return b1;
}

private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        Account acc1 = getAccount();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        cl = getCountry();
        s.Country_Finder__c = cl.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        // s.Address_Type__c='Billing Address';
        s.AccountId__c =  acc1.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
        system.assert(s!=null);
    }
    return s;
}   

private static Contact getContact(){
    if(con == null){
        con = new Contact();
        acc = getAccount();
        con.AccountID = acc.id;
        //con.Name = 'TestCont1';
        con.FirstName = 'Test';
        con.LastName = 'cont1';
        con.Salutation = 'Mr';
        con.Job_Title__c = 'CEO';
        con.Email = 'test.t@test.com';
        con.Country__c = getCountry().id;   
        system.assert(con!=null);
    }
    return con;


}
}