@isTest
private with Sharing class BillProfileTriggerHandler_Test{
    
    private static list<Global_Constant_Data__c> GcdataList;
    private static list<NewLogo__c> NewLogolist;
    
    @testSetup
    public static void setupTestData() {
    }
    public static testMethod void BillProfileTriggerHandlrMethod() {
        
        List<Country_Lookup__c> countryList =  P2A_TestFactoryCls.getcountry(1); 
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Contact> contList = P2A_TestFactoryCls.getContact(1, accList);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, accList, countryList);
        List<BillProfile__c>  billProfList = P2A_TestFactoryCls.getBPs(1, accList ,siteList,contList);
        List<Action_Item__c> inActIteList = P2A_TestFactoryCls.getActIteList(1);
        Map<Id,BillProfile__c> billProfMap = new Map<Id,BillProfile__c>();
        Map<Id,BillProfile__c> billProfMap1 = new Map<Id,BillProfile__c>();
        Map<Id,BillProfile__c> billProfMap2 = new Map<Id,BillProfile__c>();
        GcdataList = new List<Global_Constant_Data__c>{
        new Global_Constant_Data__c(name = 'Action Item Priority', Value__c = 'high'),
        new Global_Constant_Data__c(name = 'Action Item Status', Value__c = 'Assigned')};
        insert GcdataList;
        List<Global_Constant_Data__c> gclist = [select id,name from Global_Constant_Data__c where name = 'Action Item Priority'];
        System.assert(gclist!=null);
        system.assertEquals(gclist[0].name, GcdataList[0].name);
        
        inActIteList[0].Bill_Profile__c = billProfList[0].id;
        //inActIteList[0].status__c = 'Assigned';
        update inActIteList;
        
        accList[0].Customer_Type__c='ISO';
        update accList;
        
        System.assert(inActIteList!=null);
        System.assertEquals('ISO',accList[0].Customer_Type__c);     
        
        BillProfile__c bp = new BillProfile__c();
        bp.id = billProfList[0].id;
        bp.Payment_Terms__c = billProfList[0].Payment_Terms__c;
        
        billProfMap2.put(bp.id, bp);
        
        billProfList[0].Payment_Terms__c = '15 Days';
        billProfList[0].Activated__c = true;
        billProfList[0].Display_Data_Itemisation__c=false;      
        billProfList[0].Bill_Profile_ORIG_ID_DM__c='dfsdfs';
        RecordType rt = [select Id,Name from RecordType where Name='ISO Bill Profile RT'];
        billProfList[0].RecordTypeId=rt.id;
        billProfList[0].E_bill_Email_Billing_Address_CC__c='dfdsf';
        billProfList[0].CDR_Email_Address__c='fsdfsd@acc.com';
        billProfList[0].CDR_Email_Address_CC__c='fsdfsd@acc.com';
        billProfList[0].E_bill_Email_Billing_Address__c='fsdfsd@acc.com';
        update billProfList;
        System.assert(billProfList[0].Itemisation_Display_Level__c == null);
        System.assert(billProfList!=null);
        System.assert(billProfList[0].Bill_Profile_ORIG_ID_DM__c!=null);
        
        
        for(BillProfile__c billProf : billProfList)
        {
            billProfMap.put(billProf.Id,billProf);          
        }
        BillProfileTriggerHandler  billProfTrgHdlr = new BillProfileTriggerHandler();
        Test.startTest();
        billProfTrgHdlr.beforeUpdate(billProfList, billProfMap, billProfMap);
        billProfTrgHdlr.updateContractDocuments(billProfList, billProfMap2);
        //billProfTrgHdlr.updateItemisationDisplayLevel(billProfList, billProfMap);
        try{
            billProfTrgHdlr.updateItemisationDisplayLevel(billProfList, billProfMap1);
        }catch(Exception e){}
        
        Test.stopTest();        
    }
   
}