@isTest(seealldata = false)
    public class BillProfileUpdateTest {
        
      public static void updateBillProfileTest() { 
            P2A_TestFactoryCls.sampleTestData(); 
            List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
            List<Country_Lookup__c> ctrLok = P2A_TestFactoryCls.getcountry(1);
            List<Site__c> sites = P2A_TestFactoryCls.getsites(1,accList,ctrLok);
            List<Contact> contList = P2A_TestFactoryCls.getContact(1,accList);
            List<BillProfile__c> billprf = P2A_TestFactoryCls.getBPs(1,accList,sites,contList);
            List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
            List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
            List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
            List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
            List<cscfga__Configuration_Screen__c>  ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
            List<cscfga__Screen_Section__c> ssList1 =  P2A_TestFactoryCls.getScreenSec(1,configLst);
            List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,ConfigLst,ssList1);
            list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
            List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
            List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
            List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
            List<User> users = P2A_TestFactoryCls.get_Users(1);
            List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
            list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
           List<csord__Service__c> service = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList); 
           List<csord__Service__c> service1 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        List<csord__Service__c> service2 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        List<csord__Service__c> service3 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);      
           List<csord__Service_Line_Item__c> lineitem = P2A_TestFactoryCls.getSerLineItem(1,service,OrdReqList);
           List<csord__Service_Line_Item__c> lineitem1 = P2A_TestFactoryCls.getSerLineItem(1,service1,OrdReqList);
            U2C_MasterCode_Mapping__c u2c = new U2C_MasterCode_Mapping__c(Name='VLP',Product_Id__c='VLP-Onnet',
                                                InternetSingleHomeOrMultiHome__c='aaa',U2CMasterCode__c='IPTS-C:IPTS-STD:IPTCUSTSITE:DOSP');
                insert u2c;
              system.assert(u2c!=null);
            CostCentre__c cst = new CostCentre__c(name='aaa',BillProfile__c=billprf[0].id,Cost_Centre_Status__c='Active');
            insert cst;
            system.assert(cst!=null);
            Orders[0].Status__c='Completed';
            update Orders;
            system.assert(Orders!=null);
           lineitem[0].Is_Miscellaneous_Credit_Flag__c=false;
           lineitem[0].csord__Is_Recurring__c=true;  
           //lineitem[0].csord__Is_Recurring__c=false;   
           update lineitem;
           system.assert(lineitem!=null);
           lineitem1[0].Is_Miscellaneous_Credit_Flag__c=false;
           lineitem1[0].csord__Is_Recurring__c=true;       
           update lineitem1;
           system.assert(lineitem1!=null);
           service[0].csord__Order__c=Orders[0].id;
           service[0].Product_Code__c='VLP';
           service[0].Product_Id__c='VLP-Onnet';     
           service[0].InternetBillingType__c='aaa';
           //service[0].U2CInternetdiff__c='ggg';
           service[0].InternetPrimaryBackup__c='ggg';
           service[0].Parent_Bundle_Flag__c=true;
           service[0].AccountId__c=accList[0].id;
           service[0].Bill_ProfileId__c=billprf[0].id;
           service[0].Cost_Centre_Id__c=cst.id;    
           //service[0].Cease_Service_Flag__c = true;
           service[0].csordtelcoa__Product_Configuration__c=proconfigs[0].id;
           service[0].csordtelcoa__Replaced_Service__c=service1[0].id;
           update service;
           system.assert(service!=null);
           //System.assertequals(service[0].csord__Order__r.Status__c,u2c.InternetPrimaryOrBackup__c);
           System.assertequals(service[0].InternetBillingType__c,u2c.InternetSingleHomeOrMultiHome__c);
           
           service1[0].Product_Code__c='VLP';
           service1[0].Product_Id__c='DOSP';    
           service1[0].csordtelcoa__Replaced_Service__c=service2[0].id;
           update service1;
           
           service2[0].Product_Code__c='VLP';
           service2[0].Product_Id__c='DOSP';
           service2[0].csordtelcoa__Replaced_Service__c=service3[0].id;
           update service2;
           
           //service3[0].Product_Code__c='VLP';
           service3[0].Product_Id__c='DOSP';       
           update service3;
              
           //System.assertNotEquals(null,service[0].csordtelcoa__Product_Configuration__r.Name);
           System.assertNotEquals(null,service[0].Product_Code__c);
           
         // List<csord__Service__c> services = [select Id from csord__Service__c];
            Set<Id> serviceIds = new Set<Id>();
            serviceIds.add(service[0].id);
            /*integer i = 0;
            for(csord__Service__c serv :[select Id from csord__Service__c]){
               i++;
                serviceIds.add(serv.Id);
                if (i==50)
                {
                  break;
                  }
            }*/
            
           /* csord__Service__c service_c= services[0];
            Id service_c_id = service_c.id;
            Set<Id> tempSet = new Set<Id>();
            tempSet.add(service_c_id );*/
            
            Test.startTest();
          
            BillProfileUpdate bpUpdate = new BillProfileUpdate();
            bpUpdate.uPdateBillProfile(serviceIds);
            BillProfileUpdate.uPdateBillProfileService(service);
           BillProfileUpdate.uPdateBillProfileServiceLineItem(lineitem);
           system.assert(serviceIds!=null);
      
            Test.stopTest();
        }
      private static testMethod void Test() 
      {
                 
                  Exception ee = null;
                  Integer userid = 0;
                  try{
                 P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
                 system.assert(userid!=null);
                 updateBillProfileTest();
                 
                 } catch(Exception e){
                ee = e;
            } finally {
                P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
                if(ee != null){
                    throw ee;
                }
                
        }            
      } 
      
      @istest
      public static void unittest()
      {  
      List<csord__Subscription__c> Subscriptionlist = new List<csord__Subscription__c>();
      csord__Subscription__c sub = new csord__Subscription__c();
      sub.Name ='Test'; 
     sub.csord__Identification__c = 'Test-Catlyne-4238362';
     //sub.csord__Order_Request__c = i<OrdReqList.size() ? OrdReqList[i].id : OrdReqList[0].id;
     sub.OrderType__c = null;
     Subscriptionlist.add(sub);
     insert Subscriptionlist;
     List<csord__Subscription__c> su = [Select id,name from csord__Subscription__c where name = 'Test'];
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
     List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
     List<csord__Service__c> servlist = new List<csord__Service__c>();
     system.assertequals(sub.name, su[0].name); 
      //List<csord__Service__c> servlist = new List<csord__Service__c>();
         csord__Service__c ser = new csord__Service__c();
                    ser.Name = 'Test Service'; 
                    ser.csord__Identification__c = 'Test-Catlyne-4238362';
                   // ser.csord__Order_Request__c = i<OrdReqList.size() ? OrdReqList[i].id : OrdReqList[0].id;
                    ser.csord__Subscription__c =  Subscriptionlist[0].id;
                    ser.Billing_Commencement_Date__c = System.Today();
                    ser.Stop_Billing_Date__c = System.Today();
                    ser.RAG_Status_Red__c = false ; 
                    ser.RAG_Reason_Code__c = '';  
                    ser.csord__Order__c=Orders[0].id;
                    ser.Cascade_Bill_Profile__c=false;
                    
                    // if(ser.id != null)
                   // ser.csordtelcoa__Product_Basket__c = i<ProdBaskList.size()? ProdBaskList[i].id : ProdBaskList[0].id;  
                    ser.Product_Id__c = 'IPVPN';     
                    ser.Path_Instance_ID__c = 'test';                 
                    servlist.add(ser); 
                    insert servlist;                
                               
                    system.assert(servlist!=null);
                    test.starttest();
                     
                     
                     servlist[0].Cascade_Bill_Profile__c=true;
                        update servlist;
                        
                    
                    Map<Id, csord__Service__c> oldServicesMap = new Map<Id, csord__Service__c>();
                     oldServicesMap.put(ser.id,ser); 
                     //System.assertequals(false,oldServicesMap.get(ser.id).Cascade_Bill_Profile__c);
                                
                    System.assertequals(true,servlist[0].Cascade_Bill_Profile__c);
                                    
                    BillProfileUpdate.updateCascadeBillprofile(servlist,oldServicesMap); 
                    test.stoptest();
     
      }
      @isTest
      public static void updateBillProfileTest11() { 
            P2A_TestFactoryCls.sampleTestData(); 
            List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);           
            List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
            List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
              List<Country_Lookup__c> ctrLok = P2A_TestFactoryCls.getcountry(1);
            List<Site__c> sites = P2A_TestFactoryCls.getsites(1,accList,ctrLok);
            List<Contact> contList = P2A_TestFactoryCls.getContact(1,accList);
            List<BillProfile__c> billprf = P2A_TestFactoryCls.getBPs(1,accList,sites,contList);
            List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
            List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
            List<cscfga__Configuration_Screen__c>  ConfigLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
            List<cscfga__Screen_Section__c> ssList1 =  P2A_TestFactoryCls.getScreenSec(1,configLst);
            List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,ConfigLst,ssList1);
            
            List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
            List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
            List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
            List<User> users = P2A_TestFactoryCls.get_Users(1);
            List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,null);
                        
            Test.startTest();
            List<csord__Service__c> service = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList); 
            List<csord__Service__c> service1 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
            List<csord__Service__c> service2 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
            List<csord__Service__c> service3 = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);      
            List<csord__Service_Line_Item__c> lineitem = P2A_TestFactoryCls.getSerLineItem(1,service,OrdReqList);
            List<csord__Service_Line_Item__c> lineitem1 = P2A_TestFactoryCls.getSerLineItem(1,service1,OrdReqList);
            U2C_MasterCode_Mapping__c u2c = new U2C_MasterCode_Mapping__c(Name='VLP',Product_Id__c='VLP-Onnet',
            InternetSingleHomeOrMultiHome__c='aaa',U2CMasterCode__c='IPTS-C:IPTS-STD:IPTCUSTSITE:DOSP');
            insert u2c;
            List<csord__Service__c> servlist = new List<csord__Service__c>();
            CostCentre__c cst = new CostCentre__c(name='aaa',BillProfile__c=billprf[0].id,Cost_Centre_Status__c='Active');
            insert cst;
            system.assert(cst!=null);
            Orders[0].Status__c='Completed';
            update Orders;
            
           lineitem[0].Is_Miscellaneous_Credit_Flag__c=false;
           lineitem[0].csord__Is_Recurring__c=true;  
           //lineitem[0].csord__Is_Recurring__c=false;   
           update lineitem;
           
           lineitem1[0].Is_Miscellaneous_Credit_Flag__c=false;
           lineitem1[0].csord__Is_Recurring__c=true;       
           update lineitem1;
              
           service[0].csord__Order__c=Orders[0].id;
           service[0].Product_Code__c='VLP';
           service[0].Product_Id__c='VLP-Onnet';     
           service[0].InternetBillingType__c='aaa';
           //service[0].U2CInternetdiff__c='ggg';
           service[0].InternetPrimaryBackup__c='ggg';
           service[0].Parent_Bundle_Flag__c=true;
           service[0].AccountId__c=accList[0].id;
           service[0].Bill_ProfileId__c=billprf[0].id;
           service[0].Cost_Centre_Id__c=cst.id;                 
           service[0].Cease_Service_Flag__c = true;
           service[0].Order_Type_Final__c='Parallel Upgrade';
           service[0].Cascade_Bill_Profile__c=true;
           //service[0].Order_Type__c='Subscription Creation';
           //service[0].Product_Configuration_Type__c=true;        
           service[0].csordtelcoa__Product_Configuration__c=proconfigs[0].id;
           service[0].csordtelcoa__Replaced_Service__c=service1[0].id;
           update service;
           system.assert(service!=null);
           Map<Id, csord__Service__c> oldServicesMap = new Map<Id, csord__Service__c>();
                oldServicesMap.put(service[0].id,service[0]); 
           
           System.assertequals('Parallel Upgrade',service[0].Order_Type_Final__c);
           System.assertequals(true,service[0].Cascade_Bill_Profile__c);
           System.assertequals(service[0].InternetBillingType__c,u2c.InternetSingleHomeOrMultiHome__c);
           
           service1[0].Product_Code__c='VLP';
           service1[0].Product_Id__c='DOSP';    
           service1[0].csordtelcoa__Replaced_Service__c=service2[0].id;
           update service1;
           system.assert(service1!=null);
           service2[0].Product_Code__c='VLP';
           service2[0].Product_Id__c='DOSP';
           service2[0].csordtelcoa__Replaced_Service__c=service3[0].id;
           update service2;
           system.assert(service2!=null);
           //service3[0].Product_Code__c='VLP';
           service3[0].Product_Id__c='DOSP';       
           update service3;
           system.assert(service3!=null); 
             
            Set<Id> serviceIds = new Set<Id>();
            serviceIds.add(service[0].id);
           
            /*BillProfileUpdate bpUpdate = new BillProfileUpdate();
            bpUpdate.uPdateBillProfile(serviceIds);
            BillProfileUpdate.uPdateBillProfileService(service);
            BillProfileUpdate.updateCascadeBillprofile(service,oldServicesMap);
            BillProfileUpdate.uPdateBillProfileServiceLineItem(lineitem);*/
          
           
            Test.stopTest();
        }
            
    }