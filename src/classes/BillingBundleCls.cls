public with sharing class BillingBundleCls {
    
    Public csord__Subscription__c SubObj;
    public Boolean pageRefresh {get; Set;}
    public List<csord__Service__c> ChildServices {get; set;}
    public List<csord__Service__c> AllServices {get; set;}
    //Public csord__Service__c updatedchildServices {get; set;}
    
    public BillingBundleCls(ApexPages.StandardController controller) {
       //pageRefresh=false;
        SubObj =  (csord__Subscription__c) controller.getrecord();        
        List<csord__Subscription__c> SubforOpp = [select id, OpportunityRelatedList__c from csord__Subscription__c where id =: SubObj.id];
        
        ChildServices = [select id, Name, csord__Subscription__r.csordtelcoa__Subscription_Number__c, Primary_Service_ID__c, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c from csord__Service__c where csord__Subscription__c=: SubObj.id];
       // system.debug('childservices'+ChildServices);
        if(SubforOpp[0].OpportunityRelatedList__c != null){
            AllServices = [select id, Name, csord__Subscription__r.csordtelcoa__Subscription_Number__c, Primary_Service_ID__c, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c, Opportunity__c from csord__Service__c where Opportunity__c =: SubforOpp[0].OpportunityRelatedList__c];
        }
        else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'you can not create single bundle'));
        }
    }
    
    public PageReference updateItems()
    {
        List<csord__Service__c> UpdateServices = new List<csord__Service__c>();
        system.debug('child====='+ChildServices);
        
        if(SubObj != null)
        {
        system.debug('intothis');
            for( csord__Service__c obj : ChildServices ) 
            {
           
                UpdateServices.add(obj); 
            }
            
            
            
            
        }
        
        List<csord__Service__c> UpdateAllServices = new List<csord__Service__c>();
        if(SubObj != null)
        {
            for( csord__Service__c obj : AllServices ) 
            {
            
                UpdateAllServices.add(obj); 
            }
           
            
        }
        
        if(UpdateServices.size()>0)
        {
        update UpdateServices;
        }
        if(UpdateAllServices.size()>0)
        {
         update UpdateAllServices;
        }
        system.debug('===test'+UpdateServices);
        pageRefresh = true;
       //PageReference ReturnPage = new PageReference('/' + SubObj.id); 
     //  ReturnPage.setRedirect(true); 
       // return ReturnPage;
        return null;
    }

    // Services from Subcription
    public void updateSubSer()
    {
        Map<ID,csord__Service__c> serviceIdMap  = new Map<ID,csord__Service__c>([select id, Name, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c, Opportunity__c from csord__Service__c where  id in :ChildServices]);    
         system.debug('====='+serviceIdMap  );
        csord__Service__c  parentObj  = null;        
        for(csord__Service__c  serviceObj : ChildServices)
        {
            if(serviceObj.Bundle_Flag__c && serviceObj.Parent_Bundle_Flag__c)
            {
                parentObj = serviceObj;
               // parentObj.Bundle_Flag__c=true;
               // parentObj.Parent_Bundle_Flag__c=true;
                break;
            }
        }
        
        if(parentObj != null)
        {
            for(csord__Service__c  serviceObj : ChildServices)
            {
                if(serviceObj.Bundle_Flag__c)
                {
                    csord__Service__c  tmpObj   = serviceIdMap.get(parentObj.id);
                     if(tmpObj != null)
                     {
                        serviceObj.Bundle_Label_name__c = tmpObj.Bundle_Label_name__c;
                    }                        
                }
                else
                {
                    csord__Service__c  tmpObj   = serviceIdMap.get(serviceObj.id);
                    
                    if(tmpObj != null)
                    {
                        serviceObj.Bundle_Label_name__c = tmpObj.Bundle_Label_name__c;
                    }    
                }
                
            }                        
        }            
    }

    // Services from Opp
    public void updateDetails()
    {
        Map<ID,csord__Service__c> serviceIdMap  = new Map<ID,csord__Service__c>([select id, Name, Bundle_Action__c, Bundle_Flag__c, Bundle_Label_name__c, Parent_Bundle_Flag__c, Opportunity__c from csord__Service__c where  id in :allServices]);    
        csord__Service__c  parentObj  = null;        
        for(csord__Service__c  serviceObj : allServices)
        {
            if(serviceObj.Bundle_Flag__c && serviceObj.Parent_Bundle_Flag__c)
            {
                parentObj = serviceObj;
                break;
            }
        }
        
        if(parentObj != null)
        {
            for(csord__Service__c  serviceObj : allServices)
            {
                if(serviceObj.Bundle_Flag__c)
                {
                    csord__Service__c  tmpObj   = serviceIdMap.get(parentObj.id);
                     if(tmpObj != null)
                     {
                        serviceObj.Bundle_Label_name__c = tmpObj.Bundle_Label_name__c;
                    }                        
                }
                else
                {
                    csord__Service__c  tmpObj   = serviceIdMap.get(serviceObj.id);
                    
                    if(tmpObj != null)
                    {
                        serviceObj.Bundle_Label_name__c = tmpObj.Bundle_Label_name__c;
                    }    
                }
                
            }                        
        }            
    } 


}