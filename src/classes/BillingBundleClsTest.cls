@isTest
private class BillingBundleClsTest{
    //private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    private static List<Offer_Id__c>offIdlist;
    private static List<NewLogo__c> NewLogolist;
    private static List<Account> Accountslist;
    //private static CSPOFA__Orchestration_Process_Template__c orchTemplate;
    
    static testMethod void UnitTest() {
     P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
     P2A_TestFactoryCls.sampleTestData();
     
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj; 
        List<cscfga__Product_Basket__c> pblist = P2A_TestFactoryCls.getProductBasket(2);   
        
                      
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Country_Lookup__c> countrylist =  P2A_TestFactoryCls.getcountry(1);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<CS_Bandwidth__c> ListBandwidth = P2A_TestFactoryCls.getBandwidths(1);  
        List<cscfga__Product_Configuration__c> pclist = P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    
        Account accObj = new Account();
        accObj.Name = 'abcd';
      //  accObj.Account_ORIG_ID_DM__c = '01pO0000000NZyu';
        accObj.Account_ID__c = '9090';
        accObj.Type ='Customer';
        accObj.Account_Status__c = 'Active';
        accObj.Selling_Entity__c = 'Telstra INC';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Activated__c = true;
        accObj.Customer_Type_New__c ='GW';
        accObj.Industry__c = 'Education';
        accObj.Customer_Legal_Entity_Name__c = 'CustomerLegacyEntity';
        insert accObj;    
    
        Opportunity o   = new Opportunity();                
        o.AccountId     = accObj.id;
        o.name          = 'Test Opp';
        o.stagename     = 'Identify & Define';
        o.CloseDate     = system.today() + 10;
        o.Pre_Contract_Provisioning_Required__c = 'No';
        insert o;
        
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        insert ordreq;
        
        csord__Subscription__c sub = new csord__Subscription__c();
        sub.Name ='Test'; 
        sub.csord__Identification__c = 'Test-Catlyne-4238362';
        sub.csord__Order_Request__c = ordreq.id;
        sub.OpportunityRelatedList__c = o.Id;
        sub.csordtelcoa__Subscription_Number__c = 'SN-0000697';
        sub.Bundle_Action__c = 'MODIFY';
        insert sub;
        
        
        
        csord__Service__c ser = new csord__Service__c();
        ser.Name = 'Test Service';
        ser.csordtelcoa__Product_Configuration__c = pclist.get(0).id; 
        ser.csordtelcoa__Product_Basket__c = pblist.get(0).Id;
        ser.Primary_Service_ID__c = 'SN-00006989';
        ser.Bundle_Action__c = 'New';
        ser.Bundle_Flag__c = true;
        ser.Opportunity__c = sub.OpportunityRelatedList__c;
        ser.Bundle_Label_name__c = 'CRG-0013261';
        ser.Parent_Bundle_Flag__c  = true;
        ser.csord__Identification__c = 'Service_' + pclist.get(0).id;
        ser.csord__Order_Request__c = ordreq.id;
        ser.csord__Subscription__c = sub.id;
        insert ser; 
        
        /*csord__Service__c ser2 = new csord__Service__c();
        ser2.Name = 'Test Service';
        ser2.csordtelcoa__Product_Configuration__c = pclist.get(0).id; 
        ser2.csordtelcoa__Product_Basket__c = pblist.get(0).Id;
        ser2.Bundle_Action__c = 'New';
        ser2.Bundle_Flag__c = true;
        ser2.Bundle_Label_name__c = 'CRG-0013891';
        ser2.Opportunity__c = sub.OpportunityRelatedList__c;
        ser2.csord__Service__c = ser.id;
        ser2.csord__Identification__c = 'Service_' + pclist.get(1).id;
        ser2.csord__Order_Request__c = ordreq.id;
        ser2.csord__Subscription__c = sub.id;
        insert ser2;
        
        ser2.Bundle_Label_name__c  = ser.Bundle_Label_name__c;
        update ser2; 
        
        csord__Service__c ser1 = new csord__Service__c();
        ser1.Name = 'Test Service';
        ser1.csordtelcoa__Product_Configuration__c = pclist.get(0).id; 
        ser1.csordtelcoa__Product_Basket__c = pblist.get(0).Id;
        ser1.Primary_Service_ID__c = 'SN-00006989';
        ser1.Bundle_Action__c = 'New';
        ser1.Bundle_Flag__c = False;
        ser1.Opportunity__c = sub.OpportunityRelatedList__c;
        ser1.Bundle_Label_name__c = 'CRG-00132689';
        //ser1.Parent_Bundle_Flag__c  = False;
        ser1.csord__Identification__c = 'Service_' + pclist.get(1).id;
        ser1.csord__Order_Request__c = ordreq.id;
        ser1.csord__Service__c = ser.id; 
        ser1.csord__Subscription__c = sub.id;
        insert ser1;
        
        ser1.Name = 'Test Service1';
        ser1.Bundle_Label_name__c = ser.Bundle_Label_name__c;
        ser1.Parent_Bundle_Flag__c  = true;
        update ser1; */
        
        //Start Test
        Test.startTest();
        
            PageReference pageRef = Page.cagerackprivateroomdetails; // Adding VF page Name here
            pageRef.getParameters().put('id', String.valueOf(sub.id));//for page reference
            Test.setCurrentPage(pageRef);
    
            ApexPages.currentPage().getParameters().put('id',sub.id);
            ApexPages.StandardController sc = new ApexPages.StandardController(sub);
            BillingBundleCls  ac = new BillingBundleCls(sc);
            
            ac.updateItems();
            ac.updateItems(); 
            ac.updateSubSer();
            ac.updateDetails();
            
         Test.stopTest();    
            boolean b = true;
            String id = ApexPages.currentPage().getParameters().get('id');
            System.assert(b,id==null); 
    
    }
 }