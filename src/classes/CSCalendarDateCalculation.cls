/*As part of production Ticket# TGCTASK0209037, commented //with sharing */
//public with sharing class CSCalendarDateCalculation {
public class CSCalendarDateCalculation {
	static final Integer WORKING_DAYS_PER_WEEK = 5;
	static final Integer SATURDAY = 6;
	static final Integer MINUTES_PER_DAY = 1440;
	static final Integer WEEKEND_LENGTH = 2;
	
	public static Datetime calculateEndDate(Datetime startTime, Integer duration) {
		//calculated the end date for a process
		// startTime -- dateTime at which process / step begins
		// duration -- duration of process in minutes
		// @return -- dateTime at which process / step ends
		Integer startDay = Integer.valueof(startTime.format('u'));
		Integer totalDays = duration / MINUTES_PER_DAY;
		Integer minutes = Math.mod(duration, MINUTES_PER_DAY);
		Integer weeks = totalDays / WORKING_DAYS_PER_WEEK;
		Integer daysToWeekEnd = SATURDAY - startDay + 1;
		Integer dayRemainder = Math.mod(totalDays, WORKING_DAYS_PER_WEEK); 

		// add weeks
		DateTime endTime = startTime.addDays(weeks * 7);

		//add extra days

		endTime = endTime.addDays(dayRemainder);


		//add remainder minutes
		endTime = endTime.addMinutes(minutes);
		Integer endDay = Integer.valueOf(endTime.format('u'));
		if (endDay >= SATURDAY) // if the last day rolls over to Saturday, forward to Monday
		{
		    endTime = endTime.addDays(WEEKEND_LENGTH);
		}
		return endTime;
    }
    public static DateTime calculateStartDate(DateTime endTime, Integer duration)
    {
    	//calculated the start date for a process
		// endTime -- dateTime at which process / step ends
		// duration -- duration of process in minutes
		// @return -- dateTime at which process / step begins
		if (duration == null) duration = 0;
    	Integer endDay = Integer.valueof(endTime.format('u'));
    	Integer totalDays = duration / MINUTES_PER_DAY;
    	Integer minutes =Math.mod(duration, MINUTES_PER_DAY);
    	Integer weeks = totalDays / WORKING_DAYS_PER_WEEK;
    	Integer daysFromWeekend = endDay;
    	Integer dayRemainder = Math.mod(totalDays, WORKING_DAYS_PER_WEEK);

    	//subtract weeks
    	DateTime startTime = endTime.addDays(-7 * weeks);

    	//subtract extra days
    	startTime = startTime.addDays(-1*dayRemainder);

    	//subtract remainder minutes
    	startTime = startTime.addMinutes(-1*minutes);

    	Integer startDay = Integer.valueOf(startTime.format('u'));
    	if (startDay >= SATURDAY)
    	{
    		startTime = startTime.addDays(-1*WEEKEND_LENGTH);
    	}
    	return startTime;
    }

}