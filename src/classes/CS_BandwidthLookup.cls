global class CS_BandwidthLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID) {
        // NOT IMPLEMENTED
        System.Debug('BandwidthLookup.apxc: doDynamicLookupSearch');
        // List<CloudSense_Bandwidth__c> bandwidths = Database.query('SELECT Name FROM CS_Bandwidth__c WHERE Name = 'STM4'');
        return null;
    }
    public override String getRequiredAttributes() { 
        return '[ "RateCardIDTemp" ]' /*, "ProductTypeFilter"*/; 
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds, Integer pageOffset, Integer pageLimit) {
        try {
            System.Debug('BandwidthLookup.apxc');
            System.Debug('Search Fields = ' + searchFields);
            
            String searchValue = searchFields.get('searchValue') +'%';
            String rcId = searchFields.get('RateCardIDTemp');
            //String ptIds = searchFields.get('ProductTypeFilter');
            
            System.Debug('Using searchValue = ' + searchValue);
            System.Debug('Using Rate Card Id = ' + rcId);                
            //System.Debug('Using Product Type Id = ' + ptIds);
            
            //List<String> ptList = ptIds.split('\\,');
            List<CS_Route_Bandwidth_Product_Type_Join__c> bandwidths = [select Id
                                                                            , Bandwidth_Name__c
                                                                            , CS_Bandwidth_Product_Type__c
                                                                            , CS_Route_Segment__c
                                                                            , Name 
                                                                        from 
                                                                        	CS_Route_Bandwidth_Product_Type_Join__c 
                                                                        where 
                                                                            CS_Route_Segment__c = :rcId and 
                                                                            Bandwidth_Name__c like :searchValue //and
                                                                            //CS_Route_Segment__r.Product_Type__c in :ptList
                                                                        order by 
                                                                        	Sequence_No__c];                            
            system.debug('bandwidths >> ' + bandwidths); 
            return bandwidths;
        } catch (Exception e) {
            System.Debug('Exception: '+e);
            return null;
        }
    }
}