@isTest(SeeAllData = false)
private class CS_BandwidthLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
   
    private static List<CS_Route_Bandwidth_Product_Type_Join__c> routeBandwidthProductTypeJoinList;
    
    
    private static void initTestData(){
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL'),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL')
        };
        
        insert routeSegmentList;
        system.assert(routeSegmentList!=null); 
        system.assertEquals(routeSegmentList[0].name,'Route Segment 1');
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128),
            new CS_Bandwidth__c(Name = 'STM4', Bandwidth_Value_MB__c = 256)
        };
        
        insert bandwidthList;
        system.assert(bandwidthList!=null); 
        system.assertEquals(bandwidthList[0].name,'64k');
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPL'),
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPL')
        };
        
        insert bandwidthProdTypeList;
        List<CS_Bandwidth_Product_Type__c> bwprod = [select name,CS_Bandwidth__c ,Product_Type__c from CS_Bandwidth_Product_Type__c where 
                                                     Name = 'Bandwidth Product Type 1' or Name = 'Bandwidth Product Type 2'];
        system.assert(bwprod!=null); 
        system.assertEquals(bandwidthProdTypeList,bwprod);  
        
        routeBandwidthProductTypeJoinList = new List<CS_Route_Bandwidth_Product_Type_Join__c>{
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 1', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, CS_Route_Segment__c = routeSegmentList[0].Id),
            new CS_Route_Bandwidth_Product_Type_Join__c(Name = 'Product Type Join 2', CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[1].Id, CS_Route_Segment__c = routeSegmentList[1].Id)
        };
        
        insert routeBandwidthProductTypeJoinList;     
        system.assert(routeBandwidthProductTypeJoinList!=null); 
        system.assertEquals(routeBandwidthProductTypeJoinList[0].name,'Product Type Join 1');     
    }
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
           // CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            searchFields.put('Bandwidth', bandwidthList[0].Id);
            searchFields.put('RateCardIDTemp', routeSegmentList[0].Id);
            searchFields.put('searchValue', '');
            Id[] excludeIds; 
            Integer pageOffset, pageLimit;
            String productDefinitionId;
       
            CS_BandwidthLookup csBandwidthLookup = new CS_BandwidthLookup();
            //Object[] data = csBandwidthLookup.doDynamicLookupSearch(searchFields, productDefinitionId); 
            Object[] data = csBandwidthLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit); 
            String reqAtts = csBandwidthLookup.getRequiredAttributes();
            system.assertEquals(true,csBandwidthLookup!=null);
            
            
             data = csBandwidthLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
             //System.debug('*******Data: ' + data);
             //System.assert(data.size() > 0, '');  
              Test.stopTest(); 
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_BandwidthLookupTest:doLookupSearchTest';         
            ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
           
           // CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}