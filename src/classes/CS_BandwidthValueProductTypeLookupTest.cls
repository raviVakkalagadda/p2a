@isTest(SeeAllData = false)
private class CS_BandwidthValueProductTypeLookupTest {

private static Map<String, String> searchFields = new Map<String, String>();
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
     
        private static void initTestData(){
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL'),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL')
        };
        
        insert routeSegmentList;
         System.assertEquals('Route Segment 1',routeSegmentList[0].Name );
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
           
        };
        
        insert bandwidthList;
         System.assertEquals('64k',bandwidthList[0].Name );
        
            bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPL'),
          new CS_Bandwidth_Product_Type__c(Name = 'Bandwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPL')
        };
        
        insert bandwidthProdTypeList; 
         System.assertEquals('Bandwidth Product Type 1',bandwidthProdTypeList[0].Name );
         }
    
      private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('ProductType', routeSegmentList[0].Id);
            searchFields.put('searchValue', '');
            Id[] excludeIds; 
            //Integer pageOffset, pageLimit;
            String productDefinitionId;
             Integer i1 = 1;
        Integer i2 = 2;
           

             CS_BandwidthValueProductTypeLookup  csBandwidthvalueProductTypeLookup = new CS_BandwidthValueProductTypeLookup();
             Object[] data = csBandwidthvalueProductTypeLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
          
            String reqAtts = csBandwidthvalueProductTypeLookup.getRequiredAttributes();
            Object[] datas = csBandwidthvalueProductTypeLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, i1, i2); 
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');   
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }

}