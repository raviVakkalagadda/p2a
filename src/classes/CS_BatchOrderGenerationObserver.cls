/**
 * Observer pattern implementation for batch order generation
 */
global with sharing class CS_BatchOrderGenerationObserver implements csordcb.ObserverApi.IObserver{
    
    public CS_BatchOrderGenerationObserver(){
        
    }
    
   global void execute(csordcb.ObserverApi.Observable o, Object arg){
        
        csordtelcoa.OrderGenerationObservable observable = (csordtelcoa.OrderGenerationObservable) o;
        List<Id> serviceIds = observable.getServiceIds();
        
        List<Id> orderIds = observable.getOrderIds();
        
        List<Id> subscriptionIds = observable.getSubscriptionIds();
       
        processServices(serviceIds);        
        processSubscriptions(subscriptionIds);
        processOrders(orderIds);
        
    }

    /**
     * Before and After insert trigger logic for Services
     * @param serviceIds
     */
   @TestVisible  
   public void processServices(List<Id> serviceIds){
       
        Map<Id, csord__Service__c> services = new Map<Id, csord__Service__c>();
        String serviceQuery = getAllFieldQuery('csord__Service__c',serviceIds);
        List<csord__Service__c> servicesList = database.query(serviceQuery.replace('XXX','serviceIds'));
		List<Id> subscriptionIds = new List<Id>();
		Id basketId = null;
        
        /** Moved from Before and After insert trigger.*/     
        List<csord__Service__c> serviceList = new List<csord__Service__c>();
        AllServiceTriggerHandler handler = new AllServiceTriggerHandler();
		
        serviceList = handler.setOpportunityAndAccountIDOnServiceAndSubs(servicesList);
        serviceList = new MacdHandler().markServicesWithAnUpdatedConfiguration(serviceList);
		serviceList = MasterServiceToServiceLogic.process(serviceList);
        for(csord__Service__c srvc :serviceList){
            services.put(srvc.Id, srvc);
			subscriptionIds.add(srvc.csord__Subscription__c);
        }

        /** Method to create Partial terminate Services. **/
        String subsQuery = getAllFieldQuery('csord__Subscription__c',subscriptionIds);
        for(csord__Service__c srvc :(new AllSubscriptionTriggerHandler().MACDFunctionality(database.query(subsQuery.replace('XXX','subscriptionIds'))))){
            services.put(srvc.Id, srvc);
        }

        serviceList = handler.updateTerminationInfotoService(services);
        serviceList = new ServiceScreenFlowUpdater().setScreenFlowName(serviceList);
        serviceList = handler.updateBillProfileAndCostCentre(serviceList,null);
		//serviceList = handler.updateSitesOnService(serviceList,null);
        serviceList = handler.setOrderTypeOnService(serviceList);		
        serviceList = handler.UpdateSolutionId(serviceList, null);
		serviceList = handler.resourceIdAndBilateralPathId(serviceList,null);
		serviceList = handler.updateSubsName(serviceList,null);
        serviceList = handler.setOpportunityAndAccountIDOnOrder(serviceList);
        serviceList = billProfileUPdate.uPdateBillProfileService(serviceList);
        serviceList = LinkAndAssociateServiceWithClonedPC.updateLinkedServiceId(serviceList);
        update serviceList;
        
		/** Transfer data from service lines originating from a cloned inflight basket back onto the original service lines **/
		for(csord__Service__c srvc :serviceList){
            if(srvc.csordtelcoa__Product_Basket__c != null && basketId == null){basketId = srvc.csordtelcoa__Product_Basket__c;}
        }
        if(basketId != null){InFlightManager.transferClonedServiceLineItemDetails(basketId);}        
    }
    
    /**
     * Before and After insert trigger logic for Subscriptions
     * @param subscriptionIds
     */
    @TestVisible 
    public void processSubscriptions(List<Id> subscriptionIds){
        
        List<Id> serviceIds = new List<Id>();
        Map<Id, csord__Subscription__c> subscriptions = new Map<Id, csord__Subscription__c>();
        String subsQuery = getAllFieldQuery('csord__Subscription__c',subscriptionIds);
        List<csord__Subscription__c> subscriptionsList = database.query(subsQuery.replace('XXX','subscriptionIds'));
        
        for(csord__Subscription__c subs :subscriptionsList){
            subscriptions.put(subs.Id, subs); 
            for(csord__Service__c ser :subs.csord__Services__r){
                serviceIds.add(ser.Id);
            }           
        }

        /** Moved from Before and After insert trigger.*/
        List<csord__Service__c> srvdList = new List<csord__Service__c>();
        List<csord__Subscription__c> subsList = new List<csord__Subscription__c>();
        AllSubscriptionTriggerHandler handler = new AllSubscriptionTriggerHandler();
        
        subsList = AllServiceTriggerHandler.setOrderTypeOnSubscription(serviceIds, subscriptions);
        subsList = handler.subcurrencymtd(subscriptionsList,null);
        subsList = handler.setCustomerRequiredDate(subscriptionsList);
        subsList = handler.changeSubscriptionOwner(subscriptionsList);
        subsList = handler.updateOrders(subscriptionsList,null);
        
        update subsList;
    }   

    /**
     * Before and After insert trigger logic for Orders
     * @param orderIds
     */
@TestVisible 
    public void processOrders(List<Id> orderIds){
        
        Map<Id, csord__order__c> orders = new Map<Id, csord__order__c>();
        Set<Id> SolutionSet = new Set<Id>();
        String orderQuery = getAllFieldQuery('csord__order__c',orderIds);
        List<csord__order__c> ordersList = database.query(orderQuery.replace('XXX','orderIds'));
        List<csord__service__c>MasterChildServList=new List<csord__service__c>();
        for(csord__order__c order :ordersList){
            orders.put(order.Id, order); 
            for(csord__Service__c ser :order.csord__Services__r){
                if(ser.Solutions__c != null){
                    SolutionSet.add(ser.Solutions__c);
                    MasterChildServList.add(ser);
                }
            }
        }
        if(MasterChildServList!=null){
		 AllServiceTriggerHandler handler=new AllServiceTriggerHandler();
         List<csord__service__c>tempList1=handler.updateSitesOnService(MasterChildServList,null);
		 AllServiceTriggerHandler.updateCityCountryCode(MasterChildServList);
         List<csord__service__c>tempList2=LinkAndAssociateServiceWithClonedPC.updateMasterChildLinkageOnServicesCreation(MasterChildServList);
        }
        if(!SolutionSet.IsEmpty()){
            AllserviceTriggerHandler.updateRoot(SolutionSet);
        }

        /** Moved from Before and After insert trigger.*/
        List<csord__order__c> orderList = new List<csord__order__c>();
        AllOrderTriggersHandler handler = new AllOrderTriggersHandler();
        
        orderList = handler.setSubscriptionID(ordersList);
        orderList = handler.setContactDetails(orderList,null);
        orderList = handler.setCustomerRequiredDate(orderList);

        update orderList;
    }      

    /**
     * Query all the fields from SObject.
     */
    @TestVisible 
    public static String getAllFieldQuery(String ObjectName, List<Id> IdSet){		
        /** Initialize setup variables*/
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();
		String sObjName;
		
		/** To identify the object type **/
		for(Id objectId :IdSet){
			sObjName = objectId.getSObjectType().getDescribe().getName();
		}		

        /** Grab the fields from the describe method and append them to the queryString one by one.*/
        for(String s :objectFields.keySet()) {
            query += ' ' + s + ',';
        }
        
        /** Manually add related object's fields that are needed.*/
        query += '(SELECT Id, Solutions__c, csordtelcoa__Product_Basket__c  FROM csord__Services__r)';
        
        /** Strip off the last comma if it exists.*/
        if(query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }

        /**Add FROM statement*/
        query += ' FROM ' + ObjectName;

        /** Add on a WHERE/ORDER/LIMIT statement as needed. Modify as needed */
        if(ObjectName == 'csord__Service__c' && sObjName == 'csord__Subscription__c'){
            query += ' WHERE csord__Subscription__c In:XXX';
        } else{
            query += ' WHERE Id In:XXX';
        }		
        
        /** Modify as needed*/
        return query;
    }   
}