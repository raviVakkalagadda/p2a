@IsTest
public class CS_BatchOrderGenerationObserverTest {
    @TestSetup
    static void setup() {
       // P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
        List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
       // List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
        
        List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
       // P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
         
    }
    
     @IsTest
    static void testObserver2() {
        /*List<csord__Subscription__c> subscriptions = [
            select id
            from csord__Subscription__c
        ];*/
        List<Id> objIds = new List<Id>();
        for (csord__Subscription__c srv :  [select id from csord__Subscription__c]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        system.assertEquals(true,csbog!=null);
        Test.startTest();
        csbog.processSubscriptions(objIds);
        Test.stopTest();
        
    }
    
    @IsTest
    static void testObserver3() {
        /*List<csord__Order__c> orders = [
            select id
            from csord__Order__c
        ];*/
        List<Id> objIds = new List<Id>();
        for (csord__Order__c srv :  [select id from csord__Order__c]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        system.assertEquals(true,csbog!=null);
        Test.startTest();
        csbog.processOrders(objIds);
        Test.stopTest();
        
    }
    
    @IsTest
    static void testObserver() {
        /*List<csord__Service__c> services = [
            select id
            from csord__Service__c
        ];*/
        List<Id> objIds = new List<Id>();
        for (csord__Service__c srv : [select id from csord__Service__c]) {
            objIds.add(srv.Id); 
        }
        CS_BatchOrderGenerationObserver csbog = new CS_BatchOrderGenerationObserver();
        system.assertEquals(true,csbog!=null);
        Test.startTest();
        csbog.processServices(objIds);
        Test.stopTest();
        
    }
    
   
}