public class CS_ConfigChangesHelper {
	
	public static void mapConfigChangesToService(List<Id> attributeIds) {
		//
		List<cscfga__Attribute__c> attributeList = [
        	select Id, name, cscfga__product_configuration__c, cscfga__attribute_definition__r.cscfga__output_mapping__c, 
				cscfga__attribute_definition__r.cscfga__type__c,
				cscfga__attribute_definition__r.cscfga__data_type__c,
				cscfga__value__c
			from cscfga__Attribute__c
			where cscfga__attribute_definition__r.cscfga__output_mapping__c != null
				and id in :attributeIds
        ];
        Set<Id> pcIds = new Set<id>();
        for (cscfga__Attribute__c att : attributeList) {
            pcIds.add(att.cscfga__product_configuration__c);
        }
        List<csord__Service__c> serviceList = [
            select csordtelcoa__product_configuration__c
			from csord__Service__c
			where csordtelcoa__product_configuration__c in :pcIds
        ];
        Map<String, List<csord__Service__c>> serviceByProductConfigIdMap = new Map<String, List<csord__Service__c>>();
		for (csord__Service__c svc : serviceList) {
			if (!serviceByProductConfigIdMap.containsKey(svc.csordtelcoa__product_configuration__c)) {
                serviceByProductConfigIdMap.put(svc.csordtelcoa__product_configuration__c, new List<csord__Service__c> {});
			}
           	serviceByProductConfigIdMap.get(svc.csordtelcoa__product_configuration__c).add(svc);
		}
		//
		Map<Id, SObject> serviceMap = new Map<Id, SObject>();
		List<SObject> serviceObjectList = new List<SObject>();
		for (cscfga__Attribute__c att : attributeList) {
            if (!serviceByProductConfigIdMap.containskey(att.cscfga__product_configuration__c)) {
                continue;
            }
			for (SObject serviceObject : serviceByProductConfigIdMap.get(att.cscfga__product_configuration__c)) {
				//if attribute is mapped to order field
                if (att.cscfga__attribute_definition__r.cscfga__output_mapping__c != null && att.cscfga__attribute_definition__r.cscfga__output_mapping__c != '') {
                	System.debug(' Attribute Name: '+att.Name);
                    System.debug(' Attribute cscfga__Output_Mapping__c: '+att.cscfga__Attribute_Definition__r.cscfga__Output_Mapping__c);
                    System.debug(' Attribute cscfga__Type__c: '+att.cscfga__Attribute_Definition__r.cscfga__Type__c);
                    System.debug(' Attribute cscfga__Data_Type__c: '+att.cscfga__Attribute_Definition__r.cscfga__Data_Type__c);
                    serviceObject.put(
	                    att.cscfga__attribute_definition__r.cscfga__output_mapping__c,
	                    getCorrectType(
                            att.cscfga__attribute_definition__r.cscfga__type__c, att.cscfga__attribute_definition__r.cscfga__data_type__c, att.cscfga__value__c
						)
	                );
	            }
	            serviceMap.put((Id) serviceObject.get('Id'), serviceObject);
	            //serviceObjectList.add(serviceObject);
	        }
		}
		if (!serviceMap.isEmpty()) {
			update serviceMap.values();
		}
	}
	
    /*
    public static void mapConfigChangesToAssets(List<Id> attFieldIds) {
		List<SObject> assetsToUpdate = new List<SObject>();
        List <cscfga__Attribute_Field__c> attrFields = [
			select id, cscfga__value__c, (select id from csordtelcoa__assets__r), (select id from csordtelcoa__salesforce_assets__r)
            from cscfga__Attribute_Field__c 
            where id in :attFieldIds
		];
		for (cscfga__Attribute_Field__c attrField :attrFields) {
			if (attrField.csordtelcoa__assets__r != null && attrField.csordtelcoa__assets__r.size() > 0) {
				for (csord__asset__c asset :attrField.csordtelcoa__assets__r) {
					asset.csord__asset_value__c = attrField.cscfga__value__c;
					assetsToUpdate.add(asset);
        		}
        	}

            if (attrField.csordtelcoa__salesforce_assets__r != null && attrField.csordtelcoa__salesforce_assets__r.size() > 0) {
				for (Asset asset :attrField.csordtelcoa__salesforce_assets__r) {
					asset.csord__asset_value__c = attrField.cscfga__value__c;
					assetsToUpdate.add(asset);
				}
			}
		}       
		update assetsToUpdate;
    }
    */
	@TestVisible
    private static object getCorrectType(string fieldType, string fieldDataType, string fieldValue) {
        //return correct type
        if (fieldDataType == 'Boolean')
        {
            if (fieldValue == null || fieldValue == '')
            {
                return false;
            }
            else if (fieldValue.toLowerCase() == 'no' || fieldValue.toLowerCase() == 'false')
                return false;
            else if (fieldValue.toLowerCase() == 'yes' || fieldValue.toLowerCase() == 'true')
                return true;
        } else 
        if (fieldDataType == 'Decimal' || fieldDataType == 'Double')
        {
            if (fieldValue == null || fieldValue == '')
            {
                return 0;
            } else if (fieldValue.toLowerCase() == '--none--') {
            	return null;
            }  else return decimal.valueOf(fieldValue);
        } else 
        if (fieldDataType == 'Integer')
        {
            if (fieldValue == null || fieldValue == '')
            {
                return 0;
            } else if (fieldValue.toLowerCase() == '--none--') {
            	return null;
            } else return integer.valueOf(fieldValue);
        } else 
        if (fieldType == 'Date' || (fieldType == 'Calculation' && fieldDataType == 'Date'))
        {
            if (fieldValue == null || fieldValue == '')
            {
                return null;
            } else if (fieldValue.toLowerCase() == '--none--') {
            	return null;
            } else {
                if (fieldDataType == 'Date') {
                    return Date.valueOf(fieldValue);
                } else {
                    return stringToDate(fieldValue);
                }
            }
        }
        return fieldValue;
    }
    @TestVisible
    private static Date stringToDate(String s){
        if (s != null) {
          //Date String is in the format dd/mm/yyyy
          String[] stringDate = s.split('/');
          if (stringDate.size() == 3) {
            Integer d =  Integer.valueOf(stringDate[0]);
            Integer m = Integer.valueOf(stringDate[1]);
            Integer y = Integer.valueOf(stringDate[2]);
            // Correct a problem introduced by webcom sending back 2 digit years
            if (y < 2000) y += 2000;
            return date.newInstance(y,m,d);
          } else {
            return null;
          }
        } else {
            return null;
        }
    }
}