@IsTest
public class CS_CustomButtonSynchronizeTest {
    
    @TestSetup
    static void setup() {
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
           
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware');
        insert prod;
        system.assert(prod!=null);
        
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 300, IsActive = true);
            pbe.UseStandardPrice = false;
        insert pbe;
        system.assert(pbe!=null);
        
        List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.StageName = 'Identify And Define';
        opp.CloseDate = System.today();
        opp.QuoteStatus__c = 'Approved';
        opp.Product_Type__c = 'IPL';
                opp.Estimated_MRC__c = 100.00;
                opp.Estimated_NRC__c = 100.00;
        Opplist.add(opp);
        insert OPplist;
        Global_Mute__c gm = new Global_Mute__c(
            Mute_Triggers__c = true
        );
        
        insert gm;
        OpportunityLineItem opplineitem = new OpportunityLineItem();
        List<OpportunityLineItem> opplineitemlist = new List<OpportunityLineItem>(); 
        opplineitem.Opportunityid = OppList[0].id;
        opplineitem.Quantity = 12;
        opplineitem.TotalPrice = 100;
        // opplineitem.PricebookEntryId = pbEntry.id; 
        opplineitem.PricebookEntryId = pbe.id;
        opplineitemlist.add(opplineitem);
        insert opplineitemlist;
        gm.Mute_Triggers__c = false;
        update gm;
        
        list<cscfga__Product_Basket__c> basketlist = new list<cscfga__Product_Basket__c>();
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        pb.csbb__Account__c = Acclist[0].id;
        pb.cscfga__Opportunity__c = opplist[0].id;
        basketlist.add(Pb);
        Insert pb;
        system.assert(pb!=null);
        
        cscfga__Product_Category__c cat = new cscfga__Product_Category__c(
            Name = 'Test Produc Category'
        );
        insert cat;
        system.assertEquals(cat.Name , 'Test Produc Category');    
        cscfga__product_definition__c pd = new cscfga__product_definition__c(
            name = 'Test',
            cscfga__Description__c = 'test',
            cscfga__Product_Category__c = cat.Id
        );
        insert pd;
        system.assertEquals(pd.Name , 'Test'); 
        cscfga__attribute_definition__c ad = new cscfga__attribute_definition__c(
            name = 'test',
            cscfga__product_definition__c = pd.Id
        );
        insert ad;
        system.assertEquals(ad.Name , 'test'); 
        cscfga__product_configuration__c pc = new cscfga__product_configuration__c(
            name = 'test',
            cscfga__product_definition__c = pd.Id,
            cscfga__product_basket__c = pb.Id,
            cscfga__total_one_off_charge__c = 0,
            cscfga__Total_Price__c = 0,
            cscfga__total_recurring_charge__c = 0,
            cscfga__Product_Family__c = 'Test'
        );
        insert pc;
        system.assertEquals(pc.Name , 'test');    
        cscfga__attribute__c at = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad.Id,
            cscfga__product_configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Price__c = 10,
            cscfga__List_Price__c = 10,
            name = 'test',
            cscfga__Recurring__c = true,
            cscfga__is_active__c = true,
            cscfga__Line_Item_Description__c = 'Test 123'
        );
        insert at;
        system.assert(at!=null);
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c(
            csbb__Product_Category__c = cat.Id, 
            csbb__Product_Configuration__c = pc.Id,
            csbb__Product_Basket__c = pb.Id
        );
        insert pcr;
        system.assert(pcr!=null);
        OLI_Sync__c osync = new OLI_Sync__c();
        insert osync;
        system.assert(osync!=null);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        list<OpportunityTeamMember> OppTeamList = new list<OpportunityTeamMember>();
        OpportunityTeamMember OppTeam = new OpportunityTeamMember();
        OppTeam.TeamMemberRole = 'Sales special list';
        OppTeam.UserId = users[0].id;
        Oppteam.OpportunityId = Opplist[0].id;
        OppTeamList.add(OppTeam);
        insert OppTeamList;
        system.assert(OppTeamList!=null);
    }
    
    @IsTest
    static void testSync() {
        List<cscfga__Product_Basket__c> baskets = [
            select id
            from cscfga__Product_Basket__c
        ];
        Test.startTest();
        CustomButtonSynchronizeWithOpportunity cbs = new CustomButtonSynchronizeWithOpportunity();
        cbs.performAction(baskets[0].id);
        system.assertEquals(true,cbs!=null);
        Test.stopTest();
        
        //CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(baskets[0].id);
        
    }
    
    @IsTest
    static void testSync2() {
        List<cscfga__Product_Basket__c> baskets = [
            select id
            from cscfga__Product_Basket__c
        ];
        
        CustomButtonSynchronizeWithOpportunity cbs = new CustomButtonSynchronizeWithOpportunity();
        system.assertEquals(true,cbs!=null);
        //cbs.performAction(baskets[0].id);
        Global_Mute__c gm = new Global_Mute__c();
        gm.Mute_Triggers__c = true;
        insert gm;
        for (cscfga__Product_Basket__c basket : baskets) {
            basket.csordtelcoa__Synchronised_with_Opportunity__c = true;
        }
        update baskets;
        gm.Mute_Triggers__c = false;
        update gm;
        Test.startTest();
        List<cscfga__attribute__c> atts = [
            select id, name, cscfga__product_configuration__c, cscfga__product_configuration__r.cscfga__Product_Basket__c,
                cscfga__is_active__c, cscfga__Is_Line_Item__c
            from cscfga__attribute__c
        ];
        system.debug('kk+' + JSON.serializePretty(atts));
        CustomButtonSynchronizeWithOpportunity.syncWithOpportunity(baskets[0].id);
        atts = [
            select id, name, cscfga__product_configuration__c, cscfga__product_configuration__r.cscfga__Product_Basket__c,
                cscfga__is_active__c, cscfga__Is_Line_Item__c
            from cscfga__attribute__c
        ];
        system.debug('kk+' + JSON.serializePretty(atts));
        Test.stopTest();
        
    }
}