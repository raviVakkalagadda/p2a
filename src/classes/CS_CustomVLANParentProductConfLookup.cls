global with sharing class CS_CustomVLANParentProductConfLookup extends cscfga.ALookupSearch{
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String vlans = searchFields.get('VLANs');

        String vlansInService = searchFields.get('VLANs From Service');
        
        System.Debug('CS_CustomVLANParentProductConfLookup : doDynamicLookupSearch searchFields: ' +vlans+' ---- '+ searchFields);
        
        String[] vlanIds = vlans.split(',');
        String[] vlanSerIds = vlansInService.split(',');
        /*List<cscfga__Product_Configuration__c> pcList = [SELECT Id, cscfga__Parent_Configuration__c 
                                                         FROM cscfga__Product_Configuration__c
                                                         WHERE  Id in :vlanIds];

        List<csord__Service__c> serList = [SELECT Id, csord__Service__c, Master_Service__c
                                                         FROM csord__Service__c
                                                         WHERE  Id in :vlanSerIds];*/

        String pcData = '';
        String serData = '';
    //System.Debug('CS_CustomVLANParentProductConfLookup : doDynamicLookupSearch pcList: ' + pcList);
        for (cscfga__Product_Configuration__c item : [SELECT Id, cscfga__Parent_Configuration__c 
                                                         FROM cscfga__Product_Configuration__c
                                                         WHERE  Id in :vlanIds]){
            if(pcData == ''){
                pcData += item.cscfga__Parent_Configuration__c;
            } else {
                pcData += ','+item.cscfga__Parent_Configuration__c;
            }
        }
        String masterServiceId = '';
        for (csord__Service__c item : [SELECT Id, csord__Service__c, Master_Service__c
                                                         FROM csord__Service__c
                                                         WHERE  Id in :vlanSerIds]){
            masterServiceId = item.Master_Service__c;
            if(serData == ''){
                serData += item.csord__Service__c;
            } else {
                serData += ','+item.csord__Service__c;
            }
        }

        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(cscfga__Validation_Message__c = pcData, cscfga__Description__c = serData, Name = masterServiceId);
        system.Debug('CS_CustomVLANParentProductConfLookup : doDynamicLookupSearch pc: ' + pc);
        return new cscfga__Product_Configuration__c[]{pc};
    }
    
    public override String getRequiredAttributes(){ 
        return '["VLANs", "VLANs From Service"]';
    }
}