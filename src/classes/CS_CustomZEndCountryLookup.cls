global with sharing class CS_CustomZEndCountryLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){ 
    	return '["Product Type", "A End Country", "LookupMode", "A End POP City"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Set <Id> countryIds = new Set<Id>();
        String productType = searchFields.get('Product Type');
        String aEndCountry = searchFields.get('A End Country');
        String lookupMode = searchFields.get('LookupMode');
        String aEndPOP = searchFields.get('A End POP City');

        System.Debug('Search field = ' + productType);
        List<CS_Route_Segment__c> routeSegList;
        if(lookupMode == '1'){

       		routeSegList = [SELECT Id, POP_A__r.CS_Country__c, POP_Z__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__r.CS_Country__c = :aEndCountry];
       	} else if(lookupMode == '2'){
       		routeSegList = [SELECT Id, POP_A__r.CS_Country__c, POP_Z__r.CS_Country__c
       											  FROM CS_Route_Segment__c
       											  WHERE Product_Type__c = :productType 
       											  		AND POP_A__r.CS_Country__c = :aEndCountry
       											  		AND POP_A__c = :aEndPOP ];
       	}

       	for(CS_Route_Segment__c item : routeSegList){
       		countryIds.add(item.POP_Z__r.CS_Country__c);
       	}
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_Country__c> data = [SELECT Id, Name FROM CS_Country__c WHERE  Id IN :countryIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }
}