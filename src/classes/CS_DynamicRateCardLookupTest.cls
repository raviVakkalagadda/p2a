@isTest(SeeAllData = false)
private class CS_DynamicRateCardLookupTest {

   private static List<CS_Country__c> countryList;
    private static List<CS_POP__c> popList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    private static List<CS_Resilience__c> resilienceList;
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static Object[] data = new List<Object>();
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countryList;
        List<CS_Country__c> counlist= [select id,name from CS_Country__c where name = 'Croatia'];
        system.assert(counlist!=null);
        system.assertEquals(countryList[0].name,counlist[0].name);
        System.debug('****Country List: ' + countryList);
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[2].Id),
            new CS_POP__c(Name = 'POP 4', CS_Country__c = countryList[3].Id),
            new CS_POP__c(Name = 'POP 5', CS_Country__c = countryList[4].Id)
        };
        
        insert popList;
        List<CS_POP__c> poplist1= [select id,name,CS_Country__c  from CS_POP__c where name = 'POP 1'];
        system.assert(poplist1!=null);
        system.assertEquals(poplist[0].name,poplist1[0].name);
        System.debug('****POP List: ' + popList);        
        
        resilienceList = new List<CS_Resilience__c>{
            new CS_Resilience__c(Name = 'Resilience 1'),
            new CS_Resilience__c(Name = 'Resilience 2'),
            new CS_Resilience__c(Name = 'Resilience 3'),
            new CS_Resilience__c(Name = 'Resilience 4'),
            new CS_Resilience__c(Name = 'Resilience 5')
        };
        
        insert resilienceList;
        List<CS_Resilience__c> reslist= [select id,name from CS_Resilience__c where name = 'Resilience 1'];
        system.assert(reslist!=null);
        system.assertEquals(resilienceList[0].name,reslist[0].name);
        System.debug('**** Resilience List: ' + resilienceList );
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL', POP_A__c = popList[0].Id, POP_Z__c = popList[4].Id, CS_Resilience__c = resilienceList[0].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL', POP_A__c = popList[1].Id, POP_Z__c = popList[3].Id, CS_Resilience__c = resilienceList[1].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'IPL', POP_A__c = popList[2].Id, POP_Z__c = popList[2].Id, CS_Resilience__c = resilienceList[2].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'EPL', POP_A__c = popList[3].Id, POP_Z__c = popList[1].Id, CS_Resilience__c = resilienceList[3].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EPLX', POP_A__c = popList[4].Id, POP_Z__c = popList[0].Id, CS_Resilience__c = resilienceList[4].Id)
        };
        
        insert routeSegmentList;
        List<CS_Route_Segment__c> routelist= [select id,name,Product_Type__c from CS_Route_Segment__c where name = 'Route Segment 1'];
        system.assert(routelist!=null);
        system.assertEquals(routeSegmentList[0].name,routelist[0].name);
        System.debug('****Route Segment List: ' + routeSegmentList); 
    }
    
  private static testMethod void doLookupSearchTest() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
      initTestData();
            
            searchFields.put('Product Type', 'IPL');
            searchFields.put('searchValue', '');
            searchFields.put('A End POP City', popList[0].Id);
            searchFields.put('Z End POP City', popList[4].Id);
            
            CS_DynamicRateCardLookup csDynamicRateCardLookup = new CS_DynamicRateCardLookup (); 
            String requiredAtts = csDynamicRateCardLookup.getRequiredAttributes();
            data = csDynamicRateCardLookup .doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assertEquals(true,csDynamicRateCardLookup!=null); 
            
            //System.debug('*******Data: ' + data);
            //System.assert(data.size() > 0, '');
              
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_DynamicRateCardLookupTest:doLookupSearchTest';      
            ErrorHandlerException.sendException(e);
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
  }

    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}