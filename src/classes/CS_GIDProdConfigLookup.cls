global class CS_GIDProdConfigLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["BasketId"]';
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            
            Object[] returnData = new List<Object>();
            Set<String> productIds = new Set<String>{ 'GIDSSTD','GIDSECO','GIDSECP' };
            String prodDefName = 'Internet-Onnet';
            String basketId = searchFields.get('BasketId');
            
            String query = 'SELECT Id, Name, Product_Definition_Name__c, cscfga__Product_Basket__c, Product_Id__c, Service_Bill_Text__c, (SELECT Id, Name FROM Product_Configurations1__r) '+
	                            'FROM cscfga__Product_Configuration__c ' +
	                            'WHERE cscfga__Product_Basket__c =: basketId ' + 
	                                'AND Product_Definition_Name__c =: prodDefName ' +
	                                'AND Product_Id__c IN : productIds';
	                            
	       List<cscfga__Product_Configuration__c> dataList = Database.query(query);
	       System.debug('***** Query Result size = ' + dataList.size() + '*****');
	       
	       for(cscfga__Product_Configuration__c pc : dataList){
	           if(pc.Product_Configurations1__r.size() == 0){
	               returnData.add(pc);
	           }
	       }
	       
	       System.debug('*****Number of Results = ' + returnData.size() + '*****'); 
	       return returnData;
        }
}