global class CS_GIDServiceLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["Account Id"]';
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            
            Object[] returnData = new List<Object>();
            Set<String> productIds = new Set<String>{'GIDSSTD','GIDSECO','GIDSECP'};
            String prodDefName = 'Internet-Onnet';
            String AccountId = searchFields.get('Account Id');
            String inventoryStatus = 'Decommissioned';
            
            String query = 'SELECT Id, Name, Product_Id__c, AccountId__c, csordtelcoa__Product_Configuration__r.Product_Definition_Name__c, Primary_Service_ID__c, Inventory_Status__c,  (SELECT Id, Name FROM Services1__r) '+
                                'FROM csord__Service__c ' +
                                'WHERE AccountId__c =: AccountId ' + 
                                    'AND csordtelcoa__Product_Configuration__r.Product_Definition_Name__c =: prodDefName ' +
                                    'AND Product_Id__c IN : productIds ' + 
                                    'AND Inventory_Status__c !=: inventoryStatus';
                         
           List<csord__Service__c> dataList = Database.query(query);
           System.debug('***** Query Result size = ' + dataList.size() + '*****');
                  
           for(csord__Service__c s : dataList){
               if(s.Services1__r.size() == 0){
                   returnData.add(s);
               }
           }
           
           System.debug('*****Number of Results = ' + returnData.size() + '*****'); 
           return returnData;
        }
}