global with sharing class CS_IPVPNPopLookup extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String cityID = searchFields.get('Pop City');
        String countryName = searchFields.get('Country Name');
        String accountID = searchFields.get('Account Id');
        String portSpeedID = searchFields.get('Port Speed');
        String IPVPNtype = searchFields.get('Port Type');
        String onnetOffnet = searchFields.get('Onnet or Offnet');
        String dynamicCoS = searchFields.get('Enable Dynamic CoS');
        System.Debug('CS_IPVPNPopLookup : doLookupSearch searchFields' + searchFields);
        List <CS_POP__c> data;
        Map <String, String> CoSmap= new Map <String, String> { 'CoS Critical Data' => 'IPVPN_Critical_Data__c', 'CoS Interactive Data' => 'IPVPN_Interactive_Data__c','CoS Low Priority Data' => 'IPVPN_Low_Priority_Data__c','CoS Standard Data' => 'IPVPN_Standard_Data__c','CoS Video' => 'IPVPN_Video__c','CoS Voice' => 'IPVPN_Voice__c'};
        if(IPVPNtype == 'RSA'){
        	Map <String, Object> wherePart = new Map <String, Object>();
        	wherePart.put('IPVPN_Low_Priority_Data__c', true);
        	wherePart.put('IPVPN_Remote_Site_Access_Gateway__c', true);
            if(cityID != '' && cityID != null){
                wherePart.put('CS_City__c', cityID);
            }
        	data = getPops(searchFields, wherePart);
        } else {
        	Map <String, Object> wherePart = new Map <String, Object>();
        	if(dynamicCoS == 'No'){
        		
        		for(String item : CoSmap.keySet()){
        			if(Integer.valueOf(searchFields.get(item)) > 0){
        				wherePart.put(CoSmap.get(item), true);
        			}
        		}
                if(cityID != '' && cityID != null){
                    wherePart.put('CS_City__c', cityID);
                }

        		data = getPops(searchFields, wherePart);
        	} else {
        		for(String item : CoSmap.keySet()){
        			wherePart.put(CoSmap.get(item), true);
        		}
                if(cityID != '' && cityID != null){
                    wherePart.put('CS_City__c', cityID);
                }
                
        		data = getPops(searchFields, wherePart);
        	}
        }
        
        return data;
	}

	public override String getRequiredAttributes(){ 
	    return '["Product Type", "Port Type","Pop City","Port Country", "Onnet or Offnet","CoS Video", "CoS Voice","CoS Critical Data","CoS Interactive Data","CoS Standard Data","CoS Low Priority Data", "Enable Dynamic CoS"]';
	}

	
	public List<CS_POP__c> getPops(Map<String, String> attributesValues, Map<String,Object> wherePart){
		
		String countryID = attributesValues.get('Port Country');
		String connectivity = attributesValues.get('Onnet or Offnet');
		String searchValue = attributesValues.get('searchValue') +'%';
        String cityID = attributesValues.get('Pop City');
	
		String query = 'SELECT Id, Name,'+
							'Active__c,'+
							'CS_City__c,'+
							'Connectivity__c,'+
							'CS_Country__c,'+
							'CS_Provider__c,'+
							'IPVPN_Critical_Data__c,'+
							'IPVPN_Interactive_Data__c,'+
							'IPVPN_Low_Priority_Data__c,'+
							'IPVPN_Remote_Site_Access_Gateway__c,'+
							'IPVPN_Secure_Mobile_Access__c,'+
							'IPVPN_Standard_Data__c,'+
							'IPVPN_Unmanaged_Internet_Access__c,'+
							'IPVPN_Video__c,'+
							'IPVPN_Voice__c,'+
							'IsIPVPN__c,'+
							'Onnet__c,'+
							'Address1__c,'+
							'Is_Vendor_Charge_Required__c,'+
                            'Offnet__c,'+
                            'Provider_Name__c,'+
							'Pop_City_Code__c,'+
							'Pop_CCMS_Country_Code__c,'+
                            'Type__c '+
						'FROM CS_POP__c '+
        				'WHERE CS_Country__c =:countryID'+
        					' AND Active__c = true'+
        					' AND Connectivity__c =:connectivity'+
        					' AND IsIPVPN__c = true'+
        					' AND Name LIKE :searchValue';
        					

     	System.Debug('CS_IPVPNPopLookup : getPops query1: ' + query);
        String whereQuery = '';
        for(String key : wherePart.keySet()){
        	Object value = wherePart.get(key);

        	if(value instanceof String){
        		whereQuery += ' AND '+key+' = \''+value + '\'';
        	} else if (value instanceof Integer || value instanceof Boolean){
        		whereQuery += ' AND '+key+' = '+value;
        	}
        }
        query += whereQuery + ' ORDER BY Name';
        System.Debug('CS_IPVPNPopLookup : getPops query2: ' + query);
		
        return Database.query(query);
	}
}