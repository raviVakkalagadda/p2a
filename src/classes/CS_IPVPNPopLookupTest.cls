@isTest(SeeAllData = false)
private class CS_IPVPNPopLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds = new List<Id>();
    private static Integer pageOffset, pageLimit;
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_Provider__c> providerList;
    private static List<CS_POP__c> popList;
    private static Object[] data = new List<Object>();
    
    private static void initData(){
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'USA'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'Australia')
        };
        
        insert countryList;
        system.assert(countryList!=null);
        system.assertEquals(countryList[0].name,'Croatia');
        System.debug('***** Country List ' + countryList);
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'New York', CS_Country__c = countryList[1].Id),
            new CS_City__c(Name = 'Bangalore', CS_Country__c = countryList[2].Id),
            new CS_City__c(Name = 'Hong Kong', CS_Country__c = countryList[3].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[4].Id)
        };
        
        insert cityList;
        system.assert(cityList!=null);
        system.assertEquals(cityList[0].name,'Zagreb');
        System.debug('***** City List ' + cityList);
        
        providerList = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'Test Provider 1', NAS_Product__c = Boolean.valueOf('true'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('false')),
            new CS_Provider__c(Name = 'Test Provider 2', NAS_Product__c = Boolean.valueOf('false'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('false'))
        };
        
        insert providerList;
        system.assert(providerList!=null);
        system.assertEquals(providerList[0].name,'Test Provider 1');
        System.debug('***** Provider List ' + providerList);
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('true'), CS_City__c = cityList[0].Id, Connectivity__c = 'Onnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true')),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('true'), CS_City__c = cityList[0].Id, Connectivity__c = 'Offnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true')),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[0].Id, Active__c = Boolean.valueOf('false'), CS_City__c = cityList[0].Id, Connectivity__c = 'Onnet',  IPVPN_Critical_Data__c = Boolean.valueOf('true'))
        };
        
        insert popList;
         List<CS_POP__c> pop= [select name,CS_Country__c ,Active__c,CS_City__c,Connectivity__c,IPVPN_Critical_Data__c from CS_POP__c where 
                                                     Name = 'POP 1' or Name = 'POP 2' or Name = 'POP 3'];
        system.assert(pop!=null); 
        system.assertEquals(popList,pop);  
        System.debug('***** Pop List ' + popList);
        
    }
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            searchFields.put('Port Type', 'POP');
            searchFields.put('Enable Dynamic CoS', 'Yes');
            searchFields.put('searchValue', '');
            searchFields.put('Onnet or Offnet', 'Onnet');
            searchFields.put('Port Country', countryList[0].Id);    
            searchFields.put('Port City', cityList[0].Id);
            
            System.debug('**** Search fields: ' + searchFields);
            
            CS_IPVPNPopLookup csIPVPNPopLookup = new CS_IPVPNPopLookup();
            data = csIPVPNPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            String reqAtts = csIPVPNPopLookup.getRequiredAttributes();
            system.assertEquals(true,csIPVPNPopLookup!=null); 
            System.debug('**** RETURN DATA****: ' + data);
            //System.assert(data.size() == 1, '');
            Test.stopTest();
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_IPVPNPopLookupTest:doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
            //Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }
    
        private static testMethod void doLookupSearchTest_v2() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            searchFields.put('Port Type', 'POP');
            searchFields.put('Enable Dynamic CoS', 'No');
            searchFields.put('searchValue', '');
            searchFields.put('Onnet or Offnet', 'Onnet');
            searchFields.put('Port Country', countryList[0].Id);    
            searchFields.put('Port City', cityList[0].Id);
            searchFields.put('CoS Critical Data', '1');
            searchFields.put('CoS Interactive Data', '1');
            searchFields.put('CoS Low Priority Data', '1');
            searchFields.put('CoS Standard Data', '1');
            searchFields.put('CoS Video', '0');
            searchFields.put('CoS Voice', '0');
            System.debug('**** Search fields: ' + searchFields);
            
            CS_IPVPNPopLookup csIPVPNPopLookup = new CS_IPVPNPopLookup();
            data = csIPVPNPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assertEquals(true,csIPVPNPopLookup!=null);
            System.debug('**** RETURN DATA****: ' + data);
            //System.assert(data.size() == 1, '');
            Test.stopTest();
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_IPVPNPopLookupTest:doLookupSearchTest_v2';         
            ErrorHandlerException.sendException(e);
            
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }
    
            private static testMethod void doLookupSearchTest_RSA() {
        Exception ee = null;

        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            searchFields.put('Port Type', 'RSA');
            searchFields.put('Enable Dynamic CoS', 'No');
            searchFields.put('searchValue', '');
            searchFields.put('Onnet or Offnet', 'Onnet');
            searchFields.put('Port Country', countryList[0].Id);    
            searchFields.put('Port City', cityList[0].Id);

            System.debug('**** Search fields: ' + searchFields);
            
            CS_IPVPNPopLookup csIPVPNPopLookup = new CS_IPVPNPopLookup();
            data = csIPVPNPopLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assertEquals(true,csIPVPNPopLookup!=null);
            System.debug('**** RETURN DATA****: ' + data);
            //System.assert(data.size() == 1, '');
             Test.stopTest();
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_IPVPNPopLookupTest:doLookupSearchTest_RSA';         
            ErrorHandlerException.sendException(e);
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

}