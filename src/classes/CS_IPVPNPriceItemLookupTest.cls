/*
Test class was converted as class - so re-created@17/2/2017
*/
@isTest(SeeAllData = false)
private class CS_IPVPNPriceItemLookupTest {
    
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
    private static List<Account> accountList;
    private static List<CS_BIP_Postcode_Zone__c> postcodeZoneList;
    private static List<CS_BIP_Zone__c> zoneList;
    private static List<CS_Postcode__c> postCodeList;
    private static List<CS_Provider__c> providers;
    
    private static void initTestData(){
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'New Zealand')
        };
        
        insert countryList;
        System.debug('****Country List: ' + countryList);
        System.assertEquals('Croatia',countryList[0].Name );
        
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Zagreb', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Sydney', CS_Country__c = countryList[1].Id)
        };
        
        insert cityList;
        System.debug('****City List: ' + cityList);
         System.assertEquals('Zagreb',cityList[0].Name );
        
        
        zoneList = new List<CS_BIP_Zone__c>{
            new CS_BIP_Zone__c(Name = 'Zone 1'),
            new CS_BIP_Zone__c(Name = 'Zone 2')
        };
        
        insert zoneList;
        System.debug('****Zone List: ' + zoneList);
         System.assertEquals('Zone 1',zoneList[0].Name );
        
        postCodeList = new List<CS_Postcode__c>{
            new CS_Postcode__c(Name = 'Postcode 1'),
            new CS_Postcode__c(Name = 'Postcode 2')
        };
        
        insert postCodeList;
        System.debug('****Post Code List: ' + postCodeList);
        System.assertEquals('Postcode 1',postCodeList[0].Name );
        
        
        postcodeZoneList = new List<CS_BIP_Postcode_Zone__c>{
            new CS_BIP_Postcode_Zone__c(Name = 'Post Code Zone 1', CS_BIP_Zone__c = zoneList[0].Id, CS_Postcode__c = postCodeList[0].Id),
            new CS_BIP_Postcode_Zone__c(Name = 'Post Code Zone 2', CS_BIP_Zone__c = zoneList[1].Id, CS_Postcode__c = postCodeList[1].Id)
        };
        
        insert postcodeZoneList;
        System.debug('****Post Code Zone List: ' + postcodeZoneList);
        System.assertEquals('Post Code Zone 1',postcodeZoneList[0].Name );
        
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
        };
        
        insert bandwidthList;
        System.debug('****Bandwidth List: ' + bandwidthList);
        System.assertEquals('64k',bandwidthList[0].Name );
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 1', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPVPN'),
            new CS_Bandwidth_Product_Type__c(Name = 'BAndwidth Product Type 2', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPVPN')
        };
        
        insert bandwidthProdTypeList;
        System.debug('****Bandwidth Product Type List: ' + bandwidthProdTypeList);
         System.assertEquals('BAndwidth Product Type 1',bandwidthProdTypeList[0].Name );
        
        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;
        System.debug('****Account List: ' + accountList);
           System.assertEquals('Account 1',accountList[0].Name );
        
        providers = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'VODAFONE NZ', NAS_Product__c = Boolean.valueOf('true'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('false')),
            new CS_Provider__c(Name = 'Test Provider 2', NAS_Product__c = Boolean.valueOf('false'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('false'))
        };
        
        insert providers;
        System.debug('**** Providers List: ' + providers);
        System.assertEquals('VODAFONE NZ',providers[0].Name );
        
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', Country__c = countryList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id , cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC', cspmb__Account__c = accountList[0].Id, CS_BIP_Zone__c = zoneList[0].Id, BIP_Connection_Type__c = 'Redundant'),
            new cspmb__Price_Item__c(Name = 'Price Item 2', Country__c = countryList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id , cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC', CS_BIP_Zone__c = zoneList[0].Id, BIP_Connection_Type__c = 'Redundant'),
            new cspmb__Price_Item__c(Name = 'Price Item 3', Country__c = countryList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id , cspmb__Is_Active__c = true, IPVPN_Type__c = 'BIP', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC', cspmb__Account__c = accountList[0].Id, CS_BIP_Zone__c = zoneList[0].Id, BIP_Connection_Type__c = 'Redundant'),
            new cspmb__Price_Item__c(Name = 'Price Item 4', Country__c = countryList[5].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[1].Id , cspmb__Is_Active__c = true, IPVPN_Type__c = 'TransTasman', Connectivity__c = 'Offnet', Pricing_Segment__c = 'MNC', cspmb__Account__c = accountList[1].Id, CS_BIP_Zone__c = zoneList[1].Id, BIP_Connection_Type__c = 'Single Uplink', Trans_Tasman_Type__c = 'Type A'),
            new cspmb__Price_Item__c(Name = 'Price Item 5', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id , cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'MNC', cspmb__Account__c = accountList[0].Id, CS_BIP_Zone__c = zoneList[0].Id, BIP_Connection_Type__c = 'Redundant')
        };
        
        insert priceItemList;
        System.debug('****Price Item List: ' + priceItemList);
           System.assertEquals('Price Item 1',priceItemList[0].Name );
        
    }

  private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();                 
                        
            searchFields.put('Pop City', cityList[0].Id);
            searchFields.put('Port Country', countryList[0].Id);
            searchFields.put('Product Type', 'IPVPN');
            searchFields.put('Country Name', countryList[0].Name);
            searchFields.put('Account Id', accountList[0].Id );
            searchFields.put('Port Speed', bandwidthProdTypeList[0].Id);
            searchFields.put('Port Type', 'Standard');
            searchFields.put('Onnet or Offnet', 'Onnet');
            searchFields.put('Offnet Provider Name', providers[0].Name);
            searchFields.put('Account Customer Type', 'MNC');
            searchFields.put('Zone', postcodeZoneList[0].Id);
            searchFields.put('Connection Type', 'Redundant');
            searchFields.put('Type Of NNI', 'Type A');
        
            CS_IPVPNPriceItemLookup csIPVPPNPriceItemLookup = new CS_IPVPNPriceItemLookup();
            String reqAtts = csIPVPPNPriceItemLookup.getRequiredAttributes();
            Object[] data = csIPVPPNPriceItemLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            data.clear();
            searchFields.put('Port Type', 'BIP');
            
            data = csIPVPPNPriceItemLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            data.clear();
            
            searchFields.put('Port Type', 'TransTasman');
            searchFields.put('Onnet or Offnet', 'Offnet');
            searchFields.put('Port Country', countryList[5].Id);
            searchFields.put('Country Name', countryList[5].Name);
            searchFields.put('Port Speed', bandwidthProdTypeList[1].Id);
            searchFields.put('Zone', postcodeZoneList[1].Id);
            searchFields.put('Account Id', accountList[1].Id );
            
            data = csIPVPPNPriceItemLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
  }
  
  /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

}