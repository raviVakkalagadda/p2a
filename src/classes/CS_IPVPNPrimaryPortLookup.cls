global with sharing class CS_IPVPNPrimaryPortLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["Onnet or Offnet", "Replaced Master Service", "Replaced Master Service Name", "Product Definition Name", "Account Id", "Backup Port Options", "POP"]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Set <Id> resilIds = new Set<Id>();
        String accountId = searchFields.get('Account Id');
        String backupOptions = searchFields.get('Backup Port Options');
        String prodDefName = searchFields.get('Product Definition Name');
        String pop = searchFields.get('POP');
        String onnetOrOffnet = searchFields.get('Onnet or Offnet');
        String replacedMasterService = searchFields.get('Replaced Master Service');
        String replacedMasterServiceName = searchFields.get('Replaced Master Service Name');
        List<String> productId = new List<String>();
        List <csord__Service__c> data;
        
        System.debug('****** CS_IPVPNPrimaryPortLookup searchFields: ' + accountId + ' - '+ backupOptions +' - '+prodDefName+' - '+pop+' - '+onnetOrOffnet+' - '+replacedMasterService);
        
        if(prodDefName == 'IPVPN Port'){
            productId.add('CUSTSITEONNETPOP');
            productId.add('CUSTSITEOFFNETPOP');
        } else if(prodDefName.contains('VPLS')){
            productId.add('VLP-Onnet');
            productId.add('VLP-Offnet');
        }
        
        if(onnetOrOffnet == 'Onnet'){
                    if (backupOptions == 'Active/ Standby Different POP'){
                        data = [SELECT Id, Name, Product_Id__c, CS_Port_POP__c, Primary_Or_Backup__c, AccountId__c, csord__Status__c, Master_Service__c, Master_Service__r.Name, Inventory_Status__c
                                FROM csord__Service__c
                                WHERE Product_Id__c IN :productId 
                                    AND AccountId__c = :accountId 
                                    AND Primary_Or_Backup__c = 'Primary' 
                                    AND CS_Port_POP__c != :pop
                                    AND Master_Service__r.Name =:replacedMasterServiceName
                                    AND Master_Service__c != null
                                    AND Inventory_Status__c != 'Decommissioned'];
                    } else {
                        data = [SELECT Id, Name, Product_Id__c, CS_Port_POP__c, Primary_Or_Backup__c, AccountId__c, csord__Status__c, Master_Service__c, Master_Service__r.Name, Inventory_Status__c
                                FROM csord__Service__c
                                WHERE Product_Id__c IN :productId 
                                    AND AccountId__c = :accountId 
                                    AND Primary_Or_Backup__c = 'Primary' 
                                    AND CS_Port_POP__c = :pop
                                    AND Master_Service__r.Name =:replacedMasterServiceName
                                    AND Master_Service__c != null
                                    AND Inventory_Status__c != 'Decommissioned'];
                    }
        } else {
            data = [SELECT Id, Name, Product_Id__c, CS_Port_POP__c, Primary_Or_Backup__c, AccountId__c, csord__Status__c, Master_Service__c, Master_Service__r.Name, Inventory_Status__c
                    FROM csord__Service__c
                    WHERE Product_Id__c IN :productId 
                        AND AccountId__c = :accountId 
                        AND Primary_Or_Backup__c = 'Primary'
                        AND Master_Service__r.Name =:replacedMasterServiceName
                        AND Master_Service__c != null
                        AND Inventory_Status__c != 'Decommissioned'];
        }
        
        System.debug('****** CS_IPVPNPrimaryPortLookup data: ' + data);
        
       return data;
    }
}