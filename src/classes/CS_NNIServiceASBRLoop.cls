global with sharing class CS_NNIServiceASBRLoop extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        system.debug('**** CS_NNIServiceASBRLoop searchFields: '+searchFields);
        String provider = searchFields.get('CS Provider Calculated');
        String nniType = searchFields.get('Type Of NNI');
        String solutionType = searchFields.get('Solution Type Calculated');
        Boolean multicast = false;
        if(solutionType == 'IPVPN' && searchFields.get('Enable Multicast') != ''){
            multicast =Boolean.valueOf(searchFields.get('Enable Multicast'));
        }
        
        String asbrType = searchFields.get('Type Of ASBR');
        String scenario = searchFields.get('Scenario');
        String searchValue = searchFields.get('searchValue') +'%';

        system.debug('**** CS_NNIServiceASBRLoop searchFields: '+searchFields);
        List<CS_NNI_Service__c> returnList = new  List<CS_NNI_Service__c> ();
        Set<Id> servIds = new Set<Id>();
        List<CS_Provider_NNI_Service_Join__c> providerList;
        
        if(asbrType == 'Standard'){
            providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c, CS_NNI_Service__c
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND CS_Provider__c = :provider
                            AND ASBR__c = true];
        } else {
            if(scenario == 'NAS'){
                providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c, CS_NNI_Service__c
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND CS_Provider__c = :provider
                            AND NAS__c = true];
            }else if(scenario == 'Ex-Pacnet'){
                providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c, CS_NNI_Service__c
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND CS_Provider__c = :provider
                            AND Ex_Pacnet__c = true];
            }else{
                providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c, CS_NNI_Service__c
                            FROM CS_Provider_NNI_Service_Join__c
                            WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                            AND CS_Provider__c = :provider
                            AND Customer_NNI__c = true];
            }
             //AND Standalone_ASBR__c = true
        }
        system.debug('**** CS_NNIServiceASBRLoop providerList: '+providerList);
        for(CS_Provider_NNI_Service_Join__c item : providerList){
            servIds.add(item.CS_NNI_Service__c);
        }

        if(solutionType == 'IPVPN'){
            if(multicast== false){
                        returnList = [SELECT Id, Name, CS_City_Name__c, CS_POP_Name__c, CS_City__r.Country_Name__c, Service_Id__c,
                          CS_Provider__c,Multicast__c,NNI_Type__c,MulticastYesNo__c,NNI_Product_Id__c,Path_Id__c
                          FROM CS_NNI_Service__c 
                          WHERE Id in :servIds and NNI_Service_Type__c = 'IPVPN' and Name Like :searchValue];
            } else {
                        returnList = [SELECT Id, Name, CS_City_Name__c, CS_POP_Name__c, CS_City__r.Country_Name__c, Service_Id__c,
                          CS_Provider__c,Multicast__c,NNI_Type__c,MulticastYesNo__c,NNI_Product_Id__c,Path_Id__c
                          FROM CS_NNI_Service__c 
                          WHERE Id in :servIds and Multicast__c = :multicast and NNI_Service_Type__c = 'IPVPN' and Name Like :searchValue];
            }

        } else {
            returnList = [SELECT Id, Name, CS_City_Name__c, CS_POP_Name__c, CS_City__r.Country_Name__c, Service_Id__c,
                          CS_Provider__c,Multicast__c,NNI_Type__c,MulticastYesNo__c,NNI_Product_Id__c,Path_Id__c
                          FROM CS_NNI_Service__c 
                          WHERE Id in :servIds and NNI_Service_Type__c = 'VPLS' and Name Like :searchValue];
        }
        system.debug('**** CS_NNIServiceASBRLoop providerList: '+providerList);
        return returnList;
    }

    public override String getRequiredAttributes(){ 
        return '["EnableMulticastYesNo1","Scenario", "CS Provider Calculated", "EnableMulticastYesNo2", "Type Of NNI", "Type Of ASBR", "Solution Type Calculated", "Enable Multicast"]';
    }

    
    
}