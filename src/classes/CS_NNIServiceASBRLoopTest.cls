@isTest(SeeAllData = false)
private class CS_NNIServiceASBRLoopTest {

    private static List<CS_Provider__c> providers;
    private static List<CS_NNI_Service__c> nniServices;
    private static List<CS_Provider_NNI_Service_Join__c> providerNniService;

    private static Map<String, String> searchFields = new Map<String, String>();
    
    private static void initTestData(){
        providers = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'Provider 1'),
            new CS_Provider__c(Name = 'Provider 2')
        };
        
        insert providers;
        System.assert(providers!=null);
         system.assertEquals(providers[0].Name , 'Provider 1');
        
        nniServices = new List<CS_NNI_Service__c>{
            new CS_NNI_Service__c(Name = 'NNI Service 1', NNI_Type__c = 'Type A', Multicast__c = false),
            new CS_NNI_Service__c(Name = 'NNI Service 2', NNI_Type__c = 'Type A')
        };
        
        insert nniServices;  
        System.assert(nniServices!=null); 
        system.assertEquals(nniServices[0].Name , 'NNI Service 1');
        
        providerNniService = new List<CS_Provider_NNI_Service_Join__c>{
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[0].Id, CS_NNI_Service__c = nniServices[0].Id, ASBR__c = true, NAS__c = false, Ex_Pacnet__c = false, Customer_NNI__c = false),
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[0].Id, CS_NNI_Service__c = nniServices[1].Id, ASBR__c = false, NAS__c = true, Ex_Pacnet__c = false, Customer_NNI__c = false),
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[1].Id, CS_NNI_Service__c = nniServices[0].Id, ASBR__c = false, NAS__c = false, Ex_Pacnet__c = true, Customer_NNI__c = false),
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[1].Id, CS_NNI_Service__c = nniServices[1].Id, ASBR__c = false, NAS__c = false, Ex_Pacnet__c = false, Customer_NNI__c = true)
        };
        
        insert providerNniService;
        System.assert(providerNniService!=null); 
        system.assertEquals(providerNniService[0].ASBR__c , true);
        
        searchFields.put('CS Provider Calculated', providers[0].Id);
        searchFields.put('Type Of NNI', 'Type A');
        searchFields.put('Solution Type Calculated', 'IPVPN');
        searchFields.put('Enable Multicast', 'false');
        searchFields.put('Type Of ASBR', 'Standard');
        searchFields.put('Scenario', '');
        searchFields.put('searchValue', '%');
        
    }

    private static testMethod void test() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            CS_NNIServiceASBRLoop nniServiceAsbr = new CS_NNIServiceASBRLoop();
            Object[] data = nniServiceAsbr.doLookupSearch(searchFields, '', null, 0, 0);
            
         system.assertEquals(true,data!=null); 
            data.clear();
            
            searchFields.put('Solution Type Calculated', 'NotIPVPN');
            data = nniServiceAsbr.doLookupSearch(searchFields, '', null, 0, 0);
            
          system.assertEquals(true,data!=null); 
            data.clear();
            
            searchFields.put('Type Of ASBR', 'NotStandard');
            searchFields.put('Scenario', 'NAS');
            data = nniServiceAsbr.doLookupSearch(searchFields, '', null, 0, 0);
            
            system.assertEquals(true,data!=null); 
            data.clear();
            
            searchFields.put('CS Provider Calculated', providers[1].Id);
            searchFields.put('Scenario', 'Ex-Pacnet');
            data = nniServiceAsbr.doLookupSearch(searchFields, '', null, 0, 0);
            
           system.assertEquals(true,data!=null); 
            data.clear();
            
            searchFields.put('Scenario', '');
            data = nniServiceAsbr.doLookupSearch(searchFields, '', null, 0, 0);
            
           system.assertEquals(true,data!=null); 
            data.clear();
        
            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_NNIServiceASBRLoopTest :test';         
ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}