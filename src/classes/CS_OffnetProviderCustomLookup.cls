global with sharing class CS_OffnetProviderCustomLookup extends cscfga.ALookupSearch {

    public override String getRequiredAttributes(){
        return '["Port Country", "Type Of NNI", "ScreenFlowName"]';
    }
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            
            String country = searchFields.get('Port Country');
            String nniType = searchFields.get('Type Of NNI');
            String flowName = searchFields.get('ScreenFlowName');
            List<CS_Provider_NNI_Service_Join__c> providerList;
            if(flowName == 'OrderEnrichment'){
                providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c 
                				FROM CS_Provider_NNI_Service_Join__c
                				WHERE CS_NNI_Service__r.NNI_Type__c = :nniType
                				AND ASBR__c = true];
            }else{
                providerList = [SELECT CS_Provider__c, CS_NNI_Service__r.NNI_Type__c 
                                FROM CS_Provider_NNI_Service_Join__c
                                WHERE ASBR__c = true];
            }

            Set<Id> provIds = new Set<Id>();
            for(CS_Provider_NNI_Service_Join__c item : providerList){
            	provIds.add(item.CS_Provider__c);
            }

            List<CS_Country_Providers_Join__c> returnData = [SELECT ID, NAME, CS_Country__c, CS_Provider__c, ProviderName__c
            												 FROM CS_Country_Providers_Join__c
            												 WHERE CS_Provider__c in :provIds 
            												 AND CS_Country__c = :country];
            
           
           return returnData;
    } 
}