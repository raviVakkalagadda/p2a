@isTest(SeeAllData = false)
private class CS_OffnetProviderCustomLookupTest {

    private static List<CS_Provider__c> providers;
    private static List<CS_NNI_Service__c> nniServices;
    private static List<CS_Provider_NNI_Service_Join__c> providerNniService;
    private static List<CS_Country__c> countries;
    private static List<CS_Country_Providers_Join__c> countryProvidersJoin;
    
    private static Map<String, String> searchFields = new Map<String, String>();

    private static void initTestData(){
        providers = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'Provider 1'),
            new CS_Provider__c(Name = 'Provider 2')
        };
        
        insert providers;
        
        nniServices = new List<CS_NNI_Service__c>{
            new CS_NNI_Service__c(Name = 'NNI Service 1', NNI_Type__c = 'Type A'),
            new CS_NNI_Service__c(Name = 'NNI Service 2', NNI_Type__c = 'Type B')
        };
        
        insert nniServices;
        
        providerNniService = new List<CS_Provider_NNI_Service_Join__c>{
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[0].Id, CS_NNI_Service__c = nniServices[0].Id, ASBR__c = true),
            new CS_Provider_NNI_Service_Join__c(CS_Provider__c = providers[1].Id, CS_NNI_Service__c = nniServices[1].Id, ASBR__c = true)
        };
        
        insert providerNniService;
        
        countries = new List<CS_Country__c>{
            new CS_Country__c(Name = 'China'),
            new CS_Country__c(Name = 'Hong Kong')
        };
        
        insert countries;
        
        countryProvidersJoin = new List<CS_Country_Providers_Join__c>{
            new CS_Country_Providers_Join__c(CS_Country__c = countries[0].Id, CS_Provider__c = providers[0].Id),
            new CS_Country_Providers_Join__c(CS_Country__c = countries[1].Id, CS_Provider__c = providers[1].Id)
        };
        
        insert countryProvidersJoin;
        
        searchFields.put('Port Country', countries[0].Id);
        searchFields.put('Type Of NNI', 'Type A');
        searchFields.put('ScreenFlowName', 'OrderEnrichment');
    }

    private static testMethod void test() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            CS_OffnetProviderCustomLookup offnetProvidercustomLookup = new CS_OffnetProviderCustomLookup();
            Object[] data = offnetProvidercustomLookup.doLookupSearch(searchFields, '', null, 0, 0);
            
            System.assert(data.size() > 0, 'No data available');
            
            searchFields.put('ScreenFlowName', 'NOTOrderEnrichment');
            offnetProvidercustomLookup.doLookupSearch(searchFields, '', null, 0, 0);
            
            System.assert(data.size() > 0, 'No data available');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}