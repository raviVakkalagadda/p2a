public class CS_OpportunityBatchJobController {
    
    private AsyncApexJob job;
    private String oppId;
    private String batchOrderStatus = '';
    
    public Boolean getRenderPage() {
        return job != null;
    }
    
    public AsyncApexJob getAsyncJob() {
        return job;
    }
    
    public String getBatchOrderStatus() {
        if (job != null) {
            if (job.Status == 'Completed') {
                return 'Completed';
            } else if (job.Status == 'Processing') {
                return 'Processed ' + job.JobItemsProcessed + '/' + job.TotalJobItems + ' batches.';
            } else if(job.Status == 'Preparing' || job.Status == 'Holding' || job.Status == 'Queued'){
                return 'Order generation is in progress...';
            } else if(job.Status == 'Failed'){
                return 'Error in - ' + job.NumberOfErrors + '/' + job.TotalJobItems + ' batches.';
            } 
            else if(job.Status == 'Aborted'){
                return 'Order generation process has been Aborted.';
            } else{
                return '';
            }             
        } else {
            return '';
        }
    }
    
    public CS_OpportunityBatchJobController(ApexPages.StandardController ctrl) {
        oppId = ctrl.getId();
        List<cscfga__product_basket__c> baskets = [
            select id, name, csordtelcoa__Synchronised_with_Opportunity__c, csordtelcoa__Order_Generation_Batch_Job_Id__c
            from cscfga__product_basket__c
            where csordtelcoa__Synchronised_with_Opportunity__c = true
            and cscfga__Opportunity__c = :oppId
        ];
        if (!baskets.isEmpty() && baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c != null && baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c != '') {
            List<AsyncApexJob> jobs = [
                select id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                from AsyncApexJob
                where id = :baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c 
            ];
            job = jobs.Size()>0? jobs[0]: null;
        }
    }
    
    public void refreshJobStatus() {
        List<cscfga__product_basket__c> baskets = [
            select id, name, csordtelcoa__Synchronised_with_Opportunity__c, csordtelcoa__Order_Generation_Batch_Job_Id__c
            from cscfga__product_basket__c
            where csordtelcoa__Synchronised_with_Opportunity__c = true
            and cscfga__Opportunity__c = :oppId
        ];
        if (!baskets.isEmpty() && baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c != null && baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c != '') {
            List<AsyncApexJob> jobs = [
                select id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                from AsyncApexJob
                where id = :baskets[0].csordtelcoa__Order_Generation_Batch_Job_Id__c 
            ];
            job = jobs.Size()>0? jobs[0]: null;
        }
    }
}