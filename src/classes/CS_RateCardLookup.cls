global with sharing class CS_RateCardLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End POPCLS 1","Z-End POPCLS 1","A End Country","Z-End Country","A End City","Z End City","Bandwidth","IPL","EPL","EPLX","ICBS","EVPL","Show Unprotected Routes","Show Restored Routes","Show Protected Routes" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String aEndCountry = searchFields.get('A End Country');
        String zEndCountry = searchFields.get('Z-End Country');
        String aEndCity = searchFields.get('A End City');
        String zEndCity = searchFields.get('Z End City');
        String aEndPop = searchFields.get('A-End POPCLS 1');
        String zEndPop = searchFields.get('Z-End POPCLS 1');
        String bandwidth = searchFields.get('Bandwidth');
        Set <Id> bwIds = new Set<Id>();
        Set <Id> bwgrpIds = new Set<Id>();
        String Flag = searchFields.get('EVPL');
        String protected1 = searchFields.get('Show Protected Routes');
        String Unprotected = searchFields.get('Show Unprotected Routes');
        String Restored = searchFields.get('Show Restored Routes');
       
       List <String> Resiliencefilter = new List<String>();
        List <String> ProductTypeNames = new List<String>();
        Set<String> BwTypeNames = new Set<String>();
        //List <CS_Bandwidth_Product_Type__c > BandwidthGroup = new List<CS_Bandwidth_Product_Type__c >();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        if(protected1 == 'Yes' || protected1 == 'true'){
            Resiliencefilter.add('Protected');
        }
        if(Unprotected == 'Yes' || Unprotected == 'true'){
            Resiliencefilter.add('Unprotected');
        }
        if(Restored == 'Yes' || Restored == 'true'){
            Resiliencefilter.add('Restored');
        }
        
        if(Resiliencefilter.size()==0){
            Resiliencefilter.add(''); 
        }
        
        //BandwidthGroup = [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth];
         //System.debug('BWgroup--------------++++++++++++++++++'+BandwidthGroup );
         for (CS_Bandwidth_Product_Type__c key : [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth]){
       //  System.debug('FOR' );
                 if(key.Bandwidth_Group__c != null){
                // System.debug('IF' );
                 //List <string> l = key.Bandwidth_Group__c.split(',');
                 //System.debug('Split--------------++++++++++++++++++'+l );
                 //for(String l1 : l)
                System.debug('++++++++++++++++++'+key.Bandwidth_Group__c);
                BwTypeNames.add(key.Bandwidth_Group__c);
                BwTypeNames.add(key.name);
                }
        }
       // BwTypeNames = [2M,E1];
        System.debug('BWTypename++++++++++++++++++'+BwTypeNames);
      
        List<CS_Bandwidth__c > bwgrpId = [SELECT id from CS_Bandwidth__c where name IN :BwTypeNames];      
       // bwgrpId.add(bwgrpIds);
        System.debug('BWTypename++++++++++++++++++'+bwgrpId);
        if(bwgrpId.isEmpty()){
            bwgrpId = [SELECT id from CS_Bandwidth__c where id = :bandwidth];
        }
        
        System.debug('ICBS+++++++++++'+bwgrpId);
       if(aEndPop ==null && zEndPop ==null){
                                                       
                                                      
        List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList = [SELECT Id,CS_Route_Segment__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c, CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c, CS_Bandwidth_Product_Type__r.Bandwidth_Group__c,CS_Bandwidth_Product_Type__r.CS_Bandwidth__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c in :ProductTypeNames 
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId 
                                                    AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter
                                                       ];   
                                                       System.Debug('routeseg +++++++++++' + routeSegList);
                    for(CS_Route_Bandwidth_Product_Type_Join__c item : routeSegList){
        
                            bwIds.add(item.CS_Route_Segment__c);
                        }
         }
             else{
             
                 List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList = [SELECT Id,CS_Route_Segment__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c, CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,
                                                CS_Route_Segment__r.POP_A__c,CS_Route_Segment__r.POP_Z__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c in :ProductTypeNames 
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Route_Segment__r.POP_A__c = :aEndPop
                                                    AND CS_Route_Segment__r.POP_Z__c = :zEndPop
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                     AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter  ]; 
                                                    System.Debug('routesegwithnoPOP +++++++++++' + routeSegList.size());   
                                                       
                List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList1 = new List<CS_Route_Bandwidth_Product_Type_Join__c >();                                       
                                                       
                if(flag == 'Yes' || flag =='true'){                                       
                     routeSegList1 = [SELECT Id,CS_Route_Segment__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c, CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,
                                                CS_Route_Segment__r.POP_A__c,CS_Route_Segment__r.POP_Z__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c =: 'EVPL' 
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                      AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter ]; 
                                                       System.Debug('routesegwithnoPOPEVPL +++++++++++' + routeSegList1.size());
                                                       }
        System.Debug('AFTER RECORDS');                                           
        for(CS_Route_Bandwidth_Product_Type_Join__c item : routeSegList){
            bwIds.add(item.CS_Route_Segment__c);
            System.Debug('IN FOR1');  
        }
        System.Debug('OUT FOR1');
        System.Debug('routesegwithnoPOPEVPL +++++++++++' + routeSegList1.size());
        if(routeSegList1.size()>0){
            System.Debug('IN FOR2');
            for(CS_Route_Bandwidth_Product_Type_Join__c item : routeSegList1){
            bwIds.add(item.CS_Route_Segment__c);
        }
        }
    } 
                    
                                                                                               
        
        System.Debug('bwIds +++++++++++' + bwIds);
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        System.Debug('searchValue +++++++++++' + searchValue);                
        List <CS_Route_Segment__c> data = [SELECT Id, Name,CosMix__c,Network_Technology__c,Circuit_Type__c,POP_A__c,POP_Z__c,CS_Resilience__c,CS_Cable_Path__c,  Product_Type__c,A_End_Country__c,A_POP_Name__c,Cabel_Path_Name__c,Z_End_Country__c,Z_POP_Name__c,Resilience_Name__c,RTD__c,Port_Rating__c FROM CS_Route_Segment__c WHERE  Id IN :bwIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;

   }

}