global with sharing class CS_RateCardLookup_new extends cscfga.ALookupSearch{
    
    public override String getRequiredAttributes(){ 
        return '[ "Product_config_ID__c","CountForButtons","start","last","A-End POPCLS 1","Z-End POPCLS 1","A-End Country","Z-End Country","A End City","Z End City","BandwidthId","IPL","EPL","EPLX","ICBS","EVPL","Show Unprotected Routes","Show Restored Routes","Show Protected Routes","Show Always On Routes","Voice","Video","Critical data","Interactive data","Standard data","Low priority data","EVPL Transparent","EVPL VLAN" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        
        Decimal cal = 0.00;     
        ID ProductConfigID=searchFields.get('Product_config_ID__c');
        String countOfNext=searchFields.get('CountForButtons');//this will have a count of next button
        String startPage=searchFields.get('start');//this is an integer that will have value 0 or one based on first page button is clicked 
        String lastPage=searchFields.get('last');//this is an integer that will have value 0 or one based on last page button is clicked 
        String aEndCountry = searchFields.get('A-End Country');
        String zEndCountry = searchFields.get('Z-End Country');
        String aEndCity = searchFields.get('A End City');
        String zEndCity = searchFields.get('Z End City');
        String aEndPop = searchFields.get('A-End POPCLS 1');
        String zEndPop = searchFields.get('Z-End POPCLS 1');
        String bandwidth = searchFields.get('BandwidthId');
        String Voice = searchFields.get('Voice');
        String Video = searchFields.get('Video');
        String Criticaldata = searchFields.get('Critical data');
        String Interactivedata = searchFields.get('Interactive data');
        String Standarddata = searchFields.get('Standard data');
        String Lowprioritydata = searchFields.get('Low priority data');
        String EVPLT = searchFields.get('EVPL Transparent');
        String EVPLV = searchFields.get('EVPL VLAN');
        String IPL = searchFields.get('IPL');
        String EPL = searchFields.get('EPL');
        String EPLX = searchFields.get('EPLX');
        String ICBS = searchFields.get('ICBS');
        Set <Id> bwIds = new Set<Id>();
        Set <Id> bwgrpIds = new Set<Id>();
        String Flag = searchFields.get('EVPL');
        String protected1 = searchFields.get('Show Protected Routes');
        String Unprotected = searchFields.get('Show Unprotected Routes');
        String Restored = searchFields.get('Show Restored Routes');
		String AlwaysOn = searchFields.get('Show Always On Routes');
       List<CS_Route_Bandwidth_Product_Type_Join__c> FinalList = new List<CS_Route_Bandwidth_Product_Type_Join__c >(); 
       List <String> Resiliencefilter = new List<String>();
      List <String> ProductType = new List<String>();
        List <String> ProductTypeNames = new List<String>();
        Set<String> BwTypeNames = new Set<String>();
        //List <CS_Bandwidth_Product_Type__c > BandwidthGroup = new List<CS_Bandwidth_Product_Type__c >();
         ProductType.add('IPL');  
ProductType.add('EPL'); 
ProductType.add('EPLX'); 
ProductType.add('ICBS'); 

                List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList1 = new List<CS_Route_Bandwidth_Product_Type_Join__c >();
                List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList2 = new List<CS_Route_Bandwidth_Product_Type_Join__c >();
        
        
      //  for (String key : searchFields.keySet()){
        //  System.debug('KEY+++++++++++++++++++'+key);
          //  String attValue = searchFields.get(key);
            //if((attValue == 'Yes' || attValue == 'true') && key != 'EVPL Transparent' && key != 'EVPL VLAN')
              //  ProductTypeNames.add(key);
        //}
        if(IPL == 'Yes' || IPL == 'true'){
            ProductTypeNames.add('IPL');
        }
        if(EPL == 'Yes' || EPL == 'true'){
            ProductTypeNames.add('EPL');
        }
        if(EPLX == 'Yes' || EPLX == 'true'){
            ProductTypeNames.add('EPLX');
        }
        if(ICBS == 'Yes' || ICBS == 'true'){
            ProductTypeNames.add('ICBS');
        }
        if(protected1 == 'Yes' || protected1 == 'true'){
            Resiliencefilter.add('Protected');
        }
        if(Unprotected == 'Yes' || Unprotected == 'true'){
            Resiliencefilter.add('Unprotected');
        }
        if(Restored == 'Yes' || Restored == 'true'){
            Resiliencefilter.add('Restored');
        }
		if(AlwaysOn == 'Yes' || AlwaysOn == 'true'){
            Resiliencefilter.add('Always On');
        }
        
        if(Resiliencefilter.size()==0){
            Resiliencefilter.add(''); 
        }
        
        //BandwidthGroup = [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth];
         //System.debug('BWgroup--------------++++++++++++++++++'+BandwidthGroup );
         for (CS_Bandwidth_Product_Type__c key : [SELECT Id,name,CS_Bandwidth__c, Bandwidth_Group__c from CS_Bandwidth_Product_Type__c where CS_Bandwidth__c = :bandwidth]){
       //  System.debug('FOR' );
                 if(key.Bandwidth_Group__c != null){
                // System.debug('IF' );
                 //List <string> l = key.Bandwidth_Group__c.split(',');
                 //System.debug('Split--------------++++++++++++++++++'+l );
                 //for(String l1 : l)
                System.debug('++++++++++++++++++'+key.Bandwidth_Group__c);
                
                /*
                * below change was suggested by manish
                *done by:ritesh
                *date:25-nov-2016
                *change:extract comma separated values of Bandwidth_Group__c                
                */
                //BwTypeNames.add(key.Bandwidth_Group__c);
                String[] parts =     key.Bandwidth_Group__c.split(',');
                 for(string s : parts){
                    BwTypeNames.add(s);         
                 }  
                BwTypeNames.add(key.name);
                }
        }
       // BwTypeNames = [2M,E1];
        System.debug('BWTypename++++++++++++++++++'+BwTypeNames);
      
        List<CS_Bandwidth__c > bwgrpId = [SELECT id from CS_Bandwidth__c where name =: bandwidth];      
       // bwgrpId.add(bwgrpIds);
        System.debug('BWTypename++++++++++++++++++'+bwgrpId);
        if(bwgrpId.isEmpty()){
            bwgrpId = [SELECT id from CS_Bandwidth__c where id = :bandwidth];
        }
        
        System.debug('ICBS+++++++++++'+bwgrpId);
       if(aEndPop ==null && zEndPop ==null){
                                                       
                                                      
        List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.CS_Cable_Path__c,
                                                CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.Z_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c,CS_Route_Segment__r.POP_Z__c,CS_Route_Segment__r.POP_A__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c,
                                                CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.A_POP_Name__c,CS_Route_Segment__r.Resilience_Name__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c, CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c,CS_Route_Segment__r.Z_End_Country__c, CS_Bandwidth_Product_Type__r.Bandwidth_Group__c,CS_Bandwidth_Product_Type__r.CS_Bandwidth__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name, CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c,CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c in :ProductTypeNames 
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId 
                                                    AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter
                                                       ];   
                                                       System.Debug('routeseg +++++++++++' + routeSegList);
                    if(Flag == 'Yes' || Flag =='true'){   

                    if(EVPLT == 'Yes' || EVPLT == 'true') {
                     routeSegList1 = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.POP_Z__c,CS_Route_Segment__r.Resilience_Name__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c,CS_Route_Segment__r.Z_End_Country__c,CS_Route_Segment__r.Z_POP_Name__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.POP_A__c,
                                                CS_Route_Segment__r.CS_Cable_Path__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,CS_Route_Segment__r.A_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name,  CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c, CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c =: 'EVPL' 
                                                    AND CS_Route_Segment__r.Network_Technology__c like '%Transparent%'
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                     //AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter 
                                                     ]; 
                                                     }
                                                     System.Debug('routesegemnt transparent++++++++++++'+routeSegList1);
                    
                    if(EVPLV == 'Yes' || EVPLV == 'true') {
                     routeSegList2 = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.POP_Z__c,CS_Route_Segment__r.Resilience_Name__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c,CS_Route_Segment__r.Z_End_Country__c,CS_Route_Segment__r.Z_POP_Name__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.POP_A__c,
                                                CS_Route_Segment__r.CS_Cable_Path__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,CS_Route_Segment__r.A_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name,  CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c, CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c =: 'EVPL' 
                                                    AND CS_Route_Segment__r.Network_Technology__c =: 'VLAN'
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                   // AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter 
                                                     ]; 
                                                     }
                                                      System.Debug('routesegemnt vlan++++++++++++'+routeSegList2);
                                                     
                                                       System.Debug('routesegwithnoPOPEVPL +++++++++++' + routeSegList1.size());
                                                       }
                                                       
                        
                        
                        //appendng the final bandwidth list
                        if(routeSegList.size()>0){
                                FinalList.addAll(routeSegList);
                        }   
                        if(routeSegList1.size()>0){
            FinalList.addAll(routeSegList1);
        }
        if(routeSegList2.size()>0){
            FinalList.addAll(routeSegList2);
        }
                        System.debug('FINAL LIST+++++++++++++++++'+FinalList);
                        
       }            
                        
         
             else{
             
             
                                                     System.Debug('POPInsideElseBefore++++++++++++'+aEndPop);
                                                    System.Debug('POPInsideElseBefore++++++++++++'+zEndPop);
             
                 List<CS_Route_Bandwidth_Product_Type_Join__c> routeSegList = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.POP_Z__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c,CS_Route_Segment__r.Z_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c,CS_Route_Segment__r.POP_A__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.Z_End_Country__c,CS_Route_Segment__r.A_POP_Name__c,
                                                CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.Resilience_Name__c,
                                                CS_Route_Segment__r.CS_Cable_Path__c,CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name, CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c, CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c in :ProductTypeNames 
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Route_Segment__r.POP_A__c = :aEndPop
                                                    AND CS_Route_Segment__r.POP_Z__c = :zEndPop
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                     AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter  ];
                                                    System.Debug('routesegwithnoPOPList +++++++++++' + routeSegList);
                                                    System.Debug('routesegwithnoPOP +++++++++++' + routeSegList.size());
                                                    System.Debug('POPInsideElse++++++++++++'+aEndPop);
                                                    System.Debug('POPInsideElse++++++++++++'+zEndPop);                                                  
           
                                                       
                if(Flag == 'Yes' || Flag =='true'){   

                    if(EVPLT == 'Yes' || EVPLT == 'true') {
                     routeSegList1 = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.POP_Z__c,CS_Route_Segment__r.Resilience_Name__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c,CS_Route_Segment__r.Z_End_Country__c,CS_Route_Segment__r.Z_POP_Name__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.POP_A__c,
                                                CS_Route_Segment__r.CS_Cable_Path__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,CS_Route_Segment__r.A_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name, CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c, CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c =: 'EVPL' 
                                                    AND CS_Route_Segment__r.Network_Technology__c like '%Transparent%'
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                     //AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter 
                                                     ]; 
                                                     }
                                                     System.Debug('routesegemnt transparent++++++++++++'+routeSegList1);
                    
                    if(EVPLV == 'Yes' || EVPLV == 'true') {
                     routeSegList2 = [SELECT Id,CS_Route_Segment__c,CS_Route_Segment__r.A_End_Country__c,CS_Route_Segment__r.Cabel_Path_Name__c,CS_Route_Segment__r.Circuit_Type__c,CS_Route_Segment__r.POP_Z__c,CS_Route_Segment__r.Resilience_Name__c,
                                                CS_Route_Segment__r.A_End_City__r.CS_Country__c,CS_Route_Segment__r.Z_End_Country__c,CS_Route_Segment__r.Z_POP_Name__c, CS_Route_Segment__r.Z_End_City__r.CS_Country__c,CS_Route_Segment__r.Product_Type__c,CS_Route_Segment__r.POP_A__c,
                                                CS_Route_Segment__r.CS_Cable_Path__c,CS_Route_Segment__r.Network_Technology__c,CS_Route_Segment__r.A_End_City__c, CS_Route_Segment__r.Z_End_City__c,CS_Route_Segment__r.Partner_A_End__c,CS_Route_Segment__r.Partner_Z_End__c, CS_Bandwidth_Product_Type__r.CS_Bandwidth__c,CS_Route_Segment__r.A_POP_Name__c,CS_Route_Segment__r.CS_Resilience__c, CS_Route_Segment__r.Partner_Name__r.Name, CS_Route_Segment__r.NNI_Service_ID__r.Name, CS_Route_Segment__r.PartnerName__c, CS_Route_Segment__r.NNI_ServiceName__c, CS_Route_Segment__r.NNI_Location1__c,CS_Route_Segment__r.Segment__c
                                                  FROM CS_Route_Bandwidth_Product_Type_Join__c
                                                  WHERE CS_Route_Segment__r.Product_Type__c =: 'EVPL' 
                                                    AND CS_Route_Segment__r.Network_Technology__c =: 'VLAN'
                                                    AND CS_Route_Segment__r.A_End_City__r.CS_Country__c = :aEndCountry
                                                    AND CS_Route_Segment__r.Z_End_City__r.CS_Country__c = :zEndCountry
                                                    AND CS_Route_Segment__r.A_End_City__c = :aEndCity
                                                    AND CS_Route_Segment__r.Z_End_City__c = :zEndCity
                                                    AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c IN :bwgrpId
                                                   // AND  CS_Route_Segment__r.Resilience_Name__c IN : Resiliencefilter 
                                                     ]; 
                                                     }
                                                      System.Debug('routesegemnt vlan++++++++++++'+routeSegList2);
                                                     
                                                       System.Debug('routesegwithnoPOPEVPL +++++++++++' + routeSegList1.size());
                                                       }
                                                       
   
        if(routeSegList.size()>0){
            FinalList.addAll(routeSegList);
        }
        if(routeSegList1.size()>0){
            FinalList.addAll(routeSegList1);
        }
        if(routeSegList2.size()>0){
            FinalList.addAll(routeSegList2);
        }
        
        System.debug('FINAL LIST+++++++++++++++++'+FinalList);
        }
        
        
     
    //Map<String,CS_Route_Segment__c> routeKeyMap = new Map<String,CS_Route_Segment__c>();
    List<CS_PTP_Rate_Card_Dummy__c> rateCardDummyList = new List<CS_PTP_Rate_Card_Dummy__c>();
     List<CS_PTP_Rate_Card_Dummy__c> rateCardDummyListSorted = new List<CS_PTP_Rate_Card_Dummy__c>();
    List<CS_PTP_Rate_Card_Dummy__c> rateCardDummyListnew = new List<CS_PTP_Rate_Card_Dummy__c>();
    //Map<String,cspmb__Price_Item__c> priceItemMap = new Map<String,cspmb__Price_Item__c>();
    List<cspmb__Price_Item__c> PriceItemList = new List<cspmb__Price_Item__c>();
    PriceItemList = [Select Id, cspmb__Recurring_Charge__c, cspmb__One_Off_Charge__c,Critical_MRC__c,Interactive_MRC__c,Low_Priority_MRC__c ,Standard_MRC__c,Video_MRC__c,Voice_MRC__c, CS_Route_Bandwidth_Product_Type_Join__c,CS_Bandwidth_Product_Type__r.Bandwidth_Name__c from cspmb__Price_Item__c where CS_Route_Bandwidth_Product_Type_Join__c =: FinalList];
                   
                    for(CS_Route_Bandwidth_Product_Type_Join__c bwjoin : FinalList ){
                    
                    
                    System.debug('Inside for 1 +++++++++++++++++++++++++++');
                        
                            for(cspmb__Price_Item__c pl : PriceItemList ){
                            
                            System.debug('Inside for 2 +++++++++++++++++++++++++++');
                                if(bwjoin.id == pl.CS_Route_Bandwidth_Product_Type_Join__c){
                                
                                
                                           System.debug('Inside IF+++++++++++++++++++++++++++');
                                        CS_PTP_Rate_Card_Dummy__c dummyRateCard = new CS_PTP_Rate_Card_Dummy__c(); 
                                        dummyRateCard.Product__c = bwjoin.CS_Route_Segment__r.Product_Type__c;
                                         if(bwjoin.CS_Route_Segment__r.Network_Technology__c == 'VLAN'){
                                                dummyRateCard.MRC__c=((pl.Critical_MRC__c*(Decimal.valueof(Criticaldata)/100))+(pl.Interactive_MRC__c*(Decimal.valueof(Interactivedata)/100))+(pl.Low_Priority_MRC__c*(Decimal.valueof(Lowprioritydata)/100))+(pl.Standard_MRC__c*(Decimal.valueof(Standarddata)/100))+(pl.Video_MRC__c*(Decimal.valueof(Video)/100))+(pl.Voice_MRC__c*(Decimal.valueof(Voice)/100))).setscale(2);
                                            }
                                            else if(pl.cspmb__Recurring_Charge__c<0){
                                                dummyRateCard.MRC__c= 0.00;
                                            }
                                         else{
                                               dummyRateCard.MRC__c=pl.cspmb__Recurring_Charge__c;
                                            }                                       
                                        dummyRateCard.CS_A_Country__c = aEndCountry;
                                        dummyRateCard.CS_A_POP__c=bwjoin.CS_Route_Segment__r.POP_A__c;
                                        // dummyRateCard.CS_Bandwidth_Product_Type__c=pl.CS_Bandwidth_Product_Type__c;
                                         dummyRateCard.CS_Cable_Path__c=bwjoin.CS_Route_Segment__r.CS_Cable_Path__c;
                                         dummyRateCard.CS_Route_Segment__c=bwjoin.CS_Route_Segment__c;
                                         dummyRateCard.CS_Z_Country__c=zEndCountry;
                                         dummyRateCard.CS_Z_POP__c=bwjoin.CS_Route_Segment__r.POP_Z__c;
                                         dummyRateCard.Price_Item__c=pl.Id;
                                         dummyRateCard.A_End_Country_Name__c = bwjoin.CS_Route_Segment__r.A_End_Country__c;
                                         dummyRateCard.Bandwidth_Name__c = pl.CS_Bandwidth_Product_Type__r.Bandwidth_Name__c;
                                         dummyRateCard.Cable_Path_Name__c = bwjoin.CS_Route_Segment__r.Cabel_Path_Name__c;
                                         dummyRateCard.Circuit_Type__c = bwjoin.CS_Route_Segment__r.Circuit_Type__c;
                                         
                                         dummyRateCard.Network_Technology__c=bwjoin.CS_Route_Segment__r.Network_Technology__c;
                                          dummyRateCard.Z_End_Country_Name__c = bwjoin.CS_Route_Segment__r.Z_End_Country__c;
										  dummyRateCard.Segment__c= bwjoin.CS_Route_Segment__r.Segment__c;
                                        // if(aEndPop!=null)
                                            dummyRateCard.A_POP_Name__c=bwjoin.CS_Route_Segment__r.A_POP_Name__c;
                                         
                                         //if(zEndPop!=null)
                                            dummyRateCard.Z_POP_Name__c=bwjoin.CS_Route_Segment__r.Z_POP_Name__c;
                                            
                                            dummyRateCard.NRC__c=pl.cspmb__One_Off_Charge__c;
                                            dummyRateCard.Partner_Name__c = bwjoin.CS_Route_Segment__r.PartnerName__c;
                                            dummyRateCard.NNI_Service_ID__c = bwjoin.CS_Route_Segment__r.NNI_ServiceName__c;
                                            dummyRateCard.CS_Resilience__c = bwjoin.CS_Route_Segment__r.CS_Resilience__c;
                                            dummyRateCard.Resilience_Name__c = bwjoin.CS_Route_Segment__r.Resilience_Name__c;
                                            dummyRateCard.NNI_Location1__c = bwjoin.CS_Route_Segment__r.NNI_Location1__c;
                                                              rateCardDummyList.add(dummyRateCard);
                                                              system.debug('@@@Dummylist'+rateCardDummyList);

                                        
                                }
                            
                            }
                    //routeKeyMap.put(bwjoin,bwjoin.CS_Route_Segment__c);
                    }
                    
                   /* SavePoint p = Database.setSavepoint() ; 
                    insert rateCardDummyList ; 
                    Database.rollback(p) ;*/
                    
                    
                    SavePoint p = Database.setSavepoint() ; 
                    rateCardDummyListSorted=RoutingTableWrapper.getSortedRoutongTable(rateCardDummyList);   
                    insert rateCardDummyListSorted ;
                     Database.rollback(p) ;
                     
                     
                    system.debug('rateCardDummyListSorted after sorting'+rateCardDummyListSorted);
                    system.debug('rateCardDummyListSorted.size()'+rateCardDummyListSorted.size());
                    Integer start;
                    Integer endVal;
                    Integer count=countOfNext==null?0:Integer.valueOf(countOfNext);
                    Integer startPageVal=startPage==null?0:Integer.valueOf(startPage);
                    Integer lastPageVal=lastPage==null?0:Integer.valueOf(lastPage);
                    Integer tmpcount=0;
                    Integer sizeVal=50;
                    Integer max=Math.mod(rateCardDummyListSorted.size()-sizeVal,sizeVal)==0?rateCardDummyListSorted.size()/sizeVal:(rateCardDummyListSorted.size()/sizeVal)+1;
                    List<cscfga__Attribute__c>Attributes=[select name,cscfga__Value__c from cscfga__Attribute__c where cscfga__Product_Configuration__c =:ProductConfigID];
                    system.debug('counterval'+count);
                    if(count>0&&startPageVal==0&&lastPageVal==0 && count<max){
                    tmpcount=0;
                    start=count*sizeVal;
                    endVal=(count+1)*sizeVal;
                    for(CS_PTP_Rate_Card_Dummy__c r:rateCardDummyListSorted){
                                        
                    if(tmpcount>=start&& tmpcount<endVal)
                    {rateCardDummyListnew.add(r);
                    }
                    tmpcount++;
                    //updateAttribute('CountForButtons',String.valueOf(count),Attributes);
                    }
                    system.debug('next/prevcount'+count);
                    system.debug('next/prev rate card'+rateCardDummyListnew.size());
                    }
                    else if(startPageVal>0&&lastPageVal==0&&count==0){
                    count=0;
                    start=count*sizeVal;
                    endVal=(count+1)*sizeVal;
                    tmpcount=0;
                    for(CS_PTP_Rate_Card_Dummy__c r:rateCardDummyListSorted){
                                        
                    if(tmpcount>=start&& tmpcount<endVal)
                    {rateCardDummyListnew.add(r);
                    }
                    tmpcount++;
                    }
                    //updateAttribute('CountForButtons',String.valueOf(count),Attributes);
                    system.debug('start rate card'+rateCardDummyListnew.size());
                    }
                    else if((lastPageVal>0&&startPageVal==0&&count==0)||(lastPageVal==0&&startPageVal==0&&count>=max)){ 
                    count=max-1;
                    start=rateCardDummyListSorted.size()-math.mod(rateCardDummyListSorted.size(),sizeVal);
                    endVal=rateCardDummyListSorted.size();
                    tmpcount=0;
                    for(CS_PTP_Rate_Card_Dummy__c r:rateCardDummyListSorted){                                     
                    if(tmpcount>=start&& tmpcount<endVal)
                    {rateCardDummyListnew.add(r);
                    }
                    tmpcount++;
                    }
                    updateAttribute('CountForButtons',String.valueOf(count),Attributes);
                    system.debug('start'+start);
                    system.debug('endVal'+endVal);
                    system.debug('ritesh rateCardDummyListSorted.size()'+rateCardDummyListSorted.size());
                    
                    system.debug(' ritesh rateCardDummyListnew.size()'+rateCardDummyListnew.size());
                    system.debug('last rate card'+rateCardDummyListnew.size());
                    }
                    
                    else{
                    count=0;
                    tmpcount=0;
                    start=0;
                    endVal=sizeVal;
                    for(CS_PTP_Rate_Card_Dummy__c r:rateCardDummyListSorted){
                                        
                    if(tmpcount>=start&& tmpcount<endVal)
                    {rateCardDummyListnew.add(r);
                    }
                    tmpcount++;
                    }
                    //updateAttribute('CountForButtons',String.valueOf(count),Attributes);
                    system.debug('initial rate card'+rateCardDummyListnew.size());
                    }
                    
                    system.debug('@@@LastDummylist'+rateCardDummyListnew);
                     system.debug('countervalend'+count); 
                     //for debug purpose
                     system.debug('ProductConfigID'+ProductConfigID);               
                   
                   //debug ends here                     
                     return rateCardDummyListnew;
                     
                    
                    
                    } 
			@Testvisible		
            private static void updateAttribute(String name,String value,List<cscfga__Attribute__c>Attributes){
                 List<cscfga__Attribute__c>Attributes1=new List<cscfga__Attribute__c>();
                   for(cscfga__Attribute__c a:Attributes){
                   if(a.name==name){
                   a.cscfga__Value__c=value;
                   }
                   Attributes1.add(a);
                   }
                   update Attributes1;
                   
            }                   
}