@isTest(SeeAllData = false)
private class CS_StandaloneASBRPRoviderLookupTest{

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds  = new List<Id>();
    private static Integer pageOffset, pageLimit;
    
    private static List<CS_Provider__c> providers;
    
    private static void initTestData(){
        providers = new List<CS_Provider__c>{
            new CS_Provider__c(Name = 'Test Provider 1', Carrier_Customer__c = Boolean.valueOf('true'), NAS_Product__c = Boolean.valueOf('true'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('false')),
            new CS_Provider__c(Name = 'Test Provider 2', NAS_Product__c = Boolean.valueOf('true'), Offnet_Provider__c = Boolean.valueOf('true'), Onnet__c = Boolean.valueOf('true'))
        };
        
        insert providers;
        list<CS_Provider__c> pro = [select id,Name from CS_Provider__c where Name = 'Test Provider 1'];
                                system.assertEquals(providers[0].name , pro[0].name);
                                System.assert(providers!=null); 
        System.debug('**** Providers List: ' + providers);
    }
    
    private static testMethod void doLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            searchFields.put('Scenario', 'Customer NNI');
            searchFields.put('Offnet Provider Calculated', 'Test');
            searchFields.put('searchValue', '');
            
                        
            CS_StandaloneASBRPRoviderLookup standaloneASBRLookup = new CS_StandaloneASBRPRoviderLookup();
            String reqAtts = standaloneASBRLookup.getRequiredAttributes();
            Object[] data = standaloneASBRLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            system.assertEquals(true,data!=null); 
            data.clear();
            searchFields.put('Scenario', 'NAS');
            data = standaloneASBRLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            system.assertEquals(true,data!=null); 

            data.clear();
            searchFields.put('Scenario', 'ASBR');
            data = standaloneASBRLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
            system.assertEquals(true,data!=null); 
            
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CS_StandaloneASBRPRoviderLookupTest :doLookupSearchTests';         
        ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }

}