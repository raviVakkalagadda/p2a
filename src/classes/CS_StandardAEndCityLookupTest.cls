@isTest(SeeAllData = false)
private class CS_StandardAEndCityLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds  = new List<Id>();
    private static Integer pageOffset, pageLimit;
    
    private static List<CS_Country__c> countryList;
    private static List<CS_POP__c> popList;
    private static List<CS_Route_Segment__c> routeSegmentList;
    
    private static void initTestData(){
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong')
        //  new CS_Country__c(Name = 'China')
        };
        
        insert countryList;
        System.debug('****Country List: ' + countryList);
        list<CS_Country__c> countrys = [select id,Name from CS_Country__c where Name = 'Croatia'];
        System.assert(countryList!=null);
        system.assertEquals(countryList[0].Name,countrys[0].Name);
        
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name = 'POP 1', CS_Country__c = countryList[0].Id),
            new CS_POP__c(Name = 'POP 2', CS_Country__c = countryList[1].Id),
            new CS_POP__c(Name = 'POP 3', CS_Country__c = countryList[2].Id),
            new CS_POP__c(Name = 'POP 4', CS_Country__c = countryList[3].Id),
            new CS_POP__c(Name = 'POP 5', CS_Country__c = countryList[4].Id)
                                               // new CS_POP__c(Name = 'POP 6', CS_Country__c = countryList[5].Id)
        };
        
        insert popList;
        System.debug('****POP List: ' + popList);
        list<CS_POP__c> pop = [select id,Name from CS_POP__c where Name = 'POP 1'];
        System.assert(popList!=null);
        system.assertEquals(popList[0].Name, pop[0].Name);
        
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL', POP_A__c = popList[0].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL', POP_A__c = popList[1].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 3', Product_Type__c = 'EVPL', POP_A__c = popList[2].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 4', Product_Type__c = 'EPL', POP_A__c = popList[3].Id),
            new CS_Route_Segment__c(Name = 'Route Segment 5', Product_Type__c = 'EPLX', POP_A__c = popList[4].Id)
                                              //  new CS_Route_Segment__c(Name = 'Route Segment 6', Product_Type__c = 'A-End Country', POP_A__c = popList[5].Id)
        };
        
        insert routeSegmentList;
        System.debug('****Route Segment List: ' + routeSegmentList); 
        list<CS_Route_Segment__c> route = [select id,Name from CS_Route_Segment__c where Name = 'Route Segment 1'];
        System.assert(routeSegmentList!=null);
        system.assertEquals(routeSegmentList[0].Name, route[0].Name);
       
    }
    
    
  private static testMethod void doLookupSearchTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                            
            initTestData();  
            
            searchFields.put('IPL', 'true');
            searchFields.put('EPL', 'true');
            searchFields.put('EPLX', 'true');
            searchFields.put('ICBS', 'true');
            searchFields.put('EVPL', 'true');
                                              //  searchFields.put('A-End Country', 'true');
            searchFields.put('searchValue','');
            
                        
            CS_StandardAEndCityLookup standardAEndCityLookup = new CS_StandardAEndCityLookup();
            String reqAtts = standardAEndCityLookup.getRequiredAttributes();
            Object[] data = standardAEndCityLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
        
            System.debug('*******Data: ' + data);
          system.assertEquals(true,data!=null); 

            
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_StandardAEndCityLookupTest :doLookupSearchTest';         
ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
  }

}