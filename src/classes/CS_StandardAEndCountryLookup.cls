global with sharing class CS_StandardAEndCountryLookup extends cscfga.ALookupSearch {
    

    public override String getRequiredAttributes(){ return '["IPL","EPL","EPLX","ICBS","EVPL"]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        List <String> ProductTypeNames = new List<String>();
        for (String key : searchFields.keySet())
        {
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        system.debug('ProductTypeNames >> ' + ProductTypeNames);
        Set <Id> countryIds = new Set<Id>();

        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id,POP_A__r.CS_Country__c,A_End_City__r.CS_Country__c,Product_Type__c 
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames];*/
System.debug('Sowmya-------For---' );
        for(CS_Route_Segment__c item : [SELECT Id,POP_A__r.CS_Country__c,A_End_City__r.CS_Country__c,Product_Type__c 
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames]){
        System.debug('Sowmya----------'+item.Product_Type__c );
        if(item.Product_Type__c == 'EVPL' ){
            countryIds.add(item.A_End_City__r.CS_Country__c);
            }
            else{
            countryIds.add(item.POP_A__r.CS_Country__c);
            }
        }
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_Country__c> data = [SELECT Id, Name FROM CS_Country__c WHERE  Id IN :countryIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;
    }

}