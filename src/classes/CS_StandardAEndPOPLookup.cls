global with sharing class CS_StandardAEndPOPLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country","A End City","IPL","EPL","EPLX","ICBS","EVPL" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String aEndCountry = searchFields.get('A-End Country');
        String aEndCity = searchFields.get('A End City');
        Set <Id> popIds = new Set<Id>();
        String flag = searchFields.get('EVPL');
        
       
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        
        if(flag == 'No' || flag == 'false'){
        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id, POP_A__c, POP_A__r.CS_Country__c,POP_A__r.CS_City__c, POP_Z__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND POP_A__r.CS_Country__c = :aEndCountry
                                                        AND POP_A__r.CS_City__c = :aEndCity];*/

        for(CS_Route_Segment__c item : [SELECT Id, POP_A__c, POP_A__r.CS_Country__c,POP_A__r.CS_City__c, POP_Z__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND POP_A__r.CS_Country__c = :aEndCountry
                                                        AND POP_A__r.CS_City__c = :aEndCity]){
            popIds.add(item.POP_A__c);
            System.debug('IN IF');
        }
        }
        else if ((flag == 'Yes' || flag == 'true')){
     
        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id, POP_A__c, POP_A__r.CS_Country__c,POP_A__r.CS_City__c, POP_Z__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND POP_A__r.CS_Country__c = :aEndCountry
                                                        AND POP_A__r.CS_City__c = :aEndCity];*/
                                                        
                                                        
                                                        
         List<CS_POP__c> data = [SELECT Id, CS_Country__c,CS_City__c,EVPL__c
                                                  FROM CS_POP__c
                                                  WHERE EVPL__c =: 'Y'
                                                        AND CS_Country__c = :aEndCountry
                                                        AND CS_City__c = :aEndCity
                                                        ];
                                                        
            System.debug('data +++++++++++++++++++' + data);        
                                                        
         for(CS_Route_Segment__c item : [SELECT Id, POP_A__c, POP_A__r.CS_Country__c,POP_A__r.CS_City__c, POP_Z__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND POP_A__r.CS_Country__c = :aEndCountry
                                                        AND POP_A__r.CS_City__c = :aEndCity]){
            popIds.add(item.POP_A__c);                                              
        }
        for(CS_POP__c item1 : data){
            popIds.add(item1.Id);                                                
        }
        }
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_POP__c> data = [SELECT Id, Name, Address1__c,IPVPN_Critical_Data__c,Offnet__c,Provider_Name__c ,Is_Vendor_Charge_Required__c,IPVPN_Interactive_Data__c,IPVPN_Low_Priority_Data__c,Pop_City_Code__c,Pop_CCMS_Country_Code__c,IPVPN_Remote_Site_Access_Gateway__c,IPVPN_Standard_Data__c,IPVPN_Video__c,IPVPN_Voice__c,CS_Country__c,CS_City__c FROM CS_POP__c WHERE  Id IN :popIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
       return data;

   }

}