global with sharing class CS_StandardZEndCountryLookup extends cscfga.ALookupSearch {
    
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country","IPL","EPL","EPLX","ICBS","EVPL" ]';
    }

    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
        List <String> countryNames = new List<String>();
        String aEndCountry = searchFields.get('A-End Country');
        List <String> ProductTypeNames = new List<String>();
        
        for (String key : searchFields.keySet()){
            String attValue = searchFields.get(key);
            if(attValue == 'Yes' || attValue == 'true')
                ProductTypeNames.add(key);
        }
        Set <Id> countryIds = new Set<Id>();
        /*List<CS_Route_Segment__c> routeSegList = [SELECT Id,Product_Type__c,A_End_City__r.CS_Country__c,Z_End_City__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND (A_End_City__r.CS_Country__c = :aEndCountry OR
                                                         Z_End_City__r.CS_Country__c =:aEndCountry) ];*/

        for(CS_Route_Segment__c item : [SELECT Id,Product_Type__c,A_End_City__r.CS_Country__c,Z_End_City__r.CS_Country__c
                                                  FROM CS_Route_Segment__c
                                                  WHERE Product_Type__c in :ProductTypeNames 
                                                        AND (A_End_City__r.CS_Country__c = :aEndCountry OR
                                                         Z_End_City__r.CS_Country__c =:aEndCountry) ]){
        if(item.Product_Type__c == 'EVPL'){
           countryIds.add(item.Z_End_City__r.CS_Country__c);
        }else{
            countryIds.add(item.Z_End_City__r.CS_Country__c);
            }
        }
        System.Debug('doLookupSearch');
        System.Debug(searchFields);
        String searchValue = searchFields.get('searchValue') +'%';
        List <CS_Country__c> data = [SELECT Id, Name FROM CS_Country__c WHERE  Id IN :countryIds AND Name LIKE :searchValue ORDER BY Name];
        System.Debug(data);
        return data;
    }

}