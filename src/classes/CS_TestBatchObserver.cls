@IsTest
public class CS_TestBatchObserver {

	
	@IsTest
	static void testObserver() {
		Test.starttest();
		P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
		MACD_Service_Links__c mcd = new MACD_Service_Links__c(name='Internet-Onnet',Service_Field__c='GID_Service__c');
        insert mcd;
         
      Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;   
       
        P2A_TestFactoryCls.SetupTestData();

        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
         
        
        List<cscfga__Attribute_Definition__c>  AttDef= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef = new cscfga__Attribute_Definition__c(); 
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.Attribute_Type__c = 'Product Configuration';
                Attributesdef.Group_Name__c = 'Test Group name'; 
                Attributesdef.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef.add(Attributesdef);              
            
            insert AttDef;
        System.assertEquals(AttDef[0].name,'Test Att'); 
            
   
       List<cscfga__Attribute__c> lstAt = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes.cscfga__Value__c = proconfig[0].id ;
                Attributes.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes.cscfga__Attribute_Definition__c = attdef[0].id; 
                lstAt.add(Attributes);
                insert lstAt;            
        System.assertEquals(lstAt[0].name,'GCPE'); 
           
        Set<id> st = new Set<id>{servlist[0].id};
        Set<String> macdSerFieldListfields = new Set<String>();
        macdSerFieldListfields.add('name');
        Set<id> Proconfigsids = new Set<id>{proconfig[0].id};
        
        List<cscfga__Attribute_Definition__c>  AttDef1= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef1 = new cscfga__Attribute_Definition__c(); 
                Attributesdef1.name = 'Test Att';
                Attributesdef1.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef1.cscfga__Column__c = 2;
                Attributesdef1.cscfga__Row__c = 4;
                Attributesdef1.Attribute_Type__c = 'service';
                Attributesdef1.Group_Name__c = 'Test Group name'; 
                Attributesdef1.Relationship_Type__c ='ASBRPointerMultiple'; 
                AttDef1.add(Attributesdef1);
                insert AttDef1;
        System.assertEquals(AttDef1[0].name,'Test Att'); 
                
                 List<csord__Service__c> servicelist = new List<csord__Service__c>();
                csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c = ordReq[0].id;
                ser.csord__Subscription__c =  SUBList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.csordtelcoa__Product_Configuration__c = proconfig[0].id;
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = '';     
  
                servicelist.add(ser);
 
            insert servicelist;
        System.assertEquals(servicelist[0].name,'Test Service'); 
                                                 
                
                List<cscfga__Attribute__c> lstAt1 = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c();
                Attributes1.name = 'GCPE';
                Attributes1.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes1.cscfga__Value__c = servicelist[0].id ;
                Attributes1.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes1.cscfga__Attribute_Definition__c = attdef1[0].id; 
                lstAt1.add(Attributes1);
                insert lstAt1;
        System.assertEquals(lstAt1[0].name,'GCPE'); 
                
                List<cscfga__Attribute_Definition__c>  AttDef2= new List<cscfga__Attribute_Definition__c>();
         cscfga__Attribute_Definition__c Attributesdef2 = new cscfga__Attribute_Definition__c(); 
                Attributesdef2.name = 'Test Att';
                Attributesdef2.cscfga__Product_Definition__c=  prodef[0].id;
                Attributesdef2.cscfga__Column__c = 2;
                Attributesdef2.cscfga__Row__c = 4;
                Attributesdef2.Attribute_Type__c = 'service';
                Attributesdef2.Group_Name__c = 'Test Group name'; 
                Attributesdef2.Relationship_Type__c = 'Master'; 
                AttDef2.add(Attributesdef2);
                insert AttDef2;
        System.assertEquals(AttDef2[0].name,'Test Att'); 
               
                 List<cscfga__Attribute__c> lstAt2 = new List<cscfga__Attribute__c>(); 
        cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c();
                Attributes2.name = 'GCPE';
                Attributes2.cscfga__Product_Configuration__c= proconfig[0].id;
                Attributes2.cscfga__Value__c = servicelist[0].id ;
                Attributes2.cscfga__Display_Value__c = 'Test displayvalue' ;
                Attributes2.cscfga__Attribute_Definition__c = attdef2[0].id; 
                lstAt2.add(Attributes2);
                insert lstAt2;
        System.assertEquals(lstAt2[0].name,'GCPE'); 
		try {
			CS_BatchMACDObserver csobs = new CS_BatchMACDObserver();
			csobs.linkConfigurationsInMacd(Proconfigsids);
		} catch (Exception e) {
			
		}
		Test.Stoptest();
		
	}
	
	
}