@IsTest
public class CS_TestServiceMapping {
    
    
    @TestSetup
    static void setup() {
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
           
        
        Product2 prod = new Product2(Name = 'Laptop X200', 
                                     Family = 'Hardware');
        insert prod;
        system.assertEquals(true,prod!=null); 
        Id pricebookId = Test.getStandardPricebookId();
        
        PricebookEntry pbe = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 300, IsActive = true);
            pbe.UseStandardPrice = false;
        insert pbe;
         system.assertEquals(true,pbe!=null); 
        
        List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity();
        opp.Name = 'Test Opportunity';
        opp.StageName = 'Identify And Define';
        opp.CloseDate = System.today();
        opp.QuoteStatus__c = 'Approved';
        opp.Product_Type__c = 'IPL';
                opp.Estimated_MRC__c = 100.00;
                opp.Estimated_NRC__c = 100.00;
        Opplist.add(opp);
        insert OPplist;
         system.assertEquals(true,OPplist!=null); 
        Global_Mute__c gm = new Global_Mute__c(
            Mute_Triggers__c = true
        );
        
        insert gm;
        system.assertEquals(true,gm!=null); 
        OpportunityLineItem opplineitem = new OpportunityLineItem();
        List<OpportunityLineItem> opplineitemlist = new List<OpportunityLineItem>(); 
        opplineitem.Opportunityid = OppList[0].id;
        opplineitem.Quantity = 12;
        opplineitem.TotalPrice = 100;
        // opplineitem.PricebookEntryId = pbEntry.id; 
        opplineitem.PricebookEntryId = pbe.id;
        opplineitemlist.add(opplineitem);
        insert opplineitemlist;
        system.assertEquals(true,opplineitemlist!=null); 
        gm.Mute_Triggers__c = false;
        update gm;
        system.assertEquals(true,gm!=null); 
        list<cscfga__Product_Basket__c> basketlist = new list<cscfga__Product_Basket__c>();
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        pb.csbb__Account__c = Acclist[0].id;
        pb.cscfga__Opportunity__c = opplist[0].id;
        basketlist.add(Pb);
        Insert pb;
        system.assertEquals(true,pb!=null); 
        cscfga__Product_Category__c cat = new cscfga__Product_Category__c(
            Name = 'Test Produc Category'
        );
        insert cat;
         system.assertEquals(true,cat!=null); 
        cscfga__product_definition__c pd = new cscfga__product_definition__c(
            name = 'Test',
            cscfga__Description__c = 'test',
            cscfga__Product_Category__c = cat.Id
        );
        insert pd;
        system.assertEquals(true,pd!=null); 
        cscfga__attribute_definition__c ad = new cscfga__attribute_definition__c(
            name = 'test',
            cscfga__product_definition__c = pd.Id,
            cscfga__Output_Mapping__c = 'Name'
        );
        insert ad;
        system.assertEquals(true,ad!=null); 
        cscfga__attribute_definition__c ad2 = new cscfga__attribute_definition__c(
            name = 'test 2',
            cscfga__product_definition__c = pd.Id,
            cscfga__Output_Mapping__c = 'Service_Status__c',
            cscfga__Data_Type__c = 'Boolean',
            cscfga__type__c = 'Checkbox'
        );
        insert ad2;
        system.assertEquals(true,ad2!=null); 
        cscfga__attribute_definition__c ad3 = new cscfga__attribute_definition__c(
            name = 'Date',
            cscfga__product_definition__c = pd.Id,
            cscfga__Output_Mapping__c = 'csord__Activation_Date__c',
            cscfga__type__c = 'Date'
        );
        insert ad3;
        system.assertEquals(true,ad3!=null); 
        cscfga__attribute_definition__c ad4 = new cscfga__attribute_definition__c(
            name = 'Decimal',
            cscfga__product_definition__c = pd.Id,
            cscfga__Output_Mapping__c = 'Line_Number__c',
            cscfga__Data_Type__c = 'Decimal'
        );
        insert ad4;
          system.assertEquals(true,ad4!=null);
        cscfga__attribute_definition__c ad5 = new cscfga__attribute_definition__c(
            name = 'Integer',
            cscfga__product_definition__c = pd.Id,
            cscfga__Output_Mapping__c = 'Block_Number__c',
            cscfga__Data_Type__c = 'Integer'
        );
        insert ad5;
        system.assertEquals(true,ad5!=null);
        cscfga__attribute_definition__c ad6 = new cscfga__attribute_definition__c(
            name = 'Calculation',
            cscfga__product_definition__c = pd.Id,
            cscfga__Data_Type__c = 'String'
        );
        insert ad6;
        system.assertEquals(true,ad6!=null);
        cscfga__product_configuration__c pc = new cscfga__product_configuration__c(
            name = 'test',
            cscfga__product_definition__c = pd.Id,
            cscfga__product_basket__c = pb.Id,
            cscfga__total_one_off_charge__c = 0,
            cscfga__Total_Price__c = 0,
            cscfga__total_recurring_charge__c = 0,
            cscfga__Product_Family__c = 'Test'
        );
        insert pc;
         system.assertEquals(true,pc!=null);   
        cscfga__attribute__c at = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad.Id,
            cscfga__product_configuration__c = pc.Id,
            cscfga__Is_Line_Item__c = true,
            cscfga__Price__c = 10,
            cscfga__List_Price__c = 10,
            name = 'test',
            cscfga__Recurring__c = true,
            cscfga__is_active__c = true,
            cscfga__Line_Item_Description__c = 'Test 123'
        );
        insert at;
        system.assertEquals(true,at!=null); 
        cscfga__attribute__c at2 = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad2.Id,
            cscfga__product_configuration__c = pc.Id,
            name = 'test 2'
        );
        insert at2;
        system.assertEquals(true,at2!=null);
        cscfga__attribute__c at3 = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad3.Id,
            cscfga__product_configuration__c = pc.Id,
            name = 'Date'
        );
        insert at3;
        system.assertEquals(true,at3!=null);
        cscfga__attribute__c at4 = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad4.Id,
            cscfga__product_configuration__c = pc.Id,
            name = 'Decimal'
        );
        insert at4;
        system.assertEquals(true,at4!=null);
        cscfga__attribute__c at5 = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad5.Id,
            cscfga__product_configuration__c = pc.Id,
            name = 'Integer'
        );
        insert at5;
         system.assertEquals(true,at5!=null);
        cscfga__attribute__c at6 = new cscfga__attribute__c(
            cscfga__attribute_definition__c = ad6.Id,
            cscfga__product_configuration__c = pc.Id,
            name = 'Calculation'
        );
        insert at6;
        system.assertEquals(true,at6!=null);
        csbb__Product_Configuration_Request__c pcr = new csbb__Product_Configuration_Request__c(
            csbb__Product_Category__c = cat.Id, 
            csbb__Product_Configuration__c = pc.Id,
            csbb__Product_Basket__c = pb.Id
        );
        insert pcr;
        system.assertEquals(true,pcr!=null);
        OLI_Sync__c osync = new OLI_Sync__c();
        insert osync;
        system.assertEquals(true,osync!=null);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        list<OpportunityTeamMember> OppTeamList = new list<OpportunityTeamMember>();
        OpportunityTeamMember OppTeam = new OpportunityTeamMember();
        OppTeam.TeamMemberRole = 'Sales special list';
        OppTeam.UserId = users[0].id;
        Oppteam.OpportunityId = Opplist[0].id;
        OppTeamList.add(OppTeam);
        insert OppTeamList;
        system.assertEquals(true,OppTeamList!=null);
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,orderRequestList);
        List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,new List<cscfga__Product_basket__c>{pb});
    }
    
    
    @IsTest
    static void testMapping() {
        Test.startTest();
        cscfga__product_configuration__c config = [
            select id, (
                select id, cscfga__value__c, Name
                from cscfga__attributes__r
            )
            from cscfga__product_configuration__c
            limit 1
        ];
        csord__Service__c srv = [
            select id, csordtelcoa__product_configuration__c from csord__Service__c
            limit 1
        ];
        srv.csordtelcoa__product_configuration__c = config.Id;
        update srv;
        system.assertEquals(true,srv!=null);
        for (cscfga__attribute__c att : config.cscfga__attributes__r) {
            if (att.Name == 'Decimal' || att.Name == 'Integer') {
                att.cscfga__value__c = String.valueOf(1);
            } else if (att.Name != 'Date') {
                att.cscfga__value__c = 'Yes';
            } else {
                att.cscfga__value__c = String.valueOf(System.Today());
            }
        }
        update config.cscfga__attributes__r;
        Test.stopTest();
    }

}