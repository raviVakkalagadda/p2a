public with sharing class CS_VLANGroupNameIdentifierHandler{
	
	public static set<Id> vlanGroupBasketIdSet = new set<Id>();
	public static Boolean isFirstRun = false;  
	
	private static string vlanGroupProductDefinitionId = Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c;
	public static Map<Id,Integer> basketConfigCountMap = new Map<Id,Integer>();
	
	public static void vlanGroupNameIdentifierLogic(List<cscfga__Product_Configuration__c> vlanGroups){	
	    
	    if(!isFirstRun){
	        isFirstRun = true; 
    		for(cscfga__Product_Configuration__c vlanGroup : vlanGroups){
    			if(vlanGroup.cscfga__Product_Definition__c == vlanGroupProductDefinitionId && vlanGroup.cscfga__Product_Basket__c != null){
    				vlanGroupBasketIdSet.add(vlanGroup.cscfga__Product_Basket__c);
    			}
    		}
    		
    		if(vlanGroupBasketIdSet != null && !vlanGroupBasketIdSet.IsEmpty()){
    			/*AggregateResult[] ag = [SELECT Count(Id) countId, cscfga__Product_Basket__c basketId FROM cscfga__Product_Configuration__c 
    									WHERE cscfga__Product_Basket__c in:vlanGroupBasketIdSet AND
    									cscfga__Product_Definition__c =: vlanGroupProductDefinitionId Group By cscfga__Product_Basket__c]; */
    									
    			for(AggregateResult result : [SELECT Count(Id) countId, cscfga__Product_Basket__c basketId FROM cscfga__Product_Configuration__c 
    									WHERE cscfga__Product_Basket__c in:vlanGroupBasketIdSet AND
    									cscfga__Product_Definition__c =: vlanGroupProductDefinitionId Group By cscfga__Product_Basket__c]){
    				basketConfigCountMap.put((Id)result.get('basketId'), Integer.valueOf(result.get('countId')) + 1);
    			}
    		}
    		
    		System.debug('******VLAN Group basketConfigCountMap:'+ basketConfigCountMap);
    		
    		if(!basketConfigCountMap.isEmpty()){
    			Integer counter = 0;
    			for(cscfga__Product_Configuration__c pc : vlanGroups){
    				if(basketConfigCountMap.get(pc.cscfga__Product_Basket__c) != null){
    					Integer total = counter + basketConfigCountMap.get(pc.cscfga__Product_Basket__c);
                        pc.name_Identifier__c = pc.name + ' ' + total;
                        counter++;
    				}
    			}
    		}
	    }
	}
}