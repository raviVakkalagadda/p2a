@isTest(SeeAllData = false)
public class CS_VLANGroupNameIdentifierHandler_Test
{
    private static List<cscfga__Product_Basket__c> basketList = new List<cscfga__Product_Basket__c>();
    private static List<cscfga__Product_Configuration__c> vlanGroups = new List<cscfga__Product_Configuration__c>();
    private static string vlanGroupProductDefinitionId = '';
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
    
    private static void createCustomSettings(){
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        //setting.Product_Id__c = 'VLANGroup_Definition_Id';
        insert setting;
        system.assertEquals(true,setting!=null); 
        vlanGroupProductDefinitionId = Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c;
        System.debug('vlanGroupProductDefinitionId: ' + vlanGroupProductDefinitionId);
    }
    
    private static void createTestData(){
           
        basketList.add(new cscfga__Product_Basket__c(Name = 'Test basket 1'));
        basketList.add(new cscfga__Product_Basket__c(Name = 'Test basket 2'));
        basketList.add(new cscfga__Product_Basket__c(Name = 'Test basket 3'));
        
        insert basketList;
        System.debug('**** basketList: ' + basketList);
         system.assertEquals(true,basketList!=null); 
     /*   for(cscfga__Product_Basket__c basket : basketList){
            vlanGroups.add(new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = vlanGroupProductDefinitionId, Name = 'VlanGroup1 from ' + basket.Name, cscfga__Product_Basket__c = basket.Id));
            vlanGroups.add(new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = vlanGroupProductDefinitionId, Name = 'VlanGroup2 from ' + basket.Name, cscfga__Product_Basket__c = basket.Id));
        }
        
        insert vlanGroups;
        System.debug('**** vlanGroupList: ' + vlanGroups);*/
        
        
        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> vlanGroups = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
         system.assertEquals(true,vlanGroups!=null); 
    }
    
    static testMethod void VlanTest() 
    {
        Exception ee = null;
        integer userid = 0;
        try{
            disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
           Test.startTest();
            createCustomSettings();
            createTestData();  
            CS_VLANGroupNameIdentifierHandler.vlanGroupNameIdentifierLogic(vlanGroups);
            Test.stopTest();
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_VLANGroupNameIdentifierHandler_Test :VlanTest';         
            ErrorHandlerException.sendException(e); 

            ee = e;
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
           
    }
    
    static testMethod void VlanTestBlank(){
        Exception ee = null;
        integer userid = 0;
        try{
            disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            Test.startTest(); 
            createCustomSettings();
            CS_VLANGroupNameIdentifierHandler.vlanGroupNameIdentifierLogic(vlanGroups);
            Test.stopTest();
        } catch(Exception e){
            ErrorHandlerException.ExecutingClassName='CS_VLANGroupNameIdentifierHandler_Test :VlanTestBlank';         
            ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
           // Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
    }
}