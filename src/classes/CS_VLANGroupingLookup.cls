global with sharing class CS_VLANGroupingLookup extends cscfga.ALookupSearch {
    
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){

        String vlanPorts = searchFields.get('VLAN Ports Ids');
        String basketId = searchFields.get('BasketId');
        String masterService = searchFields.get('Master Service Calculated');
        String configId = searchFields.get('ConfigId');

        List<String> vlansPortIDs = new List<String>();
        if(vlanPorts != null){
        	vlansPortIDs =  vlanPorts.split(',');
        }

        System.debug('***** CS_VLANGroupingLookup vlansPortIDs: '+vlansPortIDs);
        List <cscfga__Product_Configuration__c> VLANSList;
        List <cscfga__Attribute__c> VLANGroupList;
        
        if(masterService != null && masterService != ''){
        	VLANSList = [SELECT Id, Name, cscfga__Parent_Configuration__c, 
        													   cscfga__Product_Basket__c, 
        													   Product_Definition_Name__c,
        													   cscfga__Parent_Configuration__r.Master_IPVPN_Configuration__c,
        													   Pop_Country__c,
        													   Pop_City__c,
        													   Class_of_Service_Mix__c,
        													   Port_Speed__c,
                                                               Port_Speed_Name__c,
        													   POP__c
        												FROM cscfga__Product_Configuration__c
        												WHERE cscfga__Product_Basket__c = :basketId
        													  AND Product_Code__c = 'VLV'
        													  AND cscfga__Parent_Configuration__r.Master_IPVPN_Configuration__c = :masterService
        													  AND cscfga__Parent_Configuration__c NOT IN :vlansPortIDs];

        	VLANGroupList =[SELECT Id, Name, cscfga__Product_Configuration__c,
								cscfga__Product_Configuration__r.cscfga__Product_Basket__c, 
								cscfga__Product_Configuration__r.Product_Definition_Name__c,
								cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c,
								cscfga__Value__c
						FROM cscfga__Attribute__c
						WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
							  AND cscfga__Product_Configuration__r.cscfga__Product_Definition__c = :productDefinitionId
							  AND cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c = :masterService
							  AND Name = 'VLANs'
                              AND cscfga__Product_Configuration__c != :configId];
        } else {
        	VLANSList = [SELECT Id, Name, cscfga__Parent_Configuration__c, 
        													   cscfga__Product_Basket__c, 
        													   Product_Definition_Name__c,
        													   Master_IPVPN_Configuration__c,
        													   Pop_Country__c,
        													   Pop_City__c,
        													   Class_of_Service_Mix__c,
        													   Port_Speed__c,
                                                               Port_Speed_Name__c,
        													   POP__c
        												FROM cscfga__Product_Configuration__c
        												WHERE cscfga__Product_Basket__c = :basketId
        													  AND Product_Code__c = 'VLV'
        													  AND cscfga__Parent_Configuration__c NOT IN :vlansPortIDs];
        	VLANGroupList =[SELECT Id, Name, cscfga__Product_Configuration__c,
								cscfga__Product_Configuration__r.cscfga__Product_Basket__c, 
								cscfga__Product_Configuration__r.Product_Definition_Name__c,
								cscfga__Product_Configuration__r.Master_IPVPN_Configuration__c,
								cscfga__Value__c
						FROM cscfga__Attribute__c
						WHERE cscfga__Product_Configuration__r.cscfga__Product_Basket__c = :basketId
							  AND cscfga__Product_Configuration__r.cscfga__Product_Definition__c = :productDefinitionId
							  AND Name = 'VLANs'
                              AND cscfga__Product_Configuration__c != :configId];
        }
        
        System.debug('***** CS_VLANGroupingLookup VLANSList: '+VLANSList);

        
		
		System.debug('***** CS_VLANGroupingLookup VLANGroupList: '+VLANGroupList);

		String takenVLANS ='';       										  
		for(cscfga__Attribute__c att : VLANGroupList){
			takenVLANS += att.cscfga__Value__c;
		}
		System.debug('***** CS_VLANGroupingLookup takenVLANS: '+takenVLANS);
		List <cscfga__Product_Configuration__c> returnList = new List <cscfga__Product_Configuration__c>();

		for(cscfga__Product_Configuration__c pc : VLANSList){
			if(!takenVLANS.contains(pc.Id)){
				returnList.add(pc);
			}
		}

        return returnList;
	}

	public override String getRequiredAttributes(){ 
	    return '["BasketId","VLAN Ports Ids","Product ID","Max MAC Address","Master Service Calculated","ConfigId"]';
	}

	
	
}