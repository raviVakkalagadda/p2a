@isTest(SeeAllData = false)
private class CS_VLANGroupingServiceLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static List<cscfga__Attribute__c> searchFieldAttributes;
    private static String productDefinitionId;
    private static Id[] excludeIds;
    private static Integer pageOffset = 2;  
    private static Integer pageLimit = 3;
    
    private static List<cscfga__Product_Basket__c> basketList;
    private static List<cscfga__Product_Definition__c> productDefinitionList;
    private static List<cscfga__Product_Configuration__c> vlansList;
    private static List<cscfga__Product_Configuration__c> parentConfigList;
    private static List<cscfga__Product_Configuration__c> masterServiceList;
    private static List<cscfga__Attribute__c> vlanGroupList;
        
    private static void initTestData(){
        
        basketList = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(Name = 'Basket 1')
        };
        
        insert basketList;
        system.assert(basketList!=null);
        system.assertEquals(basketList[0].name,'Basket 1');
        System.debug('**** Basket List: ' + basketList);
        
        productDefinitionList = new List<cscfga__Product_Definition__c>{
                  new cscfga__Product_Definition__c(Name = 'VLAN', cscfga__Description__c = 'VLAN'),
                  new cscfga__Product_Definition__c(Name = 'VPLS VLAN Port', cscfga__Description__c = 'VPLS VLAN Port'),
                  new cscfga__Product_Definition__c(Name = 'Master VPLS Service', cscfga__Description__c = 'Master VPLS Service')
            };   
            
        insert productDefinitionList;
        system.assert(productDefinitionList!=null);
        system.assertEquals(productDefinitionList[0].name,'VLAN');
        System.debug('**** Product Definitions: ' + productDefinitionList);
        
        masterServiceList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'Master VPLS Service', cscfga__Product_Definition__c = productDefinitionList[2].Id, cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert masterServiceList;
        system.assert(masterServiceList!=null);
        system.assertEquals(masterServiceList[0].name,'Master VPLS Service');
        System.debug('**** Master Service List: ' + masterServiceList);
        
        parentConfigList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 1', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 2', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VPLS VLAN Port 3', cscfga__Product_Definition__c = productDefinitionList[1].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id, cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert parentConfigList;
        system.assert(parentConfigList!=null);
        system.assertEquals(parentConfigList[0].name,'VPLS VLAN Port 1');
        System.debug('**** Parent Config List: ' + parentConfigList);
        
        vlansList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'VLAN 1', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[0].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 2', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[0].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 3', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[1].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 4', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[1].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 5', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[2].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'VLAN 6', cscfga__Product_Definition__c = productDefinitionList[0].Id, cscfga__Parent_Configuration__c = parentConfigList[2].Id, cscfga__Product_Basket__c = basketList[0].Id, Master_IPVPN_Configuration__c = masterServiceList[0].Id)
        };
        
        insert vlansList;
        system.assert(vlansList!=null);
        system.assertEquals(vlansList[0].name,'VLAN 1');
        System.debug('**** VLAN List: ' + vlansList);
        
        
        vlanGroupList = new List<cscfga__Attribute__c>{
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[0].Id, cscfga__Value__c = vlansList[1].Id),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[1].Id, cscfga__Value__c = ''),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[2].Id, cscfga__Value__c = vlansList[3].Id),
            new cscfga__Attribute__c(Name = 'VLANs', cscfga__Product_Configuration__c = vlansList[3].Id, cscfga__Value__c = '')
        };
        
        insert vlanGroupList;
        system.assert(vlanGroupList!=null);
        system.assertEquals(vlanGroupList[0].name,'VLANs');
        System.debug('**** VLAN Group List: ' + vlanGroupList);
        
        
    }    
        
  private static testMethod void doLookupSearchTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            productDefinitionId = productDefinitionList[0].Id;
            
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('VLAN Ports Ids', vlansList[4].Id + ',' + vlansList[5].Id );
            searchFields.put('Product ID', 'true');
            searchFields.put('Max MAC Address', 'true');
            searchFields.put('Master Service Calculated', masterServiceList[0].Id);
            searchFields.put('ConfigId', vlansList[4].Id);
            
                        
            CS_VLANGroupingServiceLookup vlanGroupingServiceLookup = new CS_VLANGroupingServiceLookup();
            String reqAtts = vlanGroupingServiceLookup.getRequiredAttributes();
            Object[] data = vlanGroupingServiceLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            system.assert(vlanGroupingServiceLookup!=null);
            System.debug('*******Data: ' + data);
           // System.assert(data.size() > 0, '');
            
            searchFields.put('Master Service Calculated', '');
            data.clear();
            data = vlanGroupingServiceLookup.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            System.debug('*******Data: ' + data);
          //  System.assert(data.size() > 0, '');
           Test.stopTest();
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_VLANGroupingServiceLookupTest:doLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
           
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
  }

}