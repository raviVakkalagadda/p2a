@isTest(SeeAllData = false)
private class CS_VPLSTPriceItemLookupTest {

    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProdTypeList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<Account> accountList;
    
    private static void initTestData(){

        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;
        system.assertEquals(accountList[0].name,'Account 1');
        System.debug('****Account List: ' + accountList);
        
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'Port Country'),
            new CS_Country__c(Name = 'Croatia'),
            new CS_Country__c(Name = 'Australia'),
            new CS_Country__c(Name = 'India'),
            new CS_Country__c(Name = 'Germany'),
            new CS_Country__c(Name = 'Hong Kong'),
            new CS_Country__c(Name = 'New Zealand')
        };
        
        insert countryList;
        system.assertEquals(countryList[0].name,'Port Country');
        System.debug('****Country List: ' + countryList);
        
        cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'Pop City', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Pop City', CS_Country__c = countryList[1].Id)
        };
        
        insert cityList;
        system.assertEquals(cityList[0].name,'Pop City');
        System.debug('****City List: ' + cityList);
        
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name = '64k', Bandwidth_Value_MB__c = 64),
            new CS_Bandwidth__c(Name = '128k', Bandwidth_Value_MB__c = 128)
        };
        
        insert bandwidthList;
        system.assertEquals(bandwidthList[0].name,'64k');
        System.debug('****Bandwidth List: ' + bandwidthList);
        
        bandwidthProdTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name = 'Port Speed', CS_Bandwidth__c = bandwidthList[0].Id, Product_Type__c = 'IPVPN'),
            new CS_Bandwidth_Product_Type__c(Name = 'Port Speed', CS_Bandwidth__c = bandwidthList[1].Id, Product_Type__c = 'IPVPN')
        };
        
        insert bandwidthProdTypeList;
        system.assertEquals(bandwidthProdTypeList[0].name,'Port Speed');
        System.debug('****Bandwidth Product Type List: ' + bandwidthProdTypeList);        
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = accountList[0].Id, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Onnet', Pricing_Segment__c = 'Account Customer Type',/*Generic_Product_Type__c = 'Product Type',*/cspmb__Product_Definition_Name__c = 'IPT'),
            new cspmb__Price_Item__c(Name = 'Price Item 2', Country__c = countryList[0].Id, City__c = cityList[0].Id, CS_Bandwidth_Product_Type__c = bandwidthProdTypeList[0].Id, cspmb__Account__c = null, cspmb__Is_Active__c = true, IPVPN_Type__c = 'Standard', Connectivity__c = 'Offnet', Pricing_Segment__c = 'Account Customer Type', /*Generic_Product_Type__c = 'Product Type',*/cspmb__Product_Definition_Name__c = 'IPT')
        };
        
        insert priceItemList;
        system.assertEquals(priceItemList[0].name,'Price Item 1');
        System.debug('****Price Item List: ' + priceItemList);        
        
        String accountID = accountList[0].id;
        CS_VPLSTPriceItemLookup vplstPriceItemLookup1 = new CS_VPLSTPriceItemLookup();
        List<cspmb__Price_Item__c> ListPriceItem = vplstPriceItemLookup1.getAccountPricing(priceItemList,accountID);
        
        Map<String,Object> wherePart = new Map<String,Object>();
        wherePart.put('Test',priceItemList[0]);
        
    }
    
  private static testMethod void doDynamicLookupSearchTest() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initTestData();  
            
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('PortPOP City', cityList[0].Id);
            searchFields.put('Country Name', countryList[0].Id);
            searchFields.put('Port Speed', bandwidthProdTypeList[0].Id);
            searchFields.put('Port Type', 'Standard');
            searchFields.put('Account Customer Type', 'MNC');
            searchFields.put('Product Type', 'IPVPN');
            searchFields.put('Onnet or Offnet', 'Onnet');
            
            
            
            CS_VPLSTPriceItemLookup vplstPriceItemLookup = new CS_VPLSTPriceItemLookup();
            String reqAtts = vplstPriceItemLookup.getRequiredAttributes();
            Object[] data = vplstPriceItemLookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            system.assert(vplstPriceItemLookup !=null);
            System.debug('*******Data: ' + data);
           // System.assert(data.size() > 0, '');
            Test.stopTest();
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='CS_VPLSTPriceItemLookupTest:doDynamicLookupSearchTest';         
            ErrorHandlerException.sendException(e); 
        } finally {
           // Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
  }

}