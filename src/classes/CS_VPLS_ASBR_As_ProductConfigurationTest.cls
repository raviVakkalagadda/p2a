@isTest(SeeAllData = false)
public class CS_VPLS_ASBR_As_ProductConfigurationTest {
    //create Basket
    private static List<cscfga__Product_Basket__c> basketList = P2A_TestFactoryCls.getProductBasket(1);
    
    //create ProductDefinitions
    private static List<cscfga__Product_Definition__c> productDefList;
    
    //create Attribute definitions
    private static List<cscfga__Attribute_Definition__c> attDefList;
    
    //create Attributes
    private static List<cscfga__Attribute__c> attributes;
    
    //create blank list of Master IPVPN Service
    private static List <cscfga__Product_Configuration__c> masterServiceList;
    
    //create blank list of PCs for ports
    private static List<cscfga__Product_Configuration__c> portList;
    
    //create blank list of PCs for ASBRs
    private static List<cscfga__Product_Configuration__c> asbrList;
    
    private static List<CS_Provider__c> providers;
    
    private static void initData(){
            createProviders(2);
        
            productDefList = new List<cscfga__Product_Definition__c>{
                  new cscfga__Product_Definition__c(Name = 'VPLS Transparent', cscfga__Description__c = 'VPLS Transparent')
                , new cscfga__Product_Definition__c(Name = 'ASBR', cscfga__Description__c = 'ASBR')
                , new cscfga__Product_Definition__c(Name = 'Master VPLS Srevice', cscfga__Description__c = 'Master VPLS Srevice')
            };     
                
            insert productDefList;
        	System.debug('**** Product definitions: ' + productDefList);
      
            attDefList = new List<cscfga__Attribute_Definition__c>{
                new cscfga__Attribute_Definition__c(Name = 'Type Of NNI'
                                                   , cscfga__Product_Definition__c = productDefList[0].Id
                                                   , cscfga__Data_Type__c = 'String'
                                                   , cscfga__Type__c = 'Select List'),
                new cscfga__Attribute_Definition__c(Name = 'Offnet CS Provider Id'
                                                   , cscfga__Product_Definition__c = productDefList[0].Id
                                                   , cscfga__Data_Type__c = 'String'
                                                   , cscfga__Type__c = 'Calculation'),
                new cscfga__Attribute_Definition__c(Name = 'Master VPLS Configuration'
                                                   , cscfga__Product_Definition__c = productDefList[0].Id
                                                   , cscfga__Data_Type__c = 'String'
                                                   , cscfga__Type__c = 'Lookup'),
                new cscfga__Attribute_Definition__c(Name = 'ASBR Connection Type'
                                                   , cscfga__Product_Definition__c = productDefList[0].Id
                                                   , cscfga__Data_Type__c = 'String'
                                                   , cscfga__Type__c = 'Calculation')
            };
        
            insert attDefList;  
        	System.debug('**** Attribute definitions: ' + attDefList);
        
            masterServiceList = new List<cscfga__Product_Configuration__c>{
                new cscfga__Product_Configuration__c(Name = 'Master VPLS Service'
                                                    , cscfga__Product_Definition__c = productDefList[2].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id)
            };
                
            insert masterServiceList;
        	System.debug('**** Master Services: ' + masterServiceList);
        
        
        	asbrList =  new List<cscfga__Product_Configuration__c>{
                new cscfga__Product_Configuration__c(Name = 'ASBR 1'
                                                    , cscfga__Product_Definition__c = productDefList[1].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id
                                                    , ASBR_NNI_Type__c = 'Type A'
                                                    , Type_of_ASBR__c = 'Standard'),
                new cscfga__Product_Configuration__c(Name = 'ASBR 2'
                                                    , cscfga__Product_Definition__c = productDefList[1].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id
                                                    , ASBR_NNI_Type__c = 'Type A'
                                                    , Type_of_ASBR__c = 'Standard')
            };
                
            insert asbrList;
        	System.debug('**** ASBRs: ' + asbrList);
        
            portList = new List<cscfga__Product_Configuration__c>{
                new cscfga__Product_Configuration__c(Name = 'VPLS Transparent Port 1'
                                                    , cscfga__Product_Definition__c = productDefList[0].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id
                                                    , ASBR_Product_Configuration__c = asbrList[0].Id
                                                    , Master_IPVPN_Configuration__c = masterServiceList[0].Id
                                                    , ASBR_Connection_Type__c = 'VPLS'
                                                    , CS_Provider__c = providers[0].Id),
                new cscfga__Product_Configuration__c(Name = 'VPLS Transparent Port 2'
                                                    , cscfga__Product_Definition__c = productDefList[0].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id
                                                    , ASBR_Product_Configuration__c = asbrList[1].Id
                                                    , Master_IPVPN_Configuration__c = masterServiceList[0].Id
                                                    , ASBR_Connection_Type__c = 'VPLS'
                                                    , CS_Provider__c = providers[1].Id),
                new cscfga__Product_Configuration__c(Name = 'VPLS Transparent Port 3'
                                                    , cscfga__Product_Definition__c = productDefList[0].Id
                                                    , cscfga__Product_Basket__c = basketList[0].Id
                                                    , ASBR_Product_Configuration__c = ''
                                                    , Master_IPVPN_Configuration__c = masterServiceList[0].Id
                                                    , ASBR_Connection_Type__c = 'VPLS'
                                                    , CS_Provider__c = providers[1].Id)
            };
                
            insert portList;
        	System.debug('**** Ports: ' + portList);
        
        	attributes = new List<cscfga__Attribute__c>{
                new cscfga__Attribute__c(Name = 'Type Of NNI'
                                        , cscfga__Product_Configuration__c = portList[0].Id
                                        , cscfga__Value__c = 'Type A'
                                        , cscfga__Attribute_Definition__c = attDefList[0].Id),
                new cscfga__Attribute__c(Name = 'Offnet CS Provider Id'
                                        , cscfga__Product_Configuration__c = portList[0].Id
                                        , cscfga__Value__c = providers[0].Id
                                        , cscfga__Attribute_Definition__c = attDefList[1].Id),
                new cscfga__Attribute__c(Name = 'Master VPLS Configuration'
                                        , cscfga__Product_Configuration__c = portList[0].Id
                                        , cscfga__Value__c = masterServiceList[0].Id
                                        , cscfga__Attribute_Definition__c = attDefList[2].Id),
                new cscfga__Attribute__c(Name = 'ASBR Connection Type'
                                        , cscfga__Product_Configuration__c = portList[0].Id
                                        , cscfga__Value__c = 'VPLS'
                                        , cscfga__Attribute_Definition__c = attDefList[3].Id),
                new cscfga__Attribute__c(Name = 'Type Of NNI'
                                        , cscfga__Product_Configuration__c = portList[1].Id
                                        , cscfga__Value__c = 'Type A'
                                        , cscfga__Attribute_Definition__c = attDefList[0].Id),
                new cscfga__Attribute__c(Name = 'Offnet CS Provider Id'
                                        , cscfga__Product_Configuration__c = portList[1].Id
                                        , cscfga__Value__c = providers[1].Id
                                        , cscfga__Attribute_Definition__c = attDefList[1].Id),
                new cscfga__Attribute__c(Name = 'Master VPLS Configuration'
                                        , cscfga__Product_Configuration__c = portList[1].Id
                                        , cscfga__Value__c = masterServiceList[0].Id
                                        , cscfga__Attribute_Definition__c = attDefList[2].Id),
                new cscfga__Attribute__c(Name = 'ASBR Connection Type'
                                        , cscfga__Product_Configuration__c = portList[1].Id
                                        , cscfga__Value__c = 'VPLS'
                                        , cscfga__Attribute_Definition__c = attDefList[3].Id),
                new cscfga__Attribute__c(Name = 'Type Of NNI'
                                        , cscfga__Product_Configuration__c = portList[2].Id
                                        , cscfga__Value__c = 'Type A'
                                        , cscfga__Attribute_Definition__c = attDefList[0].Id),
                new cscfga__Attribute__c(Name = 'Offnet CS Provider Id'
                                        , cscfga__Product_Configuration__c = portList[2].Id
                                        , cscfga__Value__c = providers[1].Id
                                        , cscfga__Attribute_Definition__c = attDefList[1].Id),
                new cscfga__Attribute__c(Name = 'Master VPLS Configuration'
                                        , cscfga__Product_Configuration__c = portList[2].Id
                                        , cscfga__Value__c = masterServiceList[0].Id
                                        , cscfga__Attribute_Definition__c = attDefList[2].Id),
                new cscfga__Attribute__c(Name = 'ASBR Connection Type'
                                        , cscfga__Product_Configuration__c = portList[2].Id
                                        , cscfga__Value__c = 'VPLS'
                                        , cscfga__Attribute_Definition__c = attDefList[3].Id)
            };
                
            insert attributes;  
        	System.debug('**** Attributes: ' + attributes);
    }
    
    private static testMethod void doLookupSearchTest(){
		Exception ee = null;
        Map<String, String> searchFields = new Map<String, String>();
        //List<cscfga__Attribute__c> searchFieldAttributes;
        String productDefinitionId;
        Id[] excludeIds;
        Integer pageOffset, pageLimit;
        
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            //searchFieldAttributes = new List<cscfga__Attribute__c>([SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]);
            
            for(cscfga__Attribute__c attr : [SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]){
                searchFields.put(attr.Name, attr.cscfga__Value__c);
            }         
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('ConfigId', '');
            searchFields.put('searchValue','');
            
            system.debug('**** searchFields: ' + searchFields);         
            
            CS_VPLS_ASBR_As_ProductConfiguration csVplsAsbrProdConfig = new CS_VPLS_ASBR_As_ProductConfiguration();
            Object[] data = csVplsAsbrProdConfig.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            String reqAtts = csVplsAsbrProdConfig.getRequiredAttributes();
            
            system.debug('**** RETURN DATA****: ' + data);
            System.assert(data.size() > 0, '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }        
    }
       
    private static testMethod void doLookupSearchTestOffnetProvider(){
        Exception ee = null;
        Map<String, String> searchFields = new Map<String, String>();
        //List<cscfga__Attribute__c> searchFieldAttributes;
        String productDefinitionId;
        Id[] excludeIds;
        Integer pageOffset, pageLimit;
        
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            //searchFieldAttributes = new List<cscfga__Attribute__c>([SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]);
            
            for(cscfga__Attribute__c attr : [SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]){
                if(attr.Name != 'Offnet CS Provider Id'){
                    searchFields.put(attr.Name, attr.cscfga__Value__c);
                } else {
                    searchFields.put(attr.Name, '');
                }
                
            }         
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('ConfigId', '');
            searchFields.put('searchValue','');
            
            system.debug('**** searchFields: ' + searchFields);         
            
            CS_VPLS_ASBR_As_ProductConfiguration csVplsAsbrProdConfig = new CS_VPLS_ASBR_As_ProductConfiguration();
            Object[] data = csVplsAsbrProdConfig.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            //String reqAtts = csVplsAsbrProdConfig.getRequiredAttributes();
            
            system.debug('**** RETURN DATA doLookupSearchTestOffnetProvider()****: ' + data);
            System.assert(data.size() == 0, '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }     
    }
    
    private static testMethod void doLookupSearchTestConfigId(){
        Exception ee = null;
        Map<String, String> searchFields = new Map<String, String>();
        //List<cscfga__Attribute__c> searchFieldAttributes;
        String productDefinitionId;
        Id[] excludeIds;
        Integer pageOffset, pageLimit;
                
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            //searchFieldAttributes = new List<cscfga__Attribute__c>([SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]);
            
            for(cscfga__Attribute__c attr : [SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]){
                searchFields.put(attr.Name, attr.cscfga__Value__c);
            }         
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('ConfigId', portList[0].Id);
            searchFields.put('searchValue','');
            
            system.debug('**** searchFields: ' + searchFields);         
            
            CS_VPLS_ASBR_As_ProductConfiguration csVplsAsbrProdConfig = new CS_VPLS_ASBR_As_ProductConfiguration();
            Object[] data = csVplsAsbrProdConfig.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            //String reqAtts = csVplsAsbrProdConfig.getRequiredAttributes();
            
            system.debug('**** RETURN DATA doLookupSearchTestConfigId()****: ' + data);
            System.assert(data.size() > 0, '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }  
    }
       
    private static testMethod void doLookupSearchExcludeTest(){
        Exception ee = null;
        Map<String, String> searchFields = new Map<String, String>();
        //List<cscfga__Attribute__c> searchFieldAttributes;
        String productDefinitionId;
        Id[] excludeIds;
        Integer pageOffset, pageLimit;
        
        
        try{
            disableAll(UserInfo.getUserId());
            Test.startTest();
                             
            initData();
            
            //searchFieldAttributes = new List<cscfga__Attribute__c>([SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]);
            
            for(cscfga__Attribute__c attr : [SELECT Name, cscfga__Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c =: portList[2].Id]){
                searchFields.put(attr.Name, attr.cscfga__Value__c);
            }         
            searchFields.put('BasketId', basketList[0].Id);
            searchFields.put('ConfigId', '');
            searchFields.put('searchValue','');
            
            system.debug('**** searchFields: ' + searchFields);   
            
            //exclude Ids
            excludeIds = new List<Id>();
            for(cscfga__Product_Configuration__c pc : [select id from cscfga__Product_Configuration__c 
                                                       where cscfga__Product_Definition__c =: productDefList[1].Id
                                                       LIMIT 1]){
    			excludeIds.add(pc.id);
			}
            
            CS_VPLS_ASBR_As_ProductConfiguration csVplsAsbrProdConfig = new CS_VPLS_ASBR_As_ProductConfiguration();
            Object[] data = csVplsAsbrProdConfig.doLookupSearch(searchFields, productDefinitionId, excludeIds, pageOffset, pageLimit);
            
            system.debug('**** RETURN DATA doLookupSearchExcludeTest()****: ' + data);
            System.assert(data.size() == 1, '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }    
    }
    
    //create Offnet Providers
    private static void createProviders(Integer numOfRecords){
        providers = new List<CS_Provider__c>();
        
        if(numOfRecords > 0){
            for(Integer i = 0; i < numOfRecords; i++){
            
                if(Math.mod(i, 2) == 0){
                providers.add(new CS_Provider__c(Name = 'Test Provider ' + i
                                                  , NAS_Product__c = Boolean.valueOf('true')
                                                  , Offnet_Provider__c = Boolean.valueOf('true')
                                                  , Onnet__c = Boolean.valueOf('false')
                                                    )
                             );
                } else {
                providers.add(new CS_Provider__c(Name = 'Test Provider ' + i
                                                  , NAS_Product__c = Boolean.valueOf('false')
                                                  , Offnet_Provider__c = Boolean.valueOf('true')
                                                  , Onnet__c = Boolean.valueOf('false')
                                                    )
                             );
                }
            }
        insert providers;
        System.debug('**** created Providers: ' + providers);
        }
        //return providers;
    }

    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
}