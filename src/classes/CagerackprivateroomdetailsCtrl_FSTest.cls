@isTest
private class CagerackprivateroomdetailsCtrl_FSTest{
    /*
    private static List<cscfga__Product_Configuration__c> pclist;
    private static List<Product_Definition_Id__c> PdIdlist;
    private static List<cscfga__Product_Definition__c> Pdlist;
    private static List<Offer_Id__c>offIdlist;
    
    static testMethod void UnitTest() {
    list<RR_Cage_or_Rack__c> listrcrcage = new list<RR_Cage_or_Rack__c>();        
    
    try{
     offIdlist = new List<Offer_Id__c>{
                 //new Offer_Id__c(name ='ASBR_TypeA_Offer_Id',Offer_Id__c = 'ASBR_TypeA_Offer_Id'),
                 //new Offer_Id__c(name ='IPVPN_Port_withoutLL_Offer_Id',Offer_Id__c = 'IPVPN_Port_withoutLL_Offer_Id'),
                 new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = 'a1kO00000023TjC'),
                 new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = 'a1kO0000001zhbX'),
                 new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = 'a1kO00000023UPq')
                 //new Offer_Id__c(name ='SMA_Gateway_Offer_Id',Offer_Id__c = 'SMA_Gateway_Offer_Id'),
                 //new Offer_Id__c(name ='VLAN_Group_Offer_Id',Offer_Id__c = 'VLAN_Group_Offer_Id'),
                 //new Offer_Id__c(name ='VPLS_Transparent_withoutLL_Offer_Id',Offer_Id__c = 'VPLS_Transparent_withoutLL_Offer_Id'),
                 //new Offer_Id__c(name ='VPLS_VLAN_withoutLL_Offer_Id',Offer_Id__c = 'VPLS_VLAN_withoutLL_Offer_Id')
       };
     }catch (Exception e) {
       }
     
     insert offIdlist;
       
     Pdlist = new List<cscfga__Product_Definition__c>{
              new cscfga__Product_Definition__c(name  = 'Master IPVPN Service',cscfga__Description__c = 'Test master IPVPN'),
              new cscfga__Product_Definition__c(name  = 'Master VPLS Service', cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VLANGroup_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'IPVPN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'SMA_Gateway_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_Transparent_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'VPLS_VLAN_Port_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_IPVPN_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS'),
              new cscfga__Product_Definition__c(name  = 'Master_VPLS_Service_Definition_Id',cscfga__Description__c = 'Test master VPLS')
            };
     insert Pdlist;      
     System.assertEquals('Master IPVPN Service',Pdlist[0].Name);

          
     PdIdlist = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'Master IPVPN Service',Product_Id__c = Pdlist[0].Id),
                new Product_Definition_Id__c(name = 'Master VPLS Service',Product_Id__c = Pdlist[1].id),
                new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = Pdlist[2].id),
                new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = Pdlist[3].id),
                new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = Pdlist[4].id),
                new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = Pdlist[5].id),
                new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = Pdlist[6].id),
                new Product_Definition_Id__c(name = 'Master_IPVPN_Service_Definition_Id',Product_Id__c = Pdlist[7].id),
                new Product_Definition_Id__c(name = 'Master_VPLS_Service_Definition_Id',Product_Id__c = Pdlist[8].id)
     };
     insert PdIdlist;
     System.assertEquals('Master VPLS Service',PdIdlist[1].Name); 
       
     List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(9);  
         
     pclist= new List<cscfga__Product_Configuration__c>{
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[0].id,cscfga__Product_Basket__c = pblist[0].Id,cscfga__Product_Family__c = 'Test family'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[1].id,cscfga__Product_Basket__c = pblist[1].Id,cscfga__Product_Family__c = 'Test family1'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[2].id,cscfga__Product_Basket__c = pblist[2].Id,cscfga__Product_Family__c = 'Test family2'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[3].id,cscfga__Product_Basket__c = pblist[3].Id,cscfga__Product_Family__c = 'Test family3'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[4].id,cscfga__Product_Basket__c = pblist[4].Id,cscfga__Product_Family__c = 'Test family4'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[5].id,cscfga__Product_Basket__c = pblist[5].Id,cscfga__Product_Family__c = 'Test family5'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[6].id,cscfga__Product_Basket__c = pblist[6].Id,cscfga__Product_Family__c = 'Test family6'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[7].id,cscfga__Product_Basket__c = pblist[7].Id,cscfga__Product_Family__c = 'Test family7'),
             new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = Pdlist[8].id,cscfga__Product_Basket__c = pblist[8].Id,cscfga__Product_Family__c = 'Test family8')
            };
     insert pclist;      
     System.assertEquals('Test family2',pclist[2].cscfga__Product_Family__c); 
        
     case c = new case();
      c.Is_Feasibility_Request__c = true;
      c.Product_Configuration__c = pclist[0].id;
      c.status = 'Approved';
      c.Number_of_Child_Case__c = 2;
      c.Number_of_Child_Case_Closed__c = 1;
      c.Reason_for_selecting_one_supplier__c = 'SingleSource';
      c.Is_Resource_Reservation__c = true;
      insert c;
    System.assertEquals(c.Reason_for_selecting_one_supplier__c, 'SingleSource');
    
     RR_Cage_or_Rack__c rcr = new RR_Cage_or_Rack__c();
      rcr.Product_Configuration__c = pclist[0].id;
      rcr.Cage_or_Room_ID__c = 'Cageroom';
      rcr.Type__c = 'New';
      rcr.Width_per_rack_mm__c  = 'Rackmm';
      rcr.Rack_IDs_allocated__c = 'Allocated';
      rcr.Power_per_rack_KVA__c = 'Powerrack';
      rcr.No_of_ceeforms_commando_sockets_per_rack__c = 'ceeforms';
      insert rcr;
    System.assertEquals(rcr.Cage_or_Room_ID__c, 'Cageroom');
    
     RR_Cage_or_Rack__c upadtercr = [select id,Product_Configuration__c,Type__c from RR_Cage_or_Rack__c where id=: rcr.id];
     listrcrcage.add(upadtercr);
     update listrcrcage;
      
     PageReference pageRef = Page.cagerackprivateroomdetails; // Adding VF page Name here
     pageRef.getParameters().put('id', String.valueOf(rcr.id));//for page reference
     Test.setCurrentPage(pageRef);

     //Put Id into the current page Parameters
     ApexPages.currentPage().getParameters().put('id',c.Id);
     ApexPages.StandardController sc = new ApexPages.StandardController(c);
     cagerackprivateroomdetailsCtrl_FS   Cc = new cagerackprivateroomdetailsCtrl_FS(sc);
     Cc.updateItems();    
     boolean b = true;
     String id = ApexPages.currentPage().getParameters().get('id');
     System.assert(b,id==null); 
    }
     */   
 }