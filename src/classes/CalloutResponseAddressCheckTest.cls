@isTest(SeeAllData = false)
private class CalloutResponseAddressCheckTest {
    private static List<cscfga__Product_Category__c> productCategoryList;
    private static Map<String, csbb.CalloutResponse> mapCR;
    private static csbb.ProductCategory productCategory;
    private static csbb.CalloutProduct.ProductResponse productResponse;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> resultMap;
    private static String categoryIndicator;
    private static csbb.Result result;
    
    private static Map<String, String> attMap;
    private static Map<String, String> responseFields;
    
    private static void initTestData(){
        
        productCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Product Category 1')
        };
        
        insert productCategoryList;
        system.assertEquals(true,productCategoryList!=null); 
        mapCR = new Map<String, csbb.CalloutResponse>();
        mapCR.put('AddressCheck', new csbb.CalloutResponse());
        
        productCategory = new csbb.ProductCategory(productCategoryList[0].Id);
        
        productResponse = new csbb.CalloutProduct.ProductResponse();
        productResponse.fields = new Map<String, String>();
        
        inputMap = new Map<String, Object>();
        resultMap = new Map<String, Object>();
        
        categoryIndicator = '';
        result = new csbb.Result();
        
        attMap = new Map<String, String>();
        responseFields = new Map<String, String>();
    }    
    
  private static testMethod void test() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            CalloutResponseAddressCheck calloutResponseAddressCheck = new CalloutResponseAddressCheck(mapCR, productCategory, productResponse);
            CalloutResponseAddressCheck calloutResponseAddressCheck1 = new CalloutResponseAddressCheck();

            System.debug('***** CR Primary: ' + calloutResponseAddressCheck.crPrimary);
            System.debug('***** Product Response fields: ' + calloutResponseAddressCheck.productResponse.fields);

            resultMap = calloutResponseAddressCheck.processResponseRaw(inputMap);
            resultMap = calloutResponseAddressCheck.getDynamicRequestParameters(inputMap);
            result = calloutResponseAddressCheck.canOffer(attMap, responseFields, productResponse);
            system.assertEquals(true,calloutResponseAddressCheck!=null); 
            system.assertEquals(true,result!=null); 

            
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CalloutResponseAddressCheckTest :test';         
        ErrorHandlerException.sendException(e); 

            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }

}