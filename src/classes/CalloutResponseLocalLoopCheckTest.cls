@isTest(SeeAllData = false)
private class CalloutResponseLocalLoopCheckTest {

    private static List<cscfga__Product_Category__c> productCategoryList;
    private static Map<String, csbb.CalloutResponse> mapCR;
    private static csbb.ProductCategory productCategory;
    private static csbb.CalloutProduct.ProductResponse productResponse;
    private static Map<String, Object> inputMap;
    private static Map<String, Object> resultMap;
    private static String categoryIndicator;
    private static csbb.Result result;
    
    private static Map<String, String> attMap;
    private static Map<String, String> responseFields;

    private static void initTestData(){
        
        productCategoryList = new List<cscfga__Product_Category__c>{
            new cscfga__Product_Category__c(Name = 'Product Category 1')
        };
        
        insert productCategoryList;
          list<cscfga__Product_Category__c> prod = [select id,Name from cscfga__Product_Category__c where Name = 'Product Category 1'];
        system.assertEquals(productCategoryList[0].Name , prod[0].Name);
        System.assert(productCategoryList!=null); 
        
        mapCR = new Map<String, csbb.CalloutResponse>();
        mapCR.put('LocalLoopCheck', new csbb.CalloutResponse());
        
        productCategory = new csbb.ProductCategory(productCategoryList[0].Id);
        
        productResponse = new csbb.CalloutProduct.ProductResponse();
        productResponse.fields = new Map<String, String>();
        
        inputMap = new Map<String, Object>();
        resultMap = new Map<String, Object>();
        
        categoryIndicator = '';
        result = new csbb.Result();
        
        attMap = new Map<String, String>();
        responseFields = new Map<String, String>();
    }

    private static testMethod void test() {
    Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
             
            initTestData();
            
            CalloutResponseLocalLoopCheck calloutResponseLocalLoopCheck = new CalloutResponseLocalLoopCheck(mapCR, productCategory, productResponse);
            CalloutResponseLocalLoopCheck calloutResponseLocalLoopCheck1 = new CalloutResponseLocalLoopCheck();

            System.debug('***** CR Primary: ' + calloutResponseLocalLoopCheck.crPrimary);
            System.debug('***** Product Response fields: ' + calloutResponseLocalLoopCheck.productResponse.fields);

            resultMap = calloutResponseLocalLoopCheck.processResponseRaw(inputMap);
            resultMap = calloutResponseLocalLoopCheck.getDynamicRequestParameters(inputMap);
            result = calloutResponseLocalLoopCheck.canOffer(attMap, responseFields, productResponse);
             system.assertEquals(true,calloutResponseLocalLoopCheck!=null); 
            
        } catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CalloutResponseLocalLoopCheckTest :test';         
                ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}