@isTest(SeeAllData = true)
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
public class CampaignTriggerTest
//End 
{
    static testmethod void TestCampaignMethod1()
    {
        Test.startTest();
        Campaign campObj = new Campaign(Name='Test',Type='Advertisement', CurrencyIsoCode ='USD');
        insert campObj;
        list<Campaign> camp = [select id,Name from Campaign where Name = 'Test'];
        system.assertEquals(campObj.Name, camp[0].Name);
        System.assert(campObj!=null); 
        Test.stopTest();
    }
}