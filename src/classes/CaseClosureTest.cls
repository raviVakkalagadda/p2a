@isTest(seealldata = false)
public class CaseClosureTest{

     @isTest
  static  void testgotoDetailPage1() {
         
         P2A_TestFactoryCls.sampletestdata();
         
         List<Account> AccList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         List<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
         List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
         List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
         List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
         List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(5,pblist,Pdlist1,pbundlelist,Offerlists); 
         proconfig[0].Product_Code__c = 'IPM';
         proconfig[0].csordtelcoa__Replaced_Service__c = Null;            

         upsert  proconfig;  
        system.assertEquals(true,proconfig!=null); 
         
         id r1=RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Local Loop Record Type');
        id r2=RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Default');
        id r3=RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'FeasibilityStudyType');
        id r4=RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Parent Product Supplier Quote');
        id r5=RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'GCPE Record Type');
        id r6 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'GCPE Renegotiation');
        id r7 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Approve');
        id r8 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'FSProductConfiguration Type');

         
         List<case> updatecase = new List<case>();
         
         case c = new case();
         
            c.accountid = accList[0].id;            
            c.Product_Configuration__c = proconfig[0].id;
            c.Product_Basket__c = pblist[0].id;
            c.status = 'Closed';
            c.Type = 'Enrichment';
            c.Enrichment_Status__c = 'completed';
            updatecase.add(c);   
                                             
            case c1 = new case();
         
            c1.accountid = accList[0].id;            
            c1.Product_Configuration__c = proconfig[0].id;
            c1.Product_Basket__c = pblist[0].id;
            c1.status = 'Closed';
            c1.Type = 'Enrichment';
            c1.Enrichment_Status__c = 'completed';
            c1.RecordTypeId = r1;
            updatecase.add(c1);   
            
            case c2 = new case();
         
            c2.accountid = accList[0].id;            
            c2.Product_Configuration__c = proconfig[0].id;
            c2.Product_Basket__c = pblist[0].id;
            c2.status = 'Closed';
            c2.Type = 'Enrichment';
            c2.Enrichment_Status__c = 'completed';
            c2.RecordTypeId = r2;
            updatecase.add(c2);   
            
            case c3 = new case();
         
            c3.accountid = accList[0].id;            
            c3.Product_Configuration__c = proconfig[0].id;
            c3.Product_Basket__c = pblist[0].id;
            c3.status = 'Closed';
            c3.Type = 'Enrichment';
            c3.Enrichment_Status__c = 'completed';
            c3.RecordTypeId = r3;
            updatecase.add(c3);   
            
            case c4 = new case();
         
            c4.accountid = accList[0].id;            
            c4.Product_Configuration__c = proconfig[0].id;
            c4.Product_Basket__c = pblist[0].id;
            c4.status = 'Closed';
            c4.Type = 'Enrichment';
            c4.Enrichment_Status__c = 'completed';
            c4.RecordTypeId = r4;
            updatecase.add(c4);   
            
            case c5 = new case();
         
            c5.accountid = accList[0].id;            
            c5.Product_Configuration__c = proconfig[0].id;
            c5.Product_Basket__c = pblist[0].id;
            c5.status = 'Closed';
            c5.Type = 'Enrichment';
            c5.Enrichment_Status__c = 'completed';
            c5.RecordTypeId = r5;
            updatecase.add(c5);   
            
            case c6 = new case();
         
            c6.accountid = accList[0].id;            
            c6.Product_Configuration__c = proconfig[0].id;
            c6.Product_Basket__c = pblist[0].id;
            c6.status = 'Closed';
            c6.Type = 'Enrichment';
            c6.Enrichment_Status__c = 'completed';
            c6.RecordTypeId = r6;
            updatecase.add(c6);   
            
            case c7 = new case();
         
            c7.accountid = accList[0].id;            
            c7.Product_Configuration__c = proconfig[0].id;
            c7.Product_Basket__c = pblist[0].id;
            c7.status = 'Closed';
            c7.Type = 'Enrichment';
            c7.Enrichment_Status__c = 'completed';
            c7.RecordTypeId = r7;
            updatecase.add(c7);
            
            case c8 = new case();
            
            c8.accountid = accList[0].id;            
            c8.Product_Configuration__c = proconfig[0].id;
            c8.Product_Basket__c = pblist[0].id;
            c8.status = 'Closed';
            c8.Type = 'Enrichment';
            c8.Enrichment_Status__c = 'completed';
            c8.RecordTypeId = r8;
            updatecase.add(c8);
            
            
            
            for(integer i=9; i <35 ; i++){
           case c9 = new case();
           if(i==9){
        id r9 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Reject');
        c9.RecordTypeId = r9;
        }
            if(i==10){
        id r10 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Approve/reject');
        c9.RecordTypeId = r10;
        }
             if(i==11){
        id r11 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Bid Management Type');
        c9.RecordTypeId = r11;
        }
              if(i==12){
        id r12 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Jeopardy');
        c9.RecordTypeId = r12;
        }
         if(i==13){
        id r13 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Connectivity');
         c9.RecordTypeId = r13;
         }
         if(i==14){
        id r14 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Cross Connect');
        c9.RecordTypeId = r14;
        }     
             if(i==15){
        id r15 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Cage/Private Room');
          c9.RecordTypeId = r15;
          }    
              if(i==16){
        id r16 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RR Cage or Rack RType');
          c9.RecordTypeId = r16;
          }
          if(i==17){
        id r17 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RR Connectivity RType');
         c9.RecordTypeId = r17;
         }
         if(i==18){
        id r18 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RR Cross Connect RType');
         c9.RecordTypeId = r18;
         }
         if(i==19){
        id r19 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Design, Implement & Test');
         c9.RecordTypeId = r19;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==20){
        id r20 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'BillingStageGateRequest');
        c9.RecordTypeId = r20;
        c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
        }
         if(i==21){
        id r21 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Supplier GIE');
          c9.RecordTypeId = r21;
          c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
          }
          if(i==22){
        id r22 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RR Internet Product');
           c9.RecordTypeId = r22;
           c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
           }
           if(i==23){
        id r23 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'IPVPN & VPLS Product');
         c9.RecordTypeId = r23;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==24){
        id r24 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RR IPVPN & VPLS Product');
         c9.RecordTypeId = r24;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==25){
        id r25 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'ETC Commercial');
         c9.RecordTypeId = r25;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==26){
        id r26 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Supplier Quoting VPN Services Renegotiation');
         c9.RecordTypeId = r26;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i== 27){
        id r27 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Local Loop Renegotiation');
         c9.RecordTypeId = r27;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==28){
        id r28 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'GCPE Renegotiation');
         c9.RecordTypeId = r28;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==29){
        id r29 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Cross Connect Renegotiation');
         c9.RecordTypeId = r29;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==30){
        id r30 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'PTP Supplier MES or OSS Renegotiation');
         c9.RecordTypeId = r30;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==31){
        id r31 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'PTP Supplier PTP Renegotiation');
         c9.RecordTypeId = r31;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==32){
        id r32 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Escalate');
         c9.RecordTypeId = r32;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==33){
        id r33 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Supplier GIE Close');
         c9.RecordTypeId = r33;
         c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
         }
         if(i==34){
        id r34 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Enrichment');
          c9.RecordTypeId = r34;
          c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
            c9.Internet_Product__c = false;          
           } 
         if(i==35){
        id r35 = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Supplier GIE Renegotiation');
          c9.RecordTypeId = r35;
          c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';                      
           } 
            c9.accountid = accList[0].id;            
            c9.Product_Configuration__c = proconfig[0].id;
            c9.Product_Basket__c = pblist[0].id;
            c9.status = 'Closed';
            c9.Type = 'Enrichment';
            c9.Enrichment_Status__c = 'completed';
            
            updatecase.add(c9);
            
            }
            insert updatecase; 
            
            test.starttest();  
  
        ApexPages.StandardController sc = new ApexPages.standardController(updatecase[0]);
        CaseClosure controller = new CaseClosure(sc);
        controller.gotoDetailPage();
        controller.returnToCase();
        controller.getRecordTypeId(updatecase[0].type);             
        system.assertEquals(true,controller!=null); 
        ApexPages.StandardController sc1 = new ApexPages.standardController(updatecase[1]);
        CaseClosure controller1 = new CaseClosure(sc1);
        controller1.gotoDetailPage();
        system.assertEquals(true,controller1!=null); 
        ApexPages.StandardController sc2 = new ApexPages.standardController(updatecase[2]);
        CaseClosure controller2 = new CaseClosure(sc2);
        controller2.gotoDetailPage();
        system.assertEquals(true,controller2!=null);
        ApexPages.StandardController sc3 = new ApexPages.standardController(updatecase[3]);
        CaseClosure controller3 = new CaseClosure(sc3);
        controller3.gotoDetailPage();
        system.assertEquals(true,controller3!=null);
        ApexPages.StandardController sc4 = new ApexPages.standardController(updatecase[4]);
        CaseClosure controller4 = new CaseClosure(sc4);
        controller4.gotoDetailPage();
        system.assertEquals(true,controller4!=null);
        ApexPages.StandardController sc5 = new ApexPages.standardController(updatecase[5]);
        CaseClosure controller5 = new CaseClosure(sc5);
        controller5.gotoDetailPage();
        system.assertEquals(true,controller5!=null);
        ApexPages.StandardController sc6 = new ApexPages.standardController(updatecase[6]);
        CaseClosure controller6 = new CaseClosure(sc6);
        controller6.gotoDetailPage();
         system.assertEquals(true,controller6!=null);
        ApexPages.StandardController sc7 = new ApexPages.standardController(updatecase[7]);
        CaseClosure controller7 = new CaseClosure(sc7);
        controller7.gotoDetailPage();
        system.assertEquals(true,controller7!=null);
        ApexPages.StandardController sc8 = new ApexPages.standardController(updatecase[8]);
        CaseClosure controller8 = new CaseClosure(sc8);
        controller8.gotoDetailPage();
       system.assertEquals(true,controller8!=null);
        for(integer i=9; i <35 ; i++){
        
         ApexPages.StandardController sc9 = new ApexPages.standardController(updatecase[i]);
        CaseClosure controller9 = new CaseClosure(sc9);
        controller9.gotoDetailPage();
        controller9.updateBasketStage();
        system.assertEquals(true,controller9!=null);
        }               
           
         test.stopTest();           
          } 
         
}