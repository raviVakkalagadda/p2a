@isTest(SeeAllData = false)
public class CaseController_Test{
public static List<Supplier__c> supplierlist;
   public static ApexPages.StandardController sc;
    private static void initTestData(){ 
         try{
            supplierlist = P2A_TestFactoryCls.getsuppliers(1);
             system.assertEquals(true,supplierlist!=null);  
            sc = new ApexPages.StandardController(supplierlist[0]);           
        }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CaseController_Test :initTestData';         
        ErrorHandlerException.sendException(e);
            }       
        
    }
    private static testMethod void multiSelectComponentController()
    {
            initTestData();
            Test.startTest();
            CaseController cc = new CaseController(sc);
            //cc.CaseController();
            Test.stopTest();
             system.assertEquals(true,cc!=null);    
            
        }
}