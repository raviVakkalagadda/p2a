public with sharing class CaseExtension {

    private final Case theCase;
    private ApexPages.StandardController stdController;
    private List<String> additionalFields = new List<String>{'Type','Approval_Status_Reason__c', 'Opportunity_Name__c'};

    public CaseExtension(ApexPages.StandardController stdController) {
    if(!Test.isRunningTest()){
        stdController.addFields(additionalFields);
        }
        this.theCase = (Case)stdController.getRecord();
        this.stdController = stdController;
    }

    public PageReference reject(){
        if(theCase.Type == 'Enrichment'){
            if(theCase.Type == 'Enrichment'){
                theCase.Enrichment_Status__c = 'Rejected';

                OpportunityManager oppManager = new OpportunityManager();
                oppManager.updateStage(theCase.Opportunity_Name__c, 'Open', 'Closed Sales', 'Prove & Close');
            }
        }
        else{
            if(theCase.Approval_Status_Reason__c == ''){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please select rejection reason'));
            }
        }    
            
        theCase.Status = 'Rejected';
        theCase.Approved_Rejected_By__c = userinfo.getUserId();
        theCase.Approval_Rejection_Date__c = Date.today();

        update theCase;

        return stdController.view();
    }

    public PageReference returnToCase(){
        return stdController.view().setRedirect(true);
    }
}