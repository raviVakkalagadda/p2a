global with sharing class CityAvailabilityCheckInternet {
  
  webservice static String doWork (String city, String id, String country, String connectivity) {

    system.debug('******** CityAvailabilityCheck ****** '+ city +' -- '+id+' -- '+country);
    Map<String, String> retMap = new Map<String, String>{'city' => city, 'id' => id,'country' => country, 'connectivity' => connectivity}; 
    return JSON.serialize(retMap);
  }
}