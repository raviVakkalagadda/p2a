@isTest
private class CityAvailabilityCheckTest {

	private static testMethod void test() {
        Exception ee = null;
        
        try{
            String city = 'Zagreb';
            String cityId = '123456789';
            String country = 'Croatia';
            String connectivity = 'Onnet';
            
            Test.startTest();
            
            String result = CityAvailabilityCheck.doWork(city, cityId, country, connectivity);
            System.assert(result.contains(city), 'Result does not contain ' + city);

            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            if(ee != null){
                throw ee;
            }
        }    
	}

}