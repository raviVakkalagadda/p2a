/**
 * @name CleanOrchestrationsteps 
 * @description framework class for batch processes
 * @revision
 * Revathi Veesam 31-03-2016 Created class
 */
 public class CleanOrchestrationsteps implements Database.Batchable<sObject>, Database.Stateful 
 {
    public CleanOrchestrationsteps() {
       
    }
     
    public Database.QueryLocator start(Database.BatchableContext ctx) {
        return
           Database.getQueryLocator([select id 
                from 
                    CSPOFA__Orchestration_Step__c
                where lastmodifieddate < LAST_N_DAYS:2]);
    }
    
   public void execute(Database.BatchableContext ctx, List<CSPOFA__Orchestration_Step__c> scope) {
        delete scope;
    }
    
    public void finish(Database.BatchableContext ctx) {
        system.debug('CleanUnsyncedAttributesBatch- finished');
    }
 }