@isTest
private class CleanProductConfigScheduledTest{
    private static cscfga__Product_Basket__c basket = null;
    static testmethod void test() {
        Test.startTest();
        
        CleanProductConfigScheduled delBatch = new CleanProductConfigScheduled();
        String schtime = '0 0 23 * * ?';              
        system.schedule('Test Territory Check', schtime, delBatch);
        system.assertEquals(true,delBatch!=null); 
        Test.stopTest();
    }
}