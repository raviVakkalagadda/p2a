@isTest (SeeAllData = false)
private class CleanUnmodifiedDataBatchTest{
   public static List<Opportunity> oppList;
   public static List<Account> Acclist;
    public static List<CSPOFA__Orchestration_Process__c> OrchestrationProcesslists;
     public static List<CSPOFA__Orchestration_Step__c> OrchestrationSteplist;
         public static List<CSPOFA__Orchestration_Step_Dependency__c> Orchestration_Step_Dependency_list;
          public static List<cscfga__Product_Basket__c> ProductBasketlist;
           public static List<cscfga__Product_Configuration__c> ProductConfiglist;
              public static List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists;

    static testmethod void test() {
    
    /*  ProductConfiglist = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(name = 'product configuration')
        };
        insert ProductConfiglist;
        
         ProductBasketlist = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(name = 'product basket')
        };
        insert ProductBasketlist;
        
         Orchestration_Step_Dependency_list = new List<CSPOFA__Orchestration_Step_Dependency__c>{
            new CSPOFA__Orchestration_Step_Dependency__c(name = 'dependency')
        };
        insert Orchestration_Step_Dependency_list;
        
         OrchestrationSteplist = new List<CSPOFA__Orchestration_Step__c>{
            new CSPOFA__Orchestration_Step__c(CSPOFA__Orchestration_Process__c = OrchestrationProcesslists[0].id)
        };
        insert OrchestrationSteplist;
        
         ProcessTemplatesLists = new List<CSPOFA__Orchestration_Process_Template__c>{
            new CSPOFA__Orchestration_Process_Template__c(name = 'template')
        };
        insert ProcessTemplatesLists;
        
         OrchestrationProcesslists = new List<CSPOFA__Orchestration_Process__c>{
            new CSPOFA__Orchestration_Process__c(CSPOFA__Orchestration_Process_Template__c=ProcessTemplatesLists[0].id)
        };
        insert OrchestrationProcesslists;*/
        
         Acclist = new List<Account>{
            new Account(
             name                          = 'Test Acc',
                BillingCountry                = 'GB',
                Activated__c                  = True,
                Account_ID__c                 = '3333',
                Account_Status__c             = 'Active',
                Customer_Legal_Entity_Name__c = 'Test'
            )
        };
        insert Acclist;
        list<Account> acc = [select id,name from Account where name = 'Test Acc'];
        system.assertEquals(Acclist[0].name , acc[0].name);
        System.assert(Acclist!=null); 
        
        
         oppList = new List<Opportunity>{
            new Opportunity(            
                AccountId     = Acclist[0].id,
                name          = 'Test Opp',
                stagename     = 'Identify & Define',
                CloseDate     = system.today() + 10,
                Pre_Contract_Provisioning_Required__c = 'No')
        };
        insert oppList;
        list<Opportunity> opp = [select id,name from Opportunity where name = 'Test Opp'];
        system.assertEquals(oppList[0].name , opp[0].name);
        System.assert(oppList!=null); 
   
        
        
      /* Test.startTest();
       CleanUnmodifiedDataBatch clean = new CleanUnmodifiedDataBatch();
       Database.executeBatch(clean);
       Test.stopTest();*/

     Exception ee = null;
 try{
 Test.startTest();

  CleanUnmodifiedDataBatch clean = new CleanUnmodifiedDataBatch();
          clean.execute(null,oppList);
          Database.executeBatch(clean);
          system.assertEquals(true,clean!=null); 
     } catch(Exception e){
         ErrorHandlerException.ExecutingClassName='CleanUnmodifiedDataBatchTest :test';         
         ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            Test.stopTest();
            if(ee != null){
                throw ee;
            }
        } 
    }
}