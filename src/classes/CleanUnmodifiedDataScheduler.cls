/**
 * @name CleanUnmodifiedDataScheduler 
 * @description scheduler class for 'CleanUnmodifiedDataBatch' batch process
 * @revision
 * Boris Bjelan 25-03-2016 Created class
 */
global class CleanUnmodifiedDataScheduler implements Schedulable {
    
    public static String CRON_EXP = '0 36 * * * ?';
    
    global void execute(SchedulableContext ctx) {
        CleanUnmodifiedDataBatch bc = new CleanUnmodifiedDataBatch();  
        Database.executeBatch(bc, 5);
    }
    
    global static String scheduleIt() {
        CleanUnmodifiedDataScheduler scheduler = new CleanUnmodifiedDataScheduler();
        return System.schedule('CleanUnmodifiedDataScheduler Job', CRON_EXP, scheduler);
    }
}