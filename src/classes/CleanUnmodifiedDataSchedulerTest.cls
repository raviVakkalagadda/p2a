@isTest
private class CleanUnmodifiedDataSchedulerTest{
    static testmethod void test() {
        Test.startTest();
        
        CleanUnmodifiedDataScheduler delBatch = new CleanUnmodifiedDataScheduler();
        String schtime = '0 16 * * * ?';              
        system.schedule('CleanUnmodifiedDataScheduler Job', schtime, delBatch);
        system.assertEquals(true,delBatch!=null);
        try{
     
        CleanUnmodifiedDataScheduler.scheduleIt();
        
        }catch(Exception e){
        ErrorHandlerException.ExecutingClassName='CleanUnmodifiedDataSchedulerTest :test';         
        ErrorHandlerException.sendException(e); 
           system.debug('exception'+e);
        }
       
        Test.stopTest();
    }
    
    //return System.schedule('CleanUnmodifiedDataScheduler Job', schtime, delBatch);
}