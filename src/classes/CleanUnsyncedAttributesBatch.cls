/**
 * @name CleanUnsyncedAttributesBatch 
 * @description framework class for batch processes
 * @revision
 * Boris Bjelan 08-03-2016 Created class
 */
public class CleanUnsyncedAttributesBatch implements Database.Batchable<sObject>, Database.Stateful {
     
    public CleanUnsyncedAttributesBatch() {
    }
     
    public Database.QueryLocator start(Database.BatchableContext ctx) {
        return 
            Database.getQueryLocator([select Id
                    from 
                        cscfga__Attribute__c 
                    where     
                        cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csordtelcoa__Synchronised_with_Opportunity__c = false and
                        cscfga__Product_Configuration__r.cscfga__Product_Basket__r.csbb__Synchronised_With_Opportunity__c = false and
                        cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__r.isClosed = true]);
    }
    
    public void execute(Database.BatchableContext ctx, List<cscfga__Attribute__c> scope) {
        delete scope;
    }
    
    public void finish(Database.BatchableContext ctx) {
        system.debug('CleanUnsyncedAttributesBatch- finished');
    }
 }