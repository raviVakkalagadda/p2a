@isTest
private class CleanUnsyncedAttributesBatchTest
{
    public static List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
    public static List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
    public static List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
    public static List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    public static List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    public static List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    
    static testmethod void unittest() 
    {
        P2A_TestFactoryCls.sampletestdata();
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodbasktlist, ProductDeflist, Pbundle, Offerlists);
        List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Attribute_Definition__c> AttDef = P2A_TestFactoryCls.getAttributesdef(2, proconfigs, ProductDeflist, ConScr, ScrSec);
        List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(2, proconfigs, AttDef);
        
        String query = 'SELECT Id,Name FROM  cscfga__Attribute__c';              
        
        Test.startTest();
        CleanUnsyncedAttributesBatch cleanorch = new CleanUnsyncedAttributesBatch(); 
        cleanorch.execute(null,Attrblist);
        Database.executeBatch(cleanorch);
        Test.stopTest();
        
        List<cscfga__Attribute__c> delAttrblist = [select id from cscfga__Attribute__c where id IN :Attrblist limit 2];
        delete delAttrblist;
        Integer i = [SELECT COUNT() FROM CSPOFA__Orchestration_Step__c];
        System.assertEquals(i, 0);
    }
}