/**
 * @name CleanUnsyncedAttributesScheduler  
 * @description scheduler class for 'CleanUnsyncedAttributesBatch' batch process
 * @revision
 * Boris Bjelan 08-03-2016 Created class
 */
global class CleanUnsyncedAttributesScheduler implements Schedulable {
    global void execute(SchedulableContext ctx) {
        CleanUnsyncedAttributesBatch bc = new CleanUnsyncedAttributesBatch();  
        Database.executeBatch(bc, 200);
    }
}