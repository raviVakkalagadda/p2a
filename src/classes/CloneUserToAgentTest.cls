@isTest
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
private class CloneUserToAgentTest
//End
{

     static testMethod void myUnitTest() {
     
         User u = getUser();
         insert u;    
         User u1 = [Select Id,FirstName,IsActive from User where Id =: u.id ];
         u1.IsActive = false;
         u1.FirstName = 'Test1';
         update u1;
         system.assert(u1!=null);
         List<user> u2 = [Select id,FirstName from user where FirstName = 'Test1'];
         system.assertequals(u1.FirstName,u2[0].FirstName);
      }
     
     private static User getUser()
    {
       
       User userObj1 = new User();
       userObj1.FirstName = 'Test';
       userObj1.LastName = 'SFDC';
       userObj1.IsActive  = true;
       userObj1.Username = 'Test.SFDC@team.telstra.com.evolution';
       userObj1.Email = 'Test.SFDC@team.telstra.com';
       userObj1.Region__c = 'US';
       userObj1.TimeZoneSidKey = 'Australia/Sydney';
       userObj1.LocaleSidKey = 'en_AU';
       userObj1.EmailEncodingKey = 'ISO-8859-1';
       userObj1.LanguageLocaleKey = 'en_US';
       userObj1.Alias = 'TSFDC';
       userObj1.EmployeeNumber = '2313';
       Profile p = [select id from Profile where name = 'TI Sales Admin'];
       
       userObj1.ProfileId = p.id;
       system.assert(userObj1!=null);
              
       return userObj1;
       
    }


}