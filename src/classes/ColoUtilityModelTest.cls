@isTest(SeeAllData = false)
public class ColoUtilityModelTest {

private static Map<String, String> searchFields = new Map<String, String>();
private static String productDefinitionId;
            //List<cscfga__Attribute__c> ReturnList = new List<cscfga__Attribute__c>();
            //List<cscfga__Attribute__c> DummyList = new List<cscfga__Attribute__c>();
            //List<cscfga__Attribute__c> UtilityModelList = new List<cscfga__Attribute__c>();
            //List<String> LinkedChildsList = new List<String>();
            


private static void initTestData(){

/*
//List<cscfga__Attribute__c> UtilityModelList = new List<cscfga__Attribute__c>();
List<cscfga__Product_Basket__c> BasketList = P2A_TestFactoryCls.getProductBasket(1);
string BasketId = BasketList[0].Id;


List<cscfga__Product_Definition__c> ProdDefList= P2A_TestFactoryCls.getProductdef(1);
List<cscfga__Configuration_Screen__c> ConfigScreenList= P2A_TestFactoryCls.getConfigScreen(1,proddeflist);
List<cscfga__Screen_Section__c> ScreenSectionList = P2A_TestFactoryCls.getScreenSec(1,ConfigScreenList);

List<cscfga__Product_Configuration__c> ProdConfList = P2A_TestFactoryCls.getProductonfig(2);
ProdConfList[0].Name = 'COLO-RACK';
ProdConfList[0].cscfga__Product_Basket__c = BasketId;
//ProdConfList[1].Name = 'COLO-CAGE-RACK';
//ProdConfList[1].cscfga__Product_Basket__c = BasketId;
//ProdConfList[1].Master_IPVPN_Configuration__c = ProdConfList[0].Id;
ProdConfList[0].cscfga__Product_Definition__c = ProdDefList[0].id;
//ProdConfList[1].cscfga__Product_Definition__c = ProdDefList[0].id;

update ProdConfList;

*/

    List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
    List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Configuration_Screen__c> ConfigScreenList= P2A_TestFactoryCls.getConfigScreen(1,Prodef);
    List<cscfga__Screen_Section__c> ScreenSectionList = P2A_TestFactoryCls.getScreenSec(1,ConfigScreenList);
    List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
    List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
    List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
    List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> ProdConfList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
system.assertEquals(true,ProdConfList!=null); 


string ConfigId = ProdConfList[0].Id;
string LinkedChilds = ProdConfList[0].Id;


List<cscfga__Attribute_Definition__c > AttributeDefList = P2A_TestFactoryCls.getAttributesdef(2,ProdConfList,Prodef,ConfigScreenList,ScreenSectionList);
AttributeDefList[0].Name= 'Utility_Model';
//AttributeDefList[1].Name= 'Utility_Model';

update AttributeDefList;
system.assertEquals(true,AttributeDefList!=null); 
List<cscfga__Attribute__c> AttList = P2A_TestFactoryCls.getAttributes(2,ProdConfList,AttributeDefList);
AttList[0].cscfga__Value__c = 'Utilities unbundled';
//AttList[1].cscfga__Value__c = 'Utilities unbundled';

update AttList;
system.assertEquals(true,AttList!=null); 
searchFields.put('Products[0].id',Products[0].id);
searchFields.put('ConfigId',ConfigId);
searchFields.put('LinkedChilds',LinkedChilds);
}




private static testMethod void doDynamicLookupSearchTest() {
CS_TestUtil.disableAll(UserInfo.getUserId());
Test.startTest();

initTestData();



ColoUtilityModel utilitymodel = new ColoUtilityModel();
           String reqAtts = utilitymodel.getRequiredAttributes();
            Object[] data = utilitymodel.doDynamicLookupSearch(searchFields, productDefinitionId);
            system.assertEquals(true,data!=null); 
 
Test.stopTest();

}
     @isTest static void testExceptions(){
         ColoUtilityModel alls=new ColoUtilityModel();
        
         try{alls.doDynamicLookupSearch(null,null);}catch(Exception e){}
       
          system.assertEquals(true,alls!=null);    
         
         
     }

}