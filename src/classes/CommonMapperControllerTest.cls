/**
    @author - Accenture
    @date - 01-Aug-2012
    @version - 1.0
    @description - This is the test class for CommonMapperController.
*/
@isTest
//Start: Suresh 28/09/2017- Test class name changed as part of tech_debt
private class CommonMapperControllerTest
//End
{
    
    static testMethod void getCommonMapperDetails() {
        Set<string> usr = new Set<string>();
        usr = getUser();
        getCommonMapper();
        system.assert(usr!=null);
        CommonMapperController.setUserAgentMapperDetails('User','Agent__c',usr);
        
    }
    
    static testMethod void testCommonMapperUtil(){
        
        //String user = getUser();
        User user = getUsrObject();
        Agent__c agent = getAgent();
        
        getCommonMapper();
        system.assert(user!=null);
        CommonMapperUtil.createUpdateRecords('User','Agent__c', user);
        
        CommonMapperUtil.createUpdateRecords('User','Agent__c',user,'C',agent);
        CommonMapperUtil.createUpdateRecords('User','Agent__c',user,'U',agent);
        
        try{
            
            CommonMapperUtil.createUpdateRecords('User','Agent__c',user,'',agent);
            
            
            
            
        }catch(Exception e){
            
            String error = e.getMessage();
            system.debug('Error:'+error);
        }
        
        
    }
    static testMethod void testCommonMapperUtil1(){
        
        User user = getUsrObject();
        Agent__c agent = getAgent();
        system.assert(user!=null);
        try{
            CommonMapperUtil.createUpdateRecords('User','Agent__c',user,'U',null);
        }
        catch(Exception e){
            
            String error = e.getMessage();
            system.debug('Error:'+error);
        }
    }
    static testMethod void testCommonMapperUtil2(){
        
        User user = getUsrObject();
        Agent__c agent = getAgent();
        system.assert(user!=null);
        try{
            CommonMapperUtil.createUpdateRecords('Agent__c','User',user,'U',agent);
        }
        catch(Exception e){
            
            String error = e.getMessage();
            system.debug('Error:'+error);
        }
    }
     static testMethod void testCommonMapperUtil3(){
        
        User user = getUsrObject();
        Agent__c agent = getAgent();
        system.assert(user!=null);
        try{
            CommonMapperUtil.createUpdateRecords('User','Agent__c',agent,'U',user);
        }
        catch(Exception e){
            
            String error = e.getMessage();
            system.debug('Error:'+error);
        }
    }
    
    
    private static User getUsrObject(){
        
        Profile prof = [SELECT Id FROM Profile Where Name='TI Sales Manager' limit 1];

        User u = new User();
        u.FirstName = 'suraj';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj.talreja@accenture.com';
        u.Username = 'suraj.talreja@accenture.com';
        u.CommunityNickname = 'suraj.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.IsActive = true;
        u.ProfileId = prof.Id;
        u.EmployeeNumber ='7777';
        insert u;
        List<user> us = [Select id,FirstName from user where FirstName = 'suraj'];
        system.assertequals(u.firstname, us[0].firstname);
        System.assert(u != null); 
        return u;
        
        
    }
    private static Set<String> getUser(){
        List<User> userList = new List<User>();
        Set<string> userSet = new Set<string>();
        
        Profile prof = [SELECT Id FROM Profile Where Name='TI Sales Manager' limit 1];

        User u = new User();
        u.FirstName = 'suraj';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj.talreja@accenture.com';
        u.Username = 'suraj.talreja@accenture.com';
        u.CommunityNickname = 'suraj.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.IsActive = true;
        u.ProfileId = prof.Id;
        u.EmployeeNumber ='7778';
        insert u;
        System.assert(u != null); 
        userList.add(u);
        userSet.add(u.Id);
        
        return userSet;
    }

    private static void getCommonMapper(){
     List<String> fmStrLst = new String [] {'User;User;Agent;Agent__c;IsActive;IsActive__c;null;null;null;false', 
        'User;User;Agent;Agent__c;AD_User_ID__c;AD_User_ID__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Street;Street__c;null;null;null;false', 
        'User;User;Agent;Agent__c;CompanyName;CompanyName__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Email;E_mail__c;null;null;null;false', 
        'User;User;Agent;Agent__c;OrgId__c;OrgId__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Role_Name__c;Role_Name__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Username;Username__c;null;null;null;false', 
        'User;User;Agent;Agent__c;LanguageLocaleKey;Language__c;null;null;null;false', 
        'User;User;Agent;Agent__c;EmployeeNumber;EmployeeNumber__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Name;FullName__c;null;null;null;false', 
        'User;User;Agent;Agent__c;ProfileId;ProfileId__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Region__c;Region__c;null;null;null;false', 
        'User;User;Agent;Agent__c;State;State__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Title;Title__c;null;null;null;false', 
        'User;User;Agent;Agent__c;LocaleSidKey;LocaleSidKey__c;null;null;null;false', 
        'User;User;Agent;Agent__c;City;City__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Country;Country__c;null;null;null;false', 
        'User;User;Agent;Agent__c;TimeZoneSidKey;TimeZoneSidKey__c;null;null;null;false', 
        'User;User;Agent;Agent__c;MobilePhone;MobilePhone__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Department;Department__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Phone;Phone__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Fax;Fax__c;null;null;null;false', 
        'User;User;Agent;Agent__c;LastName;LastName__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Division;Division__c;null;null;null;false', 
        'User;User;Agent;Agent__c;FirstName;FirstName__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Profile_Name__c;Profile_Name__c;null;null;null;false', 
        'User;User;Agent;Agent__c;Id;ID__c;null;null;null;false', 
        'User;User;Agent;Agent__c;PostalCode;PostalCode__c;null;null;null;false'
        
        };
        
        List<Common_Mapper__c> fmLst = new  List<Common_Mapper__c>();
        for(String str : fmStrLst){
            String [] spltStr = str.split(';',0);
            Common_Mapper__c cm = new Common_Mapper__c(Source_Object__c = spltStr[0], Source_Object_API_Name__c = spltStr[1] , Destination_Object__c = spltStr[2], Destination_Object_API_Name__c = spltStr[3], Source_Field_API_Name__c=spltStr[4] , Destination_Field_API_Name__c = spltStr[5], Reference_Object__c = spltStr[6],Related_ID__c=spltStr[7],Reference_Object_API_Field_Name__c=spltStr[8],Derived_From_ID__c = false);
            fmLst.add(cm);
        }
        
        insert fmLst;
        System.assert(fmLst != null); 


    }
    private static Agent__c getAgent()
    {
       /* List<Agent__c> agList = new List<Agent__c>();
        Agent__c ag = new Agent__c();
        ag.FullName__c = 'Agent Record';
        ag.LastName__c = 'Record';
        ag.Language__c ='en_US';
        ag.ProfileID__c = [SELECT Id FROM Profile Where Name='TI Sales Manager' limit 1].Id;
        ag.ID__c = [SELECT Id FROM User where Username = 'dinesh.talreja@accenture.com.ti' limit 1].Id;
        ag.Username__c = 'dinesh.talreja@accenture.com.ti';
        ag.E_mail__c = 'dinesh.talreja@accenture.com';
        ag.CurrencyIsoCode = 'GBP';
        ag.CurrencyISOCode__c = 'GBP';
        ag.LocaleSidKey__c = 'en_AU';
        ag.TimeZoneSidKey__c = 'Australia/Sydney';
        agList.add(ag);*/
        
        Agent__c ag1 = new Agent__c();
        ag1.FullName__c = 'Test Agent Record';
        ag1.LastName__c = 'Record';
        ag1.Language__c ='en_US';
        ag1.ProfileID__c = [SELECT Id FROM Profile Where Name='TI Account Manager' limit 1].Id;
        ag1.ID__c = [SELECT Id FROM User where Username = 'suraj.talreja@accenture.com' limit 1].Id;
        ag1.Username__c = 'test.user@accenture.com.tiprod';
        ag1.E_mail__c = 'irshad.alam@accenture.com';
        ag1.CurrencyIsoCode = 'GBP';
        ag1.CurrencyISOCode__c = 'GBP';
        ag1.LocaleSidKey__c = 'en_AU';
        ag1.TimeZoneSidKey__c = 'Australia/Sydney';
       // agList.add(ag1);
        insert ag1;
        System.assert(ag1!= null); 

        return ag1;
        
    }
    
    static testMethod void testCommonMapperUtil4(){
  
        Set<string> fiscalYrSet = new Set<string>(); 
        Set<id> userIds = new Set<id>();
         Set<string> usr = new Set<string>();

         usr = getUser();
        getCommonMapper();
        Map<Id, date> mapUserIdPeriod = new Map<Id, date>();
        Map<Id, date> mapUserIdPeriodE = new Map<Id, date>();
        List<Performance_Tracking__c> performanceTrackingList=new List<Performance_Tracking__c>();
        Map<Id,Performance_Tracking__c> mapUserIdPerf=new Map<Id,Performance_Tracking__c>(); 
        
         
          Performance_Tracking__c  perfobj=new Performance_Tracking__c();
                mapUserIdPerf.put(perfObj.Employee__c,perfObj);
                
         profile pr = [select id from profile where name='System administrator'];
      List<User> userlist = new List<User>();
       User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, SIP_User__c = True,
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123456@testorg.com',EmployeeNumber='123');
                
                
                userlist.add(u);
                insert userlist ;
                System.assert(userlist != null); 

            
   
                perfobj=new Performance_Tracking__c();
                //perfobj.Employee__c=usrObj.Id;
                //perfobj.Fiscal_year__c=mapUserIdPeriod.get(usrObj.Id).year()+'-'+mapUserIdPeriodE.get(usrObj.Id).year();
                //perfobj.FY_Start_date__c=mapUserIdPeriod.get(usrObj.Id);
                //perfobj.FY_End_Date__c=mapUserIdPeriodE.get(usrObj.Id);
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                perfobj.OwnerId=perfobj.Employee__c;
                perfobj.SOV_Type__c='Performance';
                perfobj.Currency__c='AUD';
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                
                performanceTrackingList.add(perfobj);
           
            CommonMapperController.createSOVPerformanceTrackingforSIPUser(usr,mapUserIdPeriod,mapUserIdPeriodE,userIds);
        
        
       }
}