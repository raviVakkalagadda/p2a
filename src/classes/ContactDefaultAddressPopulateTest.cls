@isTest
private class ContactDefaultAddressPopulateTest
{
    static testMethod void testBatch() {

        // Create 200 test Contacts - this simulates one execute.   
    
        // Important - the Salesforce.com test framework only allows you to  
        // test one execute.   
         Account a = UnitTestHelper.getAccount();
       // Account a = [SELECT id, Name, BillingStreet, BillingCity, BillingState, BillingCountry, BillingPostalCode FROM Account WHERE Name = 'ARTICON-INTEGRALIS LTD'];
        
        //Get the address fields that the contact should get assigned
        String mailingStreet = a.BillingStreet;
        String mailingCity = a.BillingCity;
        String mailingState = a.BillingState;
        String mailingCountry = a.BillingCountry;
        String mailingPostalCode = a.BillingPostalCode;

        List <Contact> contacts = new List<Contact>();
        for(integer i = 0; i<200; i++){
            Contact c = new Contact(Description='testContact'+i, AccountId=a.Id, LastName='testName', 
                Contact_Type__c='Sales'); 
            contacts.add(c);
        }
   
        insert contacts;
        system.assert(contacts!=null);
        List<Contact> c = [Select id,lastname from Contact where lastname ='testName'];
        system.assertequals(contacts[0].lastname,c[0].lastname);
   
        Test.StartTest();
        ContactDefaultAddressPopulate contactPopulate = new ContactDefaultAddressPopulate();
        
        contactPopulate.query='SELECT ID, Name, MailingStreet, MailingCity, MailingState, ' + 
            'MailingCountry, MailingPostalCode, AccountId ' +
            'FROM Contact ' +
            'WHERE MailingStreet = null and LastName = \'testName\'' +
            ' LIMIT 100';

        contactPopulate.email='ross.tailby@teamuk.telstra.com';
        ID batchprocessid = Database.executeBatch(contactPopulate);
        Test.StopTest();


   
    }
}