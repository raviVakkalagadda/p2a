@IsTest
private class ContactDefaultAddressSchedulerTest{
    
    private static String CRON_EXP = '0 0 0 3 9 ? 2022';
    
    static TestMethod void TestScheduler(){
        Test.startTest();
        
        String jobId = System.schedule('testUpdateContactAddresses',
                                        CRON_EXP,
                                        new ContactDefaultAddressScheduler());
                                        
        //Get the scheduled Update Contact Address from the Job API using Job ID
        CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, 
                         NextFireTime
                         FROM CronTrigger WHERE id = :jobId];
                         
        System.assertEquals(CRON_EXP, ct.CronExpression);
        System.assertEquals(0, ct.TimesTriggered);
        System.assertEquals('2022-09-03 00:00:00', String.valueOf(ct.NextFireTime));
        
        Test.stopTest();
    }
}