/**    @author - Accenture    
       @date - 14-JUNE-2012    
       @version - 1.0    
       @description - This is a Business class for Contract .
*/
public class ContractBusinessClass{
    
    /*   @author - Accenture    
       @date - 25-May-2012    
       @version - 1.0     
       @param - String contractId        
       @return - void
       @description - This method is used for updating fields on Account based on Contract .
*/
  public static void updateAccountfield(){
         //List of account.
    //List<Account> accList = new List<Account>();
    List<Account> acctoUpdateList = new List<Account>();
    Set<id> accountIds = new Set<Id>();
    //Map<Id,Id> contractAccounts = new Map<Id,Id>();
    Map<Id,Account> accMap = new Map<Id, Account>();
     
 for (Contract__c objContract:(List<Contract__c>)trigger.new)
    {
        accountIds.add(objContract.Account__c);
        //contractAccounts.put(objContract.Id,objContract.Account__c);
    }
    
    //accList  = [SELECT Id, NDA_Expiry_Date__c, Tcorp_NDA__c, NDA_Signed__c, NDA_Start_Date__c, Telstra_Legal_Entity_Name__c,Global_Master_Service_Agreement_Global_B__c  FROM Account WHERE Id in :accountIds];
     for (Account acc : [SELECT Id, NDA_Expiry_Date__c, Tcorp_NDA__c, NDA_Signed__c, NDA_Start_Date__c, Telstra_Legal_Entity_Name__c,Global_Master_Service_Agreement_Global_B__c  FROM Account WHERE Id in :accountIds]){
        accMap.put(acc.Id,acc);
     } 
     
     for (Contract__c objContract:(List<Contract__c>)trigger.new)
    {
    // If (accMap.containsKey(objContract.Account__c))then update the fields based on the condition below on associated Account.
        if (accMap.containsKey(objContract.Account__c)){   
                Account objAccount =accMap.get(objContract.Account__c);
                //Assigning values to the field on Account.
                objAccount.Telstra_Legal_Entity_Name__c= objContract.Telstra_Contracting_Entity__c;
        {
                if (objContract.Tcorp_NDA__c== True) {
                //Assigning values to the Tcorp field on Account.
                objAccount.Tcorp_NDA__c= True;
                }
                if (objContract.NDA_Signed__c== True) { 
                //Assigning values to NDA field on Account.
                objAccount.NDA_Signed__c= objContract.NDA_Signed__c;
                objAccount.NDA_Start_Date__c= objContract.NDA_Start_Date__c;
                objAccount.NDA_Expiry_Date__c= objContract.NDA_Expiry_Date__c; 
                }
                if (objContract.Contract_Type__c!= 'CRA' && objContract.Contract_Type__c!= 'Other') {
                //Assigning values to the Tcorp field on Account.
                objAccount.Global_Master_Service_Agreement_Global_B__c= True;
                } 
        }
        acctoUpdateList.add(objAccount);
    }
    //update  acctoUpdateList;
} 
update  acctoUpdateList;
} 
}