global class CreateETCActionItem{
    
    webservice static void createEachAI(String OrderID){
        //Changes as part of IR 088 enhancement -- querying order owner's id so that it can be assigned to the action items -- Bhargav
        List<Order__c> OrderObj = [Select Account__r.Customer_Type__c,Requested_Termination_Date__c,Termination_Reason__c,Owner.Id from Order__c where Id=:OrderID];
        //List<Order_Line_Item__c> oliListforInsert=[Select Id, Name, Within_Contract__c, Service__c, Service__r.Name, Service__r.Billing_Commencement_Date__c, Service__r.Bill_Profile_Id__r.Currency__c, Resource__c, Resource__r.Name, Resource__r.Billing_Commencement_Date__c, Resource__r.Bill_Profile_Id__r.Currency__c, ContractTerm__c, Contract_Expiry_Date__c, ParentOrder__r.Account__r.Customer_Type__c, ParentOrder__r.Account__r.Name, ParentOrder__r.Account__r.Id,ParentOrder__r.Requested_Termination_Date__c,ParentOrder__r.Termination_Reason__c from Order_Line_Item__c where ParentOrder__c=:OrderID and OrderType__c='Terminate'];
        //system.debug(olilistforinsert.size()+'olilist');
        //List<Action_Item__c> existingAIList = [Select Id, Order_Line_Item__c, Order_Action_Item__c from Action_Item__c where Order_Action_Item__c=:OrderID];
        //system.debug(existingAIList.size()+'AIlist');
        Map<Id, Action_Item__c> AIMap = new Map<Id, Action_Item__c>();
        for(Action_item__c ai:[Select Id, Order_Line_Item__c, Order_Action_Item__c from Action_Item__c where Order_Action_Item__c=:OrderID]){
            AIMap.put(ai.Order_Line_Item__c,ai);
        }
        //Commented as part of IR 088 enhancement -- assigning action item to order creator rather than to sales admin -- Bhargav
        /*List<QueueSobject> objQueue = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='Action_Item__c'];
        Map<String, Id> QueueMap= new Map<String, Id> ();
        for (QueueSObject q:objQueue){
            QueueMap.put(q.Queue.Name,q.QueueId);
        }
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        String accCustomerType= OrderObj[0].Account__r.Customer_Type__c;
        String queueStr='';
       if(usr.Region__c!=null && accCustomerType!=null){      
                if(usr.Region__c=='North Asia' || usr.Region__c=='South Asia')
                {
                    queueStr='Sales Admin - Asia';  
                }
                else if (usr.Region__c=='Australia'&& accCustomerType=='MNC')
                {
                    queueStr='Sales Admin - Australia MNC';
                } 
                 else if (usr.Region__c=='Australia'&& accCustomerType=='GSP')
                {
                    queueStr='Sales Admin - Australia GSP';
                }                
                else if(usr.Region__c=='US')
                {
                    queueStr='Sales Admin - US';
                }
                else if(usr.Region__c=='EMEA')
                {
                    queueStr='Sales Admin - EMEA';
                }else{
                  
                  queueStr='Sales Admin - Australia';
                }
                
            }else 
            {
                queueStr='Sales Admin - Australia';
            }  */
        List<Action_Item__c> actionItemList=new List<Action_Item__c>();
        Action_Item__c actionItemObj=null;
        for(Order_Line_Item__c oli:[Select Id, Name, Within_Contract__c, Service__c, Service__r.Name, Service__r.Billing_Commencement_Date__c, Service__r.Bill_Profile_Id__r.Currency__c, Resource__c, Resource__r.Name, Resource__r.Billing_Commencement_Date__c, Resource__r.Bill_Profile_Id__r.Currency__c, ContractTerm__c, Contract_Expiry_Date__c, ParentOrder__r.Account__r.Customer_Type__c, ParentOrder__r.Account__r.Name, ParentOrder__r.Account__r.Id,ParentOrder__r.Requested_Termination_Date__c,ParentOrder__r.Termination_Reason__c from Order_Line_Item__c where ParentOrder__c=:OrderID and OrderType__c='Terminate']){
            if(!AIMap.containskey(oli.Id)){
                actionItemObj=new Action_Item__c();  
                //Service__c serv=oli.Service__c;
                //Resource__c res=oli.Resource__c;
                actionItemObj.Account__c=oli.ParentOrder__r.Account__r.Id;
                //Changes as part of IR 088 -- Defect#8381 -- Changed LOV values of Status field -- Bhargav
                actionItemObj.Status__c='Sales User in Progress';
                actionItemObj.Subject__c='Request to Calculate Early Termination Charge';
                //Changes as part of IR 088 enhancement -- assigning action item to order creator rather than to sales admin -- Bhargav
                actionItemObj.OwnerId=OrderObj[0].Owner.Id;//QueueMap.get(queueStr);
                actionItemObj.Order_Action_Item__c=OrderID;
                actionItemObj.Billing_Commencement_Date__c=oli.Service__c!=null?oli.Service__r.Billing_Commencement_Date__c:oli.Resource__r.Billing_Commencement_Date__c;
                actionItemObj.Contract_Duration__c=oli.ContractTerm__c;
                actionItemObj.Contract_Expiry_Date__c=oli.Contract_Expiry_Date__c;
                actionItemObj.Service_Resource__c=oli.Service__c!=null?oli.Service__r.Name:oli.Resource__r.Name;
                actionItemObj.Service__c=oli.Service__c;
                actionItemObj.Resource__c=oli.Resource__c;
                actionItemObj.Order_Line_Item__c=oli.Id;
                actionItemObj.Due_Date__c=system.today()+2;
                //Enhancements as part of IR 088 -- Defect#8337 -- Populating Billing Currency from Bill Profile -- Bhargav
                actionItemObj.Billing_Currency__c=oli.Service__c!=null?oli.Service__r.Bill_Profile_Id__r.Currency__c:oli.Resource__r.Bill_Profile_Id__r.Currency__c;
                actionItemObj.CurrencyIsoCode=actionItemObj.Billing_Currency__c;
                actionItemObj.ETC_Checkbox__c=true;
                //Enhancements as part of IR 088 -- Defect#8389 -- Notifying period checkbox to be checked when service/resource is terminated whose contract has already expired -- Bhargav
                actionItemObj.Notifying_Period__c=true;
                 actionItemObj.Requested_Termination_Date__c=oli.ParentOrder__r.Requested_Termination_Date__c;
                 actionItemObj.Termination_Reason__c=oli.ParentOrder__r.Termination_Reason__c;
                
                //actionItemObj.ETC__c=0.00;
                //actionItemObj.New_ETC__c=0.00;
                actionItemList.add(actionItemObj); 
            }
        }
        system.debug('====actionItemList=='+actionItemList);
        if(actionItemList.size()>0 ){
            insert actionItemList;
        }
    }
}