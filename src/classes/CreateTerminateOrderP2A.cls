global class CreateTerminateOrderP2A implements CSPOFA.ExecutionHandler{
   /**
    * Method 1
    * Description:This method holds the logic for creation of terminate order if anything fails the commit is rolled back
    * Called from CS managed package through a batch process
    */
    public List<sObject> process(List<SObject> data){
        Savepoint sp = Database.setSavepoint();
        
        try{
            List<sObject> result = new List<sObject>();
            String usernameTime = userinfo.getUserName()+system.now();
            List<CSPOFA__Orchestration_Step__c> stepList = (List<CSPOFA__Orchestration_Step__c>)data; 
            List<csord__Order__c> TerminateOrderLst = new List<csord__Order__c>();
            List<csord__Order_Request__c> CreateTermOdrReqList = new List<csord__Order_Request__c>();
            List<csord__Subscription__c> UpdateSubscriptionLst = new List<csord__Subscription__c>();
            List<csord__Service__c> UpdateServiceLst = new List<csord__Service__c>();
            system.debug('=====stepList==='+stepList);
            System.debug('<<<< 1 >>>>');
            Set<Id> processIdSet = new Set<Id>();
            Map<Id, CSPOFA__Orchestration_Process__c> Maporder = new Map<Id, CSPOFA__Orchestration_Process__c>();
            Map<Id, CSPOFA__Orchestration_Process__c> Mapsubs = new Map<Id, CSPOFA__Orchestration_Process__c>();
            Map<Id, Id> createdByIdMap = new Map<Id, Id>();
            Set<Id> stepIdset = new Set<Id>();
            List<Id> subscriptionIds = new List<Id>();
            List<Id> serviceIds = new List<Id>();
            List<Id> orderIds = new List<Id>();
            
            for(CSPOFA__Orchestration_Step__c Step :stepList){
                stepIdset.add(step.Id);
            }
            
            //List<CSPOFA__Orchestration_Step__c> stepListAll = [select id,CSPOFA__Orchestration_Process__c from CSPOFA__Orchestration_Step__c where id in:stepIdset];
            
            for(CSPOFA__Orchestration_Step__c step :[select id,CSPOFA__Orchestration_Process__c from CSPOFA__Orchestration_Step__c where id in:stepIdset]) {
                processIdSet.add(Step.CSPOFA__Orchestration_Process__c);            
            }
            
            //List<CSPOFA__Orchestration_Process__c> processList = [SELECT csordtelcoa__Subscription__c,CreatedById,csordtelcoa__Subscription__r.csord__Order__c,csordtelcoa__Subscription__r.csord__Order_Request__c FROM CSPOFA__Orchestration_Process__c WHERE Id IN :processIdSet];
            
            for(CSPOFA__Orchestration_Process__c processObj :[SELECT csordtelcoa__Subscription__c,CreatedById,csordtelcoa__Subscription__r.csord__Order__c,csordtelcoa__Subscription__r.csord__Order_Request__c FROM CSPOFA__Orchestration_Process__c WHERE Id IN :processIdSet]){
                Maporder.put(processObj.csordtelcoa__Subscription__r.csord__Order__c, processObj);
                Mapsubs.put(processObj.csordtelcoa__Subscription__c, processObj);
            }

            List<csord__Order__c> OrderList = [Select Id,Name,csord__Order_Request__c,csord__Order_Request__r.Id,csord__Identification__c,
                                                csord__Account__c,csord__Order_Type__c,Status__c,Customer_Primary_Order_Contact__c,
                                                Technical_Sales_Contact__c,Customer_Technical_Contact__c,Account_Manager_PC__c,Account_Manager_2__c 
                                                from csord__Order__c 
                                                where Id IN: Maporder.keyset()];
            
            System.debug('<<<< 2 >>>>');
            List<csord__Subscription__c> subsList = [Select Id,Name,csord__Order_Request__r.csord__Module_Name__c,csord__Order_Request__r.csord__Module_Version__c,csord__Order_Request__r.csord__Processing_Mode__c,csord__Order_Request__r.csord__Process_Status__c,csord__Order_Request__r.Name,csord__Order_Request__c,csord__Order__c,csord__Order__r.Name,csord__Order__r.csord__Identification__c,csord__Order__r.csord__Account__c,csord__Order__r.Customer_Primary_Order_Contact__c,csord__Order__r.Technical_Sales_Contact__c,csord__Order__r.Customer_Technical_Contact__c,csord__Order__r.Account_Manager_PC__c,csord__Order__r.Account_Manager_2__c,csordtelcoa__Change_Type__c,OwnerId,OpportunityRelatedList__c,csord__Identification__c
                                                from csord__Subscription__c 
                                                where Id IN: Mapsubs.keyset()];                                         

            /** New change: Link the new order to cloned Subscription & Service **/
            Set<Id> subsSet = Mapsubs.keyset();
            Set<Id> servSet = new Set<Id>();
            String servquery = getAllFieldQuery('csord__Service__c',subsSet);
            String subsquery = getAllFieldQuery('csord__Subscription__c',subsSet);
            List<csord__Service__c> servObjList = database.query(servquery.replace('XXX','subsSet'));
            List<csord__Subscription__c> subsObjList = database.query(subsquery.replace('XXX','subsSet'));              
            
            for(csord__Service__c servObj :servObjList){
                servSet.add(servObj.Id);
            }

            /** New change: Link the new order to cloned Subscription & Service **/
            Id terminateRecType = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Cease Order');

                     
                                
            /** 10615: IP006/Terminate: Order not getting created: Fixed by Suparna **/
            Map<String, Id>  OldSubsWithNewOrdReqMap = new Map<String, Id>();
            Map<String, Id>  NewOrdReqWithOldSubsMap = new Map<String, Id>();
            Map<String, Id>  OdrIdentificationWithOldSubsMap = new Map<String, Id>();
            Map<Id, Id> newoldOrder = new Map<Id, Id>();
            Map<Id, Id> OldSubWithSolution = new Map<Id, Id>();
            List<Solutions__c> solnList = new List<Solutions__c>();
            Map<String, Id> SubIdentificationWithOldSubsMap = new Map<String, Id>();
            Map<Id, Id> OldSubsWithNewSubs = new Map<Id, Id>();
            csord__Subscription__c newClonedSubs = null;
            csord__Service__c newClonedServ = null;
            
            if(!subsList.IsEmpty()){
                for(csord__Subscription__c subs :subsList){
                    csord__Order_Request__c Createterminateorderreq = new csord__Order_Request__c();
                    Createterminateorderreq.csord__Module_Name__c = subs.csord__Order_Request__r.csord__Module_Name__c;
                    Createterminateorderreq.csord__Module_Version__c = subs.csord__Order_Request__r.csord__Module_Version__c;
                    Createterminateorderreq.csord__Processing_Mode__c = subs.csord__Order_Request__r.csord__Processing_Mode__c;
                    Createterminateorderreq.csord__Process_Status__c = subs.csord__Order_Request__r.csord__Process_Status__c;
                    Createterminateorderreq.Name = 'Terminate-'+subs.Id;
                    Createterminateorderreq.csord__Request_DateTime__c = Date.today();
                    if(Mapsubs.get(subs.Id) != null){Createterminateorderreq.OwnerId = Mapsubs.get(subs.Id).CreatedById;}
                    NewOrdReqWithOldSubsMap.put(Createterminateorderreq.Name, subs.Id);
                    CreateTermOdrReqList.add(Createterminateorderreq);
                }

                if(!CreateTermOdrReqList.IsEmpty())
                    insert CreateTermOdrReqList;            
            }
            
            for(csord__Order_Request__c ordRequest :CreateTermOdrReqList){
                if(NewOrdReqWithOldSubsMap.get(ordRequest.Name) != null){                           
                    OldSubsWithNewOrdReqMap.put(NewOrdReqWithOldSubsMap.get(ordRequest.Name), ordRequest.Id);
                    createdByIdMap.put(NewOrdReqWithOldSubsMap.get(ordRequest.Name), ordRequest.OwnerId);
                }
            }

            if(!subsList.IsEmpty()){

                for(csord__Subscription__c subs :subsObjList){
                    Solutions__c soln = new Solutions__c();
                    soln.Account_Name__c = subs.csord__Account__c;
                    soln.Description__c = subs.Id;
                    solnList.add(soln);
                }
                
                if(solnList.Size() > 0){insert solnList;}
                
                for(Solutions__c soln :solnList){
                    OldSubWithSolution.put(Id.ValueOf(soln.Description__c), soln.Id);
                }           

                for(csord__Subscription__c subs :subsList){
                    csord__Order__c TerminateOrderRec = new csord__Order__c();
                    TerminateOrderRec.recordtypeID = TerminateRecType;
                    TerminateOrderRec.Is_Terminate_Order__c = True;
                    TerminateOrderRec.csord__Order_Type__c = 'Terminate';
                    TerminateOrderRec.Order_Type__c = 'Terminate';
                    TerminateOrderRec.Status__c = 'New';
                    TerminateOrderRec.Full_Termination__c = true;
                    TerminateOrderRec.Is_Order_Submitted__c = false;
                    TerminateOrderRec.Order_Submitted__c = false;
                    TerminateOrderRec.Solutions__c = OldSubWithSolution.containsKey(subs.Id)? OldSubWithSolution.get(subs.Id): null;
                    TerminateOrderRec.OldodrId__c = subs.csord__Order__c!=null? subs.csord__Order__c: null;
                    TerminateOrderRec.Name = subs.csord__Order__c!=null? 'Terminate-'+subs.csord__Order__r.Name: 'Terminate-Migrated';
                    TerminateOrderRec.csord__Order_Request__c = OldSubsWithNewOrdReqMap.containskey(subs.Id)? OldSubsWithNewOrdReqMap.get(subs.Id): null;
                    TerminateOrderRec.csord__Identification__c = subs.csord__Order__c!=null? subs.csord__Order__r.csord__Identification__c: subs.csord__Identification__c;
                    TerminateOrderRec.csord__Account__c = subs.csord__Order__c!=null? subs.csord__Order__r.csord__Account__c: null;
                    TerminateOrderRec.Customer_Primary_Order_Contact__c = subs.csord__Order__c!=null? subs.csord__Order__r.Customer_Primary_Order_Contact__c: null;
                    TerminateOrderRec.Technical_Sales_Contact__c = subs.csord__Order__c!=null? subs.csord__Order__r.Technical_Sales_Contact__c: null;
                    TerminateOrderRec.Customer_Technical_Contact__c = subs.csord__Order__c!=null? subs.csord__Order__r.Customer_Technical_Contact__c: null;
                    TerminateOrderRec.Account_Manager_PC__c = subs.csord__Order__c!=null? subs.csord__Order__r.Account_Manager_PC__c: null;
                    TerminateOrderRec.Account_Manager_2__c = subs.csord__Order__c!=null? subs.csord__Order__r.Account_Manager_2__c: null;
                    OdrIdentificationWithOldSubsMap.put(TerminateOrderRec.csord__Identification__c, subs.Id);

                    if(createdByIdMap.get(subs.Id) != null){
                        TerminateOrderRec.OwnerId = createdByIdMap.get(subs.Id);
                        TerminateOrderRec.createdbyId = createdByIdMap.get(subs.Id);
                        TerminateOrderRec.lastmodifiedbyId = createdByIdMap.get(subs.Id);
                    }

                    TerminateOrderLst.add(TerminateOrderRec);
                }
                
                if(!TerminateOrderLst.IsEmpty()){
                    insert TerminateOrderLst;            
                }
            }

            for(csord__Order__c odr :TerminateOrderLst){
                orderIds.add(odr.Id);

                if(odr.csord__Identification__c != null){
                    newoldOrder.put(OdrIdentificationWithOldSubsMap.get(odr.csord__Identification__c), odr.Id);
                }
            }
            
            if(!subsObjList.IsEmpty()){
                
                for(csord__Subscription__c subs :subsObjList){
                    if(!TerminateOrderLst.isEmpty()){
                        newClonedSubs = new csord__Subscription__c();
                        newClonedSubs = subs.clone(false,false,false,false);
                        newClonedSubs.csord__Order__c = newoldOrder.get(subs.Id) != null? newoldOrder.get(subs.Id): null;
                        newClonedSubs.csord__Order_Request__c = OldSubsWithNewOrdReqMap.containsKey(subs.Id)? OldSubsWithNewOrdReqMap.get(subs.Id): null;
                        newClonedSubs.csordtelcoa__Change_Type__c = 'Terminate';
                        newClonedSubs.Solutions__c = OldSubWithSolution.containsKey(subs.Id)? OldSubWithSolution.get(subs.Id): null;
                        newClonedSubs.OpportunityRelatedList__c = null;
                        newClonedSubs.csord__Identification__c = subs.csord__Identification__c;
                        newClonedSubs.csord__External_Identifier2__c = getUniqueExternalId(subs.Id, usernameTime);
                        SubIdentificationWithOldSubsMap.put(subs.csord__Identification__c, subs.Id);
                        
                        if(createdByIdMap.get(subs.Id) != null){
                            newClonedSubs.OwnerId =  newClonedSubs.Account_Owner_ID__c;
                            newClonedSubs.createdbyId = createdByIdMap.get(subs.Id);
                            newClonedSubs.lastmodifiedbyId = createdByIdMap.get(subs.Id);
                        }

                        UpdateSubscriptionLst.add(newClonedSubs);
                    }
                }

                if(!UpdateSubscriptionLst.isEmpty()) //Nikhil -- Added null check
                    upsert UpdateSubscriptionLst;
            }
            System.debug('<<<< 9 >>>>');
            Map<Id,Id>SubsToSolnId=new Map<Id,Id>();
            for(csord__Subscription__c subs :UpdateSubscriptionLst){
                OldSubsWithNewSubs.put(SubIdentificationWithOldSubsMap.get(subs.csord__Identification__c), subs.Id);
                SubsToSolnId.put(subs.Id,subs.solutions__c);
                subscriptionIds.add(subs.Id);
            }
            
            for(csord__Service__c srvc :servObjList){
                if(!TerminateOrderLst.isEmpty()){
                    newClonedServ = new csord__Service__c();
                    newClonedServ = srvc.clone(false,false,false,false);    
                    newClonedServ.csord__Order_Request__c = OldSubsWithNewOrdReqMap.containsKey(srvc.csord__Subscription__c)? OldSubsWithNewOrdReqMap.get(srvc.csord__Subscription__c): null;
                    newClonedServ.ROC_Line_Item_Status__c = '';
                    newClonedServ.Cease_Service_Flag__c = true;
                    newClonedServ.SNOW_Complete__c = false;
                    newClonedServ.csord__Status__c = 'Service Created';               
                    newClonedServ.Inventory_Status__c = 'LIVE - AS DESIGNED';
                    newClonedServ.AccountId__c = srvc.AccountId__c;
                    newClonedServ.Bill_ProfileId__c = srvc.Bill_ProfileId__c;
                    newClonedServ.SN_request_item_number__c = '';
                    newClonedServ.Opportunity__c = null;
                    newClonedServ.Order_Type_Final__c = 'Terminate';
                    newClonedServ.csordtelcoa__Replaced_Service__c = srvc.Id;
                    newClonedServ.csord__Subscription__c = OldSubsWithNewSubs.get(srvc.csord__Subscription__c);
                    newClonedServ.Solutions__c = SubsToSolnId.containsKey(newClonedServ.csord__Subscription__c)? SubsToSolnId.get(newClonedServ.csord__Subscription__c): null;
                    newClonedServ.csord__Order__c = newoldOrder.get(srvc.csord__Subscription__c) != null? newoldOrder.get(srvc.csord__Subscription__c): null;
                    newClonedServ.csord__External_Identifier2__c = getUniqueExternalId(srvc.Id, usernameTime);
                    
                    if(createdByIdMap.get(srvc.csord__Subscription__c) != null){
                        newClonedServ.createdbyId = createdByIdMap.get(srvc.csord__Subscription__c);
                        newClonedServ.lastmodifiedbyId = createdByIdMap.get(srvc.csord__Subscription__c);
                    }

                    UpdateServiceLst.add(newClonedServ);
                }
            }
       
            if(!UpdateServiceLst.IsEmpty()) //Nikhil -- Added null check
                upsert UpdateServiceLst;
        
            Map<Id, Id> oldNewServMap = new Map<Id, Id>();
            for(csord__Service__c servObj :UpdateServiceLst){
                  oldNewServMap.put(servObj.csordtelcoa__Replaced_Service__c,servObj.Id);
                  serviceIds.add(servObj.Id);
            }

            for(csord__Service__c servObj :UpdateServiceLst){
                if(servObj.csord__Service__c != null && oldNewServMap.containskey(servObj.csord__Service__c)){
                    servObj.csord__Service__c = oldNewServMap.get(servObj.csord__Service__c);
                } else if(servObj.Master_Service__c != null && oldNewServMap.containskey(servObj.Master_Service__c)){
                    servObj.Master_Service__c = oldNewServMap.get(servObj.Master_Service__c);
                }
            }
            
            if(!UpdateServiceLst.IsEmpty()) //Nikhil -- Added null check
                update UpdateServiceLst;

            /**
             * Calling all methods of 'CS_BatchOrderGenerationObserver' class to udpate the Services, Subscriptions and Orders after Terminate orders are created and inserted.
             * As part of the Winter17(CR26) the below method call is mandatory to execute the Services, Subscriptions and Orders handler classes.
             */
            new CS_BatchOrderGenerationObserver().processServices(serviceIds);        
            new CS_BatchOrderGenerationObserver().processSubscriptions(subscriptionIds);
            new CS_BatchOrderGenerationObserver().processOrders(orderIds);

            System.debug('<<<< 11 >>>>');
            System.debug('<<<<<< This message is before the step is updated >>>>>>');
            for(CSPOFA__Orchestration_Step__c Step :stepList){
                step.CSPOFA__Status__c = 'Complete';
                step.CSPOFA__Completed_Date__c = Date.today();
                step.CSPOFA__Message__c = 'Termination Order Created';
                result.add(step);
                System.debug('<<<<<< This message is after the step is updated >>>>>>'+TerminateOrderLst);
                //sendOrderEmailtoCreator(newoldOrder.values());
            }
            
            /**
            * Do post processing tasks, for example bulk update/insert operations
            * Below if condition added for triage purpose only
            * Developer: Ritesh
            * As a part of defect fix: 14392
            */
            if(Label.EnableTerminateExceptionTrace == 'True'){
                sendExceptionMail('success \n\n result : \n\n'+result);
            }
            //if statement ends
            return result;
        } catch(Exception Ex){
        ErrorHandlerException.ExecutingClassName='CreateTerminateOrderP2A :process';
        ErrorHandlerException.objectId=String.valueOf(data[0].id);
        ErrorHandlerException.sendException(Ex); 
            system.debug('Exception Ocurred===' +Ex);
            Database.rollback(sp);
            if(Label.EnableTerminateExceptionTrace == 'True'){
                String errormsg=String.valueOF(ex.getTypeName())+'\n\n'+
                String.valueOF(ex.getMessage())+'\n\n'+
                String.valueOf(Ex.getStackTraceString());
                sendExceptionMail(errormsg);
            }
        return null;
        }
    }
 
    /**
     * Method 3
     */
    public static String getAllFieldQuery(String ObjectName, Set<Id> IdSet){
        //Initialize setup variables
        String query = 'SELECT';
        Map<String, Schema.SObjectField> objectFields = Schema.getGlobalDescribe().get(ObjectName).getDescribe().fields.getMap();

        //Grab the fields from the describe method and append them to the queryString one by one.
        for(String s : objectFields.keySet()) {
            query += ' ' + s + ',';
        }

        //Manually add related object's fields that are needed.
        
        // Strip off the last comma if it exists.
        if(query.subString(query.Length()-1,query.Length()) == ','){
            query = query.subString(0,query.Length()-1);
        }

        //Add FROM statement
        query += ' FROM ' + ObjectName;

        //Add on a WHERE/ORDER/LIMIT statement as needed
        if(ObjectName=='csord__Subscription__c'){
            query += ' WHERE Id In:XXX'; // modify as needed
        } else if(ObjectName=='csord__Service_Line_Item__c'){
            query += ' WHERE csord__Service__c In:XXX';
        }else{
            query += ' WHERE csord__Subscription__c In:XXX';    
        }
        
        //modify as needed
        return query;
    }

    /**
     * Method 4
     */
    public static String getUniqueExternalId(String uniqueValue,String usernameTime){
        Blob hash = Crypto.generateDigest('MD5', Blob.valueOf(usernameTime+uniqueValue));
        return EncodingUtil.convertToHex(hash);
    }
 
    /**
     * Method 5
     */
    public static void sendOrderEmailtoCreator(List<Id> odrIdset){
        try{
            List<csord__Order__c> termOrdLst = [Select Id,Account_ID__c,csord__Order_Number__c,Owner.Name,Owner.Email from csord__Order__c where Id IN :odrIdset];
            //List of all emails that will be sent
            List<Messaging.SingleEmailMessage> mailToOrderOwnerList = new List<Messaging.SingleEmailMessage>();
            //Email to be sent to the Order Owner
            Messaging.SingleEmailMessage mailToOrderOwner = new Messaging.SingleEmailMessage();
            //Setting the recepient address
            List<String> sendTo = new List<String>();
            sendTo.add(termOrdLst[0].Owner.Email);
            mailToOrderOwner.setToAddresses(sendTo);
            //Setting the address from which the email will be triggered
            mailToOrderOwner.setReplyTo(termOrdLst[0].Owner.Email);
            //mailToOrderOwner.setOrgWideEmailAddressId(Id);
            //Setting the email subject
            mailToOrderOwner.setSubject('Termination Order(s) of ' + termOrdLst[0].Account_ID__c + ' has been created');
            //Setting the CSS styling for the table
            String mailBody='<style type="text/css">'+
            '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
            '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
            '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
            '</style>';
            //Setting the email body -- initial information
            mailBody+='Hi '+termOrdLst[0].Owner.Name+','
            +'<br/>'
            +'<br/>'
            +'The following Termination order(s) has been created.'
            +'<br/>'
            +'<br/>'
            +'Please click on the link below to view the details of the Request.'
            +'<br/>'
            +'<br/>';
            //Setting the email body -- table headers
            mailBody+='<table class="tg">'
            +'<tr>'
            +'<th class="tg-031e"><strong>Order Number</strong></th>'
            +'</tr>';
            //Setting the email body -- table data
            for(csord__Order__c ai :termOrdLst){
                mailBody+='<tr>'
                +'<td class="tg-031e"><a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+ai.Id+'>'+ai.csord__Order_Number__c+'</a></td>'
                +'</tr>';
            }
            mailBody+='</table>'
            +'<br/>'
            +'Thanks'
            +'<br/>'
            +'<br/>'
            +'<br/>'
            +'***This is an auto-generated notification, please do not reply to this email***';
            system.debug('Test Email Panda'+mailBody);
            mailToOrderOwner.setHtmlBody(mailBody);
            mailToOrderOwnerList.add(mailToOrderOwner);
            Messaging.sendEmail(mailToOrderOwnerList);
        } catch(exception e){
        ErrorHandlerException.ExecutingClassName='CreateTerminateOrderP2A:sendOrderEmailtoCreator';
        ErrorHandlerException.objectIdList=odrIdset;
        ErrorHandlerException.sendException(e); 
        System.debug(e);}
    }

    /**
     * Method 6
     * Added to receive exception mail incase of error
     * For debug purpose only
     * Developer: Ritesh
     * Date: 21-feb-2017
     */
    public static void sendExceptionMail(String errormsg){
        //sending logs via mail
        List<messaging.singleEmailMessage>mails = new List<messaging.singleEmailMessage>();
        messaging.singleEmailMessage mail = new messaging.SingleEmailMessage();
        List<String>sendTo = new List<String>();
        List<String>getsenders = label.ExceptionReceiver.split(',');
        for(String email :getSenders){
            system.debug('email'+email);
            sendTo.add(email);
        }
        
        mail.setToAddresses(sendTo);
        String subjectMsg = 'TerminateErrorException in createTerminateOrder';
        mail.setsubject(subjectMsg);
        mail.setPlainTextBody(errormsg);
        mails.add(mail);
        messaging.sendemail(mails);
        //ends here 
    }
}