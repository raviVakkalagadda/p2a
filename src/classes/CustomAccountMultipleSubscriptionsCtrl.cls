public with sharing class CustomAccountMultipleSubscriptionsCtrl {
    
    private final Account account;
    
    //String array to keep track of the ids of choices  
    public List<String> choices {
        get {
            if (choices == null) {
                choices = new List<String>();
            }
            if (previousChoices != null && !previousChoices.isEmpty()) {
                
                choices.addAll(previousChoices);
            }
            return choices;
        } 
        set; 
        
    }
    public Integer pageNumber;
    public Integer subscriptionMaxSize = 100;
    public String title {get;set;}
    public String productBasketTitle {get;set;}
    public String Subnumber{get;set;}
    public string changeType {get;set;}
    public String oppRecordType {get; set;}
    public String processName {get; set;}
    public String dateString {get; set;}
    public List<string> getLists {get;set;}
    public Set<String> previousChoices;
    private csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
    private Map<String, csordtelcoa__Change_Types__c> changeTypeMap = csordtelcoa__Change_Types__c.getAll();
    public List<csord__Subscription__c> subscriptionnumber{get;set;}
    public List<csord__Subscription__c> subscriptions {get;set;}
    private String SubscriptionList;
    
    public CustomAccountMultipleSubscriptionsCtrl(ApexPages.StandardController stdController) {
        choices = new List<String>();
        previousChoices = new Set<String>();
        pageNumber = 0;
        title = 'MACD opportunity ' + Datetime.now().format();
        productBasketTitle = 'MACD basket ' + Datetime.now().format();
        this.account = (Account)stdController.getRecord();
        // Get the Orders & Subscriptions Options custom setting object
        
        if (getInvokeProcess()) {
            processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
        }        
    }
 @testVisible
    private class Change_Types_Wrapper implements Comparable {
        public csordtelcoa__Change_Types__c changeType;
        
        public Change_Types_Wrapper(csordtelcoa__Change_Types__c ct) {
            changeType = ct;
        }
        
        public Integer compareTo(Object compareTo) {
            Change_Types_Wrapper compareToChangeType = (Change_Types_Wrapper)compareTo;
            
            Integer retVal = 0;
            if (changeType.csordtelcoa__Sort_Order__c > compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
                retVal = 1;
            } else if (changeType.csordtelcoa__Sort_Order__c < compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
                retVal = -1;
            }
            return retVal;
        }
    }
    
    public List<SelectOption> getChangeTypes() {
        List<SelectOption> options = new List<SelectOption>();
        List<Change_Types_Wrapper> sortedChangeTypes = new List<Change_Types_Wrapper>();
        
        for (csordtelcoa__Change_Types__c ct : changeTypeMap.values()) {
            sortedChangeTypes.add(new Change_Types_Wrapper(ct));
        }
        
        sortedChangeTypes.sort();
        for (Change_Types_Wrapper ct : sortedChangeTypes) {
            options.add(new SelectOption(ct.changeType.name,ct.changeType.name));
        }
//        for (String key : changeTypeMap.keySet()) {           
//            options.add(new SelectOption(changeTypeMap.get(key).name,changeTypeMap.get(key).name));
//        }
        return options;
    }

    public Boolean getRequestDate() {
        if (changeTypeMap.containsKey(changeType)) {
            return changeTypeMap.get(changeType).csordtelcoa__Request_Date__c;
        } else {
            return false;
        }
    }
    
    public String getRequestedDateLabel() {
        if (changeTypeMap.containsKey(changeType)) {
            return changeTypeMap.get(changeType).csordtelcoa__Requested_Date_Label__c;
        } else {
            return '';
        }
    }

    public Boolean getInvokeProcess() {
        
        if (changeTypeMap.containsKey(changeType)) {
            return changeTypeMap.get(changeType).csordtelcoa__Invoke_Process__c;
        } else {
            return false;
        }
    }

    public PageReference rerenderButtons() {
        if (getInvokeProcess()) {
            processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
        } else {
            processName = '';
        }       
            return null;
    }
    
    public PageReference nextPage() {
        if (subscriptions.size() == subscriptionMaxSize) {
            pageNumber++;
//            setCon.next();
        }
        if (choices != null && !choices.isEmpty()) {
            previousChoices.addAll(choices);
        }
        setCon.next();
        return null;
    }
    
    public String getPreviousChoices() {
        return JSON.serialize(previousChoices);
        //return JSON.serialize(setcon.getSelected());
    }
    
    public PageReference previousPage() {
        if (pageNumber != 0) {
            pageNumber--;
  //          setCon.previous();
        }
        if (choices != null && !choices.isEmpty()) {
            previousChoices.addAll(choices);
        }
        
        setCon.previous();
        return null;
    }
    
    public Boolean getPreviousButton() {
        return pageNumber != 0;
    }
    
    public Boolean getNextButton() {
        return true;
    }

    

    public ApexPages.StandardSetController setCon {
        get {
            if(setCon == null) {
                setCon = new ApexPages.StandardSetController(Database.getQueryLocator(
                    [SELECT Id, Name, CreatedDate,csordtelcoa__Subscription_Number__c, Count_of_services__c, Count_of_Service_not_provisioned__c, csord__Account__c, csord__Status__c,  (Select Name From csordtelcoa__Opportunities__r)
                                                      FROM csord__Subscription__c 
                                                      WHERE csord__Account__c = :account.id 
                                                            AND csord__Status__c NOT IN :getStatusesNotAllowingChange()
                                                            AND csordtelcoa__closed_Replaced__c = false
                                                      ORDER BY Name, CreatedDate, Path_Status__c DESC LIMIT 10000]));
            }
            return setCon;
        }
        set;
    }

    // Initialize setCon and return a list of records
    public List<csord__Subscription__c> getSubscriptionsFromStdSetCon() {
        return (List<csord__Subscription__c>) setCon.getRecords();
    }


    
    
    
    //List of select options to populate select boxes  
    public List<SelectOption> getList() {  
        List<SelectOption> options = new List<SelectOption>();
        List<String> optionsnumber = new List<String>();
        /*if (pageNumber == 0) {
            subscriptions= [SELECT Id, Name, CreatedDate,csordtelcoa__Subscription_Number__c, Path_Status__c, csord__Account__c, csord__Status__c,  (Select Name From csordtelcoa__Opportunities__r)
                                                      FROM csord__Subscription__c 
                                                      WHERE csord__Account__c = :account.id 
                                                            AND csord__Status__c NOT IN :getStatusesNotAllowingChange()
                                                            AND csordtelcoa__closed_Replaced__c = false
                                                      ORDER BY Name, Path_Status__c ASC LIMIT :subscriptionMaxSize];
        } else {
            Integer offsetNumber = pageNumber * subscriptionMaxSize;
            subscriptions= [SELECT Id, Name, CreatedDate,csordtelcoa__Subscription_Number__c, Path_Status__c, csord__Account__c, csord__Status__c,  (Select Name From csordtelcoa__Opportunities__r)
                                                      FROM csord__Subscription__c 
                                                      WHERE csord__Account__c = :account.id 
                                                            AND csord__Status__c NOT IN :getStatusesNotAllowingChange()
                                                            AND csordtelcoa__closed_Replaced__c = false
                                                      ORDER BY Name, Path_Status__c ASC LIMIT :subscriptionMaxSize OFFSET :offsetNumber];
        }*/
        //if(pageNumber != 0)
            //Integer offsetNumber = pageNumber * subscriptionMaxSize;  
        subscriptions = getSubscriptionsFromStdSetCon();
        for (csord__Subscription__c s: subscriptions) {
        
            string status = '';
            
            if(s.Count_of_Service_not_provisioned__c == s.Count_of_services__c){
                status = 'LIVE - AS DESIGNED';
            }else{
                status = 'Not Live';
            }
            
            string name = s.Name + ' - ' + status;
            
            //name += ' - (id:' + s.Id + ')';
            //if (s.Opportunities__r.size() > 0) { name += ' - (related opportunity:' + s.Opportunities__r[0].Name + ')'; }
            options.add(new SelectOption(s.id, name));
             }
         return options; 
        
             }
    
    
    
    public List<SelectOption> getOppRecordTypes() {
        return getOpportunityRecordTypes();
   }
    
    public PageReference createProcesses() {
        if (String.isEmpty(processName)) {
            String warningMessage = 'Please provide a descriptive name for the "'+changeType+'" process you are requesting.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        } else {
            Date aDate;
            try {
                if (dateString != null) {  
                    aDate = date.parse(dateString);
                }
            } catch(System.TypeException ex) {
                string warningMessage = 'Please provide a properly formatted date.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            }
            
            List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c,csordtelcoa__Subscription_Number__c, CreatedDate  FROM csord__Subscription__c WHERE Id in :choices];
            csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
            csordtelcoa.StatusResult result = csordtelcoa.API_V1.createProcess(subscriptions, aChangeType, aDate, processName);

            if (result.status == csordtelcoa.StatusResult.State.SUCCESS) {
                // return the page reference to the new subscription
               // PageReference accountPage = new ApexPages.StandardController(account).view();
               // accountPage.setRedirect(true);  
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.CONFIRM, 'The Order has been started processing,you will be notified with email once order is created successfully in the system.'));
                return null;
                //return accountPage;
            } else if (result.status == csordtelcoa.StatusResult.State.ERROR) {
                string warningMessage = 'Processes creation failed with message: ' +  result.errorMessage;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            } else {
                string warningMessage = 'Processes creation ended unexpectedly. ' +  result.errorMessage;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            }
        }
    }  
    
    public PageReference CreateMultipleSubscriptionMacOpportunity() {
        
        if (String.isBlank(title)) {
            String warningMessage = 'Opportunity title cannot be blank.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        }
        
        //csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate  FROM csord__Subscription__c WHERE Id in :choices];
        /*try {
            
            
            //ID macdJobID = System.enqueueJob(new MacOpportunityFromSubscriptionsJob(choices, changeType, oppRecordType, title)); 
            //ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDOppty', choices, changeType, title, oppRecordType));
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd opportunity batch created! Please check your opprotunity list and look for last macd opportunity you own.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage();     
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating macd opportunity batch. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        }*/
        
         try {
            Id newOpptyId = csordtelcoa.API_V1.createMACOpportunityFromSubscriptions(subscriptions, changeTypeMap.get(changeType), oppRecordType, title, null);

            if (orderSubscriptionsOptions.csordtelcoa__use_batch_mode__c) {
                PageReference pageRef= Page.csordtelcoa__ChangeGenerationProgressBar;
                pageRef.getParameters().put('opportunityId', newOpptyId);
                pageRef.setRedirect(true);
                return pageRef;
            } else {                
                // return the page reference to the new opportunity
                Opportunity newOpportunity = [Select Id FROM Opportunity WHERE id = :newOpptyId];
                PageReference opptyPage = new ApexPages.StandardController(newOpportunity).view();
                opptyPage.setRedirect(true);  
                return opptyPage;
            }
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating new opportunity. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        } 
    }
    
    public PageReference CreateMultipleSubscriptionMacBasket() {
        if(ServiceTerminateorder()) {
            string Error = 'Change Basket cannot be created because a Change Order has already been created for the following selected subscriptions: '+SubscriptionList+'. Please select Subscriptions of the latest revision of the Services.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Error));
            return null;
        }
        
        if (String.isBlank(productBasketTitle)) {
            String warningMessage = 'Product basket title cannot be blank.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        }  
        try {
            
            ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDBasket', choices, changeType, productBasketTitle, null));
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd basket batch created! Please check your basket list (under the account) and look for last macd basket you own.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage();     
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating macd basket batch. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        }
        
             
        /*List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate FROM csord__Subscription__c WHERE Id in :choices];
        csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
        try {
            Id newBasketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(subscriptions, aChangeType, productBasketTitle, null); 
    
            if (orderSubscriptionsOptions.csordtelcoa__use_batch_mode__c) {
                PageReference pageRef= Page.csordtelcoa__ChangeGenerationProgressBar;
                pageRef.getParameters().put('productBasketId', newBasketId);
                pageRef.setRedirect(true);
                return pageRef;
            } else {
                cscfga__Product_Basket__c newBasket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Id = :newBasketId LIMIT 1];
                // return the page reference to the new basket
                PageReference basketPage = new ApexPages.StandardController(newBasket).view();
                basketPage.setRedirect(true);  
                return basketPage;
            }
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating new basket. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        }   */
    }

    private static csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance();

    private static Set<String> statusesNotAllowingChange = null;
     /**
      * Gets the set of all defined statuses not allowing a change as defined in the Statuses_Not_Allowing_Change__c
      * custom settings field. If the list is empty in the custom settings, an empty set is returned. The result is
      * cached in a static field.
      */
        
    public static Set<String> getStatusesNotAllowingChange() {
        if (statusesNotAllowingChange == null) {
            // if the cache is not loaded, fill the values...
            statusesNotAllowingChange = new Set<String>();
            if (!String.isEmpty(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c)) {
                // split cvs list of stages (allowing whitespaces around the comma)
                statusesNotAllowingChange.addAll(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c.split('\\s*,\\s*'));
            }
        }
        return statusesNotAllowingChange;
    }
    
     /**
  * @description Retrieves all record type names available to the current user in the form of a list of strings
  * @return a list of strings each representing a record type name
 */
 public static List<String> getAvailableRecordTypeNamesForSObject(Schema.SObjectType objType) {
     List<String> names = new List<String>();
     List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
     // If there are 2 or more RecordTypes...
     if (infos.size() > 1) {
         for (RecordTypeInfo i : infos) {
            if (i.isAvailable() 
            // Ignore the Master Record Type, whose Id always ends with 'AAA'.
            // We check the Id because Name can change depending on the user's language.
             && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                 names.add(i.getName());
         }
     } 
     // Otherwise there's just the Master record type,
     // so add it in, since it MUST always be available
     else names.add(infos[0].getName());
     return names;
 }
 
 //NC 14/07/2017 T-31974 - method to create MACD Basket 
    //called from VisualforcePage as JavascriptRemoting
    @RemoteAction
    public static String createBasketFromSubscription(String subscription, String changeTypeName, String productBasketTitle){
        system.debug('-----NC createBasketFromSubscription');
        system.debug('----- NC subscription: ' + subscription + ' changeTypeName: ' + changeTypeName);
        Id basketId = null;
        List<csord__Subscription__c> singlesub = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id = :subscription];
        csordtelcoa__Change_Types__c changeType = [SELECT Id, Name,csordtelcoa__Add_To_Existing_Subscription__c, csordtelcoa__Auto_Create_Bundle__c FROM csordtelcoa__Change_Types__c WHERE Name = :changeTypeName];
        
        system.debug('----- NC singlesub: ' + singlesub + ' changeType: ' + changeType + ' productBasketTitle: ' + productBasketTitle);
        basketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(singlesub, changeType, productBasketTitle, null);
        
        return String.valueOf(basketId);
    }
    
    //NC 14/07/2017 T-31974 - add subscriptions to existing basket
    @RemoteAction
    public static String addSubscriptionsToBasket(String basketId, String subscriptionIds2){
        List<String> subscriptionIds = (List<String>) JSON.deserialize(subscriptionIds2, List<String>.class);
        List<Id> subsIds = new List<Id>();
        Id basketId2 = Id.valueOf(basketId);
        
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id IN :subscriptionIds];
        cscfga__Product_Basket__c basket = [SELECT Id, Name, csordtelcoa__Change_Type__c, csordtelcoa__Product_Configuration_Clone_Batch_Job_Id__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
        
        for(csord__Subscription__c sub :subscriptions){
            subsIds.add(sub.Id);
        }
        
        basketId2 = csordtelcoa.API_V1.addSubscriptionsToMacBasket(subsIds, basketId2, true);
        
        return String.valueOf(basketId2);
    } 
    
    public void addMessageToPage(){
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd basket batch created! Please check your basket list (under the account) and look for last macd basket you own.');
            ApexPages.addMessage(msg);
    } 
 
    /**
    * @description Retrieves all record types available to the current user in the form of a list of SelectOption objects. Available record types are further filtered
    * using the custom settings Use_Opportunity_Record_Types__c and MACD_Opportunity_Record_Types__c 
    * @return a list of select options where value is a record type Id and label a record type name
    */
    public static List<SelectOption> getOpportunityRecordTypes() {
        List<SelectOption> options = new List<SelectOption>();
        Map<String,RecordType> oppRecordTypes = new Map<String,RecordType>([Select Id, Name From RecordType Where SobjectType = 'Opportunity']);
 
        csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
        Set<String> macdOppRecordTypes = new Set<String> ();
        if (!string.isBlank(orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c)) {
            macdOppRecordTypes = new Set<String> (orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c.split(',',0));
        }
  
        if (orderSubscriptionsOptions.csordtelcoa__Use_Opportunity_Record_Types__c) {
            List<String> availableRecordTypes = getAvailableRecordTypeNamesForSObject(Opportunity.SObjectType);
            Set<String> availableRecordTypesSet = new Set<string>(availableRecordTypes);
         
            for (String key : oppRecordTypes.keySet()) {
                if (availableRecordTypesSet.contains(oppRecordTypes.get(key).Name) && (macdOppRecordTypes.size() == 0 || macdOppRecordTypes.contains(oppRecordTypes.get(key).Name))) {
                    options.add(new SelectOption(key,oppRecordTypes.get(key).name));
                }
            }         
        } else {
            List<String> defaultRecordType = getDefaultRecordTypeNameForSObject(Opportunity.SObjectType);
        }
        
        return options;
    }
    
     /**
      * @description Retrieves the default record type name available to the current user in the form of a list with a single item
      * @return a single string that is the default record type name for the given object schema
     */
    @testVisible
    private static List<String> getDefaultRecordTypeNameForSObject(Schema.SObjectType objType) {
        List<String> names = new List<String>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        
        // If there are 2 or more RecordTypes...
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                if (i.isDefaultRecordTypeMapping())
                    names.add(i.getName());
            }
        } else if (infos.size() == 1) {
            // Otherwise there's just the Master record type,
            // so add it in, since it MUST always be available
            names.add(infos[0].getName());
        } else {
            //there are no record types for this SObject
        }
     
        return names;
    }
    
    public void next(){
        if (choices != null && !choices.isEmpty()) {
            previousChoices.addAll(choices);
        }
        setCon.next();
    }
    
    public void previous(){
        if (choices != null && !choices.isEmpty()) {
            previousChoices.addAll(choices);
        }
        setCon.Previous();
    }
    
    /**
     * @Pavan@
     * TGP0041766: P2A order with unnecessary termination items. 
     */
    public boolean ServiceTerminateorder(){
        boolean isService = False;
        Set<Id> subs = new Set<Id>();
        //List<csord__Service__c> srvcList = [Select Id, AccountId__c, csord__Subscription__c, csord__Subscription__r.Name, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c IN :choices];
        for(csord__Service__c srvc :[Select Id, AccountId__c, csord__Subscription__c, csord__Subscription__r.Name, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c IN :choices]) {
            if(srvc.csordtelcoa__Replacement_Service__c != null) {
                if(SubscriptionList == null){
                    SubscriptionList = srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);                  
                }
                else if(!subs.contains(srvc.csord__Subscription__c)){
                    SubscriptionList = SubscriptionList + srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);
                }
                isService = True;               
            }
        }
        return isService;
    } 
    
}