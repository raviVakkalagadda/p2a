global virtual with sharing class CustomButtonMultiEdit extends csbb.CustomButtonExt {
    public String performAction(String basketId)
    {
        id uid = userinfo.getUserId();
        id pid = userinfo.getProfileId();
        cscfga__Product_Basket__c baskets = [select id,csbb__Account__c, cscfga__Opportunity__c, csbb__Account__r.Customer_Type_New__c, Account_Owner_ID__c, Opportunity_Owner_ID__c from cscfga__Product_Basket__c where id =: basketId];
        string OppTeamOpp = baskets.cscfga__Opportunity__c;
        List<OpportunityTeamMember> OppTeam = [SELECT Id,Name,TeamMemberRole,UserId FROM OpportunityTeamMember WHERE OpportunityId =: OppTeamOpp];
        
        if(uid != baskets.Account_Owner_ID__c && uid != baskets.Opportunity_Owner_ID__c && pid != label.Profile_System_Administrator && pid != label.Profile_TI_Terminate_Team && pid != label.Profile_TI_Account_Manager && pid != label.Profile_TI_Sales_Admin && pid != label.Profile_TI_Order_Desk)
        {
            return '{"status":"error","title":"Error","text":"You are not authorized to request this action"}';
        }
        PageReference newUrl = new PageReference('/apex/c__ProductChooser?basketId={0}'); //c__ redirects to unpackaged host namespace https://developer.salesforce.com/forums/?id=906F000000094trIAA
        newUrl.getParameters().put('basketId', basketId);
        System.Debug(newURL);
        return '{"status":"ok","redirectURL":"' + newUrl.getUrl() + '", "text":"Redirecting to ProductChooser page..."}';
    }

}