	public with sharing class CustomCreateChangeOrderController {

		private final csord__Subscription__c subscription;
		public String changeType {get; set;}
		public String oppRecordType {get; set;}
		public String processName {get; set;}
		public String dateString {get; set;}
		private String ServiceList;
	   
		// Get the Orders & Subscriptions Options custom setting object
		//for @Testvisible modified by Anuradha on 2/20/2017
		@Testvisible
		private csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
		private Map<String, csordtelcoa__Change_Types__c> changeTypeMap = csordtelcoa__Change_Types__c.getAll();
		
		// The extension constructor initializes the private member
		// variable subscription by using the getRecord method from the standard
		// controller.
		public CustomCreateChangeOrderController (ApexPages.StandardController stdController) {
			this.subscription = (csord__Subscription__c)stdController.getRecord();
			if (this.subscription.csordtelcoa__Closed_Replaced__c) {
				String warningMessage = 'You can not perform a change on this subscription as it has has been replaced by the subscription shown below.';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
			} else if (!getAllowChange()) {
				String warningMessage = 'You can not perform a change on this subscription due to the status it has reached.';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
			}
			if (getInvokeProcess()) {
				processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
			}
		}

		//for @Testvisible modified by Anuradha on 2/20/2017
		@Testvisible
		private class Change_Types_Wrapper implements Comparable {
			public csordtelcoa__Change_Types__c  changeType;
			
			public Change_Types_Wrapper(csordtelcoa__Change_Types__c  ct) {
				changeType = ct;
			}
			
			public Integer compareTo(Object compareTo) {
				Change_Types_Wrapper compareToChangeType = (Change_Types_Wrapper)compareTo;
				
				Integer retVal = 0;
				if (changeType.csordtelcoa__Sort_Order__c > compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
					retVal = 1;
				} else if (changeType.csordtelcoa__Sort_Order__c < compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
					retVal = -1;
				}
				return retVal;
			}
		}
		
		public List<SelectOption> getChangeTypes() {
			List<SelectOption> options = new List<SelectOption>();
			List<Change_Types_Wrapper> sortedChangeTypes = new List<Change_Types_Wrapper>();
			
			for (csordtelcoa__Change_Types__c  ct : changeTypeMap.values()) {
				sortedChangeTypes.add(new Change_Types_Wrapper(ct));
			}
			
			sortedChangeTypes.sort();
			for (Change_Types_Wrapper ct : sortedChangeTypes) {
				options.add(new SelectOption(ct.changeType.name,ct.changeType.name));
			}
	//        for (String key : changeTypeMap.keySet()) {           
	//            options.add(new SelectOption(changeTypeMap.get(key).name,changeTypeMap.get(key).name));
	//        }
			return options;
		}

		public Boolean getRequestDate() {
			if (changeTypeMap.containsKey(changeType)) {
				return changeTypeMap.get(changeType).csordtelcoa__Request_Date__c;
			} else {
				return false;
			}
		}
		
		public String getRequestedDateLabel() {
			if (changeTypeMap.containsKey(changeType)) {
				return changeTypeMap.get(changeType).csordtelcoa__Requested_Date_Label__c;
			} else {
				return '';
			}
		}

		public Boolean getInvokeProcess() {
			
			if (changeTypeMap.containsKey(changeType)) {
				return changeTypeMap.get(changeType).csordtelcoa__Invoke_Process__c;
			} else {
				return false;
			}
		}

		public PageReference rerenderButtons() {
			if (getInvokeProcess()) {
				processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
			} else {
				processName = '';
			}       
				return null;
		}

		public PageReference createProcess() {
			if (String.isEmpty(processName)) {
				String warningMessage = 'Please provide a descriptive name for the "'+changeType+'" process you are requesting.';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
				return null;
			} else {
				List<csord__Subscription__c> subscriptions = new List<csord__Subscription__c> {subscription};
				
				Date aDate;
				try {
					if (dateString != null) {  
						aDate = date.parse(dateString);
					}
				} catch(System.TypeException ex) {
					string warningMessage = 'Please provide a properly formatted date.';
					ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
					return null;
				}           
				
				csordtelcoa.API_V1.createProcess(subscriptions, changeTypeMap.get(this.changeType), aDate, processName);

				// return the page reference to the new subscription
				PageReference subscriptionPage = new ApexPages.StandardController(subscription).view();
				subscriptionPage.setRedirect(true);  
				return subscriptionPage;
			}
		}
		//for @Testvisible modified by Anuradha on 2/20/2017
		@Testvisible
		private static csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance();
		//for @Testvisible modified by Anuradha on 2/20/2017
		@Testvisible
		private static Set<String> statusesNotAllowingChange = null;
		
		/**
		* Gets the set of all defined statuses not allowing a change as defined in the Statuses_Not_Allowing_Change__c
		* custom settings field. If the list is empty in the custom settings, an empty set is returned. The result is
		* cached in a static field.
		*/
		public static Set<String> getStatusesNotAllowingChange() {
			if (statusesNotAllowingChange == null) {
				// if the cache is not loaded, fill the values...
				statusesNotAllowingChange = new Set<String>();
				if (!String.isEmpty(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c)) {
					// split cvs list of stages (allowing whitespaces around the comma)
					statusesNotAllowingChange.addAll(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c.split('\\s*,\\s*'));
				}
			}
			return statusesNotAllowingChange;
		}
		
		public Boolean getAllowChange() {
			Boolean allowSubscriptionChange = 
				!this.subscription.csordtelcoa__Closed_Replaced__c && 
				!getStatusesNotAllowingChange().contains(this.subscription.csord__Status__c);

			return allowSubscriptionChange;
		}

		/**
		* @description Retrieves all record type names available to the current user in the form of a list of strings
		* @return a list of strings each representing a record type name
		*/
		public static List<String> getAvailableRecordTypeNamesForSObject(Schema.SObjectType objType) {
			List<String> names = new List<String>();
			List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
			// If there are 2 or more RecordTypes...
			if (infos.size() > 1) {
				for (RecordTypeInfo i : infos) {
					if (i.isAvailable() 
					// Ignore the Master Record Type, whose Id always ends with 'AAA'.
					// We check the Id because Name can change depending on the user's language.
					 && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
						 names.add(i.getName());
				}
			} 
			// Otherwise there's just the Master record type,
			// so add it in, since it MUST always be available
			else names.add(infos[0].getName());
			return names;
		}


		/**
		* @description Retrieves all record types available to the current user in the form of a list of SelectOption objects. Available record types are further filtered
		* using the custom settings Use_Opportunity_Record_Types__c and MACD_Opportunity_Record_Types__c 
		* @return a list of select options where value is a record type Id and label a record type name
		*/
		public static List<SelectOption> getOpportunityRecordTypes() {
			List<SelectOption> options = new List<SelectOption>();
			Map<String,RecordType> oppRecordTypes = new Map<String,RecordType>([Select Id, Name From RecordType Where SobjectType = 'Opportunity']);
	 
			csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
			Set<String> macdOppRecordTypes = new Set<String> ();
			if (!string.isBlank(orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c)) {
				macdOppRecordTypes = new Set<String> (orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c.split(',',0));
			}
	  
			if (orderSubscriptionsOptions.csordtelcoa__Use_Opportunity_Record_Types__c) {
				List<String> availableRecordTypes = getAvailableRecordTypeNamesForSObject(Opportunity.SObjectType);
				Set<String> availableRecordTypesSet = new Set<string>(availableRecordTypes);
			 
				for (String key : oppRecordTypes.keySet()) {
					if (availableRecordTypesSet.contains(oppRecordTypes.get(key).Name) && (macdOppRecordTypes.size() == 0 || macdOppRecordTypes.contains(oppRecordTypes.get(key).Name))) {
						options.add(new SelectOption(key,oppRecordTypes.get(key).name));
					}
				}         
			} else {
				List<String> defaultRecordType = getDefaultRecordTypeNameForSObject(Opportunity.SObjectType);
			}
			
			return options;
		}
		
		/**
		  * @description Retrieves the default record type name available to the current user in the form of a list with a single item
		  * @return a single string that is the default record type name for the given object schema
		 */
		//for @Testvisible modified by Anuradha on 2/20/2017
		@Testvisible
		private static List<String> getDefaultRecordTypeNameForSObject(Schema.SObjectType objType) {
			List<String> names = new List<String>();
			List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
			
			// If there are 2 or more RecordTypes...
			if (infos.size() > 1) {
				for (RecordTypeInfo i : infos) {
					if (i.isDefaultRecordTypeMapping())
						names.add(i.getName());
				}
			} else if (infos.size() == 1) {
				// Otherwise there's just the Master record type,
				// so add it in, since it MUST always be available
				names.add(infos[0].getName());
			} else {
				//there are no record types for this SObject
			}
		 
			return names;
		}
		
		public List<SelectOption> getOppRecordTypes() {
			return getOpportunityRecordTypes();
		}
		
		public PageReference CreateMacOpportuntiyFromSubscription() {
			//List<csord__Subscription__c> subscriptions = new List<csord__Subscription__c> {this.subscription};
			/* String selectedRecordType = null;
			if (oppRecordType != null) {
				String oppFieldString = Utility.getSobjectFields('Opportunity');
				Set<String> oppFieldSet = new Set<String>(oppFieldString.split(','));
				if (oppFieldSet.contains('RecordTypeId')) selectedRecordType = oppRecordType;
			} */
			datetime myDateTime = Datetime.now();
			String myDate = myDateTime.format('yyyyMMdd');      
			String opportunityName = this.subscription.Name + ' (MAC on ' + myDate + ')';

			try {
				//ID macdJobID = System.enqueueJob(new MacOpportunityFromSubscriptionsJob(choices, changeType, oppRecordType, title)); 
				ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDOppty', new List<String> { this.subscription.Id }, changeType, opportunityName, oppRecordType));
				system.debug('MacdProcessFromSubscriptionsJob' +macdJobID );

				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
					, 'Macd opportunity batch created! Please check your opprotunity list and look for last macd opportunity you own.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage();     
			} catch(csordtelcoa.TelecomsModuleException ex) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
					, 'Exception occured while creating macd opportunity batch. Please try again. If the issue persists, contact your administrator.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage(); 
			}

			
			/* try {

				Id newOpportunityId = MultipleSubscriptionsManager.createMacOpportunityFromSubscriptions(subscriptions, changeTypeMap.get(this.changeType), selectedRecordType,opportunityName, null);
				Opportunity newOpportunity = [SELECT Id, Product_Configuration_Clone_Batch_Job_Id__c FROM Opportunity WHERE Id = :newOpportunityId LIMIT 1];
		

				if (orderSubscriptionsOptions.use_batch_mode__c) {
					PageReference pageRef= Page.ChangeGenerationProgressBar;
					pageRef.getParameters().put('opportunityId', newOpportunity.Id);
					pageRef.setRedirect(true);
					return pageRef;
				} else {                
					// return the page reference to the new opportunity

					PageReference opptyPage = new ApexPages.StandardController(newOpportunity).view();
					opptyPage.setRedirect(true);  
					return opptyPage;
				}
			} catch(TelecomsModuleException ex) {

				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Exception occured while creating new opportunity. Please try again. If the issue persists, contact your administrator.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage(); 
			} */

		}

		public PageReference CreateMacBasketFromSubscription() {
			//List<csord__Subscription__c> subscriptions = new List<csord__Subscription__c> {this.subscription};
			if(ServiceTerminateorder()) {
				string Error = 'Change Basket cannot be created because a Change Order has already been created for the following selected subscriptions: '+ServiceList+'. Please select Subscriptions of the latest revision of the Services.';
				ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Error));
				return null;
			}
			
			String basketName = 'Mac basket: ' + datetime.now();
			
			try {

				ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDBasket', new List<String> { this.subscription.Id }, changeType, basketName ,null));
				
				csord__Subscription__c subs = new csord__Subscription__c(Id=this.subscription.Id,MACD_Batch_Job_Id__c=macdJobID);
				TriggerFlags.NoSubscriptionTriggers = true;
				TriggerFlags.NoServiceTriggers = true;
				TriggerFlags.NoOrderTriggers = true;
				update subs;
				TriggerFlags.NoSubscriptionTriggers = false;
				TriggerFlags.NoServiceTriggers = false;
				TriggerFlags.NoOrderTriggers = false;
				
				return new PageReference('/apex/MACDProgressBar?Id=' +this.subscription.Id);//controller.view();
				
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
					, 'Macd basket batch created! Please check your basket list (under the account) and look for last macd basket you own.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage();     
			} catch(csordtelcoa.TelecomsModuleException ex) {
				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
					, 'Exception occured while creating macd basket batch. Please try again. If the issue persists, contact your administrator.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage(); 
			}

			
			/* try {

				Id newBasketId = MultipleSubscriptionsManager.createMacBasketFromSubscriptions(subscriptions, changeTypeMap.get(this.changeType), basketName, null);
				
				cscfga__Product_Basket__c newBasket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Id = :newBasketId LIMIT 1];
		
				if (orderSubscriptionsOptions.use_batch_mode__c) {
					PageReference pageRef= Page.ChangeGenerationProgressBar;
					pageRef.getParameters().put('productBasketId', newBasket.Id);
					pageRef.setRedirect(true);
					return pageRef;
				} else {

					// return the page reference to the new basket
					PageReference basketPage = new ApexPages.StandardController(newBasket).view();
					basketPage.setRedirect(true);  
					return basketPage;
				}
			} catch(TelecomsModuleException ex) {

				ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error, 'Exception occured while creating new basket. Please try again. If the issue persists, contact your administrator.');
				ApexPages.addMessage(msg);  
				return ApexPages.currentPage(); 
			} */

				
		}
		
		/*
		* @Pavan@
		* TGP0041766:P2A order with unnecessary termination items. 
		*/
		public boolean ServiceTerminateorder(){
			boolean isService = False;
			Set<Id> subs = new Set<Id>();
			//List<csord__Service__c> srvcList = [Select Id, Name, AccountId__c, csord__Subscription__c, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c =:this.subscription.Id];
			for(csord__Service__c srvc :[Select Id, Name, AccountId__c, csord__Subscription__c, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c =:this.subscription.Id]) {
				if(srvc.csordtelcoa__Replacement_Service__c != null){
					if(ServiceList == null){
						ServiceList = srvc.Name +', ';
						subs.add(srvc.Id);              
					}
					else if(!subs.contains(srvc.Id)){
						ServiceList = ServiceList + srvc.Name +', ';
						subs.add(srvc.Id);
					}
					isService = True;               
				}
			}
			return isService;
		}  
		
	}