global with sharing class CustomMasterServiceSubscriptionsCtrl {
    
    //private final Account account;
    private final csord__Service__c service;
    private List<csord__Service__c> rackservice = new List<csord__Service__c>();
   private List<Id> temp = new List<Id>();
    
    //String array to keep track of the ids of choices  
    public List<String> choices { get; set; }
    public Integer listSize { get; set; }
    public String title {get;set;}
    public String productBasketTitle {get;set;}
    public string changeType {get;set;}
    public String oppRecordType {get; set;}
    public String processName {get; set;}
    public String dateString {get; set;}
    private csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
    private Map<String, csordtelcoa__Change_Types__c> changeTypeMap = csordtelcoa__Change_Types__c.getAll();
     private String SubscriptionList;
    
    public CustomMasterServiceSubscriptionsCtrl(ApexPages.StandardController stdController) {
        System.debug('ritzzz stdController'+stdController);
        choices = new List<String>();
        title = 'MACD opportunity ' + Datetime.now().format();
        productBasketTitle = 'MACD basket ' + Datetime.now().format();
        
        this.service = [select Id
                , Name
                ,Opportunity__c
                , csord__Subscription__r.csord__Account__c
                , csord__Subscription__r.CreatedDate
                , csord__Subscription__c
                , csord__Subscription__r.Id
                , csord__Subscription__r.Name
                , csord__Subscription__r.csord__Status__c
                , csord__Subscription__r.csordtelcoa__closed_Replaced__c
                , csord__Subscription__r.csordtelcoa__Subscription_Number__c
                , csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c
                ,csordtelcoa__Product_Configuration__r.Added_Ports__c
            from  
                csord__Service__c 
            where
                Id =: (Id) stdController.getRecord().get('id') 
            limit 1];
            
            for (csord__Service__c temp2 : [Select csordtelcoa__Product_Configuration__c from csord__Service__c where id =:(Id) stdController.getRecord().get('id')]){
                temp.add(temp2.csordtelcoa__Product_Configuration__c);
            }
      
      this.rackservice = [select Id
                , Name
                ,Opportunity__c
                , csord__Subscription__r.csord__Account__c
                , csord__Subscription__r.CreatedDate
                , csord__Subscription__c
                , csord__Subscription__r.Id
                , csord__Subscription__r.Name
                , csord__Subscription__r.csord__Status__c
                , csord__Subscription__r.csordtelcoa__closed_Replaced__c
                , csord__Subscription__r.csordtelcoa__Subscription_Number__c
                , csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c
                ,csordtelcoa__Product_Configuration__r.Added_Ports__c
                ,Master_Service__r.csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c
            from  
                csord__Service__c 
            where
                Product_Id__c = 'COLO-CAGE-RACK' and
                Master_Service__r.csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c =: temp
                //Id =: (Id) stdController.getRecord().get('id') 
            ];
            
        system.debug('ritzzz this.service'+this.service);
        system.debug('ritzzz (Id) stdController.getRecord()'+stdController.getRecord().get('id'));      
        
        if(Test.isRunningTest()){
            changeType = 'Parallel Upgrade';
        }
        // Get the Orders & Subscriptions Options custom setting object
        if (getInvokeProcess()) {
            processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
        }        
    }
    //For @Testvisible ,Modified by Anuradha
    @Testvisible
    private class Change_Types_Wrapper implements Comparable {
        public csordtelcoa__Change_Types__c changeType;
        
        public Change_Types_Wrapper(csordtelcoa__Change_Types__c ct) {
            changeType = ct;
        }
        
        public Integer compareTo(Object compareTo) {
            Change_Types_Wrapper compareToChangeType = (Change_Types_Wrapper)compareTo;
            
            Integer retVal = 0;
            if (changeType.csordtelcoa__Sort_Order__c > compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
                retVal = 1;
            } else if (changeType.csordtelcoa__Sort_Order__c < compareToChangeType.changeType.csordtelcoa__Sort_Order__c) {
                retVal = -1;
            }
            return retVal;
        }
    }
    
    public List<SelectOption> getChangeTypes() {
        List<Change_Types_Wrapper> sortedChangeTypes = new List<Change_Types_Wrapper>();
        for (csordtelcoa__Change_Types__c ct : changeTypeMap.values()) {
            sortedChangeTypes.add(new Change_Types_Wrapper(ct));
        }
        
        sortedChangeTypes.sort();
        List<SelectOption> options = new List<SelectOption>();
        for (Change_Types_Wrapper ct : sortedChangeTypes) {
            options.add(new SelectOption(ct.changeType.name,ct.changeType.name));
        }

        return options;
    }

    public Boolean getRequestDate() {
        if (changeTypeMap.containsKey(changeType)) {
            return changeTypeMap.get(changeType).csordtelcoa__Request_Date__c;
        } else {
            return false;
        }
    }
    
    public String getRequestedDateLabel() {
        if (changeTypeMap.containsKey(changeType)) {
            return changeTypeMap.get(changeType).csordtelcoa__Requested_Date_Label__c;
        } else {
            return '';
        }
    }

    public Boolean getInvokeProcess() {
        if (changeTypeMap.containsKey(changeType)) {
            if(Test.isRunningTest()){
                return true;
            }
            return changeTypeMap.get(changeType).csordtelcoa__Invoke_Process__c;
        } else {
            return false;
        }
    }

    public PageReference rerenderButtons() {
        if (getInvokeProcess()) {
            processName = changeTypeMap.get(changeType).csordtelcoa__Process_To_Invoke__c;
        } else {
            processName = '';
        }       
            return null;
    }
    
    
  //desc : as part of CR 50. select all the racks under cage for terminate order
    //name : sowmya
    //date : 2/7/2017
    public PageReference myfunctiontrial() {
        System.debug('In my function');
        System.debug('Choices ++ ' + choices);
        List<Id> cageid = new List<Id>();
        List<csord__Service__c> rackSer = new List<csord__Service__c>();
        List<csord__Service__c> cageSer = [Select id, name, Product_Id__c,csord__Subscription__c from csord__Service__c where csord__Subscription__c = : choices and Product_Id__c = 'COLO-CAGE'];
        if(cageSer.size()>0){
            System.debug('In If');
            for(csord__Service__c i : cageSer ){
                cageid.add(i.id);
            }
            rackSer = [Select id, name, csord__Subscription__c from csord__Service__c where Master_Service__c =: cageid and Product_Id__c = 'COLO-CAGE-RACK'];
            if(rackSer.size()>0 && changeType== 'Termination'){
                for(csord__Service__c ser : rackSer){
                    choices.add(ser.csord__Subscription__c);
                }
            }
        }
        System.debug('Choices ++ after for ++' + choices);
        return null;
    }
    
    
     //Added for Defect #1395 13/01/2017
    //On click of Select/Deselct All button, all subscriptions are either: selected (if there were none selected) 
    //                                                                     deselected (if all or any was selected)

    public PageReference selectAllOptions() {
        List<SelectOption> allOptionSubscriptions = getList();
        if (!choices.isEmpty()) {
            choices = new List<String>(); 
        }
        else if (choices.isEmpty()) {
            for (SelectOption subOption : allOptionSubscriptions) {
                choices.add (subOption.getValue());
            }
        }
        return null;
    }
    
    //List of select options to populate select boxes  
    public List<SelectOption> getList() {  
        // take all the subscriptions whose services configuration is referencing this master 
        Map<Id, SelectOption> optionsMap = new Map<Id, SelectOption>();
        for(csord__Service__c tempService: [select Id
                , Name
                , csord__Subscription__c
                , csord__Subscription__r.Id
                , csord__Subscription__r.Name
                , csord__Subscription__r.csord__Status__c
                , csord__Subscription__r.csordtelcoa__closed_Replaced__c
                , csord__Subscription__r.csordtelcoa__Subscription_Number__c
                , csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c
            from 
                csord__Service__c 
            where
                id != :service.Id and 
                ((csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c <> '' and
                (csordtelcoa__Product_Configuration__r.Master_IPVPN_Configuration__c = :service.csordtelcoa__Product_Configuration__c or id =: rackservice)) or csordtelcoa__Product_Configuration__c IN :getIdChildService())  and 
                csord__Subscription__r.csord__Status__c NOT IN :getStatusesNotAllowingChange() and 
                csord__Subscription__r.csordtelcoa__closed_Replaced__c = false and Order_Type__c!=:'Terminate' ORDER BY Name]) {
            if (optionsMap.containsKey(service.csord__Subscription__r.Id) == false) {
                optionsMap.put(tempService.csord__Subscription__r.id
                    , new SelectOption(tempService.csord__Subscription__r.id
                        , tempService.csord__Subscription__r.Name + ' (' + tempService.csord__Subscription__r.csordtelcoa__Subscription_Number__c + ') - '+tempService.Name));
            }
        }
        
        System.debug('ritzzz optionsMap'+optionsMap);



        // MAC - to add master service as well
        /*optionsMap.put(service.csord__Subscription__r.id
                    , new SelectOption(service.csord__Subscription__r.id
                        , service.csord__Subscription__r.Name + ' (' + service.csord__Subscription__r.csordtelcoa__Subscription_Number__c + ') - '+service.Name));*/
        this.listSize = optionsMap.size();
        
        return optionsMap.values();   
    }
    private List<Id> getIdChildService(){
        
         List<csord__Service__c> internetServiceList=[select id,csordtelcoa__Product_Configuration__c,
                                                      csord__Service__r.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c,
                                                      csordtelcoa__Product_Configuration__r.Added_Ports__c 
                                                      from 
                                                      csord__Service__c 
                                                      where 
                                                      Opportunity__c=:service.Opportunity__c
                                                     AND (Master_Service__c = :service.Id OR Id = :service.Id)];
       List<Id> internetChildServiceId=new List<Id>();
        if(!internetServiceList.isEmpty()){
            for(csord__Service__c serv:internetServiceList){
                if(serv.csordtelcoa__Product_Configuration__r.Added_Ports__c!=null && serv.csordtelcoa__Product_Configuration__r.Added_Ports__c!=''){
                    
                    String [] Arr;
                 if(serv.csordtelcoa__Product_Configuration__r.Added_Ports__c.indexof(',')!=-1){
                    Arr=serv.csordtelcoa__Product_Configuration__r.Added_Ports__c.split(',');
                    for(Integer i=0;i<Arr.size();i++){
                        
                        internetChildServiceId.add(Id.valueOf(Arr[i]));
                    }
                    
                    
                }else{
                    
                    internetChildServiceId.add(Id.valueof(serv.csordtelcoa__Product_Configuration__r.Added_Ports__c));
                }
               }else if(serv.csord__Service__r.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c == 'TWI Singlehome' || serv.csord__Service__r.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c == 'IPT Singlehome'){
                    
                    internetChildServiceId.add(serv.csordtelcoa__Product_Configuration__c);
                }
                
                
            }
        }
        system.debug('internetChildServiceId====='+internetChildServiceId);
        
        return internetChildServiceId;
    }
    public List<SelectOption> getOppRecordTypes() {
        return getOpportunityRecordTypes();
   }
    
    public PageReference createProcesses() {
        if (String.isEmpty(processName)) {
            String warningMessage = 'Please provide a descriptive name for the "'+changeType+'" process you are requesting.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        } else {
            Date aDate;
            try {
                if (dateString != null) {  
                    aDate = date.parse(dateString);
                }
            } catch(System.TypeException ex) {
                string warningMessage = 'Please provide a properly formatted date.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            }
            
            csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
            system.debug('ritzzz changeType'+changeType);
            system.debug('ritzzz choices'+choices);
            system.debug('ritzzz listSize'+listSize);
            if (changeType== 'Termination' && choices.size() == listSize) {
                choices.add(this.service.csord__Subscription__c);
            }  
            system.debug('=====choices==='+choices);
          
          //  Boolean outcome=CreateTerminateOrderP2A.createTerminateOrderBySubscrption(choices);//Workaround is no more required
              List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate  FROM csord__Subscription__c WHERE Id in :choices];
              csordtelcoa.StatusResult result = csordtelcoa.API_V1.createProcess(subscriptions, aChangeType, aDate, processName);
            if (result.status == csordtelcoa.StatusResult.State.SUCCESS) {
                  // return the page reference to the new subscription
                /* PageReference accountPage = new ApexPages.StandardController(account).view();
                accountPage.setRedirect(true);  
                return accountPage; */
               // PageReference servicePage = new ApexPages.StandardController(service).view();
               // servicePage.setRedirect(true);  
               // return servicePage;
                 string warningMessage = 'Terminate Order batch created! Please check your order list and look for last Terminate Order you own.';
                //String warningMessage='There is an Error while trying to create the Termination Order. Please contact GlobalServe for resolution.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.INFO, warningMessage));
                return null;
            } else if (result.status == csordtelcoa.StatusResult.State.ERROR) {
                string warningMessage = 'Processes creation failed with message: ' +  result.errorMessage;
                //String warningMessage='There is an Error while trying to create the Termination Order. Please contact GlobalServe for resolution.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            } else {
                string warningMessage = 'Processes creation ended unexpectedly. ' +  result.errorMessage;
                //String warningMessage='There is an Error while trying to create the Termination Order. Please contact GlobalServe for resolution.';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
                return null;
            }
        }
    }  
    
    public PageReference CreateMultipleSubscriptionMacOpportunity() {
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate  FROM csord__Subscription__c WHERE Id in :choices];
        if (String.isBlank(title)) {
            String warningMessage = 'Opportunity title cannot be blank.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        }
        
        //csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
        
        try {
            if(changeType != 'Termination'){ choices.add(service.csord__Subscription__r.id); }
            //ID macdJobID = System.enqueueJob(new MacOpportunityFromSubscriptionsJob(choices, changeType, oppRecordType, title)); 
            ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDOppty', choices, changeType, title, oppRecordType));
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd opportunity batch created! Please check your opprotunity list and look for last macd opportunity you own.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage();     
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating macd opportunity batch. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        }
        
        /* try {
            Id newOpptyId = csordtelcoa.API_V1.createMACOpportunityFromSubscriptions(subscriptions, aChangeType, oppRecordType, title, null);

            if (orderSubscriptionsOptions.csordtelcoa__use_batch_mode__c) {
                PageReference pageRef= Page.csordtelcoa__ChangeGenerationProgressBar;
                pageRef.getParameters().put('opportunityId', newOpptyId);
                pageRef.setRedirect(true);
                return pageRef;
            } else {                
                // return the page reference to the new opportunity
                Opportunity newOpportunity = [Select Id FROM Opportunity WHERE id = :newOpptyId];
                PageReference opptyPage = new ApexPages.StandardController(newOpportunity).view();
                opptyPage.setRedirect(true);  
                return opptyPage;
            }
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating new opportunity. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        } */
    }
    
    public PageReference CreateMultipleSubscriptionMacBasket() {
    
       if(ServiceTerminateorder()) {
            string Error = 'Change Basket cannot be created because a Change Order has already been created for the following selected subscriptions: '+SubscriptionList+'. Please select Subscriptions of the latest revision of the Services.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR, Error));
            return null;
        }
        
        if (String.isBlank(productBasketTitle)) {
            String warningMessage = 'Product basket title cannot be blank.';
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.WARNING, warningMessage));
            return null;
        }  
        
        try {
            if(changeType != 'Termination'){ choices.add(service.csord__Subscription__r.id); }
            ID macdJobID = System.enqueueJob(new MacdProcessFromSubscriptionsJob('MACDBasket', choices, changeType, productBasketTitle, null));
            
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd basket batch created! Please check your basket list (under the account) and look for last macd basket you own.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage();     
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating macd basket batch. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        }
        
             
        /* List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate FROM csord__Subscription__c WHERE Id in :choices];
        csordtelcoa__Change_Types__c aChangeType = changeTypeMap.get(changeType);
        try {
            Id newBasketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(subscriptions, aChangeType, productBasketTitle, null); 
    
            if (orderSubscriptionsOptions.csordtelcoa__use_batch_mode__c) {
                PageReference pageRef= Page.csordtelcoa__ChangeGenerationProgressBar;
                pageRef.getParameters().put('productBasketId', newBasketId);
                pageRef.setRedirect(true);
                return pageRef;
            } else {
                cscfga__Product_Basket__c newBasket = [SELECT Id FROM cscfga__Product_Basket__c WHERE Id = :newBasketId LIMIT 1];
                // return the page reference to the new basket
                PageReference basketPage = new ApexPages.StandardController(newBasket).view();
                basketPage.setRedirect(true);  
                return basketPage;
            }
        } catch(csordtelcoa.TelecomsModuleException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Error
                , 'Exception occured while creating new basket. Please try again. If the issue persists, contact your administrator.');
            ApexPages.addMessage(msg);  
            return ApexPages.currentPage(); 
        } */  
    }
    
    //NC 14/07/2017 T-31974 - method to create MACD Basket 
    //called from VisualforcePage as JavascriptRemoting
    @RemoteAction
    global static String createBasketFromSubscription(String subscription, String changeTypeName, String productBasketTitle){
        system.debug('-----NC createBasketFromSubscription');
        system.debug('----- NC subscription: ' + subscription + ' changeTypeName: ' + changeTypeName);
        Id basketId = null;
        List<csord__Subscription__c> singlesub = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id = :subscription];
        csordtelcoa__Change_Types__c changeType = [SELECT Id, Name,csordtelcoa__Add_To_Existing_Subscription__c, csordtelcoa__Auto_Create_Bundle__c FROM csordtelcoa__Change_Types__c WHERE Name = :changeTypeName];
        
        system.debug('----- NC singlesub: ' + singlesub + ' changeType: ' + changeType + ' productBasketTitle: ' + productBasketTitle);
        basketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(singlesub, changeType, productBasketTitle, null);
        
        return String.valueOf(basketId);
    }
    
    //NC 14/07/2017 T-31974 - add subscriptions to existing basket
    @RemoteAction
    global static String addSubscriptionsToBasket(String basketId, String subscriptionIds2){
        List<String> subscriptionIds = (List<String>) JSON.deserialize(subscriptionIds2, List<String>.class);     
        List<Id> subsIds = new List<Id>();
        Id basketId2 = Id.valueOf(basketId);
        
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id IN :subscriptionIds];
        cscfga__Product_Basket__c basket = [SELECT Id, Name, csordtelcoa__Change_Type__c, csordtelcoa__Product_Configuration_Clone_Batch_Job_Id__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
        
        for(csord__Subscription__c sub :subscriptions){
            subsIds.add(sub.Id);
        }
        
        basketId2 = csordtelcoa.API_V1.addSubscriptionsToMacBasket(subsIds, basketId2, true);
        
        return String.valueOf(basketId2);
    }    
    
    
    public void addMessageToPage(){
        ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.Info
                , 'Macd basket batch created! Please check your basket list (under the account) and look for last macd basket you own.');
            ApexPages.addMessage(msg);
    } 

    private static csordtelcoa__Orders_Subscriptions_Options__c osOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(); 

    private static Set<String> statusesNotAllowingChange = null;
     
    /**
    * Gets the set of all defined statuses not allowing a change as defined in the Statuses_Not_Allowing_Change__c
    * custom settings field. If the list is empty in the custom settings, an empty set is returned. The result is
    * cached in a static field.
    */
    public static Set<String> getStatusesNotAllowingChange() {
        if (statusesNotAllowingChange == null) {
            // if the cache is not loaded, fill the values...
            statusesNotAllowingChange = new Set<String>();
            if (!String.isEmpty(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c)) {
                // split cvs list of stages (allowing whitespaces around the comma)
                statusesNotAllowingChange.addAll(osOptions.csordtelcoa__Statuses_Not_Allowing_Change__c.split('\\s*,\\s*'));
            }
        }
        system.debug('ritzzz not allowing change'+statusesNotAllowingChange);
        return statusesNotAllowingChange;
    }
    
    /**
    * @description Retrieves all record type names available to the current user in the form of a list of strings
    * @return a list of strings each representing a record type name
    */
    public static List<String> getAvailableRecordTypeNamesForSObject(Schema.SObjectType objType) {
        List<String> names = new List<String>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        // If there are 2 or more RecordTypes...
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                if (i.isAvailable() 
                // Ignore the Master Record Type, whose Id always ends with 'AAA'.
                // We check the Id because Name can change depending on the user's language.
                 && !String.valueOf(i.getRecordTypeId()).endsWith('AAA'))
                     names.add(i.getName());
            }
        } 
        // Otherwise there's just the Master record type,
        // so add it in, since it MUST always be available
        else names.add(infos[0].getName());
        return names;
    }
 
    /**
    * @description Retrieves all record types available to the current user in the form of a list of SelectOption objects. Available record types are further filtered
    * using the custom settings Use_Opportunity_Record_Types__c and MACD_Opportunity_Record_Types__c 
    * @return a list of select options where value is a record type Id and label a record type name
    */
    public static List<SelectOption> getOpportunityRecordTypes() {
        List<SelectOption> options = new List<SelectOption>();
        Map<String,RecordType> oppRecordTypes = new Map<String,RecordType>([Select Id, Name From RecordType Where SobjectType = 'Opportunity']);
 
        csordtelcoa__Orders_Subscriptions_Options__c orderSubscriptionsOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(UserInfo.getUserId());
        Set<String> macdOppRecordTypes = new Set<String> ();
        if (!string.isBlank(orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c)) {
            macdOppRecordTypes = new Set<String> (orderSubscriptionsOptions.csordtelcoa__MACD_Opportunity_Record_Types__c.split(',',0));
        }
  
        if (orderSubscriptionsOptions.csordtelcoa__Use_Opportunity_Record_Types__c) {
            List<String> availableRecordTypes = getAvailableRecordTypeNamesForSObject(Opportunity.SObjectType);
            Set<String> availableRecordTypesSet = new Set<string>(availableRecordTypes);
         
            for (String key : oppRecordTypes.keySet()) {
                if (availableRecordTypesSet.contains(oppRecordTypes.get(key).Name) && (macdOppRecordTypes.size() == 0 || macdOppRecordTypes.contains(oppRecordTypes.get(key).Name))) {
                    options.add(new SelectOption(key,oppRecordTypes.get(key).name));
                }
            }         
        } else {
            List<String> defaultRecordType = getDefaultRecordTypeNameForSObject(Opportunity.SObjectType);
        }
        
        return options;
    }
    
     /**
      * @description Retrieves the default record type name available to the current user in the form of a list with a single item
      * @return a single string that is the default record type name for the given object schema
     */
    @testVisible
    private static List<String> getDefaultRecordTypeNameForSObject(Schema.SObjectType objType) {
        List<String> names = new List<String>();
        List<RecordTypeInfo> infos = objType.getDescribe().getRecordTypeInfos();
        
        // If there are 2 or more RecordTypes...
        if (infos.size() > 1) {
            for (RecordTypeInfo i : infos) {
                if (i.isDefaultRecordTypeMapping())
                    names.add(i.getName());
            }
        } else if (infos.size() == 1) {
            // Otherwise there's just the Master record type,
            // so add it in, since it MUST always be available
            names.add(infos[0].getName());
        } else {
            //there are no record types for this SObject
        }
     
        return names;
    }
       /*
    * @Pavan@
    * TGP0041766:P2A order with unnecessary termination items. 
    */
    public boolean ServiceTerminateorder(){
        boolean isService = False;
        Set<Id> subs = new Set<Id>();
        //List<csord__Service__c> srvcList = [Select Id, AccountId__c, csord__Subscription__c, csord__Subscription__r.Name, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c IN :choices];
        for(csord__Service__c srvc :[Select Id, AccountId__c, csord__Subscription__c, csord__Subscription__r.Name, csordtelcoa__Replacement_Service__c from csord__Service__c where csord__Subscription__c IN :choices]) {
            if(srvc.csordtelcoa__Replacement_Service__c != null) {
                if(SubscriptionList == null){
                    SubscriptionList = srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);                  
                }
                else if(!subs.contains(srvc.csord__Subscription__c)){
                    SubscriptionList = SubscriptionList + srvc.csord__Subscription__r.Name +', ';
                    subs.add(srvc.csord__Subscription__c);
                }
                isService = True;               
            }
        }
        return isService;
    }  
    
}