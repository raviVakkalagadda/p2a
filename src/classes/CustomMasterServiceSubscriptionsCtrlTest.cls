@isTest
private Class CustomMasterServiceSubscriptionsCtrlTest{
    
    static csord__Service__c csServ;
    static List<csordtelcoa__Change_Types__c> changeTypeCSList;
    
    private static void init() {

        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        changeTypeCSList = new List<csordtelcoa__Change_Types__c>();
        
        // providing the values to custom setting object
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Upgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = TRUE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = 'Test';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 7;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Downgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = FALSE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = '';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 8;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Termination';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = FALSE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = TRUE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = 'Terminate Order Creation';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 11;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Reconfiguration';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = FALSE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = '';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 6;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);

        insert changeTypeCSList;
        List<csordtelcoa__Change_Types__c> chtype = [select id,name from csordtelcoa__Change_Types__c where name ='Parallel Upgrade'];
        system.assert(chtype!=null);
        system.assertEquals(chtype[0].name,'Parallel Upgrade');
        
        List<CurrencyValue__c> CurValList = new List<CurrencyValue__c>{
                                                                        new CurrencyValue__c(CurrencyIsoCode = 'USD', Currency_Value__c = 1.0, Name = 'USD'),
                                                                        new CurrencyValue__c(CurrencyIsoCode = 'AUD', Currency_Value__c = 11.0, Name = 'AUD'),
                                                                        new CurrencyValue__c(CurrencyIsoCode = 'INR', Currency_Value__c = 62.0, Name = 'INR'),
                                                                        new CurrencyValue__c(CurrencyIsoCode = 'HKD', Currency_Value__c = 99.0, Name = 'HKD')
        };
        insert CurValList;
        List<CurrencyValue__c> cval= [select id,name from CurrencyValue__c where name ='USD'];
        system.assert(cval!=null);
        system.assertEquals(cval[0].name,'USD');
        
        Product_Definition_Id__c pdI = new Product_Definition_Id__c();
        pdI.Name = 'IPVPN_Product_ID';
        pdI.Product_Id__c = 'IPVPN';
        insert pdI;
        system.assert(pdI!=null);
        Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
        pdI1.Name = 'VPLS_Product_ID';
        pdI1.Product_Id__c = 'VLM';
        insert pdI1;
        system.assert(pdI1!=null);
        Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
        pdI2.Name = 'IPC_Product_ID';
        pdI2.Product_Id__c = 'IPTS-C';
        insert pdI2;
        system.assert(pdI2!=null);
        
        List<csord__Order__c> Orderlist = new list<csord__Order__c>();
        List<csord__Order__c> Orderlists = new list<csord__Order__c>();     
        Id orderid=ApexPages.currentPage().getParameters().get('Id') ;
        List<cscfga__Product_Definition__c> pdlist = new List<cscfga__Product_Definition__c>(); 
        List<csord__Order__c> ordrslists = new List<csord__Order__c>();
        
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        List<Country_Lookup__c> clook= [select id,name from Country_Lookup__c where name ='test'];
        system.assert(cval!=null);
        system.assertEquals(cval[0].name,'USD'); 
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        system.assertEquals(AccList[0].name,'Test Accenture');
        /*List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        accList[0].Customer_Type__c = 'GW';
        update accList[0]; */
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        SUBList[0].CurrencyIsoCode = 'USD';
        update SUBList;
        system.assert(SUBList!=null);
        
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> sitelist = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'TWI Singlehome';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        pdlist.add(pd);
        insert pdlist;
        System.assertEquals(pdlist[0].name,'TWI Singlehome'); 
        
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,Products,pdlist,pbundlelist,Offerlists);
        
        cscfga__Product_Configuration__c proconfig1 = new cscfga__Product_Configuration__c();
        proconfig1.cscfga__Product_basket__c = Products[0].id;
        proconfig1.cscfga__Product_Definition__c = pdlist[0].id;
        proconfig1.name ='TWI Singlehome';
        proconfig1.cscfga__contract_term_period__c = 12;
        proconfig1.Is_Offnet__c = 'yes';
        proconfig1.cscfga_Offer_Price_MRC__c = 200;
        proconfig1.cscfga_Offer_Price_NRC__c = 300;
        proconfig1.cscfga__Product_Bundle__c = pbundlelist[0].id;
        proconfig1.Rate_Card_NRC__c = 200;
        proconfig1.Rate_Card_RC__c = 300;
        proconfig1.cscfga__total_contract_value__c = 1000;
        proconfig1.cscfga__Contract_Term__c = 12;
        proconfig1.CurrencyIsoCode = 'USD';
        proconfig1.cscfga_Offer_Price_MRC__c = 100;
        proconfig1.cscfga_Offer_Price_NRC__c = 100;
        proconfig1.Child_COst__c = 100;
        proconfig1.Cost_NRC__c = 100;
        proconfig1.Cost_MRC__c = 100;
        proconfig1.Product_Name__c = 'test product';
        proconfig1.Added_Ports__c = proconfig[0].Id;
        proconfig1.cscfga__Product_Family__c = 'Point to Point';
        proconfig1.EvplId__c = proconfig1.Id;
        insert proconfig1;
        System.assertEquals(proconfig1.name,'TWI Singlehome'); 
        
        cscfga__Product_Configuration__c proconfig2 = new cscfga__Product_Configuration__c();
        proconfig2.cscfga__Product_basket__c = Products[0].id;
        proconfig2.cscfga__Product_Definition__c = pdlist[0].id;
        proconfig2.name ='TWI Singlehome';
        proconfig2.cscfga__contract_term_period__c = 12;
        proconfig2.Is_Offnet__c = 'yes';
        proconfig2.cscfga_Offer_Price_MRC__c = 200;
        proconfig2.cscfga_Offer_Price_NRC__c = 300;
        proconfig2.cscfga__Product_Bundle__c = pbundlelist[0].id;
        proconfig2.Rate_Card_NRC__c = 200;
        proconfig2.Rate_Card_RC__c = 300;
        proconfig2.cscfga__total_contract_value__c = 1000;
        proconfig2.cscfga__Contract_Term__c = 12;
        proconfig2.CurrencyIsoCode = 'USD';
        proconfig2.cscfga_Offer_Price_MRC__c = 100;
        proconfig2.cscfga_Offer_Price_NRC__c = 100;
        proconfig2.Child_COst__c = 100;
        proconfig2.Cost_NRC__c = 100;
        proconfig2.Cost_MRC__c = 100;
        proconfig2.Product_Name__c = 'test product';
        proconfig2.Added_Ports__c = proconfig[0].Id + ',' + proconfig1.Id;
        proconfig2.cscfga__Product_Family__c = 'Point to Point';
        proconfig2.EvplId__c = proconfig2.Id;
        insert proconfig2;
        System.assertEquals(proconfig2.name,'TWI Singlehome'); 
        
        csord__Order__c ord = new csord__Order__c();
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.RAG_Order_Status_RED__c = false;
        ord.RAG_Reason_Code__c = '';
        ord.Id = orderid;
        ord.Order_Type__c = 'New';
        ord.Is_Terminate_Order__c = false;
        ord.Is_Order_Submitted__c = true;
        ord.SD_PM_Contact__c = NULL; 
        ord.Reseller_Account_ID__c = NULL ; 
        ord.csord__Order_Request__c = OrdReqList[0].Id;
        Orderlist.add(ord);
        insert orderlist;
        System.assertEquals(orderlist[0].csord__Identification__c ,'Test-JohnSnow-4238362'); 
        
        csServ = new csord__Service__c();
        csServ.Name = 'Test Service'; 
        csServ.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ.csord__Order_Request__c = OrdReqList[0].Id;
        csServ.csord__Subscription__c = SUBList[0].Id;
        csServ.Billing_Commencement_Date__c = System.Today();
        csServ.Stop_Billing_Date__c = System.Today();
        csServ.RAG_Status_Red__c = false ; 
        csServ.RAG_Reason_Code__c = 'Text Reason';
        csServ.Opportunity__c = oppList[0].Id;
        csServ.csord__Order__c = Orderlist[0].Id;
        csServ.Customer_Required_Date__c = System.today();
        csServ.Estimated_Start_Date__c = System.now().addDays(10);
        csServ.Selling_Entity__c = 'Telstra Limited';
        csServ.Service_Type__c = 'Test service';
        csServ.Product_Code__c = 'VLV';
        csServ.Order_Channel_Type__c = 'Direct Sale';
        csServ.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ.csordtelcoa__Product_Configuration__c = proconfig2.Id;
        csServ.Updated__c = true;
        csServ.Product_Id__c = 'VLV';
        csServ.Path_Instance_ID__c = NULL;
        csServ.AccountId__c = accList[0].Id;
        csServ.Bundle_Label_name__c = 'Test Bundle';
        csServ.Parent_Bundle_Flag__c = true;
        csServ.Inventory_Status__c = System.Label.PROVISIONED;
        insert csServ;
        System.assertEquals(csServ.name,'Test Service'); 
        
        csord__Service__c csServ1 = new csord__Service__c();
        csServ1.Name = 'Test Service'; 
        csServ1.csord__Identification__c = 'Test-Catlyne-4238362';
        csServ1.csord__Order_Request__c = OrdReqList[0].Id;
        csServ1.csord__Subscription__c = SUBList[0].Id;
        csServ1.Billing_Commencement_Date__c = System.Today();
        csServ1.Stop_Billing_Date__c = System.Today();
        csServ1.RAG_Status_Red__c = false ; 
        csServ1.RAG_Reason_Code__c = 'Text Reason';
        csServ1.Opportunity__c = oppList[0].Id;
        csServ1.csord__Order__c = Orderlist[0].Id;
        csServ1.Customer_Required_Date__c = System.today();
        csServ1.Estimated_Start_Date__c = System.now().addDays(10);
        csServ1.Selling_Entity__c = 'Telstra Limited';
        csServ1.Service_Type__c = 'Test service';
        csServ1.Product_Code__c = 'VLV';
        csServ1.Order_Channel_Type__c = 'Direct Sale';
        csServ1.csordtelcoa__Product_Basket__c = Products[0].Id;
        csServ1.csordtelcoa__Product_Configuration__c = proconfig[0].Id;
        csServ1.Updated__c = true;
        csServ1.Product_Id__c = 'VLV';
        csServ1.Path_Instance_ID__c = NULL;
        csServ1.AccountId__c = accList[0].Id;
        csServ1.Bundle_Label_name__c = 'Test Bundle';
        csServ1.Parent_Bundle_Flag__c = true;
        csServ1.Inventory_Status__c = System.Label.PROVISIONED;
        csServ1.Master_Service__c = csServ.Id;
        insert csServ1;
        System.assertEquals(csServ1.name,'Test Service');

    }
    
    @isTest static void customMasterServiceSubscriptionsCtrlMethod1(){
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
        init();
        
        Test.startTest();
        PageReference tpageRef1 = Page.CustomMasterServiceSubscriptionsPage;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', csServ.id);
        ApexPages.currentPage().getParameters().put('changeType', 'Parallel Upgrade');
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(csServ);
        CustomMasterServiceSubscriptionsCtrl  ms  = new CustomMasterServiceSubscriptionsCtrl(stdController);
        ms.changeType = 'Parallel Upgrade';
        ms.processName = 'Parallel Upgrade';
        system.assertEquals(true,ms!=null);
        
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomMasterServiceSubscriptionsCtrl.Change_Types_Wrapper ctw = new CustomMasterServiceSubscriptionsCtrl.Change_Types_Wrapper(changeTypeCS);  //Covering inner/wrapper class
        ctw.compareTo(ctw);
        
        // ms.CustomMasterServiceSubscriptionsCtrl(new ApexPages.StandardController(services));
        ms.getChangeTypes();
        ms.getRequestedDateLabel();
        ms.getList();
        ms.getOppRecordTypes();
        CustomMasterServiceSubscriptionsCtrl.getStatusesNotAllowingChange();
         
        ms.getRequestDate();
        ms.getInvokeProcess();
        ms.rerenderButtons();
        ms.selectAllOptions();
        // ms.getIdChildService();
        //CustomMasterServiceSubscriptionsCtrl.getOppRecordTypes();
        List<String> strResultList = CustomMasterServiceSubscriptionsCtrl.getAvailableRecordTypeNamesForSObject(Case.SObjectType);
        
        ms.dateString = System.today().format();
        ms.createProcesses();
        ms.processName = '';
        ms.createProcesses();
        ms.CreateMultipleSubscriptionMacOpportunity();
        ms.Title = '';
        ms.CreateMultipleSubscriptionMacOpportunity();
        ms.CreateMultipleSubscriptionMacBasket();
        ms.productBasketTitle = '';
        ms.CreateMultipleSubscriptionMacBasket();
        //Pagereference p1 = trcustomCon1.validateService(); 
        
        Test.stopTest();
    }
    
    @isTest static void customMasterServiceSubscriptionsCtrlMethod2(){
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        string s1,s2,s3,s4,s5;
        init();
        
        Test.startTest();
        PageReference tpageRef1 = Page.CustomMasterServiceSubscriptionsPage;
        Test.setCurrentPage(tpageRef1);        
        ApexPages.currentPage().getParameters().put('Id', csServ.id);
        ApexPages.currentPage().getParameters().put('changeType', 'Parallel Upgrade');
        
        Apexpages.StandardController stdController = new Apexpages.StandardController(csServ);
        CustomMasterServiceSubscriptionsCtrl  ms  = new CustomMasterServiceSubscriptionsCtrl(stdController);
        ms.changeType = 'Parallel Upgrade';
        //ms.processName = 'Parallel Upgrade';
        List<String> strList = new List<String>();
        strList.add('Test1');
        strList.add('Test2');
        ms.choices = strList;
        
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c(Name = 'Test');
        CustomMasterServiceSubscriptionsCtrl.Change_Types_Wrapper ctw = new CustomMasterServiceSubscriptionsCtrl.Change_Types_Wrapper(changeTypeCS);  //Covering inner/wrapper class
        ctw.compareTo(ctw);
        system.assertEquals(true,changeTypeCS!=null);
        // ms.CustomMasterServiceSubscriptionsCtrl(new ApexPages.StandardController(services));
        ms.getChangeTypes();
        ms.getRequestedDateLabel();
        ms.getList();
        ms.getOppRecordTypes();
        CustomMasterServiceSubscriptionsCtrl.getStatusesNotAllowingChange();
         
        ms.getRequestDate();
        ms.getInvokeProcess();
        ms.rerenderButtons();
        ms.selectAllOptions();
        // ms.getIdChildService();
        //CustomMasterServiceSubscriptionsCtrl.getOppRecordTypes();
        List<String> strResultList = CustomMasterServiceSubscriptionsCtrl.getAvailableRecordTypeNamesForSObject(Case.SObjectType);
        
        
        
        
        
        ms.dateString = System.today().format();
        ms.createProcesses();
        ms.processName = '';
        ms.createProcesses();
        ms.CreateMultipleSubscriptionMacOpportunity();
        ms.Title = '';
        ms.CreateMultipleSubscriptionMacOpportunity();
        ms.CreateMultipleSubscriptionMacBasket();
        ms.productBasketTitle = '';
        ms.CreateMultipleSubscriptionMacBasket();
        ms.myfunctiontrial();
        //CustomMasterServiceSubscriptionsCtrl.createBasketFromSubscription(s1,s2,s3);
        //CustomMasterServiceSubscriptionsCtrl.addSubscriptionsToBasket(s4,s5);
        ms.addMessageToPage();
        boolean boo = ms.ServiceTerminateorder();
        //Pagereference p1 = trcustomCon1.validateService(); 
        
        Test.stopTest();
    }
    
    @isTest static void customMasterServiceSubscriptionsCtrlMethod3(){
        
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        SUBList[0].CurrencyIsoCode = 'USD';
        update SUBList;
        
         changeTypeCSList = new List<csordtelcoa__Change_Types__c>();
        
        // providing the values to custom setting object
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Upgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = TRUE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = 'Test';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 7;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Downgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = FALSE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = '';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 8;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        insert changeTypeCSList;
        
        string str = changeTypeCSList[0].Name;
        String productBasketTitle = 'productBasketTitle-12-07-2017';
        
        List<csord__Subscription__c> singlesub = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id = :SUBList[0].id];
        
        csordtelcoa__Change_Types__c changeType = [SELECT Id, Name,csordtelcoa__Add_To_Existing_Subscription__c, csordtelcoa__Auto_Create_Bundle__c FROM csordtelcoa__Change_Types__c WHERE Name ='Parallel Upgrade'];
        
        String subscription = singlesub[0].id;
          
        String changeTypeName = changeType.Name;
                
        Id basketId = csordtelcoa.API_V1.createMacBasketFromSubscriptions(singlesub, changeType,productBasketTitle, null);
        
        String Customerservicesubscr = CustomMasterServiceSubscriptionsCtrl.createBasketFromSubscription(subscription,changeTypeName,productBasketTitle);
        
    }
    
    @isTest static void customMasterServiceSubscriptionsCtrlMethod4(){
        
        List<Country_Lookup__c>Countrytlist  = new List<Country_Lookup__c>();
        Country_Lookup__c country = new Country_Lookup__c();
        country.name = 'test';
        country.Country_Code__c = 'TestP2A99';
        Countrytlist.add(country);
        insert Countrytlist; 
        
        List<Account>AccList = new List<Account>();
        Account a = new Account();
        a.name                          = 'Test Accenture';
        a.BillingCountry                = 'GB';
        a.Activated__c                  = True;
        a.Account_ID__c                 = 'test';
        a.Account_Status__c             = 'Active';
        a.Customer_Legal_Entity_Name__c = 'Test';
        a.Customer_Type_New__c = 'GW';
        a.Region__c = 'Region';
        a.Website='abc.com';
        a.Selling_Entity__c='Telstra Limited';
        a.Country__c=Countrytlist[0].Id;
        a.Industry__c='BPO';
        a.Account_ORIG_ID_DM__c = 'test';
        AccList.add(a);
        Insert AccList; 
        
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        SUBList[0].CurrencyIsoCode = 'USD';
        update SUBList;
        
         changeTypeCSList = new List<csordtelcoa__Change_Types__c>();
        
        // providing the values to custom setting object
        csordtelcoa__Change_Types__c changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Upgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = TRUE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = 'Test';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 7;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        changeTypeCS = new csordtelcoa__Change_Types__c();
        changeTypeCS.Name = 'Parallel Downgrade';
        changeTypeCS.csordtelcoa__Add_To_Existing_Subscription__c = FALSE;
        changeTypeCS.csordtelcoa__Allow_Multiple_Subscriptions__c = TRUE;
        changeTypeCS.csordtelcoa__Auto_Create_Bundle__c = TRUE;
        changeTypeCS.csordtelcoa__Invoke_Process__c = FALSE;
        changeTypeCS.csordtelcoa__Process_To_Invoke__c = '';
        changeTypeCS.csordtelcoa__Request_Date__c = FALSE;
        changeTypeCS.csordtelcoa__Requested_Date_Label__c = '';
        changeTypeCS.csordtelcoa__Sort_Order__c = 8;
        changeTypeCS.csordtelcoa__Subscription_Date_Field_API_Name__c = '';
        changeTypeCSList.add(changeTypeCS);
        
        insert changeTypeCSList;
        
        List<Id> subsIds = new List<Id>();
        string subscriptionIds2 = SUBList[0].id;
        String basketId = Products[0].id;
        Id basketId2 = Id.valueOf(basketId);
        //List<String> subscriptionIds = (List<String>) JSON.deserialize(subscriptionIds2, List<String>.class); 
        
        try{
        List<csord__Subscription__c> subscriptions = [SELECT Id, Name, csord__Account__c, CreatedDate, CurrencyIsoCode FROM csord__Subscription__c WHERE Id =:subscriptionIds2];
        cscfga__Product_Basket__c basket = [SELECT Id, Name, csordtelcoa__Change_Type__c, csordtelcoa__Product_Configuration_Clone_Batch_Job_Id__c FROM cscfga__Product_Basket__c WHERE Id = :basketId];
        
        for(csord__Subscription__c sub :subscriptions){
            subsIds.add(sub.Id);
        }
        
        
         basketId2 = csordtelcoa.API_V1.addSubscriptionsToMacBasket(subsIds, basketId2, true);
         String  Subscriptionadd = CustomMasterServiceSubscriptionsCtrl.addSubscriptionsToBasket(basketId,subscriptionIds2);
         }catch(Exception e){}
    }
}