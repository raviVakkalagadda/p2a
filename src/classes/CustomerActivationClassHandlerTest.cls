@isTest
global class CustomerActivationClassHandlerTest {
    
    
    public static void generateData(){     
        
        string customerDateTimeString = '2017-07-27 15:19:00';
        DateTime customerDateTime = DateTime.valueofGmt(customerDateTimeString);
        string customerTimeZoneSidId = 'Europe/London';
        
        TimeZone customerTimeZone = TimeZone.getTimeZone(customerTimeZoneSidId);
        //System.assertEquals('Greenwich Mean Time',  customerTimeZone.getDisplayName());
                
        integer offsetToCustomersTimeZone = customerTimeZone.getOffset(customerDateTime);
        System.debug('GMT Offset: ' + offsetToCustomersTimeZone + ' (milliseconds) to PST');
        
        // For the given Date I expect PST to be GMT - 8 hours
       // System.assertEquals(1, offsetToCustomersTimeZone / (1000 * 60 *60));
        
        // Here you might like to explicitly use 'Asia/Colombo' to get IST in your code.
        TimeZone tz = UserInfo.getTimeZone();
        // During daylight saving time for the Pacific/Auckland time zone
        integer offsetToUserTimeZone = tz.getOffset(customerDateTime);
        System.debug('GMT Offset: ' + offsetToUserTimeZone + ' (milliseconds) to NZDT');
        
        // Expect NZDT to be GMT + 13 hours
      //  System.assertEquals(8, offsetToUserTimeZone / (1000 * 60 *60));
        
        // Figure out correct to go from Customers DateTime to GMT and then from GMT to Users TimeZone
        integer correction = offsetToUserTimeZone - offsetToCustomersTimeZone;
        System.debug('correction: ' + correction);
        
        // Note: Potential issues for TimeZone differences less than a minute
        DateTime correctedDateTime = customerDateTime.addMinutes(correction / (1000 * 60));
        System.debug('correctedDateTime: ' + correctedDateTime);
        // In the users Pacific/Auckland timezone the time should be moved forward 21 hours
       // System.assertEquals(correctedDateTime, DateTime.valueofGmt('2017-07-27 22:19:00'));  
        
    }
    
     public Static testMethod void Timezonechanges(){  
     generateData();
     List<Customer_Activation_Test__c> CATList = new List<Customer_Activation_Test__c>();
     
     List<Account> Acclist = P2A_TestFactoryCls.getAccounts(1);
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
     List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,OrdReqList);
     
     
     Customer_Activation_Test__c Cat = new Customer_Activation_Test__c();
     cat.Account__c = Acclist[0].id;
     cat.Activation_appointment_Date__c = date.newInstance(2017,07,18);
     cat.Customer_Name__c = 'CATTest';
     cat.Customer_contact_details__c = 'Sample Test Street';
     cat.End_time_for_activation_activity__c =  datetime.newInstance(2017,07,29,15,19,00);
     cat.Estimated_Start_Date__c = datetime.newInstance(2017,07,29,14,48,00);
     cat.Order__c = orderList[0].id;
     cat.Start_time_for_activation_activity__c = datetime.newInstance(2017,07,18,15,19,00);
     cat.START_Time_Tibco__c = datetime.newInstance(2017,07,28,14,48,00);  
     cat.End_Time_Tibco__c =  datetime.newInstance(2017,07,28,16,48,00);  
     cat.Time_zone_for_appointment__c = 'London';
     cat.Parent_Product__c = 'CAT';
     cat.Product_Configuration_Type__c = 'New Provide';
     cat.Region__c = 'US';
     cat.CAT_InFlight_Update__c = 'NO';
     cat.Scope_of_work__c = 'Scope';
     cat.Request_Summary__c = 'Request';
     cat.Work_Order_Status__c =  'New';
     cat.Type_of_Update__c = 'data_update';
     cat.Service_Activation_Queue__c =  'GSDI - Technical Delivery';
     CATList.add(cat); 
     
     Customer_Activation_Test__c Cat1 = getCAT('pune');  
     CATList.add(Cat1);
     Customer_Activation_Test__c Cat2 = getCAT('HONG KONG/KUALA LUMPUR');         
     CATList.add(Cat2);
     Customer_Activation_Test__c Cat3 = getCAT('New York');         
     CATList.add(Cat3);
     Customer_Activation_Test__c Cat4 = getCAT('Sydney/Melbourne');          
     CATList.add(Cat4);
     Customer_Activation_Test__c Cat5 = getCAT('Tokyo');
     CATList.add(Cat5);
     Customer_Activation_Test__c Cat6 = getCAT('Los Angeles');    
     CATList.add(Cat6);    
     
     insert CATList;
     system.assertEquals(true,CATList!=null);         
     Test.startTest();
     Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
         CustomerActivationClassHandler cntrlhandler = new CustomerActivationClassHandler();
         cntrlhandler.timezonechange(CATList);
         system.assertEquals(true,cntrlhandler!=null);
         
             
     Test.StopTest(); 
     
    } 
    
     public Static testMethod void TimezonechangesUpdateTest(){ 
     generateData();
     List<Customer_Activation_Test__c> CATList = new List<Customer_Activation_Test__c>();
     
     List<Account> Acclist = P2A_TestFactoryCls.getAccounts(1);
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
     List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,OrdReqList);
     
     
     Customer_Activation_Test__c Cat = new Customer_Activation_Test__c();
     cat.Account__c = Acclist[0].id;
     cat.Activation_appointment_Date__c = date.newInstance(2017,07,18);
     cat.Customer_Name__c = 'CATTest';
     cat.Customer_contact_details__c = 'Sample Test Street';
     cat.End_time_for_activation_activity__c =  datetime.newInstance(2017,07,29,15,19,00);
     cat.Estimated_Start_Date__c = datetime.newInstance(2017,07,29,14,48,00);
     cat.Order__c = orderList[0].id;
     cat.Start_time_for_activation_activity__c = datetime.newInstance(2017,07,18,15,19,00);
     cat.START_Time_Tibco__c = datetime.newInstance(2017,07,28,14,48,00);  
     cat.End_Time_Tibco__c =  datetime.newInstance(2017,07,28,16,48,00);  
     cat.Time_zone_for_appointment__c = 'London';
     cat.Parent_Product__c = 'CAT';
     cat.Product_Configuration_Type__c = 'New Provide';
     cat.Region__c = 'US';
     cat.CAT_InFlight_Update__c = 'NO';
     cat.Scope_of_work__c = 'Scope';
     cat.Request_Summary__c = 'Request';
     cat.Work_Order_Status__c =  'New';
     cat.Type_of_Update__c = 'data_update';
     cat.Service_Activation_Queue__c =  'GSDI - Technical Delivery';
     insert cat;
       
     
     //Customer_Activation_Test__c CatA = getCAT('HONG KONG/KUALA LUMPUR');
     //System.assertEquals(Cat2.Time_zone_for_appointment__c,'HONG KONG/KUALA LUMPUR');
     
     List<Customer_Activation_Test__c>newCat=new List<Customer_Activation_Test__c>();
     List<Customer_Activation_Test__c>newCat1=new List<Customer_Activation_Test__c>();
     List<Customer_Activation_Test__c>newCat2=new List<Customer_Activation_Test__c>();
     List<Customer_Activation_Test__c>newCat3=new List<Customer_Activation_Test__c>();
     Map<Id,Customer_Activation_Test__c>oldCatMap=new Map<Id,Customer_Activation_Test__c>();
     Map<Id,Customer_Activation_Test__c>newCatMap=new Map<Id,Customer_Activation_Test__c>();
      Map<Id,Customer_Activation_Test__c>oldCatMap1=new Map<Id,Customer_Activation_Test__c>();
     Map<Id,Customer_Activation_Test__c>newCatMap1=new Map<Id,Customer_Activation_Test__c>();
     
      Customer_Activation_Test__c Cat1 = new Customer_Activation_Test__c();
      Cat1.id=cat.id;
      Cat1.Customer_Name__c='CATTest';
      
      /* Customer_Activation_Test__c Cat2 = new Customer_Activation_Test__c();
      Cat2.id=CatA.id;
      Cat2.Customer_Name__c='CATTest';*/
      
      newCat.add(cat1);
      //newCat1.add(Cat2);
      
     newCatMap.put(cat1.id,cat1);   
     //newCatMap1.put(Cat2.id,Cat2);
     
     cat.Customer_Name__c='CATTest2';    
     update cat;
     oldCatMap.put(cat.id,cat);
     
    
     System.assertEquals(newCat[0].Customer_Name__c,'CATTest');
     System.assertEquals(oldCatMap.get(newCat[0].id).Customer_Name__c,'CATTest2');
     System.assert(newCat[0].Customer_Name__c!=oldCatMap.get(newCat[0].id).Customer_Name__c); 
     
       Test.startTest();
       Test.setMock(HttpCalloutMock.class, new WebServiceConnectorMock());
         CustomerActivationClassHandler cntrlhandler = new CustomerActivationClassHandler();
         cntrlhandler.timezonechangeafterupdate(newCat,newCatMap,oldCatMap,true); 
        //cntrlhandler.timezonechangeafterupdate(newCat1,newCatMap1,oldCatMap1,true); 
     
         Test.StopTest(); 

     }
    
    Public static Customer_Activation_Test__c getCAT(String str){
    List<Account> Acclist = P2A_TestFactoryCls.getAccounts(1);
     List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
     List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,OrdReqList);
     
    Customer_Activation_Test__c Cat = new Customer_Activation_Test__c();
     cat.Account__c = Acclist[0].id;
     cat.Activation_appointment_Date__c = date.newInstance(2017,07,18);
     cat.Customer_Name__c = 'CATTest';
     cat.Customer_contact_details__c = 'Sample Test Street';
     cat.End_time_for_activation_activity__c =  datetime.newInstance(2017,07,29,15,19,00);
     cat.Estimated_Start_Date__c = datetime.newInstance(2017,07,29,14,48,00);
     cat.Order__c = orderList[0].id;
     cat.Start_time_for_activation_activity__c = datetime.newInstance(2017,07,18,15,19,00);
     cat.START_Time_Tibco__c = datetime.newInstance(2017,07,28,14,48,00);  
     cat.End_Time_Tibco__c =  datetime.newInstance(2017,07,28,16,48,00);  
     cat.Time_zone_for_appointment__c = str;
     cat.Parent_Product__c = 'CAT';
     cat.Product_Configuration_Type__c = 'New Provide';
     cat.Region__c = 'US';
     cat.CAT_InFlight_Update__c = 'NO';
     cat.Scope_of_work__c = 'Scope';
     cat.Request_Summary__c = 'Request';
     cat.Work_Order_Status__c =  'New';
     cat.Type_of_Update__c = 'data_update';
     cat.Service_Activation_Queue__c =  'GSDI - Technical Delivery';
     //insert cat;
     return cat;        
    }

}