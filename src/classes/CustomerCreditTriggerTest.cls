/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData=True)
private class CustomerCreditTriggerTest {
  static Account acc;
    static testMethod void UpdateROCStatusTest() {
        // TO DO: implement unit test
         try{
        test.startTest();
        Miscellaneous_Charge__c miscObj=getMiscObj();
        miscObj.ROC_Line_Item_Status__c='Success';
        miscObj.Credit_Status__c='Approved';
        update miscObj;
         
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        
        Miscellaneous_Charge__c miscObj=getMiscObj();
        miscObj.ROC_Line_Item_Status__c='Error';
        update miscObj;
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }
        try{
        
        Miscellaneous_Charge__c miscObj=getMiscObj();
        miscObj.ROC_Line_Item_Status__c='Success';
        miscObj.Credit_Status__c='Terminate';
        update miscObj;
        test.stopTest(); 
        }catch(Exception ex){
    
        system.debug('exception'+ex);
        }            
    }
private static Miscellaneous_Charge__c getMiscObj(){
     Miscellaneous_Charge__c miscObj=new Miscellaneous_Charge__c();
     BillProfile__c billProfile=getBillProfile();
     miscObj.Bill_Profile__c=billProfile.Id;
     miscObj.Account__c=billProfile.Account__c;
     miscObj.Charge_Frequency__c='Monthly';
     miscObj.Credit_Against__c='Account';
     miscObj.Credit_Currency__c='AUD';
     miscObj.One_of_credit_bill_text__c='Test Bill text';
     miscObj.Recuring_credit_bill_text__c='Test Bill Text';
     miscObj.Start_Date_for_credit__c='05/22/2014';    
     miscObj.Type_of_Credit__c='Recurring Credit';
     miscObj.Credit_Status__c='Unapproved';
     miscObj.Recuring_Credit_amount__c=100.00;
     miscObj.One_of_Credit_amount__c=100.00;
     miscObj.Credit_Duration__c='2';
     insert miscObj;
     return miscObj;
}    
    



  private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        insert b;
        return b;
    }
    
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Customer_Legal_Entity_Name__c = 'Sample Name';
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = true;
            acc.Account_ID__c = '4444';
            acc.Account_Status__c = 'Active';
            insert acc;
        }
        return acc;
    }
}