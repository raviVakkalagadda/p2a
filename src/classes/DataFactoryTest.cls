@isTest
public class DataFactoryTest {
    /*
    * **********************************
    * OBJECT CREATION METHODS
    * **********************************
    */

    public static List<CSPOFA__Orchestration_Process_Template__c> createOrchestrationProcessTemplates(Boolean persist){
        List<CSPOFA__Orchestration_Process_Template__c> templates = new List<CSPOFA__Orchestration_Process_Template__c>();
        templates.add(createOrchestrationProcessTemplate('ServiceProcess', false));
        templates.add(createOrchestrationProcessTemplate('Order_New', false));
        
        if(persist){
            insert templates;
        }

        return templates;
    }

    public static void createProductDefinitionIdCustomSettings(cscfga__Product_Definition__c prodDef){
        Product_Definition_Id__c productDefinitionId1 = new Product_Definition_Id__c();
        productDefinitionId1.Name = 'Master_IPVPN_Service_Definition_Id1';
        productDefinitionId1.Product_Id__c = prodDef.Id;

        Product_Definition_Id__c productDefinitionId2 = new Product_Definition_Id__c();
        productDefinitionId2.Name = 'Master_VPLS_Service_Definition_Id2';
        productDefinitionId2.Product_Id__c = prodDef.Id;

        Product_Definition_Id__c productDefinitionId3 = new Product_Definition_Id__c();
        productDefinitionId3.Name = 'VLANGroup_Definition_Id';
        productDefinitionId3.Product_Id__c = prodDef.Id;
        
        Product_Definition_Id__c productDefinitionId4 = new Product_Definition_Id__c();
        productDefinitionId4.Name = 'IPC_Definition_Id';
        productDefinitionId4.Product_Id__c = prodDef.Id;

        Product_Definition_Id__c productDefinitionId5 = new Product_Definition_Id__c();
        productDefinitionId4.Name = 'Master_IPVPN_Service_Definition_Id';
        productDefinitionId5.Product_Id__c = prodDef.Id;

        

        insert new List<Product_Definition_Id__c>{productDefinitionId1, productDefinitionId2, productDefinitionId3,productDefinitionId4};
    }

    public static void createNewLogoCustomSettings(){
        List<NewLogo__c> logoList = new List<NewLogo__c>();

        logoList.add(new NewLogo__c(Name = 'Active', Action_Item__c = 'Active'));
        logoList.add(new NewLogo__c(Name = 'Logo_Prospect', Action_Item__c = 'Logo_Prospect'));
        logoList.add(new NewLogo__c(Name = 'Logo_Former_Customer', Action_Item__c = 'Logo_Former_Customer'));
        logoList.add(new NewLogo__c(Name = 'Logo_Customer', Action_Item__c = 'Logo_Customer'));

        insert logoList;
    }

    public static SObject createObject(Schema.DescribeSObjectResult objDesc, Map<String, Object> defaultValues, Boolean persist){
        SObject obj;

        if (objDesc.isCreateable()) {
            SObjectFactory factory = new SObjectFactory();
            obj = factory.create(objDesc, defaultValues);
            
            if(persist){
                 Database.SaveResult result = Database.insert(obj, false);
                System.assert(result.isSuccess(), errorMessage('insert', result.getErrors(), obj));
            }
        }

        return obj;
    }

    public static Account createAccount(String name, Country_Lookup__c country, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('Customer_Type_New__c', 'GW');
        defaultValues.put('Selling_Entity__c', 'Telstra Corporation Limited');
        defaultValues.put('Account_Status__c', 'Active');
        defaultValues.put('Type', 'Customer');
        defaultValues.put('Country__c', country.Id);
        defaultValues.put('Activated__c', true);
        defaultValues.put('Account_ID__c', '123');
        defaultValues.put('Customer_Legal_Entity_Name__c', name);

        Account acc = (Account)createObject(Schema.SObjectType.Account, defaultValues, persist);

        return acc;
    }

    public static Case createCase(String subject, Account acc, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Subject ', subject);
        defaultValues.put('AccountId', acc.Id);
        defaultValues.put('Status', 'Assigned');

        Case theCase = (Case)createObject(Schema.SObjectType.Case, defaultValues, persist);

        return theCase;
    }

    public static Contact createContact(String firstName, String lastName, Account acc, String contactType, Country_Lookup__c country, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('FirstName ', firstName);
        defaultValues.put('LastName ', lastName);
        defaultValues.put('AccountId', acc.Id);
        defaultValues.put('Contact_Type__c', contactType);
        defaultValues.put('Country__c', country.Id);

        Contact theContact = (Contact)createObject(Schema.SObjectType.Contact, defaultValues, persist);

        return theContact;
    }

    public static Country_Lookup__c createCountry(){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', 'Australia');
        defaultValues.put('Region__c', 'Australia');
        defaultValues.put('Country_Code__c', 'AU');
        defaultValues.put('CCMS_Country_Code__c', 'AU');
        defaultValues.put('CCMS_Country_Name__c', 'Australia');

        Country_Lookup__c country = (Country_Lookup__c)createObject(Schema.SObjectType.Country_Lookup__c, defaultValues, true);

        return country;
    }

    public static csord__Order_Request__c createOrderRequest(String moduleName, String moduleVersion, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('csord__Module_Name__c', moduleName);
        defaultValues.put('csord__Module_Version__c', moduleVersion);

        csord__Order_Request__c orderRequest = (csord__Order_Request__c)createObject(Schema.SObjectType.csord__Order_Request__c, defaultValues, persist);
        
        return orderRequest;
    }

    public static csord__Order__c createOrder(String name, String status, String identification, csord__Order_Request__c orderRequest, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('Is_InFlight_Cancel__c', false);
        defaultValues.put('Status__c', status);
        defaultValues.put('csord__Identification__c', identification);
        defaultValues.put('csord__Order_Request__c', orderRequest.Id);

        csord__Order__c order = (csord__Order__c)createObject(Schema.SObjectType.csord__Order__c, defaultValues, persist);
        
        return order;
    }

    public static cscfga__Product_Definition__c createProductDefinition(String name, String description, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('cscfga__Description__c', description);
        defaultValues.put('cscfga__Product_Category__c', createProductCategory('Category1', true).Id);

        cscfga__Product_Definition__c productDef = (cscfga__Product_Definition__c)createObject(Schema.SObjectType.cscfga__Product_Definition__c, defaultValues, persist);
        
        return productDef;
    }

    public static cscfga__Product_Configuration__c createProductConfiguration(String name, cscfga__Product_Definition__c productDef, String cloningId, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('cscfga__Product_Definition__c', productDef.Id);
        defaultValues.put('Inflight_Cloning_Id__c', cloningId);

        cscfga__Product_Configuration__c productConfig = (cscfga__Product_Configuration__c)createObject(Schema.SObjectType.cscfga__Product_Configuration__c, defaultValues, persist);

        return productConfig;
    }

    public static cscfga__Product_Category__c createProductCategory(String name, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('cscfga__Active__c', true);

        cscfga__Product_Category__c category = (cscfga__Product_Category__c)createObject(Schema.SObjectType.cscfga__Product_Category__c, defaultValues, persist);
        
        return category;
    }

    public static csord__Subscription__c createSubscription(String name, String identification, csord__Order_Request__c orderRequest, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('csord__Identification__c', identification);
        defaultValues.put('csord__Order_Request__c', orderRequest.Id);

        csord__Subscription__c subscription = (csord__Subscription__c)createObject(Schema.SObjectType.csord__Subscription__c, defaultValues, persist);

        return subscription;
    }

    public static csord__Service__c createService(String name, String status, cscfga__Product_Configuration__c productConfig, csord__Subscription__c subscription, String identification, csord__Order__c order, csord__Order_Request__c orderRequest, Site__c siteA, Site__c siteZ, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        //adding account into service
        Country_Lookup__c country = new Country_Lookup__c(Name='ABCD',Region__c='ABCD',Country_Code__c='AB',CCMS_Country_Code__c='CCAB',CCMS_Country_Name__c='CCMB');
        //insert country; 
        Account accobj= new Account(Name='ServAcc1',Customer_Type_New__c='GW',Selling_Entity__c='Telstra Corporation Limited',Account_Status__c='Active',Type='Customer',Country__c=country.Id,Activated__c=true,Account_ID__c='123',Customer_Legal_Entity_Name__c=name);       
        insert accobj;
        
        defaultValues.put('Name', name);
        defaultValues.put('csord__Status__c', status);
        defaultValues.put('csordtelcoa__Product_Configuration__c', productConfig.Id);
        defaultValues.put('csord__Identification__c', identification);
        defaultValues.put('csord__Order__c', order.Id);
        defaultValues.put('csord__Order_Request__c', orderRequest.Id);
        defaultValues.put('Acity_Side__c', siteA.Id);
        defaultValues.put('Zcity_Side__c', siteZ.Id);
        defaultValues.put('Product_Id__c', 'abcd');
        //defaultValues.put('Account_ID__c', accobj.id);
        
        

        csord__Service__c service = (csord__Service__c)createObject(Schema.SObjectType.csord__Service__c, defaultValues, persist);

        return service;
    }

    public static csord__Service_Line_Item__c createServiceLineItem(String name, csord__Service__c service, String identification, csord__Order_Request__c orderRequest, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        Country_Lookup__c country = new Country_Lookup__c(Name='XYZA',Region__c='AXYZA',Country_Code__c='XYA',CCMS_Country_Code__c='CCXA',CCMS_Country_Name__c='CCAX');
        insert country; 
        Account accobj= new Account(Name='Acc1',Customer_Type_New__c='GW',Selling_Entity__c='Telstra Corporation Limited',Account_Status__c='Active',Type='Customer',Country__c=country.Id,Activated__c=true,Account_ID__c='123',Customer_Legal_Entity_Name__c=name);       
        insert accobj;
        /*Site__c s=new Site__c();
        insert s;*/
        Contact c=new Contact(LastName='SNOW',AccountId=accobj.id,Contact_Type__c='Billing',Country__c=country.id);
        insert c;
        BillProfile__c BP=new BillProfile__c(name='BP1',Billing_Contact__c=c.id,Billing_Entity__c='Telstra Incorporated',Status__c='Active',Account__c=accobj.id);
        insert BP;
        CostCentre__c CS=new CostCentre__c(name='CS1',Cost_Centre_Code__c='4567',BillProfile__c=BP.id);
        insert CS;
        csord__Order_Request__c orderReq=new csord__Order_Request__c(name='OrderName',csord__Module_Name__c='SansaStark123683468Test',csord__Module_Version__c='YouknownothingJhonSwon',csord__Process_Status__c='Requested',csord__Request_DateTime__c = System.now());
        insert orderReq;
        csord__Subscription__c sub = new csord__Subscription__c(name='sub1',csord__Identification__c='Test-Catlyne-4238362',csord__Order_Request__c=orderReq.id);
        insert sub;  
        csord__Order__c ord = new csord__Order__c(csord__Identification__c='Test-Catlyne-4238362',RAG_Order_Status_RED__c=false,RAG_Reason_Code__c='',Jeopardy_Case__c=null,csord__Order_Request__c=orderReq.id);
         insert ord;

        //working with custom settings//
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN port';
        pd.cscfga__Description__c ='Test master IPVPN port';
        insert pd; 
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'ASBR';
        pd1.cscfga__Description__c ='Test master ASBR';
        insert pd1; 
        
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c();
        pd2.name = 'IPC';
        pd2.cscfga__Description__c ='Test master IPC';
        insert pd2;

        cscfga__Product_Definition__c pd3 = new cscfga__Product_Definition__c();
        pd3.name = 'Trans';
        pd3.cscfga__Description__c ='Test master Trans';
        insert pd3;

        cscfga__Product_Definition__c pd4 = new cscfga__Product_Definition__c();
        pd4.name = 'VLan';
        pd4.cscfga__Description__c ='Test master VLan';
        insert pd4;         
        
        
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'IPVPN_Port_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        
        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'ASBR_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1; 
        
        Product_Definition_Id__c pdIdCustomSetting2 = new Product_Definition_Id__c();
        pdIdCustomSetting2.Name = 'IPC_Definition_Id';
        pdIdCustomSetting2.Product_Id__c =pd2.Id;
        insert pdIdCustomSetting2;
        
        Product_Definition_Id__c pdIdCustomSetting3 = new Product_Definition_Id__c();
        pdIdCustomSetting3.Name = 'VPLS_Transparent_Definition_Id';
        pdIdCustomSetting3.Product_Id__c =pd3.Id;
        insert pdIdCustomSetting3;
        
        Product_Definition_Id__c pdIdCustomSetting4 = new Product_Definition_Id__c();
        pdIdCustomSetting4.Name = 'VPLS_VLAN_Port_Definition_Id';
        pdIdCustomSetting4.Product_Id__c =pd4.Id;
        insert pdIdCustomSetting4;
        //custom setting code ends      
        csord__Service__c ser=new csord__Service__c();
        ser.Name='serv1';
        ser.csord__Order_Request__c=orderReq.id;
        ser.csord__Identification__c='Test-Catlyne-4238362';
        ser.csord__Subscription__c=sub.id;
        //ser.Acity_Side__c='';
        //ser.Zcity_Side__c='';
        ser.Product_Id__c='abc';
        ser.csord__Order__c=ord.id;
        ser.csordtelcoa__Replaced_Service__c=null;
        //ser.csordtelcoa__Product_Configuration__c='';
        insert ser; 
        
        defaultValues.put('Name', name);
        defaultValues.put('csord__Service__c', ser.Id);
        defaultValues.put('Account_ID__c',accobj.id);
        defaultValues.put('Net_MRC_Price__c', 0);
        defaultValues.put('Net_NRC_Price__c', 0);
        defaultValues.put('MISC_Charge_Amount__c', 1);
        defaultValues.put('csord__Total_Price__c', 0);
        defaultValues.put('csord__Identification__c', identification);
        defaultValues.put('csord__Order_Request__c', orderRequest.Id);
        defaultValues.put('Is_Miscellaneous_Credit_Flag__c', true);
        defaultValues.put('Bill_Profile__c ', BP.id);
        //defaultValues.put('Cost_Centre__c', CS.id);
        
        //defaultValues.put('Account_ID__c', accobj.id);

        csord__Service_Line_Item__c sli = (csord__Service_Line_Item__c)createObject(Schema.SObjectType.csord__Service_Line_Item__c, defaultValues, persist);
        delete country;
        return sli;
    }

    public static Site__c createSite(String name, Account acc, Country_Lookup__c country, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('AccountId__c', acc.Id);
        defaultValues.put('Address1__c', '1 Main Street');
        defaultValues.put('Country_Finder__c', country.Id);

        Site__c site = (Site__c)createObject(Schema.SObjectType.Site__c, defaultValues, persist);

        return site;
    }

    public static CSPOFA__Orchestration_Process_Template__c createOrchestrationProcessTemplate(String name, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        
        CSPOFA__Orchestration_Process_Template__c processTemplate = (CSPOFA__Orchestration_Process_Template__c)createObject(Schema.SObjectType.CSPOFA__Orchestration_Process_Template__c, defaultValues, persist);
        
        return processTemplate;
    }

    /*
    * **********************************
    * RETRIEVAL METHODS
    * **********************************
    */

    public static csord__Order__c getOrderByIdentification(String identification){
        csord__Order__c ord = [select Id from csord__Order__c where csord__Identification__c = :identification];
        return ord;
    }

    public static cscfga__Product_Configuration__c getProductConfigByName(String name){
        cscfga__Product_Configuration__c productConfig = [select Id, Inflight_Cloning_Id__c, Inflight_Cloned_From_Id__c, cscfga__Key__c, cscfga__Product_Definition__c from cscfga__Product_Configuration__c where Name = :name LIMIT 1];
        return productConfig;
    }

    public static cscfga__Product_Configuration__c getProductConfigById(String productConfigId){
        cscfga__Product_Configuration__c productConfig = [select Id, Inflight_Cloning_Id__c, Inflight_Cloned_From_Id__c, cscfga__Key__c, cscfga__Product_Definition__c from cscfga__Product_Configuration__c where Id = :productConfigId];
        return productConfig;
    }

    public static csord__Service__c getServiceWithLineItemsByName(String name){
        
        List<csord__Service_Line_Item__c>servLineList=[select Id from csord__Service_Line_Item__c where csord__Service__r.name=:name LIMIT 1];
        
        //List<csord__Service__c> service=new List<csord__Service__c>();
        csord__Service__c service=new csord__Service__c ();
        if(servLineList.size()>0){
        service = [select Id, Name, csordtelcoa__Product_Configuration__c, csordtelcoa__Product_Configuration__r.Name, csordtelcoa__Product_Configuration__r.Inflight_Cloning_Id__c, csordtelcoa__Product_Configuration__r.Inflight_Cloned_From_Id__c from csord__Service__c where Name =:name LIMIT 1];
        }
        //if(service.size()>0)return service.get(0);
        return service;
    }

    public static csord__Service__c getServiceWithLineItemsById(String serviceId){
        csord__Service__c service = [select Id, Name, csordtelcoa__Product_Configuration__c, csordtelcoa__Product_Configuration__r.Name, csordtelcoa__Product_Configuration__r.Inflight_Cloning_Id__c, csordtelcoa__Product_Configuration__r.Inflight_Cloned_From_Id__c, (select Id, Name, Net_MRC_Price__c, Net_NRC_Price__c, csord__Is_Recurring__c, csord__Total_Price__c from csord__Service_line_Items__r) from csord__Service__c where Id = :serviceId];
        return service;
    }

    public static csord__Service_Line_Item__c getServiceLineItemByName(String name){
        csord__Service_Line_Item__c sli = [select Id, Net_MRC_Price__c, Net_NRC_Price__c, csord__Is_Recurring__c, csord__Total_Price__c, csord__Service__r.csordtelcoa__Product_Configuration__c from csord__Service_Line_Item__c where Name = :name];
        return sli;
    }

    public static csord__Service_Line_Item__c getServiceLineItemByIdentification(String identification){
        csord__Service_Line_Item__c sli = [select Id, Net_MRC_Price__c, Net_NRC_Price__c, csord__Is_Recurring__c, csord__Total_Price__c, csord__Service__r.csordtelcoa__Product_Configuration__c from csord__Service_Line_Item__c where csord__Identification__c = :identification];

        //System.Debug('Loaded service line item product config for line item ('+ identification + '): ' + sli.csord__Service__r.csordtelcoa__Product_Configuration__c);

        return sli;
    }
    
    public static Site__c getSiteByName(String name){
        Site__c site = [select Id, Name from Site__c where Name = :name];

        return site;
    }

    public static Country_Lookup__c getCountry(String name){
        Country_Lookup__c country = [select Id, Region__c, Country_Code__c, CCMS_Country_Code__c, CCMS_Country_Name__c from Country_Lookup__c where Name = :name];
        return country;
    }

    private static String errorMessage(String operation, List<Database.Error> errors, SObject obj) {
        String objectName = String.valueOf(obj).split(':')[0];
        String msg = operation + ' operation failed for ' +  objectName;
        for (Database.Error error: errors) {
            msg += '\n  [' + String.join(error.getFields(), ', ') + ']: ' + error.getMessage();
        }
        return msg;
    }
}