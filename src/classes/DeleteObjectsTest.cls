@isTest
public class DeleteObjectsTest{
    static testMethod void testTestDeleteObjects(){
        Test.StartTest();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        System.assert(accList[0].Id!=null);
        List<Contact> contact = P2A_TestFactoryCls.getContact(1, AccList);
        System.assert(contact[0].Id!=null);
        String result = DeleteObjects.deleteContacts(contact[0].Id);
        Test.StopTest();
    }
}