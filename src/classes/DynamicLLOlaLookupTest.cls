@isTest(SeeAllData = false)
private class DynamicLLOlaLookupTest{

 private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
     private static List<OLA__c>OLAlist1;
     private static List<OLA__c>OLAlist2;
     private static List<OLA__c>OLAlist3;
     
      private static void initTestData()
      {
       OLAlist1 = new List<OLA__c>{
       new OLA__c(Name = 'OLALookupString')
    
       
       };
       insert OLAlist1;
       system.assertEquals(true,OLAlist1!=null);
        OLAlist2 = new List<OLA__c>{
        new OLA__c(Name = 'OLALookupString2')
       };
       insert OLAlist2;
       system.assertEquals(true,OLAlist2!=null); 
       
      OLAlist3 = new List<OLA__c>{
          new OLA__c(Name = 'OLALookupString3')
       }; 
        insert OLAlist3;
       system.assertEquals(true,OLAlist3!=null); 
      }
      
        private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
              searchFields.put('OLALookupString','Empty');
           
            DynamicLLOlaLookup dynamiclookup = new DynamicLLOlaLookup();
            Object[] data = dynamiclookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            String reqAtts = dynamiclookup.getRequiredAttributes();
            
            // data.clear();
               searchFields.put('OLALookupString2','Empty');
             data = dynamiclookup.doDynamicLookupSearch(searchFields, productDefinitionId);
              // data.clear();
                 searchFields.put('OLALookupString3','Empty');
              data = dynamiclookup.doDynamicLookupSearch(searchFields, productDefinitionId);
              system.assertEquals(true,dynamiclookup!=null); 
                } catch(Exception e){
                    ErrorHandlerException.ExecutingClassName='DynamicLLOlaLookupTest :doDynamicLookupSearchTest';         
                    ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }
}