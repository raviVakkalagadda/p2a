global class DynamicOlaLookup extends cscfga.ALookupSearch {


    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields,
String productDefinitionID){
        List<OLA__c> olas = [SELECT Id, Name, OLAField__c FROM OLA__c
            WHERE Name = :searchFields.get('Intermediary2')];
        IF (olas.isEmpty() ) {
            List<OLA__c> olas2 = [SELECT Id, Name, OLAField__c FROM OLA__c
                WHERE Name = :searchFields.get('Intermediary3')];
            IF (olas2.isEmpty() ) {
                return null;
                }
            Else{
                return olas2;
                }
            }
        Else {
            return olas;
            }
 }

 
     public override String getRequiredAttributes(){
         return '["Intermediary2", "Intermediary3"]';
 }
 
 
}