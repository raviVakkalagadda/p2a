@isTest(SeeAllData = false)
private class DynamicOlaLookupTest{

 private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
     private static List<OLA__c>OLAlist;
     private static List<OLA__c>OLAlist2;
  
      private static void initTestData()
      {
       OLAlist = new List<OLA__c>{
       new OLA__c(Name = 'Intermediary2')
    
       
       };
       insert OLAlist;
       System.assert(OLAlist!=null);
       list<OLA__c> prod = [select id,Name from OLA__c where Name = 'Intermediary2'];
       system.assertEquals(OLAlist[0].Name, prod[0].Name);
       
        OLAlist2 = new List<OLA__c>{
          new OLA__c(Name = 'Intermediary3')
       };
       insert OLAlist2;
        System.assert(OLAlist2!=null);
         list<OLA__c> prod1 = [select id,Name from OLA__c where Name = 'Intermediary3'];
       system.assertEquals(OLAlist2[0].Name, prod1[0].Name);
       
       
 
       
      }
      
        private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
              searchFields.put('Intermediary2','Empty');
           
            DynamicOlaLookup dynamicolookup = new DynamicOlaLookup();
            Object[] data = dynamicolookup.doDynamicLookupSearch(searchFields, productDefinitionId);
            String reqAtts = dynamicolookup.getRequiredAttributes();
                        

           
             searchFields.put('Intermediary3','Empty');
             data = dynamicolookup.doDynamicLookupSearch(searchFields, productDefinitionId);
              system.assertEquals(true,dynamicolookup!=null);        

               
              
                } catch(Exception e){
                    ErrorHandlerException.ExecutingClassName='DynamicOlaLookupTest :doDynamicLookupSearchTest';         
ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }
}