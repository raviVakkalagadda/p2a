@isTest(seeAllData=true)
public class EditContactSiteOverRideControllerTest {
    Static Account a;
    Static Contact c;
    Static Country_Lookup__c cl;
    Static City_Lookup__c city;
    Static Site__c s1;
    static testMethod void testConstrutor(){
    
        Contact cont= getContact();
        Site__c site1  = getSite();
       System.assert(site1!=null);   
       system.assert(cont!=null); 
          system.currentPageReference().getParameters().put('retUrl', '/'+cont.Id);    
          ApexPages.StandardController sc1 = new ApexPages.StandardController(cont);                  
          EditContactSiteOverRideController  nCS= new EditContactSiteOverRideController(sc1);    
         // System.assertEquals(nCS.reDirectPage().getUrl(),'/'+cont.id);
         system.currentPageReference().getParameters().put('retUrl', '/'+site1.Id);    
          ApexPages.StandardController sc2 = new ApexPages.StandardController(site1);                  
          EditContactSiteOverRideController  nCS1= new EditContactSiteOverRideController(sc2);
          nCS.reDirectPage();
          nCS1.reDirectPage();
          
               
    
    }
    
     private static Account getAccount()
        {
            RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
           if(a == null)
           {    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Customer_Legal_Entity_Name__c='Test';
            a.Account_ID__c='123';
            insert a;
            List<account> ac = [select id,name from account where name = 'Test Account Test 1'];
            system.assertequals(a.name,ac[0].name);
            system.assert(a!=null);
           }
        return a;
    }
    private static Contact getContact()
    {
    if( c== Null)
   {
    c = new Contact();
    Account acc = getAccount();
    c.Description='testContact';
    c.AccountId=acc.Id; 
    c.LastName='testName';
    c.Contact_Type__c='Sales';
    insert c;
    system.assert(c!=null);
   }
    return c; 
    }
    
    private static Country_Lookup__c getCountry()
    {
        if(cl == null)
       { 
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'SG';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'SG';
        insert cl;
        system.assert(cl!=null);
        }
        return cl;
    } 
    
    private static City_Lookup__c getCity(){
    if(city == null){
    city = new City_Lookup__c();
     city.Generic_Site_Code__c = 'U12';
     city.name = 'Magu';
     city.City_Code__c='xyz';
     city.OwnerID = userinfo.getuserid();
          insert city;
          system.assert(city!=null);
          }
     return city;
     
    } 
    private static Site__c getSite(){
        s1 = new Site__c();
        s1.name = 'Acc';
        s1.Country_Finder__c = getCountry().id ;
        s1.City_Finder__c = getCity().id;
        s1.Address_Type__c = 'Site Address';
        s1.Address1__c = 'abc';
        s1.Address2__c = 'xyz';
        s1.AccountId__c = getAccount().id;
        insert s1;
        system.assert(s1!=null);
        return s1;
        
    
    }


}