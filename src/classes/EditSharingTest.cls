@isTest(seealldata = false)
public class EditSharingTest
{
  
    public static testMethod void testMyEditSharing() 
    {
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<User> usrList = P2A_TestFactoryCls.get_Users(1);
        profile pr = [select id from profile where name='System administrator'];
        User u = new User(Alias = 'stand', Email='standardugf12gser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing000', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standargfgfduser12345096@testorg.com',EmployeeNumber='1230056');
                
                insert u;
        System.debug('acclist ........'+acclist );
        acclist[0].ownerid = usrList[0].id;
        update acclist;
        List<AccountShare> accountShareList = new List<AccountShare>();
        //List<AccountShare> accountShareList1 = new List<AccountShare>();
        Group grp = new Group();
        grp.Name = 'Test Account Sharing';
        insert grp;
        System.assert(grp!=null);
        
        for(Account a:acclist){
        AccountShare ashare = new AccountShare();
        ashare.AccountId = a.id;
        ashare.UserOrGroupId = u.id;
        ashare.AccountAccessLevel = 'edit';
        ashare.OpportunityAccessLevel = 'edit';
        ashare.Rowcause = 'Manual';
        accountShareList.add(ashare);
        
       }
       insert accountShareList;
        
        Test.StartTest();
        PageReference pageRef = Page.EditAccountSharing;
        pageRef.getParameters().put('accountId',acclist[0].id);
        
        pageRef.getParameters().put('recordId',accountShareList[0].id);
        Test.setCurrentPage(pageRef); 
        EditSharing ES = new EditSharing();
        ES.saveShare();
        ES.cancelShare();
        ES.getLevels();
       Test.StopTest();
       
    }
    
}