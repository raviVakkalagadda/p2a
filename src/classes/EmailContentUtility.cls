public without sharing class EmailContentUtility {
   //private  String[] ccAddress;
   //private  String[] toAddress;
   private final EmailContent emailContent;
   private Map<String,String> maptextVal=null;
   private  final String SUBJECT='SUBJECT';
   private final String AINAME='AINAME';
   private final String OWNERFULLNAME='OWNERFULLNAME';
   private final String DUEDATE='DUEDATE';
   private final String OPPNAME='OPPNAME';
   private final String OPPLINK='OPPLINK';
   private final String AILINK='AILINK';
   private final String COMMENTS='COMMENTS';
   private final String Recnum='Rec#';
   private final String ACCOUNTNAME='ACCOUNTNAME';
    public EmailContentUtility(emailContent emailContent) {

           this.emailContent = emailContent;

       }
    public EmailContentUtility() {


       }
       
   private String templateName='';
   private Schema.SobjectType objectType=null;
   
   /* public EmailContentUtility ccAddress(List<String> ccAddress) {
            this.ccAddress=ccAddress;

           return this;

       }*/
  /*  public EmailContentUtility toAddress(List<String> toAddress) {
      system.debug('======in utility-==='+toAddress);
            this.toAddress=toAddress;

           return this;

       }*/
   
   public EmailContentUtility objectType(Schema.SobjectType val) {

           objectType = val;

           return this;

       }
   public Map<String,String> maptextVal(emailContent emailData) {
          maptextVal=new Map<String,String>();
           maptextVal.put(SUBJECT,emailData.subject);
           maptextVal.put(ACCOUNTNAME,emailData.accountName);
           maptextVal.put(OWNERFULLNAME,emailData.ownerFullName);
           maptextVal.put(DUEDATE,emailData.duedate);
           maptextVal.put(OPPNAME,emailData.opportunityName);
           maptextVal.put(OPPLINK,URL.getSalesforceBaseUrl().toExternalForm() + '/'+emailData.opportunityId);
           maptextVal.put(AILINK,URL.getSalesforceBaseUrl().toExternalForm() + '/'+emailData.RecordId);
           maptextVal.put(Recnum,emailData.RecordName);
           maptextVal.put(COMMENTS,emailData.comments);
           maptextVal.put(AINAME,emailData.RecordName);
           return maptextVal;

       }
    
   public EmailContentUtility templateName(String val) {

           templateName = val;

           return this;

       }
       
   public Class EmailContent{
   private String accountOwnerRegion='';
     private String subject='';
     private String accountName ='';
     private String ownerFullName='';
     private String QueueName='';
     private String duedate='';
     private String opportunityName='';
     private String opportunityId='';
     private String RecordId='';
     private String RecordName='';
     private String comments='';
     public  EmailContent (){
     }
    public EmailContent accountOwnerRegion(String val) {

           accountOwnerRegion = val;

           return this;

       }
   public EmailContent subject(String val) {

           subject = val;

           return this;

       }
   
   public EmailContent accountName(String val) {

           accountName = val;

           return this;

       }
        public EmailContent QueueName(String val) {

           QueueName = val;

           return this;

       }
       
   
   public EmailContent ownerFullName() {

           ownerFullName = this.QueueName+' '+this.accountOwnerRegion;

           return this;

       }
   
     
   public EmailContent duedate(String val) {

           duedate = val;

           return this;

       }
   
   public EmailContent opportunityName(String val) {

           opportunityName = val;

           return this;

       }
   
   public EmailContent opportunityId(String val) {

           opportunityId = val;

           return this;

       }
   
   public EmailContent RecordId(String val) {

           RecordId = val;

           return this;

       }
   
   public EmailContent RecordName(String val) {

           RecordName = val;

           return this;

       }
   
   public EmailContent comments(String val) {

           comments = val;

           return this;

       }
       
       
 
   }
   
public void formnSendEmail(String [] toAddress,String [] ccAddress){
     //this.emailContent
EmailTemplate emailTemp=[Select id,Name,subject,body from EmailTemplate where DeveloperName=:this.templateName];
String replyToAddress = userinfo.getUserEmail();
system.debug('=====toAddress=== final'+toAddress);
String[] toAddresses = toAddress!=null && toAddress.size()>0?toAddress:getEmailAddess().split(':', 0);
system.debug('=====this.ccAddress=== final'+ccAddress);
String[] ccAddressList=ccAddress;
EmailUtility emailUtil = new EmailUtility(toAddresses);

emailUtil.plainTextBody(replaceContent(emailTemp.body))

              .senderDisplayName(userinfo.getName())

              .subject(replaceContent(emailTemp.subject))

              .useSignature(true)

              .replyTo(replyToAddress)
              .CcAddresses(ccAddressList)

              .sendEmail();
     
     
     
   }
 private String replaceContent(String str){
   maptextVal=new Map<String,String>();
   maptextVal=maptextVal(this.emailContent);
   for(String textdata:maptextVal.keyset()){
     if(str.contains(textdata)){
       system.debug('========textdata====='+textdata+'====='+maptextVal.get(textdata));
       String replce=maptextVal.get(textdata);
       str=str.replace(textdata,replce);
       system.debug(str+'===pppppp');
     }
     
   }
   return str;
 }  
   
     public String getEmailAddess(){
 // query to get all queues corresponding to Bill profile Object
          Map<String, QueueSobject> QueueMap = QueueUtil.getQueuesMapForObject(this.objecttype);
            
            //Query the User object  to get sales User who created the Order
           
             Id queid;
            String emailAddresses = '';
            String USerRegion='';
            //Setting for email attributes
            Map<String, Id> queMap= new Map<String, Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:QueueMap.values()){
                if((q.Queue.Name).contains(this.emailContent.accountOwnerRegion) && (q.Queue.Name).contains(this.emailContent.QueueName)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                   // System.debug('queue name ..'+q.Queue.Name);
                }
            }
            String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                     USerRegion=usrObj.Region__c;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                     USerRegion=usrObj.Region__c;
                }
            }
            

           System.debug('emailadresses..'+emailAddresses);
           return emailAddresses;
    
    }
   
    public String getEmailAddess(ID QueueId){
            String emailAddresses = '';
            String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+QueueId+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            if(list_GM.size()>0){
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            }else{
              lst_Ids.add(QueueId);
              
            }
            
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                }
            }
            

           System.debug('emailadresses..'+emailAddresses);
           return emailAddresses;
    
    }
 
   
       
    
}