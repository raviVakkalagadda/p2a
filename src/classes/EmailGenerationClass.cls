Public Class EmailGenerationClass{

        public static void emailSending(User user,Id Id){
         String subject;
         String body,body1,body2,body3,body4,body5;
         String emailAddresses= '';
         
         system.debug('Inside Email generation class');
         order_line_item__c Objoli=[select id,Name,parentOrder__C,Bill_profile__C,Billing_Entity__c from order_line_item__c where id=:Id];
         
        // query to get all queues corresponding to Bill profile Object
            List<QueueSobject> que = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='BillProfile__c'];
            
           // User usr =[Select Name,email,Region__c from User where id= : user.id];
            
           // User usr = usrLst[0];
            Id queid;
            String billTeam;
            
            // IR 215 start
            String queueName = null;
            String billingEntiy = Objoli.Billing_Entity__c;
            
            if(billingEntiy!=null && System.Label.PACNET_Entities.contains(billingEntiy)){
                  queueName = 'Billing - PACNET';
              }else{                
                queueName = user.Region__c;
              }
           // IR 215 end
            
            
            Map<String,Id> queMap= new Map<String,Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:que){
                if((q.Queue.Name).contains(queueName)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                    billTeam = q.Queue.Name;
                   // System.debug('queue name ..'+q.Queue.Name);
                }
            }
            
           String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : [Select Id,name,Email from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                }
            }
            
            //Getting sales force URL of Instance
            String instanceURL = URL.getSalesforceBaseUrl().toExternalForm() + '/' ;
            Subject = System.Label.Subject1 + billTeam + System.Label.Subject2;
            
            body1  = 'Dear ' + billTeam + ',';
            body3 = instanceURL + Objoli.Bill_Profile__c;
            body4 = 'Once you have activated this bill profile, please intimate the service delivery user '+ user.name + ' at his email address ' + user.email;
            body5 = +'\n'+'Please ignore this email if this bill profile is already activated.';
            
             if(emailAddresses  != ''){
             system.debug('email Address'+ emailAddresses);
                Order__c ordr = [Select id,name from Order__c where id  =: Objoli.parentOrder__c];
                body2 = 'Following bill profiles requires immediate activation as the service delivery user '+ user.name + ' is waiting for completion of an order line item '
                         + Objoli.name+ ' for an order '+ ordr.name;  
                         
                body = body1+'\n'+body2 +'\n' + body3 + '\n'+ body4 + '\n' + body5;
                GenericEmailSending  genericObj = new GenericEmailSending();
                 system.debug('genericObj'+ body);
                genericObj.send(emailAddresses,subject,body); 
               
            
            }
           
    
    }
      public void insertDynDataInBody(Map<Id,User> mapIdUser,List<Id> userLst,List<Action_Item__c> actionItemList,String body){
        try{
        EMailVO prEMailVO= new EMailVO();
       
       // prEMailVO.to=new String[]{actionItemList[0].Order_Action_Item__r.CreatedBy.Email};
        prEMailVO.subject='Salesforce:Termination Order # '+actionItemList[0].Order_Action_Item__r.Name+' has been cancelled';
        String body1 = 'Hi '+actionItemList[0].Owner.Name+','+'</br>'+'</br>';
                
        String body2 = 'This is to inform you that following action item(s) are not required to complete as the Termination Order # '+actionItemList[0].Order_Action_Item__r.Name+' has been cancelled.'+'</br>';
       // String body3=URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Id+'</br></br>';
        prEMailVO.body=body1+body2+'Details as follows.'+'</br>'+'</br>'+body;
        List<String> emailList=new String[]{};
        for(Id id:userLst){
            
        emailList.add(mapIdUser.get(id).Email); 
        }
        prEMailVO.to=new String[]{'suparna.sikdar@accenture.com'};
        prEMailVO.to=emailList;
        sendMail(prEMailVO);
        }catch (Exception ex) {
            ErrorHandlerException.ExecutingClassName='EmailGenerationClass:insertDynDataInBody';
            ErrorHandlerException.objectList=actionItemList;
            ErrorHandlerException.sendException(ex);
            System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex);
        }
    }
        public  Boolean sendMail(EMailVO prEMailVO){
        Boolean mailSent=false;
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            system.debug('=====prEMailVO.to==='+prEMailVO.to);
            mail.setToAddresses(prEMailVO.to);
           
            mail.setSubject(prEMailVO.subject);
            mail.setHtmlBody(prEMailVO.body);
            List<Messaging.SendEmailResult> results= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            mailSent=results.get(0).isSuccess();
            if(!mailSent){
                System.Debug('Errors while sending Mail ..'+results.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            ErrorHandlerException.ExecutingClassName='EmailGenerationClass:sendMail';
            ErrorHandlerException.sendException(ex);
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent;
    }
 
      public void getEmailList(Map<Id,List<Action_Item__c>> mapQueueIdAIList){
        
        List<String> dynDatas = new List<String>();
        Set<Id> idSet=new Set<Id>();
        List<GroupMember> groupMemberList = [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =:mapQueueIdAIList.keyset()];
        List<Id> gmId=new List<Id>();
         List<User> userList = [SELECT Id FROM User where Id=:mapQueueIdAIList.keyset()];
        Map<Id,List<Id>> mapQuserList=new Map<Id,List<Id>>();
        for(Id id:mapQueueIdAIList.keyset()){
            gmId=new List<Id>();
            for(GroupMember gm:groupMemberList){
                if(gm.GroupId==id){
                    gmId.add(gm.UserOrGroupId);
                }
                idSet.add(gm.UserOrGroupId);
            }
            
            mapQuserList.put(id,gmId);
        }
         for(Id id:mapQueueIdAIList.keyset()){
             gmId=new List<Id>();
            for(User usr:userList){
                if(usr.Id==id && usr.Id!=UserInfo.getUserId()){
                    
                      gmId.add(usr.Id);
                }
                  idSet.add(usr.Id);
            }
            
             mapQuserList.put(id,gmId);
        }
        List<User> lst_UserObj = [Select Id,name,Email from User where id in :idSet];
        Map<Id,User> mapIdUser=new Map<Id,User>(lst_UserObj);
        for(Id qid:mapQueueIdAIList.keyset()){
        //get list of all the ordered items
        string htmlOrderedList='';
        string htmlOrderedList1='';
        string htmlOrderedList2='';
        string htmlOrderedList3='';
        string htmlOrderedList4='';
         htmlOrderedList1='<style type="text/css">'+
        '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
        '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
        '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
        '</style>';
        
        htmlOrderedList2='<table class="tg">'+
        '<tr>'+
        '<th class="tg-031e"><strong>Action Item</strong></th>'+
        '<th class="tg-031e"><strong>Service ID</strong></th>'+
        '<th class="tg-031e"><strong>Resource ID</strong></th>'+
        '<th class="tg-031e"><strong>Overall status</strong></th>'+
        '</tr>';
        for(Action_Item__c aIObj:mapQueueIdAIList.get(qid))
        {
       
         htmlOrderedList4+='<tr>'+
        '<td class="tg-031e">'+'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Id+'>'+aIObj.Name+'</a></td>'
         +'<td class="tg-031e">'+(aIObj.Service__c==null?'N/A':'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Service__c+'>'+aIObj.Service__r.Name+'</a>')+'</td>'
         +'<td class="tg-031e">'+(aIObj.Resource__c==null?'N/A':'<a href='+URL.getSalesforceBaseUrl().toExternalForm() + '/'+aIObj.Resource__c+'>'+aIObj.Resource__r.Name+'</a>')+'</td>'
         +'<td class="tg-031e">'+(aIObj.Status__c==null?'N/A':'Cancelled')+'</td>'
         +'</tr>';
        
        }
        
        String htmlOrderedList5='</table>';
        
        
    
    
                 
        //insert dynamic data in email html body
        system.debug('=====mapIdUser==='+mapIdUser);
         system.debug('=====mapQuserList.get(qid)==='+mapQuserList.get(qid));
           system.debug('=====mapQueueIdAIList.get(qid)==='+mapQueueIdAIList.get(qid));
        insertDynDataInBody(mapIdUser,mapQuserList.get(qid),mapQueueIdAIList.get(qid),htmlOrderedList1+htmlOrderedList+htmlOrderedList2+'</br>'+htmlOrderedList3+htmlOrderedList4+htmlOrderedList5+'Thanks,'+'</br>'+'***This is an auto-generated notification, please do not reply to this email***.');
        //send email
        
        } 
     }
      public class EmailVO{
    public String sender{get;set;}
    public String senderDispName{get;set;}
    public String[] to{get;set;}
    public String[] cc{get;set;}
    public String[] bcc{get;set;}
    public String subject{get;set;}
    public String body{get;set;}
    public String header{get;set;}
    public String footer{get;set;}
    public String module{get;set;}
    public String originator{get;set;}
    public String replaceCharStart{get;set;}
    public String replaceCharEnd{get;set;}
    public Boolean isCIC{get;set;}
    }

}