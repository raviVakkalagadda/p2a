@isTest(seealldata=false)
public class EmailGenerationClsTest {
 
 Static Account acc1 = getAccount();

static opportunity opp1=getOpportunity();

  private static Account getAccount(){                
        Account acc = new Account();            
        Country_Lookup__c c = getCountry();            
        acc.Name = 'Test Account';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = c.Id;            
        acc.Selling_Entity__c = 'Telstra INC';  
        acc.Activated__c= true;  
        acc.Account_ID__c = '9090';  
        acc.Customer_Legal_Entity_Name__c = 'Test' ;     
        insert acc;  
         system.assertEquals(true,acc!=null); 
        return acc;    
   } 
   private static Country_Lookup__c getCountry(){        
           Country_Lookup__c country = new Country_Lookup__c();            
           country.CCMS_Country_Code__c = 'HK';            
           country.CCMS_Country_Name__c = 'India';            
           country.Country_Code__c = 'HK';            
           insert country;
           system.assertEquals(true,country!=null);          
           return country;    
         } 
           private static Opportunity getOpportunity(){                 
        Opportunity opp = new Opportunity();            
       // Account acc = getAccount();           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc1.Id;   
        //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement         
        opp.StageName = 'Identify & Define' ;  
        opp.Stage__c= 'Identify & Define' ;          
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Complex';
        opp.Estimated_MRC__c=8;
        opp.Estimated_NRC__c=2;
        opp.ContractTerm__c='1';
        opp.Order_Type__c='New';
        //opp.Stage_Gate_1_Action_Created__c = true;         
        //opp.Approx_Deal_Size__c = 9000;
         insert opp;  
         system.assertEquals(true,opp!=null);         
        return opp;    
       } 
       private static cscfga__Product_Basket__c  getProductbasket(){
      cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c ();
      pb.cscfga__Opportunity__c =opp1.id;
      pb.csordtelcoa__Synchronised_with_Opportunity__c =true;
      pb.csbb__Synchronised_With_Opportunity__c =true;
     
     return pb;  
       }


 static void emailgeneration() 
  { 
  
   EmailGenerationCls emailgen = new EmailGenerationCls(); 
   
   Test.startTest();
       
    Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
    insert cntryLkupObj;
    
    System.assertEquals('HK',cntryLkupObj.Name ); 

    
    City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
    insert cityObj;
    
   System.assertEquals('Bangalore',cityObj.Name ); 

        
    Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
    insert accObj;
    
    System.assertEquals('abcd',accObj.Name ); 

    
    Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
    insert contObj;
    
    System.assertEquals('tech',contObj.LastName); 

     
      
    Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
    insert oppObj;
    
          
    Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
    insert siteObj;
    
    System.assertEquals('Test_site',siteObj.Name); 

    
    BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Telstra Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
    
    insert   billProfObj; 
    
    System.assertEquals('test',billProfObj.Name); 

    
    Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
    insert orderObj;
    
    System.assertEquals(accObj.Id ,orderObj.Account__c); 
    
    
    order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
    insert ordLineItemObj;
    
    System.assertEquals(ordLineItemObj.ParentOrder__c ,OrderObj.Id); 
    
    
    Profile profObj = [select Id from Profile  where name ='Standard User'];
    User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
            email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
    
    System.assertEquals(profObj.id ,userObj.profileId); 

    
   
    Test.stopTest();    
  
  }

   static testmethod void insertDynDataInBody() 
   {
   
       String Accountregion= 'Australia';
       EmailGenerationCls emailgen = new EmailGenerationCls ();
       EmailGenerationCls.EmailVO emailVOObj = new EmailGenerationCls.EmailVO();
        
             
       Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
       insert cntryLkupObj;
       
      System.assertEquals(cntryLkupObj.Name ,'HK'); 

        
       Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');
       insert accObj;
       
      System.assertEquals(accObj.Name ,'abcd'); 

       
       
       Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(),
                                         Account__c =accObj.Id);
       insert orderObj;
      
      System.assertEquals(orderObj.Account__c ,accObj.Id); 

       
       Action_Item__c actionItemObj = new Action_Item__c(Order_Action_Item__c=orderObj.Id);
       List<Action_Item__c> actionItemList = new List<Action_Item__c>();
       actionItemList.add(actionItemObj);
        insert actionItemList;
       //actionItemList[0].Order_Action_Item__c = orderObj.Id;
       emailVOObj.subject = 'Hello'+actionItemList[0].Order_Action_Item__c;
       String body ='testing';
       emailVOObj.body = body;
       emailVOObj.sender ='Telstra';
       emailVOObj.senderDispName ='Telstra Account';
       emailVOObj.header = 'Head';
       emailVOObj.footer = 'foot';
       emailVOObj.module ='module Test';
       emailVOObj.originator ='test';
       emailVOObj.replaceCharStart ='test';
       emailVOObj.replaceCharEnd ='testingEnd';
       emailVOObj.isCIC = true;
       String[] toString = new String[]{'to@gmail.com'};
       String[] ccString = new String[]{'cc@gmail.com'};
       String[] bccString = new String[]{'bcc@gmail.com'};
       emailVOObj.to = toString;
       emailVOObj.cc = ccString;
       emailVOObj.bcc = bccString;
       
       Test.startTest();
       Profile profObj = [select Id from Profile  where name ='Standard User'];
       User userObj = new User(profileId = profObj.id, username = 'mohantesh12@telstra.com',
            email = 'mohantesh12@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser12',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
              
        System.assertEquals(userObj.username ,'mohantesh12@telstra.com'); 
    
            
       Map<Id,User> mapIdUser = new Map<Id,User>();
       mapIdUser.put(userObj.Id, userObj);
       Id idObj = userObj.Id;
       List<Id> userLst = new List<Id>();
       
        list<case> CaseList = new list<case>();
            case cs = new case();
            cs.Status ='Unassigned';
            cs.Send_Overdue_Email__c  = false;
            cs.Is_Feasibility_Request__c = true;
            cs.Is_Resource_Reservation__c = true ;
            cs.type = 'enrichment';
            Date d = date.today(); 
            cs.Due_Date__c = d.adddays(-1);
            CaseList.add(cs);
          insert CaseList;
          system.assertEquals(true,CaseList!=null); 
       
       User userObj1 = new User(profileId = profObj.id,username = 'sumit@telstra.com', alias='nuser12',  lastname='Suman',firstname='Sumit',
            email = 'sumit@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles', EmployeeNumber='1234589', Region__c='Australia');
            insert userObj1;
       
       //emailgen.insertDynDataInBody(mapIdUser, userLst, actionItemList, body) ;
       emailgen.sendMail(emailVOObj,Accountregion,UserInfo.getuserid()) ; 
       emailgen.insertDynDataInBody(CaseList, body) ;
       emailgen.getEmailList(CaseList);
       Test.stopTest(); 
       
   }
  private static testMethod void Test() {
        Exception ee = null;
        integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assertEquals(true,userid!=null); 
            emailgeneration();
            } catch(Exception e) {
                ErrorHandlerException.ExecutingClassName='EmailGenerationClsTest :Test';         
                ErrorHandlerException.sendException(e); 
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
@isTest static void testExceptions(){
         EmailGenerationCls alls=new EmailGenerationCls();    
         AllSOVPerfTrackingCurrencyCheckHandler alls1=new AllSOVPerfTrackingCurrencyCheckHandler();
             
         try{alls.insertDynDataInBody(null,null);}catch(Exception e){}
         try{alls1.updateCurrencybeforeInsert(null);}catch(Exception e){}
          
     }  
  
}