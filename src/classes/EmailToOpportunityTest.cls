@isTest(seeAlldata=true)
public class EmailToOpportunityTest {

    static testmethod void emailOpportunity(){
        
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        EmailToOpportunity emailtoopp=new EmailToOpportunity();
        Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
        inAtt.body = blob.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/text';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt };
              
        email.Subject= 'Opp-00088477';
        email.plainTextBody='Thisistestadatafromsalesforce';
        email.fromAddress='sekha123r@gmail.com';
        
        String oppId ='Opp-00088477';
        String conId = null; 
        
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh1234@telstra.com',
            email = 'mohantes232h@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
    
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                      Email=email.fromAddress);
        List<Contact> contactList = new List<Contact>();
        contactList.add(contObj);
        insert contactList;
        system.debug('ContactList SIze::'+contactList.size());
        
        conId = contactList.get(0).id;
        Opportunity oppObj = new Opportunity(Name='Test Opportunity',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c='Identify & Define',
                                           QuoteStatus__c = 'Approved', Sales_Status__c = 'Won', Win_Loss_Reasons__c='Product', 
                                           Order_Type__c ='New',ContractTerm__c = '29',Total_NRC__c=24,Total_MRC__c=35,
                                            OwnerId = userObj.Id);
        
        insert oppobj;
        system.assert(oppobj!=null);
      
        
        Task tsk = new Task(Description =  email.plainTextBody,Priority = 'Normal',Status = 'Inbound Email',
                       Subject =  email.Subject,IsReminderSet = true,ReminderDateTime = System.now()+3,
                       WhatId  = oppObj.Id,WhoId  = conId,Type = 'Email',
                       ActivityDate = System.today()+5);
        insert tsk;
        system.assert(tsk!=null);
        
        Test.startTest();
        emailtoopp.handleInboundEmail(email,env);
        Test.stopTest();
    }
    static testmethod void emailOrder(){
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        EmailToOpportunity emailtoopp=new EmailToOpportunity();
        Messaging.InboundEmail.BinaryAttachment inAtt = new Messaging.InboundEmail.BinaryAttachment();
        inAtt.body = blob.valueOf('test');
        inAtt.fileName = 'my attachment name';
        inAtt.mimeTypeSubType = 'plain/text';
        email.binaryAttachments = new Messaging.inboundEmail.BinaryAttachment[] {inAtt };
        
        email.Subject= 'ODR-18841';
        email.plainTextBody='Thisistestadatafromsalesforce1';
        email.fromAddress='mohantes2yhg32h@telstra1.com';
        
       
        String conId = null; 
        
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'mohantes2yhg32h@telstra1.com',
            email = 'mohantes2yhg32h@telstra1.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
    
        Account accObj = new Account(name = 'abcdefd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited1',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        system.assert(accObj!=null);
        
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                      Email=email.fromAddress);
        List<Contact> contactList = new List<Contact>();
        contactList.add(contObj);
        insert contactList;
        
        Opportunity oppObj = new Opportunity(Name='Test Opportunity',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c='Identify & Define',
                                           QuoteStatus__c = 'Approved', Sales_Status__c = 'Won', Win_Loss_Reasons__c='Product', 
                                           Order_Type__c ='New',ContractTerm__c = '29',Total_NRC__c=24,Total_MRC__c=35,
                                            OwnerId = userObj.Id);
        
        insert oppObj;
        
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id, OwnerId = userObj.Id,
              Opportunity__c=oppObj.Id);
        insert orderObj; 
         String ordId = 'ODR-18841';
        
        Task tsk = new Task(Description =  email.plainTextBody,Priority = 'Normal',Status = 'Inbound Email',
                       Subject =  email.Subject,IsReminderSet = true,ReminderDateTime = System.now()+3,
                       WhatId  = oppObj.Id,WhoId  = conId,Type = 'Email',
                       ActivityDate = System.today()+5);
        insert tsk;
        
        
        Test.startTest();
        emailtoopp.handleInboundEmail(email,env);
        Test.stopTest();
    }
    static testmethod void emailtoopp3(){
        Messaging.InboundEmail email = new Messaging.InboundEmail();
        Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
        EmailToOpportunity emailtoopp=new EmailToOpportunity();
        
        email.Subject= 'Account:103560';
        email.plainTextBody='Thisistestadatafromsalesforc1e';
        email.fromAddress='sekha123reqr@gmail.com';
        String accid1 ='103560';
        String conId = null; 
         
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh12934@telstra.com',
            email = 'mohantesh12934@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
    
       Account accObj = new Account(name = 'abcd3',Customer_Type_New__c ='GW',OwnerId = userObj.Id,
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='103560',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj; 
        system.assert(accObj!=null);
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                      Email=email.fromAddress);
        List<Contact> contactList = new List<Contact>();
        contactList.add(contObj);
        insert contactList;
        
        conId = contactList.get(0).id; 
        
        Opportunity oppObj = new Opportunity(Name='Test Opportunity',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c='Identify & Define',
                                           QuoteStatus__c = 'Approved', Sales_Status__c = 'Won', Win_Loss_Reasons__c='Product', 
                                           Order_Type__c ='New',ContractTerm__c = '29',Total_NRC__c=24,Total_MRC__c=35,
                                            OwnerId = userObj.Id);
        
        insert oppobj;
        system.assert(oppobj!=null);
        
        Task tsk = new Task(Description =  email.plainTextBody,Priority = 'Normal',Status = 'Inbound Email',
                       Subject =  email.Subject,IsReminderSet = true,ReminderDateTime = System.now()+3,
                       WhatId  = oppObj.Id,WhoId  = conId,Type = 'Email',
                       ActivityDate = System.today()+5);
        insert tsk;
        
        System.debug('AccountId+++++'+accobj.Id);
        //String accId =accObj.Account_ID__c;
         //System.debug('ACCid============='+accId);
        Test.startTest();
        emailtoopp.handleInboundEmail(email,env);
        Test.stopTest();
        
    }
    
}