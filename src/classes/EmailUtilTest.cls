@isTest(SeeAllData=false)
private class EmailUtilTest{
    static testMethod void send(){
        Test.startTest();
        P2A_TestFactoryCls.sampleTestData();
        String Subject = 'Test Mail';
        String emailAddresses = 'abc@accenture.com';
        String body = 'This is test for email sending';
        List<User> usrLst =[Select Name,email,Region__c from User where id= :UserInfo.getUserId()];
         system.assertEquals(true,usrLst!=null); 
        list<String> slist = new list<string>();
        slist.add('abc@accenture.com');
        slist.add('abc@accenture.com');
        slist.add('abc@accenture.com');
        slist.add('abc@accenture.com');
        
        String emailAddress = usrLst[0].Email;
        
        EmailUtil.EmailData mailData = new EmailUtil.EmailData();
        mailData.addToAddress(emailAddresses);
        mailData.addToAddresses(slist);
        mailData.addCCAddress(emailAddresses);
        mailData.addCCAddresses(slist);
        //EmailUtil.sendEmail(mailData);
        system.assertEquals(true,mailData!=null); 
      
    Test.stopTest();    
    }

}