@isTest(seeAllData=False)
private class ErrorControllerTest{

    
@TestSetup static void SetUp(){
//creating EHM custom setting   
ErrorHandlerMechanism__c EHM=new ErrorHandlerMechanism__c();
EHM.name='Logger';
EHM.EmailRecipient__c='ritesh.kumar.yadav@accenture.com';
EHM.EnableMailer__c=true;
EHM.EnableLog__c=true;
EHM.Last_Modified_By__c='Ritesh';
EHM.Last_Modified_Time__c=system.now();
insert EHM;
system.assertEquals(true,EHM!=null); 
//creating error records
List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1); 
List<Solutions__c>solList=new List<Solutions__c>();

for(integer i=0;i<40;i++){
Solutions__c sol=new Solutions__c(Account_Name__c=ListAcc[0].id);
solList.add(sol);
}
insert solList;
system.assertEquals(true,solList!=null);
List<Id>solIdList=new List<Id>();
for(solutions__c sol:solList){
    solIdList.add(sol.id);
}

try{

integer i=1/0;//will cause the exception

}catch(Exception e){
ErrorHandlerException.ExecutingClassName='AllServiceTriggerHandler:afterUpdate';
ErrorHandlerException.objectList=solList;
ErrorHandlerException.objectIdList=solIdList;
ErrorHandlerException.sendException(e);
ErrorHandlerException.logException('Some Exception Name',e.getMessage(),null,solIdList,ErrorHandlerException.ExecutingClassName);
}
    
}   

@isTest static void ErrorControllerTestMethod(){
//coverage starts here

test.startTest();
ErrorController ecObj=new ErrorController();

//set the fields
ecObj.Sort_Name='true';
ecObj.Sort_Log_Time='true';
ecObj.Sort_Log_Date='true';
ecObj.Sort_Class_Name='true';
ecObj.Sort_Object_Id='true';
ecObj.Sort_Object_Name='true';
ecObj.Sort_Object_Type='true';
ecObj.Sort_Stack_Trace='true';
ecObj.ErrorList=new List<Error__c>();
ecObj.deleteList=new List<Error__c>();
ecObj.varList=new List<String>();
ecObj.logsStatus='logging';

pagereference pg1=ecObj.getQueryFromSelectedFields();
system.assertEquals(true,ecObj!=null);
test.StopTest();    
}

@isTest static void getQueryStringTest(){
test.startTest();
ErrorController ecObj=new ErrorController();

ecObj.Field_Name=true;
ecObj.Field_Log_Time=true;
ecObj.Field_Log_Date=true;
ecObj.Field_Class_Name=true;
ecObj.Field_Object_Id=true;
ecObj.Field_Object_Name=true;
ecObj.Field_Object_Type=true;
ecObj.Field_Stack_Trace=true;


pagereference pg1=ecObj.getQueryString();
pagereference pg2=ecObj.getfieldsFromSobject();
pagereference pg3=ecObj.updateLoggingConfig();
pagereference pg4=ecObj.test();
List<SelectOption>s1=ecObj.getitems();
List<SelectOption>s2=ecObj.getInputItems();
List<SelectOption>s3=ecObj.getOutputItems();
System.assert(ecObj.AvailableFieldList.size()>0);
ecObj.SobjectFieldValMap.put(ecObj.AvailableFieldList[0],ecObj.AvailableFieldList[0]);
pagereference pg5=ecObj.AddFields();
System.assert(ecObj.SelectedFieldMap.size()>0);
ecObj.removableFieldList.add(ecObj.SelectedFieldMap.values()[0]);
pagereference pg6=ecObj.RemoveFields();

//for negative scenario
ecObj.SobjectFieldValMap=new Map<String,String>();
List<SelectOption>s4=ecObj.getInputItems();
system.assertEquals(true,ecObj!=null);
test.StopTest();    
}

@isTest static void getRecordsTest(){
test.startTest();
ErrorController ecObj=new ErrorController();
ecObj.queryString='select name,Log_Time__c,Log_Date__c,Class_Name__c,Object_ID__c,Object_Name__c,Object_Type__c,Stack_Trace__c from error__c';
pagereference pg1=ecObj.getRecords();
pagereference pg2=ecObj.exportData();
pagereference pg3=ecObj.editRecords();
pagereference pg4=ecObj.updateRecords();
ecObj.getPaginationRecords(1);
ecObj.firstPage();
ecObj.lastPage();
ecObj.nextPage();
ecObj.previousPage();

System.assert(ecObj.ErrorWrapList.size()>0);
ecObj.ErrorWrapList[0].selectedProduct=true;

pagereference pg5=ecObj.deleteSelectedRecords();
system.assertEquals(true,ecObj!=null);
test.StopTest();
}

@isTest static void ExceptionTest(){
ErrorController ecObj=new ErrorController();
ecObj.queryString='abcd';
ecObj.SelectedFieldMap=null;    
system.assertEquals(true,ecObj!=null);
try{pagereference pg1=ecObj.getRecords();}catch(Exception e){
ErrorHandlerException.ExecutingClassName='ErrorControllerTest :ExceptionTest';         
ErrorHandlerException.sendException(e); 
}
try{pagereference pg2=ecObj.getQueryFromSelectedFields();}catch(Exception e){
ErrorHandlerException.ExecutingClassName='ErrorControllerTest :ExceptionTest';         
ErrorHandlerException.sendException(e); 
}  
}

}