@isTest(SeeAllData=false)
public class ErrorhandlingEmailTest {
    
        public static testMethod void ErrorhandlingEmailTest_Method1(){
        p2a_testfactorycls.disableall(userinfo.getuserid());
        P2A_TestFactoryCls.sampleTestData();
        List<csord__Order_Request__c> requestList = P2A_TestFactoryCls.getorderrequest(1);       
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, requestList);
        List<csord__Service__c> serviceList = P2A_TestFactoryCls.getService(1, requestList, subscriptionList); 
        system.assert(serviceList!=null);
        string billReason='EMEA';        
        Id pid = serviceList[0].id;
        //serviceList[0].Name = 'TestName';
        //update serviceList[0];
        
        ErrorhandlingEmail.emailSending(billReason, pId);
        ErrorhandlingEmail.emailSendingP2OError(billReason, pId);
        //ErrorhandlingEmail.EmailOnOrderType(billReason, pId);
          p2a_testfactorycls.enableall(userinfo.getuserid());
    }
}