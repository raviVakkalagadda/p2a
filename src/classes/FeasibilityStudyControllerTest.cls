@isTest(SeeAllData = true)
private class FeasibilityStudyControllerTest {
    
     Static Account a;
     Static Opportunity opp = getOpportunity();
     Static Action_Item__c ati;
     Static Country_Lookup__c cl;  
     
    static testMethod void myFeasibilityStudyControllerTest() {
    try{        
       Action_Item__c ati1 = getActionItem();        
       //opp= getOpportunity();  
       OpportunityLineItem oli = getOpportunityLineItem();
       //oli.OpportunityId = opp.Id;  
       system.currentPageReference().getParameters().put('actId',ati1.id);
       ApexPages.StandardController sc1 = new ApexPages.StandardController(opp); 
       FeasibilityStudyController fSC = new FeasibilityStudyController();
       FeasibilityTask__c ftask = new FeasibilityTask__c();
       // Set the Feasibility Task Owner id
         List<QueueSobject> objQueue = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='FeasibilityTask__c'];
         ftask.ownerId = objQueue[0].QueueId;
         fSC.ftask = ftask;
       List<FeasibilityStudyController.OppProductWrapper> accList = fSC.getOppProducts();
       for (FeasibilityStudyController.OppProductWrapper oppwrap : accList){
           oppWrap.selected =true;
           system.assert(accList!=null);
       }
       fSC.getOppProducts();
       fSC.getSelOppProducts();
       fSC.getSelected();
       fSC.createFeasibilityStudy();
       fSC.cancelRequest();
       fSC.getPriorities();
       fSC.GetSelectedAccounts();
     }catch(Exception e){}        
    }
    
    private static Opportunity getOpportunity() {
        Opportunity o = new Opportunity();
        o.Name = 'UnitTest Opp';
        o.AccountId = getAccount().Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        o.StageName = 'Identify & Define';
        o.Stage__c='Identify & Define';
        o.CloseDate = System.today();
        insert o;
        List<Opportunity> op = [select id,name from Opportunity where name = 'UnitTest Opp'];
        system.assertequals(o.name,op[0].name);
      system.assert(o!=null); 
        
        return o;
    }
    
     private static Action_Item__c getActionItem() {
        Action_Item__c ati1 = new Action_Item__c();
        //opp = getOpportunity();
        ati1.Subject__c = 'Request Feasibility Study';
        ati1.Opportunity__c = opp.Id;
        ati1.Account__c = getAccount().Id;
        ati1.Status__c = 'Assigned';
        ati1.Region__c = 'EMEA';
        
        insert ati1;
      system.assert(ati1!=null); 
        
        return ati1;
     }
     
     private static Account getAccount()
        {   if(a == null){
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            //a.Selling_Entity__c = 'Telstra INC';
            //a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.Customer_Legal_Entity_Name__c='Test';
            a.Account_Status__c ='Active';
            a.Account_ID__C='789';
            insert a;
        system.assert(a!=null); 
          
        }
        return a;
    }
    
    private static Country_Lookup__c getCountry()
    {
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'SG';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'SG';
        insert cl;
        system.assert(cl!=null);
        
        return cl;
    } 
    
    private static OpportunityLineItem getOpportunityLineItem(){
        List<OpportunityLineItem> oli1 = new List<OpportunityLineItem>();
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.CPQItem__c = '1';
        oli.Quantity = 5;
        oli.Is_GCPE_Shared_with_Multiple_Services__c = 'NA';
        oli.Quantity__c = 1;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';
        oli1.add(oli);
        insert oli1;
        List<OpportunityLineItem> opli = [Select id,unitprice from OpportunityLineItem where unitprice = 10];
        system.assertequals(oli1[0].unitprice, opli[0].unitprice);
         system.assert(oli1!=null); 
       
        return oli;
        
    }
    
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        system.assert(p!=null);
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='IPVN';
        insert prod;
          system.assert(prod!=null); 
       
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p;
          system.assert(p!=null); 
         
        return p;    
    }
}