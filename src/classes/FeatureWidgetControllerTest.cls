@isTest
public class FeatureWidgetControllerTest {

    private static cscfga__Product_Basket__c basket = null;
    private static cscfga__Product_Configuration__c config = null;
    private static csord__Service__c service = null;
    private static csord__Subscription__c subscription = null;

    private static void createTestData() {
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        List<cscfga__Product_Definition__c> pd1 = [Select id,name from cscfga__Product_Definition__c where name = 'Test definition 1'];
        system.assertequals(prodDefintion.name,pd1[0].name);
        
        system.assert(prodDefintion!=null);
        basket = new cscfga__Product_Basket__c();
        insert basket;
         system.assert(basket!=null);
        config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id);  
        insert config;
        system.assert(config!=null);
        csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        
        subscription = new csord__Subscription__c(Name = 'Test subscription'
            , csord__Identification__c = 'dummy'
            , csord__Order_Request__c = orderRequest.Id);
        insert subscription; 
        system.assert(subscription!=null);
        service = new csord__Service__c(Name = 'Test service'
            , csordtelcoa__Product_Configuration__c = config.Id
            , csordtelcoa__Product_Basket__c = basket.Id
            , csord__Identification__c = 'Service_' + config.Id
            , csord__Subscription__c = subscription.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert service;
        system.assert(service!=null);
        List<csord__Service__c> s = [Select id,name from csord__Service__c where name = 'Test service'];
        system.assertequals(service.name,s[0].name);
        csord__Service_Line_Item__c sli1 = TestDataFactory.createServiceLineItem('LineItem', service, 'SLI1', orderRequest , true);
        csord__Service_Line_Item__c sli2 = TestDataFactory.createServiceLineItem('LineItem', service, 'SLI1', orderRequest , true);

        
    }
    
    static testmethod void testFeatureMethods() {
        cspmb__Feature__c feature = new cspmb__Feature__c(
            Name = 'Test',
            cspmb__Feature_Description__c = 'Test'
        );
        insert feature;
        
        cspmb__Price_Item__c priceItem = new cspmb__Price_Item__c(
            Name = 'Test'
        );
        insert priceItem;
        system.assert(priceItem!=null);
        system.assert(priceItem!=null);
        cspmb__Price_Item_Feature_Association__c featureAssoc = new cspmb__Price_Item_Feature_Association__c(
            cspmb__Price_Item__c = priceItem.Id,
            cspmb__Feature__c = feature.Id
        );
        insert featureAssoc;
        system.assert(featureAssoc!=null);
        Feature_Widget_Settings__c featureSettings = new Feature_Widget_Settings__c(
            Name = 'Test',
            Add_On_Fields__c = 'Test',
            Custom_Object__c = 'cspmb__Feature__c',
            Custom_Object_Fields__c = 'abc,xyz,sjd,sdjh'
        );
        insert featureSettings;
        system.assert(featureSettings!=null);
        Feature_Widget_Settings__c featureSettings2 = new Feature_Widget_Settings__c(
            Name = 'Test1',
            Custom_Object__c = 'cspmb__Feature__c',
            Custom_Object_Fields__c = 'abc,xyz,sjd,sdjh'
        );
        insert featureSettings2;
         Feature_Widget_Settings__c featureSettings3 = new Feature_Widget_Settings__c(
            Name = 'Test3',
            Add_On_Fields__c = 'Test3',
            Custom_Object__c = 'cspmb__Feature__c',
            Custom_Object_Fields__c = 'abc,xyz,sjd,sdjh'
        );
        insert featureSettings3;
        system.assert(featureSettings3!=null);
        /*cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'VLAN',
            cscfga__Description__c = 'VLAN',
            Add_On__c = 'Test',
            Feature__c = 'Test',
            Rate_Card__c = 'Test',
            Price_Item_Attribute__c = 'Test'
        );
        insert pd;
        
        cscfga__Product_Configuration__c pc = new cscfga__Product_Configuration__c(
            cscfga__Product_Definition__c = pd.Id,
            Name = 'Test'
        );
        insert pc;*/
        
        // test feature retrieval
        String features = FeatureWidgetController.getAvaialbleFeatures(priceItem.Id, 'Test');
        String features1 = FeatureWidgetController.getAvaialbleFeatures(priceItem.Id, 'Test1');
        String features2 = FeatureWidgetController.getAvaialbleFeatures(priceItem.Id, 'Test3');
        
        Id configId=feature.Id; String featureCategory;
        Id configIds; List<Id> featureIds; String category;
        featureIds=new List<Id>();
        featureIds.add(feature.Id);
        category='test1';
        try{
        // test delete -> cannot test properly due to missing relationship
       FeatureWidgetController.deletefeatures(configId, 'Test');
       FeatureWidgetController.deletefeatures(configId, 'Test1');
       FeatureWidgetController.deletefeatures(priceItem.Id, 'Test');
         FeatureWidgetController.deletefeatures(configId,featureCategory);
        // test save -> cannot test properly due to missing relationship
       FeatureWidgetController.saveFeatures(configId, new List<Id>{feature.Id}, 'Test');
       FeatureWidgetController.saveFeatures(configId, new List<Id>{feature.Id}, 'Test1');
        FeatureWidgetController.saveFeatures(configId,featureIds,category);
        }catch(Exception e){
           system.debug('Exception '+e);
        }
    }
    
}