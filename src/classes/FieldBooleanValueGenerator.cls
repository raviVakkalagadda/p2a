public class FieldBooleanValueGenerator implements FieldValueGenerator {
    
    private Boolean defaultValue;
    
    public FieldBooleanValueGenerator() {
        this(false);
    }
    
    public FieldBooleanValueGenerator(Boolean defaultValue) {
        this.defaultValue = defaultValue;
    }
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.BOOLEAN == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        return this.defaultValue;
    }
}