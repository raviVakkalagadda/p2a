@isTest
private class FieldCurrencyValueGeneratorTest {
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals('Currency',cv.Name);
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
        FieldCurrencyValueGenerator  fcvg = new FieldCurrencyValueGenerator();
        Boolean  b = fcvg.canGenerateValueFor(SdFRcuurency);
        object obj = fcvg.generate(SdFRcuurency);
        Test.stopTest();
    
    }
}