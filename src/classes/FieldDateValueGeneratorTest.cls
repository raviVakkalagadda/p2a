@isTest
private class FieldDateValueGeneratorTest{
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
        
        FieldDateValueGenerator fdvgcon = new FieldDateValueGenerator(2);
        FieldDateValueGenerator  fdvg = new FieldDateValueGenerator ();  
        Boolean  b = fdvg.canGenerateValueFor(SdFRcuurency);
        object obj = fdvg.generate(SdFRcuurency);
        
        Test.stopTest();
    
    }
}