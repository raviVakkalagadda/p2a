public class FieldDoubleValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.DOUBLE == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        // limit to 0-100 range to work with latitude and longitude fields
        return Math.random() * 100;
    }
}