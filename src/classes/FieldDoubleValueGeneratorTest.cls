@isTest
private class FieldDoubleValueGeneratorTest{

    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        
        System.assertEquals('Currency',cv.name);
        
        Double myDouble = 1261992;
        system.assertEquals('1,261,992', myDouble.format());
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
            FieldDoubleValueGenerator  fdvg = new FieldDoubleValueGenerator();  
            Boolean  b = fdvg.canGenerateValueFor(SdFRcuurency);
            object obj = fdvg.generate(SdFRcuurency);
        Test.stopTest();
    }

}