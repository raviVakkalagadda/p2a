public class FieldReferenceValueGenerator implements FieldValueGenerator {
    
    public Boolean canGenerateValueFor(Schema.DescribeFieldResult fieldDesc) {
        return Schema.DisplayType.REFERENCE == fieldDesc.getType();
    }
    
    public Object generate(Schema.DescribeFieldResult fieldDesc) {
        List<Schema.sObjectType> references = fieldDesc.getReferenceTo();
        if (references.size() > 1) {
            // FIXME:  does not handle namePointing references yet.
            return null;
        }
        
        Schema.SObjectType type = references.get(0);
        DescribeSObjectResult typeDesc = type.getDescribe();
        Id result;
        
        if (typeDesc == Schema.SObjectType.User) {
            result = UserInfo.getUserId();
        } else if (typeDesc.isCreateable()) {
            SObject obj = new SObjectFactory().create(typeDesc);
            System.debug(System.LoggingLevel.ERROR, 'Creating reference object ' + typeDesc.getName() + ' for field ' + fieldDesc.getName());
            insert obj;
            result = obj.Id;
        } else if (typeDesc.isQueryable()) {
            System.debug(System.LoggingLevel.ERROR, 'Querying reference object ' + typeDesc.getName() + ' for field ' + fieldDesc.getName());
            String query = 'SELECT Id FROM ' + typeDesc.getName() + ' LIMIT 1';
            List<SObject> objects = Database.query(query);
            if (!objects.isEmpty()) {
                result = objects.get(0).Id;
            }
        }
        
        return result;
    }
}