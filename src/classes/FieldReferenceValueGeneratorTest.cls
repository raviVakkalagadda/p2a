@isTest
private class FieldReferenceValueGeneratorTest {
    
    static testMethod void unittest() {

        List<Account> accList = P2A_TestFactoryCls.getAccounts(3);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<CurrencyValue__c> currList = P2A_TestFactoryCls.getCurVal();
        system.assertEquals(true,currList!=null); 
        Schema.DescribeFieldResult SdFRcuurencyOpp = Opportunity.Description.getDescribe();
        Schema.DescribeFieldResult SdFRcuurencyAcc = Account.Name.getDescribe(); 
        /*
        List<Schema.sObjectType> references = SdFRcuurencyOpp.getReferenceTo();
        System.debug('****'+references);
        for (Schema.sObjectType ref : references){
            System.debug('@@@@'+ref);
        }
        */
        //Schema.SObjectType type = references.get(0);
        //DescribeSObjectResult typeDesc = type.getDescribe();
        
        Test.startTest();
        try{
           FieldReferenceValueGenerator  fdvg = new FieldReferenceValueGenerator();  
            Boolean b = fdvg.canGenerateValueFor(SdFRcuurencyAcc);
            Object obj = fdvg.generate(SdFRcuurencyOpp);
            system.assertEquals(true,fdvg!=null); 
        }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='FieldReferenceValueGeneratorTest :unittest';         
            ErrorHandlerException.sendException(e); 
        }  
        Test.stopTest();
    }
}