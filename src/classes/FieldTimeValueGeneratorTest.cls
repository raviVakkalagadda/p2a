@isTest
private class FieldTimeValueGeneratorTest {
    
    static testMethod void unittest() {
        
        CurrencyValue__c cv = new CurrencyValue__c();
        cv.CreatedDate = Datetime.now();
        cv.name = 'Currency';
        cv.Currency_Value__c = 45;
        insert cv;
        System.assertEquals(45,cv.Currency_Value__c );
        
        Blob blobValue = EncodingUtil.convertFromHex('4A4B4C');
        system.debug('--blobValue--'+blobValue);
        System.assertEquals('JKL', blobValue.toString());
        
        
        Schema.DescribeFieldResult SdFRcuurency = CurrencyValue__c.Currency_Value__c.getDescribe();
        
        Test.startTest();
            FieldTimeValueGenerator  fdvg = new FieldTimeValueGenerator();  
          FieldTimeValueGenerator  fdvg1 = new FieldTimeValueGenerator(10); 
            Boolean  b = fdvg.canGenerateValueFor(SdFRcuurency);
            object obj = fdvg.generate(SdFRcuurency);
          
        Test.stopTest();
    
    }
}