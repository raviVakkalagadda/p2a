public class FieldValueGeneratorNotFoundException extends Exception {
    //for @Testvisible , modified by Anuradha on 2/20/2017
    @Testvisible
	private Schema.DescribeFieldResult fieldDesc;
 
    public FieldValueGeneratorNotFoundException(Schema.DescribeFieldResult fieldDesc) {
        this.fieldDesc = fieldDesc;
        this.setMessage('No generator found for field ' + fieldDesc.getName() + ' of type ' + fieldDesc.getType());
    }
}