/**
    @author - Accenture
    @date - 10-MAY-2012
    @version - 1.0
    @description - This class is used to provide the service to generate the Order 
                   forms in contracts.
*/
public class FutureServiceContractAssociation{
    
     /**
        @author - Accenture
        @date - 10-MAY-2012
        @param - Map<Id,String> productMap, Map<Id,Id> opportunityMap
        @return - void 
        @description - This method is used to generate service for order forms.
  **/
    //@Future
 /*   public static void processService(Map<Id,String> productMap, Map<Id,Id> opportunityMap, List<Service__c> servMasterList){
        
         //query Service to get Service Object based on request ID
         //List<Service__c> servList = [Select Id,Contract_Form__c from Service__c where Id in : productMap.keyset()];
         
         // Creating the service list object
         List<Service__c> serviceToUpdate = new List<Service__c>();
         
         // Map to store Service object and ID mapping 
         Map<Id,Service__c> serviceMap = new Map<Id,Service__c>();
         
         //Map of Id of service and Contract
         Map<Id, contract__c> serviceContMap = new Map<Id,Contract__c>();
        // Putting the values 
        /*for (Service__c service : servList){
            serviceMap.put(service.id,service);
        }*/
        
        /** query RecordType to get RecordType Object based on request
            @SobjectType
            @Name
        */
     /*   RecordType orderFormType = [SELECT Name, Id, SobjectType FROM RecordType where SobjectType='Contract__c' and Name='Order Form'];
        
        //for(Id serviceId:productMap.keyset()){
            //System.debug('---------1----------');
            
            /** query Contract to get Contract Object based on request
                @Product_ID
                @Opportunity
                @RecordTypeId 
            */
        /*    List<Contract__c> contra = [Select Id, Product_Id__c, Opportunity__c from Contract__c where Product_ID__c=:productMap.values() and RecordTypeId=:orderFormType.Id ];
            //System.debug('Contra Size---------'+contra.size());
            for (Service__c serv:servMasterList){
                for (Contract__c cont : contra){
                    if (cont.Product_Id__c == productMap.get(serv.id) && cont.Opportunity__c == opportunityMap.get(serv.id)){
                        if (!serviceContMap.containsKey(serv.id)){
                            serviceContMap.put(serv.id, cont);
                        }
                    }
                }
            } 
            
             for (Service__c serv:servMasterList){
                 if (serviceContMap.containskey(serv.id)){
                     serv.Contract_Form__c= serviceContMap.get(serv.id).Id;
                     serviceToUpdate.add(serv);
                 }
             }
            
            //if (contra.size()>0){
                //if (contra.size()==1){                       
                   // Service__c  tmpService = serviceMap.get(serviceId);
                   // tmpService.Contract_Form__c= contra.get(0).id;
                   // serviceToUpdate.add(tmpService);
                   //}else{
                   // Get the List and Mail the Admin with the Product Id and Opportunity with the List of Contracts.                      
                   //}
           // }
        //}
        update serviceToUpdate;
    }
    */
}