global with sharing class GIAAS_Utils {
	
	global static GIAAS_Plan_Description__c getDescription(String planName) {
		GIAAS_Plan_Description__c [] ret = [SELECT Id , Internet_Data_Usage__c, IP_Addresses__c, 
		Operating_system__c , Plan_Name__c , Plan_Name_Code__c , Server_Memory__c , Service_CPU__c , Storage__c FROM 
		GIAAS_Plan_Description__c WHERE Plan_Name__c = :planName] ; 
		
		if (ret.size() > 0)
			return ret[0] ; 
		return null ; 
	}
	 
	global static GIAAS_Rate_Card__c getPrice(Map<String , String> pricingParameters) {
		//TODO fix this later 
		GIAAS_Rate_Card__c dummy =  [SELECT Id , NRC__c , MRC__c , NRC_Cost__c , MRC_Cost__c FROM GIAAS_Rate_Card__c limit 1] ;
		return dummy ;  
		if (pricingParameters.get('product') == 'Storage')
			return getStoragePrice(pricingParameters.get('storageType')) ;
		
		return null ; 
			 
	}
	
	global static GIAAS_Rate_Card__c getStoragePrice(String storageType) {
		GIAAS_Rate_Card__c [] rateCards = [SELECT Id , NRC__c , MRC__c , NRC_Cost__c , MRC_Cost__c FROM GIAAS_Rate_Card__c WHERE 
			Product__c = 'Storage' AND Storage_Type__c = :storageType ] ; 
		if (rateCards.size() > 0)
			return rateCards[0] ; 
		return null ; 
	}
    
}