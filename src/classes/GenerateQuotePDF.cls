public class GenerateQuotePDF {

    private Map<String,String> fastQuoteStringMap = new Map<String,String>();
    public Id configId {get;set;}
    private String fastQuoteString;
    public List<FastQuoteStringWrapper> fastQuoteStringList{get;set;}
    public List<String> displayHeaderList{get;set;}
    private List<cscfga__Attribute__c> attrList = new List<cscfga__Attribute__c>();
    public Map<String,String> fixedQuoteValueMap{get;set;}
    public Set<String> fixedQuoteKeySet {get;set;}
    public String accountName {get;set;}
    public String basketName {get;set;}
    public String textFromTermscond{
    get {
        StaticResource sr = [
                select Body
                from StaticResource
                where Name = 'termscond'
                ];
        return sr.Body.toString();
    }
    }
    private Set<String> querriedAttributeNames = new Set<String> {'FastQuoteResult','Contract Term','Line Item Description','Rate Card MRC','Rate Card NRC'};
    public  GenerateQuotePDF(ApexPages.StandardController controller){
        configId = controller.getRecord().Id;
        fixedQuoteValueMap = new Map<String,String>();
        fixedQuoteKeySet  = new Set<String>();
        displayHeaderList = new List<String> {'Product Name','Contract Term','Rate Card MRC','Rate Card NRC'};
        attrList = [Select name,cscfga__Display_Value__c,cscfga__value__c,cscfga__Product_Configuration__r.cscfga__Product_Basket__r.Name,cscfga__Product_Configuration__r.cscfga__Product_Basket__r.Account__c from cscfga__Attribute__c where name in:querriedAttributeNames and cscfga__Product_Configuration__c=:configId and cscfga__Product_Configuration__r.cscfga__Product_Definition__r.name='Point To Point-POC'];
        for(cscfga__Attribute__c attr: attrList)
        {
            
            if(attr.name=='FastQuoteResult')
             fastQuoteString=attr.cscfga__value__c;
            else if(attr.name =='Line Item Description')
            {
                fixedQuoteValueMap.put('Product Name',attr.cscfga__Display_Value__c);
            }
            else if(attr.name.contains('Rate Card'))
            {
                fixedQuoteValueMap.put(attr.name,'$'+attr.cscfga__Display_Value__c);
            } 
            else
            {
                fixedQuoteValueMap.put(attr.name,attr.cscfga__Display_Value__c);
            }
        
        } 
        System.debug('****fixedQuoteValueMap'+fixedQuoteValueMap);
        accountName = attrList[0].cscfga__Product_Configuration__r.cscfga__Product_Basket__r.Account__c;
        basketName = attrList[0].cscfga__Product_Configuration__r.cscfga__Product_Basket__r.Name;
        fixedQuoteKeySet  = fixedQuoteValueMap.keySet();
        if(fastQuoteString!=null)
            processFastQuoteString();
    }
    
    public Map<String,String> processFastQuoteString()
    {
        
       fastQuoteStringList = (List<FastQuoteStringWrapper>)System.JSON.deserialize(fastQuoteString,List<FastQuoteStringWrapper>.class);  
       System.debug('*****fastQuoteStringList'+fastQuoteStringList);
       return fastQuoteStringMap;
    
    
    }
    public class FastQuoteStringWrapper
    {
       public String Contract_Term_0{get;set;}
       public String Bandwidth_0{get
       {
           List<String> parts = Bandwidth_0.split('\\|');
           if (parts.size()==2)
               return parts[1];
           else
               return Bandwidth_0;
       }
                                 set;}
       public Decimal Rate_Card_MRC_0{get;set;}
       public Decimal Rate_Card_NRC_0{get;set;}
    }    
    
    
}