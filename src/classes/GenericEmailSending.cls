/*
@description - This class contains mail sending Logic. 
*/
public class GenericEmailSending {
    private static Id orgWideEmailAddressId;
    
    /*
*   @param - String emailAddress String subject String body
*   @return - void noReturn
*   @description - This method sends mail by taking subject,body,emailaddresses as arguments
*/
    public void send(String emailAddress, String subject, String body) {
        EmailUtil.EmailData mailData = new EmailUtil.EmailData();
        mailData.addToAddresses(emailAddress.split(':', 0));
        mailData.message.setSubject(subject);
        mailData.message.setPlainTextBody(body);
        mailData.message.setOrgWideEmailAddressId(getOrgWideEmailAddressId());          
        
        List<Messaging.SendEmailResult> sendResults = EmailUtil.sendEmail(mailData);
        
        if (!sendResults.get(0).isSuccess()) { 
            System.StatusCode statusCode = sendResults.get(0).getErrors()[0].getStatusCode();
            String errorMessage = sendResults.get(0).getErrors()[0].getMessage();
        }   
    }
    
    /** @param - String emailAddress String subject String body
@return - void noReturn
@description -This method is used getting user's email addresses
*/
    public void mail(String emailAddress, String subject, String body) {    
        Messaging.reserveSingleEmailCapacity(2);
        
        EmailUtil.EmailData mailData = new EmailUtil.EmailData();
        mailData.addToAddresses(emailAddress.split(':', 0));
        mailData.addCCAddress(label.Testing_User);
        mailData.message.setSubject(subject);
        mailData.message.setPlainTextBody(body);
        mailData.message.setOrgWideEmailAddressId(getOrgWideEmailAddressId());         
        mailData.message.setTargetObjectId(UserInfo.getUserId());
        mailData.message.setSaveAsActivity(false);
        
        EmailUtil.sendEmail(mailData);
    }
    
    public void getRequiredEmailAddresses(List<Order__c> orderList){
        // query to get all queues corresponding to Bill profile Object
        List<QueueSobject> que = QueueUtil.getQueuesListForObject(BillProfile__c.SObjectType);
        
        //Query the User object  to get sales User who created the Order
        List<User> usrLst = [Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        Id queid;
        String billTeam;
        String emailAddresses = '';
        String USerRegion='';
        
        String subject;
        String body;
        
        Map<String, Id> queMap = new Map<String, Id> ();
        
        //Get Billing Team Queue id corresponding to sales user region.
        for (QueueSObject q:que){
            if((q.Queue.Name).contains(usr.Region__c)){
                queMap.put(q.Queue.Name,q.QueueId);
                queid = q.QueueId;
                billTeam = q.Queue.Name;
            }
        }
        
        //Getting the all Users id's  corresponding to queue
        //List<GroupMember> list_GM = [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid];
        
        List<Id> lst_Ids = new List<ID>();
        for(GroupMember gmObj : [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid]){
            lst_Ids.add(gmObj.UserOrGroupId);
        }
        
        //Query the Billing Team Users 
        //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
        
        //Here concanating all email addresses of Users belong to Billing Queue
        for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
            if(emailAddresses == ''){
                emailAddresses = usrObj.Email;
                USerRegion=usrObj.Region__c;
            }
            else{
                if(!emailAddresses.contains(usrObj.Email)){emailAddresses += ':'+usrObj.Email;}//defect fix:14129  
                USerRegion=usrObj.Region__c;
            }
        }
        
        if(orderList[0].Selling_Entity__c == 'Telstra Limited'){   
            subject = 'Please Update Below Order details in ROC';
            body = 'Dear Billing Team,'+'\n'+'\n';
            body += 'This order '+orderList[0].name+' has been created  in P2O by EMEA region user will not be sent to Billing System via IP006. please download the order/change using the following link and enter it into ROC.'+'\n';
            body += URL.getSalesforceBaseUrl().toExternalForm() + '/' + orderList[0].Id;
        }
        else{
            subject = 'Please Update Below Order details in ROC';     
            body = 'Dear Billing Team,'+'\n'+'\n';     
            body += 'The following order has been created in P2O against a migrated/existing service, please download the order/change using the following link and enter it into ROC.'+'\n';
            body += URL.getSalesforceBaseUrl().toExternalForm() + '/' + orderList[0].Id;
        }
        
        GenericEmailSending emailSendingObj = new GenericEmailSending();
        emailSendingObj.send(emailAddresses, subject, body);
    }
    
    //Sending email for Live is design
    public void sendEmailtoBillingTeamonLiveisDesign(csord__Service__c oLIObj,List<String> productNameList){
        
        String userRegion =oLIObj.Telstra_Billing_Entity__c!=null?getRegionBasedOnBillingEntity(oLIObj.Telstra_Billing_Entity__c):null;
        if(Test.isRunningTest()){
            userRegion = 'US';
        }
        if(oLIObj.Telstra_Billing_Entity__c != '' && 
           userRegion != null){
            
            String productGIAASNames = '';
            //getting product name List and concatenating all product name
            for(String productName : productNameList){    
                if(productGIAASNames == ''){
                    productGIAASNames = productName;      
                }
                else{        
                    productGIAASNames = productGIAASNames+','+productName;
                }   
            }
            
            // query to get all queues corresponding to Bill profile Object
            List<QueueSobject> queueSObjList = QueueUtil.getQueuesListForObject(BillProfile__c.SObjectType);
            
            Id queid;
            String billTeam;
            String emailAddresses = '';
            csord__order__c orderNumber = new csord__order__c();
            List<csord__order__c> orderNumberList = [select csord__Order_Number__c from csord__order__c where id =:oLIObj.csord__order__c];
            if(orderNumberList.size() > 0){
                orderNumber = orderNumberList[0];
            }
            csord__Subscription__c subscriptionName = new csord__Subscription__c();
            List<csord__Subscription__c> subscriptionNameList = [select Name from csord__Subscription__c where id =:oLIObj.csord__Subscription__c];
            if(subscriptionNameList.size() > 0){
                subscriptionName = subscriptionNameList[0];
            }
            
            Map<String, Id> queMap= new Map<String, Id> ();
            
            
            //Get Billing Team Queue id corresponding to SD user region.
            for (QueueSObject q:queueSObjList){
                if((q.Queue.Name).contains(userRegion)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                    billTeam = q.Queue.Name;
                }
            }
            system.debug('queMap ritzzz'+queMap);    
            //Getting the all Users id's  corresponding to queue
            //List<GroupMember> list_GM = [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid];
            //System.debug('list_GM ritzzz '+list_GM);    
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid]){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            //System.debug('lst_Ids ritzzz '+lst_Ids);    
            //Query the Billing Team Users 
            //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            //system.debug('lst_UserObj ritzzz'+lst_UserObj);    
            
            //here getting email id of user for based on Region
            if(userRegion == 'US' || userRegion == 'Australia'){                
                //Here concanating all email addresses of Users belonging to US region
                for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                    if(emailAddresses == ''){
                        emailAddresses = usrObj.Email;
                        USerRegion = usrObj.Region__c;
                    }
                    else{
                        if(!emailAddresses.contains(usrObj.Email)){emailAddresses += ':' + usrObj.Email;}//defect fix:14129 
                        USerRegion = usrObj.Region__c;
                    }
                }   
            }
            else{
                emailAddresses = getEmailAddresses(userRegion);
            }  
            
            String body;
            String subject;
            
            
            //Getting salesforce URL of Instance
            String instanceURLofService = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oLIObj.id;   
            String instanceURLofSubscription = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oLIObj.csord__Subscription__c;
            
            subject = 'Services under '+orderNumber.csord__Order_Number__c+' order are marked as Live as Designed/Decommissioned Please intiate the process to start billing for service';  
            body =  'Dear Billing Team,'+'\n\n';
            body += 'The service under following order '+orderNumber.csord__Order_Number__c+' is marked as Live as Designed/Decommissioned, please take necessary actions to start billing'+'\n';
            body += 'Account ID: ' + oLIObj.Account_ID__c + '\n';
            body += 'Account Name: ' + oLIObj.Account_Name__c + '\n';
            body += 'Order: ' + orderNumber.csord__Order_Number__c + '\n'; 
            body += 'Subscription: ' + oLIObj.csord__Subscription__c + '\n';
            body += 'Product: ' + oLIObj.Product_Id__c + '\n';
            body += 'Order Type: ' + oLIObj.Product_Configuration_Type__c + '\n';
            body += 'Link to Subscription: '+ instanceURLofSubscription + '\n';
            body += 'Link to Service: '+ instanceURLofService + '\n\n'; 
            body += '';
            
            //If order line item is GIAAS product, Net NRC price should be zero while billing for some of GIAAS products
            if(productGIAASNames != ''){
                body += 'Please Note this order contains GIAAS Products, Please update Net NRC Price as zero in Billing System for ' + productGIAASNames + ' products';
            } 
            
            system.debug('emailAddresses ritzzz'+emailAddresses);
            if(emailAddresses!=''){sendEmailForBlockingBilling(emailAddresses, subject, body);}
            
        }        
    }
    
    //Sending email to EMEA Queue members when Billing entity is Telstra Limited
    public void sendEmailtoBillingTeam(csord__Service__c oLIObj,List<String> productNameList){     
        String productGIAASNames = '';
        //getting product name List and concatenating all product name
        for(String productName : productNameList){
            if(productGIAASNames == ''){
                productGIAASNames = productName;     
            }
            else{     
                productGIAASNames = productGIAASNames+','+productName;
            } 
        }
        
        // query to get all queues corresponding to Bill profile Object
        List<QueueSobject> queueSObjList = QueueUtil.getQueuesListForObject(BillProfile__c.SObjectType);
        
        Id queid;
        String billTeam;
        String emailAddresses = '';
        System.debug('oLIObj.Telstra_Billing_Entity__c: ' + oLIObj.Telstra_Billing_Entity__c);
        String userRegion = getRegionBasedOnBillingEntity(oLIObj.Telstra_Billing_Entity__c);
        System.debug('userRegion: ' + userRegion);
        
        Map<String, Id> queMap= new Map<String, Id> ();
        
        if(Test.isRunningTest()){
            userRegion = 'Australia';
        }
        
        //Get Billing Team Queue id corresponding to SD user region.
        for (QueueSObject q:queueSObjList){
            if((q.Queue.Name).contains(userRegion)){
                queMap.put(q.Queue.Name,q.QueueId);
                queid = q.QueueId;
                billTeam = q.Queue.Name;
            }
        }
        system.debug('queMap ritzzz'+queMap);  
        
        //Getting the all Users id's  corresponding to queue
        //List<GroupMember> list_GM = [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid];
        
        //System.debug('list_GM ritzzz '+list_GM);    
        List<Id> lst_Ids = new List<ID>();
        for(GroupMember gmObj : [SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId = :queid]){
            lst_Ids.add(gmObj.UserOrGroupId);
        }
        
        //Query the Billing Team Users 
        //List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];   
        
        //system.debug('lst_UserObj ritzzz'+lst_UserObj);         
        
        //here getting email id of user for based on Region
        if(userRegion == 'US' || userRegion == 'Australia'){                
            //Here concanating all email addresses of Users belonging to US region
            for(User usrObj : [Select Id,name,Email,Region__c from User where id in :lst_Ids]){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                    USerRegion=usrObj.Region__c;
                }
                else{
                    emailAddresses += ':'+usrObj.Email; 
                    if(!emailAddresses.contains(usrObj.Email)){emailAddresses += ':'+usrObj.Email;} //defect fix:14129
                }
            }      
        }
        else{
            emailAddresses = getEmailAddresses(userRegion);
        }
        
        system.debug('emailAddresses ritzzz'+emailAddresses);    
        String subject;
        String body;
        if(oLIObj.Telstra_Billing_Entity__c != ''){ 
            //Getting salesforce URL of Instance
            String instanceURLofService = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oLIObj.id;   
            String instanceURLofSubscription = URL.getSalesforceBaseUrl().toExternalForm() + '/'+oLIObj.csord__Subscription__c;
            
            subject = oLIObj.Name+' service is complete - '+oLIObj.Name +' '+ oLIObj.Product_Id__c+' Please create/update Service details in Subex ROC';
            body = 'Dear Billing Team,\n\n';
            body += 'The following service are completed and require you to manually enter the charges into the Billing System.\n';
            body += 'Account ID: ' + oLIObj.Account_ID__c + '\n';
            body += 'Account Name: ' + oLIObj.Account_Name__c + '\n';
            body += 'Service: ' + oLIObj.Name + '\n'; 
            body += 'Subscription: ' + oLIObj.csord__Subscription__c + '\n';
            body += 'Product: ' + oLIObj.Product_Id__c + '\n';
            body += 'Service Type: ' + oLIObj.Product_Configuration_Type__c + '\n';
            body += 'Link to Subscription: '+ instanceURLofSubscription + '\n';
            body += 'Link to Service: ' + instanceURLofService + '\n\n'; 
            body += '';
            
            //If order line item is GIAAS product, Net NRC price should be zero while billing for some of GIAAS products
            if(productGIAASNames != ''){
                body += 'Please Note this order contains GIAAS Products, Please update Net NRC Price as zero in Billing System for '+productGIAASNames+' products';
            }
        }
        
        sendEmailForBlockingBilling(emailAddresses,subject,body);
    }
    
    //This method returns region based on Billing Entity of Bill Profile
    public String getRegionBasedOnBillingEntity(String billingEntity){
        String region ;
        Boolean isPacnetEntity = false;
        Map<String,TELSTRA_Entities__c> TelstraEntityMap = TELSTRA_Entities__c.getAll();        
        List<PACNET_Entities__c> allPacnetEntities = PACNET_Entities__c.getall().values();  
        for(PACNET_Entities__c Pacnet : allPacnetEntities) {
            if(billingEntity!=null && billingEntity== Pacnet.PACNET_Entity__c){
                isPacnetEntity = true;
                break;                        
            }
        }
        
        if(billingEntity!= null && TelstraEntityMap.get('AustraliaRegion').TELSTRA_Entity__c.contains(billingEntity)){ //For Australia Region
            region = 'Australia';
        }
        else if(billingEntity!= null && TelstraEntityMap.get('USRegion').TELSTRA_Entity__c.contains(billingEntity)){ // For US region based on Billing Entity
            region = 'US';
        }
        else if(billingEntity!= null && TelstraEntityMap.get('EMEARegion').TELSTRA_Entity__c.contains(billingEntity)){ //For EMEA Based on Billing Entity
            region = 'EMEA';
        }
        else if(billingEntity!= null && TelstraEntityMap.get('NorthAsiaRegion').TELSTRA_Entity__c.contains(billingEntity)/* || TelstraEntityMap.get('NorthAsiaRegion1').TELSTRA_Entity__c.contains(billingEntity)) //commented by jana*/){ //For North Asia Based these Billing entity 
            region = 'North Asia';
        }
        else if(billingEntity!= null && (TelstraEntityMap.get('SouthAsiaRegion').TELSTRA_Entity__c.contains(billingEntity)||  TelstraEntityMap.get('SouthAsiaRegion1').TELSTRA_Entity__c.contains(billingEntity))){ //For South Asia based on Billing Entity
            region = 'South Asia';
        }
        else if(isPacnetEntity){
            region = 'PACNET';
        }
        
        return region;
    }
    
    //This method returns email addresses based on region of Billing entity
    public String getEmailAddresses(String region){
        String emailAddresses;
        if(region == 'EMEA'){
          emailAddresses =label.BillingOrderEmail;//'billingorders@intl.telstra.com'; 
            //emailAddresses = 'sanjeev.b.kuma@accenture.com';   
        }
        else if(region == 'North Asia' || region == 'South Asia'){    
            emailAddresses =label.TGBillingEmail;//'TGBilling@team.telstra.com';
            //emailAddresses = 'sanjeev.b.kuma@accenture.com';
        }
        else if(region=='PACNET'){
            /*
*defect fix:13860
*developer:ritesh
*date:4-jan-2017
*description:added default emailAddresses  for PACNET billing entities
*/                
            emailAddresses =system.label.PACNET_Billing_Team;
        }
        return emailAddresses;
    }
    
    /*
*   This method sends mail by taking subject,body,emailaddresses as arguments
*/
    public void sendEmailForBlockingBilling(String emailAddress, String subject, String body) {
        EmailUtil.EmailData mailData = new EmailUtil.EmailData();
        mailData.addToAddresses(emailAddress.split(':', 0));
        mailData.addCCAddress(label.Testing_User);
        mailData.message.setSubject(subject);
        mailData.message.setPlainTextBody(body);
        mailData.message.setOrgWideEmailAddressId(getOrgWideEmailAddressId());
        System.debug('mailData ritzzz'+mailData  );            
        List<Messaging.SendEmailResult> results = EmailUtil.sendEmail(mailData);
        
        if (!results.get(0).isSuccess()) { 
            System.StatusCode statusCode = results.get(0).getErrors()[0].getStatusCode();
            String errorMessage = results.get(0).getErrors()[0].getMessage();
        }
    }
    
    /*
*   Get all GIAAS products in order whose zero out NRC Falg is set true
*/
    public List<String> getProductNamesOfGIAAS(List<Order_line_item__c> oLIList){
        List<String> productNamesList = new List<String>();
        //get all productname of GIAAS whose Zero Out NRC Flag is true
        for(Order_line_item__c oLIObject : oLIList){  
            if(oLIObject.Is_GIAAS_Product__c && oLIObject.Product__r.Root_Product_For_Zero_Out_NRC_Flag__c!=null && oLIObject.Product__r.Root_Product_For_Zero_Out_NRC_Flag__c.contains(oLIObject.Root_Product_ID__c)){ 
                productNamesList.add(oLIObject.Product_Name__c); 
            }  
        }
        return productNamesList; 
    }
    
    private String getOrgWideEmailAddressId(){
        if(orgWideEmailAddressId == null){
            List<OrgWideEmailAddress> owea = [select Id from OrgWideEmailAddress where Address='Telstra.TGHRIS.P20.SupportTeam@accenture.com'];
            if(owea.size() > 0){
                orgWideEmailAddressId = owea.get(0).Id;         
            }
        }
        return orgWideEmailAddressId;
    }
}
