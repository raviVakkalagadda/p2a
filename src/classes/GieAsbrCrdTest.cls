@isTest(SeeAllData = false)
private class GieAsbrCrdTest{
    private static Map<String, String> searchFields = new Map<String, String>();
    
    private static List<cscfga__Product_Basket__c> basketList;
    private static List<cscfga__Product_Configuration__c> asbrList;
    private static List<cscfga__Product_Configuration__c> portList; 
    
    Private static List<cscfga__Product_Definition__c> ProductDeflist;  
      private static void initTestData(){
        
        basketList = new List<cscfga__Product_Basket__c>{
            new cscfga__Product_Basket__c(Name = 'Basket 1')
        };
        
        insert basketList;
         System.assertEquals('Basket 1',basketList[0].Name ); 
         
           list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> asbrList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);

        
        List<cscfga__Product_Configuration__c> portList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
       /* asbrList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'ASBR 1', Customer_Required_Date__c =  Date.newInstance(2017,11,11),cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'ASBR 2', Customer_Required_Date__c =  Date.newInstance(2017,12,11),cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert asbrList;
        System.assertEquals('ASBR 1',asbrList[0].Name );  
        
        portList = new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(Name = 'IPVPN Port 1',Customer_Required_Date__c =  Date.newInstance(2017,11,11), ASBR_Product_Configuration__c = asbrList[0].Id, cscfga__Product_Basket__c = basketList[0].Id),
            new cscfga__Product_Configuration__c(Name = 'IPVPN Port 2',Customer_Required_Date__c =  Date.newInstance(2017,12,11), ASBR_Product_Configuration__c = asbrList[0].Id, cscfga__Product_Basket__c = basketList[0].Id)
        };
        
        insert portList;  
        System.assertEquals('IPVPN Port 1',portList[0].Name );   */
    }
    
   private static testMethod void doDynamicLookupSearchTest() {
    Exception ee = null;
    
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
            List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
            list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
            ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
            List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
            List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
            List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
                             
            initTestData();  
            
            String productDefinitionId = proconfigs[0].id;   
            searchFields.put('LinkedGIEs', proconfigs[0].Id);
            
            GieAsbrCrd gie = new GieAsbrCrd();  
            String reqAtts = gie.getRequiredAttributes();
            Object[]  data = gie.doDynamicLookupSearch(searchFields,productDefinitionId); 
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
  }  
      
}