/**
    @author - Accenture
    @date - 09- Dec-2014
    @version - 1.0
    @description - This is the test class for GlobalAccountCURDonAccountCURDOperationTrigger
*/
@isTest(SeeAllData = true)
private class GlobalAccountControllerTest{
    static Account acc;
    static Country_Lookup__c cl;
    static Global_Account__c ga;
    
    static testMethod void globalAccountdetailsMethod() {
    test.starttest();
    
        ga= getGlobalAccount();
        acc =getAccount();
        system.currentPageReference().getParameters().put('retUrl', '/'+ga.id);    
        ApexPages.StandardController sc1 = new ApexPages.StandardController(ga);         
        GlobalAccountAccessController gAAC = new  GlobalAccountAccessController(sc1);
        gAAC.gAccId=ga.id;
        gAAC.pageredir();
        system.assert(gAAC!=null);
        GlobalAccountViewAccessController gAA = new GlobalAccountViewAccessController(sc1);
        gAA.gAccId=ga.id;
        gAA.pageredir();
    test.Stoptest();        
    }
     private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'INDIA';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
            List<Country_Lookup__c> c = [Select id,CCMS_Country_Name__c from Country_Lookup__c where CCMS_Country_Name__c = 'INDIA'];
            system.assertequals(cl.CCMS_Country_Name__c,c[0].CCMS_Country_Name__c);
        }
        return cl;
    } 
    private static Account getAccount(){
        if(acc == null){ 
            acc = new Account();
            acc.Name = 'Test Account Test 2';
            acc.Customer_Type__c = 'MNC';
            cl = getCountry();
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Account_Manager__c = null;
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
        }
        return acc;
    }
    private static Global_Account__c getglobalAccount(){
        if(ga == null){ 
          ga = new Global_Account__c();
            acc= getAccount();
            ga.Account__c = acc.id;
            ga.Name = 'test1243';
            cl =getCountry();
            ga.Country__c =cl.id;
            insert ga; 
            system.assert(ga!=null); 
        }
        return ga;
    }
  }