/**
    @author - Accenture
    @date - 09-JUNE-2012
    @version - 1.0
    @description - This class is used for Action item creation .
*/
public with sharing class HandleRequestController {

    //====================================
    // VARIABLE
    // ===================================
    Id recordId;
    Id oppId;
    Id accountId;
    Id leadId;
    Id billprofileId;
    String recordType;
    Action_Item__c objActionItem;
    Boolean SecurityBillProfile = false;
    //====================================
    // CONSTRUCTOR
    // ===================================
    public HandleRequestController() {
    }
    
    public HandleRequestController (ApexPages.StandardController controller) {
        // Get Record Id from UI
        try {
            recordId = System.currentPageReference().getParameters().get('recordId');
            
            oppId = System.currentPageReference().getParameters().get('oppId');
            
            recordType = System.currentPageReference().getParameters().get('recordType');
             
            accountId  = System.currentPageReference().getParameters().get('accountId');
            
            leadId  =System.currentPageReference().getParameters().get('leadId');
            
            billprofileId = System.currentPageReference().getParameters().get('billprofileId');
            
            SecurityBillProfile = Boolean.valueof(System.currentPageReference().getParameters().get('security_bill_for_activation__c'));

            if (accountId==null && billprofileId!=null){
                BillProfile__c billProf = [Select Account__c from BillProfile__c where Id = : billprofileId];
                accountId = billProf.Account__c;
            }
            

            System.debug('oppId-->'+oppId);
            System.debug('accountId-->'+accountId);
            System.debug('leadId-->'+leadId);
            System.debug('recordId-->'+recordId);
            System.debug('recordType-->'+recordType);
            System.debug('billprofile-->'+ billprofileId);
        } catch(Exception excp) {

            System.debug('Error In HandleRequestController'+excp.getMessage());
            //CreateApexErrorLog.InsertHandleException('ApexClass','HandleRequestController',excp.getMessage(),'BillProfile__C',Userinfo.getName());
        }
        //sObj = controller.getRecord();
    }
    
    // Redirect process for Opportunity Action Items
    public PageReference redirectToOpportunity() {
       
        String sfdcURL = NULL;
        RecordType rt = [select Id,Name from RecordType where Name=:recordType limit 1]; 
        List<QueueSobject> que = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='Action_Item__c'];
        Map<String, Id> queMap= new Map<String, Id> ();
        for (QueueSObject q:que){
        queMap.put(q.Queue.Name,q.QueueId);
        }
        List<User> usrLst =[Select Name, Region__c,Role_Name__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        Map<ID,ID> accountOppbillProfileMapId = new Map<ID,ID>();
        Map<ID,ID> accountBillProfileMapId = new Map<ID,ID>();
        accountOppbillProfileMapId.put(oppId,accountId);
        accountBillProfileMapId.put(billprofileId ,accountId);
        
        if(recordType == 'Request Pricing Approval'){
         
            sfdcURL ='/apex/NewPricingApprovalPopulate?oppId='+oppId;
            //return (new PageReference(sfdcURL));
           }        
       else if(recordType == 'Request Carrier Quote'){
            
         
          
           // ai.Priority__c = 'High'; //Shailesh-Business said to remove this field see SAAS action item 00014
            objActionItem = Util.createActionItem('Request Carrier Quote', accountOppbillProfileMapId, oppId, null);
            objActionItem.Feedback__c = '';
            insert objActionItem;
            sfdcURL ='/'+oppId;
            //return (new PageReference(sfdcURL));
        }else if(recordType == 'Request Contract Approval'){
            
                    
           // ai.Priority__c = 'High'; //Shailesh-Business said to remove this field see SAAS action item 00014
            objActionItem = Util.createActionItem('Request Contract Approval', accountOppbillProfileMapId, oppId, null);
            objActionItem.Feedback__c = '';
            objActionItem.Subject__c = 'Request Contract Approval';
            insert objActionItem;
            sfdcURL ='/'+oppId;
            //return (new PageReference(sfdcURL));
        }else if(recordType == 'Request Credit Check'){
            
            sfdcURL = '/apex/NewCreditCheckPopulate?oppId='+oppId;
            //return (new PageReference(sfdcURL));
        }else if(recordType == 'Request Feasibility Study'){
            
           // Action_Item__c objActionItem = new Action_Item__c();
        
            
            //ai.Priority__c = 'High'; //Shailesh-Business said to remove this field see SAAS action item 00014
            objActionItem = Util.createActionItem('Request Feasibility Study', accountOppbillProfileMapId, oppId, null);
            objActionItem.Feedback__c = '';
            objActionItem.Subject__c = 'Request Feasibility Study';
            Opportunity ObjOpp = [Select Id,Region__c,CurrencyIsoCode from Opportunity where Id=:objActionItem.Opportunity__c limit 1 ];
            objActionItem.Region__c = ObjOpp.Region__c;
            objActionItem.CurrencyIsoCode = ObjOpp.CurrencyIsoCode;
            Util.assignOwnersToRegionalQueue(usr, objActionItem, 'Request Feasibility Study');
            insert objActionItem;
      
            sfdcURL ='/'+oppId;
            //return (new PageReference(sfdcURL));
         }else if(recordType == 'Request Contract'){
            
           // Action_Item__c objActionItem = new Action_Item__c();
           
     /*       objActionItem = Util.createActionItem('Request Contract', accountOppbillProfileMapId, oppId, null);
            objActionItem.Subject__c = 'Request for Contract';
            objActionItem.Priority__c = 'High';
            objActionItem.Due_Date__c = System.Today() + 5 ;*/
            
            // Putting into Proper Regional Queue
            
//            Util.assignOwnersToRegionalQueue(usr,objActionItem, 'Request Contract');
      
  //          objActionItem.Feedback__c = '';
    //        insert objActionItem;
      //      sfdcURL ='/'+oppId;  
            //return (new PageReference(sfdcURL));
         sfdcURL ='/apex/NewRequestContract?oppId='+oppId;    
        }else if(recordType == 'Request for Bill Profile Approval' && (SecurityBillProfile == false)){
            
          
           
            objActionItem = Util.createActionItem('Request for Bill Profile Approval', accountBillProfileMapId, null , billprofileId);
            objActionItem.Subject__c = 'Request for Bill Profile Approval';
            Util.assignOwnersToRegionalQueue(usr,objActionItem, 'Request for Bill Profile Approval');
            
        
 
            objActionItem.Feedback__c = '';
            insert objActionItem;
            System.debug('ActionItem************'+objActionItem.Id);
            sfdcURL ='/'+billprofileId; 
            System.debug('Bill Profile ID ' +billprofileId );
            
            //return (new PageReference(sfdcURL));

        }else if(recordType == 'Request for Bill Profile Approval' && SecurityBillProfile){ //added this else if as part of defect 2717
            
          
           
            objActionItem = Util.createActionItem('Request for Bill Profile Approval', accountBillProfileMapId, null , billprofileId);
            objActionItem.Subject__c = 'Request for Security Bill Profile Activation';
            Util.assignOwnersToRegionalQueue(usr,objActionItem, 'Request for Bill Profile Approval');
            
        
 
            objActionItem.Feedback__c = '';
            insert objActionItem;
            System.debug('ActionItem************'+objActionItem.Id);
            sfdcURL ='/'+billprofileId; 
            System.debug('Bill Profile ID ' +billprofileId );
            
            //return (new PageReference(sfdcURL));
        }else if ( recordType =='Request Quote Support'){
               //
           try{                                         
               ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
               String contIdPrefix = contObj.ObjectIdPrefix__c;               
               String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
               String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
               String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
               String date2 ='&'+Adhoc_Request__c.getValues('Due Date').value__c+'=';
               String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
               Date dt = System.Today()+2; 
                String dt2 = dt.format();
                if (oppId!=null){
                   Opportunity ObjOpp = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];                                
                   sfdcURL ='/'+contIdPrefix+'/e?'+Opp+EncodingUtil.urlEncode(ObjOpp.Name,'UTF-8')+Opplkpup+oppId+account+EncodingUtil.urlEncode(ObjOpp.Account.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+rt.Id; 
                   }else{
                       account = Adhoc_Request__c.getValues('Account').value__c+'=';
                       Account acct2 = [Select id,Name from Account where id=:accountId limit 1];
                       sfdcURL ='/'+contIdPrefix+'/e?'+account+EncodingUtil.urlEncode(acct2.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+accountId+'&nooverride=1&RecordType='+rt.Id; 
                   }
                // Add Condition for Date and others.                              
            
            } catch(Exception excp) {
                ErrorHandlerException.ExecutingClassName='HandleRequestController:redirectToOpportunity';         
                ErrorHandlerException.sendException(excp); 
                System.debug('Error In HandleRequestController');
            }
            
        }else if ( recordType =='Request Order Support'){
               //
           try{                                         
               ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
               String contIdPrefix = contObj.ObjectIdPrefix__c;               
               String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
               String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
               String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
               String date2 ='&'+Adhoc_Request__c.getValues('Due Date').value__c+'=';
               String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
               Date dt = System.Today()+5;
               //Date dt; 
              // Integer WeekDay = (System.TODAY().daysBetween(Date.newInstance(2000,1, 3)))/7;
               //if(WeekDay==0){dt=System.Today()+4;}else{dt=System.Today()+5;}
                String dt2 = dt.format();
                if (oppId!=null){
                   Opportunity ObjOpp = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];                                
                   sfdcURL ='/'+contIdPrefix+'/e?'+Opp+EncodingUtil.urlEncode(ObjOpp.Name,'UTF-8')+Opplkpup+oppId+account+EncodingUtil.urlEncode(ObjOpp.Account.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+rt.Id; 
                   }else{
                       account = Adhoc_Request__c.getValues('Account').value__c+'=';
                       Account acct2 = [Select id,Name from Account where id=:accountId limit 1];
                       sfdcURL ='/'+contIdPrefix+'/e?'+account+EncodingUtil.urlEncode(acct2.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+accountId+'&nooverride=1&RecordType='+rt.Id; 
                   }
                // Add Condition for Date and others.                              
            
            } catch(Exception excp) {
                System.debug('Error In HandleRequestController');
                ErrorHandlerException.ExecutingClassName='HandleRequestController:redirectToOpportunity';         
                ErrorHandlerException.sendException(excp);
            }
            
        }else if ( recordType =='Request Account Support'){
               //
           try{                                         
               ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
               String contIdPrefix = contObj.ObjectIdPrefix__c;               
               String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
               String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
               String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
               String date2 ='&'+Adhoc_Request__c.getValues('Due Date').value__c+'=';
               String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
               Date dt = System.Today()+2; 
                String dt2 = dt.format();
                if (oppId!=null){
                   Opportunity ObjOpp = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];                                
                   sfdcURL ='/'+contIdPrefix+'/e?'+Opp+EncodingUtil.urlEncode(ObjOpp.Name,'UTF-8')+Opplkpup+oppId+account+EncodingUtil.urlEncode(ObjOpp.Account.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+rt.Id; 
                    //sfdcURL = EncodingUtil.urlEncode(sfdcURL,'UTF-8');
                   }else{
                       account = Adhoc_Request__c.getValues('Account').value__c+'=';
                       Account acct2 = [Select id,Name from Account where id=:accountId limit 1];
                       sfdcURL ='/'+contIdPrefix+'/e?'+account+EncodingUtil.urlEncode(acct2.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+accountId+'&nooverride=1&RecordType='+rt.Id; 
                       //sfdcURL = EncodingUtil.urlEncode(sfdcURL,'UTF-8');
                   }
                // Add Condition for Date and others.                              
            
            } catch(Exception excp) {
                System.debug('Error In HandleRequestController');
                ErrorHandlerException.ExecutingClassName='HandleRequestController:redirectToOpportunity';         
                ErrorHandlerException.sendException(excp);
            }
            
        }
        
        else if(recordType == 'Adhoc Request'){
                //
           try{
                
              //   objActionItem = Util.createActionItem('Adhoc Request', accountOppbillProfileMapId, oppId, billprofileId);  
               //  objActionItem.Subject__c = 'Adhoc Request';
               //  insert objActionItem;  
               //  sfdcURL ='/'+oppId;    
           //Opportunity o = [select Name,Account.Name from Opportunity where Id=:oppId Limit 1]; 
   
            //sfdcURL ='/a0z/e?CF00NO0000000IEKf='+o.Name+'&CF00NO0000000IEKf_lkid='+oppId+'&CF00NO0000000IELc='+o.Account.Name+'&CF00NO0000000IELc_lkid='+accountId+'&retURL=%2F'+oppId+'&RecordType='+rt.Id;
            
               ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
               String contIdPrefix = contObj.ObjectIdPrefix__c;
               Id recordTypeId = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Adhoc Request');
               String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
               String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
               String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
               String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
               //system.debug('custom settings*******'+account + '***'+accountlkpup +'***'+ Opp + '******'+ Opplkpup);
               if (OppId!=null){
                   Opportunity ObjOpp = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];
                
                   //system.debug('Opportunity Name'+ ObjOpp.Name + 'account Name'+ ObjOpp.Account.Name);
                   sfdcURL ='/'+contIdPrefix+'/e?'+Opp+ObjOpp.Name+Opplkpup+oppId+account+ObjOpp.Account.Name+ accountlkpup+accountId+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+recordTypeId; 
                   System.debug('The SFDCURL is '+sfdcURL); 
              }else{
               Account acctmp = [Select Id,Name from Account where Id=:accountId limit 1];
               sfdcURL ='/'+contIdPrefix+'/e?'+account+acctmp.Name+ accountlkpup+accountId+'&retURL=%2F'+accountId+'&nooverride=1&RecordType='+recordTypeId;                 
               }    
            
            } catch(Exception excp) {
                System.debug('Error In HandleRequestController');
                ErrorHandlerException.ExecutingClassName='HandleRequestController:redirectToOpportunity';         
                ErrorHandlerException.sendException(excp);
            }
                 /*IR065/SOMP/Action Item*/ 
        }
        
         /* IR 122 New Logo Record Type */
        else if (recordType == 'New Logo for Manual Process' || recordType == 'Win Back for Manual Process')
        {
            try{
                 ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
                 String contIdPrefix = contObj.ObjectIdPrefix__c;
                 Map<String,NewLogo__c> newLogoCustMap = NewLogo__c.getAll();
                 
                 String profName=[Select Id,Name from Profile where Id=:userinfo.getProfileId()].Name;  
                
                 String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
                 String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
                 String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
                 String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
                
                if (accountId!=null && (profName == newLogoCustMap.get('Logo_System_Administrator').Action_Item__c
                     || profName == newLogoCustMap.get('Logo_TI_Sales_Admin').Action_Item__c || profName == newLogoCustMap.get('Logo_TI_Account_Manager').Action_Item__c ||
                     profName == newLogoCustMap.get('Logo_TI_Sales_Manager').Action_Item__c ||  profName == newLogoCustMap.get('Logo_TI_HR').Action_Item__c))
                { 
                    Account acctmp = [Select Id,Name from Account where Id=:accountId limit 1];
                    sfdcURL ='/'+contIdPrefix+'/e?'+account+acctmp.Name+ accountlkpup+accountId+'&retURL=%2F'+accountId+'&nooverride=1&RecordType='+rt.Id;                  
                   
                }
                
                else
                {
                    sfdcURL = '/'+'/apex/NewLogoWinBackCustomer';
                }   
                 
            } catch(Exception excp) {
                System.debug('Error In HandleRequestController');
                ErrorHandlerException.ExecutingClassName='HandleRequestController:redirectToOpportunity';         
                ErrorHandlerException.sendException(excp);
            }
        }  else if(recordType == 'Request to Modify Opportunity Split'){
            System.debug('Accountuaaa'+accountOppbillProfileMapId.get(oppId));
          
            ObjectPrefixIdentifiers__c contObj1= ObjectPrefixIdentifiers__c.getInstance('Action Item');
            String contIdPrefix1 = contObj1.ObjectIdPrefix__c;
            String account1 = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
            String accountlkpup1 = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
            String Opp1 = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
            String date21 ='&'+Adhoc_Request__c.getValues('Due Date').value__c+'=';
            String Opplkpup1 = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
            String Subject1 = '&'+Adhoc_Request__c.getValues('Subject').value__c+'=';
            Date dt1 = System.Today()+2;
            String dt12 = dt1.format();
            RecordType rt1 = [select Id,Name from RecordType where Name=:recordType];
            String s1 = rt1.ID;
            String retURL = oppId;
            if (oppId!=null){
                Opportunity ObjOpp1 = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];
            
                sfdcURL ='/'+contIdPrefix1+'/e?'+Opp1+EncodingUtil.urlEncode(ObjOpp1.Name,'UTF-8')+Opplkpup1+oppId+account1+EncodingUtil.urlEncode(ObjOpp1.Account.Name,'UTF-8')+ accountlkpup1+accountId+date21+dt12+Subject1+'Request to Modify Opportunity Split'+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+rt.Id;
            }
            //return (new PageReference(sfdcURL));
            }       
        
       /* End of IR 222 NEW LOGO */
        
        else if(recordType =='Request Stage Reversal'){
            
             ObjectPrefixIdentifiers__c contObj= ObjectPrefixIdentifiers__c.getInstance('Action Item');
             
              Opportunity ObjOpp = [select Id,Name,Account.Name from Opportunity where Id=:oppId Limit 1];
             
               String contIdPrefix = contObj.ObjectIdPrefix__c;               
               String account = '&'+Adhoc_Request__c.getValues('Account').value__c+'=';
               String accountlkpup = '&'+ Adhoc_Request__c.getValues('Account Lookup').value__c + '=';
               String Opp = Adhoc_Request__c.getValues('Opportunity').value__c +'=';
               String date2 ='&'+Adhoc_Request__c.getValues('Due Date').value__c+'=';
               String Opplkpup = '&'+Adhoc_Request__c.getValues('Opportunity Lookup').value__c+'=';
               Date dt = System.Today()+2; 
               String dt2 = dt.format();
            
                system.debug('-----------recordtype------'+rt.id);
            
            
            sfdcURL ='/'+contIdPrefix+'/e?'+Opp+EncodingUtil.urlEncode(ObjOpp.Name,'UTF-8')+Opplkpup+oppId+account+EncodingUtil.urlEncode(ObjOpp.Account.Name,'UTF-8')+ accountlkpup+accountId+date2+dt2+'&retURL=%2F'+oppId+'&nooverride=1&RecordType='+rt.Id;         
                    
            
            
        }
        
            return (new PageReference(sfdcURL));
            
    }
    
}