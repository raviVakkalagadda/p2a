public class IPVPNServiceSelectionCtlr {
    private Id MasterServiceId;
    private Id BasketId;
    private Id screenFlowId;
    public String selectedProductId {get;set;}
    private Map<Id, csord__Service__c> RelatedServiceMap;
    private static final String BaseURL = '/apex/csmle1__MultiLineEditor?id={0}&productDefinitionId={1}&batchSize=5&showHeader=false&sideBar=false&retURL={2}&screenFlowId={3}';
    
    public IPVPNServiceSelectionCtlr(ApexPages.StandardController ctrl){
        MasterServiceId = ctrl.getRecord().Id;
        BasketId = [select csordtelcoa__Product_Basket__c from csord__Service__c where Id =:MasterServiceId].get(0).csordtelcoa__Product_Basket__c;
        try{
            RelatedServiceMap = new Map<Id, csord__Service__c>([select Id, Name, Master_Service__c,csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.csordtelcoa__Screen_Flow_For_Service_Changes__c
                                                            from csord__Service__c where Master_Service__c =:MasterServiceId]);

            if(RelatedServiceMap.isEmpty())
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Services available for selection'));
            //else
                //screenFlowId = RelatedServiceMap.values().get(0).csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.csordtelcoa__Screen_Flow_For_Service_Changes__c;
        }
        catch(Exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Services available for selection'));
        }
        
    }

    public List<SelectOption> getProductsToSelect(){
        List<SelectOption> ProductsToSelect = new List<SelectOption>();
        Set<String> SetUniqueServ = new Set<String>();
        for(csord__Service__c thisServ : RelatedServiceMap.values()){
            if(SetUniqueServ.isEmpty() || !SetUniqueServ.contains(thisServ.Name)){
                SetUniqueServ.add(thisServ.Name);
                ProductsToSelect.add(new SelectOption(thisServ.Id, thisServ.Name));
                selectedProductId = thisServ.Id;
            }
        }
        return ProductsToSelect;
    }

    public PageReference EditProduct(){
        /*RelatedServiceMap = new Map<Id, csord__Service__c>([select Id, Name, csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.csordtelcoa__Screen_Flow_For_Service_Changes__c
                                                            from csord__Service__c where Id =:selectedProductId]);
        */  
        //try{
        Id ProdDefnId = [select Id, csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c
                            from csord__Service__c where Id =:selectedProductId].get(0).csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__c;
        //ProdDefnId = 'a26O0000000dWq4IAE';
        System.debug('ProdDefnId ==> ' + ProdDefnId);
        screenFlowId = [select Id, cscfga__Screen_Flow__c, cscfga__Product_Definition__c from cscfga__Screen_Flow_Product_Association__c
                        where cscfga__Screen_Flow__r.Name = 'IPVPNPortMLE' AND cscfga__Product_Definition__c =:ProdDefnId].get(0).cscfga__Screen_Flow__c;
        //screenFlowId = 'a2CO0000002OYMPMA4';
        System.debug('ProdDefnId ==> ' + ProdDefnId + ' screenFlowId ==> ' + screenFlowId);
        return new PageReference(String.format(BaseURL, new String[]{BasketId, ProdDefnId, MasterServiceId, screenFlowId}));
        //}
        /*catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No Services available for selection' + ApexPages.getMessages()));
            return null;
        }*/
        
    }

}