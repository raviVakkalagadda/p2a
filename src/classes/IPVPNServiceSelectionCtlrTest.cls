@isTest(seealldata=false)
public with sharing class IPVPNServiceSelectionCtlrTest {
    
    static testmethod void TestIPVPNServiceSelectionCtlr() {
    P2A_TestFactoryCls.sampleTestData();
        csord__Service__c masterServ = new csord__Service__c();
        List<csord__Service__c> childServs = new List<csord__Service__c>(); 
        csord__Service__c childServ = new csord__Service__c();
        
        //Map<id,csord__Order__c> mprodo= new Map<id,csord__Order__c>();
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        List<cscfga__Product_basket__c> baskets =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<csord__Order_Request__c> ordReqId = P2A_TestFactoryCls.getorderrequest(1);
        //RecordType rc = [select id from RecordType where DeveloperName='OffNet_Record_Type' limit 1];
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, ordReqId);
        List<csord__Order__c> order = P2A_TestFactoryCls.getorder(5,ordReqId);    
        //List<csord__service__c> servList = P2A_TestFactoryCls.getservice(5,OrdReqList,subscriptionList);    
        List<case> caseList=P2A_TestFactoryCls.getcase(1,ListAcc); 
        /*
      //  Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
           csord__Order_Request__c ordReqId = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert ordReqId;
        */
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        system.assert(prodDefintion!=null);
        List<cscfga__Product_Definition__c> pd = [Select id,name from cscfga__Product_Definition__c where name ='Test definition 1'];
        system.assertequals(prodDefintion.name,pd[0].name);
        /*
        List<cscfga__Product_Basket__c> baskets = new List<cscfga__Product_Basket__c>();
        //for (Integer i = 0; i < (multipleBaskets ? 2 : 1); i++) {
            baskets.add(new cscfga__Product_Basket__c());
        //}
        insert baskets;
        */
        List<cscfga__Product_Configuration__c> configs = new List<cscfga__Product_Configuration__c>();
        //for (Integer i = 0; i < (multipleBaskets ? 2 : 1); i++ ) {
            configs.add(new cscfga__Product_Configuration__c(Name = 'Test config '
                , cscfga__Product_Definition__c = prodDefintion.Id
                , cscfga__Product_Basket__c = baskets[0].Id));
        //}
        insert configs;
        /*
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        insert order;
        */
        csord__Subscription__c sub = new csord__Subscription__c(csord__order__c = order[0].Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId[0].Id);
        insert sub;
        system.assert(sub!=null);
        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        //masterServ.Master_Service__c = '';
        masterServ.csord__Subscription__c = sub.Id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.csord__Order_Request__c = ordReqId[0].Id;//'a2bO00000018nkO';
        insert masterServ;

        for(integer xx=0;xx<3;xx++){
            childServ = new csord__Service__c();
            childServ.csordtelcoa__Service_Number__c = '1234-' + xx;
            childServ.Name = 'Test Serv 1234-' + xx;
            childServ.Master_Service__c = masterServ.Id;
            childServ.csord__Subscription__c = sub.Id;
            childServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
            childServ.csord__Order_Request__c = ordReqId[0].Id;//'a2bO00000018nkO';
            childServ.csordtelcoa__Product_Configuration__c = configs.get(0).Id;
            childServs.add(childServ); 
        }
        insert childServs;
        system.assert(childServs!=null);
        cscfga__Screen_Flow__c sf = new cscfga__Screen_Flow__c(name='IPVPNPortMLE', 
                                            cscfga__Template_Reference__c = 'IPVPNPortMLE', MLEFlow__c=true);
        insert sf;

        cscfga__Screen_Flow_Product_Association__c sfProdAssoc = new cscfga__Screen_Flow_Product_Association__c(cscfga__Screen_Flow__c = sf.Id,
                                                                    cscfga__Product_Definition__c = prodDefintion.ID);
        insert sfProdAssoc;
        
        Test.startTest();
        
        IPVPNServiceSelectionCtlr mastCtrl = new IPVPNServiceSelectionCtlr(new ApexPages.StandardController(masterServ));
        mastCtrl.getProductsToSelect();
        mastCtrl.EditProduct();
        Test.stopTest();
        
    }
}