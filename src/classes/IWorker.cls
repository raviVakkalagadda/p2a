public interface IWorker {
    void work(Object params, Object context);
}