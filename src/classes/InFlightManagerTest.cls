@isTest(SeeAllData=false)
public class InFlightManagerTest{
@isTest

  public static void InFlightManagerTest_Method1(){
      
    
    P2A_TestFactoryCls.sampleTestData();
     
    List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    list<cscfga__Product_Category__c> prodCategoryLists=  P2A_TestFactoryCls.getProductCategory(1);   
    cscfga__Product_Category__c prodCategoryList; 
    List<csbb__Product_Configuration_Request__c> prodConfReq = P2A_TestFactoryCls.getProductonfigreq(1,prodCategoryLists);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    List<csord__Order_Request__c> requestList = P2A_TestFactoryCls.getorderrequest(1);
    List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,requestList);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);   
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, requestList);
    List<csord__Service__c> serviceList = P2A_TestFactoryCls.getService(1, requestList, subscriptionList);
    List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1, serviceList,requestList);
    system.assertEquals(true,serLineItemList!=null); 
    List<cscfga__Product_Basket__c> basketList = P2A_TestFactoryCls.getProductBasket(1);
    Exception ee = null;
        try{
        
        
        Test.startTest();  

        InFlightManager.cancelOrders(orderList);
        InFlightManager.transferClonedServiceLineItemDetails(prodBaskList[0].Id);
        InFlightManager.cancelOrder(orderList[0]);
        Test.stopTest();       
        }
        catch(Exception e){
            ErrorHandlerException.ExecutingClassName='InFlightManagerTest :InFlightManagerTest_Method1';         
            ErrorHandlerException.sendException(e); 
                ee = e;
                }
            finally {
                if(ee != null){
                    throw ee;
                }
            }
    }        
    
    public static testMethod void InFlightManagerTest_Method2(){
  
     P2A_TestFactoryCls.sampleTestData();
     
    List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    list<cscfga__Product_Category__c> prodCategoryLists=  P2A_TestFactoryCls.getProductCategory(1);   
    cscfga__Product_Category__c prodCategoryList; 
    List<csbb__Product_Configuration_Request__c> prodConfReq = P2A_TestFactoryCls.getProductonfigreq(1,prodCategoryLists);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    List<csord__Order_Request__c> requestList = P2A_TestFactoryCls.getorderrequest(1);
    List<csord__Order__c> orderList = P2A_TestFactoryCls.getorder(1,requestList);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);   
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, requestList);
    List<csord__Service__c> serviceList = P2A_TestFactoryCls.getService(1, requestList, subscriptionList);
    List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1, serviceList,requestList);
    system.assertEquals(true,serLineItemList!=null); 
    List<cscfga__Product_Basket__c> basketList = P2A_TestFactoryCls.getProductBasket(1);
      
    Exception ee = null;
        try{
        
        Test.startTest();
        InFlightManager.transferClonedServiceLineItemDetails(prodBaskList[0].Id);
        Test.stopTest();      
        }
        catch(Exception e){
            ErrorHandlerException.ExecutingClassName='InFlightManagerTest :InFlightManagerTest_Method2';         
            ErrorHandlerException.sendException(e); 
                ee = e;
                }
            finally {
                if(ee != null){
                    throw ee;
                }
            }
    }
    @istest
    public static void negativescenario()
    {
     //insert account  
    P2A_TestFactoryCls.sampleTestData();
    List<Account> acclist = new List<Account>();
    account a = new account();
    a.name    = 'Test Accenture ';
    a.BillingCountry  = 'GB';
    a.Activated__c  = True;
    a.Account_ID__c  = 'test';
    a.Account_Status__c = 'Active';
    a.Customer_Legal_Entity_Name__c = 'Test';
    a.Customer_Type_New__c = 'MNO';
    a.Region__c = 'Australia';
    a.Website='abc.com';
    a.Selling_Entity__c='Telstra Limited';
    a.Industry__c='BPO';
    a.Account_ORIG_ID_DM__c = 'test';
    acclist.add(a);
    insert acclist;
    system.assert(acclist[0].id!=null); 
    
    //insert opportunity
    
        List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity(); 
        opp.AccountId = acclist[0].id;
        opp.name  = 'Test Opp ';
        opp.stagename = 'Identify & Define';
        opp.CloseDate= system.today()+5;
        opp.Pre_Contract_Provisioning_Required__c = 'yes';
        opp.QuoteStatus__c = 'draft';
        opp.Product_Type__c = 'IPL';
        opp.Estimated_MRC__c = 100.00;
        opp.Estimated_NRC__c = 100.00;
        opp.Opportunity_Type__c = 'Simple';
        opp.Sales_Status__c = 'Open';
        opp.Order_Type__c = 'New';
        opp.Stage__c='Identify & Define';
        opp.ContractTerm__c='10'; 
        opp.CreditCheckDoneBy__c= 'testname';
        opplist.add(opp);
        insert opplist ;
        system.assert(opplist[0].id!=null);
        
         List<cscfga__Product_Basket__c> ProductBasketlist = new List<cscfga__Product_Basket__c>();
          cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
                p.csordtelcoa__Synchronised_with_Opportunity__c = true;
                p.Name = 'New Basket' + system.now();
                p.csordtelcoa__Account__c = acclist[0].id;
                p.cscfga__Opportunity__c = opplist[0].id ;
                p.Quote_Status__c = 'Pending Pricing Approval';
                p.csordtelcoa__Change_Type__c = 'Upgrade';
                p.cscfga__total_contract_value__c = 34;
                p.csbb__Synchronised_With_Opportunity__c = true;
                ProductBasketlist.add(p);
                
               
               
           cscfga__Product_Basket__c p1 = new cscfga__Product_Basket__c();
                p1.csordtelcoa__Synchronised_with_Opportunity__c = false;
               p1.Name = 'New Basket' + system.now();
                p1.csordtelcoa__Account__c = acclist[0].id;
                p1.cscfga__Opportunity__c = opplist[0].id ;
                p1.Quote_Status__c = 'Pending Pricing Approval';
                p1.csordtelcoa__Change_Type__c = ProductBasketlist[0].id;
                p1.cscfga__total_contract_value__c = 20;
                p1.csbb__Synchronised_With_Opportunity__c = false;
                p1.cscfga__Basket_Status__c = 'Valid';
                ProductBasketlist.add(p1);
              insert ProductBasketlist ; 
               system.assert(ProductBasketlist[0].id!=null); 
                 
               
                 cscfga__Product_Basket__c p3 = new cscfga__Product_Basket__c();
                 p3= ProductBasketlist[0].clone(false, true, false, false);
               
               
               List<cscfga__Product_Definition__c> ProductDeflist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c();
        pb.name = 'Master IPVPN Service'; 
        pb.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        ProductDeflist.add(pb);
        insert ProductDeflist ;
               
                List<cscfga__Product_Configuration__c> ProductConfiglist = new List<cscfga__Product_Configuration__c>(); 
                cscfga__Product_Configuration__c pb10 = new cscfga__Product_Configuration__c();
                pb10.Name = 'Standalone ASBR';
                pb10.Product_Name__c = 'IPT';
                pb10.cscfga__Product_Family__c ='Master IPVPN Service';
                pb10.Internet_Onnet_BillingType__c = 'Aggregated';
                pb10.cscfga__Product_Basket__c = ProductBasketlist[0].id;
                pb10.cscfga__Product_Definition__c = ProductDeflist[0].id;
                pb10.Product_Code__c = 'ASBRB';
                ProductConfiglist.add(pb10);
               
                insert ProductConfiglist;
                
    
        
                
         List<cscfga__Configuration_Screen__c> configList = new List<cscfga__Configuration_Screen__c>();
           cscfga__Configuration_Screen__c configScrList = new cscfga__Configuration_Screen__c();
           configScrList.name = 'test';
           configScrList.cscfga__Product_Definition__c = ProductDeflist[0].id;
           configList.add(configScrList);
           insert configList ;
           
      List<cscfga__Screen_Section__c>  ScrSecList = new List<cscfga__Screen_Section__c>();
       cscfga__Screen_Section__c SSecList = new cscfga__Screen_Section__c(); 
       SSecList.name = 'test';
        SSecList.cscfga__Configuration_Screen__c = configList[0].id;
        ScrSecList.add(SSecList); 
        insert ScrSecList ;   
        
     List<cscfga__Attribute_Definition__c> AttributesdefList = new List<cscfga__Attribute_Definition__c>(); 
     cscfga__Attribute_Definition__c  Attributesdef = new cscfga__Attribute_Definition__c();
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c= ProductDeflist [0].id;
                Attributesdef.cscfga__Configuration_Screen__c = configList[0].id;
                Attributesdef.cscfga__Screen_Section__c = ScrSecList[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                AttributesdefList.add(Attributesdef); 
                insert AttributesdefList ;   
                
                
            List<cscfga__Attribute__c>  AttributesList = new List<cscfga__Attribute__c>();
              cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= ProductConfiglist[0].id;
                Attributes.cscfga__Value__c = 'Test';
                Attributes.cscfga__Attribute_Definition__c = AttributesdefList[0].id; 
                AttributesList.add(Attributes); 
                insert AttributesList ;              
       
       //system.assert(ProductConfiglist[0].id!=null);
        
        List<csord__Order_Request__c> OredrRequetslist = new List<csord__Order_Request__c>();
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        OredrRequetslist.add(ordreq); 
        insert  OredrRequetslist ;
        system.assert(OredrRequetslist[0].id!=null); 
        
      List<csord__Subscription__c> Subscriptionlist = new List<csord__Subscription__c>();
      csord__Subscription__c sub = new csord__Subscription__c();
                 sub.Name ='Test'; 
                sub.csord__Identification__c = 'Test-Catlyne-4238362';
                sub.csord__Order_Request__c = OredrRequetslist[0].id;
                sub.OrderType__c = null;
                Subscriptionlist.add(sub);
                insert Subscriptionlist;
        
            List<csord__Order__c> Orderlist = new List<csord__Order__c>();          
            csord__Order__c ord = new csord__Order__c();
            ord.csord__Identification__c = 'Test-JohnSnow-4238362';
            ord.RAG_Order_Status_RED__c = false;
            ord.RAG_Reason_Code__c = '';
            ord.Jeopardy_Case__c = null;
            ord.csord__Order_Request__c = OredrRequetslist [0].id;
            Orderlist.add(ord);
           //system.assert(Orderlist[0].id!=null);  
           
            List<csord__Service__c> Servicelist = new List<csord__Service__c>();
            csord__Service__c ser = new csord__Service__c();
            ser.Name = 'Test Service'; 
            ser.csord__Identification__c = 'Test-Catlyne-4238362';
            ser.csord__Order_Request__c = OredrRequetslist[0].id;
            ser.csord__Subscription__c = Subscriptionlist[0].id;
            ser.Billing_Commencement_Date__c = System.Today();
            ser.Stop_Billing_Date__c = System.Today();
            ser.RAG_Status_Red__c = false ; 
            ser.RAG_Reason_Code__c = '';                            
            // if(ser.id != null)
            ser.csordtelcoa__Product_Basket__c = ProductBasketlist[0].id; 
            ser.csordtelcoa__Product_Configuration__c = ProductConfiglist[0].id;
            ser.Product_Id__c = 'IPVPN';     
            ser.Path_Instance_ID__c = 'test';                 
            Servicelist.add(ser);
            insert Servicelist ;
            
            Test.starttest();
                InFlightManager inflight = new InFlightManager ();
                cscfga__Product_Basket__c prod = InFlightManager.cloneSyncedBasket(opplist[0],Orderlist);
                
                
              Test.stoptest(); 
               
    
    }
    @istest
    public static void negativescenario1()
    {
     P2A_TestFactoryCls.sampleTestData();
    List<Account> acclist = new List<Account>();
    account a = new account();
    a.name    = 'Test Accenture ';
    a.BillingCountry  = 'GB';
    a.Activated__c  = True;
    a.Account_ID__c  = 'test';
    a.Account_Status__c = 'Active';
    a.Customer_Legal_Entity_Name__c = 'Test';
    a.Customer_Type_New__c = 'MNO';
    a.Region__c = 'Australia';
    a.Website='abc.com';
    a.Selling_Entity__c='Telstra Limited';
    a.Industry__c='BPO';
    a.Account_ORIG_ID_DM__c = 'test';
    acclist.add(a);
    insert acclist;
    system.assert(acclist[0].id!=null);  
     
     
     List<Opportunity> opplist = new List<Opportunity>();
        Opportunity opp = new Opportunity(); 
        opp.AccountId = acclist[0].id;
        opp.name  = 'Test Opp ';
        opp.stagename = 'Identify & Define';
        opp.CloseDate= system.today()+5;
        opp.Pre_Contract_Provisioning_Required__c = 'yes';
        opp.QuoteStatus__c = 'draft';
        opp.Product_Type__c = 'IPL';
        opp.Estimated_MRC__c = 100.00;
        opp.Estimated_NRC__c = 100.00;
        opp.Opportunity_Type__c = 'Simple';
        opp.Sales_Status__c = 'Open';
        opp.Order_Type__c = 'New';
        opp.Stage__c='Identify & Define';
        opp.ContractTerm__c='10'; 
        opp.CreditCheckDoneBy__c= 'testname';
        opplist.add(opp);
        insert opplist ;
        system.assert(opplist[0].id!=null);
        
         List<cscfga__Product_Basket__c> ProductBasketlist = new List<cscfga__Product_Basket__c>();
          cscfga__Product_Basket__c p = new cscfga__Product_Basket__c();
                p.csordtelcoa__Synchronised_with_Opportunity__c = true;
                p.Name = 'New Basket' + system.now();
                p.csordtelcoa__Account__c = acclist[0].id;
                p.cscfga__Opportunity__c = opplist[0].id ;
                p.Quote_Status__c = 'Pending Pricing Approval';
                p.csordtelcoa__Change_Type__c = 'Upgrade';
                p.cscfga__total_contract_value__c = 34;
                p.csbb__Synchronised_With_Opportunity__c = true;
                ProductBasketlist.add(p);
                insert ProductBasketlist ;
            system.assert(ProductBasketlist[0].id!=null);  
            
             
         List<cscfga__Product_Definition__c> ProductDeflist = new List<cscfga__Product_Definition__c>();
        cscfga__Product_Definition__c pb = new cscfga__Product_Definition__c();
        pb.name = 'Master IPVPN Service'; 
        pb.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        ProductDeflist.add(pb);
        insert ProductDeflist ;
            
            List<cscfga__Product_Configuration__c> ProductConfiglist = new List<cscfga__Product_Configuration__c>(); 
                cscfga__Product_Configuration__c pb10 = new cscfga__Product_Configuration__c();
                pb10.Name = 'Standalone ASBR';
                pb10.Product_Name__c = 'IPT';
                pb10.cscfga__Product_Family__c ='Master IPVPN Service';
                pb10.Internet_Onnet_BillingType__c = 'Aggregated';
                pb10.cscfga__Product_Basket__c = ProductBasketlist[0].id;
               pb10.cscfga__Product_Definition__c = ProductDeflist[0].id;
                pb10.csordtelcoa__Ignore_For_Order_Decomposition__c = true;
                pb10.Product_Code__c = 'ASBRB';
                ProductConfiglist.add(pb10);
                insert ProductConfiglist;
                
                List<cscfga__Configuration_Screen__c> configList = new List<cscfga__Configuration_Screen__c>();
           cscfga__Configuration_Screen__c configScrList = new cscfga__Configuration_Screen__c();
           configScrList.name = 'test';
           configScrList.cscfga__Product_Definition__c = ProductDeflist[0].id;
           configList.add(configScrList);
           insert configList ;
           
      List<cscfga__Screen_Section__c>  ScrSecList = new List<cscfga__Screen_Section__c>();
       cscfga__Screen_Section__c SSecList = new cscfga__Screen_Section__c(); 
       SSecList.name = 'test';
        SSecList.cscfga__Configuration_Screen__c = configList[0].id;
        ScrSecList.add(SSecList); 
        insert ScrSecList ;   
        
     List<cscfga__Attribute_Definition__c> AttributesdefList = new List<cscfga__Attribute_Definition__c>(); 
     cscfga__Attribute_Definition__c  Attributesdef = new cscfga__Attribute_Definition__c();
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c= ProductDeflist [0].id;
                Attributesdef.cscfga__Configuration_Screen__c = configList[0].id;
                Attributesdef.cscfga__Screen_Section__c = ScrSecList[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                AttributesdefList.add(Attributesdef); 
                insert AttributesdefList ;   
                
                
            List<cscfga__Attribute__c>  AttributesList = new List<cscfga__Attribute__c>();
              cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
                Attributes.name = 'GCPE';
                Attributes.cscfga__Product_Configuration__c= ProductConfiglist[0].id;
                Attributes.cscfga__Value__c = 'Test';
                Attributes.cscfga__Attribute_Definition__c = AttributesdefList[0].id; 
                Attributes.cscfga__Is_Line_Item__c = true ;
                Attributes.cscfga__is_active__c = true;
                Attributes.cscfga__Recurring__c = true;
                Attributes.cscfga__Is_rate_line_item__c = false;
                AttributesList.add(Attributes); 
                insert AttributesList ;             
                
                List<csord__Order_Request__c> OredrRequetslist = new List<csord__Order_Request__c>();
        csord__Order_Request__c ordreq = new csord__Order_Request__c();
        ordreq.Name = 'OrderName';
        ordreq.csord__Module_Name__c = 'SansaStark123683468Test';
        ordreq.csord__Module_Version__c = 'YouknownothingJhonSwon';
        ordreq.csord__Process_Status__c = 'Requested';
        ordreq.csord__Request_DateTime__c = System.now();
        OredrRequetslist.add(ordreq); 
        insert  OredrRequetslist ;
        system.assert(OredrRequetslist[0].id!=null); 
        
      List<csord__Subscription__c> Subscriptionlist = new List<csord__Subscription__c>();
      csord__Subscription__c sub = new csord__Subscription__c();
                 sub.Name ='Test'; 
                sub.csord__Identification__c = 'Test-Catlyne-4238362';
                sub.csord__Order_Request__c = OredrRequetslist[0].id;
                sub.OrderType__c = null;
                Subscriptionlist.add(sub);
                insert Subscriptionlist;
        
            List<csord__Order__c> Orderlist = new List<csord__Order__c>();          
            csord__Order__c ord = new csord__Order__c();
            ord.csord__Identification__c = 'Test-JohnSnow-4238362';
            ord.RAG_Order_Status_RED__c = false;
            ord.RAG_Reason_Code__c = '';
            ord.Jeopardy_Case__c = null;
            ord.csord__Order_Request__c = OredrRequetslist [0].id;
            Orderlist.add(ord);
           //system.assert(Orderlist[0].id!=null);  
           
            List<csord__Service__c> Servicelist = new List<csord__Service__c>();
            csord__Service__c ser = new csord__Service__c();
            ser.Name = 'Test Service'; 
            ser.csord__Identification__c = 'Test-Catlyne-4238362';
            ser.csord__Order_Request__c = OredrRequetslist[0].id;
            ser.csord__Subscription__c = Subscriptionlist[0].id;
            ser.Billing_Commencement_Date__c = System.Today();
            ser.Stop_Billing_Date__c = System.Today();
            ser.RAG_Status_Red__c = false ; 
            ser.RAG_Reason_Code__c = '';                            
            // if(ser.id != null)
            ser.csordtelcoa__Product_Basket__c = ProductBasketlist[0].id; 
            ser.csordtelcoa__Product_Configuration__c = ProductConfiglist[0].id;
            ser.Product_Id__c = 'IPVPN';     
            ser.Path_Instance_ID__c = 'test';                 
            Servicelist.add(ser);
            insert Servicelist; 
            
         List<csord__Service_Line_Item__c> serLineItemList = new List<csord__Service_Line_Item__c>();
       csord__Service_Line_Item__c servLineItem = new csord__Service_Line_Item__c();
                servLineItem.csord__Service__c = Servicelist[0].id;
                servLineItem.csord__Is_Recurring__c = true;
                servLineItem.Is_Miscellaneous_Credit_Flag__c = true;                                               
                servLineItem.Transaction_Status__c= null;                                               
                servLineItem.Type_of_Charge__c= 'RCR - Recurring credit';
                servLineItem.Is_ETC_Line_Item__c= false;  
                servLineItem.MISCService__c = servLineItem.csord__Service__c;
                servLineItem.MISC_Charge_Amount__c=90;
                servLineItem.Frequency__c='Monthly';
                servLineItem.Billing_End_Date__c = System.Today(); 
                servLineItem.csord__Identification__c ='hnerkfwfefwrjge';
                servLineItem.csord__Order_Request__c= Orderlist[0].id;
                servLineItem.Net_MRC_Price__c = 40;
                servLineItem.Net_NRC_Price__c = 40;
                servLineItem.csord__Total_Price__c = 40;
                
                
                serLineItemList.add(servLineItem);
                insert serLineItemList ;
                
                Test.starttest();
              //  InFlightManagerTest infigh = new InFlightManagerTest();
              // InFlightManager.transferClonedServiceLineItemDetails(ProductBasketlist[0].Id);
                InFlightManager.transferClonedServiceLineItemDetails(ProductBasketlist[0].id);
                InFlightManager.setScreenFlows(ProductConfiglist);
                Test.stoptest();
                
    
    }
}