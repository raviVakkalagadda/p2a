global with sharing class InternetTable extends cscfga.ALookupSearch {
    
   public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){


        String country = searchFields.get('Choose Country');
        String countryname = searchFields.get('Country Name');
        String network = searchFields.get('Network');
        System.debug('Internet Country: '+country);

        System.debug('Internet searchFields: '+searchFields);
        String city = searchFields.get('ChinaCity');
        String contractterm = searchFields.get('Contract Term');
        String bandwidth = searchFields.get('Committed Bandwidth excluding direct China');
        String ratefilter= searchFields.get('rating filter');
        String trafficfilter= searchFields.get('Traffic filter');
        String calAstype = searchFields.get('Calculated AS Type');
        String acctype = searchFields.get('Account type');
        //System.Debug('doDynamicLookupSearch' + searchFields1);
         List <cspmb__Price_Item__c> data = new List<cspmb__Price_Item__c>();
        System.Debug('PriceItemQuery : '+ data);
        List <String> Products = new List<String>();
        List <String> ProductsGW = new List<String>();
           Products.add('IPT');
           Products.add('GID');
           Products.add('GID-PBS');
           Products.add('TWI');
           ProductsGW.add('IPT');
           ProductsGW.add('GID');
           ProductsGW.add('GID-PBS');
           
    if(trafficfilter == 'Custom'){
        //omit ratefilter and trafficfilter
        if(acctype == 'GW'){
        if(countryname == 'AUSTRALIA' && network == 'Australia (AS 1221)'){
        //query based on state
        data = [SELECT Name,                
                                            cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                           cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                           cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,State__c,DoSP_options__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            //AND City__c =: city 
                                            AND cspmb__Contract_Term__c =: contractterm
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            AND AS_Type__c=:calAstype
                                            ];
        
        }
        
        if(countryname == 'CHINA'){
        //query based on city
        data = [SELECT Name,                
                                            cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                           cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                           cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            AND AS_Type__c=:calAstype
                                            ];
        
        }
        else{
        
                                        data = [SELECT Name,                
                                            cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                           cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                           cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            //AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            AND AS_Type__c=:calAstype
                                            ];
                          }
                          }
           
        if(acctype != 'GW'){
        
        if(countryname == 'CHINA'){
        //query based on city
        data = [SELECT Name,                
                                            cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                           cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                           cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            AND AS_Type__c=:calAstype
                                            ];
        
        }
        else{
        
        
        
        
        
        data = [SELECT Name,                
                                            cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                           cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                           cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            //AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: ProductsGW
                                            AND AS_Type__c=:calAstype
                                            //AND AS_Type__c=:calAstype
                                            ];
        
        }
        }
        
    }
    
    else if(trafficfilter =='Standard'){
        //omit only ratefilter
        
        if(acctype == 'GW'){
        
        if(countryname == 'AUSTRALIA' && network == 'Australia (AS 1221)'){
        //query based on state
        System.debug('IN HERE......');
        data = [SELECT Name,
                                           cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                            cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                          cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            //AND City__c =: city 
                                            AND cspmb__Contract_Term__c =: contractterm
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            //AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            ];
        
        }
        if(countryname == 'CHINA'){
        //query based on city
        
        data = [SELECT Name,
                                           cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                            cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                          cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            //AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            ];
        
        }
        else{
        
        
        
                                        data = [SELECT Name,
                                           cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                            cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                          cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            //AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            //AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: Products
                                            ];
                }
                }
                
        if(acctype != 'GW') {
        
        
        
        if(countryname == 'CHINA'){
        //query based on city
        data = [SELECT Name,
                                           cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                            cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                          cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                            AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            //AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: ProductsGW
                                            ];
        
        
        }
        else{
        
         data = [SELECT Name,
                                           cspmb__One_Off_Charge__c,Committed_Bandwidth__c,
                                            cspmb__Recurring_Charge__c,Product__c,Contract_Term__c,Charge_per_Mbps__c,
                                          cspmb__Product_Definition_Name__c,DoSP_bandwidth__c,DoSP_options__c,State__c,CS_State__c,DoSP_Additional_Site__c,DoSP_Additional_User_Port__c,Dosp_Extra_Objects__c,Bandwidth_Product_Type_Name__c,Product_Sub_Type__c,Country_Name__c,Pop_Name__c,cspmb__Contract_Term__c,Port_Rating__c,Traffic_Dispersion__c,Provider__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE Country__c =: country 
                                           // AND City__c =: city 
                                            AND CS_Bandwidth_Product_Type__r.CS_Bandwidth__c =: bandwidth 
                                            //AND Supported_Rating__c=:ratefilter 
                                            //AND Traffic_Dispersion__c=:trafficfilter 
                                            AND cspmb__Product_Definition_Name__c =: ProductsGW
                                            ];
        
        
        }       
        }
    }  
        return data;
    }

    public override String getRequiredAttributes(){ 
        return '[ "Choose Country", "ChinaCity", "Committed Bandwidth excluding direct China", "rating filter", "Traffic filter", "Calculated AS Type","Account type","Country Name","Network","Contract Term"]';
    }
   
}