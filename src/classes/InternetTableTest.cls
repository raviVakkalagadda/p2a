@isTest(SeeAllData = false)
private class InternetTableTest{

  //  private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    private static Id[] excludeIds;
    private static List<CS_Country__c> countryList;
    private static List<IPL_Rate_Card_Bandwidth_Join__c> iplRateCardBandwidthJoinList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CloudSense_Bandwidth__c> bandwidthList;
    private static List<IPL_Rate_Card_Item__c> iplRateCardItemList;
    
    
    private static testMethod void initTestData(){
    
                countryList = new List<CS_Country__c>{
                new CS_Country__c(Name = 'AUSTRALIA'),
                new CS_Country__c(Name = 'CHINA')
            };
    
      insert countryList;
        
    list<CS_Country__c> countrys = [select id,Name from CS_Country__c where Name = 'AUSTRALIA'];
        system.assertEquals(countryList[0].Name , countrys[0].Name);
        System.assert(countryList!=null);
 
        
        bandwidthList = new List<CloudSense_Bandwidth__c>{
            new CloudSense_Bandwidth__c(Name = '64k'),
            new CloudSense_Bandwidth__c(Name = '128k')
        };
        
        insert bandwidthList;
        
  list<CloudSense_Bandwidth__c> band = [select id,Name from CloudSense_Bandwidth__c where Name = '64k'];
        system.assertEquals(bandwidthList[0].Name , band[0].Name);
        System.assert(bandwidthList!=null);

        
        iplRateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 1', Product_Type__c = 'IPL'),
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 2', Product_Type__c = 'IPL')
        };
        
        insert iplRateCardItemList;
        
  list<IPL_Rate_Card_Item__c> rate = [select id,Name from IPL_Rate_Card_Item__c where Name = 'Rate Card 1'];
        system.assertEquals(iplRateCardItemList[0].Name , rate[0].Name);
        System.assert(iplRateCardItemList!=null);

        
        iplRateCardBandwidthJoinList = new List<IPL_Rate_Card_Bandwidth_Join__c>{
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[0].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[0].Id),
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[1].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[1].Id)
        };
        
        insert iplRateCardBandwidthJoinList;
        system.assertEquals(iplRateCardBandwidthJoinList[0].Bandwidth__c , bandwidthList[0].Id);
        System.assert(iplRateCardBandwidthJoinList!=null);

        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[0].Id),
            new cspmb__Price_Item__c(Name = 'Price Item 2', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[1].Id)
        };
        
        insert priceItemList;
        
     list<cspmb__Price_Item__c> prod = [select id,Name from cspmb__Price_Item__c where Name = 'Price Item 1'];
        system.assertEquals(priceItemList[0].Name , prod[0].Name);
        System.assert(priceItemList!=null);

                
    }
    
  private static testMethod void doDynamicLookupSearchTest1() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            //searchFields.put('Choose Country', 'AUSTRALIA' );
            //searchFields.put('Country Name', 'CHINA');
            searchFields.put( 'Traffic filter', 'Custom');
            searchFields.put('Account type', 'GW');
            searchFields.put('Country Name', 'AUSTRALIA');
            searchFields.put('Network', 'Australia (AS 1221)');
            //searchFields.get('')
            

            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest2() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            //searchFields.put('Choose Country', 'AUSTRALIA' );
            searchFields.put('Country Name', 'CHINA');
            searchFields.put( 'Traffic filter', 'Custom');
            searchFields.put('Account type', 'GW');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest3() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
            searchFields.put('Country Name', 'CHINA');
            searchFields.put( 'Traffic filter', 'Custom');
            searchFields.put('Account type', 'MNC');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest4() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
           searchFields.put( 'Traffic filter', 'Custom');
            searchFields.put('Account type', 'MNC');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest5() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
           searchFields.put( 'Traffic filter', 'Standard');
           searchFields.put('Account type', 'GW');
           searchFields.put('Country Name', 'AUSTRALIA');
           searchFields.put('Network', 'Australia (AS 1221)');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest6() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
           searchFields.put( 'Traffic filter', 'Standard');
           searchFields.put('Account type', 'GW');
           searchFields.put('Country Name', 'CHINA');
           //searchFields.put('Network', 'Australia (AS 1221)');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest7() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
           searchFields.put( 'Traffic filter', 'Standard');
           searchFields.put('Account type', 'MNC');
           searchFields.put('Country Name', 'CHINA');
           //searchFields.put('Network', 'Australia (AS 1221)');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }
  
  private static testMethod void doDynamicLookupSearchTest8() {

        Test.startTest();
            Map<String, String> searchFields = new Map<String, String>();
            integer i1 = 1;
            integer i2 = 2;
            
           searchFields.put( 'Traffic filter', 'Standard');
           searchFields.put('Account type', 'MNC');
           //searchFields.put('Country Name', 'CHINA');
           //searchFields.put('Network', 'Australia (AS 1221)');
            InternetTable InternetTables= new InternetTable();
            Object[] data = InternetTables.doLookupSearch(searchFields, productDefinitionId,excludeIds,i1,i2);
            String reqAtts = InternetTables.getRequiredAttributes();
            system.assertEquals(true,data!=null);
        Test.stopTest();  
           
  }

}