/**
    This class Creates/updates Invoice object corresponding to Invoice Line Items
    and attaches Invoice Line Items as child to Invoice Object.

**/

Public class InvoiceCreateUpdateController{
 public static void createUpdateInvoice(List<Invoice_Line_Item__c> lst_ILI){
 
     List<String> lst_invoiceNuber = new List<String>();
     List<Invoice__c> lst_InvoiceToCreate = new List<Invoice__c>();
     Map<String,Invoice__c> map_INumberInvoiceToCreate = new Map<String,Invoice__c>();
     List<String> lst_BillProfileIntNumber = new List<String>();
     Map<String,BillProfile__c> map_CustAccNumberBillObj = new Map<String,BillProfile__c>();
 
 
    // This loop gets collection of invoice numbers and Customer Account numbers
    //This customer Account Number is nothing but BillProfile Intergration number in billprofile obj
    
     for(Invoice_Line_Item__c ILIObj : lst_ILI){
        lst_invoiceNuber.add(ILIObj.Invoice_Number__c);
        lst_BillProfileIntNumber.add(ILIObj.Customer_Account_Number__c);
        system.debug('Customer_Acc_no******'+ILIObj.Customer_Account_Number__c );
     
     }
    // querying for corresponding Invoice Objects and BillProfile Objects 
    
     List<Invoice__c> lst_InvoiceObj = [select id,CurrencyIsoCode,Invoice_Number__c,BillAmount__c from Invoice__c where Invoice_Number__c in :lst_invoiceNuber];
     //List<Invoice__c> lst_InvoiceObj = [select id,CurrencyIsoCode,Invoice_Number__c from Invoice__c where Invoice_Number__c in :lst_invoiceNuber];
     //List<BillProfile__c> lst_BillProfileObj = [select id,Bill_Profile_Integration_Number__c,Account__c from BillProfile__c where Bill_Profile_Integration_Number__c in :lst_BillProfileIntNumber];
 
    //Creating map of BillProfile integrtaion number as key and BillProfile object as value. 
    
     for(BillProfile__c billProfileObj : [select id,Bill_Profile_Integration_Number__c,Account__c from BillProfile__c where Bill_Profile_Integration_Number__c in :lst_BillProfileIntNumber]){
        map_CustAccNumberBillObj.put(billProfileObj.Bill_Profile_Integration_Number__c,billProfileObj);
         system.debug('BPNo.**********'+billProfileObj.Bill_Profile_Integration_Number__c);
     
     }
 
      for(Invoice_Line_Item__c ILTObj : lst_ILI){
          if(ILTObj.Invoice_Line_Amount__c != null){
                ILTObj.Billing_Amount__c = Double.valueOf(ILTObj.Invoice_Line_Amount__c);
            }
           
            
            ILTObj.ServiceId__c = ILTObj.Circuit_Id__c;
            ILTObj.Service_Description__c = ILTObj.Invoice_Line_Description__c;
            integer inLineNum1 = Integer.valueof(ILTObj.Invoice_Line_Number__c);
            String inLineNum = String.valueof(inLineNum1);
            System.debug('invoice line number is ...'+inLineNum1 +'  ' +inLineNum );
            ILTObj.Name = inLineNum;
            ILTObj.Invoice_Line_External_Id__c = ILTObj.Invoice_Number__c+ILTObj.Invoice_Line_Number__c;
            
            Invoice__c invoiceObj = new Invoice__c();
            //check if Invoice already created or not
              if(lst_InvoiceObj != null && lst_InvoiceObj.size()>0){
                 for(Invoice__c invoiceObj1 : lst_InvoiceObj ){
                    if(invoiceObj1.Invoice_Number__c == ILTObj.Invoice_Number__c){
                        ILTObj.Invoice__c = invoiceObj1.id;
                                            
                    }
                 
                 }
              
              }else{
                    if(map_CustAccNumberBillObj.keySet().contains(ILTObj.Customer_Account_Number__c)){
                    
                          invoiceObj.Invoice_Number__c = ILTObj.Invoice_Number__c;
                          invoiceObj.Invoice_Date__c = ILTObj.Invoice_Date__c;
                          invoiceObj.Billing_Account__c = map_CustAccNumberBillObj.get(ILTObj.Customer_Account_Number__c).Account__c;
                          invoiceObj.Bill_Profile_Name__c= map_CustAccNumberBillObj.get(ILTObj.Customer_Account_Number__c).Id;
                          invoiceObj.name = ILTObj.Invoice_Number__c;
                          invoiceObj.Invoice_Issue_Date__c = ILTObj.Invoice_Date__c;
                          invoiceObj.Currency__c = ILTObj.Currency__c;
                          invoiceObj.Customer_Account_Number__c = ILTObj.Customer_Account_Number__c;
                          
                          
                          if(!map_INumberInvoiceToCreate.keySet().contains(ILTObj.Invoice_Number__c)){
                            map_INumberInvoiceToCreate.put(ILTObj.Invoice_Number__c,invoiceObj);
                         }
                    }
                  
              }
       }
        
        if(map_INumberInvoiceToCreate.values().size()>0){
            lst_InvoiceToCreate = map_INumberInvoiceToCreate.values();
            insert lst_InvoiceToCreate;
        }
    
    // Here Setting Parent and child relationship
      for(Invoice__c invoiceObj : lst_InvoiceToCreate){
        for(Invoice_Line_Item__c ILIObj : lst_ILI){
            if(invoiceObj.Invoice_Number__c == ILIObj.Invoice_Number__c){
                ILIObj.Invoice__c = invoiceObj.Id;
                
            }
        }
      }
  
 
    }
}