/**        @author - Accenture        
           @date - 21-JUNE-2012     
           @version - 1.0   
           @description - This class contains the Business Logic for the Lead triggers.     
*/
public class LeadBusinessClass{
    
/**        @author - Accenture        
           @date - 21-JUNE-2012     
           @version - 1.0   
           @description - This method is used for updating the Lead Status when the Marketin assigns lead to Sales.     
*/
    public static void updateLeadStatus(){
// List of Leads

        List<Lead> leadList = new List<Lead> ();
        Map<ID,ID> leadUserMapping = new Map<ID,ID>();

        for (Lead lead: (List<Lead>)trigger.new){
            Lead objLeadold = (Lead)trigger.oldMap.get(lead.Id);
            if (objLeadold.OwnerId!= lead.OwnerId){
                leadUserMapping.put(lead.Id,lead.OwnerId);  
            }
        }
        //List<Lead> objLeadList = [SELECT Status, OwnerId FROM Lead WHERE Id = :leadUserMapping.Keyset()];
        //List<User> usrList  = [SELECT Id, Name, ProfileId FROM User WHERE Id = :leadUserMapping.values()];
        Set<Id> usrSet = new Set<Id>();
        for (User usr : [SELECT Id, Name, ProfileId FROM User WHERE Id = :leadUserMapping.values()]){
            
            if(usr.ProfileId== Global_Constant_Data__c.getvalues('Sales Manager').value__c || usr.ProfileId== Global_Constant_Data__c.getvalues('Account Manager').value__c || usr.ProfileId== Global_Constant_Data__c.getvalues('Sales Admin').value__c){
                usrSet.add(usr.Id);
            }
        }
        for(Lead objLead : [SELECT Status, OwnerId FROM Lead WHERE Id = :leadUserMapping.Keyset()]){
            if (!usrSet.isEmpty() && objLead.Status <>'Sales Accepted Lead (SAL)'){
            if(usrSet.contains(objLead.OwnerId )){
                objLead.Status = 'Marketing Qualified Lead (MQL)';
                leadList.add(objLead);
            }
        }
    
 // Bulk update

        /*if (leadList.size() > 0) 

         update leadList;*/
} 
    if (leadList.size() > 0) {
        update leadList;
    }
    }
    
}