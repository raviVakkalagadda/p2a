Public Class LinkAndAssociateServiceWithClonedPC{

//Method 1

public static List<csord__service__c> updateLinkedServiceId(List<csord__service__c> newServices){
//Disable Triggers
//TriggerFlags.NoProductConfigurationTriggers=true;
//TriggerFlags.NoProductBasketTriggers=true;
//TriggerFlags.NoAttributeTriggers=true; 
try{
//variable declaration  
List<csord__service__c>ChildServList=new List<csord__service__c>();
//List<csord__service__c>ParentServList=new List<csord__service__c>();
//List<cscfga__Attribute__c> attributeList=new List<cscfga__Attribute__c>();
Map<Id,csord__service__c>ChildPCToServMap=new Map<Id,csord__service__c>();
Map<Id,csord__service__c>ParentPCToServMap=new Map<Id,csord__service__c>();
Map<Id,cscfga__Attribute__c> PcToattributeMap=new Map<Id,cscfga__Attribute__c>();
Map<Id,Id>ChildPcToParentPcMap=new Map<Id,Id>();
set<Id>parentConfigId=new Set<Id>();
set<Id>AllConfigId=new Set<Id>();
   
/*ChildServList=[select id,Name,csordtelcoa__Product_Configuration__c,csordtelcoa__Product_Configuration__r.Cloning_Parent__c,
csordtelcoa__Product_Configuration__r.Order_Type__c from csord__service__c where ID IN:newServices AND 
csordtelcoa__Product_Configuration__r.Order_Type__c IN ('New Provide')];*/

for(csord__service__c s:[select id,Name,csordtelcoa__Product_Configuration__c,csordtelcoa__Product_Configuration__r.Cloning_Parent__c,csordtelcoa__Product_Configuration__r.Order_Type__c from csord__service__c where ID IN:newServices AND csordtelcoa__Product_Configuration__r.Order_Type__c IN ('New Provide')]){
    if(s.csordtelcoa__Product_Configuration__c!=null 
       && s.csordtelcoa__Product_Configuration__r.Cloning_Parent__c!=null
       && s.csordtelcoa__Product_Configuration__c!=s.csordtelcoa__Product_Configuration__r.Cloning_Parent__c
       && !parentConfigId.contains(s.csordtelcoa__Product_Configuration__r.Cloning_Parent__c)){
        parentConfigId.add(s.csordtelcoa__Product_Configuration__r.Cloning_Parent__c);
        ChildPcToParentPcMap.put(s.csordtelcoa__Product_Configuration__c,s.csordtelcoa__Product_Configuration__r.Cloning_Parent__c);
        ChildPCToServMap.put(s.csordtelcoa__Product_Configuration__c,s);
        if(!AllConfigId.contains(s.csordtelcoa__Product_Configuration__c)){
            AllConfigId.add(s.csordtelcoa__Product_Configuration__c);
        }
    }
    
}
if(!parentConfigId.isEmpty()){
/*ParentServList=[select id,Name,csordtelcoa__Product_Configuration__c,
csordtelcoa__Product_Configuration__r.Order_Type__c from csord__service__c where csordtelcoa__Product_Configuration__c IN:parentConfigId AND 
csordtelcoa__Product_Configuration__r.Order_Type__c IN ('Parallel Upgrade','Parallel Downgrade')];
system.debug('Parallel Upgrade pragya' +ParentServList);*/
for(csord__service__c s:[select id,Name,csordtelcoa__Product_Configuration__c,csordtelcoa__Product_Configuration__r.Order_Type__c from csord__service__c where csordtelcoa__Product_Configuration__c IN:parentConfigId AND csordtelcoa__Product_Configuration__r.Order_Type__c IN ('Parallel Upgrade','Parallel Downgrade')]){
    if(s.csordtelcoa__Product_Configuration__c!=null){
        ParentPCToServMap.put(s.csordtelcoa__Product_Configuration__c,s);
        if(!AllConfigId.contains(s.csordtelcoa__Product_Configuration__c)){
            AllConfigId.add(s.csordtelcoa__Product_Configuration__c);
        }
    } 
}

//Assign values
       /*attributeList =[SELECT Id, Name, cscfga__Product_Configuration__c,cscfga__Value__c,cscfga__Display_Value__c
            FROM cscfga__Attribute__c
            WHERE cscfga__Product_Configuration__c IN:AllConfigId AND
            Name in('Linked_Service_ID__c')];//to be removed*/
        
        for(cscfga__Attribute__c atr:[SELECT Id, Name, cscfga__Product_Configuration__c,cscfga__Value__c,cscfga__Display_Value__c FROM cscfga__Attribute__c WHERE cscfga__Product_Configuration__c IN:AllConfigId AND Name in('Linked_Service_ID__c')]){
            if(atr.Name=='Linked_Service_ID__c'){
            PcToattributeMap.put(atr.cscfga__Product_Configuration__c,atr);
            }            
        }
      
for(Id childPcId:ChildPcToParentPcMap.keyset()){
if(ChildPCToServMap.containsKey(childPcId)
   &&ChildPcToParentPcMap.containsKey(childPcId)
   && ParentPCToServMap.containsKey(ChildPcToParentPcMap.get(childPcId))){
    if(PcToattributeMap.containsKey(childPcId)&& PcToattributeMap.containsKey(ChildPcToParentPcMap.get(childPcId))){
        PcToattributeMap.get(childPcId).cscfga__Value__c=ParentPCToServMap.get(ChildPcToParentPcMap.get(childPcId)).id;
        PcToattributeMap.get(childPcId).cscfga__Display_Value__c=ParentPCToServMap.get(ChildPcToParentPcMap.get(childPcId)).Name;
        PcToattributeMap.get(ChildPcToParentPcMap.get(childPcId)).cscfga__Value__c=ChildPCToServMap.get(childPcId).Id;
        PcToattributeMap.get(ChildPcToParentPcMap.get(childPcId)).cscfga__Display_Value__c=ChildPCToServMap.get(childPcId).Name;
    }
}   
    
}

System.debug('PcToattributeMap'+PcToattributeMap);
System.debug('ParentPCToServMap'+ParentPCToServMap);
System.debug('ChildPCToServMap'+ChildPCToServMap);
if(PcToattributeMap.size()>0){
update PcToattributeMap.values();
}
system.debug('PcToattributeMap.values'+PcToattributeMap.values());
}

   
}catch(Exception e){
    ErrorHandlerException.ExecutingClassName='LinkAndAssociateServiceWithClonedPC :updateLinkedServiceId';   
    ErrorHandlerException.objectList=newServices;
    ErrorHandlerException.sendException(e); 
    System.debug(e);
}
return newServices;
//Enable Triggers
/*TriggerFlags.NoProductConfigurationTriggers=false;
TriggerFlags.NoProductBasketTriggers=false;
TriggerFlags.NoAttributeTriggers=false;     
*/
}

/*
*Method : 2
*CR:019
*Developer:Ritesh
*Description:The below method will link a child with master at service level
*will be called in after insert of service trigger handler
*/

public static List<csord__Service__c> updateMasterChildLinkageOnServicesCreation(List<csord__Service__c> newServices){

try{
Map<Id,csord__Service__c>currentServMap=new Map<Id,csord__Service__c>();
set<Id>solutionSet=new Set<Id>();   
for(csord__Service__c serv:newServices){
     if(serv.solutions__c!=null){
         solutionSet.add(serv.solutions__c);
     }
     currentServMap.put(serv.id,serv);   
     
}


if(!solutionSet.isEmpty()){          
List<csord__Service__c>childServList=[select Name,csordtelcoa__Product_Configuration__c,Primary_Port_Service__c,csordtelcoa__Product_Configuration__r.Parent_Configuration_For_Sequencing__c
 from csord__Service__c where solutions__c IN:solutionSet AND (Product_Configuration_Type__c!='Terminate' OR Cease_Service_Flag__c!=true)];

 Map<Id,Id>PcToServiceMap=new Map<Id,Id>();
 List<csord__Service__c>updateList=new List<csord__Service__c>();
 for(csord__Service__c serv:childServList){
    if(serv.csordtelcoa__Product_Configuration__c!=null){
        PcToServiceMap.put(serv.csordtelcoa__Product_Configuration__c,serv.id);
    }     
 }
 for(csord__Service__c serv:childServList){
    if(serv.csordtelcoa__Product_Configuration__c!=null 
    && serv.csordtelcoa__Product_Configuration__r.Parent_Configuration_For_Sequencing__c!=null
    && PcToServiceMap.containsKey(serv.csordtelcoa__Product_Configuration__r.Parent_Configuration_For_Sequencing__c)){
     csord__Service__c s=new csord__Service__c ();
     s.id=serv.id;
     s.Primary_Port_Service__c=PcToServiceMap.get(serv.csordtelcoa__Product_Configuration__r.Parent_Configuration_For_Sequencing__c);
       updateList.add(s);     
    
    } 
 }
 
 

 if(updateList.size()>0)upsert updateList;
 System.debug('updateList'+updateList);
 
} 
return newServices;
//System.assertEquals(true,false);
}catch(Exception e){
    ErrorHandlerException.ExecutingClassName='LinkAndAssociateServiceWithClonedPC :updateMasterChildLinkageOnServicesCreation';   
    ErrorHandlerException.objectList=newServices;
    ErrorHandlerException.sendException(e); 
    System.debug(e);
    return newServices;}

}


//Method 3
//will be called when opportunity stage changes from closed sales to closed won

public static void updateLinkedServiceIdOnEnrichment(List<cscfga__Product_Configuration__c> pcList,Map<Id, cscfga__Product_Configuration__c>oldProductConfigsMap){
try{

    Boolean runflag=false;
    for(cscfga__Product_Configuration__c pc:pcList){
        if(pc.OpportunityStage__c!='Closed Won' && pc.OpportunityStage__c!=oldProductConfigsMap.get(pc.id).OpportunityStage__c){
            runflag=true;
            break;
        }
    }
if(runFlag){    
Map<Id,cscfga__Attribute__c>PcToAtrMap=new Map<Id,cscfga__Attribute__c>();
set<Id>childPcSet=new Set<Id>();
Map<Id,cscfga__Product_Configuration__c>childPcToParentServMap=new Map<Id,cscfga__Product_Configuration__c>();
for(cscfga__Attribute__c atr:[select id,name,cscfga__Value__c,cscfga__Display_Value__c,cscfga__Product_Configuration__c  from cscfga__Attribute__c where cscfga__Product_Configuration__c IN:pcList AND Name IN('Linked_Service_ID__c')]){
    if(atr.name=='Linked_Service_ID__c' && atr.cscfga__Product_Configuration__c!=null){
        PcToAtrMap.put(atr.cscfga__Product_Configuration__c,atr);
        if(!childPcSet.contains(atr.cscfga__Product_Configuration__c))childPcSet.add(atr.cscfga__Product_Configuration__c);
    }
}
if(!childPcSet.isEmpty()){
    set<Id>ParentPcSet=new Set<Id>();
    Map<Id,Id>childtoParentPcMap=new Map<Id,Id>();

    for(cscfga__Product_Configuration__c pc:[select Id,Order_Type__c ,Cloning_Parent__c from cscfga__Product_Configuration__c where Id IN:childPcSet AND Order_Type__c IN ('New Provide')]){
        if(pc.Cloning_Parent__c!=null && !ParentPcSet.contains(pc.Cloning_Parent__c)){
            ParentPcSet.add(pc.Cloning_Parent__c);
            childtoParentPcMap.put((Id)pc.Cloning_Parent__c,pc.id);
        }
    }
    if(!ParentPcSet.isEmpty()){

    for(cscfga__Product_Configuration__c pc:[select Id,Order_Type__c,csordtelcoa__Replaced_Service__c,csordtelcoa__Replaced_Service__r.name from cscfga__Product_Configuration__c where Id IN:ParentPcSet AND csordtelcoa__Replaced_Service__c!=null AND Order_Type__c IN('Parallel Upgrade','Parallel Downgrade')]){
        if(childtoParentPcMap.containsKey(pc.id)){
            childPcToParentServMap.put(childtoParentPcMap.get(pc.id),pc);
        }
    }
     for(id pcid:PcToAtrMap.keyset()){
         if(childPcToParentServMap.containsKey(pcid)){
             PcToAtrMap.get(pcid).cscfga__Value__c=childPcToParentServMap.get(pcid).csordtelcoa__Replaced_Service__c;
             PcToAtrMap.get(pcid).cscfga__Display_Value__c=childPcToParentServMap.get(pcid).csordtelcoa__Replaced_Service__r.name;
         }
     }  
     if(PcToAtrMap.size()>0){update PcToAtrMap.values();}
    }
}
}
}catch(Exception e){
    ErrorHandlerException.ExecutingClassName='LinkAndAssociateServiceWithClonedPC :updateLinkedServiceIdOnEnrichment';   
    ErrorHandlerException.objectList=pcList;
    ErrorHandlerException.sendException(e); 
    System.debug(e);}
}


}
