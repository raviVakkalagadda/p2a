@isTest
private class LinkAndAssociateServiceWithClonedPCtest{
 public static testmethod void LinkAndAssociateServiceWithClonedPCtest1(){ 

       P2A_TestFactoryCls.sampleTestData();

        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<solutions__c> solution2 = P2A_TestFactoryCls.getsolutions(1,acclist);
        List<solutions__c> sols = P2A_TestFactoryCls.getsolutions(1,acclist);
        List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
        List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
        List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(4, prodbasktlist, ProductDeflist, Pbundle, Offerlists);
        
        List<cscfga__Product_Configuration__c> proconfigs1= P2A_TestFactoryCls.getProductonfig(4, prodbasktlist, ProductDeflist, Pbundle, Offerlists);
        proconfigs1[0].Parent_Configuration_For_Sequencing__c = proconfigs[0].id;
        proconfigs1[0].Cloning_Parent__c = 'Test';
        update proconfigs1[0];
        
        System.assert(proconfigs1!=null);
        
        List<cscfga__Attribute_Definition__c> AttDef = P2A_TestFactoryCls.getAttributesdef(1, proconfigs, ProductDeflist, ConScr, ScrSec);
        
        List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(1, proconfigs, AttDef);
        Attrblist[0].Name = 'Linked_Service_ID__c';
        Attrblist[0].cscfga__Value__c = '10';
        Attrblist[0].cscfga__Display_Value__c = '20';
        upsert Attrblist ;
        
         System.assert(Attrblist!=null);
        
        Map<Id,cscfga__Attribute__c> PcToattributeMap=new Map<Id,cscfga__Attribute__c>();
        for(cscfga__Attribute__c attbute:Attrblist){
          PcToattributeMap.put(Attrblist[0].id,Attrblist[0]);
        }
 
 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        Orders[0].Order_Type__c = 'Parallel Upgrade';
        update Orders[0];
        List<csord__Service__c> servList1 = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        servList[0].csordtelcoa__Product_Configuration__c = proconfigs[0].id;
        servList[0].Updated__c = true;
        servList[0].csordtelcoa__Replaced_Service__c = servList1[0].id;
        servList[0].Cease_Service_Flag__c = true;
        servList[0].Order_Type_Final__c = 'Terminate';
        servList[0].solutions__c = solution2[0].id;
        servList[0].Primary_Port_Service__c = servList1[0].id;
        update servList;
         System.assert(servList!=null);
          set<Id> parentConfigId = new Set<Id>();
         for(csord__service__c csord: servList ){
            parentConfigId.add(servList[0].id);
         }
        
         proconfigs1[0].csordtelcoa__Replaced_Service__c = servList[0].id;
         proconfigs1[0].Order_Type_Text__c='';
         update proconfigs1[0];
         
       
        
        Map<Id,csord__service__c> ChildPCToServMap =new Map<Id,csord__service__c>();
        for(csord__service__c sevice:servList){
          ChildPCToServMap.put(servList[0].id,servList[0]);
        }
        
        test.starttest();      
            LinkAndAssociateServiceWithClonedPC link = new LinkAndAssociateServiceWithClonedPC();
            //LinkAndAssociateServiceWithClonedPC.updateLinkedServiceId(servList);
            //LinkAndAssociateServiceWithClonedPC.updateLinkedServiceIdOnEnrichment(proconfigs);
            LinkAndAssociateServiceWithClonedPC.updateMasterChildLinkageOnServicesCreation(servList);
          test.stoptest();   
         
 }
     @istest
     public static void unittest1() {
        P2A_TestFactoryCls.sampleTestData();
        
        List<Account> acclist = P2A_TestFactoryCls.getAccounts(1);
        List<solutions__c> solution2 = P2A_TestFactoryCls.getsolutions(1,acclist);
        List<Opportunity> opplist = P2A_TestFactoryCls.getOpportunitys(1,acclist);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList1 = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Basket__c> prodbasktlist = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        prodbasktlist[0].csordtelcoa__Change_Type__c = 'Upgrade';
        update prodbasktlist[0];
        system.assertEquals(true,prodbasktlist!=null); 
        List<cscfga__Product_Basket__c> prodbasktlist1 = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        prodbasktlist1[0].csordtelcoa__Change_Type__c = '   Parallel Upgrade';
        update prodbasktlist1[0];
        system.assertEquals(true,prodbasktlist1!=null);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Configuration_Screen__c> ConScr = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Screen_Section__c> ScrSec = P2A_TestFactoryCls.getScreenSec(1, ConScr);
    
  
 
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(4, prodbasktlist, ProductDeflist, Pbundle, Offerlists);
       
        List<cscfga__Product_Configuration__c> proconfigs1= P2A_TestFactoryCls.getProductonfig(4, prodbasktlist, ProductDeflist, Pbundle, Offerlists);
        proconfigs1[0].Parent_Configuration_For_Sequencing__c = proconfigs[0].id;
        proconfigs1[0].Cloning_Parent__c = proconfigs[0].id;
        proconfigs1[0].csordtelcoa__Replaced_Service__c = null;
        proconfigs1[0].csordtelcoa__Replaced_Subscription__c = null;
        update proconfigs1[0];
        system.assertEquals(true,proconfigs1!=null);
        List<cscfga__Product_Configuration__c> proconfigs2= P2A_TestFactoryCls.getProductonfig(4, prodbasktlist1, ProductDeflist, Pbundle, Offerlists);
        proconfigs2[0].Parent_Configuration_For_Sequencing__c = proconfigs[0].id;
        proconfigs2[0].Cloning_Parent__c = proconfigs[0].id;
        proconfigs2[0].csordtelcoa__Replaced_Service__c = null;
        proconfigs2[0].csordtelcoa__Replaced_Subscription__c = null;
        update proconfigs2[0];
        system.assertEquals(true,proconfigs2!=null);
        
        List<cscfga__Attribute_Definition__c> AttDef = P2A_TestFactoryCls.getAttributesdef(1, proconfigs, ProductDeflist, ConScr, ScrSec);
        List<cscfga__Attribute__c> Attrblist = P2A_TestFactoryCls.getAttributes(1, proconfigs, AttDef);
        List<csord__Service__c> Servicelist = new List<csord__Service__c>();
        
        csord__Service__c ser = new csord__Service__c();
        ser.Name = 'Test Service'; 
        ser.csord__Identification__c = 'Test-Catlyne-4238362';
        ser.csord__Order_Request__c = OrdReqList[0].id;
        ser.csord__Subscription__c = SUBList[0].id;
        ser.Billing_Commencement_Date__c = System.Today();
        ser.Stop_Billing_Date__c = System.Today();
        ser.RAG_Status_Red__c = false ; 
        ser.RAG_Reason_Code__c = ''; 
        ser.csordtelcoa__Product_Basket__c = prodbasktlist[0].id;  
        ser.Product_Id__c = 'IPVPN';     
        ser.Path_Instance_ID__c = 'test';
        ser.Cease_Service_Flag__c = true;
        ser.Order_Type_Final__c = 'Terminate';
        ser.Primary_Port_Service__c = servList1[0].id;
        ser.solutions__c = solution2[0].id;
        ser.csordtelcoa__Product_Configuration__c = proconfigs1[0].id;                           
        Servicelist.add(ser);
        
        csord__service__c ser1 = new csord__service__c();
        ser1.Name = 'Test ser1vice'; 
        ser1.csord__Identification__c = 'Test-Catlyne-4238362';
        ser1.csord__Order_Request__c = OrdReqList[0].id;
        ser1.csord__Subscription__c = SUBList[0].id;
        ser1.Billing_Commencement_Date__c = System.Today();
        ser1.Stop_Billing_Date__c = System.Today();
        ser1.RAG_Status_Red__c = false ; 
        ser1.RAG_Reason_Code__c = ''; 
        ser1.csordtelcoa__Product_Basket__c = prodbasktlist1[0].id;  
        ser1.Product_Id__c = 'IPVPN';     
        ser1.Path_Instance_ID__c = 'test';
        ser1.Cease_service_Flag__c = true;
        ser1.Order_Type_Final__c = 'Terminate';
        ser1.Primary_Port_service__c = servList1[0].id;
        ser1.solutions__c = solution2[0].id;
        ser1.csordtelcoa__Product_Configuration__c = proconfigs2[0].id;                           
        servicelist.add(ser1);

        insert Servicelist;
        system.assertEquals(true,Servicelist!=null);
        test.starttest(); 
        LinkAndAssociateServiceWithClonedPC.updateLinkedServiceId(Servicelist);
        
        test.stoptest();     
 }
      @isTest static void testExceptions(){
         LinkAndAssociateServiceWithClonedPC alls=new LinkAndAssociateServiceWithClonedPC();
        
         try{LinkAndAssociateServiceWithClonedPC.updateLinkedServiceId(null);}catch(Exception e){}
         try{LinkAndAssociateServiceWithClonedPC.updateMasterChildLinkageOnServicesCreation(null);}catch(Exception e){}
         try{LinkAndAssociateServiceWithClonedPC.updateLinkedServiceIdOnEnrichment(null,null);}catch(Exception e){}
          system.assertEquals(true,alls!=null);    
         
         
     }
 
 }