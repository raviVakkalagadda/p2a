@isTest
private class LocalLoopAvailabilityCheckTest {

    private static testMethod void test() {
        Exception ee = null;
        
        try{
            String city = 'Zagreb';
            String cityId = '123456789';
            String country = 'Croatia';
            String pop = 'POP 1';
            
            Test.startTest();
            
            String result = LocalLoopAvailabilityCheck.doWork(city, cityId, country, pop);
            System.assert(result.contains(city), 'Result does not contain ' + city);
                 
           Test.stopTest();
            
        } catch(Exception e){
            ee = e;
        } finally {
           // Test.stopTest();
            if(ee != null){
                throw ee;
            }
        } 
    }

}