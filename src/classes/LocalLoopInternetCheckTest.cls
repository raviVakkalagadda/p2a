@isTest(SeeAllData = false)
private class LocalLoopInternetCheckTest {

    private static List<Local_Access_Rate_Card__c> rateCardList;

    private static void initTestData(){
        rateCardList = new List<Local_Access_Rate_Card__c>{
            new Local_Access_Rate_Card__c(Name = 'Country 1', City__c = 'City 1', Building__c = 'Building 1',  Street__c = 'Street 1', District__c = 'District 1'),
            new Local_Access_Rate_Card__c(Name = 'Country 2', City__c = 'City 2', Building__c = 'Building 2',  Street__c = 'Street 2', District__c = 'District 2'),
            new Local_Access_Rate_Card__c(Name = 'Country 3', City__c = 'City 3', Building__c = 'Building 3',  Street__c = 'Street 3', District__c = 'District 3'),
            new Local_Access_Rate_Card__c(Name = 'Country 4', City__c = 'City 4', Building__c = 'Building 4',  Street__c = 'Street 4', District__c = 'District 4'),
            new Local_Access_Rate_Card__c(Name = 'Country 5', City__c = 'City 5', Building__c = 'Building 5',  Street__c = 'Street 5', District__c = 'District 5'),
            new Local_Access_Rate_Card__c(Name = 'Country 6', City__c = 'City 6', Building__c = 'Building 6',  Street__c = 'Street 6', District__c = 'District 6'),
            new Local_Access_Rate_Card__c(Name = 'Country 7', City__c = 'City 7', Building__c = 'Building 7',  Street__c = 'Street 7', District__c = 'District 7')
        };
        
        insert rateCardList;  
        System.assertEquals('Country 1',rateCardList[0].Name );   
    }

  private static testMethod void test() {
        Exception ee = null;
        
        try{
            String city = 'City 1';
            String country = 'Country 1';
            String building = 'Building 1';
            String street = 'Street 1';
            String district = 'District 1'; 
            
            Test.startTest();
            
            initTestData();
            
            String result = LocalLoopInternetCheck.getAddresses(city, country, building, street, district);
            System.assert(result.contains(city), 'Result does not contain ' + city);

            
        } catch(Exception e){
            ee = e;
        } finally {
            Test.stopTest();
            if(ee != null){
                throw ee;
            }
        } 
  } 

}