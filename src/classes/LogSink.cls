global class LogSink implements cscfga.Log.Sink {
		global void logMessage(LoggingLevel level, String message) {
			System.debug(LoggingLevel.ERROR, message);
		}

}