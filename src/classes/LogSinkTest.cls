@isTest
private class LogSinkTest {
    
    static testMethod void test() {

       System.LoggingLevel level = LoggingLevel.ERROR;
       Test.startTest();
            LogSink obj = new LogSink();
            obj.logMessage(level,'test');
        Test.stopTest();
        system.assertEquals(true,obj!=null);
    
    }
}