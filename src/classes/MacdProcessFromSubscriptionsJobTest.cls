@isTest(SeeAllData = false)
private class  MacdProcessFromSubscriptionsJobTest{

public String macdType {get; set;}
public string changeType {get;set;}
public String title {get;set;}
public String RecordType {get; set;}

    private static cscfga__Product_Basket__c basket = null;
    private static cscfga__Product_Configuration__c config = null;
    private static csord__Service__c service = null;
    private static csord__Subscription__c subscription = null;
    
    
     private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
    
    private static void tryToCreateTestData() {
    
      P2A_TestFactoryCls.sampleTestData();
      
        Exception ee = null;
        try {
            disableAll(UserInfo.getUserId());
            //disableTelcoAll(UserInfo.getUserId());
            createTestData();
        } catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='MacdProcessFromSubscriptionsJobTest :sampleTestData';         
            ErrorHandlerException.sendException(e); 

            ee = e;
        } finally {
            enableAll(UserInfo.getUserId());
            //enableTelcoAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
 private static void createTestData() {
       
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        system.assertEquals(true,productCategory!=null);
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        system.assertEquals(true,prodDefintion!=null); 
        basket = new cscfga__Product_Basket__c();
        insert basket;
        system.assertEquals(true,basket!=null); 
        config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id);  
        insert config;
         system.assertEquals(true,config!=null); 
        csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        system.assertEquals(true,orderRequest!=null); 
        subscription = new csord__Subscription__c(Name = 'Test subscription'
            , csord__Identification__c = 'dummy'
            , csord__Order_Request__c = orderRequest.Id);
        insert subscription; 
         system.assertEquals(true,subscription!=null); 
        service = new csord__Service__c(Name = 'Test service'
            , csordtelcoa__Product_Configuration__c = config.Id
            , csordtelcoa__Product_Basket__c = basket.Id
            , csord__Identification__c = 'Service_' + config.Id
            , csord__Subscription__c = subscription.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert service;
        system.assertEquals(true,service!=null); 
    }
    
     
    private static testMethod void testMacBasketFromSubscription() {
    integer userid = 0;
      P2A_TestFactoryCls.sampleTestData();
      try{
        tryToCreateTestData();
        system.assertEquals(true,userid!=null); 
       // MacdProcessFromSubscriptionsJob ctr = new MacdProcessFromSubscriptionsJob();
        Test.startTest();
        //ctr.dateString = system.today().format();
         //ctr.macdType ='MACDOppty';
         //ctr.changeType = 'Renewal';
         //ctr.title ='test';
         //ctr.RecordType ='test';
        Test.stopTest();
        }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='MacdProcessFromSubscriptionsJobTest :testMacBasketFromSubscription';         
            ErrorHandlerException.sendException(e); 
        }

   
   }
}