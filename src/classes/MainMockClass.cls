@isTest
global class MainMockClass{    
    global class SuspendOrderResponse_elementGenerator implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.SuspendOrderResponse_element respElement = new wwwTibcoComAffOrderservice.SuspendOrderResponse_element();
           respElement.message= 'Mock response';
           response.put('response_x', respElement);   
       }
    }
    global class SyncSubmitOrderResponse_elementGenerator implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
            wwwTibcoComAffOrderservice.SyncSubmitOrderResponse_element respElement= new wwwTibcoComAffOrderservice.SyncSubmitOrderResponse_element();
           respElement.status= 'Mock response';
           response.put('response_x', respElement); 
       }
    }
    
    global class SyncSubmitOrderResponse_GetOrdersResponse_element  implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.GetOrdersResponse_element respElement = new wwwTibcoComAffOrderservice.GetOrdersResponse_element();
           respElement.ExternalBusinessTransactionID = 'Mock response';
           response.put('response_x', respElement); 
       }
    }
    
    global class SyncSubmitOrderResponse_PerformBulkOrderAction  implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_element PerformBulkOrderAction = new wwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_element(); 
           PerformBulkOrderAction.message = 'Mock response';
           response.put('response_x', PerformBulkOrderAction); 

       }
    }
    
    global class SyncSubmitOrderResponse_GetOrderDetails implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrder.orderType GetOrderDetails = new wwwTibcoComAffOrder.orderType();
           GetOrderDetails.orderRef = 'Mock response';
           response.put('response_x', GetOrderDetails); 
       }
    }
    
    global class SyncSubmitOrderResponse_SubmitOrder implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           wwwTibcoComAffOrderservice.SubmitOrderResponse_element SubmitOrder = new wwwTibcoComAffOrderservice.SubmitOrderResponse_element();
           
           SubmitOrder.orderId= 'Mock response';
           response.put('response_x', SubmitOrder); 

       }
    }
    
    global class SyncSubmitOrderResponse_CancelOrderResponse_element  implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.CancelOrderResponse_element CancelOrder = new wwwTibcoComAffOrderservice.CancelOrderResponse_element();
           CancelOrder.message = 'Mock response';
           response.put('response_x', CancelOrder); 

       }
    }
   
    global class SyncSubmitOrderResponse_GetOrderExecutionPlan implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffPlan.planType GetOrderExecutionPlan = new wwwTibcoComAffPlan.planType();
           GetOrderExecutionPlan.status = 'Mock response';
           response.put('response_x', GetOrderExecutionPlan); 

       }
    }
    
    global class SyncSubmitOrderResponse_WithdrawOrderResponse_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.WithdrawOrderResponse_element WithdrawOrder = new wwwTibcoComAffOrderservice.WithdrawOrderResponse_element();
           WithdrawOrder.message = 'Mock response';
           response.put('response_x', WithdrawOrder); 
       }
    }
    
    global class SyncSubmitOrderResponse_GetEnrichedExecutionPlanResponse_element  implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_element GetEnrichedExecutionPlan = new wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_element();
           GetEnrichedExecutionPlan.ExternalBusinessTransactionID = 'Mock response';
           response.put('response_x', GetEnrichedExecutionPlan); 
       }
    }
    
    global class SyncSubmitOrderResponse_ActivateOrderResponse_element   implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.ActivateOrderResponse_element ActivateOrder = new wwwTibcoComAffOrderservice.ActivateOrderResponse_element();
           ActivateOrder.message= 'Mock response';
           response.put('response_x', ActivateOrder); 
       }
    }
    
    global class SuspendOrderResponse_BillingActivateResponse implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element BillingActivateResponse = new TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
           BillingActivateResponse.Ack_xc = 'Mock response';
           response.put('response_x', BillingActivateResponse);   
       }
    }
    
    global class wwwTibcoComSchemasXsdgenerationBill implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element TibcoXsdBillingActivateResponse = new wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
           TibcoXsdBillingActivateResponse.Ack_xc = 'Mock response';
           response.put('response_x', TibcoXsdBillingActivateResponse);   
       }
    }
    
    
    global class TigComSchemaIdGenratorrquest implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           //Map<String, TigComSchemasIdGeneratorresponse.TSID_element> response_map_x = new Map<String, TigComSchemasIdGeneratorresponse.TSID_element>();
           TigComSchemasIdGeneratorrequest.Envelope_element TigschemasgenerRequest = new TigComSchemasIdGeneratorrequest.Envelope_element();
           TigschemasgenerRequest.Service_Type = 'Mock response';
           //response_map_x.put('response_x',TigschemasgenerRequest);
       }
    }

    global class GetServiceNowInventoryDetails implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           GetServiceNowParamDetails.Service_xc request_x = new GetServiceNowParamDetails.Service_xc();
           request_x.object_type_xc = 'Mock response';
           soapAction = request_x.object_type_xc;
           //response.put('response_x', request_x);
       }
    }
    
    
    global class SuspendOrderResponse_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           wwwTibcoComAffOrderservice.SuspendOrderResponse_element SuspendorderResponse = new wwwTibcoComAffOrderservice.SuspendOrderResponse_element();
           SuspendorderResponse.orderRef = 'Mock Response';
           soapAction = SuspendorderResponse.orderRef;
           
       }
    }
    
    global class GetOrdersRequest_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.GetOrdersRequest_element request_x  = new  wwwTibcoComAffOrderservice.GetOrdersRequest_element();
           request_x.orderRef = 'Mock Response';
           soapAction = request_x.orderRef;
           
       }
    }
    
     global class PerformBulkOrderActionRequest_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.PerformBulkOrderActionRequest_element request_x = new wwwTibcoComAffOrderservice.PerformBulkOrderActionRequest_element();
           request_x.action = 'Mock Response';
           responseNS = request_x.action ;
           
       }
    }
    
    global class GetOrderDetailsRequest_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           wwwTibcoComAffOrderservice.GetOrderDetailsRequest_element request_x = new wwwTibcoComAffOrderservice.GetOrderDetailsRequest_element();
           request_x.orderRef= 'Mock Response';
           soapAction = request_x.orderRef;
           
       }
    }
    
    global class GetOrderExecutionPlanResponse_element implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
              
               String responseType) {
           wwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_element request_x = new wwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_element();
           request_x.ExternalBusinessTransactionID = 'Mock Response';
           //wwwTibcoComAffOrderservice.GetOrderExecutionPlanResponse_element response_x = request_x.ExternalBusinessTransactionID;
           system.assertEquals(true,request_x!= null);
           
       }
    }
    
   global class InvDetailsOutputVO implements WebServiceMock {
       global void doInvoke(
               Object stub,
               Object request,
               Map<String, Object> response,
               String endpoint,
               String soapAction,
               String requestName,
               String responseNS,
               String responseName,
               String responseType) {
           
           GetInventoryDetailsParams.InvDetailsOutputVO  InvDetlsOutput= new GetInventoryDetailsParams.InvDetailsOutputVO();
           InvDetlsOutput.ObjectType = 'Mock response';
           response.put('response_x',InvDetlsOutput);
       }
    }
   
             
}