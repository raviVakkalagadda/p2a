public class MasterServiceLogic {
    
    // private context data
	//For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<Id, cscfga__Product_Configuration__c> contextProductMap;
	//For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<Id, Id> clonedTokenMap;
    
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<String, String> productToMasterServiceMap = new Map<String, String> { 
        key15(Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c)         => key15(Product_Definition_Id__c.getvalues('Master_IPVPN_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c)        => key15(Product_Definition_Id__c.getvalues('Master_IPVPN_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c)   => key15(Product_Definition_Id__c.getvalues('Master_VPLS_Service_Definition_Id').Product_Id__c),
        key15(Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c)     => key15(Product_Definition_Id__c.getvalues('Master_VPLS_Service_Definition_Id').Product_Id__c)  
    };
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<String, String> productToSolutionTypeMap = new Map<String, String> { 
        key15(Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c)            => 'IPVPN',
        key15(Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c)           => 'IPVPN',
        key15(Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c)      => 'Transparent Mode',
        key15(Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c)        => 'VLAN Mode'  
    };
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<String, String> solutionTypeToOfferMap = new Map<String, String> { 
        'IPVPN'                 => key15(Offer_Id__c.getvalues('Master_IPVPN_Service_Offer_Id').Offer_Id__c),
        'Transparent Mode'     => key15(Offer_Id__c.getvalues('Master_VPLS_Transparent_Offer_Id').Offer_Id__c),
        'VLAN Mode'             => key15(Offer_Id__c.getvalues('Master_VPLS_VLAN_Offer_Id').Offer_Id__c)  
    };
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<String, String> solutionTypeToOfferConfigMap;
    //For @Testvisible , modified by Anuradha
	@Testvisible
    private static Map<String, cscfga__Product_Configuration__c> offerConfigMap;
    
    /* private static void initMasterServiceMap() {
        if (productToMasterServiceMap == null) {
            productToMasterServiceMap = new Map<String, String>();
            for (Master_Service_Map__c ms: Master_Service_Map__c.getall().values()) {
                productToMasterServiceMap.put(key15(ms.Product_Definition_Id__c), key15(ms.Master_Product_Definition_Id__c));
            }
        }   
    } */
   
    // main business logic method
    public static void process(Id basketId) {

        // initialize context data 
        initContextData(basketId);
        
        // process context data
        processContextData(basketId);
    }
    
    @TestVisible
    private static void initOfferConfigMap() {

        offerConfigMap = new Map<String, cscfga__Product_Configuration__c>();
        for (cscfga__Product_Configuration__c pc: [SELECT id
                    , Name
                    , cscfga__Product_Definition__r.cscfga__Product_Category__c
                    , cscfga__Product_Definition__c
                    , cscfga__Configuration_Offer__c
                    , cscfga__Configuration_Offer__r.Name
                    , Solution_Type__c
                    , cscfga__Configuration_Status__c
                FROM  
                    cscfga__Product_Configuration__c 
                WHERE 
                    cscfga__Configuration_Offer__r.Id in :solutionTypeToOfferMap.values()]) {
            
            offerConfigMap.put(pc.Id, pc);
            offerConfigMap.put(pc.Solution_Type__c, pc);
        }
        
        system.debug('offerConfigMap: ' + offerConfigMap.keySet());
    }
    
    @TestVisible
    private static void initContextData(Id basketId) {
        
        Map<Id, cscfga__Product_Configuration__c> allProductsMap = new Map<Id, cscfga__Product_Configuration__c>([SELECT Id
                , Name
                , cscfga__Product_Basket__r.Name
                , cscfga__Product_Basket__c
                , cscfga__Product_Definition__r.Name
                , cscfga__Product_Definition__c
                , Master_IPVPN_Configuration__c
                , cscfga__Configuration_Offer__c 
            FROM 
                cscfga__Product_Configuration__c 
            WHERE 
                cscfga__Product_Basket__c = :basketId and
                cscfga__Product_Definition__c in :productToSolutionTypeMap.keySet() and
                cscfga__Configuration_Offer__c = '' 
            ORDER BY 
                Id DESC]);
        
        system.debug('productToSolutionTypeMap.keySet: ' + productToSolutionTypeMap.keySet());
        system.debug('allProductsMap.keySet: ' + allProductsMap.keySet());
        
        // initialize data context map
        contextProductMap = new Map<Id, cscfga__Product_Configuration__c>();
        
        // iterate and collect data
        for (cscfga__Product_Configuration__c pc: allProductsMap.values()) {
        	if (pc.cscfga__Product_Definition__r.Name.contains('Archived')) {
        		if (pc.cscfga__Product_Definition__r.Name.contains('IPVPN Port')) {
        			pc.cscfga__Product_Definition__c = Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c;
        		} else if (pc.cscfga__Product_Definition__r.Name.contains('SMA Gateway')) {
        			pc.cscfga__Product_Definition__c = Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c;
        		} else if (pc.cscfga__Product_Definition__r.Name.contains('VPLS Transparent')) {
        			pc.cscfga__Product_Definition__c = Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c;
        		} else if (pc.cscfga__Product_Definition__r.Name.contains('VPLS VLAN Port')) {
        			pc.cscfga__Product_Definition__c = Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c;
        		}
        	}
        	if (productToSolutionTypeMap.keySet().contains(key15(pc.cscfga__Product_Definition__c))) {
        		contextProductMap.put(pc.Id, pc);
        	}
        }
    }
    
    @TestVisible
    private static void processContextData(Id basketId) {
    
        // initialize static data
        initOfferConfigMap();
    
        // create missing master service products
        createBasketMasterService(basketId);

        // update ipvpn master services references
        updateProductsMasterService(basketId);
    }
    
    @TestVisible
    private static string key15(Id sfdcId) {
        return 
            string.valueOf(sfdcId).substring(0, 15);
    }

    @TestVisible
    private static String getSolutionType(cscfga__Product_Configuration__c config) {
        return 
            productToSolutionTypeMap.get(key15(config.cscfga__Product_Definition__c));
    }
    
    @TestVisible
    // this has to be fixed, as it has to return the offer name -> product configuration id that represents the master config
    private static Map<String, cscfga__Product_Configuration__c> createSolutionMasterConfigMap(Id basketId) {
        
        //select all Master IPVPN Service product configurations whose basketId equals IPVPN Port basketId
        List<cscfga__Product_Configuration__c> masters = [SELECT Id
                , Name
                , cscfga__Product_Basket__r.Name
                , cscfga__Product_Basket__c
                , cscfga__Product_Definition__c
                , cscfga__Product_Definition__r.Name  
                , Solution_Type__c
            FROM 
                cscfga__Product_Configuration__c 
            WHERE 
                cscfga__Product_Definition__c in :productToMasterServiceMap.values() and
                cscfga__Product_Basket__c = :basketId
            ORDER BY 
                CreatedDate desc];
        
        Map<String, cscfga__Product_Configuration__c> mastersMap = new Map<String, cscfga__Product_Configuration__c>();
        for (cscfga__Product_Configuration__c config : masters) {
            if (!mastersMap.containsKey(config.Solution_Type__c)) { 
                mastersMap.put(config.Solution_Type__c, config);
            }
        }
        
        return mastersMap;
    }
    
    @TestVisible
    private static void createBasketMasterService(Id basketId) { 
        
        // check and create missing IPVPNService config
        Map<String, cscfga__Product_Configuration__c> solutionTypeToMasterConfigMap = createSolutionMasterConfigMap(basketId);
        
        Map<String, Id> mastersToCreateMap = new Map<String, Id>();
        for (cscfga__Product_Configuration__c config: contextProductMap.values()) {
            string solutionType = getSolutionType(config);
            if (offerConfigMap.containsKey(solutionType)
                && !solutionTypeToMasterConfigMap.containsKey(solutionType)
                && !mastersToCreateMap.containsKey(solutionType)
                && config.Master_IPVPN_Configuration__c==NULL) {
                mastersToCreateMap.put(solutionType, offerConfigMap.get(solutionType).Id);
            }
        }
        
        // insert the configs
        Set<Id> toCopy = new Set<Id>(mastersToCreateMap.values());
        if (toCopy.size() > 0) {
            Map<Id, Id> oldToNewMap = cscfga.ProductConfigurationBulkActions.copyProductConfigurations(toCopy, null, basketId, false);
            createMasterConfigurationRequests(basketId, toCopy, oldToNewMap);
            finishMasterConfigurations(basketId, toCopy, oldToNewMap);
        }
    }

    @TestVisible
    private static void createMasterConfigurationRequests(Id basketId, Set<Id> masterIds, Map<Id, Id> oldToNewMap) {
        
        List<csbb__Product_Configuration_Request__c> toInsert = new List<csbb__Product_Configuration_Request__c>();
        for (Id masterId: masterIds) {
            if (offerConfigMap.containsKey(masterId)) {
                toInsert.add (new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basketId
                    , csbb__Product_Category__c = offerConfigMap.get(masterId).cscfga__Product_Definition__r.cscfga__Product_Category__c
                    , csbb__Product_Configuration__c = oldToNewMap.get(masterId)
                    , csbb__Optionals__c = '{}'));
            }
        }
        
        if (toInsert.size() > 0) {
            insert toInsert;
        }
    }

    /* @TestVisible
    private static void finishMasterConfigurations(Id basketId, Set<Id> masterIds, Map<Id, Id> oldToNewMap) {

        cscfga.API_1.ApiSession apiSession;
        for (Id masterId: masterIds) {
            apiSession = cscfga.API_1.getApiSession(new cscfga__Product_Configuration__c(Id = oldToNewMap.get(masterId))); 
            apiSession.executeRules();
            apiSession.updateLineItems();
            apiSession.persistConfiguration();
            apiSession.close();
        }
    }
    
    @TestVisible
    private static void finishMasterConfigurations(Id basketId, Set<Id> masterIds, Map<Id, Id> oldToNewMap) {

        Set<Id> toValidate = new Set<Id>();
        for (Id masterId: masterIds) {
            toValidate.add(oldToNewMap.get(masterId));
        }
        
        if (toValidate.size() > 0) {
            system.debug('toValidate ' + toValidate);
            cscfga.ProductConfigurationBulkActions.revalidateConfigurations(toValidate);
        }
    } */
    //For @Testvisible , modified by Anuradha
    @TestVisible
    private static void finishMasterConfigurations(Id basketId, Set<Id> masterIds, Map<Id, Id> oldToNewMap) {

        List<cscfga__Product_Configuration__c> toUpdate = new List<cscfga__Product_Configuration__c>();
        for (Id masterId: masterIds) {
            if (offerConfigMap.containsKey(masterId)) {
                toUpdate.add(new cscfga__Product_Configuration__c(Id = oldToNewMap.get(masterId)
                    , cscfga__Configuration_Status__c = offerConfigMap.get(masterId).cscfga__Configuration_Status__c));
            }
        }
        
        if (toUpdate.size() > 0) {
            system.debug('finishing: ' + toUpdate);
            update toUpdate;
        }
    }
    
    @TestVisible
    private static void updateProductsMasterService(Id basketId) {
        
        // take the product configuration ids
        List<cscfga__Product_Configuration__c> pcs = contextProductMap.values();
        
        // create the basket to master ipvpn service map
        Map<String, cscfga__Product_Configuration__c> solutionTypeToMasterConfigMap = createSolutionMasterConfigMap(basketId);
        
        // update master service for every port
        List<cscfga__Product_Configuration__c> toUpdate = new List<cscfga__Product_Configuration__c>();
        for (cscfga__Product_Configuration__c pc : pcs) {
            cscfga__Product_Configuration__c masterPc  = solutionTypeToMasterConfigMap.get(getSolutionType(pc)); 
            if (masterPc != null) {
                toUpdate.add(new cscfga__Product_Configuration__c(Id = pc.Id
                    , Master_IPVPN_Configuration__c = masterPc.Id));
            }
        }
        
        if (toUpdate.size() > 0) {
            system.debug('toUpdate' + toUpdate);
            update toUpdate;
        }
    }  
   
}