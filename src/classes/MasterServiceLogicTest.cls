@isTest(SeeAllData=false)
public class MasterServiceLogicTest {
    
    private static cscfga__Product_Basket__c basket = null;
    
    private static cscfga__Product_Configuration__c config = null;
        
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

    private static void createTestData() {
        P2A_TestFactoryCls.sampletestdata();
        List<String> definitionIds= new List<String>(MasterServiceLogic.productToSolutionTypeMap.keySet());

        basket = new cscfga__Product_Basket__c();
        insert basket;
        system.assertEquals(true,basket!=null); 
        config = new cscfga__Product_Configuration__c(Name = 'Test config'
            , cscfga__Product_Definition__c = definitionIds[0]
            , cscfga__Product_Basket__c = basket.Id);
        insert config;
        system.assertEquals(true,config!=null); 
     
        /* cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession(config); 
        apiSession.persistConfiguration(true);
        apiSession.close(); */
    }

    private static void tryToCreateTestData() {
        Exception ee = null;
        try {
            disableAll(UserInfo.getUserId());
            createTestData();
        } catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='MasterServiceLogicTest :tryToCreateTestData';         
ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    @testVisible
    private static testMethod void testProcessCall() {
    P2A_TestFactoryCls.sampletestdata();
        tryToCreateTestData();
        set<id> masterids = new set<id>();
        masterids.add(basket.id);
        Map<id,id> mapids = new map<id,id>();
        mapids.put(basket.id , basket.id);
        MasterServiceLogic.process(basket.Id);
        MasterServiceLogic.createMasterConfigurationRequests(basket.Id, masterids, mapids);
        MasterServiceLogic.finishMasterConfigurations(basket.Id, masterids, mapids);
        MasterServiceLogic.updateProductsMasterService(basket.Id);
        cscfga__Product_Configuration__c toAssert = [select Id, Name, Master_IPVPN_Configuration__c from cscfga__Product_Configuration__c where Id =: config.Id];
        system.assertEquals(true,toAssert!=null); 
    }
}