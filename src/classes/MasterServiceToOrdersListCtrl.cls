public class MasterServiceToOrdersListCtrl {
	private final Id masterServiceId;
    public List<ServiceDetailWrapper> serWrapLst;
    
    public MasterServiceToOrdersListCtrl (ApexPages.StandardController stdController) {
        this.masterServiceId = (Id) stdController.getRecord().get('id'); 
        serWrapLst = new List<ServiceDetailWrapper>();
    }
    //Test
    public List<ServiceDetailWrapper> getserWrapLst() {
        
        Map<Id, String> ServiceProcessMap = new Map<Id, String>();
        List<String> OrderIds = new List<String>();
        Map<Id, csord__Service__c> mapServChild;// = new Map<Id, csord__Service__c>();
        Map<Id, csord__Service__c>  mapServ =  new Map<Id, csord__Service__c>([select Id
                , Name
                , csord__Order__c
                , csord__Order__r.Name
                , Order_Number__c
                , csord__Order__r.Order_Type__c
                , csord__Status__c
                , csord__Order__r.Customer_Required_Date__c
                , Master_Service__c
                , csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name
            from
                csord__Service__c 
            where 
                Master_Service__c = :masterServiceId]);

        if(!mapServ.isEmpty()){
            for(csord__Service__c ser : mapServ.values()){
                OrderIds.add(ser.csord__Order__c);
            }
            
            mapServChild = new Map<Id, csord__Service__c>([select Id
                                                            , Name
                                                            , csord__Order__c
                                                            , csord__Order__r.Name
                                                            , Order_Number__c
                                                            , csord__Order__r.Order_Type__c
                                                            , csord__Status__c
                                                            , csord__Order__r.Customer_Required_Date__c
                                                            , Master_Service__c
                                                            , csord__Service__c
                                                            , csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name
                                                        from
                                                            csord__Service__c 
                                                        where 
                                                            csord__Service__c IN:mapServ.keyset()]);
            if(!mapServChild.isEmpty()){
                for(csord__Service__c ser : mapServChild.values()){
                    OrderIds.add(ser.csord__Order__c);
                }
                mapServ.putAll(mapServChild);
            }
            List<CSPOFA__Orchestration_Process__c> orchProcLst = 
                        [select Id, CSPOFA__Progress__c , Order__c FROM CSPOFA__Orchestration_Process__c 
                            Where Order__c IN :OrderIds];

            if(!orchProcLst.isEmpty()){
                
                for(csord__Service__c thisServ : mapServ.values()){
                    for(CSPOFA__Orchestration_Process__c thisProc : orchProcLst){
                        ServiceProcessMap.put(thisProc.Order__c, thisProc.CSPOFA__Progress__c);
                    }
                    ServiceDetailWrapper thisServWrap = new ServiceDetailWrapper();
                    thisServWrap.Name = thisServ.csord__Order__r.Name;// + ' - ' + thisServ.csordtelcoa__Service_Number__c;
                    thisServWrap.OrderNumber = thisServ.Order_Number__c;
                    thisServWrap.OrderType = thisServ.csord__Order__r.Order_Type__c;
                    thisServWrap.Product = thisServ.csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name;
                    thisServWrap.CRD = (String.isNotBlank(String.valueOf(thisServ.csord__Order__r.Customer_Required_Date__c))? thisServ.csord__Order__r.Customer_Required_Date__c.format() : null);
                    thisServWrap.ServiceName = thisServ.Name;
					thisServWrap.ServiceId = thisServ.Id;
                    System.debug('AAAA ==> ' + ServiceProcessMap);
                    thisServWrap.Progress = ServiceProcessMap.get(thisServ.csord__Order__c);
                    thisServWrap.Id = thisServ.csord__Order__c;
                    serWrapLst.add(thisServWrap);
                }
            } 

        }  
        return serWrapLst;
    }

    public class ServiceDetailWrapper{
        public String Id {get;set;}
        public String Name {get;set;}
        public String CRD {get;set;}
        public String ServiceName {get;set;}
		public String ServiceId {get;set;}
        public String Progress {get;set;}
        public String OrderNumber {get;set;}
        public String Product {get;set;}
        public String OrderType {get;set;}
    }
}