@isTest(SeeAllData=false)
public with sharing class MasterServiceToOrdersListCtrlTest {
    
    static testmethod void TestGetSerWrapLst() {     
         //  allowpctoinsert.sampletestdata();
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
        List<csord__Service__c> serList = P2A_TestFactoryCls.getService(1,orderRequestList, subList);
        serList[0].Primary_Service_ID__c = 'Test Service';
        serList[0].Group_Service_ID__c = 'Master Service';
        serList[0].Group_Service_ID__c = 'group service';
        upsert serList[0]; 
        
        csord__Service__c masterServ = new csord__Service__c(); 
        List<csord__Service__c> childServs = new List<csord__Service__c>(); 
        csord__Service__c childServ = new csord__Service__c();
        List<CSPOFA__Orchestration_Process__c> thisOrchProcLst = new List<CSPOFA__Orchestration_Process__c>();
        
       // Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        csord__Order_Request__c ordReqId = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert ordReqId;
         CSPOFA__Orchestration_Process_Template__c orchprocessTemplist = new CSPOFA__Orchestration_Process_Template__c(name = 'Template',CSPOFA__Associated_Profile__c = 'Profile');
                            insert orchprocessTemplist;
        List<csord__Order__c> ordLst = new List<csord__Order__c>();
        List<csord__Order__c> newOrdLst = [select Id from csord__Order__c ORDER BY CreatedDate DESC limit 3];
        List<csord__Subscription__c> subLst = new List<csord__Subscription__c>();
        
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Order_Type__c = 'New',
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order);
        csord__Order__c order2 = new csord__Order__c(
                                                    name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order2);

        csord__Order__c order3 = new csord__Order__c(
                                                    name='Test Order3', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order3);
        insert ordLst;

        csord__Subscription__c sub = new csord__Subscription__c(csord__order__c = ordLst.get(0).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub);
        
        csord__Subscription__c sub2 = new csord__Subscription__c(csord__order__c = ordLst.get(1).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub2);
        csord__Subscription__c sub3 = new csord__Subscription__c(csord__order__c = ordLst.get(2).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub3);
        insert subLst;
        CSPOFA__Orchestration_Process__c thisOrchProc;

       
        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        masterServ.Master_Service__c = serList[0].id;
        masterServ.csord__Subscription__c = sub.Id;
        masterServ.csord__Status__c = 'completd';
        masterServ.csord__Order__c = ordLst[0].id;
        masterServ.csordtelcoa__Product_Configuration__c = proconfigs[0].id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
        //masterServ.Order_Number__c =  '897565165';
        //insert masterServ;
        
        
       /* 
        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        //masterServ.Master_Service__c = '';
        masterServ.csord__Subscription__c = sub.Id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
        insert masterServ;
       
       */ 
        
        for(integer xx=0;xx<3;xx++){
            childServ = new csord__Service__c();
            childServ.csordtelcoa__Service_Number__c = '1234-' + xx;
            childServ.Name = 'Test Serv 1234-' + xx;
            childServ.Master_Service__c = masterServ.Id;
            childServ.csord__Subscription__c = subLst.get(xx).Id;
            childServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
            childServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
            //childServ.csord__Order__c = newOrdLst.get(xx).Id;
            childServs.add(childServ);
        }
        insert childServs;

        

        /*for(csord__Service__c thisServ : childServs){
            thisServ.csord__Order__c = ordLst.get(0).Id;
            update thisServ;
        }*/

        for(integer xx=0;xx<3;xx++){
            thisOrchProc = new CSPOFA__Orchestration_Process__c();
            thisOrchProc.Order__c = ordLst.get(xx).Id;
            thisOrchProc.CSPOFA__Progress__c = '25%';
            thisOrchProc.CSPOFA__Status__c = 'In Progress';
            thisOrchProc.CSPOFA__State__c = 'ACTIVE';
           // thisOrchProc.CSPOFA__Orchestration_Process_Template__c = [select Id from CSPOFA__Orchestration_Process_Template__c
            //                                                            where Name ='Order_New'].get(0).Id;
            thisOrchProc.CSPOFA__Orchestration_Process_Template__c = orchprocessTemplist.Id; 
            thisOrchProcLst.add(thisOrchProc);
        }
        
        
         List<csord__Service__c>  thisServ = [select Id
                                            , Name
                                            , csord__Order__c
                                            , csord__Order__r.Name
                                            , Order_Number__c
                                            , csord__Order__r.Order_Type__c
                                            , csord__Status__c
                                            , csord__Order__r.Customer_Required_Date__c
                                            , Master_Service__c
                                            , csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name
                                        from
                                            csord__Service__c 
                                        where 
                                            id =:serList[0].id];
                
                
         
        MasterServiceToOrdersListCtrl.ServiceDetailWrapper thisServWrap = new MasterServiceToOrdersListCtrl.ServiceDetailWrapper();
        
        thisServWrap.Name = thisServ[0].csord__Order__r.Name;
        thisServWrap.OrderNumber = thisServ[0].Order_Number__c;
        thisServWrap.OrderType = thisServ[0].csord__Order__r.Order_Type__c;
        thisServWrap.Product = thisServ[0].csordtelcoa__Product_Configuration__r.cscfga__Product_Definition__r.Name;
        thisServWrap.CRD = (String.isNotBlank(String.valueOf(thisServ[0].csord__Order__r.Customer_Required_Date__c))? thisServ[0].csord__Order__r.Customer_Required_Date__c.format() : null);
        thisServWrap.ServiceName = thisServ[0].Name;
        thisServWrap.ServiceId = thisServ[0].Id;
        //System.debug('AAAA ==> ' + ServiceProcessMap);
        //thisServWrap.Progress = ServiceProcessMap.get(thisServ.csord__Order__c);
        thisServWrap.Id = thisServ[0].csord__Order__c;
        //serWrapLst.add(thisServWrap);
        
        
        Test.startTest();
        MasterServiceToOrdersListCtrl mastCtrl = new MasterServiceToOrdersListCtrl(new ApexPages.StandardController(masterServ));
        List<MasterServiceToOrdersListCtrl.ServiceDetailWrapper> mmWrap = mastCtrl.getserWrapLst();
        System.assert(mmWrap != null);
        MasterServiceToOrdersListCtrl.ServiceDetailWrapper serviceWrap = new MasterServiceToOrdersListCtrl.ServiceDetailWrapper();
        //System.assertEquals(mmWrap.get(0).Progress, '25%');
        Test.stopTest();
    }
}