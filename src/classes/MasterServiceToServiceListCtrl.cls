public with sharing class MasterServiceToServiceListCtrl {
    
    private final Id masterServiceId;
    public List<ServiceDetailWrapper> serWrapLst;
    
    public MasterServiceToServiceListCtrl (ApexPages.StandardController stdController) {
        this.masterServiceId = (Id) stdController.getRecord().get('id'); 
        serWrapLst = new List<ServiceDetailWrapper>();
    }
    //Test
    public List<ServiceDetailWrapper> getserWrapLst() {
        
        Map<Id, String> ServiceProcessMap = new Map<Id, String>();
        Map<Id, csord__Service__c> mapServChild;
        Map<Id, csord__Service__c>  mapServ =  new Map<Id, csord__Service__c>([select Id
                , Name
                , csordtelcoa__Service_Number__c
                , csord__Status__c
                , Customer_Required_Date__c
                , Master_Service__c
                , Primary_Service_ID__c
                , csord__Subscription__c
                , csord__Subscription__r.Id
                , csord__Subscription__r.Name
                , csord__Subscription__r.csord__Status__c
            from
                csord__Service__c 
            where 
                Master_Service__c = :masterServiceId]);

        if(!mapServ.isEmpty()){
            mapServChild =  new Map<Id, csord__Service__c>([select Id
                                                            , Name
                                                            , csordtelcoa__Service_Number__c
                                                            , csord__Status__c
                                                            , Customer_Required_Date__c
                                                            , Master_Service__c
                                                            , Primary_Service_ID__c
                                                            , csord__Subscription__c
                                                            , csord__Subscription__r.Id
                                                            , csord__Subscription__r.Name
                                                            , csord__Subscription__r.csord__Status__c
                                                        from
                                                            csord__Service__c 
                                                        where 
                                                            csord__Service__c IN:mapServ.keyset()]);
            if(!mapServChild.isEmpty()){
                mapServ.putAll(mapServChild);
            }

            List<CSPOFA__Orchestration_Process__c> orchProcLst = 
                        [select Id, CSPOFA__Progress__c , csordtelcoa__Service__c FROM CSPOFA__Orchestration_Process__c 
                            Where csordtelcoa__Service__c IN :mapServ.keySet()];

            if(!orchProcLst.isEmpty()){
                
                for(csord__Service__c thisServ : mapServ.values()){
                    for(CSPOFA__Orchestration_Process__c thisProc : orchProcLst){
                        ServiceProcessMap.put(thisProc.csordtelcoa__Service__c, thisProc.CSPOFA__Progress__c);
                    }
                    ServiceDetailWrapper thisServWrap = new ServiceDetailWrapper();
                    thisServWrap.Name = thisServ.Name + ' - ' + thisServ.csordtelcoa__Service_Number__c;
                    thisServWrap.CRD = (String.isNotBlank(String.valueOf(thisServ.Customer_Required_Date__c))? thisServ.Customer_Required_Date__c.format() : null);
                    //thisServWrap.CRD = thisServ.Customer_Required_Date__c.format();
                    thisServWrap.Status = thisServ.csord__Status__c;
                    thisServWrap.Subscription = thisServ.csord__Subscription__r.Name;
                    System.debug('AAAA ==> ' + ServiceProcessMap);
                    thisServWrap.Progress = ServiceProcessMap.get(thisServ.Id);
                    thisServWrap.Id = thisServ.Id;
                    thisServWrap.ServiceID = thisServ.Primary_Service_ID__c;
                    serWrapLst.add(thisServWrap);
                }
            } 

        }  
        return serWrapLst;
    }

    public class ServiceDetailWrapper{
        public String Id {get;set;}
        public String Name {get;set;}
        public String CRD {get;set;}
        public String Status {get;set;}
        public String Subscription {get;set;}
        public String Progress {get;set;}
        public String ServiceID {get;set;}
    }
}