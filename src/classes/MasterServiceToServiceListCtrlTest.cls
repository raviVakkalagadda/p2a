@isTest(SeeAllData=false)
public class  MasterServiceToServiceListCtrlTest { 
    
    static testmethod void TestGetSerWrapLst(){
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        P2A_TestFactoryCls.sampleTestData();
        
         Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                insert pdI2;
        
        csord__Service__c masterServ = new csord__Service__c(); 
        List<csord__Service__c> childServs = new List<csord__Service__c>(); 
        csord__Service__c childServ = new csord__Service__c();
        List<CSPOFA__Orchestration_Process__c> thisOrchProcLst = new List<CSPOFA__Orchestration_Process__c>();
        // Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        
        csord__Order_Request__c ordReqId = new csord__Order_Request__c(Name = 'Test request'
                                                                       , csord__Module_Version__c = 'dummy'
                                                                       , csord__Module_Name__c = 'dummy');
        insert ordReqId;
        
        System.assertEquals(ordReqId.name ,'Test request'); 

        
        CSPOFA__Orchestration_Process_Template__c orchprocessTemplist = new CSPOFA__Orchestration_Process_Template__c(name = 'Template',CSPOFA__Associated_Profile__c = 'Profile');
        insert orchprocessTemplist;
        
         System.assertEquals(orchprocessTemplist.name ,'Template'); 
       
        
        csord__Order__c order = new csord__Order__c(
            name='Test Order', 
            csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
            csord__Order_Request__c = ordReqId.Id,
            Customer_Required_Date__c = Date.newInstance(2016,08,01));
        insert order;
         System.assertEquals(order.name ,'Test Order'); 
        
        
        csord__Subscription__c sub = new csord__Subscription__c(csord__order__c = order.Id,
                                                                csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                                csord__Order_Request__c = ordReqId.Id);
        insert sub;
        
        System.assertEquals(sub.csord__Identification__c ,'Order_a24O0000000OZ7aIAG'); 
        
        
        CSPOFA__Orchestration_Process__c thisOrchProc;
        
        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        //masterServ.Master_Service__c = '';
        masterServ.csord__Subscription__c = sub.Id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
        insert masterServ;
        
        System.assertEquals(masterServ.name ,'Test Master Serv 1234'); 

        
        for(integer xx=0;xx<3;xx++){
            childServ = new csord__Service__c();
            childServ.csordtelcoa__Service_Number__c = '1234-' + xx;
            childServ.Name = 'Test Serv 1234-' + xx;
            childServ.Master_Service__c = masterServ.Id;
            childServ.csord__Subscription__c = sub.Id;
            childServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
            childServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
            childServs.add(childServ);
        }
        insert childServs;
        
        System.assertEquals(childServs[0].csord__Subscription__c ,sub.Id); 
        
        
        for(integer xx=0;xx<3;xx++){
            thisOrchProc = new CSPOFA__Orchestration_Process__c();
            thisOrchProc.csordtelcoa__Service__c = childServs.get(xx).Id;
            thisOrchProc.CSPOFA__Progress__c = '25%';
            thisOrchProc.CSPOFA__Status__c = 'In Progress';
            thisOrchProc.CSPOFA__State__c = 'ACTIVE';
            //  thisOrchProc.CSPOFA__Orchestration_Process_Template__c = [select Id from CSPOFA__Orchestration_Process_Template__c
            //                                                             where Name ='Order_New'].get(0).Id;
            thisOrchProc.CSPOFA__Orchestration_Process_Template__c = orchprocessTemplist.Id; 
            thisOrchProcLst.add(thisOrchProc);
        }
        insert thisOrchProcLst;
        System.assertEquals(thisOrchProcLst[0].CSPOFA__Progress__c ,'25%'); 
        
        Test.startTest();
        MasterServiceToServiceListCtrl mastCtrl = new MasterServiceToServiceListCtrl(new ApexPages.StandardController(masterServ));
        List<MasterServiceToServiceListCtrl.ServiceDetailWrapper> mmWrap = mastCtrl.getserWrapLst();
        System.assert(mmWrap != null);
        System.assertEquals(mmWrap.get(0).Progress, '25%');
        Test.stopTest();
    }
}