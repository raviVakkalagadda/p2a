@isTest(SeeAllData = false)
public class MiscChargeGCCTest{

    static testmethod void TestMiscChargeGCC() {
        
        List<Country_Lookup__c> conList = P2A_TestFactoryCls.getcountry(1);      
        List<cscfga__Configuration_Offer__c> offer = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Definition__c> prodDefList = P2A_TestFactoryCls.getProductdef(1);
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Site__c> siteList = P2A_TestFactoryCls.getsites(1, ListAcc, conList);
        List<Contact> contactList = P2A_TestFactoryCls.getContact(1, ListAcc);
        List<BillProfile__c> bpList = P2A_TestFactoryCls.getBPs(1, ListAcc,siteList,contactList);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(1,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = P2A_TestFactoryCls.getservice(1,OrdReqList,subscriptionList);    
        List<case>caseList=P2A_TestFactoryCls.getcase(1,ListAcc); 
        List<csord__Service_Line_Item__c> sliList = P2A_TestFactoryCls.getSerLineItem(1, servList,OrdReqList);
        system.assertEquals(true,sliList!=null); 
        /*
        Name,Service_Bill_Text__c,CurrencyIsoCode,Bill_ProfileId__c,
        Bill_ProfileId__r.Name,Cost_Centre_Id__c,Cost_Centre_Id__r.Name,
        csord__Order_Request__c,csord__Identification__c,AccountId__c
        */
        List<MISC_Chargeable_Product__c> miscCP = new List<MISC_Chargeable_Product__c>();
        MISC_Chargeable_Product__c miscCPobj1 = new MISC_Chargeable_Product__c();
        MISC_Chargeable_Product__c miscCPobj2 = new MISC_Chargeable_Product__c();
            miscCPobj1.Name = 'Intercom';
            miscCPobj1.One_Time_Charge_Amount__c = 30;
            miscCPobj1.One_Time_Charge_Text__c = 'Network Feature Change (Intercom) One-off Charge - SD';
            miscCPobj1.Product_Code__c = 'IPVPN';
        miscCP.add(miscCPobj1);
            miscCPobj2.Name = 'TestName2';
            miscCPobj2.One_Time_Charge_Amount__c = 1000;
            miscCPobj2.One_Time_Charge_Text__c = 'ek hazaar';
            miscCPobj2.Product_Code__c = 'IPVPN';
        miscCP.add(miscCPobj2);
        insert miscCP;
        system.assertEquals(true,miscCP!=null); 
        
        //SELECT Id, Name, Cost_Centre_Code__c, BillProfile__c FROM CostCentre__c
        List<CostCentre__c> ccList = new List<CostCentre__c>();
        CostCentre__c cc1 = new CostCentre__c();
            cc1.Name='costTest1';
            cc1.Cost_Centre_Code__c = 'SUMIT';
            cc1.BillProfile__c = bpList[0].Id;
        ccList.add(cc1);
        insert ccList;
        
        PageReference pageRef = new PageReference('https://telstra--p2aval.cs57.my.salesforce.com/a3R0k0000004C9x');
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id',servList[0].id);
        
        Test.startTest();
        try{
        ApexPages.Standardcontroller sc = new ApexPages.Standardcontroller(ListAcc[0]);
        MiscChargeGCC obj1 = new MiscChargeGCC();
        MiscChargeGCC obj2 = new MiscChargeGCC(sc);
        obj2.miscChargeSLI=sliList[0];
        obj2.chargeAmount=30;
        obj2.cost=0;
        obj2.billText='Network Feature Change (Intercom) One-off Charge - SD';
        obj2.typeofCharge='OCH - One off charge';
        obj2.purchaseOrder='abc';
        obj2.Frequency='Monthly';
        obj2.billProfileName=bpList[0].Name;
        obj2.billProfileId=bpList[0].Id;
        obj2.CostCentreId=ccList[0].Id;
        obj2.CostCentreName=ccList[0].Name;
        obj2.typeofMAC='Intercom';
        obj2.serviceReferenceNo='12345';
        obj2.chargeID='1234';
        obj2.U2CMasterCode='';
        obj2.accId=ListAcc[0].Id;
        obj2.insertToMiscChargeObj();
        obj2.getOneTimeChargeText();
        obj2.cancel();
        system.assertEquals(true,obj1!=null);
        system.assertEquals(true,obj2!=null);
        }catch(Exception e){
            ErrorHandlerException.ExecutingClassName='MiscChargeGCCTest :TestMiscChargeGCC';         
            ErrorHandlerException.sendException(e); 
        }
        Test.stopTest();
    }
}