    public class MonitorOrchestrationProcessCreation{
        
        public List<AsyncApexJob> jobs {get; set;}
        public boolean Enablepolling {get; set;}
        public boolean validateESD {get; set;}
        public String isFinished {get; set;}
        public Id orderId {get; set;}
        Set<Id> AsyncApexJobIds = new Set<Id>();
        csord__Order__c order = null;
        integer JobItemsProcessed;
        integer TotalJobItems;
        integer waitTimer = 0;      

        public MonitorOrchestrationProcessCreation(ApexPages.StandardController controller){
            orderId = controller.getId();
            jobs = new List<AsyncApexJob>();
            Enablepolling = true;
            validateESD = true;
            TotalJobItems = 0;
            isFinished = '';
            refreshJobStatus();
        }

        public void refreshJobStatus(){
            try{
                if(orderId != null){
                    order = [
                        Select Id, Solutions__c, csord__Order_Type__c, RecordTypeID
                        From csord__Order__c
                        Where Id =:orderId
                    ];
                    List<Attachment> attachmentList = [
                                Select Id, Name 
                                From Attachment
                                Where ParentId =:order.Solutions__c
                    ];
                    if(!attachmentList.IsEmpty()){
                        for(Attachment attachmentName :attachmentList){
                            try{
                                Id jobId = Id.ValueOf(attachmentName.Name.split('-')[2]);
                                Id odrId = Id.ValueOf(attachmentName.Name.split('-')[1]);
                                if(!AsyncApexJobIds.contains(jobId) && orderId == odrId){
                                    AsyncApexJobIds.add(jobId);
                                    TotalJobItems++;
                                }
                            } catch(Exception e) {}
                        }
                    } 
                    if(!AsyncApexJobIds.IsEmpty()){
                        jobs = [
                            Select Id, JobItemsProcessed, NumberOfErrors, Status, TotalJobItems
                            From AsyncApexJob
                            Where Id IN :AsyncApexJobIds
                        ];                  
                    } else{
                        Enablepolling = true;
                        validateESD = false;
                    }
                }
                getBatchOrderStatus();
            } catch(Exception e){
                System.debug('Error: MoniterOrchestrationProcessCreation; refreshJobStatus" method update failure - ' +e);
              }     
        }
        
        public String getBatchOrderStatus(){
            if(!jobs.IsEmpty()){
                JobItemsProcessed = 0;
                for(AsyncApexJob job :jobs){
                    if(job.Status == 'Completed'){
                        JobItemsProcessed++;
                    }                   
                }
                for(AsyncApexJob job :jobs){
                    if(job.Status == 'Preparing' || job.Status == 'Holding' || job.Status == 'Queued' || job.Status == 'Processing'){
                        Enablepolling = true;
                        return 'Orchestration Process Creation is in Progress: '+String.ValueOf((JobItemsProcessed*100)/TotalJobItems) +'%';
                    }
                    else if(job.Status == 'Failed' || job.Status == 'Aborted' || job.NumberOfErrors > 0){
                        isFinished = 'Error';
                        Enablepolling = true;
                        return 'Error';
                    }
                }
            }
            if(JobItemsProcessed == TotalJobItems){
                waitTimer++;
                if(Math.mod(waitTimer,12) != 0 && waitTimer < 12){
                    Enablepolling = true;
                    if(waitTimer == 11){isFinished = 'Completed';}
                    return 'Critical Path Update in Progress...';
                }
                isFinished = 'Completed';
                return 'Completed, Please refresh the page.';
            } else if(Enablepolling == false){
                isFinished = 'Completed';
                return 'Completed, Please refresh the page.';
            }       
            return 'Orchestration Process Template Creation is in Progress...';
        }
        
        public void deleteAttachmentOfSolution(){
            if(isFinished == 'Error' || !validateESD){
                restartProcess(order);
            } else if(isFinished == 'Completed' && Enablepolling == true){
                deleteAttachment(order.Solutions__c, orderId);
                
                if(order.RecordTypeID == RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation')){
                    updateRecordTypeId(orderId);
                }
                Enablepolling = false;
            }
        }

        @future
        public static void deleteAttachment(Id solutionId, Id orderId){
            if(solutionId != null){
                Map<Id, Attachment> jobAndAttachmentMap = new Map<Id, Attachment>();

                for(Attachment attachmentName :[Select Id, Name From Attachment Where ParentId =:solutionId]){
                    try{
                        Id jobId = Id.ValueOf(attachmentName.Name.split('-')[2]);
                        Id odrId = Id.ValueOf(attachmentName.Name.split('-')[1]);
                        if(!jobAndAttachmentMap.containsKey(jobId) && orderId == odrId){
                            jobAndAttachmentMap.put(jobId, attachmentName);
                        }
                    } catch(Exception e) {}
                }

                for(AsyncApexJob job :[Select Id, Status From AsyncApexJob Where Id IN :jobAndAttachmentMap.KeySet()]){
                    if(!jobAndAttachmentMap.IsEmpty() && jobAndAttachmentMap.get(job.Id) != null){
                        jobAndAttachmentMap.remove(job.Id);
                    }
                }
                delete jobAndAttachmentMap.Values();
            }
        }
        
        @future
        public static void updateRecordTypeId(Id orderId){
            TriggerFlags.NoSubscriptionTriggers = true;
            TriggerFlags.NoServiceTriggers = true;
            TriggerFlags.NoOrderTriggers = true;

            if(orderId != null){
                csord__Order__c order = [Select Id, csord__Order_Type__c, RecordTypeID From csord__Order__c Where Id =:orderId];

                if(order.csord__Order_Type__c == 'Terminate'){
                    order.RecordTypeID = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Cease Order for SD');
                } else{
                    order.RecordTypeID = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'Order Enrichment');
                }
                update order;
            }
        }

        public void restartProcess(csord__Order__c order){
            if(Enablepolling == true){
                deleteAttachment(order.Solutions__c, order.Id);
                List<Id> serviceIds = new List<Id>();
                
                for(csord__Service__c service :[Select Id, Estimated_Start_Date__c from csord__Service__c where csord__Order__c =:orderId]){
                    if(service.Estimated_Start_Date__c == null){
                        serviceIds.add(service.Id);
                    }
                }
                if(!serviceIds.IsEmpty()){
                    new OrchestrationProcessCreation().updateESD(serviceIds);
                }

                if(order.RecordTypeID == RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation')){
                    updateRecordTypeId(orderId);
                }
                isFinished = 'Completed';
                Enablepolling = false;
            }
        }       
    }