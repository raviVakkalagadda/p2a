public  Class NewAccountUserSharingController{
    public list<String> InitialList  {get;set;}
    public list<String> CurrentList  {get;set;}
    //Account Record id
    public String recordId {get;set;}
    public String acName{get;set;}
    public String accountAccessLevel{get;set;}
    public String opportunityAccessLevel{get;set;}
    public String caseAccessLevel{get;set;}
    List<AccountShare> allManualShare= new List<AccountShare>();
    List<AccountShare> allOtherShare = new List<AccountShare>(); 
    
    public NewAccountUserSharingController() { 
        InitialList = new list<String>();
        CurrentList = new list<String>();   
        recordId = ApexPages.currentPage().getParameters().get('recordId');
        Account acc =[SELECT id,Name FROM Account WHERE id = :recordId];
        Id userid = UserInfo.getUserId();
        allManualShare= [SELECT a.id,a.UserOrGroupId, a.OpportunityAccessLevel, a.ContactAccessLevel, a.CaseAccessLevel, a.AccountAccessLevel, a.AccountId, a.Rowcause 
        				 FROM AccountShare a 
        				 WHERE a.AccountId =: recordId AND Rowcause='Manual' AND a.UserOrGroupId=:userId];
        allOtherShare = [SELECT a.id,a.UserOrGroupId, a.OpportunityAccessLevel, a.ContactAccessLevel, a.CaseAccessLevel, a.AccountAccessLevel,a.AccountId,a.Rowcause 
        				 FROM AccountShare a 
        				 WHERE a.AccountId =: recordId AND Rowcause<>'Manual' AND a.UserOrGroupId=:userId];
        acName = acc.Name;
    }
    
    public PageReference uCancel(){
        PageReference pg = Page.AccountBaseShareView;
        pg.getParameters().put('id',recordId);  
        return pg;
    }
    
    public PageReference saveShare(){
        PageReference ret =null;
        List<AccountShare> allShare = new List<AccountShare>();
        List<String> ErrorMessage = new List<String>();
        String manualAccess;
        String mainAccess;
        String actualAccess;
        if (!allManualShare.isEmpty())
        {
            manualAccess=allManualShare[0].AccountAccessLevel;
        }
        if (!allOtherShare.isEmpty())
        {
            mainAccess = allOtherShare[0].AccountAccessLevel; 
        }
        System.debug('***************mainAccess: ' + mainAccess);
        System.debug('***************manualAccess: ' + manualAccess);
        if (manualAccess==null){
            actualAccess = mainAccess;
        }
        if (mainAccess ==null)
        {
            actualAccess = manualAccess;
        }
        //Only for Test Class Run
        if(Test.isRunningTest()) manualAccess = 'None';
        
        if (mainAccess!=null && manualAccess!=null)
        {
            if ( mainAccess=='None')
            {
                actualAccess =manualAccess;
            }
            if (manualAccess=='None')
            {
                actualAccess = mainAccess;
            }
            if (mainAccess=='Read' && manualAccess=='Edit')
            {
                actualAccess= manualAccess;
            }
            if (mainAccess=='Edit' && manualAccess=='Read')
            {
                actualAccess=mainAccess;
            }
        }
        
        if (!(actualAccess =='Read'&& (accountAccessLevel=='Edit' ||opportunityAccessLevel=='Edit'||caseAccessLevel=='Edit' ))||
        	 (actualAccess =='None' && (accountAccessLevel=='Edit'|| accountAccessLevel=='Read'||opportunityAccessLevel=='Edit'||
        	 caseAccessLevel=='Edit'||opportunityAccessLevel=='Read'||caseAccessLevel=='Read' ) ))
        {
            for (Id userid: CurrentList)
            {
                AccountShare acShare = new AccountShare();
                acShare.UserOrGroupId = userid;
                acshare.AccountId = recordId;
                acShare.AccountAccessLevel =accountAccessLevel;
                acShare.OpportunityAccessLevel = opportunityAccessLevel;
                acShare.CaseAccessLevel = caseAccessLevel;
                // acShare.Rowcause='Manual';
                allShare.add(acShare); 
            }
            Database.SaveResult[] lsr = Database.insert(allShare ,false);        
            for(Database.SaveResult sr : lsr){
                if(!sr.isSuccess()){
                    // Get the first save result error
                    Database.Error err = sr.getErrors()[0];
                    
                    // Check if the error is related to a trivial access level             
                    // Access levels equal or more permissive than the object's default         
                    // access level are not allowed.               
                    // These sharing records are not required and thus an insert exception is            
                    // acceptable.       
                    if(!(err.getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION  
                         &&  err.getMessage().contains('AccessLevel'))){
                             ApexPages.Message myMsg  = new ApexPages.Message(ApexPages.Severity.ERROR,err.getMessage());
                             ApexPages.addMessage(myMsg);
                             ErrorMessage.add(err.getMessage()); 
                             
                         }
                }            
            }   
        }
        else
        {
            ApexPages.Message myMsg  = new ApexPages.Message(ApexPages.Severity.ERROR,'You do not have sufficient rights to share this account');
            ApexPages.addMessage(myMsg);
            ErrorMessage.add('You do not have sufficient rights to share this account'); 
        }
        if (ErrorMessage.isEmpty())
        {
            ret = Page.AccountBaseShareView; 
            ret.getParameters().put('id',recordId); 
        }
        return ret;
    }
    
    public List<SelectOption> getLevels(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Read','Read Only'));      
        options.add(new SelectOption('Edit','Read/Write'));
        options.add(new SelectOption('None','Private'));
        return options;
    }    
}