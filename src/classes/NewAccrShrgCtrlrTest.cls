@isTest(seealldata=false)
public class NewAccrShrgCtrlrTest 
{
    static testmethod void testNewAccrShrgCtrlr1() 
    {
        List<String> CurrentList = new List<String>();   
        
        RecordType rt=[SELECT Id,Name FROM RecordType WHERE SObjectType = 'Account' AND recordType.name='Activated Account' Limit 1];
        System.debug('Record Type :::'+rt.Id); 
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                     Selling_Entity__c = 'PT. Reach Network Services Indonesia',Country__c =cntryLkupObj.Id,
                                     Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='12349',
                                     Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test',
                                     RecordTypeId =rt.Id);
        
        insert accObj;
        
        ApexPages.currentPage().getParameters().put('recordId',accObj.id);
        
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh1256@telstra.com',
                                email = 'mohantesh1256@telstra.com', emailencodingkey = 'UTF-8',
                                localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
        insert userObj;
        system.assert(userObj!=null);
        
        
        AccountShare acShare = new AccountShare(UserOrGroupId=userObj.id,AccountId=accObj.Id,AccountAccessLevel='Edit',
                                                OpportunityAccessLevel='Edit',CaseAccessLevel='Edit');
        //List<AccountShare> accShareList = new List<AccountShare>();
        //accShareList.add(acShare);
        insert acShare;
        
        AccountShare accManShareObj = new AccountShare(AccountAccessLevel='Edit',AccountId=accObj.Id,CaseAccessLevel='Edit',
                                                       OpportunityAccessLevel='Edit',UserOrGroupId= userObj.Id,RowCause='Manual'); 
        
        insert accManShareObj;
        system.assert(accManShareObj!=null); 
        
        
        /*AccountShare accOtherShareObj = new AccountShare(AccountAccessLevel='Edit',AccountId=accObj.Id,CaseAccessLevel='Edit',
OpportunityAccessLevel='Edit',UserOrGroupId= userObj.Id,RowCause='') ;

insert accOtherShareObj;*/
        
        
        String manualAccess = accManShareObj.AccountAccessLevel;
        // String mainAccess = accOtherShareObj.AccountAccessLevel;
        
        // System.debug('manualAccess>>>'+manualAccess+'>>>mainAccess>>>'+mainAccess);
        String actualAccess = 'none';
        
        
        Test.startTest();
        NewAccountUserSharingController newAccShrCtrlrObj = new NewAccountUserSharingController();   
        //AccountBaseShareViewController accShrVWCtrlrObj = new AccountBaseShareViewController();
        
        newAccShrCtrlrObj.uCancel(); 
        newAccShrCtrlrObj.saveShare();
        newAccShrCtrlrObj.getLevels();
        
        Test.stopTest();
    }
    static testmethod void testNewAccrShrgCtrlr2() 
    {
        List<String> CurrentList = new List<String>();   
        
        RecordType rt=[SELECT Id,Name FROM RecordType WHERE SObjectType = 'Account' AND recordType.name='Activated Account' Limit 1];
        System.debug('Record Type :::'+rt.Id); 
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                     Selling_Entity__c = 'PT. Reach Network Services Indonesia',Country__c =cntryLkupObj.Id,
                                     Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='12349',
                                     Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test',
                                     RecordTypeId =rt.Id);
        
        insert accObj;
        system.assert(accObj!=null);
        ApexPages.currentPage().getParameters().put('recordId',accObj.id);
        
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh1256@telstra.com',
                                email = 'mohantesh1256@telstra.com', emailencodingkey = 'UTF-8',
                                localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
                                alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
        insert userObj;
        system.assert(userObj!=null);
        
        List<AccountTeamMember> accountTeamMemberList = new list<AccountTeamMember>();
        accountTeamMemberList.add(New AccountTeamMember(AccountId = accObj.id, TeamMemberRole = 'Account Manager', UserId= userObj.Id));
        insert accountTeamMemberList;
        
        String strRowCause = Schema.AccountShare.RowCause.Manual;
        
        List<AccountShare> accountShareList = new List<AccountShare>();
        AccountShare accManShareObj = new AccountShare(AccountAccessLevel='Edit',AccountId=accObj.Id,CaseAccessLevel='Edit',
                                                       OpportunityAccessLevel='Edit',UserOrGroupId= userObj.Id,RowCause = strRowCause); 
        accountShareList.add(accManShareObj);
        
        AccountShare accOtherShareObj = new AccountShare(AccountAccessLevel='Edit',AccountId=accObj.Id,CaseAccessLevel='Edit',
                                                         OpportunityAccessLevel='Edit',UserOrGroupId= userObj.Id) ;
        accountShareList.add(accOtherShareObj);
        
    AccountShare acShare = new AccountShare(UserOrGroupId=userObj.id,AccountId=accObj.Id,AccountAccessLevel='Edit',
                                                OpportunityAccessLevel='Edit',CaseAccessLevel='Edit');
        accountShareList.add(acShare);
        insert accountShareList;
        system.assert(accountShareList!=null);
        Test.startTest();
        NewAccountUserSharingController newAccShrCtrlrObj = new NewAccountUserSharingController(); 
        newAccShrCtrlrObj.accountAccessLevel = 'Edit';
        newAccShrCtrlrObj.opportunityAccessLevel = 'Edit';
        newAccShrCtrlrObj.caseAccessLevel = 'Edit';
        List<String> strList = new List<String>();
        strList.add(userObj.Id);
        newAccShrCtrlrObj.CurrentList = strList;
        //AccountBaseShareViewController accShrVWCtrlrObj = new AccountBaseShareViewController();
        
        newAccShrCtrlrObj.uCancel(); 
        newAccShrCtrlrObj.saveShare();
        newAccShrCtrlrObj.getLevels();
        
        Test.stopTest();
    }
}