/**
    @author - Accenture
    @date - 23-May-2012
    @version - 1.0
    @description - This class is used to create the Action Item on clicking the button
                   "Request Credit Check" and also auto populate the values of opportunity
                   into the Action Item.
*/
public class NewCreditCheckPopulateController{
    // Variable
    Id oppId;Map<ID,ID> accountOppbillProfileMapId = new Map<ID,ID>();
    // Getter Setter Methods
    public Action_Item__c action { get; set; }
    
    // Constructor
    public NewCreditCheckPopulateController(){
        
       // action = new Action_Item__c();
        
        // query RecordType to get RecordType Object based on request Name
        //RecordType rt = [select Id,Name from RecordType where Name='Request Credit Check']; 
        oppId = System.currentPageReference().getParameters().get('oppId');
        system.debug('oppId***********'+oppId);
        
    /*    // Setting the values in Action Object
        action.Opportunity__c = oppId;        
        //action.Due_Date__c = System.Today() + 2;Expected this SLA to be defined
        action.RecordTypeId = rt.Id;
       
        action.Status__c = 'Assigned';*/
        // ai.Priority__c = 'High'; //Shailesh-Business said to remove this field see SAAS action item 00014
        
        
        // query Opportunity to get Opportunity Object based on request Opportunity ID
        Opportunity opp2= [Select Id,CurrencyIsoCode,Region__c,AccountId,Account.Account_ID__c,ownerid,Account.AccountNumber,Account.Credit_Limit__c,Account.credit_limit_expiry_date__c,Account.type,Account.Customer_Group__c,ContractTerm__c,Total_Contract_Value__c,Total_MRC__c,Total_NRC__c,Account.Business_Registration__c from Opportunity where Id=:oppId  limit 1 ];
       //PSM//Approx_Deal_Size__c,
        accountOppbillProfileMapId.put(oppId,opp2.AccountId);
        action = Util.createActionItem('Request Credit Check', accountOppbillProfileMapId, oppId, null);
        action.Subject__c = 'Request Credit Check';
        action.Region__c = opp2.Region__c;
        action.Opportunity_related_user__c =opp2.ownerid;
        //Util.assignOwnersToRegionalQueue(null, action, 'Request Credit Check');
        action.Feedback__c = '';
      //PSM//  action.Approx_Deal_Size__c = opp2.Approx_Deal_Size__c;
       
        //action.Account__c = opp2.AccountId;
        
        // Fetch the Bill Profile from the First bill Profile Row , set Billing_Currency__c, Billing_Frequency__c and Billing_Location__c
           
       
       
        /*
        a.  Customer Account Number – Auto populated in the Task page, which goes to the finance user -a
        b.  New/Existing Customer? – Auto populated in the Task page, which goes to the finance user-a
        c.  Billing location– Auto populated in the Task page, which goes to the finance user-b
        d.  Estimated contract value Approximate deal size – Auto populated in the Task page, which goes to the finance user-a
        e.  Contract duration term– Auto populated in the Task page, which goes to the finance user-a
        f.  MRC (Monthly Recurring Charges) – Auto populated in the Task page, which goes to the finance user-a
        g.  NRC (Non-Recurring Charges) – Auto populated in the Task page, which goes to the finance user-a
        h.  Billing Currency – Auto populated in the Task page, which goes to the finance user-b
        i.  Billing Frequency – Auto populated in the Task page, which goes to the finance user-b
        k.  Business Registration Number – Auto populated in the Task page, which goes to the finance user-a 
        l.  Previous Credit Check Date-a
        */
        
        // Check Previous Credit check Date
        action.Account_Number__c= opp2.Account.Account_ID__c;
        // Checking the customer is new or already existed.Commented as per new Changes.
        /* if (opp2.Account.Customer_Group__c=='Internal'){
                action.New_Existing_Customer__c='Existing';
        }
        else{
            action.New_Existing_Customer__c='New';
        }*/
        //Setting the values from Opportunity to Action Item
        action.Total_Contract_Value__c = opp2.Total_Contract_Value__c;
        action.Contract_Term__c = opp2.ContractTerm__c;
        action.MRC_Monthly_Recurring_Charges__c = opp2.Total_MRC__c;
        action.NRC_Non_Recurring_Charges__c= opp2.Total_NRC__c;
        action.Business_Registration_Number__c = opp2.Account.Business_Registration__c;
        action.Billing_Currency__c = opp2.CurrencyIsoCode;
        action.CurrencyIsoCode = opp2.CurrencyIsoCode;
        action.creditlimit__c = opp2.Account.credit_limit__c;
        action.credit_limit_expiry_date__c = opp2.Account.credit_limit_expiry_date__c;
            if(opp2.Account.type == 'Prospect')
                action.new_existing_customer__c = 'New';
            else
                action.new_existing_customer__c = 'Existing';   
        
        List<Action_Item__c> lastAction=null;
        List<Opportunity> openOpportunities=new List<Opportunity>();
        action.Total_MRC_for_all_Open_Opportunities__c=0;
        action.Total_NRC_for_all_Open_Opportunities__c=0;        
                
        try{
            
             lastAction= [Select Id,Name,Opportunity__c,Account__r.Account_ID__c,account__R.credit_limit__c,account__r.credit_limit_expiry_date__c,account__r.type,LastModifiedDate,MRC_Monthly_Recurring_Charges__c,NRC_Non_Recurring_Charges__c,Total_Estimated_MRC__c,Total_Estimated_NRC__c from Action_Item__c where Opportunity__c=:oppId  and RecordTypeId=:action.RecordTypeId order by LastModifiedDate Desc];
                          
            }catch(Exception e){
                lastAction=null;
            }
            
           
                    
            if (lastAction!=null){
                if (lastAction.size()!=0){
                    action.Previous_Credit_Check_Date__c=Date.newInstance(lastAction.get(0).LastModifiedDate.year(),lastAction.get(0).LastModifiedDate.month(),lastAction.get(0).LastModifiedDate.day());
                    action.Action_Item_ID_Previous_Credit_Check__c=lastAction[0].Name;                    
                    action.Previous_MRC__c=lastAction[0].MRC_Monthly_Recurring_Charges__c;
                    action.Previous_NRC__c=lastAction[0].NRC_Non_Recurring_Charges__c;
                    action.Previous_Estimated_MRC__c=lastAction[0].Total_Estimated_MRC__c;
                    action.Previous_Estimated_NRC__c=lastAction[0].Total_Estimated_NRC__c;
                    if(action.MRC_Monthly_Recurring_Charges__c != null && lastAction[0].MRC_Monthly_Recurring_Charges__c != null){
                        action.Difference_in_MRC__c=action.MRC_Monthly_Recurring_Charges__c-lastAction[0].MRC_Monthly_Recurring_Charges__c;
                    }
                    if(action.NRC_Non_Recurring_Charges__c != null && lastAction[0].NRC_Non_Recurring_Charges__c != null){
                        action.Difference_in_NRC__c=action.NRC_Non_Recurring_Charges__c-lastAction[0].NRC_Non_Recurring_Charges__c;
                    }
                    }                                                  
            }
                    try{
                        //modifies as part of somp added Sales_Status__c in query updated closed won and closed lost as per SOMP reqirement
                            openOpportunities=[Select Total_MRC__c,CurrencyIsoCode,Accountid,Total_NRC__c,Account_Number__c,StageName,Sales_Status__c from Opportunity where AccountId =:opp2.AccountId and (StageName!='Prove & Close' and Sales_Status__c!='Won') and Sales_Status__c!='Lost'];
                    }catch(Exception e){
                            openOpportunities=null;
                            system.debug('openOpportunities======'+openOpportunities.size());                                         
                    }
                    if (openOpportunities!=null){
                        if (openOpportunities.size()!=0){
                            //Here all getting all currencies type and form Map key as Currencycode and value as CurrencyType Object
                            List<CurrencyType> currencyTypeList = new List<CurrencyType>();
                            Map<String,CurrencyType> isoCodeCurrencyTypeObjMap = new Map<String,CurrencyType>();
                            currencyTypeList = [Select id,ConversionRate,IsCorporate,IsoCode from CurrencyType];
                            
                            for(CurrencyType currencyTypeObj : currencyTypeList){
                                isoCodeCurrencyTypeObjMap.put(currencyTypeObj.IsoCode,currencyTypeObj);
                            }
                            
                            for(Opportunity opp1:openOpportunities){
                                Double conversionRateValue = isoCodeCurrencyTypeObjMap.get(opp1.CurrencyIsoCode).ConversionRate;
                                if(opp1.Total_MRC__c != null ){
                                    
                                    action.Total_MRC_for_all_Open_Opportunities__c=action.Total_MRC_for_all_Open_Opportunities__c + ((opp1.Total_MRC__c/conversionRateValue).setscale(2));
                                   }
                                 if(opp1.Total_NRC__c != null){
                                    action.Total_NRC_for_all_Open_Opportunities__c=action.Total_NRC_for_all_Open_Opportunities__c + ((opp1.Total_NRC__c/conversionRateValue).setscale(2));
                                }                                    
                            }
                           // system.debug('Total MRC =========' + action.Total_MRC_for_all_Open_Opportunities__c);
                            //system.debug('Total NRC =========' + action.Total_NRC_for_all_Open_Opportunities__c);                    
                        }
                    }                            
           
    
    }
    
 // Action on button "Save"   
 public PageReference save(){
    
     // Before Saving the Action, Prepopulate the Bill Profile Data
   /*( if (action.Bill_Profile__c!=null){ 
        BillProfile__c billProfile= [Select Id,CurrencyIsoCode, Billing_Frequency__c, ADDRESS_2__c, ADDRESS_1__c From BillProfile__c where Id=:action.Bill_Profile__c];
        action.Billing_Frequency__c = billProfile.Billing_Frequency__c;
        action.Billing_Currency__c = billProfile.CurrencyIsoCode;
        action.Billing_Location__c = billProfile.ADDRESS_1__c+' '+billProfile.ADDRESS_2__c;
      }*/
       List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        Util.assignOwnersToRegionalQueue(usr,action,'Request Credit Check');
      system.debug('********Action Item Id***************'+ action.Id);
      try{
         insert action;
      }catch(Exception e){
        CreateApexErrorLog.InsertHandleException('ApexClass','NewCreditCheckPopulateController',e.getMessage(),'Action_Item__c',Userinfo.getName());
      }
     String sfdcURL ='/'+oppId;     
     return(new PageReference(sfdcURL));
 }
 
 // Action on button "Cancel"
 public PageReference cancel(){
     String sfdcURL ='/'+oppId;     
     return(new PageReference(sfdcURL));
 }
 
 }