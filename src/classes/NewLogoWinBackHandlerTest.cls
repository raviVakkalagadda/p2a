@isTest(SeeAllData = false)
public with sharing class NewLogoWinBackHandlerTest 
{
  static testmethod void TestNewLogoWinBackHandler1()
    { 
        
        /*List<NewLogo__c> newLogoList = new List<NewLogo__c>();       
        NewLogo__c nl;
       
        // providing the values to custom setting object
        nl = new NewLogo__c();
        nl.Name = 'Active';
        nl.Action_Item__c = 'Active';
        newLogoList.add(nl);
        
        insert newLogoList;*/
        
       /* NewLogo__c actNewLogo = new NewLogo__c();
        actNewLogo.Name = 'Test';
        actNewLogo.Action_Item__c= 'Active';
        insert actNewLogo;

        NewLogo__c actNewProp = new NewLogo__c();
        actNewProp.Name = 'Test1';
        actNewProp.Action_Item__c= 'Prospect';
        insert actNewProp;*/
        
        Map<Id,Account> oldacc = new Map<Id,Account>();
        Map<Id,Account> newacc = new Map<Id,Account>();
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
        boolean isAfter;
        boolean isUpdate;    
        Test.startTest();   
                       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;
        system.assertEquals(cntryLkupObj.Name,'HK');
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type = 'Prospect';
        accObj.Account_Type__c = 'Prospect';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34567';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';
        insert accObj;
        system.assertEquals(accObj.Industry , 'Education');
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;
        
        List<Account> acclist = new List<Account>();
        //acclist.add(accObj);
        //insert acclist;
               
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true);
        
        
        List<Opportunity> OppList = new List<Opportunity>();
        OppList.add(oppObj);
        insert OppList;
        system.assertEquals(OppList[0].Win_Loss_Reasons__c,'Product');
        
         newLogoWinBackHandler.newLogoWinBackHdlrMthd(acclist , isAfter, isUpdate, oldacc, newacc); 
         newLogoWinBackHandler.newLogoCommonMthdHdlr(acclist , isAfter, isUpdate, oldacc, newacc);
        Test.stopTest();
    }
    
    static testmethod void TestNewLogoWinBackHandler2()
    {Map<Id,Account> oldacc = new Map<Id,Account>();
        Map<Id,Account> newacc = new Map<Id,Account>();
         boolean isAfter;
        boolean isUpdate; 
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
                
        Test.startTest();   
                       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type ='Former Customer';
        accObj.Account_Type__c = 'Former Customer';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34568';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';        
        insert accObj;
        system.assertEquals(accObj.Name , 'abcd');
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;
         List<Account> acclist = new List<Account>();    
               
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', UIFlag__c = 1, Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true,
                 Estimated_MRC__c = 100.00, Estimated_NRC__c= 100.00, Product_Type__c = 'C&PS - Cloud Services');
        insert oppObj;
        system.assertEquals(oppObj.Order_Type__c,'Renewal');
        
        oppObj.Stage__c='Closed Won';
		oppObj.UIFlag__c = 5;
        update oppObj;
        
        //OppList.add(OppObj);
        //insert OppList;
        
         newLogoWinBackHandler.newLogoWinBackHdlrMthd(acclist , isAfter, isUpdate, oldacc, newacc); 
         newLogoWinBackHandler.newLogoCommonMthdHdlr(acclist , isAfter, isUpdate, oldacc, newacc);
        
        Test.stopTest();
    }
    
     /*static testmethod void AllSubscriptionTriggerTest()
  {
    Map<Id,Account> oldacc = new Map<Id,Account>();
        Map<Id,Account> newacc = new Map<Id,Account>();
        List<Account> acc = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> sub = P2A_TestFactoryCls.getOpportunitys(1,acc);
        boolean isAfter;
        boolean isUpdate;
        NewLogoWinBackHandler logo = new NewLogoWinBackHandler();
       logo.newLogoWinBackHdlrMthd(acc , isAfter, isUpdate, oldacc, newacc); 
       // logo.newLogoCommonMthdHdlr(acc , isAfter, isUpdate, oldacc, newacc); 
        }*/
        

    
}