/**
    @author - Accenture
    @date - 31-JULY-2012
    @version - 1.0
    @description - This is test class for NewRequestContractController.
*/
@isTest
private class NewRequestContractTest {
   static Opportunity opp;
    static Account acc;
    static Country_Lookup__c co;
    
    static testMethod void requestContract() {
         List<Global_Constant_Data__c> globalConstantDataObjList = new List<Global_Constant_Data__c>();
       
       Global_Constant_Data__c globalConstantDataObj;
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Status';
       globalConstantDataObj.Value__c = 'Assigned';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       globalConstantDataObj = new Global_Constant_Data__c();
       globalConstantDataObj.Name = 'Action Item Priority';
       globalConstantDataObj.Value__c = 'High';
       globalConstantDataObjList.add(globalConstantDataObj);
       
       insert globalConstantDataObjList;      
         
      // Opportunity opp = UnitTestOpportunityHelper.getValidDatesOpportunity();  
      // Account acc= getAccount();
      // opp.AccountId = acc.Id;  
      // update opp;
       opp = getOpportunity();
       system.debug('OPP**********'+opp.Id);  
       
       PageReference pageRef1 = new PageReference('/apex/NewRequestContract?oppId='+ opp.Id);
       Test.setCurrentPage(pageRef1);
                  
      NewRequestContractController rq = new NewRequestContractController();
            rq.cancel();
      system.assertEquals(rq.save().getUrl(),'/'+opp.Id);
    }
    
    private static Opportunity getOpportunity(){
        
        if(opp == null){    
            opp = new Opportunity();
            opp.Name = 'TestOpp';
            acc = getAccount();
            opp.AccountId = acc.Id; 
            system.debug('accountIdopp*****'+ opp.AccountId);
          //  opp.Approx_Deal_Size__c =10.00; 
          opp.Estimated_MRC__c=8;
            opp.Estimated_NRC__c=2;
            opp.ContractTerm__c='1';
            //opp.Close_Date_Month__c = '4/7/2012';
            opp.CloseDate = system.today();
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.Opportunity_Type__c = 'Simple';
        
            insert opp;
        }
        return opp;
        
    }
    private static Account getAccount(){
            if(acc == null){
            
                acc = new Account();
                acc.Name = 'Testacc';
                acc.Customer_Type__c = 'MNC';
                acc.CurrencyIsoCode ='USD';
                acc.Activated__c = true;
                acc.Selling_Entity__c ='Telstra Limited';
                co = getCountry();
                acc.Country__c = co.Id;
                acc.Account_ID__c = '9090';
                acc.Customer_Legal_Entity_Name__c='Test';
                           
                insert acc;
            }
            system.debug('accId*****'+ acc.Id); 
            return acc;
         }             
   private static Country_Lookup__c getCountry(){
       if(co == null){
       
            co = new Country_Lookup__c();
            co.Name = 'INDIA';
            co.Country_Code__c ='IN';
        
            insert co;
       }
            return co;
         }
 
    
     
    
}