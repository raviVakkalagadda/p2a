/**    @author - Accenture    
       @date - 15-12-2013    
       @version - 1.0    
       @description - This class retrives all OLI's corresponding to ParentOrder
    and pass it to TreeView component.
**/

public class OLITreeComponentController {
    

    public OLITreeComponentController(ApexPages.StandardController controller) {    

    }

    public OLITreeComponentController.TreeModel pac;
    public List<TreeModel> treeLst {get;set;}
    public integer count {get;set;}
      Id orderId = System.currentPageReference().getParameters().get('id');
      //system.debug('Order id +++++++++++++++++++++++++ ' + orderId);
      //system.debug('Opportunity Id **********' + oppId);
       public OLITreeComponentController(){
               // Creating TreeModel List Object
        treeLst = new List<TreeModel>();
  
        // query Order Line Item fields
        List<String> olilistid = new List<String>();
        //List<Order_Line_Item__c> oliLst = [SELECT ParentCPQItem__c,Id,CPQItem__c,Product__c,Line_Item_Status__c,Master_Service_ID__c,Primary_Service_ID__c,Resource__c,Customer_Required_Date__c,Name,Product_Name__c,OrderType__c,Customer_Address__c,ParentOrder__c,Site_A__r.City_name__c,Site_B__r.City_name__c from Order_Line_Item__c where ParentOrder__c =: orderId order by CPQItem__c];
      
       // system.debug('OLILIST++++'+oliLst.size());
        //This will hold new sorted array list
        List<Order_Line_Item__c> sortedoliLst = new List<Order_Line_Item__c>(); 
        //Call new sorting wrapper class and sort this list view array
       // List<SortingWrapper> treesortlist = new List<SortingWrapper>();
       //for(Order_Line_Item__c soli: oliLst){
        //    treesortlist.add(new SortingWrapper(new TreeSorting(soli.CPQItem__c,soli)));
       // }
        //treesortlist.sort();
        List<OPPOLISortingWrapper> OppolWrap = new List<OPPOLISortingWrapper>();
        for(Order_Line_Item__c oli : [SELECT ParentCPQItem__c,Id,CPQItem__c,Product__c,Line_Item_Status__c,Master_Service_ID__c,Primary_Service_ID__c,Resource__c,Customer_Required_Date__c,Name,Product_Name__c,OrderType__c,Customer_Address__c,ParentOrder__c,Site_A__r.City_name__c,Site_B__r.City_name__c from Order_Line_Item__c where ParentOrder__c =: orderId order by CPQItem__c]){
            OPPOLITreeSorting sor = new OPPOLITreeSorting(oli.CPQItem__c, oli);
            system.debug('sor111111++'+OppolWrap);
            OPPOLISortingWrapper wrap = new OPPOLISortingWrapper(sor);
            system.debug('wrap11111++'+OppolWrap);
            OppolWrap.add(wrap); 
            system.debug('OppolWrap++'+OppolWrap);
        }
        OppolWrap.sort();
         system.debug('sorting+++'+OppolWrap);
        //for(SortingWrapper sw: treesortlist){
           // Order_Line_Item__c newoli = new Order_Line_Item__c();
            //TreeSorting p = sw.pac;
            //System.debug(' After Sort List '+ p.Position_Edit);
            //Populate all the 14 attributes here required for Tree view
            //newoli = p.oli;
            //newoli.CPQItem__c =  p.Position_Edit; 
            /*newoli.ParentCPQItem__c = p.oli.ParentCPQItem__c;
            newoli.ParentOrder__c = p.oli.ParentOrder__c;
            newoli.Line_Item_Status__c = p.oli.Line_Item_Status__c;
            newoli.OrderType__c = p.oli.OrderType__c;
            newoli.Site_A__c = p.oli.Site_A__c;
            newoli.Site_A__c = p.oli.Site_B__c;
            newoli.Master_Service_ID__c = p.oli.Master_Service_ID__c;
            newoli.Primary_Service_ID__c = p.oli.Primary_Service_ID__c;
            newoli.Customer_Required_Date__c = p.oli.Customer_Required_Date__c;*/
            //newoli.Name = p.oli.Name;
            //newoli.Id = p.oli.Id;
            //newoli.Product_Name__c = p.oli.Product_Name__c;
            //newoli.Customer_Address__c = p.oli.Customer_Address__c;
            
            //sortedoliLst.add(newoli);
        //}

        // Map to store OrderLineItemClass object 
        Map<String, TreeModel> oliLevlMap = new Map<String, TreeModel>();
        boolean productFlag;
        //for(Order_Line_Item__c oli : sortedoliLst){
       
        for(OPPOLISortingWrapper wrap: OppolWrap ){
            OPPOLITreeSorting  sor = wrap.pac;
            system.debug('wrap+++for++'+wrap);
            Order_Line_Item__c oli = sor.oOLI;
            productFlag = true;
            TreeModel ordrLinItm = new TreeModel();
            ordrLinItm.cPQItem = oli.CPQItem__c;
            String orderId = oli.ParentOrder__c;
           
            //As customer address is truncated to 50 characters
            Map<Id,String> mapOLICPQItem = new Map<Id,String>();
             
           /* for(Order_Line_Item__c olist:[Select Id,CPQItem__c from Order_Line_Item__c where ParentOrder__c =: orderId Order by CPQItem__c]){
              mapOLICPQItem.put(olist.id,olist.CPQItem__c);
            }
            ordrLinItm.oLICPQItems = '';
            for(String oliMapvalues : mapOLICPQItem.values()){
                ordrLinItm.oLICPQItems = +oliMapvalues+','+ordrLinItm.oLICPQItems;
                system.debug('allCPQItems+++++'+ordrLinItm.oLICPQItems);
            } */
                      if(oli.Customer_Address__c != null)
               if(oli.Customer_Address__c.length()>50)
                 ordrLinItm.customerAddress = oli.Customer_Address__c.substring(0,50);
                
                else
                
                 ordrLinItm.customerAddress = oli.Customer_Address__c;
                
                
            if(oli.Site_A__r.City_name__c != null){
                ordrLinItm.cityName = oli.Site_A__r.City_name__c;
            }else{
                ordrLinItm.cityName = oli.Site_B__r.City_name__c;
            }
            ordrLinItm.LineItemDetails = oli;
            if(oli.ParentCPQItem__c!=null && oli.ParentCPQItem__c!=''){
                productFlag = false;
                if(oliLevlMap.containsKey(oli.ParentCPQItem__c)){
                    TreeModel parntOli = oliLevlMap.get(oli.ParentCPQItem__c);
                    List<TreeModel> parntOlilst = parntOli.children ;
                    parntOlilst.add(ordrLinItm);
                    parntOli.isFile = false;
                }
            }
               oliLevlMap.put(oli.CPQItem__c,ordrLinItm) ;
               if(productFlag)
                treeLst.add(ordrLinItm);
           
          //  treeLst.sort();
            
     
            System.debug('zzzzzzzzzzzzzzzz'+ treeLst); 
            System.debug('zzzzzzzzzzzzzzzz111 :'+ treeLst.size()); 
    }
        /* for(Order_Line_Item__c ollli : oliLst){
         TreeModel ordrLinItms = new TreeModel();
             ordrLinItms.oLICPQItems = ollli.CPQItem__c;
             system.debug('CPQSSSSSSS+++'+ordrLinItms.oLICPQItems);
             treeLst.add(ordrLinItms);
       olilistid.add(ordrLinItm.oLICPQItems);
       }  */
    }
  
    
  public class TreeModel {
        
        public TreeModel pac;
        public Order_Line_Item__c LineItemDetails {get;set;}
        public boolean isFile {get;set;}
        public String cPQItem {get;set;}
        public String cityName {get;set;}
        public String customerAddress {get;set;}
        public String oLICPQItems {get;set;}
        public List<TreeModel> children {get;set;}
           public TreeModel(){
            children  = new List<TreeModel>();
            isFile = true;
        }
        
     public Integer compareTo(Object compareTo) {

            TreeModel compareToPac = (TreeModel)compareTo;

            Integer returnValue = 0;

            String cpqItem1 = (pac.cPQItem).replace('.','A');
            String cpqItem2 = (compareToPac.cPQItem).replace('.','A');

            if (cpqItem1 > cpqItem2) {
            returnValue = 1;
            } else if (cpqItem1 < cpqItem2) {
            returnValue = -1;
            }

            return returnValue;
        } 
    
    }
}