@isTest
private class OpportunityEditOverrideTest {

    static testMethod void OpportunityEditOverrideTestMethod() {
    
        List<Account> accList = new List<Account>();
        List<Opportunity> oppList = new List<Opportunity>();
        accList=P2A_TestFactoryCls.getAccounts(2);
        oppList=P2A_TestFactoryCls.getOpportunitys(1, accList);
    
        //String currentPageId='a3R0k0000004C9x';
        PageReference pageRefs = Page.BlockEditOpportunityButton;
        Test.setCurrentPage(pageRefs);
        Id currentRecordId = ApexPages.currentPage().getParameters().get('Id');
        //Id currentRecordId=Id.valueOf(currentPageId);

     /*   String sObjName =currentRecordId.getSObjectType().getDescribe().getName();
        Map<String, Schema.SObjectType> globalDescMap=Schema.getGlobalDescribe();
        Schema.SObjectType targetType=globalDescMap.get(sObjName);
        sObject sObj=targetType.newSObject();*/

        
        
        String sObjName1 = String.valueOf(oppList[0].Id);
        system.assert(oppList[0].id!=null);
        PageReference pageRef = new PageReference('https://telstra--p2aval.cs57.my.salesforce.com/a3R0k0000004C9x');
        System.currentPageReference().getHeaders().put('User-Agent','iPhone');
        pageRef.getHeaders().put('User-Agent','iPhone');
        ApexPages.StandardController controller = new ApexPages.StandardController(oppList[0]);
        OpportunityEditOverride obj = new OpportunityEditOverride(controller);
        
        System.currentPageReference().getHeaders().put('User-Agent','iPad');
        pageRef.getHeaders().put('User-Agent','iPad');
        OpportunityEditOverride obj1 = new OpportunityEditOverride(controller);
        
        System.currentPageReference().getHeaders().put('User-Agent','nexus');
        pageRef.getHeaders().put('User-Agent','nexus');
        OpportunityEditOverride obj2 = new OpportunityEditOverride(controller);
        
        pageRef=obj.redirect();
        //obj.redirect();
        OpportunityEditOverride.getRedirectionForMobile(sObjName1);
        }
}