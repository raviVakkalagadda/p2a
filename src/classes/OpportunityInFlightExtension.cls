/** Responsible for controlling the in flight functinality **/
public with sharing class OpportunityInFlightExtension{

    public Opportunity opp{get; set;}
    public List<OrderWrapper> orderWrappers{get; set;}
    public Boolean pageError{get; set;}

    private ApexPages.StandardController controller;
    private List<String> additionalFields = new List<String>{'Name'};
    private Set<String> allowedProfiles = new Set<String>{'TI Account Manager', 'TI Sales Admin', 'TI Sales Manager', 'System Administrator', 'Engineering', 'TI Order Desk'};

    public OpportunityInFlightExtension(ApexPages.StandardController stdController) {
    if(!Test.isRunningTest()){ /** Added for test class coverage **/
        stdController.addFields(additionalFields);
        }
        controller = stdController;

        this.opp = (Opportunity)stdController.getRecord();

        pageError = false;
    }

    public PageReference loadOrders(){
        Profile userProfile = [Select Name from Profile where Id = :Userinfo.getProfileid()];
        if(allowedProfiles.contains(userProfile.Name)){
            List<csord__Order__c> orders = [select Id, Name, Status__c, Subscription__r.Count_of_B_T_Services__c, Subscription__r.Count_of_services__c, (select Id, Name from csord__Services__r) from csord__Order__c where csordtelcoa__Opportunity__c = :opp.Id and (csord__Status__c != 'Cancelled' or csord__Status2__c != 'Cancelled' or Status__c != 'Cancelled')];

            if(orders.size() > 0){
                orderWrappers = new List<OrderWrapper>();
                for(csord__Order__c order : orders){
                    if(order.Status__c != 'Completed' && order.Subscription__r.Count_of_B_T_Services__c == order.Subscription__r.Count_of_services__c){
                        orderWrappers.add(new OrderWrapper(order));
                    }
                    else{
                        addError('Order(s) have already been provisioned, you can not edit this opportunity');
                    } 
                }
            }
            else{
                addError('No Order is created for this opportunity.  Please create order first.');
            }
        }
        else{
            addError('You do not have permission to perform this action');
        }

        return null;
    }

    @testvisible
    private void addError(String message){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, message));
        pageError = true;
    }

    public PageReference selectChangeOrders(){
        TriggerFlags.NoOpportunityTriggers=true;
        //TriggerFlags.NoOrderTriggers=true;
        List<csord__Order__c> ordersSelected = new List<csord__Order__c>();
        for(OrderWrapper ow : orderWrappers){
            if(ow.selected){
                ordersSelected.add(ow.order);
            }
        }
        if(ordersSelected.size() == 0){   
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'No Order has been selected.'));
                return null;       
        } else {
            opp.StageName = 'InFlight';
            opp.Sales_Status__c = 'Open';
            opp.Stage__c = 'InFlight';      
            opp.Revise_Opportunity_Flag__c = True; /** Defect Fix: 14507 **/
            opp.csordtelcoa__Order_Generation_Batch_Job_Id__c = '';
        
            /** Clone the basket currently synced to the Opportunity **/
            cscfga__Product_Basket__c basket = InFlightManager.cloneSyncedBasket(opp, ordersSelected);

            /** Cancel existing services **/
            InFlightManager.cancelOrders(ordersSelected);

            /** Replace screen flow of product configuration in the cloned basket for remaining services **/
            InFlightManager.setScreenFlows(null);
            
            update opp;

            /** Redirect the user to the cloned basket **/
            return new PageReference('/apex/InFlightChangeProgressBar?Id=' +basket.Id);
            }
    }

    public PageReference returnToOpportunity(){
        return controller.view().setRedirect(true);
    }

    public class OrderWrapper{
        public Boolean selected {get; set;}
        public csord__Order__c order {get; set;}

        public OrderWrapper(csord__Order__c order){
            this.order = order;
        }
    }
}