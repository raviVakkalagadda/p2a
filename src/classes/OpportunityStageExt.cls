/**    @author - Accenture    
       @date - 11-07-2014    
       @version - 1.0    
       @description - This class is used as an Extension to update Stage,Sales Status and notify user with Message.  
**/
public class OpportunityStageExt{
    public String oppID{get;set;}
    public String opportunityCurrentStage{get;set;}
    public String opportunityNextStage;
    public String redirectUrl{get; Set;}
    public String salesStatus{get;set;}
    public String validationMessage{get;set;}
    public Opportunity opportunity1{get;set;}
    public Boolean winlossReason{get;set;}
        public OpportunityStageExt(ApexPages.StandardController controller) {
        this.oppID = Apexpages.currentPage().getParameters().get('OpportunityId');
        if(this.oppID==null){this.oppID = Apexpages.currentPage().getParameters().get('id');}
        this.salesStatus = Apexpages.currentPage().getParameters().get('salesStatus');
        this.opportunityCurrentStage = Apexpages.currentPage().getParameters().get('OpportunityCurrentStage');
        this.opportunityNextStage = Apexpages.currentPage().getParameters().get('opportunityNextStage');
        this.redirectUrl=Apexpages.currentPage().getParameters().get('redirectUrl');
        this.salesStatus=Apexpages.currentPage().getParameters().get('salesStatus');
        this.validationMessage=Apexpages.currentPage().getParameters().get('validationMessage');
        system.debug('=======this.salesstatus===='+this.salesStatus);
        system.debug('=======this.validationMessage===='+this.validationMessage);
        opportunity1=new Opportunity();

         try{this.winlossReason = ([select Id, Win_Loss_Reasons__c from Opportunity where Id = :this.oppID limit 1]).Win_Loss_Reasons__c==null?false:true;}catch(Exception e){
			          
			 system.debug('Query failed Class OpportunityStageExt Line 28: ' +e);}

    
    }

    public PageReference saveStage(){
        try{
            redirectUrl = 'Exception';
            validationMessage = '';        
            
        OpportunityManager oppManager = new OpportunityManager();
        Opportunity opportunity=new Opportunity();
        opportunity =oppManager.updateStage(oppID, salesStatus, opportunityCurrentStage, opportunityNextStage);
        opportunity.Win_Loss_Reasons__c=this.salesStatus=='Lost' && opportunity.Win_Loss_Reasons__c==null?opportunity1.Win_Loss_Reasons__c:(this.salesStatus=='Won' || this.salesStatus=='Open')&& opportunity.Win_Loss_Reasons__c==null?opportunity1.Win_Reasons__c:opportunity.Win_Loss_Reasons__c;
        opportunity.Reason__c=opportunity1.Reason__c;
       if( opportunityCurrentStage=='Propose' || salesStatus=='Lost'){
        update opportunity;
        }
            redirectUrl = null;
        }
        catch(system.DMLException e){ 
		
            redirectUrl = 'Exception';
            validationMessage = '';
            
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here 
                validationMessage=validationMessage+e.getDmlMessage(i)+'';
                if(validationMessage.contains('Win/Loss')){
                redirectUrl = 'Exception';
                }
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, validationMessage));
                System.debug(e.getDmlMessage(i)); 
            }
            ErrorHandlerException.ExecutingClassName='OpportunityStageExt:saveStage';         
            ErrorHandlerException.sendException(e); 
            System.debug('validationMessage'+validationMessage);
        }
        catch(system.Exception ex){
            redirectUrl = 'Exception';
            validationMessage = '';
            System.debug('========Exception occured===' + ex.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
			ErrorHandlerException.ExecutingClassName='OpportunityStageExt:saveStage';         
            ErrorHandlerException.sendException(ex);
        }

        if(redirectUrl == null && ((this.opportunityCurrentStage!='Propose' && this.salesStatus!='Lost') || this.winlossReason)){
            return new pagereference('/' + oppID);
        }
        else{
            return null;
        }
    }

}