@isTest(SeeAllData = false)
public class OpportunityTriggerHandler_Test 
{
    static testmethod void TestOppTrigHdlrMethod()
    {
        
        Test.startTest();
        OpportunityTriggerHandler oppHdlrDtls = new OpportunityTriggerHandler();
        // VendorQuoteDetail__c venQuote = new VendorQuoteDetail__c()
        Map<Id,Opportunity> oppOldMap = new Map<Id,Opportunity>();
        Map<Id,Opportunity> oppNewMap = new Map<Id,Opportunity>();
        
        
        oppHdlrDtls.isDisabled();
        oppHdlrDtls.getName();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Opportunity> oppNewList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        for(Integer i = 0; i < oppNewList.size(); i++)
        {    
            oppNewList.get(i).Pre_Contract_Provisioning_Required__c = 'Yes';
            oppNewList.get(i).Sales_Status__c = 'Won';
            oppNewList.get(i).Win_Loss_Reasons__c = 'Cancelled'; 
            oppNewList.get(i).Quote_Simplification__c = true;
            oppNewList.get(i).QuoteStatus__c = 'Accepted';
        }    
        // oppNewList.get(0).StageName = 'Closed Won';
        update oppNewList;
        System.assert(oppNewList!=null); 
        List<Case> caseList = P2A_TestFactoryCls.getcase(1, accList);
        
        
        List<cscfga__Product_Basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        prodBaskList[0].Max_Contract_Term__c = 12;
        
        prodBaskList.get(0).csordtelcoa__Synchronised_with_Opportunity__c = false;
        
        prodBaskList.get(0).cscfga__Opportunity__c = oppNewList.get(0).Id;
        
        //below code added by ritesh
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='Test master IPVPN';
        insert pd;
         System.assert(pd!=null);
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        System.assert(pd1!=null);
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c();
        pd2.Name = 'VLANGroup';
        pd2.cscfga__Description__c ='Test VLANGroup';
        insert pd2;
        System.assert(pd2!=null);
        cscfga__Product_Definition__c pd3 = new cscfga__Product_Definition__c();
        pd3.Name = 'IPVPN';
        pd3.cscfga__Description__c ='Test IPVPN';
        insert pd3;
        System.assert(pd3!=null);
        cscfga__Product_Definition__c pd4 = new cscfga__Product_Definition__c();
        pd4.Name = 'IPVPN';
        pd4.cscfga__Description__c ='Test SMA Gateway';
        insert pd4;
        System.assert(pd4!=null);
        cscfga__Product_Definition__c pd5 = new cscfga__Product_Definition__c();
        pd5.Name = 'VPLS Transparent';
        pd5.cscfga__Description__c ='Test VPLS Transparent';
        insert pd5;
        System.assert(pd5!=null);
        cscfga__Product_Definition__c pd6 = new cscfga__Product_Definition__c();
        pd6.Name = 'VPLS VLAN';
        pd6.cscfga__Description__c ='Test VPLS VLAN';
        insert pd6; 
        System.assert(pd6!=null);
        //for offers
        cscfga__Configuration_Offer__c pf1 = new cscfga__Configuration_Offer__c();
        pf1.Name = 'IPVPN';     
        insert pf1;
        System.assert(pf1!=null);
        cscfga__Configuration_Offer__c pf2 = new cscfga__Configuration_Offer__c();
        pf2.Name = 'VPLS Transparent';      
        insert pf2; 
        System.assert(pf2!=null);
        cscfga__Configuration_Offer__c pf3 = new cscfga__Configuration_Offer__c();
        pf3.Name = 'VPLS VLAN';     
        insert pf3; 
        System.assert(pf3!=null);
        
        //for offers logic ends     
        
        
        
        //insert custom setting records
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'Master_IPVPN_Service_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        System.assert(pdIdCustomSetting!=null);
        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'Master_VPLS_Service_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1;
         System.assert(pdIdCustomSetting1!=null);
        Product_Definition_Id__c pdIdCustomSetting2 = new Product_Definition_Id__c();
        pdIdCustomSetting2.Name = 'VLANGroup_Definition_Id';
        pdIdCustomSetting2.Product_Id__c =pd2.Id;
        insert pdIdCustomSetting2;
         System.assert(pdIdCustomSetting2!=null);
        Product_Definition_Id__c pdIdCustomSetting3 = new Product_Definition_Id__c();
        pdIdCustomSetting3.Name = 'IPVPN_Port_Definition_Id';
        pdIdCustomSetting3.Product_Id__c =pd3.Id;
        insert pdIdCustomSetting3;
         System.assert(pdIdCustomSetting3!=null);
        Product_Definition_Id__c pdIdCustomSetting4 = new Product_Definition_Id__c();
        pdIdCustomSetting4.Name = 'SMA_Gateway_Definition_Id';
        pdIdCustomSetting4.Product_Id__c =pd4.Id;
        insert pdIdCustomSetting4;
         System.assert(pdIdCustomSetting4!=null);
        Product_Definition_Id__c pdIdCustomSetting5 = new Product_Definition_Id__c();
        pdIdCustomSetting5.Name = 'VPLS_Transparent_Definition_Id';
        pdIdCustomSetting5.Product_Id__c =pd5.Id;
        insert pdIdCustomSetting5;
         System.assert(pdIdCustomSetting5!=null);
        Product_Definition_Id__c pdIdCustomSetting6 = new Product_Definition_Id__c();
        pdIdCustomSetting6.Name = 'VPLS_VLAN_Port_Definition_Id';
        pdIdCustomSetting6.Product_Id__c =pd6.Id;
        insert pdIdCustomSetting6;  
         System.assert(pdIdCustomSetting6!=null);
        // adding offers
        Offer_Id__c pdIdOfferSetting1 = new Offer_Id__c();
        pdIdOfferSetting1.Name = 'Master_IPVPN_Service_Offer_Id';
        pdIdOfferSetting1.Offer_Id__c =pf1.Id;
        insert pdIdOfferSetting1;
        System.assert(pdIdOfferSetting1!=null);
        Offer_Id__c pdIdOfferSetting2 = new Offer_Id__c();
        pdIdOfferSetting2.Name = 'Master_VPLS_Transparent_Offer_Id';
        pdIdOfferSetting2.Offer_Id__c =pf1.Id;
        insert pdIdOfferSetting2;
        System.assert(pdIdOfferSetting2!=null);
        Offer_Id__c pdIdOfferSetting3 = new Offer_Id__c();
        pdIdOfferSetting3.Name = 'Master_VPLS_VLAN_Offer_Id';
        pdIdOfferSetting3.Offer_Id__c =pf3.Id;
        insert pdIdOfferSetting3;
        //end of adding offers      
        System.assert(pdIdOfferSetting3!=null);
        
        //insert product configuration record for Master IPVPN Service
        List<cscfga__Product_Configuration__c> pclist= new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c =  prodBaskList.get(0).Id,cscfga__Configuration_Status__c='Incomplete'),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c =  prodBaskList.get(0).Id,Solution_Type__c='IPVPN',cscfga__Configuration_Status__c='Incomplete'),
                 new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c =  prodBaskList.get(0).Id,cscfga__Configuration_Status__c='Incomplete'),
                 new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c =  prodBaskList.get(0).Id,Solution_Type__c='VLAN Mode',cscfga__Configuration_Status__c='Incomplete'),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c =  prodBaskList.get(0).Id,Solution_Type__c='Transparent Mode',cscfga__Configuration_Status__c='Incomplete'),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd2.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='VlanGroup1 from ' + prodBaskList.get(0).Name),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd2.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='VlanGroup2 from ' + prodBaskList.get(0).Name),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd3.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='IPVPN from ' + prodBaskList.get(0).Name),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd4.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='SMA Gateway from ' + prodBaskList.get(0).Name),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd5.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='VPLS Transparent from ' + prodBaskList.get(0).Name),
                new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd6.id,cscfga__Contract_Term__c = 12,cscfga__Product_Basket__c = prodBaskList.get(0).Id,Name='VPLS VLAN from ' + prodBaskList.get(0).Name)
                };
                    insert pclist;
        //ritesh code ends
        System.assert(pclist!=null);
        update prodBaskList;
        for(Opportunity oppObj : oppList){
            oppOldMap.put(oppObj.Id, oppObj);
            
        } 
        
        for(Opportunity oppObj : oppNewList) {
            oppNewMap.put(oppObj.Id, oppObj);
            
        }
        
        oppHdlrDtls.beforeInsert(oppList);
        oppHdlrDtls.afterInsert(oppList, oppNewMap);
        oppHdlrDtls.beforeUpdate(oppList, oppNewMap, oppOldMap);
        oppHdlrDtls.afterUpdate(oppList, oppNewMap, oppOldMap);
        oppHdlrDtls.beforeDelete(oppOldMap);
        oppHdlrDtls.afterDelete(oppOldMap);
        //oppHdlrDtls.afterUndelete(oppOldMap);
        
        oppHdlrDtls.runBeforeUpdate(oppList, oppOldMap);
        oppHdlrDtls.runAfterUpdate(oppList, oppOldMap);  
        
        System.assertEquals(oppNewList.get(0).Pre_Contract_Provisioning_Required__c,  'Yes');        
        Test.stopTest();
    }
    @istest(seealldata = false)
    
    Public static void OpportunityTriggerHandlerTestMetd(){
        
        //Test.starttest();
        List<Opportunity> Opp_List = new List<Opportunity>();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = new list<Opportunity>(); //P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Opp_Product_Type__c> Optlist = new list<Opp_Product_Type__c>();
        Opportunity o   = new Opportunity();                
        o.AccountId     =  Acclist[0].id;
        o.name          = 'Test Opp ';
        o.stagename     = 'Closed Won';
        o.CloseDate     = system.today() + 10;
        o.Pre_Contract_Provisioning_Required__c = 'No';
        o.QuoteStatus__c = 'Approved';
        o.Total_NRC__c = 200;
        o.Total_MRC__c = 300;
        O.Quotestatus__c = 'Approved';
        O.Order_Type__c = 'Renewal';
        O.currencyisocode = 'INR' ;
        OppList.add(o);          
        insert OppList;
        
       System.assert(OppList!=null);

        Map<id, Opportunity> Oopmap = new Map<id, Opportunity>();
        
        
        
        Map<Id,Opportunity>oppMap=new Map<Id,Opportunity>([Select id,Quotestatus__c,name,Pre_Contract_Provisioning_Required__c from Opportunity where id =:OppList[0].id]);
        
        Oopmap.put(OppList[0].id, opplist[0]);
        
        
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Contact> contactList = P2A_TestFactoryCls.getContact(1,accList);
        List<Site__c> SitesList = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        List<BillProfile__c> bpslist = P2A_TestFactoryCls.getBPs(1, AccList, SitesList, contactList);
        
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        Products[0].Max_Contract_Term__c = 12;
        update Products[0]; 
        
        Pricebook2 pb = new Pricebook2();
        pb.name = 'Text book123 ';
        insert pb;   
        System.assert(pb!=null); 
        Resource__c res = new Resource__c();
        res.Name = 'Test resource';
        //insert res;
        CostCentre__c c = new CostCentre__c();
        c.Cost_Centre_Code__c = 'Test code';
        c.BillProfile__c = bpslist[0].id;
        c.Name = 'TestBillprofile';
        insert c;
         System.assert(c!=null); 
        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd; 
     System.assert(newProd!=null); 
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id = newProd.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        pbe.CurrencyIsocode = 'INR';
        insert pbe;
        System.assert(pbe!=null); 
        OpportunityLineItem Oppitem = new OpportunityLineItem();
        Oppitem.Opportunityid = Opplist[0].id;
        Oppitem.Resource__c = res.id;
        Oppitem.OrderType__c = 'New';
        Oppitem.Is_Prior_Parent__c = true;
        Oppitem.BillProfileId__c = bpslist[0].id;
        Oppitem.CostCentreId__c = c.id;
        Oppitem.CPQItem__c = 'Test CPQ';
        Oppitem.Display_line_item_on_Invoice__c = true;
        Oppitem.Zero_Charge_MRC_Flag__c = true ;
        Oppitem.Zero_Charge_NRC_Flag__c = true;
        Oppitem.Zero_Charge_ETC_Flag__c = true;
        Oppitem.is_bundle__c = true;
        Oppitem.Parent_Bundle_Flag__c = true;
        Oppitem.Parent_Charge_ID__c = null;
        Oppitem.Bundle_Label_Name__c = 'Test Bundle';
        Oppitem.NetNRCPrice__c = 300;
        Oppitem.Reported_New_SOV__c = 300;
        Oppitem.Reported_Renewal_SOV__c = 400;
        Oppitem.Contract_Value__c = 500;
        Oppitem.SOV_Type_of_Order__c = 'Test Order';
        Oppitem.Contract_Expiry_Date__c = System.today();
        oppitem.Quantity = 12;
       
        //oppitem.TotalPrice = 12900;
       // oppitem.Pricebookentryid = pbEntry.id;
       oppitem.Pricebookentryid = pbe.id;
       oppitem.unitprice = 490;
        insert Oppitem;
        System.assert(Oppitem!=null); 
        Opp_Product_Type__c opt = new Opp_Product_Type__c();
        Opt.Opportunity__c = Opplist[0].id;
        opt.MRC__c = 300;
        opt.NRC__c = 200;
        opt.Reported_New_SOV__c = 500;
        opt.Reported_Renewal_SOV__c = 200;
        opt.Product_Type_Name__c = 'PEN';
        optlist.add(opt);
        insert optlist;
        System.assert(optlist!=null); 
        OpportunityTriggerHandler op = new OpportunityTriggerHandler();
        //op.updateSOVToOppLineItem(opplist,oppMap,oppMap);
        
        Opportunity opp   = new Opportunity();                
        opp.AccountId     =  Acclist[0].id;
        opp.name          = 'Test Opp ';
        opp.stagename     = 'Identify & Define';
        opp.CloseDate     = system.today() + 10;
        opp.Pre_Contract_Provisioning_Required__c = 'No';
        opp.QuoteStatus__c = 'Draft';
        opp.Total_NRC__c = 0;
        opp.Total_MRC__c = 0;
        Opp.Quotestatus__c = 'Accepted'; 
        Opp_List.add(opp);          
        insert Opp_List;
         System.assert(Opp_List!=null); 
        List<Opp_Product_Type__c> optlists = new List<Opp_Product_Type__c>();
        
        Opp_Product_Type__c opts = new Opp_Product_Type__c();
        Opts.Opportunity__c = Opp_list[0].id;
        opts.MRC__c = 300;
        opts.NRC__c = 200;
        opts.Reported_New_SOV__c = 500;
        opts.Reported_Renewal_SOV__c = 200;
        opts.Product_Type_Name__c = 'PEN';
        optlists.add(opts);
        insert optlists;
        System.assert(optlists!=null); 
        Map<id,Opportunity> Oppmapnew = new Map<id,Opportunity>();
        Oppmapnew.put(Opp_List[0].id, opp_list[0]);
        
        OpportunityTriggerHandler otp = new OpportunityTriggerHandler();
        otp.updateSOVToOppLineItem(opp_list,Oppmapnew,Oppmapnew);
        
        List<Opportunity> OList = P2A_TestFactoryCls.getOpportunitys(1 ,AccList);
        olist[0].stagename = 'closed won';
        update olist;
         System.assert(olist!=null); 
        List<Opp_Product_Type__c> opt_lists = new List<Opp_Product_Type__c>();
        
        Opp_Product_Type__c optes = new Opp_Product_Type__c();
        Optes.Opportunity__c = OList[0].id;
        optes.MRC__c = 300;
        optes.NRC__c = 200;
        optes.Reported_New_SOV__c = 500;
        optes.Reported_Renewal_SOV__c = 200;
        optes.Product_Type_Name__c = 'PEN';
        opt_lists.add(optes);
        insert opt_lists;
        
         System.assert(opt_lists!=null); 
        Map<id,Opportunity> Oppmap1 = new Map<id,Opportunity>();
        Oppmap1.put(OList[0].id, OList[0]);
        
        Test.starttest();
        OpportunityTriggerHandler otps = new OpportunityTriggerHandler();
        otp.updateSOVToOppLineItem(OList,Oppmap1,Oppmap1);
        otps.beforeUpdate(OList,Oppmap1,Oppmap1);
        otps.afterUpdate(OList,Oppmap1,Oppmap1);
        otps.runBeforeUpdate(OList,Oppmap1);
        otps.updateTotalReportedSOV(OList ,Oppmap1 ,Oppmap1);
        otps.runAfterUpdate(OList,Oppmap1);
        otps.oppmulticurrency(Oppmap1);
        otps.updateInflightOrder(OList,Oppmap1,Oppmap1);
        otps.refreshMainItem(OList,Oopmap ,Oppmap1);

        Test.stoptest(); 
    }
    
    Public static testmethod void test1(){
    
        Test.starttest();
        List<Opportunity> Opp_List = new List<Opportunity>();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList1 = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Opportunity> oppList = new List<Opportunity>();
        List<Opp_Product_Type__c> Optlist = new list<Opp_Product_Type__c>();
         ID splittypeidactive = [select DeveloperName,isactive,id from OpportunitySplitType where isactive=:true][0].id;
         
        OpportunitySplit oppSplits = new OpportunitySplit(splitOwnerid = userinfo.getuserid() ,
                    Total_Product_SOV__c=25.35, Team_Role__c='tds',SplitTypeId = splittypeidactive, 
                    SplitPercentage_Product_SOV__c=54 , OpportunityID = oppList1[0].id);
        
        //System.assertEquals(true,oppSplits.OppSplitType__c);
        Set<ID> splIds = new Set<ID>();
        splIds.add(oppSplits.id);
        
        Opportunity o   = new Opportunity();                
        o.AccountId     =  Acclist[0].id;
        o.name          = 'Test Opp ';
        o.stagename     = 'Closed won';
        o.CloseDate     = system.today() + 10;
        o.Pre_Contract_Provisioning_Required__c = 'No';
        o.QuoteStatus__c = 'Approved';
        o.Total_NRC__c = 200;
        o.Total_MRC__c = 300;
        O.Quotestatus__c = 'Approved';
        O.Order_Type__c = 'Renewal';
        O.currencyisocode = 'INR' ;
        OppList.add(o);          
        insert OppList;
         System.assert(OppList!=null); 
        Map <id, Opportunity> Oopmap = new Map<id, Opportunity>();
        Oopmap.put(OppList[0].id, opplist[0]);
        
        
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Contact> contactList = P2A_TestFactoryCls.getContact(1,accList);
        List<Site__c> SitesList = P2A_TestFactoryCls.getsites(1,accList,countrylist);
        List<BillProfile__c> bpslist = P2A_TestFactoryCls.getBPs(1, AccList, SitesList, contactList);
        
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        Products[0].Max_Contract_Term__c = 12;
        update Products[0]; 
        
        Pricebook2 pb = new Pricebook2();
        pb.name = 'Text book123 ';
        insert pb;  
        System.assert(pb!=null);   
        Resource__c res = new Resource__c();
        res.Name = 'Test resource';
        //insert res;
        CostCentre__c c = new CostCentre__c();
        c.Cost_Centre_Code__c = 'Test code';
        c.BillProfile__c = bpslist[0].id;
        c.Name = 'TestBillprofile';
        insert c;
         System.assert(c!=null);   
        Product2 newProd = new Product2(Name = 'test product', family = 'test family');
        insert newProd; 
    System.assert(newProd!=null);
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = Test.getStandardPricebookId();
        pbe.Product2Id = newProd.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        pbe.CurrencyIsocode = 'INR';
        insert pbe;
        System.assert(pbe!=null);
        OpportunityLineItem Oppitem = new OpportunityLineItem();
        Oppitem.Opportunityid = Opplist[0].id;
        Oppitem.Resource__c = res.id;
        Oppitem.OrderType__c = 'New';
        Oppitem.Is_Prior_Parent__c = true;
        Oppitem.BillProfileId__c = bpslist[0].id;
        Oppitem.CostCentreId__c = c.id;
        Oppitem.CPQItem__c = 'Test CPQ';
        Oppitem.Display_line_item_on_Invoice__c = true;
        Oppitem.Zero_Charge_MRC_Flag__c = true ;
        Oppitem.Zero_Charge_NRC_Flag__c = true;
        Oppitem.Zero_Charge_ETC_Flag__c = true;
        Oppitem.is_bundle__c = true;
        Oppitem.Parent_Bundle_Flag__c = true;
        Oppitem.Parent_Charge_ID__c = null;
        Oppitem.Bundle_Label_Name__c = 'Test Bundle';
        Oppitem.NetNRCPrice__c = 300;
        Oppitem.Reported_New_SOV__c = 300;
        Oppitem.Reported_Renewal_SOV__c = 400;
        Oppitem.Contract_Value__c = 500;
        Oppitem.SOV_Type_of_Order__c = 'Test Order';
        Oppitem.Contract_Expiry_Date__c = System.today();
        oppitem.Quantity = 12;
        oppitem.Parent_Config_SOV_Usage__c = 'No';
       
        //oppitem.TotalPrice = 12900;
       // oppitem.Pricebookentryid = pbEntry.id;
       oppitem.Pricebookentryid = pbe.id;
       oppitem.unitprice = 490;
        insert Oppitem;
        System.assert(Oppitem!=null);
        Opp_Product_Type__c opt = new Opp_Product_Type__c();
        Opt.Opportunity__c = Opplist[0].id;
        opt.MRC__c = 300;
        opt.NRC__c = 200;
        opt.Reported_New_SOV__c = 500;
        opt.Reported_Renewal_SOV__c = 200;
        opt.Product_Type_Name__c = 'PEN';
        opt.CurrencyIsoCode = 'INR' ;
        optlist.add(opt);
        insert optlist;
        System.assert(optlist!=null);
        OpportunityTriggerHandler op = new OpportunityTriggerHandler();
        op.updateSOVToOppLineItem(opplist,Oopmap ,Oopmap );
        
        Opportunity opp   = new Opportunity();                
        opp.AccountId     =  Acclist[0].id;
        opp.name          = 'Test Opp ';
        opp.stagename     = 'Inflight';
        opp.CloseDate     = system.today() + 10;
        opp.Pre_Contract_Provisioning_Required__c = 'No';
        opp.SOV_Contract_Type__c = 'Duplicate';
        opp.Total_NRC__c = 0;
        opp.Total_MRC__c = 0;
        Opp.Quotestatus__c = 'approved'; 
        Opp_List.add(opp);          
        insert Opp_List;
        System.assert(Opp_List!=null);
        List<Opp_Product_Type__c> optlists = new List<Opp_Product_Type__c>();
        
        Opp_Product_Type__c opts = new Opp_Product_Type__c();
        Opts.Opportunity__c = Opp_list[0].id;
        opts.MRC__c = 300;
        opts.NRC__c = 200;
        opts.Reported_New_SOV__c = 500;
        opts.Reported_Renewal_SOV__c = 200;
        opts.Product_Type_Name__c = 'PEN';
        opts.CurrencyIsoCode = 'INR' ;
        optlists.add(opts);
        insert optlists;
        System.assert(optlists!=null);
        Map<id,Opportunity> Oppmap = new Map<id,Opportunity>();
        Oppmap.put(Opp_List[0].id, opp_list[0]);
        
        OpportunityTriggerHandler otp = new OpportunityTriggerHandler();
        otp.updateSOVToOppLineItem(opp_list,Oppmap ,Oppmap );
        
        List<Opportunity> OList = P2A_TestFactoryCls.getOpportunitys(1 ,AccList);
        OList[0].stagename = 'closed won';      
        update olist;
        
        List<Opportunity> OList1 = P2A_TestFactoryCls.getOpportunitys(1 ,AccList);
        olist[0].StageName = 'InFlight';
        update OList1;
        
        
        System.assert(olist!=null);
        List<Opp_Product_Type__c> opt_lists = new List<Opp_Product_Type__c>();
        
        Opp_Product_Type__c optes = new Opp_Product_Type__c();
        Optes.Opportunity__c = OList[0].id;
        optes.MRC__c = 300;
        optes.NRC__c = 200;
        optes.Reported_New_SOV__c = 500;
        optes.Reported_Renewal_SOV__c = 200;
        optes.Product_Type_Name__c = 'PEN';
        optes.CurrencyIsoCode = 'INR' ;
        opt_lists.add(optes);
        insert opt_lists;
        System.assert(opt_lists!=null);
        Opp_Product_Type__c optes1 = new Opp_Product_Type__c(); //done
        Optes1.Opportunity__c = opp_list[0].id;
        optes1.MRC__c = 300;
        optes1.NRC__c = 200;
        optes1.Reported_New_SOV__c = 500;
        optes1.Reported_Renewal_SOV__c = 200;
        optes1.Product_Type_Name__c = 'PEN';
        optes1.CurrencyIsoCode = 'INR' ;
        opt_lists.add(optes1);
        upsert opt_lists;
        System.assert(opt_lists!=null);
        
        Map<id,Opportunity> Oppmap1 = new Map<id,Opportunity>();
        
        Oppmap1.put(OList[0].id,OList[0]);
                
        Map<id,Opportunity> oldOppMap = new Map<id,Opportunity>();
        oldOppMap.put(OList[0].id,OList[0]);
        
        Map<id,Opportunity> oldOppMap1 = new Map<id,Opportunity>();
        oldOppMap1.put(OList1[0].id,OList1[0]);
        
        List<Opportunity> OListLst = new List<Opportunity>();
        Opportunity OList12 = new Opportunity();
        OList12.id = OList[0].id;
        OList12.Order_Type__c='Renewal';
        OList12.CurrencyIsoCode='INR';
        OList12.OwnerId = userinfo.getuserid();
        OListLst.add(OList12);
        
        System.assertNotEquals(OListLst[0].OwnerId,OList[0].OwnerId);
        OpportunityTriggerHandler otps = new OpportunityTriggerHandler();       
        otps.refreshMainItem(OList,Oopmap ,Oppmap1);
        Map<id, id> oppaccmap = new Map <id,id>();
        oppaccmap.put(OList[0].id , OList[0].id);
        OpportunityTriggerHandler.createCrossCountryAppAIAsync(oppaccmap ,oppList1[0].id);
        otps.createSOVPerformanceTracking(OList, Oppmap1, Oppmap1);
        otps.createCrossCountryAppAI(OList, Oppmap1, Oppmap1);
        OpportunityTriggerHandler.createSOVPerformanceTrackingAsyn(splIds,splIds);            
        //otps.refreshMainItem(OListLst,oldOppMap ,oldOppMap);
        otps.oppOwnerChange(OListLst,oldOppMap);
        otps.updateInflightOrder(OList1,oldOppMap1,oldOppMap1);
        
        set<id> oppids = new set<id>();
        oppids.add(olist[0].id);
        //OpportunityTriggerHandler.createSOVPerformanceTrackingAsyn(Oppmap1,oppIds);
        Test.stoptest(); 
    
    }
    
 Public static testmethod void test10(){
    
        Test.starttest();
        List<Opportunity> Opp_List = new List<Opportunity>();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = new list<Opportunity>(); //P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Opp_Product_Type__c> Optlist = new list<Opp_Product_Type__c>();
        
        Opportunity o   = new Opportunity();                
        o.AccountId     =  Acclist[0].id;
        o.name          = 'Test Opp ';
        o.stagename     = 'Closed Lost';
        o.CloseDate     = system.today() + 10;
        o.Pre_Contract_Provisioning_Required__c = 'No';
        o.QuoteStatus__c = 'Approved';
        o.Total_NRC__c = 200;
        o.Total_MRC__c = 300;
        O.Quotestatus__c = 'Approved';
        O.Order_Type__c = 'Renewal';
        O.currencyisocode = 'INR' ;
        o.Sales_Status__c = 'Lost';
        o.Win_Loss_Reasons__c = 'Commercial Model';
        OppList.add(o);          
        insert OppList;
         System.assert(OppList!=null);
        Action_Item__c a = new Action_Item__c();
        a.Quote_Subject__c = 'Pricing Validation';
        a.Opportunity__c = opplist[0].id;
        insert a;
        System.assert(a!=null);
        Map <id, Opportunity> Oppmap = new Map<id, Opportunity>();
        Oppmap.put(OppList[0].id, opplist[0]);
        
 
        OpportunityTriggerHandler otps = new OpportunityTriggerHandler();
        otps.SetPricingAIclosedlost(Oppmap );
        
        Test.stoptest(); 
    
    }
     
     @isTest static void testExceptions(){
         OpportunityTriggerHandler alls=new OpportunityTriggerHandler();
        
         try{alls.beforeInsert(null);}catch(Exception e){}
         try{alls.beforeUpdate(null,null,null);}catch(Exception e){}
         try{alls.afterUpdate(null,null,null);}catch(Exception e){}
         try{OpportunityTriggerHandler.createCrossCountryAppAIAsync(null,null);}catch(Exception e){}
         try{alls.changeAccountID(null,null);}catch(Exception e){}
             
         
         
     }
    
}