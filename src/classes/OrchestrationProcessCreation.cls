/** Observer pattern implementation for batch order generation **/
public with sharing class OrchestrationProcessCreation{

   @TestVisible  
   public void OrchestrationProcessTemplateCreation(List<Id> serviceIds, List<Id> orderIds){

        /** Create Orchestration Process template for Services. NC fix for T-32493.*/
        Map<Integer, List<Id>> orderIdChunks = createChunks(orderIds, 1);
        Map<Integer, List<Id>> serviceIdChunks = createChunks(serviceIds, 5);
        for(Integer i :serviceIdChunks.keySet()){
            QueueableChainedJobStack.add(new OrchestratorProcessHelper.ServiceOrchestratorProcessWorker(Label.Service_Orchestrator_Process_Name), serviceIdChunks.get(i));
        }
        
        for(Integer i :orderIdChunks.keySet()){
            QueueableChainedJobStack.add(new OrchestratorProcessHelper.OrderOrchestratorProcessWorker(Label.Order_Orchestrator_Process_Name + Label.Orchestrations_Process_New), orderIdChunks.get(i));
        }
        
        /**Set Orchestrator Process Processing mode to Background **/
        for(Integer i : orderIdChunks.keySet()){
            if(orderIdChunks.get(i) != null && !orderIdChunks.get(i).isEmpty()){
                QueueableChainedJobStack.add(new OrchestratorProcessHelper.OrchestratorProcessWorker('Orders'), orderIdChunks.get(i));
           }
        }
        
        for(Integer i : serviceIdChunks.keySet()){
            if(serviceIdChunks.get(i) != null && !serviceIdChunks.get(i).isEmpty()){
              QueueableChainedJobStack.add(new OrchestratorProcessHelper.OrchestratorProcessWorker('Services'), serviceIdChunks.get(i));
            }
        }
        /** End - Set Orchestrator Process Processing mode to Background **/
               
        for(Integer i :serviceIdChunks.keySet()){
            QueueableChainedJobStack.add(new OrderAndServiceFactory.OrderAndServicesFromServicesWorker(), serviceIdChunks.get(i));
        }
         
        QueueableChainedJobStack.enqueue();
        /** Fix End for T-32493.*/
    }

   @TestVisible  
   public void updateESD(List<Id> serviceIds){
        Map<Integer, List<Id>> serviceIdChunks = createChunks(serviceIds, 5);
        for(Integer i :serviceIdChunks.keySet()){
            QueueableChainedJobStack.add(new OrderAndServiceFactory.OrderAndServicesFromServicesWorker(), serviceIdChunks.get(i));
        }
        QueueableChainedJobStack.enqueue();
   }   
    
    /**
     * Nikola Culej - method to chunk List<Id> into smaller pieces
     * done to resolve the too many DML rows issue comming from QueueableChainedJob
     */
    @TestVisible 
    private Map<Integer, List<Id>> createChunks(List<Id> ids, Integer chunkSize){
        Map<Integer, List<Id>> chunkMap = new Map<Integer, List<Id>>();
        Integer i = 0;
        
        for(Id tempId: ids){
            if(chunkMap.get(i) == null){
                chunkMap.put(i, new List<Id>());
            }
            
            if(chunkMap.get(i).size() == 0){
                List<Id> tmpList = new List<Id>();
                tmpList.add(tempId);
                chunkMap.put(i, tmpList);
            } else {
                chunkMap.get(i).add(tempId);
                if(chunkMap.get(i).size() >= chunkSize){
                    i = i + 1;
                }
            }
        }
        return chunkMap;
    }  
}