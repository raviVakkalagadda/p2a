@isTest
private class OrchestrationRetryBatchTest{
    private static List<CSPOFA__Orchestration_Process__c> orchprocesslist; 
    private static List<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist;
    
    static testmethod void unittest() {
        P2A_TestFactoryCls.sampleTestData();  
        orchprocessTemplist = new List<CSPOFA__Orchestration_Process_Template__c>{
                          new CSPOFA__Orchestration_Process_Template__c(name = 'Template',CSPOFA__Associated_Profile__c = 'Profile'),
                          new CSPOFA__Orchestration_Process_Template__c(name = 'Template1',CSPOFA__Associated_Profile__c = 'Profile1')  
                          };
                          insert orchprocessTemplist;
        
        orchprocesslist = new List<CSPOFA__Orchestration_Process__c>{
                          new CSPOFA__Orchestration_Process__c(name = 'Orchestration',CSPOFA__Status__c = 'unassigned',CSPOFA__State__c ='Inactive',CSPOFA__Orchestration_Process_Template__c =orchprocessTemplist[0].id),
                          new CSPOFA__Orchestration_Process__c(name = 'Orchestration1',CSPOFA__Status__c ='notassigned',CSPOFA__State__c = 'Inactive',CSPOFA__Orchestration_Process_Template__c =orchprocessTemplist[1].id)
        }; 
        insert orchprocesslist;
                  
        String query = 'SELECT Id,Name FROM  CSPOFA__Orchestration_Step__c';              
        
        list<CSPOFA__Orchestration_Step__c> csoslist = new list<CSPOFA__Orchestration_Step__c>();
        CSPOFA__Orchestration_Step__c Csos = new CSPOFA__Orchestration_Step__c();
        Csos.name = 'OrchestrationStep';
        Csos.CSPOFA__Orchestration_Process__c = orchprocesslist[0].id; 
        Csos.CSPOFA__Status__c = 'NotCompleted';
        insert Csos;
       
        list<CSPOFA__Orchestration_Step__c> Orchestration = [SELECT Id, CSPOFA__Status__c, CSPOFA__Orchestration_Process__r.CSPOFA__Status__c,
                                        CSPOFA__Orchestration_Process__r.CSPOFA__State__c FROM CSPOFA__Orchestration_Step__c 
                                         where id =:Csos.id limit 1]; 
                                        
        for(CSPOFA__Orchestration_Step__c os: Orchestration) {
             os.CSPOFA__Status__c = 'In Progress';
             os.CSPOFA__Orchestration_Process__r.CSPOFA__Status__c = 'In Progress';
             os.CSPOFA__Orchestration_Process__r.CSPOFA__State__c = 'ACTIVE';
             csoslist.add(os);
           }
           update csoslist;

        Test.startTest();
           OrchestrationRetryBatch  cleanorch = new OrchestrationRetryBatch(); 
           cleanorch.execute(null,csoslist);
           Database.executeBatch(cleanorch);
        Test.stopTest();

        Integer i = [SELECT COUNT() FROM CSPOFA__Orchestration_Step__c];
        System.assertEquals(i, 1);
    }
}