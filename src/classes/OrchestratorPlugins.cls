global class OrchestratorPlugins {
global class ChatterConnectApiProvider implements
CSPOFA.ChatterApiProvider_V1 {
/**
* @description Post feed item to chatter feed.
* ConnectApi.RateLimitException is not handled, the client code MUST wrap
the call in a try-catch block and handle this exception!
*
* @param feedItemInput: a properly prepared ConnectAPI.FeedItemInput
class instance
* @param subjectID: string representation of the ID of target object to
whose stream the post will be made.
*
* */
public void post(String subjectId, ConnectApi.FeedItemInput
feedItemInput) {
Id subject = normalizeId(subjectId);
ConnectApi.FeedType targetFeedType =
ConnectApi.FeedType.Record;
if (subject != null &&
subject.getSObjectType().getDescribe().getName().toLowerCase().startsWith('user')) {
targetFeedType =
ConnectApi.FeedType.UserProfile;
}
//ConnectApi.FeedType feedType = new ConnectApi.FeedType('News');
//can throw RateLimitException
ConnectApi.ChatterFeeds.postFeedElement('internal', subjectId, ConnectApi.FeedElementType.FeedItem, 'GES-I Chatter test');
}
//for @testvisible, modified by Anuradha
@testvisible
private Id normalizeId(String idText) {
try{
return Id.valueOf(idText);
}
catch(Exception e){
return null;
}
}
}
global class ChatterApiProviderFactory implements CSPOFA.PluginFactory {
public Object create(){
return new ChatterConnectApiProvider();
}
}
global class OlaActivityProvider implements CSPOFA.OlaActivityOperations {
        private String calendarId;
        public OlaActivityProvider() {}
        public void setIdentifier(String identifier) {
            calendarId = identifier;
        }
        public Long getTimeAmount(Datetime startTime, Datetime endTime) {
            CSSX.Calendar_Funcs_API_V1.SearchContext sc = new CSSX.Calendar_Funcs_API_V1.SearchContext(calendarId);
            sc.startDateTime = startTime;
            sc.endDateTime = endTime;
            System.debug('test>'+startTime);
            System.debug('test>'+endTime);
            Long milis = CSSX.Calendar_Funcs_API_V1.getDuration(sc, CSSX.Calendar_Funcs_API_V1.SearchType.MILLISECOND);
            System.debug('test>'+milis);
            return milis;
        }
        public Datetime getNextTimepoint(Datetime startTime, Long timeAmount) {
            CSSX.Calendar_Funcs_API_V1.SearchContext sc = new CSSX.Calendar_Funcs_API_V1.SearchContext(calendarId);
            sc.startDateTime = startTime;
            Datetime newTimePoint = CSSX.Calendar_Funcs_API_V1.forward(sc, CSSX.Calendar_Funcs_API_V1.SearchType.MILLISECOND, timeAmount);
            return newTimePoint;
        }
        public String getIdentifierType() {
            return 'CSSX__Calendar__c';
        }
    }
    global class OlaActivityProviderFactory implements CSPOFA.PluginFactory {
        public Object create(){
            return new OlaActivityProvider();
        }
    }
}