@isTest (seealldata = false)
public class OrchestratorPluginsTest{
    
    //P2A_TestFactoryCls.sampleTestData();
    public static List<Account> AccList =  P2A_TestFactoryCls.getAccounts(1);     
    public static string identifier = AccList[0].id;         
    public static Long lng  = 21474838973344648L;     
    public static string subjectId;
    public static string ids;   
    public static Datetime startTime = Datetime.newInstance(2016,11,26,11,23,11);
    public static Datetime endTime = Datetime.newInstance(2016,11,26,11,23,11);
    public static ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
    //public static ConnectApi.FeedItemInput feedItemInput = 'news' ;   
    public static OrchestratorPlugins.OlaActivityProvider orchOlaActivProvid = new OrchestratorPlugins.OlaActivityProvider();
    
    public static testMethod void OrchestratorPluginsTest_Method(){ 
        P2A_TestFactoryCls.sampleTestData();        
        OrchestratorPlugins.ChatterConnectApiProvider opchatterApiprov = new OrchestratorPlugins.ChatterConnectApiProvider(); 
        system.assertEquals(true,opchatterApiprov!=null); 
        try{      
        opchatterApiprov.post(subjectId,feedItemInput);     
        }catch(Exception e)
        {
        ErrorHandlerException.ExecutingClassName='OrchestratorPluginsTest:OrchestratorPluginsTest_Method';         
        ErrorHandlerException.sendException(e); 
        }
        opchatterApiprov.normalizeId(ids);
        OrchestratorPlugins.ChatterApiProviderFactory opchatterApiprovFactry = new OrchestratorPlugins.ChatterApiProviderFactory();
        Object obj = opchatterApiprovFactry.create();       
        orchOlaActivProvid.setIdentifier(identifier);       
    }
    public static testMethod void OrchestratorPluginsTest_Method1(){
        try{                                
            Long lngdata = orchOlaActivProvid.getTimeAmount(startTime,endTime);         
            system.assert(lngdata!=null);
        }
        catch(exception e)
        {
        ErrorHandlerException.ExecutingClassName='OrchestratorPluginsTest:OrchestratorPluginsTest_Method1';         
        ErrorHandlerException.sendException(e);
        }   
    }
    public static testMethod void OrchestratorPluginsTest_Method2(){
        try{                            
            Datetime Dt = orchOlaActivProvid.getNextTimepoint(startTime,lng);           
            system.assert(Dt!=null);
        }
        catch(exception e)
        {
        ErrorHandlerException.ExecutingClassName='OrchestratorPluginsTest:OrchestratorPluginsTest_Method2';         
        ErrorHandlerException.sendException(e);
        }   
    }
    public static testMethod void OrchestratorPluginsTest_Method3(){
        try{                                
            String str = orchOlaActivProvid.getIdentifierType();            
            system.assert(str!=null);
        }
        catch(exception e)
        {
        ErrorHandlerException.ExecutingClassName='OrchestratorPluginsTest:OrchestratorPluginsTest_Method3';         
        ErrorHandlerException.sendException(e);
        }   
    }
    public static testMethod void OrchestratorPluginsTest_Method4(){
        try{                        
            OrchestratorPlugins.OlaActivityProviderFactory orchplugolaactivprvFactory = new OrchestratorPlugins.OlaActivityProviderFactory();
            Object obj1 = orchplugolaactivprvFactory.create();
            system.assertEquals(true,orchplugolaactivprvFactory!=null); 
        }
        catch(exception e)
        {
        ErrorHandlerException.ExecutingClassName='OrchestratorPluginsTest:OrchestratorPluginsTest_Method4';         
        ErrorHandlerException.sendException(e);
        }   
    }
}