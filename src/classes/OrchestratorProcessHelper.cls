public class OrchestratorProcessHelper {
    
    public static CSPOFA__Orchestration_Process_Template__c findOrchestrationProcess(string processName) {
        CSPOFA__Orchestration_Process_Template__c processTemplate = [select Id, Name
            from CSPOFA__Orchestration_Process_Template__c
            where Name = :processName 
            limit 1];
            
        return processTemplate;
    }
    
    public static void createOrchestrationProcess(string processName, List<csord__Subscription__c> subscriptions) {
        
        CSPOFA__Orchestration_Process_Template__c processTemplate = findOrchestrationProcess(processName);
        List<CSPOFA__Orchestration_Process__c> newProcessList = new List<CSPOFA__Orchestration_Process__c>();
        system.debug('Matija createOrchestrationProcess subscriptions: '+ subscriptions);
        system.debug('Matija createOrchestrationProcess subscriptions processTemplate: '+ processName+' - '+ processTemplate);
       
        for(csord__Subscription__c item : subscriptions) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.csordtelcoa__Subscription__c = item.Id;
            process.Name = item.Name + ' ' + processName;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplate.Id;
            newProcessList.add(process);
        }
        system.debug('Matija createOrchestrationProcess subscriptions newProcessList: '+ newProcessList);
        if(!newProcessList.isEmpty()) {
            insert newProcessList;
        }        
    }

    public static void createOrchestrationProcess(string processName, List<csord__Order__c> orders) {
    
        CSPOFA__Orchestration_Process_Template__c processTemplate = findOrchestrationProcess(processName);
        List<CSPOFA__Orchestration_Process__c> newProcessList = new List<CSPOFA__Orchestration_Process__c>();
        Map<Id, OrderAndServices> oandsMap = new Map<Id, OrderAndServices>();
        Set<Id> lstOrderIds = new Set<Id>();

        system.debug('Matija createOrchestrationProcess orders: '+ orders);
        system.debug('Matija createOrchestrationProcess orders processTemplate: '+ processName+' - '+ processTemplate);
        for(csord__Order__c item : orders) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            lstOrderIds.add(item.Id);
            process.Order__c = item.Id;
            process.Name = item.csord__Order_Number__c + processName;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplate.Id;
            process.CSPOFA__Processing_Mode__c = 'Foreground';
            newProcessList.add(process);
        }
        system.debug('Matija createOrchestrationProcess orders newProcessList: '+ newProcessList);
        if(!newProcessList.isEmpty()) {
            insert newProcessList;
        }
    }

    public static void createOrchestrationProcess(string processName, List<csord__Service__c> services) {
    
        CSPOFA__Orchestration_Process_Template__c processTemplate = findOrchestrationProcess(processName);
        List<CSPOFA__Orchestration_Process__c> newProcessList = new List<CSPOFA__Orchestration_Process__c>();
        system.debug('Matija createOrchestrationProcess services: '+ services);
        system.debug('Matija createOrchestrationProcess services processTemplate: '+ processName+' - '+ processTemplate);
        for(csord__Service__c item : services) {
            CSPOFA__Orchestration_Process__c process = new CSPOFA__Orchestration_Process__c();
            process.csordtelcoa__Service__c = item.Id;
            system.debug('item Name: ' + item.Name);
            process.Name = item.Name + ' ' + processName;
            process.CSPOFA__Orchestration_Process_Template__c = processTemplate.Id;
            process.CSPOFA__Processing_Mode__c = 'Foreground';
            newProcessList.add(process);
        }
        system.debug('Matija createOrchestrationProcess services newProcessList: '+ newProcessList);
        if(!newProcessList.isEmpty()) {
            insert newProcessList;
        }
    }
    
    @future
    @testVisible
    private static void createOrderOrchestrationProcessFuture(string processName, List<Id> orderIds) {
        //List<csord__Order__c> orders = [select Id, csord__Order_Number__c, Name from csord__Order__c where Id in: orderIds];
        createOrderOrchestrationProcessBase(processName, orderIds);
    }
    
    @future
    @testVisible
    private static void createSubscriptionOrchestrationProcessFuture(string processName, List<Id> subscriptionIds) {
        //List<csord__Subscription__c> subscriptions = [select Id, Name from csord__Subscription__c where Id in: subscriptionIds];
        createSubscriptionOrchestrationProcessBase(processName, subscriptionIds);
    }
    
    @future
    @testVisible
    private static void createServiceOrchestrationProcessFuture(string processName, List<Id> serviceIds) {
        //List<csord__Service__c> services = [select Id, Name from csord__Service__c where Id in: serviceIds];
        createServiceOrchestrationProcessBase(processName, serviceIds);
    }
    
    @testVisible
    private static void createOrderOrchestrationProcessBase(string processName, List<Id> orderIds) {
        List<csord__Order__c> orders = [select Id, csord__Order_Number__c, Name from csord__Order__c where Id in: orderIds];
        createOrchestrationProcess(processName, orders);
    }
    
    @testVisible
    private static void createSubscriptionOrchestrationProcessBase(string processName, List<Id> subscriptionIds) {
        List<csord__Subscription__c> subscriptions = [select Id, Name from csord__Subscription__c where Id in: subscriptionIds];
        createOrchestrationProcess(processName, subscriptions);
    }
    
    @testVisible
    public static void createServiceOrchestrationProcessBase(string processName, List<Id> serviceIds) {
        List<csord__Service__c> services = [select Id, Name from csord__Service__c where Id in: serviceIds];
        createOrchestrationProcess(processName, services);
    }
    
    public static void createOrderOrchestrationProcess(string processName, List<Id> orderIds) {
        if (System.isFuture() || System.isBatch() || System.isScheduled()) {
            createOrderOrchestrationProcessBase(processName, orderIds);            
        } else {
            createOrderOrchestrationProcessFuture(processName, orderIds);
        }
    }
    
    public static void createSubscriptionOrchestrationProcess(string processName, List<Id> subscriptionIds) {
        if (System.isFuture() || System.isBatch() || System.isScheduled()) {
            createSubscriptionOrchestrationProcessBase(processName, subscriptionIds);
        } else {
            createSubscriptionOrchestrationProcessFuture(processName, subscriptionIds);            
        }
    }
    
    public static void createServiceOrchestrationProcess(string processName, List<Id> serviceIds) {
        if (System.isFuture() || System.isBatch() || System.isScheduled()) {
            createServiceOrchestrationProcessBase(processName, serviceIds);
        } else {
            createServiceOrchestrationProcessFuture(processName, serviceIds);            
        }
    }
    
    public class OrderOrchestratorProcessWorker implements IWorker {
        @Testvisible
        private string m_processName;
       
        public OrderOrchestratorProcessWorker(String processName) {
            this.m_processName = processName;
        }
        
        public void work(Object params, Object context) {
            createOrderOrchestrationProcessBase(this.m_processName, (List<Id>)params);
        }
    }
    
    public class ServiceOrchestratorProcessWorker implements IWorker {
        @Testvisible
        private string m_processName;
       
        public ServiceOrchestratorProcessWorker(String processName) {
            this.m_processName = processName;
        }
        
        public void work(Object params, Object context) {
            createServiceOrchestrationProcessBase(this.m_processName, (List<Id>)params);
        }
    }
    
    /*NC - set Orchestrator Process to Foreground Processing Mode*/
    public class OrchestratorProcessWorker implements IWorker{
        private string objectType;
    
        public OrchestratorProcessWorker(String type){
            this.objectType = type;
        }
        
        public void work(Object params, Object context){
            updateOrchestratorProcessProcessingMode(this.objectType, (List<Id>)params);
        }
    }
    
    public static void updateOrchestratorProcessProcessingMode(String type, List<Id> ids){
        List<CSPOFA__Orchestration_Process__c> processList = new List<CSPOFA__Orchestration_Process__c>();
        List<CSPOFA__Orchestration_Process__c> processListToUpdate = new List<CSPOFA__Orchestration_Process__c>();
        
        if(type.equals('Orders')){
             processList = [SELECT Id, Name, CSPOFA__Processing_Mode__c FROM CSPOFA__Orchestration_Process__c WHERE Order__c in: ids];
        } else if(type.equals('Services')){
            processList = [SELECT Id, Name, CSPOFA__Processing_Mode__c FROM CSPOFA__Orchestration_Process__c WHERE csordtelcoa__Service__c in: ids];
        }
        
        system.debug('*****updateOrchestratorProcessProcessingMode ProcessList: ' + processList.size());
        
        if(!processList.isEmpty()){
            for(CSPOFA__Orchestration_Process__c op : processList){
                op.CSPOFA__Processing_Mode__c = 'Background';
                processListToUpdate.add(op);
            }        
        }
        
        if(!processListToUpdate.isEmpty()){
            update processListToUpdate;
        }
    }
    
    public class SubscriptionOrchestratorProcessWorker implements IWorker {
        private string m_processName;
       
        public SubscriptionOrchestratorProcessWorker(String processName) {
            this.m_processName = processName;
        }
        
        public void work(Object params, Object context) {
            createSubscriptionOrchestrationProcessBase(this.m_processName, (List<Id>)params);
        }
    }
    
    public class OrchestratorProcessHelperException extends Exception {}
}