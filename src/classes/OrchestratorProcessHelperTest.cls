/*
Test class was converted as class - so re-created@16/2/2017
*/
@isTest(SeeAllData = false)
public class OrchestratorProcessHelperTest {

    private static CSPOFA__Orchestration_Process_Template__c orchTemplate = null;
    private static CSPOFA__Orchestration_Process__c orchProcess = null;
    
    private static cscfga__Product_Basket__c basket = null;
    
    private static cscfga__Product_Configuration__c config = null;
    private static cscfga__Product_Configuration__c masterConfig = null;
    
    private static csord__Service__c service = null;
    private static csord__Subscription__c subscription = null;
    private static csord__Order__c order = null;
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
        /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        } else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }

    private static void enableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);
        
        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = false;
        }

        upsert telcoOptions;
    }

    private static void createTestData() {
        
        orchTemplate = new CSPOFA__Orchestration_Process_Template__c(Name = 'testTemplate');
        insert orchTemplate;
        
        /* orchProcess = new CSPOFA__Orchestration_Process__c(Name = 'testProcess', CSPOFA__Orchestration_Process_Template__c = orchTemplate.Id);
        insert orchProcess;*/
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        cscfga__Product_Definition__c prodDefintion = new cscfga__Product_Definition__c (Name = 'Test definition 1'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test definition 1');
        insert prodDefintion;
        
        basket = new cscfga__Product_Basket__c();
        insert basket;
        
        masterConfig = new cscfga__Product_Configuration__c(Name = 'Test master config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id);  
        insert masterConfig;
        
        config = new cscfga__Product_Configuration__c(Name = 'Test config '
            , cscfga__Product_Definition__c = prodDefintion.Id
            , cscfga__Product_Basket__c = basket.Id
            , Master_IPVPN_Configuration__c = masterConfig.Id);  
        insert config;

        csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        
        order = new csord__Order__c(Name = 'Test order'
            , csord__Identification__c = 'Order_' +  config.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert order;
        
        subscription = new csord__Subscription__c(Name = 'Test subscription'
            , csord__Identification__c = 'Subscription_' + config.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert subscription; 
        
        service = new csord__Service__c(Name = 'Test service'
            , csordtelcoa__Product_Configuration__c = config.Id
            , csordtelcoa__Product_Basket__c = basket.Id
            , csord__Identification__c = 'Service_' + config.Id
            , csord__Subscription__c = subscription.Id
            , csord__Order_Request__c = orderRequest.Id);
        insert service;
        
        /* cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession(config); 
        apiSession.persistConfiguration(true);
        apiSession.close(); */
    }

    private static void tryToCreateTestData() {
        Exception ee = null;
        try {
            disableAll(UserInfo.getUserId());
            disableTelcoAll(UserInfo.getUserId());
            createTestData();
        } catch(Exception e) {
            ee = e;
        } finally {
            enableAll(UserInfo.getUserId());
            enableTelcoAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
    private static testMethod void testCreateServiceOrchestrationProcess() {
      P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
    
        Test.startTest();
        OrchestratorProcessHelper.createServiceOrchestrationProcess('testTemplate', new List<Id>{ service.Id });
        Test.stopTest();
    
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE csordtelcoa__Service__c = :service.Id OR Service__c = :service.Id  
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }
    
    private static testMethod void testCreateSubscriptionOrchestrationProcess() {
      P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
    
        Test.startTest();
        OrchestratorProcessHelper.createSubscriptionOrchestrationProcess('testTemplate', new List<Id>{ subscription.Id });
        Test.stopTest();
    
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE csordtelcoa__Subscription__c= :subscription.Id  
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }
    
    private static testMethod void testCreateOrderOrchestrationProcess() {
       P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
    
        Test.startTest();
        OrchestratorProcessHelper.createOrderOrchestrationProcess('testTemplate', new List<Id>{ order.Id });
        Test.stopTest();
        
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE Order__c = :order.Id 
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }

    private static testMethod void testServiceOrchestrationWorker() {
       P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
    
        Test.startTest();
        OrchestratorProcessHelper.ServiceOrchestratorProcessWorker worker = new OrchestratorProcessHelper.ServiceOrchestratorProcessWorker('testTemplate');
        worker.work(new List<Id>{ service.Id }, null);
        Test.stopTest();
    
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE csordtelcoa__Service__c = :service.Id OR Service__c = :service.Id  
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }
    
    private static testMethod void testSubscriptionOrchestrationWorker() {
       P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
    
        Test.startTest();
        OrchestratorProcessHelper.SubscriptionOrchestratorProcessWorker worker = new OrchestratorProcessHelper.SubscriptionOrchestratorProcessWorker('testTemplate');
        worker.work(new List<Id>{ subscription.Id }, null);
        
        Test.stopTest();
    
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE csordtelcoa__Subscription__c= :subscription.Id  
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }
    
    private static testMethod void testOrderOrchestrationWorker() {
       P2A_TestFactoryCls.sampleTestData();
        tryToCreateTestData();
  
        Test.startTest();
        OrchestratorProcessHelper.OrderOrchestratorProcessWorker worker = new OrchestratorProcessHelper.OrderOrchestratorProcessWorker('testTemplate');
        worker.work(new List<Id>{ order.Id }, null);
       
        OrchestratorProcessHelper.updateOrchestratorProcessProcessingMode('test',new List<Id>{ order.Id });
        Test.stopTest();
        
        CSPOFA__Orchestration_Process__c proc = [SELECT Id, Name
            FROM CSPOFA__Orchestration_Process__c 
            WHERE Order__c = :order.Id 
            LIMIT 1];
        
        system.assert(proc != null, 'proc is null');
    }
    
}