/**
    @author - Accenture
    @date - 20- Jun-2012
    @version - 1.0
    @description - This is the test class for OrderAfterUpdate Trigger.
*/
@isTest
public class OrderAfterUpdateTest {
    public static Order__c ord;
    public static Order__c neword;
    public static Order__c neword1;
    //public static Order_Line_Item__c oli;

    
   static testMethod void getOrderDetailsTest() {
    
        
        List<Task> testTask = new List<Task>(); 
        Task ts1 = new Task();
        Task ts2 = new Task();
        Task ts3 = new Task();
        Task ts4 = new Task();
        Task ts5 = new Task();
        Task ts6 = new Task();
        Task ts7 = new Task();
        Task ts8 = new Task();
        Task ts9 = new Task();
        Task ts10 = new Task();
        Task ts11 = new Task();
        Task ts12 = new Task();
        Task ts13 = new Task();

        User user = getUser();
        ord = getOrder();
        
        ord=[select Id,OrderType__c,ownerid,Is_In_Flight__c,SD_PM_Contact__c,Is_Order_Submitted__c from Order__c where Id =: ord.Id limit 1];
        

        
        
        Order_Line_Item__c oli = getOrderLineItem();
        Order_Line_Item__c oli2 = getOrderLineItem2();
        Order_Line_Item__c oli3 = getOrderLineItem3();
      
        Order_Line_Item__c oli1 = [select Id,OrderType__c,ParentOrder__c from Order_Line_Item__c where Id =:oli.id limit 1];
        system.debug('OLIId********'+ oli1.id + oli1.OrderType__c + oli1.ParentOrder__c);
        
        User user1 = [select Id,Name from User where Id=: user.id limit 1];
        
        
        ord = [select Id,OrderType__c,ownerid from Order__c where Id =: ord.Id limit 1];
        system.debug('Ordertype****'+ ord.OrderType__c+ ord.Id);
        ord.SD_PM_Contact__c = user1.Id;
        test.starttest();
        update ord;
        System.assertEquals(ord.SD_PM_Contact__c ,user1.Id); 
        
       testTask = [SELECT Subject,Status,Priority,WhatId,Description FROM Task WHERE WhatId =: ord.Id limit 1];
         
         oli1.OrderType__c = 'Cancel';
         update oli1;
         
         System.assertEquals(oli1.OrderType__c ,'Cancel'); 

         
         ord.Is_Order_Submitted__c = false;
               update ord;
               
          System.assertEquals(ord.Is_Order_Submitted__c ,false); 
        
                  oli1.OrderType__c = 'Cancel';
                 oli1.Line_Item_Status__c ='Reject';
                 oli1.Reason_for_Rejection__c='NA';
         update oli1;
         
          System.assertEquals(oli1.Reason_for_Rejection__c,'NA'); 
        
         
         ord.Is_Order_Submitted__c = false;
         ord.Is_reject_button_clicked__c = true;
         ord.Status__c = 'New';
         ord.SD_PM_Contact__c = null;
         update ord;
         System.assertEquals(ord.Status__c ,'New'); 
         
         oli1.OrderType__c = 'Downgrade';
         update oli1;
         ord.Is_Order_Submitted__c = true;
         ord.Is_In_Flight__c = true;
         ord.SD_PM_Contact__c = user1.Id;
         update ord;
         System.assertEquals(ord.Is_In_Flight__c ,true); 
         
         if(ord.SD_PM_Contact__c != null && ord.OrderType__c !=0){
        System.debug('SDPM1----' + ord.SD_PM_Contact__c);
         
        ts1.OwnerId =ord.SD_PM_Contact__c;
        ts1.Subject= 'Acknowledgement Email';
        ts1.priority= 'High';
        ts1.IsReminderSet = true;
        ts1.ReminderDateTime= System.now() + 5;
        ts1.WhatId = ord.Id;
        ts1.Description = 'Please Send Acknowledgement Email';
        insert ts1; 
        System.assertEquals(ts1.WhatId , ord.Id); 
        
        ts2.OwnerId =ord.SD_PM_Contact__c;
        ts2.Subject= 'Regular/Weekly Status Update Email';
        ts2.priority= 'High';
        ts2.IsReminderSet = true;
        ts2.ReminderDateTime= System.now() + 5;
        ts2.WhatId = ord.Id;
        ts2.Description = 'Please Send Regular/Weekly Status Update Emai';
        insert ts2;
        System.assertEquals(ts2.WhatId , ord.Id); 
        
        }
        
        update ord;
        
        neword = getOrder1();
        
        neword=[select Id,OrderType__c,ownerid,Is_In_Flight__c,SD_PM_Contact__c,Is_Order_Submitted__c from Order__c where Id =: neword.Id limit 1];
        
        System.debug('SDPM2----' + neword.SD_PM_Contact__c);
        System.debug('inflight----' + neword.Is_In_Flight__c);
        System.debug('sub----' + ord.Is_Order_Submitted__c);
        System.debug('sub1----' + neword.Is_Order_Submitted__c);
        System.debug('ordertype----' + ord.OrderType__c);

        
        
        if(ord.Is_Order_Submitted__c && neword.Is_Order_Submitted__c == false && neword.Is_In_Flight__c && neword.SD_PM_Contact__c != null && ord.OrderType__c !=0) {
        
        System.debug('insideloop----' + ord.SD_PM_Contact__c);
                
        ts3.OwnerId =neword.SD_PM_Contact__c;
        ts3.Subject= 'Welcome Pack';
        ts3.priority= 'High';
        ts3.IsReminderSet = true;
        ts3.ReminderDateTime= System.now() + 5;
        ts3.WhatId = neword.Id;
        ts3.Description = 'Please Send Welcome Pack';
        insert ts3;
        System.assertEquals(ts3.priority, 'High'); 
        
        ts4.OwnerId =neword.SD_PM_Contact__c;
        ts4.Subject= 'Billing Date Letter';
        ts4.priority= 'High';
        ts4.IsReminderSet = true;
        ts4.ReminderDateTime= System.now() + 5;
        ts4.WhatId = neword.Id;
        ts4.Description = 'Please Send Billing Date Letter';
        insert ts4;
        System.assertEquals(ts4.priority, 'High'); 
        
        ts5.OwnerId =neword.SD_PM_Contact__c;
        ts5.Subject= 'Intimation to Billing/SA Letter';
        ts5.priority= 'High';
        ts5.IsReminderSet = true;
        ts5.ReminderDateTime= System.now() + 5;
        ts5.WhatId = neword.Id;
        ts5.Description = 'Please Send Intimation to Billing/SA Letter';
        insert ts5;
        System.assertEquals(ts5.priority, 'High'); 
        
        ts6.OwnerId =neword.SD_PM_Contact__c;
        ts6.Subject= 'Test Letter';
        ts6.priority= 'High';
        ts6.IsReminderSet = true;
        ts6.ReminderDateTime= System.now() + 5;
        ts6.WhatId = neword.Id;
        ts6.Description = 'Please Send Test Letter';
        insert ts6;
        System.assertEquals(ts5.priority, 'High'); 
        
        ts7.OwnerId =neword.SD_PM_Contact__c;
        ts7.Subject= 'Activation Letter';
        ts7.priority= 'High';
        ts7.IsReminderSet = true;
        ts7.ReminderDateTime= System.now() + 5;
        ts7.WhatId = neword.Id;
        ts7.Description = 'Please Send Activation Letter';
        insert ts7;
        System.assertEquals(ts7.priority, 'High'); 
        
            
        
       }
       
       update neword;
        
        neword1 = getOrder2();
        
        neword1=[select Id,OrderType__c,ownerid,Is_In_Flight__c,SD_PM_Contact__c,Is_Order_Submitted__c from Order__c where Id =: neword.Id limit 1];
        
        neword.SD_PM_Contact__c = null;
        neword.Is_In_Flight__c = true;
        update neword;
        neword1.Is_Order_Submitted__c= true;
        //neword1.OrderType__c =1;
        update neword1;
        System.assertEquals(neword1.Is_Order_Submitted__c , true); 
        
        System.debug('SDPM3----' + neword.SD_PM_Contact__c);
          System.debug('inflight----' + neword.Is_In_Flight__c);
       // System.debug('sub----' + ord.Is_Order_Submitted__c);
        System.debug('sub2----' + neword1.Is_Order_Submitted__c);
        System.debug('ordertype----' + neword1.OrderType__c);
        
        if(neword1.Is_Order_Submitted__c && neword.Is_In_Flight__c && neword.SD_PM_Contact__c == null && ord.OrderType__c !=0) {
       
         
         ts8.OwnerId =neword1.SD_PM_Contact__c;
        ts8.Subject= 'Welcome Pack';
        ts8.priority= 'High';
        ts8.IsReminderSet = true;
        ts8.ReminderDateTime= System.now() + 5;
        ts8.WhatId = neword1.Id;
        ts8.Description = 'Please Send Welcome Pack';
        insert ts8;
        System.assertEquals(ts8.priority, 'High'); 
        
        ts9.OwnerId =neword1.SD_PM_Contact__c;
        ts9.Subject= 'Billing Date Letter';
        ts9.priority= 'High';
        ts9.IsReminderSet = true;
        ts9.ReminderDateTime= System.now() + 5;
        ts9.WhatId = neword1.Id;
        ts9.Description = 'Please Send Billing Date Letter';
        insert ts9;
        System.assertEquals(ts9.priority, 'High'); 
        
        ts10.OwnerId =neword1.SD_PM_Contact__c;
        ts10.Subject= 'Intimation to Billing/SA Letter';
        ts10.priority= 'High';
        ts10.IsReminderSet = true;
        ts10.ReminderDateTime= System.now() + 5;
        ts10.WhatId = neword1.Id;
        ts10.Description = 'Please Send Intimation to Billing/SA Letter';
        insert ts10;
        System.assertEquals(ts10.priority, 'High'); 
        
        ts11.OwnerId =neword1.SD_PM_Contact__c;
        ts11.Subject= 'Test Letter';
        ts11.priority= 'High';
        ts11.IsReminderSet = true;
        ts11.ReminderDateTime= System.now() + 5;
        ts11.WhatId = neword1.Id;
        ts11.Description = 'Please Send Test Letter';
        insert ts11;
        System.assertEquals(ts11.priority, 'High'); 
        
        ts12.OwnerId =neword1.SD_PM_Contact__c;
        ts12.Subject= 'Activation Letter';
        ts12.priority= 'High';
        ts12.IsReminderSet = true;
        ts12.ReminderDateTime= System.now() + 5;
        ts12.WhatId = neword1.Id;
        ts12.Description = 'Please Send Activation Letter';
        insert ts12;
        System.assertEquals(ts12.priority, 'High'); 
       
        
        update neword1;
        }
         
         test.StopTest();
                  
        
    }
       
    private static Order__c getOrder(){
      if(ord == null){
       User u2= getUser2();
        ord = new Order__c(Is_Order_Submitted__c = true,Is_In_Flight__c = true,SD_PM_Contact__c =u2.Id );
        Opportunity o = getOpportunity();
       
        ord.Opportunity__c = o.Id;
               insert ord;
      }               
     
     return ord;
    }
      private static Order__c getOrder1(){
      if(neword == null){
      User u1= getUser1();
        neword = new Order__c(Is_Order_Submitted__c = false,Is_In_Flight__c = true,SD_PM_Contact__c =u1.Id);
        Opportunity o = getOpportunity();
        
        neword.Opportunity__c = o.Id;
         insert neword;
      }               
     
     return neword;
    }
    private static Order__c getOrder2(){
      if(neword1 == null){
       neword1 = new Order__c(Is_Order_Submitted__c = true,Is_In_Flight__c = false);
        Opportunity o = getOpportunity();
           neword1.Opportunity__c = o.Id;
         insert neword1;
      }               
     
     return neword1;
    }
    private static Order_Line_Item__c getOrderLineItem(){
        
        
        Order_Line_Item__c oli = new Order_Line_Item__c();
        ord = getOrder();
        oli.ParentOrder__c = ord.Id;
        oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'New Provide';
        oli.Product__c = getProduct().id;
      
        oli.CPQItem__c = '1';
       
        
        insert oli;
        return oli;
    }
        private static Order_Line_Item__c getOrderLineItem2(){
        
        
        Order_Line_Item__c oli2 = new Order_Line_Item__c();
        neword = getOrder1();
        oli2.ParentOrder__c = neword.Id;
        oli2.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli2.Opportunity_Line_Item_ID__c = 'Opp Line Item 002';
        oli2.OrderType__c = 'New Provide';
        oli2.Product__c = getProduct().id;
      
        oli2.CPQItem__c = '1';
       
        
        insert oli2;
        return oli2;
    }
     private static Order_Line_Item__c getOrderLineItem3(){
        
        
        Order_Line_Item__c oli3 = new Order_Line_Item__c();
        neword1 = getOrder2();
        oli3.ParentOrder__c = neword1.Id;
        oli3.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli3.Opportunity_Line_Item_ID__c = 'Opp Line Item 003';
        oli3.OrderType__c = 'New Provide';
        oli3.Product__c = getProduct().id;
      
        oli3.CPQItem__c = '1';
       
        
        insert oli3;
        return oli3;
    }
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = a.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        insert opp;
        return opp;
    }
    private static Account getAccount(){
        Account acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
        acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c = True;
        acc.Account_ID__c = '9090';
        acc.Customer_Legal_Entity_Name__c= 'scott';
        insert acc;
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        //c2.CCMS_Country_Code__c = 'IND';
        //c2.CCMS_Country_Name__c = 'India';
        c2.Country_Code__c = 'xxx';
        insert c2;
        return c2;
    } 
      private static Product2 getProduct(){
            
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPL';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            return prod1;
        
     
     }
    private static User getUser(){
        User u = new User();
        u.FirstName = 'suraj';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj.talreja@accenture.com';
        u.Username = 'suraj.talreja@accenture.com';
        u.CommunityNickname = 'suraj.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.EmployeeNumber = '873484';
        u.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u;
        return u;
    }
        private static User getUser1(){
        User u1 = new User();
        u1.FirstName = 'test';
        u1.LastName = 'sekar';
        u1.Alias = 'tsekar';
        u1.Email = 'test.sekar@accenture.com';
        u1.Username = 'test.sekar@accenture.com';
        u1.CommunityNickname = 'test.sekar';
        u1.TimeZoneSidKey = 'Australia/Sydney';
        u1.LanguageLocaleKey = 'en_US';
        u1.LocaleSidKey = 'en_AU';
        u1.EmailEncodingKey = 'ISO-8859-1';
        u1.EmployeeNumber = '873484';
        u1.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u1;
        return u1;
    }
            private static User getUser2(){
        User u2 = new User();
        u2.FirstName = 'gili';
        u2.LastName = 'bmw';
        u2.Alias = 'gbmw';
        u2.Email = 'gili.bmw@accenture.com';
        u2.Username = 'gili.bmw@accenture.com';
        u2.CommunityNickname = 'gili.bmw';
        u2.TimeZoneSidKey = 'Australia/Sydney';
        u2.LanguageLocaleKey = 'en_US';
        u2.LocaleSidKey = 'en_AU';
        u2.EmailEncodingKey = 'ISO-8859-1';
        u2.EmployeeNumber = '873484';
        u2.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u2;
        return u2;
    }
}