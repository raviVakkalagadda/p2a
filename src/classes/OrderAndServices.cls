// Required for critical path calculations.  Groups order with related services and access to all orchestration processes
public class OrderAndServices {
    public csord__Order__c m_order;
    public List<csord__Service__c> m_services;
    public CSPOFA__Orchestration_Process__c m_orderProcess;
    public List<CSPOFA__Orchestration_Process__c> m_serviceProcesses;
    public Map<Id, List<CSPOFA__Orchestration_Step__c>> m_orderProcessSteps;
    public Map<Id, List<CSPOFA__Orchestration_Step__c>> m_serviceProcessSteps;
    public OrderAndServices(csord__Order__c order, List<csord__Service__c> services, CSPOFA__Orchestration_Process__c orderProcess, List<CSPOFA__Orchestration_Process__c> serviceProcesses, Map<Id, List<CSPOFA__Orchestration_Step__c>> orderProcessSteps, Map<Id, List<CSPOFA__Orchestration_Step__c>> serviceProcessSteps)
    {
        System.Debug('CREATING OrderAndServices OBJECT WITH: ');
        System.Debug('Order = '+order);
        System.Debug('Services = '+services);
        System.Debug('Order Process = '+orderProcess);
        System.Debug('ServiceProcesses = '+serviceProcesses);
        System.Debug('Order Steps = '+orderProcessSteps);
        System.Debug('Service Steps = '+serviceProcessSteps);
        m_order = order;
        m_services = services;
        m_orderProcess = orderProcess;
        m_serviceProcesses = serviceProcesses;
        m_orderProcessSteps = orderProcessSteps;
        m_serviceProcessSteps = serviceProcessSteps;
    }

}