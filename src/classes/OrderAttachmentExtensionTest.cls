@isTest(seealldata=true)
public class OrderAttachmentExtensionTest {
    static testmethod void emailgeneration() 
    {
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
        insert orderObj; 
        
        Test.startTest();
        
        ApexPages.StandardController con = new ApexPages.StandardController(orderObj);
        Attachment attach=new Attachment();     
        attach.Name='Unit Test Attachment';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=orderObj.id;
        insert attach;
        List<Attachment> attachmentList=[select id, name from Attachment where parent.id=:orderObj.id];
        system.assertequals(attach.name, attachmentList[0].name);
        system.assert(attach!=null);
        ApexPages.currentPage().getParameters().put('recordIdToDelete',attach.id);
        ApexPages.currentPage().getParameters().put('recordIdToEdit',attach.id);
        ApexPages.currentPage().getParameters().put('recordIdToView',attach.id);
                   
        
        OrderAttachmentExtension ordAttExtObj = new OrderAttachmentExtension(con);
        ordAttExtObj.getRetOrderAttachments();
        Boolean shouldRedirect=ordAttExtObj.shouldRedirect;
        ordAttExtObj.delRequest();
        ordAttExtObj.editRequest();
        ordAttExtObj.viewRequest();
        String redirectUrl=ordAttExtObj.redirectUrl;
        String redirectUr2=ordAttExtObj.redirectUrl2;
        Boolean shouldRedirect1 =ordAttExtObj.shouldRedirect2;
        Test.stopTest();
    }   
}