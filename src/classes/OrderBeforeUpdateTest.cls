@isTest(SeeAllData = false)
private class OrderBeforeUpdateTest {
    static Order__c ord;
    static Opportunity opp;
   // static Order_Line_Item__c olit;
    static Product2 prod;
    static User u;
    static Contact con;
    static Account acc;
    static testMethod void orderUpdate() {
            
          ord = getOrder();
          u = getUser();
          con = getContact();
          system.debug('OrderId******'+ord.Id);
         // olit = getOrderLineItem();
          ord.Technical_Sales_Contact__c = u.Id;
          ord.Technical_Specialist_Contact__c = u.Id; 
          ord.SD_PM_Contact__c = u.Id;
          ord.Customer_Technical_Contact__c = con.Id;
         // ord.SD_OM_Contact__c = u.Id;
         // olit.Line_Item_Status__c = 'Complete';
            system.assert(u!=null);
         
         try{
            //update olit;    
         }
         catch(DMLException e){
            system.debug('Error' +e.getMessage());
         }   
        //  system.debug('Status*********'+ olit.Line_Item_Status__c + 'Parent_ID********'+ olit.ParentOrder__c + 'Keymilestone*****'+olit.Key_Milestone__c + 'Supplier****'+olit.Supplier_Milestone__c);
          test.startTest();
            try{        
                    
                  update ord;
                
                }   
            catch(DMLException e){
                system.debug('Error' +e.getMessage());
                ErrorHandlerException.ExecutingClassName='TestOrderBeforeUpdate :orderUpdate';         
                ErrorHandlerException.sendException(e); 
            }  
            
           Test.stopTest();         
    }
    private static User getUser()
    {
        if(u == null){
            u = new User();
            u.FirstName = 'Test';
            u.LastName = 'SFDC';
            u.IsActive  = true;
            u.Username = 'Test.SFDC@team.telstra.com.evolution';
            u.Email = 'Test.SFDC@team.telstra.com';
            u.Region__c = 'US';
            u.TimeZoneSidKey = 'Australia/Sydney';
            u.LocaleSidKey = 'en_AU';
            u.EmployeeNumber = '123424'; 
            u.EmailEncodingKey = 'ISO-8859-1';
            u.LanguageLocaleKey = 'en_US';
            u.Alias = 'TSFDC';
            Profile p = [select id from Profile where name = 'TI Service Delivery'];
            u.ProfileId = p.id;
                  
            insert u;
            //list<u> prod = [select id,Name from u where FirstName = 'Test'];
        //system.assertEquals(u.Name , prod[0].Name);
        System.assert(u!=null); 
        
        }      
        return u;
    }
    private static void getQuote(){
        Quote__c q = new Quote__c();
        opp = getOpportunity();
        q.Opportunity__c = opp.Id;
        q.Name = 'Test Quote';
        insert q;
        system.assert(q!=null);
        //return q;
    }
    private static void getOpportunityLineItem(){
        
        //List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';       
        
        insert oli;
        system.assert(oli!=null);
    }
    
    
     private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        system.assert(p!=null);
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        if(prod == null){
            prod = new Product2();
            prod.Name = prodName;
            prod.ProductCode = prodName;
            prod.Key_Milestone__c = false;
            prod.Supplier_Milestone__c = true;
            prod.Product_ID__c = 'GIP';
            insert prod;
            system.assert(prod!=null);
        }
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        system.assert(p!=null);
        return p;    
    }
     private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        insert b;
        system.assert(b!=null);
        return b;
    }
     /* private static Order_Line_Item__c getOrderLineItem(){
        if(olit == null){
        olit = new Order_Line_Item__c();
        prod = getProduct('Global IPVPN');
        ord = getOrder();
        olit.ParentOrder__c = ord.Id;
        olit.Product__c = prod.Id;
        olit.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        olit.Order_Placed_with_Supplier__c = system.today();
        olit.Supplier_Bill_Start_Date__c = system.today();
        olit.Supplier_Cancel_Date__c = system.today();
        olit.Supplier_Commitment_Date__c = system.today();
        olit.Standard_Delivery_Date__c = system.today();
        olit.Supplier_Order_Accepted_Date__c = system.today();
        olit.Actual_Supply_Date__c = system.today();
        olit.OrderType__c = 'New Provide';
        olit.CPQItem__c = '1';
        olit.Line_Item_Status__c = 'new';
         olit.Is_GCPE_shared_with_multiple_services__c = 'No'; 
        //oli.Key_Milestone__c ='';
      
       // insert olit;
        }
       /* system.debug('Milestone *' +olit.Supplier_Milestone__c);
        system.debug('Product Id' +olit.Product__c);
        
        return olit;*/
   // }
    private static Order__c getOrder(){
        if(ord == null){
            ord = new Order__c();
            Opportunity o = getOpportunity();
            acc = getAccount();
            u = getUser();
            ord.Opportunity__c = o.Id;
            ord.OwnerId = u.Id;
            ord.OLIUpdateRequired__c = false;
            ord.Account__c = acc.Id;
            insert ord; 
            system.assert(ord!=null);
        }
        return ord;
    }
    private static Contact getContact(){
        if(con == null){
            acc = getAccount();
            Country_Lookup__c c = getCountry();
            con = new Contact();
            con.LastName = 'Tech';
            con.AccountId = acc.Id;
            con.Contact_Type__c = 'Technical';
            con.Country__c = c.Id;
            con.Primary_Contact__c = true;
            con.MobilePhone = '123333';
            insert con;
            system.assert(con!=null);
        }
        return con;
    }
    private static Opportunity getOpportunity(){
        if(opp == null){
        opp = new Opportunity();
        acc= getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acc.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        opp.Contract_Signed_Date__c =system.today();
        opp.Signed_Contract__c = 'Yes';
        opp.Signed_Contract_Order_form_Recieved_Date__c = system.today();
        opp.Upload_signed_order_Contract__c = true;
        con = getContact();
        opp.Customer_Primary_Contact__c = con.id;
        u = getUser();
        opp.Technical_Sales_Contact__c = u.id;
        insert opp;
        system.assert(opp!=null);
        }
        return opp;
    }
     private static Account getAccount(){
        if(acc == null){
            acc = new Account();
            Country_Lookup__c cl = getCountry();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            acc.Customer_Legal_Entity_Name__c = 'Sample Entity';
            acc.Country__c = cl.Id;
            acc.Account_ID__c='122';
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c=True;
            acc.Account_Status__c='Active';
            insert acc;
            system.assert(acc!=null);
            List<Account> ac = [Select id,name from Account where name = 'Test Account Test 1' ];
            system.assertequals(acc.name,ac[0].name);
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c cl = new Country_Lookup__c();
       // cl.CCMS_Country_Code__c = 'IND';
       // cl.CCMS_Country_Name__c = 'ARMENIA';
        cl.Name = 'ARMENIA';
        cl.Country_Code__c = 'AM';
        insert cl;
        system.assert(cl!=null);
        return cl;
    } 
}