public Without Sharing class OrderConfigPricingValActionItem {
  //email notification when Order Config gets created with status unassigned
 public void SendEmailforOrderConfigAIonInsert(List<Action_Item__c> newActionItems){
   try{
    Id OwnerId;
    EmailContentUtility emailutil=new EmailContentUtility();
     List<String> toAddressList=new List<String>();
    for(Action_Item__c actionItemObj:newActionItems){
    if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && actionItemObj.Status__c=='Unassigned'){
        OwnerId=actionItemObj.ownerId;
        toAddressList=emailutil.getEmailAddess(OwnerId).split(':', 0);
        SendEmailforOrderConfigUnassigned(newActionItems,'Order_Support_Action_Item_to_Unassigned1','Order Desk Support',toAddressList);
    }
    }
    
    }catch(Exception ex){
        /*
        * Added Error handling on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:SendEmailforOrderConfigAIonInsert';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
        system.debug('Ex='+ex);
    }
}

public void rejectOrderConfigfromPricingAI(List<Action_Item__c> newActionItems, Map<Id, Action_Item__c> newActionItemsMap, Map<Id, Action_Item__c> oldActionItemsMap){
    try{
    List<Action_Item__c> AIlist=new List<Action_Item__c>();
    Set<Id> AISet=new Set<Id>();
    ID OwnerId;
    List<String> toAddressList=new List<String>();
    List<String> ccAddressList=new List<String>();
    String Assignedto=null;
    for(Action_Item__c actionItemObj:newActionItems){
  if((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation') )&& actionItemObj.Status__c!=null && actionItemObj.Status__c!=oldActionItemsMap.get(actionItemObj.id).Status__c && actionItemObj.Status__c=='Rejected'){
        toAddressList.add(actionItemObj.Opportunity_Email__c);
        Assignedto=actionItemObj.Assigned_User__c;
        system.debug('opportunity email====='+actionItemObj.Opportunity_Email__c);
        //OwnerId=actionItemObj.ownerId;
        if(actionItemObj.Pricing_Validation_Order_Configuration__c!=null){AISet.add(actionItemObj.Pricing_Validation_Order_Configuration__c);}
    }
    }
    
    
  if(AISet.size()>0){
    AIlist=[select Status__c,ownerId,Assigned_User__c,Approved_by_Email__c,Id,Commercial_Impact__c,Account_Owner_Region__c,Quote_Subject__c,Account_Name__c,Due_Date__c,Opportunity_Name__c,Opportunity_id__c,Name,Order_Approval_Comments__c from Action_Item__c where Id IN:AISet];
    EmailContentUtility emailutil=new EmailContentUtility();
    
    for (Action_Item__c AIObj:AIlist){
        AIObj.Status__c='Rejected';
        OwnerId=AIObj.ownerId;
        ccAddressList=emailutil.getEmailAddess(OwnerId).split(':', 0);
         system.debug('AIObj.Assigned_User__c====='+ccAddressList);
        if(Assignedto!=null){ccAddressList.add(Assignedto);}
        system.debug('AIObj.Assigned_User__c====='+ccAddressList);
    }
    
   if(AIlist.size()>0){ if(!Util.rjectionEmailforOD){
    SendEmailforPricingValRejection(AIlist,'Pricing_Validation_Action_Item_Rejected','GPD',ccAddressList,toAddressList);
     }
     Util.rjectionEmailforPV=true;update AIlist;
     
   }
  } 
  }catch(Exception ex){
        /*
        *Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:rejectOrderConfigfromPricingAI';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
        system.debug('ex='+ex);
    
  }
    
    
    
}

public void rejectPricingFromOrderConfigAI(List<Action_item__c> newActionItems, Map<Id,Action_item__c> newActionItemsMap, Map<Id,Action_item__c> oldActionItemsMap){
    try{
    List<Action_Item__c> AIlist=new List<Action_Item__c>();
    Set<Id> AISet=new Set<Id>();
    Set<Id> closedAI=new Set<Id>();
    List<String> ccAddressList=new List<String>();
    List<String> toAddressList=new List<String>();
     for(Action_Item__c actionItemObj:newActionItems){
    if((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support'))&& actionItemObj.Status__c!=null && actionItemObj.Status__c!=oldActionItemsMap.get(actionItemObj.id).Status__c && actionItemObj.Status__c=='Rejected'){
        system.debug('=====actionItemObj.Created_By_Email__c===='+actionItemObj.Created_By_Email__c);
        toAddressList.add(actionItemObj.Created_By_Email__c);
        if(actionItemObj.Pricing_Validation_Order_Configuration__c!=null){AISet.add(actionItemObj.Pricing_Validation_Order_Configuration__c);}
    }
     }
      if(AISet.size()>0){
    AIlist=[select Id,Status__c,Assigned_User__c,Approved_by_Email__c,IsRejectedFromOrderConfig__c from Action_Item__c where Id IN:AISet];
    
  
    for (Action_Item__c AIObj:AIlist){
        AIObj.Status__c='Rejected';
        AIObj.IsRejectedFromOrderConfig__c=true;
        system.debug('Approved Email===='+AIObj.Approved_by_Email__c);
       if(AIObj.Approved_by_Email__c!=null){ ccAddressList.add(AIObj.Approved_by_Email__c);}
         system.debug('Assigned Email===='+AIObj.Assigned_User__c);
        if(AIObj.Assigned_User__c!=null){ccAddressList.add(AIObj.Assigned_User__c);}
    }
    
   if(AIlist.size()>0) {
       if(!Util.rjectionEmailforPV){
    SendEmailforOrderConfigRejection(newActionItems,'Order_Support_Action_Item_to_Rejected','Request Order Support',ccAddressList,toAddressList);
     }
     Util.rjectionEmailforOD=true;update AIlist;
   
   }
  }
    }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:rejectPricingFromOrderConfigAI';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
        system.debug('ex===='+ex);
    }    
    
}
public void updateSalesJustificationtoPricing(List<Action_item__c> newActionItems, Map<Id,Action_item__c> newActionItemsMap, Map<Id,Action_item__c> oldActionItemsMap){
   try{ List<Action_Item__c> AIlist=new List<Action_Item__c>();
    Set<Id> AISet=new Set<Id>();
    String salesComment='';
    for(Action_Item__c actionItemObj:newActionItems){
    if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && actionItemObj.Support_Details__c!=null){
     if(actionItemObj.Pricing_Validation_Order_Configuration__c!=null){salesComment=actionItemObj.Support_Details__c;AISet.add(actionItemObj.Pricing_Validation_Order_Configuration__c);}
    }
     }
      if(AISet.size()>0){
    AIlist=[select Id,Sales_Justification_OS__c from Action_Item__c where Id IN:AISet];
    
  
    for (Action_Item__c AIObj:AIlist){
         AIObj.Sales_Justification_OS__c=salesComment;
    }
    
   if(AIlist.size()>0) {update AIlist;
   }
  }
   }catch(Exception ex){
    /*
    * Added Error handling statements on 25th Sep.
    */
    ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:updateSalesJustificationtoPricing';
    ErrorHandlerException.objectList=newActionItems;
    ErrorHandlerException.sendException(ex);
    system.debug('ex='+ex);
   }    
    
}

 public void unAssignAgainPricingValAI(List<Action_item__c> newActionItems, Map<Id,Action_item__c> newActionItemsMap, Map<Id,Action_item__c> oldActionItemsMap){
   try{
      Set<Id> closedAI=new Set<Id>();
    List<Action_Item__c> AIlist=new List<Action_Item__c>();
    Id OwnerId;
    EmailContentUtility emailutil=new EmailContentUtility();
     List<String> toAddressList=new List<String>();
    for(Action_Item__c actionItemObj:newActionItems){
  if((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support'))&& actionItemObj.Commercial_Impact__c=='Yes'){
        //actionItemObj.Status__c='Closed – Non Commercial';
        if(actionItemObj.Pricing_Validation_Order_Configuration__c!=null){closedAI.add(actionItemObj.Pricing_Validation_Order_Configuration__c);}
    }
    
    }
    if(closedAI.size()>0){
    AIlist=[select Id,Commercial_Impact__c,Status__c,ownerId,Assigned_User__c,Approved_by_Email__c from Action_Item__c where Id IN:closedAI and Commercial_Impact__c=:'No'];
    
  
    for (Action_Item__c AIObj:AIlist){
        AIObj.Status__c='Unassigned';
        AIObj.Commercial_Impact__c='Yes';
        OwnerId=AIObj.ownerId;
        AIObj.Date_Time_Opened__c=system.now();
        toAddressList=emailutil.getEmailAddess(OwnerId).split(':', 0);
    }
    
   if(AIlist.size()>0){ update AIlist;
   SendEmailforPricingConfigUnassigned(newActionItems,'Pricing_Validation_Action_Item_UnAssigned','GPD',toAddressList);
   }
  }
   }catch(Exception ex){
            /*
            * Added Error handling statements on 25th Sep.
            */
            ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:unAssignAgainPricingValAI';
            ErrorHandlerException.objectList=newActionItems;
            ErrorHandlerException.sendException(ex);
            system.debug('ex='+ex);
    } 
 
  
 }
 
  public void unCommercialIMPACTUpdateAI(List<Action_item__c> newActionItems, Map<Id,Action_item__c> newActionItemsMap, Map<Id,Action_item__c> oldActionItemsMap){
   try{
    Set<Id> closedAI=new Set<Id>();
    List<Action_Item__c> AIlist=new List<Action_Item__c>();
    String OwnerId;
    List<String> toAddressList=new List<String>();
    EmailContentUtility emailutil=new EmailContentUtility();
    for(Action_Item__c actionItemObj:newActionItems){
  if((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support')) && actionItemObj.Commercial_Impact__c=='No'){
        //actionItemObj.Status__c='Closed – Non Commercial';
        if(actionItemObj.Pricing_Validation_Order_Configuration__c!=null){closedAI.add(actionItemObj.Pricing_Validation_Order_Configuration__c);}
    }
    
    
    
    }
    if(closedAI.size()>0){
    AIlist=[select Id,ownerId,Account_Owner_Region__c,Name,Order_Approval_Comments__c,Quote_Subject__c,Account_Name__c,Due_Date__c,Opportunity_Name__c,Opportunity_id__c,Commercial_Impact__c,Status__c,Assigned_User__c,Approved_by_Email__c from Action_Item__c where Id IN:closedAI];
    
  
    for (Action_Item__c AIObj:AIlist){
        AIObj.Status__c='Closed – Non Commercial';
        AIObj.Commercial_Impact__c='No';
        OwnerId=AIObj.ownerId;
       }
    
   if(AIlist.size()>0){ update AIlist;
     
      toAddressList=emailutil.getEmailAddess(OwnerId).split(':', 0);
    SendEmailforPricingConfigCommercialImpact(AIlist,'Pricing_Validation_Action_Item_Assigned_Non_Commerical','GPD',toAddressList);
   }
  }
   
   }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:unCommercialIMPACTUpdateAI';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
        system.debug('ex='+ex);
   }
  
 }
 public void approveOrderConfigPricingValAI(List<Action_item__c> newActionItems, Map<Id,Action_item__c> newActionItemsMap, Map<Id,Action_item__c> oldActionItemsMap){
    try{
      Set<Id> oppId=new Set<Id>();
     for(Action_Item__c actionItemObj:newActionItems){
    if(actionItemObj.Status__c!=null && actionItemObj.Status__c!=oldActionItemsMap.get(actionItemObj.id).Status__c && ((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation') && (actionItemObj.Status__c=='Rejected' || actionItemObj.Status__c=='Approved')) || (actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && (actionItemObj.Status__c=='Rejected' || actionItemObj.Status__c=='Completed')))){
        actionItemObj.Approved_By__c =userinfo.getUserId();
        actionItemObj.Approved_by_Email__c =userinfo.getUserEmail();
        actionItemObj.Approved_Date__c =system.now();
        actionItemObj.Completed_By_Order__c=userinfo.getUserId();
        actionItemObj.Completion_Date__c=system.today();
    }
    if((actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && actionItemObj.Status__c!='Completed' && actionItemObj.Status__c!='Rejected') || (actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation') && actionItemObj.Status__c!='Approved' && actionItemObj.Status__c!='Rejected')){
        actionItemObj.Approved_By__c =null;
        actionItemObj.Approved_by_Email__c =null;
        actionItemObj.Approved_Date__c =null;
        actionItemObj.Completed_By_Order__c=null;
        actionItemObj.Completion_Date__c=null;
    }
     if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && actionItemObj.Status__c!=null && actionItemObj.Status__c!=oldActionItemsMap.get(actionItemObj.id).Status__c && actionItemObj.Status__c=='Completed'){
         if(actionItemObj.Opportunity__c!=null){
          oppId.add(actionItemObj.Opportunity__c);
         }
     }
    /* if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support') && actionItemObj.Completed_By_Order__c!=null && actionItemObj.Completed_By_Order__c!=oldActionItemsMap.get(actionItemObj.id).Completed_By_Order__c && actionItemObj.Status__c=='Completed'){
        actionItemObj.Completion_Date__c=system.today();
    }*/
   /*if((actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration') && actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation')){ 
    actionItemObj.Status__c='Closed - Non Commercial';
    actionItemObj.Commercial_Impact__c='No';
   }
   
    if((actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration') && actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support')){
        actionItemObj.Commercial_Impact__c='No'; 
    } */
    
     }
    if(oppId.size()>0){
     List<Opportunity> oppList=[select id,Order_Support_AI_completed__c from Opportunity where ID in:oppId];
    for(Opportunity opp:oppList){
      opp.Order_Support_AI_completed__c=true;
      
    }
    update oppList;
    }
    }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:approveOrderConfigPricingValAI';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);    
    system.debug('ex='+ex);
   }
 }
 
 public void SendEmailforOrderConfigUnassigned(List<Action_Item__c> newActionItems,String Template,String quename,List<String> toAddressList){
   SendEmailNotification(newActionItems,Template,quename,null,toAddressList);
 }
 
 public void SendEmailforPricingValRejection(List<Action_Item__c> newActionItems,String Template,String quename,List<String> ccAddressList,List<String> toAddressList){
    
  SendEmailNotification(newActionItems,Template,quename,ccAddressList,toAddressList);   
    
 }
public void SendEmailforOrderConfigRejection(List<Action_Item__c> newActionItems,String Template,String quename,List<String> ccAddressList,List<String> toAddressList){
    
  SendEmailNotification(newActionItems,Template,quename,ccAddressList,toAddressList);
    
 }
 
 public void SendEmailforPricingConfigUnassigned(List<Action_Item__c> newActionItems,String Template,String quename,List<String> toAddressList){
    
  SendEmailNotification(newActionItems,Template,quename,null,toAddressList);  
    
 }
 public void SendEmailforPricingConfigCommercialImpact(List<Action_Item__c> newActionItems,String Template,String quename,List<String> toAddressList){
    
  SendEmailNotification(newActionItems,Template,quename,null,toAddressList);  
    
 }
 
 
 public void SendEmailNotification(List<Action_Item__c> newActionItems,String Template,String quename,List<String> ccAddressList,List<String> toAddressList){
    EmailContentUtility.EmailContent emailObj=new EmailContentUtility.EmailContent();
    system.debug('===newActionItems==='+newActionItems);
    for(Action_Item__c AIObj:newActionItems){
    emailObj.accountOwnerRegion(AIObj.Account_Owner_Region__c)
            .subject(AIObj.Quote_Subject__c)
            .QueueName(quename)
            .accountName(AIObj.Account_Name__c)
            .ownerFullName()
            .duedate(String.valueOf(AIObj.Due_Date__c ))
            .opportunityName(AIObj.Opportunity_Name__c)
            .opportunityId(AIObj.Opportunity_id__c)
            .RecordId(AIObj.Id)
            .RecordName(AIObj.Name)
            .comments(AIObj.Order_Approval_Comments__c==null?'':AIObj.Order_Approval_Comments__c);
}
    system.debug('===ccAddressList==='+ccAddressList);
    (new EmailContentUtility(emailObj)).templateName(Template)./*toAddress(toAddressList).ccAddress(ccAddressList).*/objectType(Action_item__c.SobjectType).formnSendEmail(toAddressList,ccAddressList);
 }
 public void updateOrderConfigAIDetailsOnInsert(List<Action_Item__c> newActionItems){
 try{
    Map<Id,Account> accMap=new Map<Id,Account>([select id,Customer_Type__c,Owner.name,Customer_Type_New__c,Telstra_Legal_Entity_Name__c,Customer_Legal_Entity_Name__c from Account where ID =:newActionItems[0].Account__c]);
    Map<Id,Opportunity> oppMap=new Map<Id,Opportunity>([select id,ContractTerm__c,Owner.Email from opportunity where ID =:newActionItems[0].Opportunity__c]);
    List<cscfga__Product_Basket__c> prodbasketList=[select id from cscfga__Product_Basket__c where cscfga__Opportunity__c=:newActionItems[0].Opportunity__c and csordtelcoa__Synchronised_with_Opportunity__c=:true];
   system.debug('====accMap==='+accMap);
 system.debug('====oppMap==='+oppMap);
    for(Action_Item__c actionItemObj:newActionItems){
    if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support')){
      // actionItemObj.Status__c=/*(actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration')?'Closed - Non Commercial':*/'Unassigned';
       actionItemObj.Status__c='Unassigned'; //changes for CR52 by Swetha
      actionItemObj.Commercial_Impact__c=(actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration')?'No':'Yes';
     actionItemObj.Subject__c=actionItemObj.Quote_Subject__c;
      actionItemObj.Date_Time_Opened__c=system.now();
     actionItemObj.Contract_Term__c=oppMap.get(actionItemObj.Opportunity__c).ContractTerm__c;
     actionItemObj.Product_Basket__c=prodbasketList.size()>0?prodbasketList[0].id:null;
     actionItemObj.Account_Owner__c=accMap.get(actionItemObj.Account__c).Owner.NAme;
     actionItemObj.Customer_Type__c=accMap.get(actionItemObj.Account__c).Customer_Type__c;
     actionItemObj.Opportunity_Email__c=oppMap.get(actionItemObj.Opportunity__c).Owner.Email;
     actionItemObj.Telstra_Contracting_Entity__c=accMap.get(actionItemObj.Account__c).Telstra_Legal_Entity_Name__c;
    if(actionItemObj.Account_Owner_Region__c=='South Asia' || actionItemObj.Account_Owner_Region__c=='Australia'){
      actionItemObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Order Desk Support - South Asia & AUS');   
   }else{
      actionItemObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType, 'Order Desk Support - '+ actionItemObj.Account_Owner_Region__c);
   }
        
    }
    }
 }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:updateOrderConfigAIDetailsOnInsert';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
    system.debug('ex='+ex);
   }  
 }
      
public void createPricingValidationAI(List<Action_Item__c> newActionItems) {
 try{
  List<Action_Item__c> actionItemList=new List<Action_Item__c>();
  Id OwnerId;
  EmailContentUtility emailutil=new EmailContentUtility();
  List<String> toAddressList=new List<String>();
  Action_Item__c pricingValObj=null;
  for(Action_Item__c actionItemObj:newActionItems){
    if(actionItemObj.recordtypeid==RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Request Order Support')){
      pricingValObj=new Action_Item__c();
      pricingValObj.Account__c=actionItemObj.Account__c;
      pricingValObj.Opportunity__c=actionItemObj.Opportunity__c;
      pricingValObj.Opportunity_Email__c=actionItemObj.Opportunity_Email__c;
      pricingValObj.Quote_Subject__c ='Pricing Validation';
      pricingValObj.Subject__c=pricingValObj.Quote_Subject__c;
      pricingValObj.Date_Time_Opened__c=system.now();
      pricingValObj.Account_Owner__c=actionItemObj.Account_Owner__c;
      pricingValObj.Customer_Type__c=actionItemObj.Customer_Type__c;
      pricingValObj.Telstra_Contracting_Entity__c=actionItemObj.Telstra_Contracting_Entity__c;
      pricingValObj.Customer_Required_Date__c =actionItemObj.Customer_Required_Date__c ;
      pricingValObj.Pre_order_Required__c=actionItemObj.Pre_order_Required__c;
      pricingValObj.Due_Date__c=actionItemObj.Due_Date__c;
      pricingValObj.Contract_Term__c =actionItemObj.Contract_Term__c;
      pricingValObj.Sales_Justification_OS__c=actionItemObj.Support_Details__c;
      pricingValObj.Pricing_Validation_Order_Configuration__c =actionItemObj.id;
      pricingValObj.Product_Basket__c=actionItemObj.Product_Basket__c;
      pricingValObj.Status__c=(actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration')?'Closed - Non Commercial':'Unassigned';
      pricingValObj.Commercial_Impact__c=(actionItemObj.Opportunity_Order_Type__c=='Termination' || actionItemObj.Opportunity_Order_Type__c=='Reconfiguration')?'No':'Yes';
      pricingValObj.recordtypeid=RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType,'Pricing Validation');
      if((actionItemObj.Account_Owner_Region__c=='EMEA' || actionItemObj.Account_Owner_Region__c=='US') && actionItemObj.Customer_Type__c=='MNC'){
      pricingValObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'GPD ENT US EMEA');
      }else if ((actionItemObj.Account_Owner_Region__c=='North Asia' || actionItemObj.Account_Owner_Region__c=='South Asia' || actionItemObj.Account_Owner_Region__c=='Australia') && actionItemObj.Customer_Type__c=='MNC') {
      pricingValObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'GPD Asia ANZ');
      
      }
        else if (actionItemObj.Customer_Type__c=='GSP') {
         pricingValObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'GPD GW');   
        
        }else{
        pricingValObj.OwnerId = QueueUtil.getQueueIdByName(Action_Item__c.SObjectType,'GPD Asia ANZ');
        
      
        }
        OwnerId=pricingValObj.ownerId;
        toAddressList=emailutil.getEmailAddess(OwnerId).split(':', 0);
      actionItemList.add(pricingValObj);
    }
    
  }
  if(actionItemList.size()>0)
  {
  system.debug('actionItemList:'+actionItemList);
  insert actionItemList;
  AllActionItemTriggerHandler aiobj=new AllActionItemTriggerHandler();
  aiobj.runManagedSharing(actionItemList);
   updatingAILink(actionItemList[0].Pricing_Validation_Order_Configuration__c,actionItemList[0].id) ;
   List<Action_item__c> actionItemPricingValList=new List<Action_Item__c>();
   actionItemPricingValList=[select Id,Commercial_Impact__c,Account_Owner_Region__c,Quote_Subject__c,Account_Name__c,Due_Date__c,Opportunity_Name__c,Opportunity_id__c,Name,Order_Approval_Comments__c from Action_item__c where Id =:actionItemList[0].id];
   if(actionItemPricingValList[0].Commercial_Impact__c=='Yes'){
  SendEmailforPricingConfigUnassigned(actionItemPricingValList,'Pricing_Validation_Action_Item_UnAssigned','GPD',toAddressList);
   }
  }
 }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:createPricingValidationAI';
        ErrorHandlerException.objectList=newActionItems;
        ErrorHandlerException.sendException(ex);
     
    system.debug('ex='+ex);
   }
  
  
}
  
@future
public static void updatingAILink(Id actionItemId,Id actionItemId1){
  try{
    List<Action_Item__c> acclist=[select id,Pricing_Validation_Order_Configuration__c from Action_Item__c where id=:actionItemId];
   for(Action_Item__c actionItemObj:acclist){
    actionItemObj.Pricing_Validation_Order_Configuration__c=actionItemId1;
    
   }
   
   update acclist;
    }catch(Exception ex){
        /*
        * Added Error handling statements on 25th Sep.
        */
        ErrorHandlerException.ExecutingClassName='OrderConfigPricingValActionItem:updatingAILink';
        ErrorHandlerException.objectId=actionItemId;
        ErrorHandlerException.sendException(ex);
    system.debug('ex='+ex); 
    }   
}
  
    
}