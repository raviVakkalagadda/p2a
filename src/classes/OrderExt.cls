public with sharing class OrderExt {

	private csord__Order__c order;
    private ApexPages.StandardController stdController;

    public OrderExt(ApexPages.StandardController stdController) {
        this.order = (csord__Order__c)stdController.getRecord();
        this.stdController = stdController;
    }

    public PageReference cancel(){
        order = InFlightManager.cancelOrder(order);
        return stdController.save();
    }
}