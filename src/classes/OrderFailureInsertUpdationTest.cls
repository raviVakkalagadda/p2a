/*****
        Test class for the OrderFailureInsertUpdation trigger
   *****/
   @isTest(SeeAllData = false)
private class OrderFailureInsertUpdationTest {
   public static Order__c ord;
   public static City_Lookup__c city;
    static Account acc;
  /*  static testMethod void getorderOLIDetails() {      
        test.startTest();
      //  Order_Submission_Failures__c olifailures = getFailures();
      //  olifailures.error_Description__c = 'Failed';
      // update olifailures;
      test.stopTest();

    }*/
       
    
    public static Order_Submission_Failures__c getFailures(){
    Order_Submission_Failures__c ordfail = new Order_Submission_Failures__c();    
      //  ordfail.Failed_Order_Line_Item__c = getOrderLineItem().id;
        ordfail.Error_Description__c= 'Failed in CCMS';
        insert ordfail;
        system.assert(ordfail!=null);
        return ordfail;
       }
    
     private static Order__c getOrder(){
        if(ord == null){
        ord = new Order__c();
        Opportunity o = getOpportunity();
        ord.Opportunity__c = o.Id;
        ord.oliUpdateRequired__c = true;
        insert ord;
        system.assert(ord!=null);
        }
        return ord;
    }
    
    private static Order_line_Item__c getOrderLineItem(){
        
        Order_Line_Item__c ordli = new Order_Line_Item__c();
        ord = getOrder();
        Order_Line_Item__c oli1 = new Order_Line_Item__c();
        oli1.ParentOrder__c = ord.Id;
        system.debug('order****'+ ord.id);
        oli1.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli1.OrderType__c = 'New Provide';
        oli1.CPQItem__c = '1';
        oli1.Site_A__c = getSite().id;
        oli1.Line_Item_Status__c = 'Failed';
        oli1.Site_B__c = getSite().id;
        oli1.Is_GCPE_shared_with_multiple_services__c = 'No';
        //ordli.add(oli1);
        system.assert(ordli!=null);
       //insert oli1;
        return oli1;
    }
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = a.Id;
         //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
        opp.StageName = 'Identify & Define';
        opp.Stage__c='Identify & Define';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';

        insert opp;
        System.assert(opp!=null); 
        return opp;
    }
    private static Account getAccount(){
        if(acc == null){
            acc = new Account();
            Country_Lookup__c cl = getCountry();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c= true;
            acc.Account_Id__c ='12123';
            acc.Customer_Legal_Entity_Name__c='Test';
            acc.Account_Status__c = 'Active';
            insert acc;
            system.assert(acc!=null);
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c c2 = new Country_Lookup__c();
        //c2.CCMS_Country_Code__c = 'IND';
        //c2.CCMS_Country_Name__c = 'India';
        c2.Country_Code__c = 'xxx';
        insert c2;
        system.assert(c2!=null);
        return c2;
    } 
    private static Site__c getSite(){
        Site__c s1 = new Site__c();
        s1.name = 'Acc';
        s1.Country_Finder__c = getCountry().id ;
        s1.City_Finder__c = getCity().id;
        s1.Address_Type__c = 'Site Address';
        s1.Address1__c = 'abc,2nd street, Mughal block';
        s1.Address2__c = 'xyz,Master street,2nd crown block';
        s1.AccountId__c = getAccount().id;
        insert s1;
        system.assert(s1!=null);
        return s1;
        
    
    }
    private static City_Lookup__c getCity(){
        if(city == null){
        city = new City_Lookup__c();
        city.Generic_Site_Code__c = 'U12';
        city.name = 'Magu';
        city.City_Code__c='xyz';
        city.OwnerID = userinfo.getuserid();
        insert city;
        system.assert(city!=null);
     }
     return city;
     
    }
}