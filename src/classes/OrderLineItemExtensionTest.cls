@isTest(seealldata=false)
public class OrderLineItemExtensionTest {
   static testmethod void TestOrderLineItemExtension1()
    {
        // Country_Lookup__c Object instance and Mandatory attributes with values
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
        insert cityObj;
        list<City_Lookup__c> prod = [select id,Name from City_Lookup__c where Name = 'Bangalore'];
        system.assertEquals(cityObj.Name , prod[0].Name);
        System.assert(cityObj!=null); 
        
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
      
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj;
        Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
        insert siteObj;
    
        BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Pacnet Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
        List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
        insert   billProfObj; 
    
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
        insert orderObj; 
    
        order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
        Set<Id> orderset=new Set<Id>();
        List<Id> listid=new List<Id>();
            orderset.add(ordLineItemObj.ParentOrder__c);
            ListId.add(ordLineItemObj.Bill_Profile__c);
       // insert ordLineItemObj;
     
    
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
            email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
     Test.startTest();
        ApexPAges.StandardController sc = new ApexPages.StandardController(ordLineItemObj);
        ApexPages.currentPage().getParameters().put('id',ordLineItemObj.Id);
       // String id = ApexPages.currentPage().getParameters().get('id');
        String oliId = ApexPages.currentPage().getParameters().get('id');
        OrderLineItemExtension ordprodext=new OrderLineItemExtension(sc);
       // EmailGenerationClass emailgen = new EmailGenerationClass (); 
        ordprodext.getErrorMessage();
      //  ordprodext.SendEmail();
        ordprodext.BillingSME='sekhartest';
        ordprodext.BillingSMEEmail='sekhar@gmail.com';
        ordprodext.BillingSMEmobile='9994309902';
        ordprodext.BillName='testdata';
     Test.stopTest();
    }
    static testmethod void TestOrderLineItemExtension2(){
      
         Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        City_Lookup__c cityObj = new City_Lookup__c(Name ='Bangalore',City_Code__c='Beijing',
                                                    Generic_Site_Code__c ='HK#');
        insert cityObj;
        list<City_Lookup__c> prod1 = [select id,Name from City_Lookup__c where Name = 'Bangalore'];
        system.assertEquals(cityObj.Name , prod1[0].Name);
        System.assert(cityObj!=null);
        
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
      
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=accObj.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj;
        Site__c siteObj = new Site__c(Name='Test_site',Address1__c='43',Address2__c='Bangalore',Country_Finder__c=cntryLkupObj.Id,
                             City_Finder__c=cityObj.Id,AccountId__c=accObj.Id,Address_Type__c='Billing Address');
        insert siteObj;
    
        BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Telstra Limited',Account__c=siteObj.AccountId__c,
                                              Bill_Profile_Site__c=siteObj.Id, Start_Date__c= Date.today(),
                                             Invoice_Breakdown__c='Summary Page', First_Period_Date__c=Date.today(),
                                             Status__c='Active' ,Name ='test');
        List<BillProfile__c> lstBprofile=new List<BillProfile__c>();
        //lstBprofile.add();
        insert   lstBprofile; 
    
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
        insert orderObj; 
    
        order_line_item__c ordLineItemObj = new order_line_item__c(Bill_Profile__c=billProfObj.Id,
          Is_GCPE_shared_with_multiple_services__c='NA',ParentOrder__c = OrderObj.Id);
        Set<Id> orderset=new Set<Id>();
        List<Id> listid=new List<Id>();
            orderset.add(ordLineItemObj.ParentOrder__c);
            ListId.add(ordLineItemObj.Bill_Profile__c);
       // insert ordLineItemObj;
     
    
        Profile profObj = [select Id from Profile  where name ='Standard User'];
        User userObj = new User(profileId = profObj.id, username = 'mohantesh@telstra.com',
            email = 'mohantesh@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='1234589',Region__c='Australia');
            insert userObj;
     Test.startTest();
        ApexPAges.StandardController sc = new ApexPages.StandardController(ordLineItemObj);
        ApexPages.currentPage().getParameters().put('id',ordLineItemObj.Id);
       // String id = ApexPages.currentPage().getParameters().get('id');
        String oliId = ApexPages.currentPage().getParameters().get('id');
        OrderLineItemExtension ordprodext=new OrderLineItemExtension(sc);
       // EmailGenerationClass emailgen = new EmailGenerationClass (); 
        ordprodext.getErrorMessage();
      //  ordprodext.SendEmail();
        ordprodext.BillingSME='sekhartest';
        ordprodext.BillingSMEEmail='sekhar@gmail.com';
        ordprodext.BillingSMEmobile='9994309902';
        ordprodext.BillName='testdata';
     Test.stopTest();
    }
        
}