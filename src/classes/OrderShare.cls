/* Type   : Managed Apex Sharing for Order__c
// Author : Siddharth Sinha
// Date   : 08-06-2015
// The Order object's OWD has been made as Private
// This is to extend the Manual Sharing of the Order to Order's related Account Owner,CreatedBy,Opportunity Owner
*/ 

public class OrderShare{
List<Order__Share> OrdShares = new List<Order__Share>();
/** For Each record in the trigger perform the sharing tasks below **/
public void CustomShare(List<Order__c> olist){
for(Order__c t:olist){
//t.Customer_Type__c=t.Account_SFDCid__c;
//Account abc = [Select Customer_Type_New__c from Account where id =: t.Account_SFDCid__c];
//t.Customer_Type__c=abc.Customer_Type_New__c;
//system.debug('SidCust'+t.Customer_Type__c);

/** Create a new Action_Item_Share record to be inserted in to the Action_Item_Share table. **/
Order__Share Ord_CreatedBy = new Order__Share();
Order__Share Ord_AccountOwner = new Order__Share();
Order__Share Ord_OppOwner = new Order__Share();

/** Populate the Order_Share record with the ID of the record to be shared. **/
Ord_CreatedBy.ParentId = t.Id;
Ord_AccountOwner.ParentId = t.Id;
Ord_OppOwner.ParentId = t.Id;

//Debug Testing
system.debug('Sid.OwnerId'+t.CreatedById);
system.debug('SidAccOwnerID'+t.Account__r.OwnerId);
system.debug('SidOppOwnerID'+t.Opportunity__r.OwnerId);

/** Share to - Order Creator **/ 
Ord_CreatedBy.UserOrGroupId = t.CreatedById;
/** Specify that the Apex sharing reason **/
Ord_CreatedBy.RowCause = Schema.Order__Share.RowCause.Creator__c;

/** Share to - Account Owner **/ 
Ord_AccountOwner.UserOrGroupId = t.Account_Owner_Id__c;
/** Specify that the Apex sharing reason **/
Ord_AccountOwner.RowCause = Schema.Action_Item__Share.RowCause.Account_Owner__c;

/** Share to - Related Opportunity Owner **/ 
Ord_OppOwner.UserOrGroupId = t.Opportunity_Owner_Id__c;
/** Specify that the Apex sharing reason **/
Ord_OppOwner.RowCause = Schema.Action_Item__Share.RowCause.Opportunity_Owner__c;

   
/** Specify that the AI should have edit/read access for this particular Action_Item record. **/
Ord_CreatedBy.AccessLevel = 'Edit';
Ord_AccountOwner.AccessLevel = 'Read';
Ord_OppOwner.AccessLevel = 'Read';

 /** Add the new Share record to the list of new Share records. **/
OrdShares.add(Ord_CreatedBy);
OrdShares.add(Ord_AccountOwner);
OrdShares.add(Ord_OppOwner);
}
/** Insert all of the newly created Share records and capture save result **/
Database.SaveResult[] jobShareInsertResult = Database.insert(OrdShares,false);
}

}