/**    @author - Accenture    
       @date - 16-April-2012    
       @version - 1.0    
       @description - This class is used as controller to validate the order submission.
       
       @author - Accenture    
       @date - 24-April-2013    
       @version - 1.1    
       @description - Added the OLIUpdateRequired__c flag to fix the defect 3910.
      
**/


public class OrderSubmitValidator{
    /* NC Comment
    private class OrderSubmitValidatorException extends Exception {}
    public ID orderId {get;set;}
    Order__c ordr {get;set;}
    public Boolean flagtoCheckETC{get;set;}
    public OrderSubmitValidator(ApexPages.StandardController controller){
        this.orderId = Apexpages.currentPage().getParameters().get('id');
        List<String> fieldLst = new List<String>();
        fieldLst.add('Is_Order_Submitted__c');
        fieldLst.add('Opportunity__c');
        fieldLst.add('Clean_Order_Date__c');
        fieldLst.add('SRF_document_links__c');
        fieldLst.add('SD_PM_Contact__c');
        fieldLst.add('Customer_Primary_Order_Contact_Email__c');
        fieldLst.add('Status__c');
        fieldLst.add('Signed_Termination_Form_Attached__c');
        fieldLst.add('Full_Termination_Order__c');
        fieldLst.add('Requested_Termination_Date__c');
        fieldLst.add('Count_of_rejected_line_item__c');
        
        fieldLst.add('CreatedById');
        controller.addFields(fieldLst);
        ordr = (Order__c)controller.getRecord();
        orderId = ordr.id;
    }
    public OrderSubmitValidator(){}
    public PageReference validateOrder(){
        String errMsg = 'Order Submission Error:\n';
        String OtherCityErrMsg = 'Order has Site(s) which do not have City Code and Other City name is populated. Create/Populate City Codes to generate Service IDs successfully!!! Line Item Information: ';
        Boolean othrCityFlag = false;
        String siteACodeErrMsg = 'Site A codes are not populated in following line items:\n';
        Boolean siteAcodeFlag = false;
        String siteBCodeErrMsg = 'Site B codes are not populated in following line items:\n';
        Boolean sitBecodeFlag = false;
        String offPopErrMsg = 'Invalid Offnet POP codes are present in following line items: \n';
        Boolean offPopErrFlag = false;
        //String orderStatusFailure = 'Order Status cannot be Failed for an Order Submission:\n';
        //Boolean orderflag = false;
        String OLIStatusFailures = 'OrderLineItem Status must be updated to “New” prior to the resubmission of the order for the following line items:\n';
        Boolean OLIFlag = false;
        flagtoCheckETC=false;
       try{
        if(this.orderId==null){
            this.orderId = Apexpages.currentPage().getParameters().get('id');
            this.ordr = [select Requested_Termination_Date__c,Count_of_rejected_line_item__c,Full_Termination_Order__c,CreatedById,Signed_Termination_Form_Attached__c,Is_Order_Submitted__c,Status__c, Opportunity__c, Clean_Order_Date__c, SRF_document_links__c, SD_PM_Contact__c from Order__c where id =:this.orderId];
            //this.ordr = [select Requested_Termination_Date__c,Full_Termination_Order__c,CreatedById,Signed_Termination_Form_Attached__c,Is_Order_Submitted__c,Status__c, Opportunity__c, Clean_Order_Date__c, SRF_document_links__c, SD_PM_Contact__c from Order__c where id =:this.orderId];
        }
        system.debug('ordr = '+ordr);
       List<Order_Line_Item__c> updateOliLst = new List<Order_Line_Item__c>();
        List<Action_Item__c> ETCactionItemList= new List<Action_Item__c>();
       ETCactionItemList=[select Id,Order_Action_Item__r.CreatedById,Status__c,ownerId from Action_Item__c where Order_Action_Item__c=:ordr.Id and Subject__c='Request to Calculate Early Termination Charge'];
       for(Action_Item__c aiObj:ETCactionItemList){
        
        //Changes as part of IR 088 enhancements -- changing the condition to check if the action items are completed -- Bhargav
        if(aiObj.Status__c!='Completed'){
            flagtoCheckETC=true;
            break;
            
        }
        
       }

       for(Order_Line_Item__c oli: [select id, Product_Code__c,CPQItem__c,Line_Item_Status__c, Service_ID_Sequence_Number__c, Master_Service_ID__c, Primary_Service_ID__c,  (select id, Site_A_Code__c, Site_B_Code__c, Create_Primary_Service__c, Other_City_A__c, Other_City_B__c, CPQItem__c, Create_Path__c from Product_Configurations1__r) from Order_Line_Item__c where ParentOrder__c = :this.orderId]){
            /*for(Product_Configuration__c pc: oli.Product_Configurations1__r) {
                if(pc.Create_Path__c == 'TRUE'){
                        if(pc.Site_A_Code__c == null || pc.Site_A_Code__c==''){
                                siteACodeErrMsg+=pc.CPQItem__c+', ';
                                siteAcodeFlag = true;
                        }
                        else if(pc.Site_B_Code__c == null || pc.Site_B_Code__c==''){
                                siteBCodeErrMsg+=pc.CPQItem__c+', ';
                                sitBecodeFlag = true;
                        }
                        else if(pc.Site_A_Code__c == 'OTHER' || pc.Site_B_Code__c=='OTHER'){
                                OtherCityErrMsg+=pc.CPQItem__c+', ';
                                othrCityFlag = true;
                        }
                        
                         
                    else{
                        String Partrn = '\\D+';
                        Pattern pat = Pattern.compile(Partrn);
                        Matcher myMatcher = pat.matcher(pc.Site_A_Code__c);
                        boolean flag = false;

                        //if(!Pattern.matches(Partrn,pc.Site_A_Code__c))
                        if(!myMatcher.find())
                            flag = true;
                            
                        myMatcher = pat.matcher(pc.Site_B_Code__c);
                       if(!myMatcher.find())
                            flag = true;

                        if(flag == true){
                            offPopErrMsg+=pc.CPQItem__c+', ';
                            offPopErrFlag = true;
                        }
                        system.debug('offPopErrFlag ' +offPopErrFlag);
                    }
                  }
                   if(oli.Line_Item_Status__c == 'Failed'){
                                OLIStatusFailures+=oli.CPQItem__c+', ';
                                OLIFlag = true;
                        }
                }*/
                /* NC Comment
            }

       if(siteAcodeFlag==true ||sitBecodeFlag==true || othrCityFlag ==true|| offPopErrFlag ==true){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errMsg));
            if(othrCityFlag==true)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, OtherCityErrMsg.substring(0,OtherCityErrMsg.length()-2)));
            if(siteAcodeFlag==true)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, siteACodeErrMsg.substring(0,siteACodeErrMsg.length()-2)));
            if(sitBecodeFlag==true)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, siteBCodeErrMsg.substring(0,siteBCodeErrMsg.length()-2)));
            if(offPopErrFlag==true)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, offPopErrMsg.substring(0,offPopErrMsg.length()-2)));  
       }
       else{
            if(ordr.Is_Order_Submitted__c == TRUE)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'This order is already submitted, So it cant be re-submitted'));
                 else if(ordr.Full_Termination_Order__c && ordr.Status__c=='Cancelled')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Order Status must be updated to “New” prior to the submission of the order'));
               
             else if(flagtoCheckETC && ordr.Full_Termination_Order__c){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please complete Request to Calculate ETC Action Item for all line items before submitting an Order'));
                
            }
                
            else if(!ordr.Signed_Termination_Form_Attached__c && ordr.Full_Termination_Order__c){
           //Enhancements as part of IR 088 -- Defect#8340 -- Updated Error Message -- Raviteja
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Signed Termination Form Attached option has not been selected. Please ensure the Customer has signed the appropriate Termination form and that it’s attached to the Order.'));
                
            }
            
            else if(!ordr.Full_Termination_Order__c && (ordr.Opportunity__c == null ||ordr.Clean_Order_Date__c == null ||ordr.SRF_document_links__c == null ||ordr.SRF_document_links__c == '' || ordr.SD_PM_Contact__c == null))
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please fill the values for Opportunity, Clean Order Date, SRF document links, SD PM Name before submitting an Order'));
            else if(ordr.Full_Termination_Order__c && (ordr.Clean_Order_Date__c == null ||ordr.Requested_Termination_Date__c==null || ordr.SRF_document_links__c == null ||ordr.SRF_document_links__c == '' || ordr.SD_PM_Contact__c == null))
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Please fill the values for Clean Order Date, Requested Termination Date, SD PM Name before submitting an Order'));
            
            //Added as part of CR-455
            else if(ordr.Customer_Primary_Order_Contact_Email__c == null)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Customer Primary Contact email address is required for customer login to CSMC and C-3. Please update the record in Account Contacts.'));
             else if(ordr.Status__c == 'Failed')
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'The Order Status must be updated to “New” prior to the resubmission of the order'));
             else if(OLIFlag==true)
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, OLIStatusFailures.substring(0,OLIStatusFailures.length()-2)));
            else if(ordr.Status__c=='Reject' || ordr.Count_of_rejected_line_item__c > 0)
             //added as part of IR111 - to not allow submission in case of rejection
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Order cannot be submitted because some of the order line items or the whole order is rejected'));  
            else{
                ordr.Is_Order_Submitted__c = TRUE;
                ordr.OLIUpdateRequired__c = FALSE; // Added as part of defect 3910 fix.
                Util.orderSubmitFlag=false;
                update ordr;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Order is submitted successfully!!!'));
             }
       }
        }catch(DMLException dx){
            system.debug('DMLException inside generateTelstraServiceIds - '+dx.getMessage());
            CreateApexErrorLog.InsertHandleException('ApexClass','TelstraServiceIDController',dx.getMessage(),'Order',Userinfo.getName());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'DMLException'));
        }catch(Exception ex){
            system.debug('Exception inside generateTelstraServiceIds - '+ex.getMessage());
            CreateApexErrorLog.InsertHandleException('ApexClass','TelstraServiceIDController',ex.getMessage(),'Order',Userinfo.getName());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Exception'));

        }

       return null;
    }
    NC Comment*/

}