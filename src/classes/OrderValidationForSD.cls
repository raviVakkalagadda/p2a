/** @author - Accenture    
    @date - 07-Jan-2016    
    @version - 1.0    
    @description - This class is used as controller to validate the order submission to Service Delivery.
*/
public Class OrderValidationForSD{
    private class OrderSubmitValidatorException extends Exception {}
    public Id orderId {get;set;}
    public csord__Order__c ordr {get;set;}
  public Map<Id, Id> serviceIds = new Map<Id, Id>();
  public Map<Id, Id> orderIds = new Map<Id, Id>();
   
    public OrderValidationForSD(ApexPages.StandardController controller){
        this.orderId = Apexpages.currentPage().getParameters().get('Id');
        List<String> fieldLst = new List<String>();
        fieldLst.add('csord__Reference__c');
        fieldLst.add('Account_Owner_Region__c');
        fieldLst.add('csord__Account__r.Customer_Type__c');
        fieldLst.add('csord__Account__r.Preferred_Delivery_Queue__c');      
        fieldLst.add('Order_Submitted_to_SD__c');
        fieldLst.add('Status__c');
        fieldLst.add('csord__Order_Type__c');
        fieldLst.add('Requested_Termination_Date__c'); 
        fieldLst.add('Termination_Reason__c'); 
        fieldLst.add('Signed_Termination_Form_Attached__c');
        fieldLst.add('Is_Terminate_Order__c');
    if(!Test.isRunningTest()){ /** Added for test class coverage **/
      controller.addFields(fieldLst);
        }
        ordr = (csord__Order__c)controller.getRecord();
    orderIds.put(this.orderId, this.orderId);
    }
    
    public PageReference validateOrder(){
        List<csord__Order__c> csordlst = new List<csord__Order__c> ();
        List<csord__Service__c> CPSServList = new List<csord__Service__c>();
    List<csord__Service__c> servList = new List<csord__Service__c>();
        List<Case> EtcCaseList = null;
        Case caseObj = null;
        String errorMsg = '';
        Boolean missingCRD = false;
        Boolean error = false;
        Boolean missingSiteCode = false;
        Boolean missingSiteCodeNID_EMC = false;
        csord__Order__c orders = new csord__Order__c();
        Boolean isOrderChannelTypeDistributor = false;
        set<Id> PartnerAccountIds = new set<Id>();
        String OppPartnerResellerAccount = null; 
        Id accID;
        Id oppID;    

        /** 
         * ETC function: Suparna
         * Modified the method to generate Orchestration Process templete. //Sapan
         */
        List<csord__Service__c> srvcList = [select Id,Customer_Required_Date__c,Product_Code__c,Site_A_Code__c,Site_B_Code__c,A_Side_Site__c,Z_Side_Site__c,Bill_ProfileId__r.Billing_Entity__c,Billing_Entity_Region__c,AccountId__c,AccountId__r.OwnerId,Bill_ProfileId__c,AccountId__r.Region__c,AccountId__r.Owner.Name,Account_owner_region__c,Billing_Commencement_Date__c,Contract_Duration__c,Contract_Terms__c,Termination_Date__c,Termination_Reason__c,Product_Configuration_Type__c,Name,Customer_Type_New__c,Order_Type__c,csord__Order__c,Contract_Expiry_Date__c,Contract_Term__c,Product_Id__c,Order_Channel_Type__c from csord__Service__c where csord__Order__c =:this.orderId];
    
        /** Start GCC Reseller **/
        orders = [Select Id, Is_Terminate_Order__c, Reseller_Account_ID__c, csordtelcoa__Opportunity__c, csord__Account__c from csord__Order__c where Id =:this.orderId];
        
    for(csord__Service__c services :srvcList){
      if(services.Product_Id__c == 'GCC' && services.Order_Channel_Type__c == 'Distributor'){
        isOrderChannelTypeDistributor = true;
      } 
    }
         
    if(orders.csordtelcoa__Opportunity__c!=null){
      oppID = orders.csordtelcoa__Opportunity__c;
    }
    if(orders.csord__Account__c!=null){
      accID = orders.csord__Account__c;
    } 
    List<OpportunityPartner> PartnerArrayList = [Select AccountToID from OpportunityPartner where OpportunityId =:oppID and Role = 'GCC Resale'];

    if(!PartnerArrayList.isempty()){
      for(OpportunityPartner Opptypartner:PartnerArrayList){
        if(Opptypartner.AccountToID!=accID){
          PartnerAccountIds.add(Opptypartner.AccountToID);
        }
      }
    }
    Account[] AccArray = [Select Id, Name, Account_ID__c from Account where ID  IN :PartnerAccountIds and Billing_Status__c = 'Active' and Activated__c = true limit 1];

    if(AccArray.Size() > 0){
      OppPartnerResellerAccount = AccArray[0].name;
    }
        /** End GCC Reseller **/
        
        for(csord__Service__c srvc :srvcList){
            serviceIds.put(srvc.Id, srvc.Id);
            if(srvc.Order_Type__c == 'Terminate'){
                servList.add(srvc);
            }
      if(srvc.Product_Code__c == 'PS'){
        CPSServList.add(srvc);
      }
            
            if(srvc.Customer_Required_Date__c == null){
                errorMsg += srvc.Name +', ';
                missingCRD = true;                       
      } else if(Ordr.Is_Terminate_Order__c != true){
        if((srvc.Product_Code__c == 'NID' || srvc.Product_Code__c == 'EMC') 
          && (srvc.Site_A_Code__c == null || srvc.Site_A_Code__c.equalsIgnoreCase('Other') 
          || srvc.Site_B_Code__c == null || srvc.Site_B_Code__c.equalsIgnoreCase('Other') 
          || srvc.A_Side_Site__c == null || srvc.A_Side_Site__c.equalsIgnoreCase('Other') 
          || srvc.Z_Side_Site__c == null || srvc.Z_Side_Site__c.equalsIgnoreCase('Other'))){
            errorMsg += srvc.Name +', ';
            missingSiteCodeNID_EMC = true;              
        } else if(srvc.Site_A_Code__c == null || srvc.Site_A_Code__c.equalsIgnoreCase('Other') 
          || srvc.Site_B_Code__c == null || srvc.Site_B_Code__c.equalsIgnoreCase('Other') 
          || srvc.A_Side_Site__c == null || srvc.A_Side_Site__c.equalsIgnoreCase('Other') 
          || srvc.Z_Side_Site__c == null || srvc.Z_Side_Site__c.equalsIgnoreCase('Other')){
            errorMsg = srvc.Name +', ';
            missingSiteCode = true;             
        }   
      }                                            
        }

        String missingCtExpirydateServ = '';
        String missingRequestedTerminateServ = '';
        String missingTerminateReasonServ = '';
        String missingbillprofileServ = '';
        Boolean hasError = false;
        Map<Id,csord__Service__c> MissingServIdMap = new Map<Id,csord__Service__c>();
        if(servList.Size() > 0){
            for(csord__Service__c servOb :servList){
                if(servOb.Contract_Expiry_Date__c == null && servOb.Customer_Type_New__c != 'ISO'){
                    missingCtExpirydateServ+=servOb.Name+',';
                    MissingServIdMap.put(servOb.Id, servOb);
                }
                if(servOb.Termination_Date__c == null){
                    missingRequestedTerminateServ+=servOb.Name+',';
                }
                if(servOb.Termination_Reason__c == null){
                    missingTerminateReasonServ+=servOb.Name+',';
                }
                /** Added condition to check whether bill profile is available **/
                if(servOb.Bill_ProfileId__c == null && servOb.Customer_Type_New__c != 'ISO'){
                    missingbillprofileServ+=servOb.Name+',';
                }
            }

            if(missingCtExpirydateServ != ''){
                missingCtExpirydateServ = missingCtExpirydateServ.removeEnd(',');
            }
            if(missingRequestedTerminateServ != ''){
                missingRequestedTerminateServ = missingRequestedTerminateServ.removeEnd(',');
            }
            if(missingTerminateReasonServ != ''){
                missingTerminateReasonServ = missingTerminateReasonServ.removeEnd(',');
            }
            if(missingbillprofileServ != ''){
                missingbillprofileServ = missingbillprofileServ.removeEnd(',');
            }
        }

    String queueStr = '';
    if(Ordr.Order_Submitted_to_SD__c == true){
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Order was already assigned.'));
      Ordr.RecordTypeID = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation');
            csordlst.add(ordr);
            update csordlst;
            hasError = true;
        }
        else if(missingCRD == true && errorMsg != null){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'Please enter "Customer Required Date" for the following Services: ' +errorMsg));
            hasError = true;
        } 
        else if(missingSiteCodeNID_EMC == true && errorMsg != null){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The Order cannot be assigned to SD. "Customer Specific Site Code" is missing in the Site of the following NID/EMC Services (Send an e-mail to Global Serve to create a new City, if require): ' +errorMsg));
            hasError = true;
        }
        else if(missingSiteCode == true && errorMsg != null){ 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The Order cannot be assigned to SD. Please select a valid "City" in the Site of the following Services (Send an e-mail to Global Serve to create a new City, if require): ' +errorMsg));
            hasError = true;
        }
        else if(missingCtExpirydateServ != ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Stop Billing Date/Billing Commencement Date for '+missingCtExpirydateServ+' before assigning to SD Team.Email also will be notified to Billing SME for filling up missing information'));
            hasError = true;
        }
        else if((missingRequestedTerminateServ != '')){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Customer Requested Termination Date for '+missingRequestedTerminateServ+' before assigning to SD Team.'));
            hasError = true;
        }
        else if(missingTerminateReasonServ != ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter Termination Reason for '+missingTerminateReasonServ+' before assigning to SD Team.An email to notify Billing SME is sent out for filling up missing information.'));
            hasError = true;
        }
        else if(missingbillprofileServ != ''){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please fill the Bill Profile for '+missingbillprofileServ+' before assigning to SD Team.An email to notify Billing SME is sent out for filling up missing information.'));
            hasError = true;
        }
       else if(servList.Size() > 0 && !Ordr.Signed_Termination_Form_Attached__c){
           ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'The Signed Termination Form Attached option has not been selected. Please ensure the Customer has signed the appropriate Termination form and that it’s attached to the Order.'));
            hasError = true;
        }
        else if(!orders.Is_Terminate_Order__c && isOrderChannelTypeDistributor == true && orders.Reseller_Account_ID__c == null && OppPartnerResellerAccount == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Reseller name is required when ordering via Distributor Customer. Please add the activated Reseller’s customer account into Opportunity Partner section with GCC Resale Role.'));
            hasError = true;
        }
        else if(ordr.csord__Account__r.Preferred_Delivery_Queue__c != '' && ordr.csord__Account__r.Preferred_Delivery_Queue__c != null){ 
            queueStr = ordr.csord__Account__r.Preferred_Delivery_Queue__c;
        }
        else if(CPSServList != null && CPSServList.Size() > 0){ 
            queueStr = 'Consulting and Professional Services';
        }       
        else if(ordr.Account_Owner_Region__c == 'Australia'){ 
            queueStr = 'Service Delivery OM - Australia';
        }
        else if(ordr.Account_Owner_Region__c == 'EMEA'){
            queueStr = 'Service Delivery OM - EMEA';
        }
        else if(ordr.Account_Owner_Region__c == 'North Asia'){
            queueStr = 'Service Delivery OM - North Asia';
        }
        else if(ordr.Account_Owner_Region__c == 'South Asia'){
            queueStr = 'Service Delivery OM - South Asia';
        }
        else{
            queueStr = 'Service Delivery OM - US';
        }    

    if(!hasError){    
      if(Ordr.csord__Account__r.Customer_Type__c != 'ISO'){
        ordr.OwnerId = QueueUtil.getQueueIdByName(csord__Order__c.SObjectType, queueStr);
      }
      Ordr.Order_Submitted_to_SD__c = true;
      Ordr.Order_Submitted_to_SD_Date__c = system.now();
      Ordr.RecordTypeID = RecordTypeUtil.getRecordTypeIdByName(csord__Order__c.SObjectType, 'MonitorOrchestrationProcessCreation');
      csordlst.add(ordr);
      if(Ordr.csord__Account__r.Customer_Type__c != 'ISO'){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'The order has been assigned successfully to the Service Delivery \'' + queueStr + '\''));
      } else{
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'The order has been assigned successfully to the Service Delivery'));
      }
      
      if(servList.Size() > 0){
         for(csord__Service__c servOb :servList){
          if(servOb.Termination_Date__c == null){
            servOb.Termination_Date__c = Ordr.Requested_Termination_Date__c;
          }
          if(servOb.Termination_Reason__c == null){
            servOb.Termination_Reason__c = Ordr.Termination_Reason__c;
          }
        }

        EtcCaseList = new List<Case>();
        Set<ID> AIidset = new Set<ID>();
        for(csord__Service__c servObj :servList){
          if(servObj.Contract_Expiry_Date__c > servObj.Termination_Date__c){   
            caseObj = new Case();
            caseObj.OwnerId = UserInfo.getUserId();
            caseObj.Subject = 'Request to calculate ETC';
            caseObj.CS_Service__c = servObj.id;
            caseObj.AccountId = servObj.AccountId__c;
            caseObj.Status = 'Sales User in Progress';
            caseObj.Order__c = orderId;
            caseObj.Due_Date__c = system.today()+2;
            caseobj.billing_commencement_date__c = servObj.billing_commencement_date__c;
            caseobj.Contract_Term__c = String.valueOf(servObj.Contract_Term__c);
            caseobj.Termination_Reason__c = servObj.Termination_Reason__c;
            caseObj.RecordTypeId = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'RequestToETCCase');
            EtcCaseList.add(caseObj);
          }
        }

        if(EtcCaseList.Size() > 0){
          AIidset = getInsertedID(EtcCaseList);
        }
        if(AIidset.Size() > 0){
          EmailGenerationCls emailGenCls = new EmailGenerationCls(); 
          sendETCCalculationEmailtoCreator(AIidset);
        }
      }
      
      /** ETC Functiona : Suparna **/
      update csordlst;

      /** Orchestration Process templete creation for the services for which the orchastration process template creation has failed. //Sapan **/
      List<CSPOFA__Orchestration_Process__c> orchesList = [Select Id, csordtelcoa__Service__c, Order__c from CSPOFA__Orchestration_Process__c where csordtelcoa__Service__c IN :serviceIds.KeySet() or Order__c IN :orderIds.KeySet()];
      
      for(CSPOFA__Orchestration_Process__c orches :orchesList){
        if(orches.csordtelcoa__Service__c != null && serviceIds.get(orches.csordtelcoa__Service__c) != null){
          serviceIds.remove(orches.csordtelcoa__Service__c);
        } else if(orches.Order__c != null && orderIds.get(orches.Order__c) != null){
          orderIds.remove(orches.Order__c);
        }
      }
      if(!serviceIds.IsEmpty() || !orderIds.IsEmpty()){
        new OrchestrationProcessCreation().OrchestrationProcessTemplateCreation(serviceIds.Values(), orderIds.Values());
      }
    }
        
    if(missingCtExpirydateServ != null){
      /** 
       * Fix for 15780 starts
       * Developer: Ritesh
       * Root Cause: Each time contract expiry date is missing and user tries to assign order to SD, Billing case gets created even if there is already one.
       **/
      List<Case>BillingCase = [Select Id, CS_Service__c from Case where subject = 'Request For Billing Information' and Status = 'Assigned' and CS_Service__c IN:MissingServIdMap.KeySet()];
      for(Case c :BillingCase){
        if(MissingServIdMap.containsKey(c.CS_Service__c)){
          MissingServIdMap.remove(c.CS_Service__c);
        }               
      }
      if(MissingServIdMap.Size() > 0){
        assignCaseToBillingTeam(MissingServIdMap);
      }
    } 
    return null;
    }
    
  /**This method to create a case and send email notification to Billing team for services having no Billingcommencement Date/Contract Duration **/
    private void assignCaseToBillingTeam(Map<Id,csord__Service__c> MissingServIdMap){
    try{
      Map<String, QueueSobject> QueueMap = QueueUtil.getQueuesMapForObject(Case.SObjectType);
      List<Case> casetoBillingList = new List<Case>();
      Case caseObj = null;  
      
      for(Id servId :MissingServIdMap.KeySet()){
        caseObj = new Case();
        caseObj.CS_Service__c = servId;
        caseObj.Billing_Commencement_Date__c = MissingServIdMap.get(servId).Billing_Commencement_Date__c;
        caseObj.Contract_Term__c=String.valueof(MissingServIdMap.get(servId).Contract_Duration__c);    
        caseObj.AccountId = MissingServIdMap.get(servId).AccountId__c;
        caseObj.Status = 'Assigned';
        caseObj.Subject = 'Request For Billing Information';
        caseObj.RecordTypeID = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Case for Billing');
        
        /** Start IR215 **/
        String queueName = null;
        Boolean isPacnetEntity = false;
        String billingEntity = null;
        String BillingEntityRegion;
        String AccownerRegion;
 
        List<PACNET_Entities__c> allPacnetEntities = PACNET_Entities__c.getall().values();
        Map<String, PACNET_Entities__c> pacnetEntityMap = new Map<String, PACNET_Entities__c>();
        
        for(PACNET_Entities__c entity: allPacnetEntities){
          pacnetEntityMap.put(entity.PACNET_Entity__c,entity);
        }

        billingEntity = MissingServIdMap.containskey(servId)?MissingServIdMap.get(servId).Bill_ProfileId__r.Billing_Entity__c:null;
        BillingEntityRegion = MissingServIdMap.containskey(servId)?MissingServIdMap.get(servId).Billing_Entity_Region__c:null;
        AccownerRegion = MissingServIdMap.containskey(servId)?MissingServIdMap.get(servId).Account_owner_region__c:null;
      
        if(MissingServIdMap.get(servId).Bill_ProfileId__c!=null){
          if(billingEntity!=null){
            if(pacnetEntityMap.containskey(billingEntity)){
              isPacnetEntity = true;
            }
            if(isPacnetEntity){                  
              queueName = 'PACNET';              
            }
            else{                                
              queueName = BillingEntityRegion;        
            }
          }
          if(billingEntity == null){
            queueName = AccownerRegion;
                    }
        } 

        caseObj.OwnerId = QueueMap.get('Billing - '+queueName).QueueId;
        /** End IR 215 **/
        caseObj.Due_Date__c = System.today()+2;
        casetoBillingList.add(caseObj); 
      }
      
      Set<ID> AIidset = new Set<ID>();
      if(casetoBillingList.Size() > 0){
        AIidset = getInsertedID(casetoBillingList); 
        if(AIidset.Size() > 0){
          EmailGenerationCls emailGenCls = new EmailGenerationCls();  
          emailGenCls.getEmailList([Select Id, Due_Date__c, Requested_Termination_Date__c, Account.region__c, CaseNumber, CS_Service__r.Name, Billing_Commencement_Date__c, Contract_Term__c, Subject, Owner.Name, CS_Service__c, CreatedById from Case where Id IN :AIidset]);
        }  
      }  
    } catch(Exception ex){
      system.debug('====Exception Occurred In OrderValidationForSD===='+ex.getMessage());
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));  
      }
    }
  @testVisible
  private Set<Id> getInsertedID(List<Case> tobeInsertedCaseList){
    Database.SaveResult[] srList = Database.insert(tobeInsertedCaseList, false);
    Set<Id> AIidset = new Set<Id>();
    
    /** Iterate through each returned result **/
    for(Database.SaveResult sr :srList){
      if(sr.isSuccess()){
      /** Operation was successful, so get the ID of the record that was processed **/
      System.debug('Successfully inserted account. Account ID: ' + sr.getId());
      AIidset.add(sr.getId());
      } else{
      /** Operation failed, so get all errors **/
        for(Database.Error err :sr.getErrors()) {
          System.debug('The following error has occurred.');                    
          System.debug(err.getStatusCode() + ': ' + err.getMessage());
          System.debug('case fields that affected this error: ' + err.getFields());
        }
      }
    }
    return AIidset;
  }
  
  @testVisible
  private void sendETCCalculationEmailtoCreator(Set<ID> AIidset){
    List<Case> caseList = [Select Id, Order__c, Requested_Termination_Date__c, Account.Name, Due_Date__c, Account.region__c, CaseNumber, CS_Service__r.Name, Billing_Commencement_Date__c, Contract_Term__c, Subject, Owner.Name, CS_Service__c from Case where Id IN :AIidset];
        /** List of all emails that will be sent **/
        List<Messaging.SingleEmailMessage> mailToOrderOwnerList = new List<Messaging.SingleEmailMessage>();
        /** Email to be sent to the Order Owner **/
        Messaging.SingleEmailMessage mailToOrderOwner = new Messaging.SingleEmailMessage();
        /** /Setting the recepient address **/
        List<String> sendTo = new List<String>();
        sendTo.add(UserInfo.getUserEmail());
        mailToOrderOwner.setToAddresses(sendTo);
        /** Setting the address from which the email will be triggered **/
        mailToOrderOwner.setReplyTo(UserInfo.getUserEmail());
        /** Setting the email subject **/
        mailToOrderOwner.setSubject('Request for ETC Case(s) for ' + caseList[0].Account.Name + ' assigned to '+ UserInfo.getName() + ' due on ' + caseList[0].Due_Date__c.format());
        /** Setting the CSS styling for the table **/
        String mailBody='<style type="text/css">'+
        '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
        '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
        '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
        '</style>';
        /** Setting the email body - initial information **/
        mailBody+='Hi '+UserInfo.getName()+','
        +'<br/>'
        +'<br/>'
        +'The following Case(s) has been created against a Service and has been assigned to you.'
        +'<br/>'
        +'<br/>'
        +'Please indicate if ETC waiver is requested. Billing SMEs will be assigned once you click Assign to Billing button.'
        +'<br/>'
        +'<br/>'
        +'The details are as follows'
        +'<br/><br/>';
        /** Setting the email body - table headers **/
        mailBody+='<table class="tg">'
        +'<tr>'
        +'<th class="tg-031e"><strong>Case Number</strong></th>'
        +'<th class="tg-031e"><strong>Requested Termination Date</strong></th>'
        +'</tr>';
        /** Setting the email body - table data **/
        for(Case ai:caseList){
            mailBody+='<tr>'
            +'<td class="tg-031e"><a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+ai.Id+'>'+ai.CaseNumber+'</a></td>'
            +'<td class="tg-031e">'+ai.Requested_Termination_Date__c.format()+'</td>'
            +'</tr>';
        }
        mailBody+='</table>'
        +'<br/>'
        +'Please click on the link below to view the order details'
        +'<br/>'
        +'<br/>'
        +'<a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+caseList[0].Order__c+'>'+URL.getSalesforceBaseUrl().toExternalForm()+'/'+caseList[0].Order__c+'</a>'
        +'<br/>'
        +'<br/>'
        +'Thanks'
        +'<br/>'
        +'<br/>'
        +'<br/>'
        +'***This is an auto-generated notification, please do not reply to this email***';
        system.debug('Test Email Panda'+mailBody);
        mailToOrderOwner.setHtmlBody(mailBody);
        mailToOrderOwnerList.add(mailToOrderOwner);
        Messaging.sendEmail(mailToOrderOwnerList);
  }
}