@isTest
public with sharing class OrderValidationForSDTest{
    
    
    public static List<csord__Service__c> getService(Integer totalRecords, List<csord__Order_Request__c> OrdReqList,List<csord__Subscription__c> SUBList,List<csord__Order__c> Orders){
        List<csord__Service__c> ServForHdlrList= new List<csord__Service__c>();
         List<Product_Definition_Id__c> ProductDefinitionIds = new List<Product_Definition_Id__c>();
        List<CSPOFA__Orchestration_Process_Template__c>otList=new List<CSPOFA__Orchestration_Process_Template__c>();
        if(totalRecords > 0){
            for(Integer i=0; i < totalRecords; i++){
                
                Product_Definition_Id__c pdI = new Product_Definition_Id__c();
                pdI.Name = 'IPVPN_Product_ID';
                pdI.Product_Id__c = 'IPVPN';
                //insert pdI;
                Product_Definition_Id__c pdI1 = new Product_Definition_Id__c();
                pdI1.Name = 'VPLS_Product_ID';
                pdI1.Product_Id__c = 'VLM';
                //insert pdI1;
                Product_Definition_Id__c pdI2 = new Product_Definition_Id__c();
                pdI2.Name = 'IPC_Product_ID';
                pdI2.Product_Id__c = 'IPTS-C';
                //insert pdI2;
                
                ProductDefinitionIds.add(pdI);
                ProductDefinitionIds.add(pdI1);
                ProductDefinitionIds.add(pdI2);
                
                CSPOFA__Orchestration_Process_Template__c ot=new CSPOFA__Orchestration_Process_Template__c();
                ot.name='servicenew';
                otList.add(ot);
                
                csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c =  OrdReqList[0].id;
                ser.csord__Subscription__c = SUBList[0].id;
                ser.csord__Order__c = Orders[0].id;
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = '';
                ser.Primary_Service_ID__c='Test';
                ser.Group_Service_ID__c='Test';
                ser.Master_Service_ID__c='Test';
                ser.Cease_Service_Flag__c=true;
                ServForHdlrList.add(ser);
            }
            
            insert ProductDefinitionIds;
            insert otList;
            insert ServForHdlrList;   
        }
        return ServForHdlrList;
    }  
    
    
        
    static testmethod void OrderValidationForSDTest1(){
        
        P2A_TestFactoryCls.SetupTestData(); 
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<user> userList = P2A_TestFactoryCls.get_Users(1);
        List<account> acclist=P2A_TestFactoryCls.getAccounts(10);
        List<contact> conlist=P2A_TestFactoryCls.getContact(10, accList);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(10);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(10, AccList, countrylist);
        List<BillProfile__c> bplist=P2A_TestFactoryCls.getBPs(10, AccList, SiteList, conList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList =   P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList);    
        List<csord__service__c> servList = OrderValidationForSDTest.getservice(1,OrdReqList,subscriptionList,Orders); 
        servlist[0].AccountId__c=acclist[0].id;
        update servlist[0];
        bplist[0].Activated__c=true;
        bplist[0].Approved__c=true;
        bplist[0].Status__c='Active';
        bplist[0].Account__c=acclist[0].id;
        update bplist[0];
        
        userList[0].Region__c='Australia';
        update userList;
        
        acclist[0].OwnerId = userList[0].Id;
        update acclist;
       
        Orders[0].Order_Submitted_to_SD__c=false;
        Orders[0].Signed_Termination_Form_Attached__c=true;
        update Orders[0];
        
        servList[0].Billing_Commencement_Date__c = System.Today();
        servList[0].Stop_Billing_Date__c = System.Today();        
        servList[0].Customer_Required_Date__c = System.Today();
        servList[0].Termination_Date__c=System.today()-3;
        servList[0].Contract_Duration__c =1;
        servList[0].Termination_Reason__c='Test';
        servList[0].Bill_ProfileId__c=bplist[0].id;     
        update servList[0];
        Test.startTest(); 

        servList[0].Contract_Duration__c =12;
        update servList[0];
        PageReference pageRef12 = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef12);
            pageRef12.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc12 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl12 = new OrderValidationForSD(stc12);
            PageReference   objPageRef12 = objCtrl12.validateOrder();
         Test.stopTest();        
    }
    
   
    static testmethod void OrderValidationForSDTest2(){
         
        P2A_TestFactoryCls.SetupTestData(); 
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<account> acclist=P2A_TestFactoryCls.getAccounts(1);
        List<contact> conlist=P2A_TestFactoryCls.getContact(1, accList);
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(1, AccList, countrylist);
        List<BillProfile__c> bplist=P2A_TestFactoryCls.getBPs(1, AccList, SiteList, conList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);List<case> caseList = P2A_TestFactoryCls.getcase(1,acclist);
        
        List<CSPOFA__Orchestration_Process__c>processList =   P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = OrderValidationForSDTest.getservice(1,OrdReqList,subscriptionList,Orders); 
        servlist[0].AccountId__c=acclist[0].id;
        update servlist[0];
        bplist[0].Activated__c=true;
        bplist[0].Approved__c=true;
        bplist[0].Status__c='Active';
        bplist[0].Account__c=acclist[0].id;
        update bplist[0];
        
        servList[0].Termination_Date__c = System.today();
        update servList;
        
        caseList[0].Due_Date__c=System.today();
        caseList[0].CS_Service__c = servList[0].id;     
        update caseList;
        
        Set<ID> ids = new Set<ID>();
        ids.add(caseList[0].id);
        
        Test.startTest();
            PageReference pageRef = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl = new OrderValidationForSD(stc);
            PageReference   objPageRef = objCtrl.validateOrder();
       
        Orders[0].Order_Submitted_to_SD__c=false;
        Orders[0].Signed_Termination_Form_Attached__c=true;
        update Orders[0];
        
        servList[0].Billing_Commencement_Date__c = System.Today();
        servList[0].Stop_Billing_Date__c = System.Today();
        servList[0].Termination_Date__c=System.today()-3;
        servList[0].Contract_Duration__c =1;
        servList[0].Termination_Reason__c='Test';
        servList[0].Bill_ProfileId__c=bplist[0].id;
        servList[0].Product_Code__c = 'NID'; 
        servList[0].A_City_Code__c = '';        
        //servList[0].Site_B_Code__c = null;
        //servList[0].A_Side_Site__c = null;
        //servList[0].Z_Side_Site__c = null;        
        update servList[0];
            
       PageReference pageRef1 = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef1);
            pageRef1.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc1 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl1 = new OrderValidationForSD(stc1);
         
            PageReference   objPageRef1 = objCtrl1.validateOrder();
      /////////
      objCtrl1.sendETCCalculationEmailtoCreator(ids);
         Test.stopTest();
    }
    
    static testmethod void OrderValidationForSDTest3(){
         
        P2A_TestFactoryCls.SetupTestData(); 
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<account> acclist=P2A_TestFactoryCls.getAccounts(1);
        List<contact> conlist=P2A_TestFactoryCls.getContact(1, accList);
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(1, AccList, countrylist);
        List<BillProfile__c> bplist=P2A_TestFactoryCls.getBPs(1, AccList, SiteList, conList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);    
        List<CSPOFA__Orchestration_Process__c>processList =   P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = OrderValidationForSDTest.getservice(1,OrdReqList,subscriptionList,Orders); 
        servlist[0].AccountId__c=acclist[0].id;
        update servlist[0];
        bplist[0].Activated__c=true;
        bplist[0].Approved__c=true;
        bplist[0].Status__c='Active';
        bplist[0].Account__c=acclist[0].id;
        update bplist[0];

        Test.startTest();
            PageReference pageRef = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl = new OrderValidationForSD(stc);
            PageReference   objPageRef = objCtrl.validateOrder();
       
        Orders[0].Order_Submitted_to_SD__c=false;
        Orders[0].Signed_Termination_Form_Attached__c=true;
        update Orders[0];
        
        servList[0].Billing_Commencement_Date__c = System.Today();
        servList[0].Stop_Billing_Date__c = System.Today();
        servList[0].Termination_Date__c=System.today()-3;
        servList[0].Contract_Duration__c =1;
        servList[0].Termination_Reason__c='Test';
        servList[0].Bill_ProfileId__c=bplist[0].id;
        servList[0].A_Side_Site__c = '';        
        update servList[0];
            
       PageReference pageRef1 = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef1);
            pageRef1.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc1 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl1 = new OrderValidationForSD(stc1);
            PageReference   objPageRef1 = objCtrl1.validateOrder();
      
         Test.stopTest();
    }   

    static testmethod void OrderValidationForSDTest4(){
         
        P2A_TestFactoryCls.SetupTestData(); 
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<account> acclist=P2A_TestFactoryCls.getAccounts(1);
        List<contact> conlist=P2A_TestFactoryCls.getContact(1, accList);
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(1, AccList, countrylist);
        List<BillProfile__c> bplist=P2A_TestFactoryCls.getBPs(1, AccList, SiteList, conList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);    
        List<CSPOFA__Orchestration_Process__c>processList =   P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = OrderValidationForSDTest.getservice(1,OrdReqList,subscriptionList,Orders); 
        servlist[0].AccountId__c=acclist[0].id;
        update servlist[0];
        bplist[0].Activated__c=true;
        bplist[0].Approved__c=true;
        bplist[0].Status__c='Active';
        bplist[0].Account__c=acclist[0].id;
        update bplist[0];

        Test.startTest();
            PageReference pageRef = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl = new OrderValidationForSD(stc);
            PageReference   objPageRef = objCtrl.validateOrder();
       
        Orders[0].Order_Submitted_to_SD__c=false;
        Orders[0].Signed_Termination_Form_Attached__c=true;
        Orders[0].csord__Order_Type__c= 'Terminate';
        update Orders[0];
        
        servList[0].Cease_Service_Flag__c = true; 
        servList[0].Billing_Commencement_Date__c = System.Today();
        servList[0].Stop_Billing_Date__c = System.Today();
        servList[0].Termination_Date__c=null;
        servList[0].Contract_Duration__c =1;
        servList[0].Termination_Reason__c=null;
        servList[0].Bill_ProfileId__c=null;
        update servList[0];
            
       PageReference pageRef1 = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef1);
            pageRef1.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc1 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl1 = new OrderValidationForSD(stc1);
            PageReference   objPageRef1 = objCtrl1.validateOrder();
      
         Test.stopTest();
    }

    static testmethod void OrderValidationForSDTest5(){
         
        P2A_TestFactoryCls.SetupTestData(); 
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        List<account> acclist=P2A_TestFactoryCls.getAccounts(1);
        List<contact> conlist=P2A_TestFactoryCls.getContact(1, accList);
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(1, AccList, countrylist);
        List<BillProfile__c> bplist=P2A_TestFactoryCls.getBPs(1, AccList, SiteList, conList);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(1);    
        List<CSPOFA__Orchestration_Process__c>processList =   P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(1,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);    
        List<csord__service__c> servList = OrderValidationForSDTest.getservice(1,OrdReqList,subscriptionList,Orders); 
        servlist[0].AccountId__c=acclist[0].id;
        update servlist[0];
        bplist[0].Activated__c=true;
        bplist[0].Approved__c=true;
        bplist[0].Status__c='Active';
        bplist[0].Account__c=acclist[0].id;
        update bplist[0];

        Test.startTest();
            PageReference pageRef = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef);
            pageRef.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl = new OrderValidationForSD(stc);
            PageReference   objPageRef = objCtrl.validateOrder();
       
        Orders[0].Order_Submitted_to_SD__c=true;
        Orders[0].Signed_Termination_Form_Attached__c=true;
        Orders[0].csord__Order_Type__c= 'Terminate';
        update Orders[0];
        
        servList[0].Cease_Service_Flag__c = true; 
        servList[0].Billing_Commencement_Date__c = System.Today();
        servList[0].Stop_Billing_Date__c = System.Today();
        servList[0].Termination_Date__c=null;
        servList[0].Contract_Duration__c =1;
        servList[0].Termination_Reason__c=null;
        servList[0].Bill_ProfileId__c=null;
        update servList[0];
            
       PageReference pageRef1 = Page.Order_Enrichment_Validation_Page;
            Test.setCurrentPage(pageRef1);
            pageRef1.getParameters().put('id',Orders[0].id);        
            ApexPages.StandardController stc1 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl1 = new OrderValidationForSD(stc1);
         
            PageReference   objPageRef1 = objCtrl1.validateOrder();
      
         Test.stopTest();
    }  
    static testmethod void OrderValidationForSDTest6(){
        P2A_TestFactoryCls.SetupTestData(); 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);  
        //List<csord__Service__c> serList1 = new List<csord__Service__c>();
        
            csord__Service__c ser1 = new csord__Service__c();
                ser1.Name = 'Test Service'; 
                ser1.csord__Identification__c = 'Test-Catlyne-4238362';
                ser1.csord__Order_Request__c =  OrdReqList[0].id;
                ser1.csord__Subscription__c = subscriptionList[0].id;
                ser1.csord__Order__c = Orders[0].id;
                ser1.RAG_Status_Red__c = false ; 
                ser1.RAG_Reason_Code__c = '';
                ser1.Primary_Service_ID__c='Test';
                ser1.Group_Service_ID__c='Test';
                ser1.Master_Service_ID__c='Test';
                ser1.Cease_Service_Flag__c=true;
                ser1.Customer_Required_Date__c = System.today() ;
                ser1.Product_Code__c = 'EMC';
                ser1.A_City_Code__c = null;             
                ser1.A_Side_Site__c=null;
                //ser1.Stop_Billing_Date__c=System.today();
                //ser1.Z_Side_Site__c=null; 
                //ser1.Z_City_Code__c = null;               
             insert ser1; 
                 
             ApexPages.StandardController stc13 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl12 = new OrderValidationForSD(stc13);
          //   objCtrl12.validateOrder(); 
              
    }   
    static testmethod void OrderValidationForSDTest8(){
        P2A_TestFactoryCls.SetupTestData(); 
        List<account> acclist=P2A_TestFactoryCls.getAccounts(10);
        List<contact> conlist=P2A_TestFactoryCls.getContact(10, accList);  
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(10);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(10, AccList, countrylist);       
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);  
        List<csord__Service__c> serList2 = P2A_TestFactoryCls.getService(1,OrdReqList,subscriptionList);
        List<BillProfile__c> bilprof = P2A_TestFactoryCls.getBPs(1,acclist,sitelist,conlist);
        
        serList2[0].Customer_Required_Date__c = System.today();
        serList2[0].Product_Code__c='sdfsdf';
        serList2[0].A_City_Code__c = 'zzzz';
        serList2[0].Stop_Billing_Date__c=System.today();
        serList2[0].Termination_Date__c=System.today();
        serList2[0].Termination_Reason__c='sdfds';
        serList2[0].AccountId__c=acclist[0].id;     
        serList2[0].Bill_ProfileId__c=bilprof[0].id;        
        update serList2;
        
        Orders[0].Order_Submitted_to_SD__c=false;
        Orders[0].Signed_Termination_Form_Attached__c=true; 
        Orders[0].Is_Terminate_Order__c=true;
        update Orders;      
        Test.startTest();   
             ApexPages.StandardController stc13 = new ApexPages.StandardController(Orders[0]);
            OrderValidationForSD objCtrl12 = new OrderValidationForSD(stc13);
           //  objCtrl12.validateOrder(); 
       Test.stopTest();
              
    }
    
    
    static testmethod void  OrderValidationForSDTest10(){
        
        P2A_TestFactoryCls.SetupTestData(); 
        
        string Id = userinfo.getUserId();
        
        user u1 = [select id, Region__c from user where id =: id];
        u1.Region__c = 'Australia';
        update u1;
        
        user u2 = [select id, Region__c from user where id =: id];
        u2.Region__c = 'EMEA';
        update u2;
        
        user u3 = [select id, Region__c from user where id =: id];
        u3.Region__c = 'North Asia';
        update u3;
        
        user u4 = [select id, Region__c from user where id =: id];
        u4.Region__c = 'South Asia';
        update u4;
        
        List<account> acclist=P2A_TestFactoryCls.getAccounts(10);
        acclist[0].OwnerId = u1.id;
        acclist[0].Region__c = 'UAE';
        acclist[0].Customer_Type_New__c = 'MNC';
        acclist[0].Customer_Type__c = 'MNC';
        update acclist[0];
        
        acclist[1].OwnerId = u2.id;
        acclist[1].Customer_Type__c = 'MNC';
        update acclist[1];
        
        acclist[2].OwnerId = u3.id;
        acclist[2].Customer_Type__c = 'MNC';
        update acclist[2];
        
        acclist[3].OwnerId = u4.id;
        acclist[3].Customer_Type__c = 'MNC';
        update acclist[3];
        
        
        Id RecordTypeIdCase = Schema.SObjectType.Case.getRecordTypeInfosByName().get('RequestToETCCase').getRecordTypeId();
         
        Id RecordTypeIdOrder = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Cease Order for SD').getRecordTypeId();
        Id RecordTypeIdOrdeEnrich = Schema.SObjectType.csord__Order__c.getRecordTypeInfosByName().get('Order Enrichment').getRecordTypeId();    
         
        
        List<contact> conlist=P2A_TestFactoryCls.getContact(10, accList);  
        List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(10);
        List<site__c> sitelist=P2A_TestFactoryCls.getsites(10, AccList, countrylist);       
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);       
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(4,OrdReqList);
        Orders[0].csord__Order_Type__c= 'Terminate';
        Orders[0].RecordTypeId = RecordTypeIdOrdeEnrich;
        update Orders[0];
        
        List<csord__Service__c> serList2 = P2A_TestFactoryCls.getService(1,OrdReqList,subscriptionList);
        List<BillProfile__c> bilprof = P2A_TestFactoryCls.getBPs(1,acclist,sitelist,conlist);
        
        csord__Service__c ser = new csord__Service__c();
        ser.Name = 'Test Service'; 
        ser.csord__Identification__c = 'Test-Catlyne-4238362';
        ser.csord__Order_Request__c =  OrdReqList[0].id;
        ser.csord__Subscription__c = subscriptionList[0].id;
        ser.csord__Order__c = Orders[0].id;
        ser.RAG_Status_Red__c = false ; 
        ser.RAG_Reason_Code__c = '';
        ser.Primary_Service_ID__c='Test';
        ser.Group_Service_ID__c='Test';
        ser.Master_Service_ID__c='Test';
        ser.Cease_Service_Flag__c=true;
        ser.Updated__c = true;
        ser.Product_Id__c = 'GCC';
        ser.Customer_Required_Date__c = Date.Today().addDays(15);
        ser.Product_Code__c = 'EMC';
        ser.A_City_Code__c= 'Other';
        ser.Z_City_Code__c = 'Other';
        ser.A_Side_Site__c = 'Other';
        ser.Z_Side_Site__c = 'Other';
        ser.Billing_Entity_Region__c = 'UAE';
        ser.AccountId__c = acclist[0].id;
        //ser.Account_owner_region__c = 'UAE';
        ser.Billing_Commencement_Date__c = Date.Today().addDays(15);
        ser.Contract_Duration__c = 12;
        ser.Termination_Date__c = Date.Today().addDays(20);
        ser.Termination_Reason__c = 'No response';
        ser.Stop_Billing_Date__c = null;
        ser.Contract_Term__c = 12;
        ser.Order_Channel_Type__c = 'Direct Sale';
        insert ser;
        
        Test.startTest();
        
        List<Case> EtcCaseList=new List<Case>();
        Case caseObj=new Case();
        caseObj.OwnerId=UserInfo.getUserId();
        caseObj.Subject='Request to calculate ETC';
        caseObj.CS_Service__c=ser.id;
        caseObj.AccountId=ser.AccountId__c;
        caseObj.Status='Sales User in Progress';
        caseObj.Order__c=Orders[0].id;
        caseObj.Due_Date__c=system.today()+2;
        caseobj.billing_commencement_date__c=ser.billing_commencement_date__c;
        caseobj.Contract_Term__c=String.valueOf(ser.Contract_Term__c);
        caseobj.Termination_Reason__c=ser.Termination_Reason__c;
        caseObj.RecordTypeId= RecordTypeIdCase;
        EtcCaseList.add(caseObj);
        insert EtcCaseList;
         
        ApexPages.StandardController stc13 = new ApexPages.StandardController(Orders[0]);
        OrderValidationForSD objCtrl12 = new OrderValidationForSD(stc13);            
        Set<Id> InsertedIds = objCtrl12.getInsertedID(EtcCaseList); 
            
       Test.stopTest();
        
    }
    
}