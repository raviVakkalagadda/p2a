@isTest(SeeAllData=false)
public with sharing class OrderValidationOnSubmitTest{ 
Private static csord__order__c ord;
 
 @istest 
  public static void  OrderValidationForSb(){
  P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
  
       P2A_TestFactoryCls.sampleTestData();
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name , 'TWI Singlehome');
        
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);        
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);     
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        proconfig[0].name ='TWI Singlehome';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name ,'TWI Singlehome');
        
        List<Pricing_Approval_Request_Data__c> papprovalist = P2A_TestFactoryCls.getProductonfigappreq(1,pblist,proconfig);        
        List<User> users = P2A_TestFactoryCls.get_Users(1);
      
     // Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
      
      /*csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId,
                                                    Is_Order_Submitted__c=true,
                                                    //Is_Order_Submitted__c = true,
                                                    SD_PM_Contact__c = null,
                                                    Clean_Order_Date__c = System.today(),
                                                    Status__c='Submitted',
                                                    Is_Terminate_Order__c = true,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
      insert order;*/
      
      csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        system.assertEquals(orderRequest.csord__Module_Name__c,'dummy');
        
      List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);   
      
        CSPOFA__Orchestration_Process_Template__c ot=new CSPOFA__Orchestration_Process_Template__c();
        ot.name='order_new';
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = orderRequest.id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        insert order;  
        system.assertEquals(order.name,'Test Order');
         
      csord__Order__c ord1 = new csord__Order__c();
                ord1 .csord__Identification__c = 'Test-JohnSnow-4238362';
               // ord.csord__Order_Request__c = ordreqlist[0].id;
                ord1.Order_Type__c ='Terminate';
                ord1.Clean_Order_Date__c = System.today();
                
                //insert ord;
            //Opportunity oppObj = OpportunityHelp.getOpportunity();    
            
    OrderValidationOnSubmit ovs = new OrderValidationOnSubmit(new ApexPages.StandardController(order)); 
    ovs.validateOrdertoSubmit(); 
}
@istest 
  public static void  OrderValidationForSbs(){
  P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
  
       P2A_TestFactoryCls.sampleTestData();
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name ,'TWI Singlehome');
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        proconfig[0].name ='TWI Singlehome';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name ,'TWI Singlehome');
        
        List<Pricing_Approval_Request_Data__c> papprovalist = P2A_TestFactoryCls.getProductonfigappreq(1,pblist,proconfig);
        
        List<User> users = P2A_TestFactoryCls.get_Users(1);
  
   // Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
   
     csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        system.assertEquals(orderRequest.csord__Module_Name__c,'dummy');
        
        
    csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = orderRequest.id,
                                                    Is_Order_Submitted__c=false,
                                                    SD_PM_Contact__c = users[0].id,
                                                    Clean_Order_Date__c = System.today(),
                                                    Status__c='Accepted',
                                                    Is_Terminate_Order__c = true,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
                                                    
    insert order; 
    system.assertEquals(order.name,'Test Order');   
      
     /* csord__Order__c ore = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId,
                                                    Is_Order_Submitted__c=false,
                                                    //Is_Order_Submitted__c = true,
                                                    SD_PM_Contact__c = null,
                                                    Clean_Order_Date__c = System.today(),
                                                    Status__c='Submitted',
                                                    Is_Terminate_Order__c = true,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
                                              
      insert ore;
      */
      OrderValidationOnSubmit ovst = new OrderValidationOnSubmit(new ApexPages.StandardController(order)); 
      ovst.validateOrdertoSubmit(); 
  }
  
 @istest 
  public static void  OrderValidationFor_Sbss_Test(){
  
    /*Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
      
      csord__Order__c ored = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId,
                                                    Is_Order_Submitted__c=false,
                                                    //Is_Order_Submitted__c = true,
                                                   
                                                    Status__c='Submitted',
                                                    Is_Terminate_Order__c = true,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
                                              
      insert ored;
     */
      
      csord__Order_Request__c orderRequest = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert orderRequest;
        system.assertEquals(orderRequest.csord__Module_Name__c,'dummy');
        
        
    csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = orderRequest.id,
                                                    Is_Order_Submitted__c=false,
                                                    Status__c='new',
                                                    Is_Terminate_Order__c = true,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
    insert order;  
      
      OrderValidationOnSubmit ovstt = new OrderValidationOnSubmit(new ApexPages.StandardController(order)); 
      ovstt.validateOrdertoSubmit(); 
  }  
  
   @istest 
  public static void  OrderValidationForSbss(){
  
    if(ord == null){
         ord  = new csord__order__c();
         List<CSPOFA__Orchestration_Process_Template__c> orchProcessTempt = P2A_TestFactoryCls.getOrchestrationProcess(1);
         List<CSPOFA__Orchestration_Process__c> orchProcess =  P2A_TestFactoryCls.getOrchestrationProcesss(1, orchProcessTempt);
         List<CSPOFA__Orchestration_Step__c> orchestrationStep = P2A_TestFactoryCls.getOrchestrationStep(1,orchProcess);
         
         List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
         //List<csord__Order__c> =  P2A_TestFactoryCls.getorder(1,OrdReqList);
         List<Account> Acclist = P2A_TestFactoryCls.getAccounts(1);
         List<Contact> ConList = P2A_TestFactoryCls.getContact(1, Acclist);
         List<User> Userlist = P2A_TestFactoryCls.get_Users(1);
         List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,Acclist);
    
     P2A_TestFactoryCls.sampleTestData();
        
         
      csord__Order_Request__c cord = new csord__Order_Request__c(name ='test',csord__Module_Name__c='test',csord__Module_Version__c='34');
      insert cord;
        
        
     csord__Order__c coObj=  new csord__Order__c(name = 'allorder',csord__Order_Type__c = 'Terminate',Status__c = 'New', Order_Type__c = 'type', csord__Identification__c = 'Test-JohnSnow-4238362',
            csord__Status2__c = 'Submitted',Order_Submitted_Date__c = System.now(),csord__Order_Request__c = cord.id,Order_Submitted__c = false,csordtelcoa__Opportunity__c = ListOpp[0].id);
            
     insert coObj; 
     
     Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.csord__Order__c; 
     Map<String,Schema.RecordTypeInfo> caseOrderRecordTypeInfo = cfrSchema.getRecordTypeInfosByName();

     Id rtId = caseOrderRecordTypeInfo.get('Cease Order').getRecordTypeId(); 
     
     ord.Name = 'allorder';
        ord.recordtypeID = caseorderRecordTypeInfo.get('Cease Order').getRecordTypeId();
        ord.Is_Terminate_order__c = false;
        ord.OldodrId__c= coObj.id;
        ord.csord__order_Request__c = cord.id;
        ord.csord__Identification__c = 'Test-JohnSnow-4238362';
        ord.csord__Account__c = Acclist[0].id;
        ord.csord__Status2__c = '';
        ord.csord__order_Type__c = 'Terminate';
        ord.Order_Submitted_Date__c = System.now();
        ord.Status__c = 'New';
        ord.Customer_Primary_order_Contact__c = ConList[0].id;
        ord.Technical_Sales_Contact__c = Userlist[0].id;
        ord.Customer_Technical_Contact__c = ConList[0].id;
        ord.Account_Manager_PC__c = Userlist[0].id;
        ord.Account_Manager_2__c = Userlist[0].id;
        ord.OwnerId =  Userlist[0].id;
        ord.Full_Termination__c=true;
        ord.Is_order_Submitted__c=false;
        ord.order_Submitted__c=false;
        ord.csordtelcoa__Opportunity__c = ListOpp[0].id;
        Insert ord;
        system.assertEquals(ord.Full_Termination__c,true);
        
        
      OrderValidationOnSubmit ovstt = new OrderValidationOnSubmit(new ApexPages.StandardController(ord)); 
      ovstt.validateOrdertoSubmit(); 
        
       }
 
     } 
         
   
 }