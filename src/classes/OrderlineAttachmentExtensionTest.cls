@isTest(seealldata=true)
public class OrderlineAttachmentExtensionTest {
  static testmethod void emailgeneration1() 
    {
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'test', Customer_Legal_Entity_Name__c = 'test');  
        insert accObj;
        Order__c orderObj = new Order__c(Requested_Termination_Date__c=Date.today(), Account__c =accObj.Id);
      insert orderObj; 
      
        System.assert(orderObj!=null); 
        
        Test.startTest();
        
        ApexPages.StandardController con = new ApexPages.StandardController(orderObj);
        Attachment attach=new Attachment();     
      attach.Name='Unit Test Attachment';
      Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
      attach.body=bodyBlob;
        attach.parentId=orderObj.id;
        insert attach;
        List<Attachment> attachmentList=[select id, name from Attachment where parent.id=:orderObj.id];
        ApexPages.currentPage().getParameters().put('recordIdToDelete',attach.id);
        ApexPages.currentPage().getParameters().put('recordIdToEdit',attach.id);
        ApexPages.currentPage().getParameters().put('recordIdToView',attach.id);
                   
        
        OrderLineItemAttachmentExtension  ordAttExtObj1 = new OrderLineItemAttachmentExtension (con);
        ordAttExtObj1.getRetOrderAttachments();
        Boolean shouldRedirect=ordAttExtObj1.shouldRedirect;
        ordAttExtObj1.delRequest();
        ordAttExtObj1.editRequest();
        ordAttExtObj1.viewRequest();
        String redirectUrl=ordAttExtObj1.redirectUrl;
        String redirectUr2=ordAttExtObj1.redirectUrl2;
        Boolean shouldRedirect1 =ordAttExtObj1.shouldRedirect2;
        Test.stopTest();
    }   
}