/*
Author: Nikola Culej - CloudSense
Create missing PCRs when configuration is added from MLE screen
This was added to fix defect 13495
*/
public class PCRsForMLEController {

private Map<String, String> params;
private String basketId;

public PCRsForMLEController(){
    params = System.currentPageReference().getParameters();
    if(params != null){
        basketId = params.get('basketId');
    }
}

public PageReference createPCRsForMLE(){
    createPCRsForMLE(basketId);
    return new PageReference('/' + basketId);
}


public Map<String, String> getParams(){
    return params;
}

public void createPCRsForMLE(String basketId){
    System.debug('**** Create PCRs for Basket ****');
    
    Set<Id> basketIds = new Set<Id>();
    basketIds.add(Id.valueOf(basketId));
    
    Map<Id,Id> pcMap = new Map<Id,Id>();
    
    List<csbb__Product_Configuration_Request__c> pcrs = new List<csbb__Product_Configuration_Request__c>();  
    List<Attachment> attachmentList = new List<Attachment>();
    Map<Id, List<Attachment>> pcrsAttachmentsMap = new Map<Id, List<Attachment>>();
    Map<Id, csbb__Product_Configuration_Request__c> pcrsMap = new Map<Id, csbb__Product_Configuration_Request__c>();
    
    List<csbb__Product_Configuration_Request__c> pcrsToAdd = new List<csbb__Product_Configuration_Request__c>();
    
    List<String> pdList = new List<String>{
        Product_Definition_Id__c.getvalues('ASBR_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c,
		Product_Definition_Id__c.getvalues('Rack_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('Cage_Private_Room_Definition_Id').Product_Id__c,
        Product_Definition_Id__c.getvalues('Cross_Connect_Definition_Id').Product_Id__c
    };
    
    Map<String, String> offerMap = new Map<String, String>{
        Product_Definition_Id__c.getvalues('IPVPN_Port_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('IPVPN_Port_withoutLL_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('VPLS_VLAN_Port_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('VPLS_VLAN_withoutLL_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('VPLS_Transparent_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('VPLS_Transparent_withoutLL_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('ASBR_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('ASBR_TypeA_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('SMA_Gateway_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('SMA_Gateway_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('VLANGroup_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('VLAN_Group_Offer_Id').Offer_Id__c,
		Product_Definition_Id__c.getvalues('Rack_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('Rack_For_Connected_Colo_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('Rack_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('Rack_For_Cage/Private_Room_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('Cage_Private_Room_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('Cage_Private Room_Offer_Id').Offer_Id__c,
        Product_Definition_Id__c.getvalues('Cross_Connect_Definition_Id').Product_Id__c => Offer_Id__c.getvalues('Cross_Connect_Offer_Id').Offer_Id__c
    };
    
    List<cscfga__Product_Configuration__c> pcList = [SELECT Id, Name, cscfga__Product_Basket__c, cscfga__Root_Configuration__c, cscfga__Product_Definition__c, cscfga__Product_Definition__r.cscfga__Product_Category__c, cscfga__originating_offer__c, (SELECT Id, Name FROM csbb__Product_Configuration_Requests__r)
                FROM cscfga__Product_Configuration__c
                WHERE cscfga__Product_Basket__c IN :basketIds
                    AND cscfga__Product_Definition__c IN: pdList
                    AND cscfga__Root_Configuration__c = null];
                    
                  
    //create Map<ProductConfigId, ProductDefId>                
    for(cscfga__Product_Configuration__c pc : pcList){
        pcMap.put(pc.Id, pc.cscfga__Product_Definition__c);    
    }   
    
    System.debug('**** PC list: ' + pcList);
            
    //get pcrs
    pcrs = [SELECT Id, Name, csbb__Product_Configuration__r.cscfga__Product_Definition__c, csbb__Optionals__c, csbb__Offer__c
            FROM csbb__Product_Configuration_Request__c
            WHERE csbb__Product_Configuration__c IN : pcList];
    
    //create Map<ProductDefinitionId, pcr>
    for(csbb__Product_Configuration_Request__c pcr : pcrs){
        pcrsMap.put(pcr.csbb__Product_Configuration__r.cscfga__Product_Definition__c, pcr);
    }
    
    //get Attachments
    attachmentList = [SELECT Id, Name, ParentId, Body
                        FROM Attachment
                        WHERE ParentId IN : pcrs];
                        
    //create Map<ProdDefId, List<Attachment>> 
    for(csbb__Product_Configuration_Request__c pcr : pcrs){
        List<Attachment> tempAttachmentList = new List<Attachment>();
        for(Attachment att : attachmentList){
            if(att.ParentId == pcr.Id){
                tempAttachmentList.add(att);
            }
        }
       pcrsAttachmentsMap.put(pcr.csbb__Product_Configuration__r.cscfga__Product_Definition__c, tempAttachmentList); 
    }
    
    //create missing pcrs, get the csbb__Optionals__c from an existing PCR, csbb__Offer__c is taken from the PC but if it's blank use a "default" offer 
    //csbb__Optionals__c, csbb__Offer__c and csbb__Status__c are mandatory in order for Advanced Cloning to work also the pcr has to have attachments, these are taken from an already existing pcr
    for(cscfga__Product_Configuration__c pc : pcList){
        Id offerId = pc.cscfga__originating_offer__c;
        String defaultOffer = offerMap.get(string.valueOf(pc.cscfga__Product_Definition__c).substring(0, 15));
        
        if(offerId == null && defaultOffer != null){
            offerId = Id.valueOf(defaultOffer);
        }
        
        csbb__Product_Configuration_Request__c tempPcr = pcrsMap.get(pc.cscfga__Product_Definition__c);
        
        if(pc.csbb__Product_Configuration_Requests__r != null && pc.csbb__Product_Configuration_Requests__r.size() < 1 && tempPcr != null){
            pcrsToAdd.add(new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = pc.cscfga__Product_Basket__c
                            , csbb__Product_Category__c = pc.cscfga__Product_Definition__r.cscfga__Product_Category__c
                            , csbb__Product_Configuration__c = pc.Id
                            , csbb__Optionals__c = tempPcr.csbb__Optionals__c
                            , csbb__Offer__c = offerId
                            , csbb__Status__c = 'finalized'));
        }
    }
    
    if(pcrsToAdd.size() > 0){
        System.debug('**** pcrsToAdd: ' + pcrsToAdd);
        insert pcrsToAdd;
        
        List<Attachment> attListToAdd = new List<Attachment>();
        
        for(csbb__Product_Configuration_Request__c pcr : pcrsToAdd){
            Id tempProdDefId = pcMap.get(pcr.csbb__Product_Configuration__c);
            List<Attachment> attList = pcrsAttachmentsMap.get(tempProdDefId);
            
            if(attList != null && !attList.isEmpty()){
                for(Attachment att : attList){
                    attListToAdd.add(new Attachment(Name = att.Name, ParentId = pcr.Id, Body = att.Body));
                }    
            }
            
        }
        
        insert attListToAdd;
        System.debug('**** Added PCRs: ' + pcrsToAdd);
    }
    
}
}