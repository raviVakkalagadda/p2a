public with sharing class PREmailController {
/*
    public void getEmailList(List<EmailContentVO> emailList,String opportunityID,String userName){
        
        List<String> dynDatas = new List<String>();
     

        //get list of all the ordered items
        string htmlOrderedList='';
        string htmlOrderedList1='';
        string htmlOrderedList2='';
     
         htmlOrderedList1='<table bgcolor="#a3cddb" border="1" cellpadding="0" cellspacing="0" width="540"><tbody>'
                        +'<tr>'
                        +'<th align="left" bgcolor="#a3cddb" height="30" valign="middle" width="70"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Sl.No</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Opportunity Id</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"> <strong>Opportunity LineItem NO</strong></font> </span> </th>'
                        +'<th width="50"></th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"> <strong>CartLine Item Id</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"> <span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>PR Number</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>PO Number</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"> <strong>Line Description</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Purchasing Operational Unit</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Opportunity Stage</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"> <span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Site Name</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Site Code</strong></font> </span> </th>'
                        +'<th width="50"> </th>'
                        +'<th align="left" bgcolor="#a3cddb" valign="middle" width="440"><span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;"><font color="#000000" face="Arial, Verdana, sans-serif"><strong>Order Type</strong></font> </span> </th><td bgcolor="#a3cddb" width="20"> </td> </tr>';
                        htmlOrderedList2='</tbody></table>';
                        Integer i=1;
                        for(EmailContentVO emailContentVO:emailList){
                        htmlOrderedList +='<tr>'
                            +'<td width="70" height="30" align="left"  bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000">'
                            +'<strong>'+i+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.OpportunityId+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.OpportunityLineItemNumber+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'

                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.CartLineItemId+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'


                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.PRNumber+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'


                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.PONumber+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.LineDescription+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.PurchasingOperatingUnit+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.OpportunityStage+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.SiteName+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.SiteCode+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            +'<td width="70" align="left" valign="middle" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;">'
                            +'<span style="font-size: 12px; color: #000000; font-weight: normal; margin: 0; padding: 0;">'
                            +'<font face="Arial, Verdana, sans-serif" color="#000000"><strong>'+emailContentVo.OrderType+'</strong></font></span></td>'
                            +'<td width="15" bgcolor="#daecf2" style="border-bottom: 1px solid #ACB7B8;"> </td>'
                            
                            
                            +'</tr>';
                            i++;
                            
        }
        //insert dynamic data in email html body
        insertDynDataInBody(htmlOrderedList1+htmlOrderedList+htmlOrderedList2,opportunityID,userName);
        //send email
        
        
        
    }
     public void insertDynDataInBody(String body,String opportunityID,String userName){
        try{
        EMailVO prEMailVO= new EMailVO();
       
        prEMailVO.to=new String[]{'suparna.sikdar@accenture.com','elneeta.vandana@accenture.com','n.rao.muppalla@accenture.com','Kumar_Kumarasamy@infosys.com','arun_n03@infosys.com','nishit_sabnis@infosys.com','rahul.chanani@accenture.com','renie.daniel.mathew@accenture.com','Niki.YUAN@team.telstra.com'};
        prEMailVO.subject=opportunityID+' '+'Offnet product has been requested for change';
        
        prEMailVO.body=userName +' '+'has requested for this'+' '+opportunityID+' '+'Offnet product change'+'<br>'+'<br>'+body;
        sendMail(prEMailVO);
        }catch (Exception ex) {
            System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex);
        }
    }
        public  Boolean sendMail(EMailVO prEMailVO){
        Boolean mailSent=false;
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setReplyTo(prEMailVO.sender);
            //mail.setSenderDisplayName(prmEMailVO.senderDispName);
            system.debug('====System.Label.RFQ_Team_Group_Id===='+System.Label.RFQ_Team_Group_Id);
            String[] addressArr=system.Label.RFQ_Team_Group_Id.split(',',0);
            system.debug('======RFQ Team Group Id====='+addressArr);
            mail.setToAddresses(addressArr);
            //mail.setToAddresses(new String[]{'suparna.sikdar@accenture.com','elneeta.vandana@accenture.com','n.rao.muppalla@accenture.com','Kumar_Kumarasamy@infosys.com','arun_n03@infosys.com','nishit_sabnis@infosys.com','rahul.chanani@accenture.com','renie.daniel.mathew@accenture.com'});
            //mail.setCcAddresses(prEMailVO.cc);
           // mail.setBccAddresses(prEMailVO.bcc);
            mail.setSubject(prEMailVO.subject);
            mail.setHtmlBody(prEMailVO.body);
           /* If(prmEMailVO.sender!=null)
            {
            OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: prmEMailVO.sender];
            if ( owea.size() > 0 ) {
                mail.setOrgWideEmailAddressId(owea.get(0).Id);
            }
            }
            */
           /* List<Messaging.SendEmailResult> results= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            mailSent=results.get(0).isSuccess();
            if(!mailSent){
                System.Debug('Errors while sending Mail ..'+results.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent;
    }
    public class EmailVO{
    public String sender{get;set;}
    public String senderDispName{get;set;}
    public String[] to{get;set;}
    public String[] cc{get;set;}
    public String[] bcc{get;set;}
    public String subject{get;set;}
    public String body{get;set;}
    public String header{get;set;}
    public String footer{get;set;}
    public String module{get;set;}
    public String originator{get;set;}
    public String replaceCharStart{get;set;}
    public String replaceCharEnd{get;set;}
    public Boolean isCIC{get;set;}
    }
 */   

}