/**
    @author - Accenture
    @date - 3-Aug-2012
    @version - 1.0
    @description - This is the test class for PRManualController
*/
@isTest(SeeAllData = false)
public class PRManualControllerTest {
       
    static testMethod void getPRManualControllerDetails() {
    
       Id pricebookId = Test.getStandardPricebookId();
              
       Product2 prod= new Product2(Name = 'GFTS - enepath',ProductCode = 'GFTS-e',Product_ID__c ='IPT',
                                           Installment_NRC_Bill_Text__c='test',IsActive=true,ETC_Bill_Text__c='testing',
                                           MRC_Bill_Text__c='test123',NRC_Bill_Text__c='working',
                                           Root_Product_Bill_Text__c='Dell',Service_Bill_Text__c='service',
                                           RC_Credit_Bill_Text__c='credit',NRC_Credit_Bill_Text__c='NRcredit'); 
        insert prod;
        list<Product2> prod1 = [select id,Name from Product2 where Name = 'GFTS - enepath'];
        system.assertEquals(prod.Name , prod1[0].Name);
        System.assert(prod!=null);    
        
        //Pricebook2 customPB = new Pricebook2(Name='Custom Pricebook', isActive=true, IsStandard = true);
        //insert customPB;
        
       
        PricebookEntry customPrice = new PricebookEntry(
            Pricebook2Id = pricebookId, Product2Id = prod.Id,
            UnitPrice = 12000, IsActive = true);
        insert customPrice;
        
  
       /* PricebookEntry p= new PricebookEntry(Pricebook2Id = Pricebook2Obj.Id,Product2Id =  prod.Id,
                                                        IsActive = true, CurrencyIsoCode='USD',
                                                        UseStandardPrice=false, UnitPrice=0.01);
        insert p; */
  
    
    Country_Lookup__c cl = new Country_Lookup__c();
    cl.CCMS_Country_Code__c = 'SG';
    cl.CCMS_Country_Name__c = 'India';
    cl.Country_Code__c = 'SG';
    insert cl;
    
    City_Lookup__c ci = new City_Lookup__c();
    ci.Name = 'AtS';
    ci.City_Code__c ='AT';
    // ci.Generic_Site_Code__c = 'CHE&';
    insert ci;
    
    
     
    Account acc = new Account();
    //Country_Lookup__c c = getCountry();
    acc.Name = 'Test Account Test 1';
    acc.Customer_Type__c = 'MNC';
    acc.Country__c = cl.Id;
    acc.Selling_Entity__c = 'Telstra INC';
    acc.Activated__c = True;
    acc.Account_ID__c = '9090';
    acc.Account_Status__c = 'Active';
    acc.Customer_Legal_Entity_Name__c='Test';
    insert acc;
    
    
    Contact con = new Contact();
    con.LastName = 'Talreja';
    con.FirstName = 'Dinesh';
    con.AccountId = acc.Id;
    con.Contact_Type__c = 'Technical';
    con.Country__c = cl.Id;
    con.Primary_Contact__c = true;
    con.MobilePhone = '123333';
    con.Phone = '1223';
    insert con;  
    
    Site__c s = new Site__c();
    s.Name = 'TestSite';
    //s.AccountId__c = acc.Id;
    s.Address1__c ='door no.10';
    s.Address2__c='bangalore,Testdata';
    s.Country_Finder__c = cl.Id;
    s.AccountId__c = acc.Id;
    s.City_Finder__c = ci.Id;
    s.Address_type__c='Billing Address';
    insert s;
    
    BillProfile__c b = new BillProfile__c();
    b.Account__c = acc.Id;
    b.Bill_Profile_Site__c = s.Id;
    b.Billing_Entity__c = 'Telstra Corporation';
    b.CurrencyIsoCode = 'GBP';
    b.Payment_Terms__c = '30 Days';
    b.Billing_Frequency__c = 'Monthly';
    b.Start_Date__c = Date.today();
    b.Invoice_Delivery_Method__c = 'Postal';
    b.Postal_Delivery_Method__c = 'First Class';
    b.Invoice_Breakdown__c = 'Summary Page';
    b.Billing_Contact__c = con.id;
    b.Status__c = 'Approved';
    b.Payment_Method__c = 'Manual';
    b.Minimum_Bill_Amount__c = 10.20;
    b.First_Period_Date__c = date.today();
    b.Rating_Rounding_Level__c = 2;
    b.Bill_Decimal_Point__c = 0;
    b.FX_Group__c = 'Corporate';
    b.FX_Rule__c = 'FX Rate at end of period';
            
    insert b;
    
    
    

    Opportunity opp = new Opportunity();
    opp.Name = 'Test Opportunity';
    opp.AccountId = acc.Id;
    opp.StageName = 'Identify & Define';
    opp.Stage__C='Identify & Define';
    opp.CloseDate = System.today();
    opp.Estimated_MRC__c=800;
    opp.Estimated_NRC__c=1000;
    opp.ContractTerm__c='10';
    
    //opp.Approx_Deal_Size__c = 9000;
    opp.Signed_Contract_Upload_Date__c = System.today();
    insert opp;
    
    
    List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
    OpportunityLineItem oli = new OpportunityLineItem();
    //b = getBillProfile();
    oli.OpportunityId = opp.Id;
    oli.IsMainItem__c = 'Yes';
    oli.OrderType__c = 'New Provide';
    oli.BillProfileId__c = b.Id;
    oli.Quantity = 5;
    oli.UnitPrice = 10;
    oli.PricebookEntryId = customPrice.Id;
    oli.ContractTerm__c = '12';
    oli.OffnetRequired__c = 'Yes';
    oli.Purchase_Requisition_Number__c = '';
    oli.Site_B__c = null;
    oli.CPQItem__c = '1';
    lst.add(oli);
    
        
    OpportunityLineItem oppLineItem = new OpportunityLineItem();
    oppLineItem.OpportunityId = opp.Id;
        oppLineiTem.IsMainItem__c = 'No';
        oppLineItem.OrderType__c = 'Renew';
        oppLineItem.Quantity = 5;
        oppLineItem.UnitPrice = 10;
        oppLineItem.BillProfileId__c = b.Id;
        oppLineItem.PricebookEntryId = customPrice.Id;
        oppLineItem.ContractTerm__c = '24';
        oppLineItem.OffnetRequired__c = 'Yes';
        oppLineItem.Purchase_Requisition_Number__c = '';
        oppLineItem.Site_B__c = null;
        oppLineItem.CPQItem__c = '2';
        lst.add(oppLineItem);
        
        OpportunityLineItem oppLineItem1 = new OpportunityLineItem();
       // b = getBillProfile();
        oppLineItem1.OpportunityId = opp.Id;
        oppLineiTem1.IsMainItem__c = 'Yes';
        oppLineItem1.OrderType__c = 'New Provide';
        oppLineItem1.Quantity = 5;
        oppLineItem1.UnitPrice = 10;
        oppLineItem1.BillProfileId__c = b.Id;
        oppLineItem1.PricebookEntryId = customPrice.Id;
        oppLineItem1.ContractTerm__c = '48';
        oppLineItem1.OffnetRequired__c = 'Yes';
        oppLineItem1.Purchase_Requisition_Number__c = '';
        oppLineItem1.CPQItem__c = '3';
        oppLineItem1.Site__c = null;
        lst.add(oppLineItem1);
        //insert lst;
        
    
    
            
       //system.currentPageReference().getParameters().put('id',opp.id);
       // ApexPages.StandardController sc1 = new ApexPages.StandardController(opp); 
       // PRManualController prc = new PRManualController(sc1);
        //public OppProductWrapper(Id oliid, String a,String item2,String procode,String prnum,Boolean preselect)
        
        
        PRManualController prc1 = new PRManualController();
        Id oliid =  opp.Id;
        String a = 'a';
        String item2 = 'item2';
        String procode = 'procode';
        String prnum = 'prnum';
        String e;
        Boolean preselect = true;
        
        PRManualController.OppProductWrapper prWrapper = new PRManualController.OppProductWrapper(oliid,a,item2,procode,prnum,preselect);
        
        /*List<PRManualController.OppProductWrapper> oppList = prc.getOppProducts();
        for (PRManualController.OppProductWrapper oppWrap : oppList){
            oppWrap.selected = true;
            System.debug('Selected' +oppWrap.selected);
       }*/
       
       prc1.getOppProducts();
       prc1.getSelOppProducts();
       prc1.getSelected();
       prc1.updatePRInfo();
       prc1.cancelRequest();
       prc1.GetSelectedAccounts();
        prc1.setErrorMessage(e);
       prc1.getErrorMessage();
      
       
       
    }
}