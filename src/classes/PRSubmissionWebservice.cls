/*  
@Name:PRSubmissionWebservice
@Version:1.0
@Author: Accenture 
@Purpose:This is a custom webservice apex class which will be invoked by TIBCO middle layer


    This class does following things:   
    1. Pull all the opportunities which has offnet components listed on it when Opportunity triggered for PR request SFDC
    2. Pull all the Offnet line item details related to the opportunity
*/

global class PRSubmissionWebservice {
    /*
    Input parameters for SFDC custom webservice:
    -
    Output parameter return by the SFDC custom webservice:
    @result - xml formatted string
    @return values could be:
    - If success:
        
    - If Failed:    
        Failed to fetch line item details
    */

  /*  global class LineItemDetails{
        webservice String OperatingUnit;
        webservice String PurchaseRequisitionNumber;
        webservice String EmployeeNumber;
        webservice String SalesPersonNumber;
        webservice String OpportunityNumber;
        webservice String LineDescription;
        webservice Date NeedByDate;
        webservice String NoteFromSales;
        webservice String OpportunityCurrency;
        webservice String Industry;
        webservice String CartItemId;
        webservice String OpportunityStage;
        webservice String ProductType;
        webservice String ProductId;
        webservice String ServiceType;
        webservice String ClassofService;
        webservice String ExistingPRNumber;
        webservice String ExistingPONumber;
        webservice String OrderType;
        webservice String LeadSource;
        webservice String PrimaryQuoteId;
        webservice String CustomerAccountNumber;
        webservice String CustomerAccountName;
        webservice String AccountNature;
        webservice String SellingEntity;
        webservice String OpportunityLineItemNumber;
        webservice String ContractTerm;
        webservice String SpecificQuoteRequirements;
        webservice String PopZCode;
        webservice String PopACode;
        webservice String TIMSQuote_OrderID;
        webservice String OpportunityLineID;
        webservice String OpportunityID;
        webservice String AccountNumber;
            
        webservice String CustomerSiteAAddress1;    
        webservice String CustomerSiteAAddress2;
        webservice String SiteACity;
        webservice String SiteAState;
        webservice String SiteACountry;
        webservice String CustomerSiteBAddress1;    
        webservice String CustomerSiteBAddress2;
        webservice String SiteBCity;
        webservice String SiteBState;
        webservice String SiteBCountry;
        
        /* Changes Part of CR 594 */
      /*  webservice String PortType;
        webservice String AssociatedUIA;
        webservice String U2CMasterCode;
        webservice String RootProductId;
        /* Changes Part of CR 594 */
  //  }
/*  
@param - Id opptId
@return - List<LineItemDetails> liList
@description -This method is used to calculate line item details and send it to Tibco. .
*/


    /*webService static List<LineItemDetails> fetchLineItemDetails(Id opptId){
        
        List<LineItemDetails> liList = new List<LineItemDetails>();
        Map<Id,Opportunity> OppObjMap = new Map<Id,Opportunity>();
        List<Opportunity> oList = new List<Opportunity>();
        List<OpportunityLineItem> oliList=new List<OpportunityLineItem>();
        Map<String,String> CPEObjMap = new Map<String,String>();
        Map<String,String> AccNatureObjMap = new Map<String,String>();
        List<AccountNatureMapper__c> AccNatureList=new List<AccountNatureMapper__c>();
        List<PurchasingEntityMapper__c> CPEList=new List<PurchasingEntityMapper__c>();
        String country_purcahsing_Unit='';
        LineItemDetails Lid=null;
        try{
            //Fetch the Opportunity which is eligible to flow to TigFin
            //Opportunity which has an offnet product in the deal is eligible
           //added Sales_Status__c to query as part of SOMP
            List<Opportunity> OffnetOpp = [SELECT Id,Is_Offnet_Product_in_Deal__c,Employee_Number_To_TIGFIN__c,StageName,Product_Type__c,Industry__c,Selling_Entity__c,TigFin_PR_Actions__c,Opportunity_ID__c,Account_Number__c, Employee_Number__c,Customer_Account_Name__c,External_ID__c,LeadSource,Sales_Status__c  FROM Opportunity WHERE Id=:opptId];
            
            for(Opportunity oppobj: OffnetOpp){
                OppObjMap.put(oppobj.Id,oppobj);
            }

            // OppObjMap will contain details about only one opportunity
            
            for(Id OppId:OppObjMap.keySet()){  

                Opportunity opp = OppObjMap.get(OppId);
                
                //Fetch offnet opportunity line item related to this opportunity to trigger IP031 
                List<OpportunityLineItem> OffnetOppLineItem = [SELECT Id,Product_id2__c,Root_Product_Id__c,Port_Type__C,Do_you_want_to_associate_a_UIA__c,U2C_Master_Code__c,GIE_router_model__c,Power_per_rack__c,Video_Hardware_Maintenance_Level__c,Unit_of_Power__c,Utility_model__c,Type_of_space__c,Specify_the_additional_items_required__c,Media_type__c,Sub_Product_Type__c,Video_device_financing_model__c,Video_hardware_name_and_model__c,Overall_Video_Service_Management__c,Concierge_Service__c,Level_of_Supplier_management__c,GMNS_PS_Type__c,Financing_Model__c,Upstream_speed__c,Downstream_speed__c,Internet_Access_Technology__c,Type__c,COSVoice__c,Other_NNI_Location__c,COS_Low_Priority__c,Site__c,Site_B__c,Site_B_State__c,Type_of_circuit__c,EMC_Type__c,Number_of_E1_T1_Cables_Required__c,NID_Port_Speed__c,Model__c,NID_Rack_Mount_Size__c,NID_WAN_Interface_SFP_Type__c,Type_of_Installation__c,GCPE_type_associated_with_GMNS__c,Hardware_Size__c,DCoS_required__c,Maintenance_MRC__c,Local_Loop_Speed__c,COSStandardData__c,COSInteractiveData__c,COSVideo__c,COSCriticalData__c,NNI_Provider__c,Interface_type__c,MTU_Size__c,Installation_after_business_hours_only__c,GCPE_Type__c,GCPE_Hardware_Manufacturer__c,GCPE_Hardware_Model__c,Port_speed__c,Line_Description__c,Line_Description_for_TIFGIN__c,POP_A_Code__c,POP_Z_Code__c,Site_B_City__c,Customer_Site_A_Address_2__c,Site_A_City__c,Site_A_State__c,Customer_Site_B_Address_1__c,Product_Id__c,Service_Type__c,ProductCode__c,Existing_PR_Number__c,Existing_PO_Number__c,OrderType__c,PricebookEntry.product2.Name,PricebookEntry.Product2.Product_ID__c,Purchase_Requisition_Number__c,Contract_term__c,Need_By_Date__c,Specific_Quote_Requirements__c,CurrencyIsoCode,CPQItem__c,CartItemGuid__c,Customer_Site_A_Address_1__c,Customer_Site_B_Address_2__c,Type_of_NNI_Required__c,Type_A_NNI_Port_CoS_mix__c,PricebookEntry.product2.Id FROM OpportunityLineItem WHERE OpportunityId =: opp.Id and OffnetRequired__c =: 'Yes' and Trigger_Required__c=true];
                
                   system.debug('===OffnetOppLineItem===='+OffnetOppLineItem);
                  
                   for(OpportunityLineItem oli: OffnetOppLineItem){
                    
                      system.debug('===================='+oli.PricebookEntry.product2.Id);
                      
                    //To fetch Account nature corresponding to the   product id
                    AccountNatureMapper__c AccNatureObj=[SELECT Id,Account_Nature__c,Product_Code__c,Product_ID__c,Product_Name__c,Type__c from AccountNatureMapper__c where Product_Name__c=:oli.PricebookEntry.product2.Id limit 1];
                    
                    // siteAcountry and siteBcountry both can't be blank
                    Site__c siteAObj = new Site__c();
                    Site__c siteBObj = new site__c();
                    //To fetch country corresponding to site A
                    siteAObj=[Select Id,Name,Country_Finder__c from Site__c where Id=:oli.Site__c limit 1];
                    if(siteAObj.Id == null){
                    //To fetch country corresponding to site B
                    siteBObj =[Select Id,Name,Country_Finder__c from Site__c where Id=:oli.Site_B__c limit 1];
                    }
                    
                    system.debug('siteAObj===siteBObj.Id'+siteAObj+'==='+siteBObj.Id);
                    //If Site A is not available , Site B is selected assuming both cannot be blank.
                    country_purcahsing_Unit=siteAObj.Country_Finder__c!=null?siteAObj.Country_Finder__c:siteBObj.Country_Finder__c;
                    system.debug('===Product_ID__c=='+oli.PricebookEntry.Product2.Product_ID__c);
                    //To fetch direct purchasing unit, purchasing entity corresponding to country
                    PurchasingEntityMapper__c CPEObj=[SELECT Id,Country_Name__c,Direct_Purchasing_PU__c,Purchasing_Entity__c,Other__c from PurchasingEntityMapper__c where Country_Name__c=:country_purcahsing_Unit limit 1];
                    //To fetch direct purchasing unit, purchasing entity corresponding to selling entity 
                    SellingEntityPurchasingEntityMapper__c SEPEObj=[select Id,Country__c,Direct_Purchasing_PU__c,Purchasing_Entity__c,Region__c,Selling_Entity__c from SellingEntityPurchasingEntityMapper__c where Selling_Entity__c=:opp.Selling_Entity__c limit 1];
                    Lid = new LineItemDetails();
                    Lid.SellingEntity=opp.Selling_Entity__c;
                    Lid.PurchaseRequisitionNumber=oli.Purchase_Requisition_Number__c;
                    Lid.ContractTerm=oli.Contract_term__c;
                    Lid.NeedByDate=oli.Need_By_Date__c;
                    Lid.SpecificQuoteRequirements=oli.Specific_Quote_Requirements__c;
                    Lid.OpportunityCurrency=oli.CurrencyIsoCode;
                    Lid.CustomerAccountName=opp.Customer_Account_Name__c;
                    Lid.Industry=opp.Industry__c;
                    Lid.OpportunityLineItemNumber=oli.CPQItem__c;
                    Lid.CartItemId=oli.CartItemGuid__c;
                    Lid.OpportunityStage=opp.StageName;
                    Lid.ProductType=opp.Product_Type__c;
                    Lid.ProductId=oli.Product_id2__c;
                    Lid.ServiceType=oli.Service_Type__c;
                    
                    //CR 594 Changes starts here
                    Lid.RootProductId=oli.Root_Product_Id__c;
                    Lid.PortType = oli.Port_Type__C;
                    Lid.AssociatedUIA = oli.Do_you_want_to_associate_a_UIA__c;
                    Lid.U2CMasterCode = oli.U2C_Master_Code__c;
                    //CR 594 Changes ends here
                
                    //Sending Existing PR Number for Terminate and P&C Order Types
                    if(oli.OrderType__c=='Terminate' || (opp.StageName!='InFlight' && (oli.OrderType__c=='Downgrade - Provide & Cease' || oli.OrderType__c=='Upgrade - Provide & Cease'))){
                    Lid.PurchaseRequisitionNumber=oli.Existing_PR_Number__c;
                    Lid.ExistingPONumber=oli.Existing_PO_Number__c;
                    oli.Purchase_Requisition_Number__c=oli.Existing_PR_Number__c;
                    }else if(opp.Sales_Status__c=='Lost'){
                        //updated stage name Closed lost as per SOMP reuirement
                    //Lid.ExistingPRNumber=oli.Existing_PR_Number__c;
                    //Lid.ExistingPONumber=oli.Existing_PO_Number__c;
                    Lid.PurchaseRequisitionNumber=oli.Purchase_Requisition_Number__c;
                    }else{
                    Lid.PurchaseRequisitionNumber=oli.Purchase_Requisition_Number__c;   
                    }
                    Lid.OrderType=oli.OrderType__c;
                    Lid.LeadSource=opp.LeadSource;
                    Lid.AccountNumber=opp.Account_Number__c;
                    // If Site A is ZZZZ, dummy values for ZZZZ site is not sent
                    if(siteAObj.Id!=null){
                    if(siteAObj.Name!='ZZZZ'){
                    Lid.CustomerSiteAAddress1=oli.Customer_Site_A_Address_1__c;
                    Lid.CustomerSiteAAddress2=oli.Customer_Site_A_Address_2__c;
                    Lid.SiteACity=oli.Site_A_City__c;
                    Lid.SiteAState=oli.Site_A_State__c;                   
                    Lid.SiteACountry=[select Id,Name from  Country_Lookup__c  where  Id=:siteAObj.Country_Finder__c].Name;
                    Lid.PopACode=oli.POP_A_Code__c;
                    }
                    }
                    // If Site A is ZZZZ, dummy values for ZZZZ for site B is not sent assuming both site A and B cannot be blank
                    if((siteAObj.Id == null)&& siteBObj.Id!=null){
                    if(siteBObj.Name!='ZZZZ'){
                    Lid.CustomerSiteBAddress1=oli.Customer_Site_B_Address_1__c;
                    Lid.CustomerSiteBAddress2=oli.Customer_Site_B_Address_2__c;
                    Lid.SiteBCity=oli.Site_B_City__c;
                    Lid.SiteBState=oli.Site_B_State__c;
                    Lid.SiteBCountry=[select Id,Name from  Country_Lookup__c  where  Id=:siteBObj.Country_Finder__c].Name;
                    Lid.PopZCode=oli.POP_Z_Code__c;
                    }
                    }
                    //Sending Employee number and Sales person number of opportunity owner
                    Lid.EmployeeNumber = opp.Employee_Number_To_TIGFIN__c;
                    Lid.SalesPersonNumber = opp.Employee_Number_To_TIGFIN__c;
                    Lid.OpportunityLineID=oli.CartItemGuid__c;
                    Lid.TIMSQuote_OrderID=opp.External_ID__c;
                    Lid.OpportunityID=opp.Opportunity_ID__c;
                    //Sending Purchasing Entity from CPE PU for GCPE , NIDS , EMCS and Direct purchasing PU for other products.
                    if(oli.PricebookEntry.Product2.Product_ID__c=='GCPE' ||oli.PricebookEntry.Product2.Product_ID__c=='NIDS' || oli.PricebookEntry.Product2.Product_ID__c=='EMCS'){
                    Lid.OperatingUnit=CPEObj.Purchasing_Entity__c;
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-SS' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-PM' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-POC'){
                    Lid.OperatingUnit=SEPEObj.Purchasing_Entity__c;
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='COLO-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-XC' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-MISC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-WBC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-IPA' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-PMC' || oli.PricebookEntry.Product2.Product_ID__c=='GTPS')
                    {
                    Lid.OperatingUnit=SEPEObj.Direct_Purchasing_PU__c;
                    }
                    else{
                    Lid.OperatingUnit=CPEObj.Direct_Purchasing_PU__c;
                    }
                    Lid.AccountNature=AccNatureObj.Account_Nature__c;
                    //Concatenating COS data and Line description for all the offnet products .Label is not shown if any field is null.
                    Lid.ClassofService=(oli.COSVoice__c!=null?'Voice:'+oli.COSVoice__c+'%'+',':'')
                    +(oli.COSVideo__c!=null?'Video:'+oli.COSVideo__c+'%'+',':'')
                    +(oli.COSCriticalData__c!=null?'Critical Data:'+oli.COSCriticalData__c+'%'+',':'')
                    +(oli.COSInteractiveData__c!=null?'Interactive Data:'+oli.COSInteractiveData__c+'%'+',':'')
                    +(oli.COSStandardData__c!=null?'Standard Data:'+oli.COSStandardData__c+'%'+',':'')
                    +(oli.COS_Low_Priority__c!=null?'Low Priority Data:'+oli.COS_Low_Priority__c+'%'+',':'');
                    Lid.ClassofService=Lid.ClassofService.substring(0,Lid.ClassofService.length()-1);
                    if(oli.PricebookEntry.Product2.Product_ID__c=='LLOOP'){
                   Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Service_Type__c!=null?'Service Type:'+ oli.Service_Type__c+',':'')
                               +(oli.Local_Loop_Speed__c!=null?'Speed:'+ oli.Local_Loop_Speed__c+',':'')
                               +(oli.Interface_type__c!=null?'Interface Type:'+ oli.Interface_type__c+',':'')
                               +(oli.MTU_Size__c!=null?'MTU:'+ oli.MTU_Size__c+',':'')
                               +(oli.Installation_after_business_hours_only__c!=null?'Installation After Business Hours only:'+ oli.Installation_after_business_hours_only__c+',':'');
                                           
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='GCPE'){
                        
                    Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.GCPE_Type__c!=null?'TYPE:'+ oli.GCPE_Type__c+',':'')
                               +(oli.Financing_Model__c!=null?'Financing Model:'+ oli.Financing_Model__c+',':'')
                               +(oli.Maintenance_MRC__c!=null?'Maintenance:'+ oli.Maintenance_MRC__c+',':'')
                                       +(oli.Installation_after_business_hours_only__c!=null?'Installation after Business Hours only:'+ oli.Installation_after_business_hours_only__c+',':'');
   
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='OFFPOP'){
                        
                     Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Type_of_NNI_Required__c!=null?'NNI required:'+ oli.Type_of_NNI_Required__c+',':'')
                               +(oli.Port_speed__c!=null?'Port speed:'+ oli.Port_speed__c+',':'')
                                 +(oli.Other_NNI_Location__c!=null?'NNI location:'+ oli.Other_NNI_Location__c+',':'')
                                 +(Lid.ClassofService!=null?'CoS mix:'+Lid.ClassofService+',':'')
                                 +(oli.DCoS_required__c!=null?',DCos required:'+ oli.DCoS_required__c+',':'');

                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-SS' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-PM' || oli.PricebookEntry.Product2.Product_ID__c=='G-MNS-PS-POC'){
                        
                     Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.GMNS_PS_Type__c!=null?oli.PricebookEntry.Product2.Product_ID__c+''+'Type:'+ oli.GMNS_PS_Type__c+',':'')
                               +(oli.Hardware_Size__c!=null?'Hardware size:'+ oli.Hardware_Size__c+',':'');

                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='NIDS'){
                        
                    Lid.LineDescription=
                    (oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Model__c!=null?'Model:'+ oli.Model__c+',':'')
                               +(oli.NID_Port_Speed__c!=null?'Port Speed:'+ oli.NID_Port_Speed__c+',':'')
                               +(oli.NID_Rack_Mount_Size__c!=null?'Rack Mount Size:'+ oli.NID_Rack_Mount_Size__c+',':'')
                               +(oli.Interface_type__c!=null?'Interface type:'+ oli.Interface_type__c+',':'')
                               +(oli.NID_WAN_Interface_SFP_Type__c!=null?'NID WAN Interface SFP Type:'+ oli.NID_WAN_Interface_SFP_Type__c+',':'')
                               +(oli.Type_of_Installation__c!=null?'Type of installation?:'+ oli.Type_of_Installation__c+',':'');

                        
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='EMCS'){
                        
                    Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.EMC_Type__c!=null?'Type:'+ oli.EMC_Type__c+',':'')
                               +(oli.Type_of_Installation__c!=null?'Type of installation:'+ oli.Type_of_Installation__c+',':'')
                               +(oli.Number_of_E1_T1_Cables_Required__c!=null?'Number of E1/T1 Cables:'+ oli.Number_of_E1_T1_Cables_Required__c+',':'');
                        
                    }else if(oli.PricebookEntry.Product2.Product_ID__c=='OFFNETPROVIDER-AEND' || oli.PricebookEntry.Product2.Product_ID__c=='OFFNETPROVIDER-ZEND'){
                    Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Type_of_circuit__c!=null?'Type of circuit:'+ oli.Type_of_circuit__c+',':'');
                    
                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GIEG' || oli.PricebookEntry.Product2.Product_ID__c=='GIE_ROUTER' || oli.PricebookEntry.Product2.Product_ID__c=='GIER-BACKPOP'){
                    Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                               +(oli.Internet_Access_Technology__c!=null?'Internet Access Technology:'+ oli.Internet_Access_Technology__c+',':'')
                               +(oli.Downstream_speed__c!=null?'Downstream speed:'+ oli.Downstream_speed__c+',':'')
                               +(oli.Upstream_speed__c!=null?'Upstream speed:'+ oli.Upstream_speed__c+',':'')
                               +(oli.GIE_router_model__c!=null?'GIE Router Model:'+ oli.GIE_router_model__c+',':'');

                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='COLO-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-CAGE-RACK' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-XC' || oli.PricebookEntry.Product2.Product_ID__c=='COLO-MISC'){
                     Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                +(oli.Power_per_rack__c!=null?'Power Per Rack:'+ oli.Power_per_rack__c+',':'')
                                +(oli.Unit_of_Power__c!=null?'Unit Of Power:'+ oli.Unit_of_Power__c+',':'')
                                +(oli.Utility_model__c!=null?'Utility Model:'+ oli.Utility_model__c+',':'')
                                +(oli.Type_of_space__c!=null?'Type Of Space:'+ oli.Type_of_space__c+',':'')
                                +(oli.Specify_the_additional_items_required__c!=null?'Specify Additional Items Required:'+oli.Specify_the_additional_items_required__c+',':'')
                                +(oli.Media_type__c!=null?'Media Type:'+oli.Media_type__c+',':'');
                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GCF-WBC' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-IPA' || oli.PricebookEntry.Product2.Product_ID__c=='GCF-PMC'){
                       Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                           +(oli.Type__c!=null?'Type:'+ oli.Type__c+',':'');

                     }else if(oli.PricebookEntry.Product2.Product_ID__c=='GTPS' || oli.PricebookEntry.Product2.Product_ID__c=='GTPR' || oli.PricebookEntry.Product2.Product_ID__c=='GTPM' ){
                                        Lid.LineDescription=(oli.PricebookEntry.product2.Name!=null?'Product Name:'+ oli.PricebookEntry.product2.Name+',':'')
                                                   +(oli.Type__c!=null?'Type:'+ oli.Type__c+',':'')
                                                   +(oli.Sub_Product_Type__c!=null?'Sub Product Type:'+ oli.Sub_Product_Type__c+',':'')
                                                   +(oli.Video_device_financing_model__c!=null?'Video Device Financing Model:'+ oli.Video_device_financing_model__c+',':'')
                                                   +(oli.Video_Hardware_Maintenance_Level__c!=null?'Video Hardware Maintenance Level:'+ oli.Video_Hardware_Maintenance_Level__c+',':'')
                                                   +(oli.Overall_Video_Service_Management__c!=null?'Overall Video Service Management:'+ oli.Overall_Video_Service_Management__c+',':'')
                                                   +(oli.Concierge_Service__c!=null?'Concierge Service:'+ oli.Concierge_Service__c+',':'')
                                                   +(oli.Level_of_Supplier_management__c!=null?'Level of supplier management:'+ oli.Level_of_Supplier_management__c+',':'');
                     }
                     
                     else{
                        Lid.LineDescription=',';
                    }
                    //Removing comma from the last item in line description
                     Lid.LineDescription=Lid.LineDescription.substring(0,Lid.LineDescription.length()-1);
                     oli.Line_Description_for_TIFGIN__c=Lid.LineDescription;
                     oli.Operating_Unit_TIGFIN__c=Lid.OperatingUnit;
                     oli.Trigger_Required__c=false;
                     // Adding Lid object to the list i.e adding each opportunity line item details to be sent for IP031
                     liList.add(Lid);
                     oliList.add(oli);
                     system.debug('====ContractTerm===='+Lid.ContractTerm);
                }
                system.debug('size   ===='+liList);
                
                if(liList.size() > 0){
                  //  o.TigFin_PR_Actions__c = 'Processed';
                  //  oList.add(o);
                }
                   
            }
            
            //Updating  oliList with line description and operating unit.
            if(oList.size() > 0){
             update oList;
            }
            if(oliList.size()>0){
                
             update oliList;
            }
            return liList;
        }
        catch (exception e){
            system.debug('*** failed to fetch line item details '+e);
            return liList;
            //return 'Failed '+e;
        }       
    }
    */
}