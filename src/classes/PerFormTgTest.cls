@isTest(SeeAllData=false)
private class PerFormTgTest {

static testMethod void getPerf() {
   List<Performance_Tracking__c> perfList1=new List<Performance_Tracking__c>();
             Performance_Tracking__c perfobj=null;
        
                perfobj=new Performance_Tracking__c();
               
                perfobj.Fiscal_year__c='2015-2016';
                perfobj.FY_Start_date__c=system.now();
                perfobj.FY_End_Date__c=system.now();
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                //sov in opp currency
                
                perfobj.SOV_Type__c='Performance';
              
               
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                 /*Currency Conversion*/
                
                perfList1.add(perfobj);
                
                perfobj=new Performance_Tracking__c();
               
                perfobj.Fiscal_year__c='2015-2016';
                perfobj.FY_Start_date__c=system.now();
                perfobj.FY_End_Date__c=system.now();
                perfobj.Q1_Rolled_Up__c=0;
                perfobj.Q1_Individual_Performance__c=0;
                perfobj.Q2_Rolled_Up__c=0;
                perfobj.Q2_Individual_Performance__c=0;
                perfobj.Q3_Rolled_Up__c=0;
                perfobj.Q3_Individual_Performance__c=0;
                perfobj.Q4_Rolled_Up__c=0;
                perfobj.Q4_Individual_Performance__c=0;
                perfobj.Q1_GAM_Roll_Up__c=0;
                perfobj.Q2_GAM_Roll_Up__c=0;
                perfobj.Q3_GAM_Roll_Up__c=0;
                perfobj.Q4_GAM_Roll_Up__c=0;
                //sov in Opp currency
                perfobj.Q1_Rolled_Up_Opp__c=0;
                perfobj.Q1_Individual_Performance_Opp__c=0;
                perfobj.Q2_Rolled_Up_Opp__c=0;
                perfobj.Q2_Individual_Performance_Opp__c=0;
                perfobj.Q3_Rolled_Up_Opp__c=0;
                perfobj.Q3_Individual_Performance_Opp__c=0;
                perfobj.Q4_Rolled_Up_Opp__c=0;
                perfobj.Q4_Individual_Performance_Opp__c=0;
                //sov in opp currency
                
                perfobj.SOV_Type__c='Transaction CR/DR';
              
               
                if(perfobj.Currency__c==null ){perfobj.Currency__c='AUD';}
                perfobj.CurrencyIsoCode=perfobj.Currency__c;
                 /*Currency Conversion*/
                
                perfList1.add(perfobj);
                
                insert perfList1;
               // list<Performance_Tracking__c> prod = [select id,Name from Performance_Tracking__c where Q1_Individual_Performance__c = 0];
        //system.assertEquals(perfList1[0].Name , prod[0].Name);
        System.assert(perfList1!=null); 
         
            
        
        Test.startTest();
      
   List<Performance_Tracking__c> performanceTrackingList=new List<Performance_Tracking__c>();
        // Added a where clause to the query to fetch Performance Tracking records for only the concerned Opportunity -- Bhargav
        performanceTrackingList=[select Opportunity_Id_GS__c,ParentRoleId__c,Currency__c,Q1_Individual_Performance_Opp__c,Q1_Rolled_Up_Opp__c,Q2_Individual_Performance_Opp__c,Q2_Rolled_Up_Opp__c,Q3_Individual_Performance_Opp__c,Q3_Rolled_Up_Opp__c,Q4_Individual_Performance_Opp__c,Q4_Rolled_Up_Opp__c,CurrencyIsoCode,OwnerId,Credit_Debit_Date__c,Q1_SOV_Credit__c,Q2_SOV_Credit__c,Q3_SOV_Credit__c,Q4_SOV_Credit__c,Opportunity_Look_Up__c,SOV_Type__c,UserRoleId__c,Employee__c,Employee__r.UserRoleId,Q1_End_date__c,Q1_Start_date__c,Q2_End_date__c,Q2_Start_date__c,Fiscal_year__c,Q3_End_date__c,Q3_Start_date__c,Q4_End_date__c,Q4_Start_date__c,FY_End_Date__c,FY_Start_date__c,Q1_Individual_Performance__c,Q1_Rolled_Up__c,Q1_Target__c,Q2_Individual_Performance__c,Q2_Rolled_Up__c,Q2_Target__c,Q3_Individual_Performance__c,Q3_Rolled_Up__c,Q3_Target__c,Q4_Individual_Performance__c,Q4_Rolled_Up__c,Q4_Target__c,Q1_GAM_Roll_Up__c,Q2_GAM_Roll_Up__c,Q3_GAM_Roll_Up__c,Q4_GAM_Roll_Up__c from Performance_Tracking__c where   Fiscal_year__c=:'2015-2016' ORDER BY SOV_Type__c DESC limit 5];
       update performanceTrackingList;
                
        Test.stopTest();
                     
    }


}