@isTest

class PopulateManagerTriggerTest {
    static testmethod void PopulateManagerOnContactTest() {
        Country_Lookup__c con=new Country_Lookup__c(name='Test Country',Country_Code__c='ABC');
        insert con;
        Account acc=new Account(name='Test Account',Customer_Legal_Entity_Name__c='Test',Customer_Type__c='MNC',Selling_Entity__c='Telstra INC',Country__c=con.id);
        insert acc;
        Contact c=new Contact(LastName='Test Contact',AccountId=acc.Id,Country__c=con.Id,Contact_Type__c='Sales');
        insert c;
    }
}