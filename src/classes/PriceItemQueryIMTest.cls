@isTest(SeeAllData = false)
private class PriceItemQueryIMTest {
    
    private static Map<String, String> searchFields = new Map<String, String>();
    private static String productDefinitionId;
    
    private static List<Account> accountList;
    private static List<IPL_Rate_Card_Bandwidth_Join__c> iplRateCardBandwidthJoinList;
    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CloudSense_Bandwidth__c> bandwidthList;
    private static List<IPL_Rate_Card_Item__c> iplRateCardItemList;
    
    private static void initTestData(){
        
        accountList = new List<Account>{
            new Account(Name = 'Account 1', Customer_Type_New__c = 'MNC'),
            new Account(Name = 'Account 2', Customer_Type_New__c = 'MNC')
        };
        
        insert accountList;  
           System.assertEquals('Account 1',accountList[0].Name );     
        
        bandwidthList = new List<CloudSense_Bandwidth__c>{
            new CloudSense_Bandwidth__c(Name = '64k'),
            new CloudSense_Bandwidth__c(Name = '128k')
        };
        
        insert bandwidthList;
          System.assertEquals('64k',bandwidthList[0].Name );
        
        iplRateCardItemList = new List<IPL_Rate_Card_Item__c>{
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 1', Product_Type__c = 'IPL'),
            new IPL_Rate_Card_Item__c(Name = 'Rate Card 2', Product_Type__c = 'IPL')
        };
        
        insert iplRateCardItemList;
          System.assertEquals('Rate Card 1',iplRateCardItemList[0].Name );
        
        iplRateCardBandwidthJoinList = new List<IPL_Rate_Card_Bandwidth_Join__c>{
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[0].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[0].Id),
            new IPL_Rate_Card_Bandwidth_Join__c(Bandwidth__c = bandwidthList[1].Id, IPL_Rate_Card_Item__c = iplRateCardItemList[1].Id)
        };
        
        insert iplRateCardBandwidthJoinList;
           System.assertEquals(bandwidthList[0].Id,iplRateCardBandwidthJoinList[0].Bandwidth__c );
        
        priceItemList = new List<cspmb__Price_Item__c>{
            new cspmb__Price_Item__c(Name = 'Price Item 1', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[0].Id, cspmb__Account__c = accountList[0].Id),
            new cspmb__Price_Item__c(Name = 'Price Item 2', IPL_Rate_Card_Bandwidth_Join__c = iplRateCardBandwidthJoinList[1].Id, cspmb__Account__c = null)
        };
        
        insert priceItemList;
         System.assertEquals('Price Item 1',priceItemList[0].Name );        
    }    
    
    private static testMethod void doDynamicLookupSearchTest() {
        Exception ee = null;
        
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('Bandwidth', bandwidthList[0].Id);
            searchFields.put('Account Id', accountList[0].Id);
            searchFields.put('RateCardIDTemp', iplRateCardItemList[0].Id);
            
            PriceItemQueryIM priceItemQuery = new PriceItemQueryIM();
            Object[] data = priceItemQuery.doDynamicLookupSearch(searchFields, productDefinitionId);
            String reqAtts = priceItemQuery.getRequiredAttributes();
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            
            data.clear();
            searchFields.put('Account Id', accountList[1].Id);
            searchFields.put('RateCardIDTemp', iplRateCardItemList[1].Id);
            searchFields.put('Bandwidth', bandwidthList[1].Id);
            
            data = priceItemQuery.doDynamicLookupSearch(searchFields, productDefinitionId);
            
            System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');
            Test.stopTest();
        } catch(Exception e){
            ee = e;
        } finally {
            //Test.stopTest();
            CS_TestUtil.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        } 
    }

}