global with sharing class PriceItemQueryNew extends cscfga.ALookupSearch {
    
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){

        String bandwidhtID = searchFields.get('Bandwidth');
        String accountID = searchFields.get('Account Id');
        String rateCardID = searchFields.get('RateCardIDTemp');
        String selectedproduct= searchFields.get('Selected Product Type');
        //System.Debug('doDynamicLookupSearch' + searchFields);
         List <cspmb__Price_Item__c> data;
        //if(selectedproduct != 'EVPL'){
		 /*
        * Below code is added by Developer: Krishna Deshpande
        * Added if condition to check that bandwidth and ratecards are not null
        */
        if(bandwidhtID != null && rateCardID != null){
            data = [SELECT Id, cspmb__One_Off_Cost__c,
                                            cspmb__Recurring_Cost__c,
                                            cspmb__One_Off_Charge__c,
                                            cspmb__Recurring_Charge__c,
                                            cspmb__Account__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE CS_Route_Bandwidth_Product_Type_Join__r.CS_Bandwidth_Product_Type__r.CS_Bandwidth__c=:bandwidhtID 
                                            AND CS_Route_Bandwidth_Product_Type_Join__r.CS_Route_Segment__c=:rateCardID
                                            AND cspmb__Account__c = :accountID];
        }   
       
        if (data.isEmpty()){
            data = [SELECT Id, cspmb__One_Off_Cost__c,
                    cspmb__Recurring_Cost__c,
                    cspmb__One_Off_Charge__c,
                    cspmb__Recurring_Charge__c,
                    cspmb__Account__c
                    FROM cspmb__Price_Item__c 
                    WHERE CS_Route_Bandwidth_Product_Type_Join__r.CS_Bandwidth_Product_Type__r.CS_Bandwidth__c=:bandwidhtID 
                    AND CS_Route_Bandwidth_Product_Type_Join__r.CS_Route_Segment__c=:rateCardID
                    AND cspmb__Account__c = null];
        }
        System.Debug('PriceItemQuery : '+ data);
    //}
        /*if(selectedproduct == 'EVPL'){
              data = [SELECT Id, Critical_MRC__c
                                            FROM cspmb__Price_Item__c 
                                            WHERE CS_Route_Bandwidth_Product_Type_Join__r.CS_Bandwidth_Product_Type__r.CS_Bandwidth__c=:bandwidhtID 
                                            AND CS_Route_Bandwidth_Product_Type_Join__r.CS_Route_Segment__c=:rateCardID
                                            AND cspmb__Account__c = null];
                                            System.Debug('PriceItemQueryEVPL : '+ data);
        }*/
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "Account Id","RateCardIDTemp","Bandwidth","Selected Product Type"]';
    }
}