public class PricingApproval{

    public static void setPARD(Set<Id> basketID){
        try{
            Map<Id, cscfga__Product_Configuration__c> ProductConfigListMap = new Map<Id, cscfga__Product_Configuration__c>([select id, Inflight_Cloning_Id__c,CommaSepProdDetails__c, cscfga__Product_Basket__c, Rate_Card_RC__c, Rate_Card_NRC__c, cscfga__One_Off_Charge__c, cscfga__Contract_Term__c, Cost_Nrc_Formula__c, Cost_Mrc_Formula__c, cscfga__Recurring_Charge__c, Rate_Burst_Rate__c, Rate_China_Burst_Rate__c, Target_Burst_Rate__c, Target_China_Burst_Rate__c, csordtelcoa__Replaced_Product_Configuration__c,Order_Type__c,csordtelcoa__Replaced_Service__r.Approved_Service_MRC__c,csordtelcoa__Replaced_Service__r.Approved_Service_NRC__c,cscfga_Offer_Price_MRC__c,cscfga_Offer_Price_NRC__c from cscfga__Product_Configuration__c where cscfga__Product_Basket__c=:basketID]);
            System.debug('ProductConfigListMap------>'+ProductConfigListMap);
    
            if(!ProductConfigListMap.IsEmpty()){
                List<cscfga__Product_Configuration__c> ProductConfigList = new List<cscfga__Product_Configuration__c>();
                List<Pricing_Approval_Request_Data__c> replacedPardList = new List<Pricing_Approval_Request_Data__c>();
                List<Pricing_Approval_Request_Data__c> pardList = new List<Pricing_Approval_Request_Data__c>();
                Map<Id, String> mapPardOrderType = new Map<Id, String>();
                Map<Id, Id> Replaced_PARD_PC_MAP = new Map<Id, Id>();
                Map<Id, Id> PARD_PC_MAP = new Map<Id, Id>();
                Set<Id> removablePCID = new Set<Id>();
                Set<Id> replacedPCID = new Set<Id>();
                Set<Id> PCID = new Set<Id>();

                for(cscfga__Product_Configuration__c pc :ProductConfigListMap.Values()){
                    PCID.add(pc.Id);
                    removablePCID.add(pc.Id);
                    ProductConfigList.add(pc);
                    /** Update to get the order type from PC (Sowmya)*/
                    mapPardOrderType.put(pc.Id, pc.Order_Type__c);                  
                }
                System.debug('removablePCID------>'+removablePCID);
                System.debug('ProductConfigList------>'+ProductConfigList);
                System.debug('mapPardOrderType------>'+mapPardOrderType);

                /**
                 * Logic for new PC and PARD Starts
                 * Initialisation
                 */
                //List<Pricing_Approval_Request_Data__c> incompletePardList = [select Id, Product_Basket__c, Product_Configuration__c, Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,is_Pricing__c,Product_Basket__r.quote_status__c,Order_Type__c,Approved_Offer_Price_MRC__c,Approved_Offer_Price_NRC__c,Approved_Configuration__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :PCID];
                //System.debug('incompletePardList------>'+incompletePardList);

                for(Pricing_Approval_Request_Data__c PD :[select Id, Product_Basket__c, Product_Configuration__c, Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,is_Pricing__c,Product_Basket__r.quote_status__c,Order_Type__c,Approved_Offer_Price_MRC__c,Approved_Offer_Price_NRC__c,Approved_Configuration__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :PCID]){
                    if(removablePCID.contains(PD.Product_Configuration__c)){
                        removablePCID.remove(PD.Product_Configuration__c);
                    }
                    if(PD.Product_Configuration__c != null){
                        PARD_PC_MAP.put(PD.Product_Configuration__c, PD.Id);
                    }
                    pardList.add(PD);
                }
                System.debug('removablePCID------>'+removablePCID);
                System.debug('pardList------>'+pardList);
                System.debug('PARD_PC_MAP------>'+PARD_PC_MAP);

                if(!removablePCID.IsEmpty()){
                    List<Pricing_Approval_Request_Data__c> insertPardList = new List<Pricing_Approval_Request_Data__c>();
                    for(Id rePCID :removablePCID){
                        Pricing_Approval_Request_Data__c pardObj = new Pricing_Approval_Request_Data__c();
                        pardObj.Product_Configuration__c = rePCID;
                        insertPardList.add(pardObj);
                    }
                    if(insertPardList.Size()>0){insert insertPardList;}
                    System.debug('insertPardList------>'+insertPardList);

                    for(Pricing_Approval_Request_Data__c PD :insertPardList){
                        if(PD.Product_Configuration__c != null){
                            PARD_PC_MAP.put(PD.Product_Configuration__c, PD.Id);
                        }
                        pardList.add(PD);
                    }
                    System.debug('PARD_PC_MAP------>'+PARD_PC_MAP);
                }               
                /** Logic for new PC and PARD ends here*/

                /** Updation Starts*/
                List<Pricing_Approval_Request_Data__c> updatePardList = new List<Pricing_Approval_Request_Data__c>();                

                /** This list gets the CS attrbutes for PC */
                Map<Id, String> mapProdConfigIdAtt = new Map<Id, String>();
                Map<Id, List<String>> NameValuePair = new Map<Id, List<String>>();
                String actualConfigStr = '';

                List<cscfga__Attribute__c> productinfo = [SELECT Id,name,cscfga__Attribute_Definition__r.cscfga__Label__c, cscfga__Product_Configuration__c, cscfga__Value__c FROM cscfga__Attribute__c WHERE 
                    cscfga__Product_Configuration__c IN :PCID 
                    AND cscfga__Attribute_Definition__r.cscfga__Screen_Section__r.name NOT IN ('Price', 'Pricing', 'Pricing Summary')             
                    AND cscfga__Attribute_Definition__r.Is_required_for_pricing__c='Yes' order by cscfga__Attribute_Definition__r.cscfga__Row__c, cscfga__Attribute_Definition__r.cscfga__Column__c];   
                    System.debug('productinfo------>'+productinfo);          
            
                for(cscfga__Attribute__c iterator :productinfo){
                
                    System.debug('inside for');
                    if(iterator.cscfga__Product_Configuration__c != null && iterator.cscfga__Value__c != null){
                        
                        String labelName = iterator.cscfga__Attribute_Definition__r.cscfga__Label__c!=null? iterator.cscfga__Attribute_Definition__r.cscfga__Label__c: iterator.name;
                        labelName = labelName.trim();
                        labelName = labelName.endswith(':')? labelName.substring(0,labelName.length()-1): labelName;
                        
                        String val = String.valueOf(iterator.cscfga__Value__c);
                        val = val.contains(':')?val.replaceAll(':','-'):val;
                        val = val.replaceAll('\\<.*?\\>',''); /** This line will remove HTML tags*/

                        String ordertype = iterator.cscfga__Product_Configuration__c!=null && mapPardOrderType.containskey(iterator.cscfga__Product_Configuration__c)? mapPardOrderType.get(iterator.cscfga__Product_Configuration__c): '';
                        
                        if(ordertype == null || ordertype == ''){
                            System.debug('In if ordertype');
                            ordertype = 'New Provide';
                        }            
                        System.debug('ORDERTYPE------>'+ordertype);
 
                        String nameValStr = '';
                        nameValStr = nameValStr+labelName+ ':' + val;
                        
                        if(NameValuePair.containsKey(iterator.cscfga__Product_Configuration__c)){
                            List<String> nameValList = NameValuePair.get(iterator.cscfga__Product_Configuration__c);
                            nameValList.add(nameValStr);
                            NameValuePair.put(iterator.cscfga__Product_Configuration__c, nameValList);
                        } else{
                            List<String> nameValList = new List<String>();
                            String tmp = 'Order Type:'+ordertype;
                            nameValList.add(tmp);
                            nameValList.add(nameValStr);
                            NameValuePair.put(iterator.cscfga__Product_Configuration__c, nameValList);
                        }
                    }
                    System.debug('ritzzz nameValList1'+NameValuePair);
                }
                /** Logic ends here*/

                /** 
                 * Logic starts for NP Orders
                 * Logic will be based on if a product config contains replaced product config or not
                 * This logic is for the one without replaced product config
                 */
                for(cscfga__Product_Configuration__c ProductConfig :ProductConfigList){
                    Id pardID = PARD_PC_MAP.get(ProductConfig.Id);
                    Pricing_Approval_Request_Data__c PD = (new Map<Id, Pricing_Approval_Request_Data__c>(pardList)).get(pardID);
        
                    /**
                     * Here update the attributes of PD with attributes of PC
                     * These fields are common for all PC's for all Order Type
                     */
                    PD.Product_Configuration__c = productConfig.id;
                    PD.Product_Basket__c = productConfig.cscfga__Product_Basket__c;
                    PD.Rate_RC__c = productConfig.Rate_Card_RC__c!=null?productConfig.Rate_Card_RC__c: 0.00;
                    PD.Rate_NRC__c = productConfig.Rate_Card_NRC__c!=null?productConfig.Rate_Card_NRC__c: 0.00; 
                    PD.Offer_RC__c =  productConfig.cscfga_Offer_Price_MRC__c;
                    PD.Offer_NRC__c =  productConfig.cscfga_Offer_Price_NRC__c;     
                    PD.Contract_Term__c = productConfig.cscfga__Contract_Term__c;
                    PD.Rate_Burst_Rate__c = productConfig.Rate_Burst_Rate__c;
                    PD.Target_Burst_Rate__c = productConfig.Target_Burst_Rate__c;
                    PD.Rate_China_Burst_Rate__c = productConfig.Rate_China_Burst_Rate__c;                   
                    PD.Target_China_Burst_Rate__c = productConfig.Target_China_Burst_Rate__c;  
                    PD.Actual_Configuration__c = mapProdConfigIdAtt.containskey(PD.Product_Configuration__c)? mapProdConfigIdAtt.get(PD.Product_Configuration__c): null;
                    /** Logic for actual config ends here*/
 
                    /** For product information name value pair*/
                    if(NameValuePair.containsKey(ProductConfig.id)){
                        NameValuePair.put(pardID,NameValuePair.get(ProductConfig.id));
                        NameValuePair.remove(ProductConfig.id);
                    }      
                    System.debug('NameValuePair------>'+NameValuePair);
                    
                    updatePardList.add(PD);
                }
                System.debug('updatePardList------>'+updatePardList);   

                /** Final updation*/
                if(updatePardList.Size() > 0){    
                    update updatePardList;
                    updateProductInformationFromPCTrigger(PCID);
                }
            }
        } catch(Exception e){
            
          }
    }

    /** This method will retrieve the PARD whenever it is called*/
    public static List<Pricing_Approval_Request_Data__c> getPARD(Set<Id> pcID){  

        String pardQuery = 'select id, Approved_RC__c, Approved_NRC__c, Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__c,'+
                            'Actual_Configuration__c, Approved_Configuration__c,Approved_Offer_Price_MRC__c, Approved_Offer_Price_NRC__c, Offer_NRC__c,'+
                            'Offer_RC__c, Rate_NRC__c, Rate_RC__c,is_Pricing__c ,'+
                            'Product_Basket__c,Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,Product_Basket__r.quote_status__c '+                 
                            ' from Pricing_Approval_Request_Data__c '+
                            'WHERE Product_Configuration__c =:pcID LIMIT 500';
       
        List<Pricing_Approval_Request_Data__c> pardList = Database.query(pardQuery);
        
        return pardList;
    }

    public static void setActualConfiguration(List<cscfga__Product_Configuration__c> ProductConfig){

        Map<Id, String> mapProdConfigIdAtt = new Map<Id, String>(); 
        List<Pricing_Approval_Request_Data__c> updatePardList = new List<Pricing_Approval_Request_Data__c>();
        String actualConfigStr = '';
        
        List<cscfga__Attribute__c> attList = [SELECT Id,cscfga__Product_Configuration__c, cscfga__Value__c FROM cscfga__Attribute__c 
            WHERE cscfga__Product_Configuration__c IN :ProductConfig
                AND cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__c NOT IN (null, '') 
                AND cscfga__Attribute_Definition__r.cscfga__Screen_Section__r.name NOT IN ('Price', 'Pricing', 'Pricing Summary')
                AND (NOT cscfga__Attribute_Definition__r.cscfga__Configuration_Screen__r.name like '%Enrichment%') 
                AND cscfga__is_active__c=true           
                AND cscfga__Attribute_Definition__r.cscfga__Column__c NOT IN (null) order by cscfga__Attribute_Definition__r.cscfga__Row__c, cscfga__Attribute_Definition__r.cscfga__Column__c];
        
        for(cscfga__Attribute__c iterator :attList){
            if(iterator.cscfga__Product_Configuration__c != null && iterator.cscfga__Value__c != null && !iterator.cscfga__Value__c.contains('<')){
                if(mapProdConfigIdAtt.containsKey(iterator.cscfga__Product_Configuration__c)){
                    String tempStr = mapProdConfigIdAtt.get(iterator.cscfga__Product_Configuration__c) + ',' + iterator.cscfga__Value__c;
                    mapProdConfigIdAtt.remove(iterator.cscfga__Product_Configuration__c);
                    mapProdConfigIdAtt.put(iterator.cscfga__Product_Configuration__c, tempStr);
                    tempStr='';
                } else{
                    mapProdConfigIdAtt.put(iterator.cscfga__Product_Configuration__c, iterator.cscfga__Value__c);
                }
            }  
        }
        
        //List<Pricing_Approval_Request_Data__c> pardList = [select Id,Product_Basket__c, Product_Configuration__c, Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,is_Pricing__c,Product_Basket__r.quote_status__c,Approved_Offer_Price_MRC__c,Approved_Offer_Price_NRC__c,Actual_Configuration__c,Approved_Configuration__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :ProductConfig];

        for(Pricing_Approval_Request_Data__c pdata :[select Id,Product_Basket__c, Product_Configuration__c, Product_Configuration__r.csordtelcoa__Replaced_Product_Configuration__c,is_Pricing__c,Product_Basket__r.quote_status__c,Approved_Offer_Price_MRC__c,Approved_Offer_Price_NRC__c,Actual_Configuration__c,Approved_Configuration__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :ProductConfig]){
            if(mapProdConfigIdAtt.containsKey(pdata.Product_Configuration__c)){
                pdata.Actual_Configuration__c = mapProdConfigIdAtt.get(pdata.Product_Configuration__c);       
                updatePardList.add(pdata);
            }   
        }
        
        update updatePardList;
    }

    public static void sleep(Long inputTime){
        Long startTime = System.currentTimeMillis();           
        Long startTime1=startTime+inputTime;
        Long startTime2=0;
        while(startTime2<startTime1){
            startTime2=System.currentTimeMillis();
        }         
    }   

    /** Method update the product information of PARD*/
    public static void updateProductInformation(Map<Id, List<String>> NameValuePair){
        try{
            List<Pricing_Approval_Request_Data__c> updatePardList = new List<Pricing_Approval_Request_Data__c>();
        
            for(Id pard :NameValuePair.keySet()){
                Pricing_Approval_Request_Data__c pd = new Pricing_Approval_Request_Data__c();
                pd.Id = pard;
                
                List<String> nameValListdata = NameValuePair.get(pard);
                String tempstr = '';
                
                for(String actualString :nameValListdata){
                    List<String> slist = actualString.split(':');
                    tempstr += slist.get(0)+':';
                    try{
                        Id tempid = slist.get(1);
                        String sObjName = tempid.getSObjectType().getDescribe().getName();
                        
                        if(CheckRecursive.PricingApprovalclass()){
                            SObject record = Database.query('Select Name From ' + sObjName + ' Where Id = :tempid');
                            tempstr += String.valueOf(record.get('name'))+'; ';
                            System.debug('id objectname name '+tempid+' '+sObjName+' '+String.valueOf(record.get('name')));
                        }
                    }
                    catch(Exception e){
                        tempstr += slist.get(1)+'; ';   
                    }
                }   
                System.debug('ritesh tempstr'+tempstr);

                String temp2 = pd.Product_Information__c!=null? pd.Product_Information__c: '';
                tempstr = tempstr.trim();
                
                if(tempstr.endswith(',') || tempstr.endswith(':') || tempstr.endswith(';')){
                    tempstr = tempstr.substring(0,tempstr.length()-1);
                }
                pd.Product_Information__c = tempstr!=null? tempstr.replaceAll('^,+|,+,$',''): temp2;
                
                updatePardList.add(pd);
            }
            system.debug('ritesh updatePardList'+updatePardList);
            
            if(updatePardList.size()>0){update updatePardList;}
        }
        catch(Exception e){
        
        }
        
    }  
    
    public static void updateProductInformationFromPCTrigger(Set<Id> pcIDList){
        Map<Id, List<String>> NameValuePair1 = new Map<Id, List<String>>(); 
        Map<Id, List<String>> NameValuePair2 = new Map<Id, List<String>>();
        Map<Id, String> mapPardOrderType = new Map<Id, String>();
        Map<Id, Id> PCToPardMap = new Map<Id, Id>();
    
        //List<Pricing_Approval_Request_Data__c> PARDList = [select Id,Order_Type__c,Product_Configuration__c,Product_Basket__r.Quote_Status__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :pcIDList];
        //System.debug('PARDList'+PARDList);
        
        for(Pricing_Approval_Request_Data__c PD :[select Id,Order_Type__c,Product_Configuration__c,Product_Basket__r.Quote_Status__c from Pricing_Approval_Request_Data__c where Product_Configuration__c IN :pcIDList]){
            if(PD.Product_Configuration__c != null && PD.Product_Basket__r.Quote_Status__c != 'Approved'){
                PCToPardMap.put(PD.Product_Configuration__c, PD.Id);
            }
            mapPardOrderType.put(PD.Product_Configuration__c, PD.Order_Type__c);
        }

        if(!PCToPardMap.keySet().isEmpty()){        
            List<cscfga__Attribute__c> productinfo = [SELECT Id,name,cscfga__Attribute_Definition__r.cscfga__Label__c, cscfga__Product_Configuration__c,cscfga__Display_Value__c, cscfga__Value__c FROM cscfga__Attribute__c 
                WHERE cscfga__Product_Configuration__c IN :PCToPardMap.keySet()
                    AND cscfga__Attribute_Definition__r.cscfga__Screen_Section__r.name NOT IN ('Price', 'Pricing', 'Pricing Summary')             
                    AND cscfga__Attribute_Definition__r.Is_required_for_pricing__c='Yes' order by cscfga__Attribute_Definition__r.cscfga__Row__c, cscfga__Attribute_Definition__r.cscfga__Column__c];
            System.debug('productinfo'+productinfo);
            
            for(cscfga__Attribute__c iterator1 :productinfo){
                
                if(iterator1.cscfga__Product_Configuration__c != null && iterator1.cscfga__Value__c != null){

                    String labelName1 = iterator1.cscfga__Attribute_Definition__r.cscfga__Label__c!=null? iterator1.cscfga__Attribute_Definition__r.cscfga__Label__c: iterator1.name;
                    labelName1 = labelName1.trim();
                    labelName1 = labelName1.endswith(':')? labelName1.substring(0,labelName1.length()-1): labelName1;
                    String val = String.valueOf(iterator1.cscfga__Value__c);

                    try{
                        /** Incase the value is an ID ,then take the display value*/
                        Id CheckIfValueIsId = iterator1.cscfga__Value__c;
                        val = iterator1.cscfga__Display_Value__c!=null? iterator1.cscfga__Display_Value__c: '';             
                    } catch(Exception e){
                        
                    }
                
                    val = val.contains(':')? val.replaceAll(':','-'): val;
                    val = val.replaceAll('\\<.*?\\>',''); /** This line will remove html tags*/

                    String ordertype = iterator1.cscfga__Product_Configuration__c!=null && mapPardOrderType.containskey(iterator1.cscfga__Product_Configuration__c)? mapPardOrderType.get(iterator1.cscfga__Product_Configuration__c): '';

                    if(ordertype == null || ordertype == ''){
                        System.debug('In if ordertype');
                        ordertype = 'New Provide';
                    }            
                    System.debug('ORDERTYPE------>'+ordertype);
                
                    String nameValStr1 = '';
                    nameValStr1 = nameValStr1+labelName1+ ':' + val;
                    System.debug('nameValStr1----->'+nameValStr1);
                    
                    if(NameValuePair1.containsKey(iterator1.cscfga__Product_Configuration__c)){
                        List<String> nameValList1 = NameValuePair1.get(iterator1.cscfga__Product_Configuration__c);
                        nameValList1.add(nameValStr1);
                        NameValuePair1.put(iterator1.cscfga__Product_Configuration__c, nameValList1);
                    } else{
                        List<String> nameValList1 = new List<String>();
                        String tmp = 'Order Type:'+ordertype;
                        nameValList1.add(tmp);
                        nameValList1.add(nameValStr1);
                        NameValuePair1.put(iterator1.cscfga__Product_Configuration__c, nameValList1);
                    }
                }
                System.debug('ritzzz productinfo'+productinfo);
                System.debug('ritzzz nameValList1'+NameValuePair1);
            }
            
            for(Id ProductConfigId :NameValuePair1.keySet()){
                if(PCToPardMap.containsKey(ProductConfigId)){
                    NameValuePair2.put(PCToPardMap.get(ProductConfigId), NameValuePair1.get(ProductConfigId));          
                }
            }
            System.debug('NameValuePair1'+NameValuePair1);

            /** Call the method from here*/
            if(NameValuePair2.Size() > 0){PricingApproval.updateProductInformation(NameValuePair2);}
        }
    }
}