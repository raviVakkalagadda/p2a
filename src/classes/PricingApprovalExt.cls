public with sharing class PricingApprovalExt 
{
//For Test class coverage commented by Anuradha in the GIT 3/10/2017
//As part of CR this class is commenting
   /* private ApexPages.StandardController sc;
    public Boolean isVisible;
    public Boolean isVisible1 {get; set;}
    public Boolean isLApprovedRC {get; Set;}
    public Boolean isLApprovedNRC {get; Set;}
    public string isLastApprovedRC {get; Set;}
    public string isLastApprovedNRC {get; Set;}    
    public decimal TRateRC {get; Set;}
    public decimal TRateNRC {get; Set;}
    
    public decimal TTargetRC {get; Set;}
    public decimal TTargetNRC {get; Set;}
    public decimal TRateBurstRate {get; Set;}
    public decimal TRateChinaBurstRate {get; Set;} 
    public decimal TTargetBurstRate {get; Set;}
    public decimal TTargetChinaBurstRate {get; Set;}
    public decimal TApprovedBurstRate {get; Set;}
    public decimal TApprovedChinaBurstRate {get; Set;}
    
    public decimal TOfferRC {get; Set;}
    public decimal TOfferNRC {get; Set;}
    public decimal TApprovedRC {get; Set;}
    public decimal TApprovedNRC {get; Set;}
    public decimal TLApprovedRC {get; Set;}
    public decimal TLApprovedNRC {get; Set;}
    public decimal TCostRC {get; Set;}
    public decimal TCostNRC {get; Set;}
    public decimal TDiscount {get; Set;}    
    public Pricing_Approval_Request_Data__c TDiscountval{get; set;}
    public Boolean selected {get; set;}
    public decimal GCapex {get; Set;}
    public decimal discountcostNRC {get; Set;}
    public Boolean pageRefresh {get; Set;} 
    
    public PricingApprovalExt(ApexPages.StandardController sc) 
    {
        Case caseObj =(case)sc.getRecord();
        this.sc = sc;
        TDiscountval=new Pricing_Approval_Request_Data__c();
        PricingFinancialSummaryManishcls controller  = new PricingFinancialSummaryManishcls( new ApexPages.StandardController(caseObj));
        GCapex  = controller.CapexrandTotal;
        GrandTotal();
        
    } 
    ID csid=ApexPages.currentPage().getParameters().get('id'); 
    public PageReference updateItems()
    {
        List<Pricing_Approval_Request_Data__c > priceApplst = new List<Pricing_Approval_Request_Data__c >();
        if(pc != null)
        {
            for( PricingApprovalWrapper obj : pc) 
            {
                priceApplst.add(obj.priceObj); 
            }
            update priceApplst ;
            GrandTotal();
        }
        pc = null;
        pageRefresh = true;
        return null;
    } 
    
    public PricingApprovalWrapper[] pc
    {
        get 
        {
            if (pc == null) 
            { 
                case r = [
                    select Product_Basket__r.ID, Order_Type__c
                    from case
                    where id = :sc.getId()
                ];
                Id ppmId = r.Product_Basket__r.ID;
                if(r.Order_Type__c != null && !r.Order_Type__c.equalsignorecase('new')){
                    isLApprovedRC = False;
                    isLApprovedNRC = False;
                }
                else{                      
                    isLApprovedRC = True;
                    isLApprovedNRC = True;
                } 
                
                pc =  new List<PricingApprovalWrapper>();
                
                for (Pricing_Approval_Request_Data__c obj : [
                    select Product_Configuration__c,Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__r.Offnet_Product__c,
                    Product_Configuration__r.Name, Rate_RC__c, Rate_NRC__c, Offer_RC__c,Last_Approved_NRC__c, Is_Offnet__c, Product_Information__c,
                    Offer_NRC__c, Approved_RC__c, Approved_NRC__c, Cost_RC__c, Cost_NRC__c, Last_Approved_RC__c, Discount_Approved_RC__c,
                    Margin__c, Contract_Term__c, Rate_Burst_Rate__c, Rate_China_Burst_Rate__c, Target_Burst_Rate__c, Target_China_Burst_Rate__c,
                    Approved_Burst_Rate__c, Approved_China_Burst_Rate__c
                    from Pricing_Approval_Request_Data__c
                    where Product_Basket__c = :ppmId and Product_Configuration__c!= null and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'VLAN Group%')
                    and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Master VPLS Service%') and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Master IPVPN Service%')
                    and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'Standalone ASBR%') and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'ASBR%')
                    and (NOT Product_Configuration__r.cscfga__Product_Definition__r.Name like 'NID%')])
                {
                   
          //Following code added as part of Multicurrencydefect fix Defect#13077
          // Author: Anuradha
          
          if(obj.Offer_NRC__c != null)  obj.Offer_NRC__c = obj.Offer_NRC__c.setscale(2);
                    if(obj.Rate_NRC__c !=null)    obj.Rate_NRC__c  = obj.Rate_NRC__c.setscale(2);
                    if(obj.Rate_RC__c!=null)      obj.Rate_RC__c   = obj.Rate_RC__c.setscale(2);
                    if(obj.Offer_RC__c != null)   obj.Offer_RC__c = obj.Offer_RC__c.setscale(2);
                    if(obj.Last_Approved_NRC__c!= null) obj.Last_Approved_NRC__c = obj.Last_Approved_NRC__c.setscale(2);
                    if(obj.Approved_RC__c!= null)  obj.Approved_RC__c = obj.Approved_RC__c.setscale(2);
                    if(obj.Cost_RC__c!=null)       obj.Cost_RC__c = obj.Cost_RC__c.setscale(2);
                    if(obj.Cost_NRC__c!=null)      obj.Cost_NRC__c = obj.Cost_NRC__c.setscale(2);
                    if(obj.Last_Approved_RC__c!= null) obj.Last_Approved_RC__c= obj.Last_Approved_RC__c.setscale(2);
                    if(obj.Discount_Approved_RC__c!= null)  obj.Discount_Approved_RC__c = obj.Discount_Approved_RC__c.setscale(2);
                    if(obj.Rate_Burst_Rate__c != null)      obj.Rate_Burst_Rate__c =  obj.Rate_Burst_Rate__c.setscale(2);
                    if(obj.Rate_China_Burst_Rate__c!= null) obj.Rate_China_Burst_Rate__c = obj.Rate_China_Burst_Rate__c.setscale(2);
                    if(obj.Target_Burst_Rate__c != null)    obj.Target_Burst_Rate__c = obj.Target_Burst_Rate__c.setscale(2); 
                    if(obj.Target_China_Burst_Rate__c != null) obj.Target_China_Burst_Rate__c =obj.Target_China_Burst_Rate__c.setscale(2);
          
          
          //Code Ended 13077
          
          
          obj.Discount_Approved_RC__c=TDiscountval.Discount_Approved_RC__c;
                    PricingApprovalWrapper  wrappObj =  new PricingApprovalWrapper (obj,false);
                    wrappObj.disable  = (obj.Product_Configuration__r.Offnet_Product__c==''|| obj.Product_Configuration__r.Offnet_Product__c=='No' );
                    pc.add(wrappObj);
                    TDiscountval.Discount_Approved_RC__c=obj.Discount_Approved_RC__c ==null ? 0 : obj.Discount_Approved_RC__c;
                }
                List<Pricing_Approval_Request_Data__c> obj = [select Product_Configuration__c, Product_Configuration__r.cscfga__Product_Family__c, Product_Configuration__r.Name
                    from Pricing_Approval_Request_Data__c
                    where Product_Basket__c =:ppmid and Product_Configuration__r.cscfga__Product_Family__c IN ('TWI Multihome', 'Internet-Onnet', 'IPT Multihome')];
                //{
                    if(obj.size() >0)
                    {
                        isVisible1 = false;
                        }
                        else
                        {
                        isVisible1 = true;
                        }
                //}
                system.debug('isVisible1=>'+isVisible1+' isLApprovedRC=>'+isLApprovedRC);
            }
            return pc;
        }
        private set;
    } 
   public void GrandTotal()
    {
        TRateRC = 0; TRateNRC = 0; TOfferRC = 0; TOfferNRC = 0; TApprovedRC = 0; TApprovedNRC = 0; TCostRC = 0; TCostNRC = 0; TLApprovedRC = 0; TLApprovedNRC = 0;  discountcostNRC = 0;
        TRateBurstRate = 0; TRateChinaBurstRate = 0; TTargetBurstRate = 0; TTargetChinaBurstRate = 0; TApprovedBurstRate = 0; TApprovedChinaBurstRate = 0;
        GCapex   =  GCapex   == null ? 0 :    gcapex ;            
        
        
        System.debug('PX----' + pc);
        
        
        if(Pc.size()>0){
            for(integer i     =0; i<pc.size(); i++){
                TRateRC+=pc[i].priceObj.Rate_RC__c                                                   == null ? 0 :pc[i].priceObj.Rate_RC__c;
                TRateNRC+=PC[i].priceObj.Rate_NRC__c                                                 == null ? 0 :PC[i].priceObj.Rate_NRC__c;
                TOfferRC+=Pc[i].priceObj.Offer_RC__c                                                 == null ? 0 :Pc[i].priceObj.Offer_RC__c;
                TOfferNRC+=Pc[i].priceObj.Offer_NRC__c                                               == null ? 0 :Pc[i].priceObj.Offer_NRC__c;
                TApprovedRC+=Pc[i].priceObj.Approved_RC__c                                           == null ? 0 :Pc[i].priceObj.Approved_RC__c;
                TApprovedNRC+=Pc[i].priceObj.Approved_NRC__c                                         == null ? 0 :Pc[i].priceObj.Approved_NRC__c;
                TCostRC+=Pc[i].priceObj.Cost_NRC__c                                                  == null ? 0 :Pc[i].priceObj.Cost_RC__c;
                TCostNRC+=Pc[i].priceObj.Cost_NRC__c                                                 == null ? 0 :Pc[i].priceObj.Cost_NRC__c;
                TLApprovedRC+=Pc[i].priceObj.Last_Approved_RC__c                                     == null ? 0 :Pc[i].priceObj.Last_Approved_RC__c;
                TLApprovedNRC+=Pc[i].priceObj.Last_Approved_NRC__c                                   == null ? 0 :Pc[i].priceObj.Last_Approved_NRC__c;
                
                TRateBurstRate+=pc[i].priceObj.Rate_Burst_Rate__c                                    == null ? 0 :pc[i].priceObj.Rate_Burst_Rate__c;
                TRateChinaBurstRate+=pc[i].priceObj.Rate_China_Burst_Rate__c                         == null ? 0 :pc[i].priceObj.Rate_China_Burst_Rate__c;
                TTargetBurstRate+=pc[i].priceObj.Target_Burst_Rate__c                                == null ? 0 :pc[i].priceObj.Target_Burst_Rate__c;
                TTargetChinaBurstRate+=pc[i].priceObj.Target_China_Burst_Rate__c                     == null ? 0 :pc[i].priceObj.Target_China_Burst_Rate__c;
                TApprovedBurstRate+=pc[i].priceObj.Approved_Burst_Rate__c                            == null ? 0 :pc[i].priceObj.Approved_Burst_Rate__c;
                TApprovedChinaBurstRate+=pc[i].priceObj.Approved_China_Burst_Rate__c                 == null ? 0 :pc[i].priceObj.Approved_China_Burst_Rate__c;
            }
            // Added setScale(2) as part of Multicurrency defect fix Defect#13077
            TApprovedRC = (TApprovedRC-TDiscountval.Discount_Approved_RC__c).setscale(2);
            TCostNRC = (TCostNRC + gcapex).setscale(2);
        }
        
    } 
   public class PricingApprovalWrapper 
    {
        public Boolean selectRecord {get;set;}
        public Pricing_Approval_Request_Data__c priceObj{get;set;}
        public Boolean disable {get;set;}        
        public PricingApprovalWrapper(Pricing_Approval_Request_Data__c  obj,Boolean selectRecord)
        {
            priceObj = obj;
            this.selectRecord  = selectRecord;
            disable  = false;
        }          
    }
    
    public PageReference createCase()
    {
        List<Case>  caseLst =  new List<Case>();
        list<case> updateCaseList = new list<case>();
        Set<ID> prductConfigSet  = new Set<ID>();
        
        //Record types
        Id recGcpe  = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'GCPE Record Type'); 
        Id localLop = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Local Loop Record Type');
        Id except   = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Escalate');
        
        for(PricingApprovalWrapper  obj : pc)
        {    
            if(obj.selectRecord)
            {
                System.debug('Selected products>>>>>>>>>'+obj+obj.selectRecord+'@@@@obj.priceObj.Product_Configuration__c'+obj.priceObj.Product_Configuration__c);
                prductConfigSet.add(obj.priceObj.Product_Configuration__c);
                System.debug('Selected products>>>>>>>>>'+obj.priceObj.Product_Configuration__c);
            }
        }
        Map<String,cscfga__Attribute__c> attMap =  new Map<String,cscfga__Attribute__c>();
        Map<ID,cscfga__Product_Configuration__c> productConfig = new Map<id,cscfga__Product_Configuration__c>([select id, cscfga__Product_Basket__r.CurrencyIsoCode ,cscfga__Product_Basket__r.Product_Basket_Id__c,csordtelcoa__Replaced_Service__c,csordtelcoa__Replaced_Subscription__c,csordtelcoa__Replaced_Service__r.Service_Speed__c,csordtelcoa__Replaced_Service__r.Supplier_Name__c,csordtelcoa__Replaced_Service__r.CompleteAddress__c,csordtelcoa__Replaced_Service__r.Serial_Number_For_CPE__c,csordtelcoa__Replaced_Service__r.Supplier_Service_ID__c,No_of_Off_Nets__c,Is_Offnet__c, cscfga__Root_Configuration__c, name, cscfga__Product_Definition__c, cscfga__Product_Family__c, cscfga__Product_Basket__c, cscfga__Product_Basket__r.cscfga__Opportunity__c,cscfga__Product_Basket__r.cscfga__Opportunity__r.accountID,cscfga__Product_Basket__r.cscfga__Opportunity__r.ownerId from cscfga__Product_Configuration__c WHERE 
                
               // (
               // ((csordtelcoa__Replaced_Product_Configuration__c!=null) AND ( ((Rate_Card_NRC_Check__c = True) OR (Rate_Card_MRC_Check__c = True ))   OR ( // // ((Rate_Card_NRC_Check__c = False) AND (Rate_Card_MRC_Check__c = FALSE))  AND (((Offer_NRC_Check__c = True) OR (Offer_Check_MRC__c = True)))))) 
               // OR ((csordtelcoa__Replaced_Product_Configuration__c=null) AND (((Is_offnet__c = 'No' OR Is_offnet__c = '') AND (((Current_Offer_NRC_Check__c  //= True) OR (Current_Offer_MRC_Check__c = True)) OR (Rate_Card_NRC_Value__c = 0.00 AND Rate_Card_MRC_value__c=0.00 ) OR (Rate_Card_NRC_Value__c //<0.00 OR Rate_Card_MRC_value__c < 0.00 )) ) OR ((Is_offnet__c = 'Yes') AND  ((Current_Offer_NRC_Check__c = True) OR //(Current_Offer_MRC_Check__c = True))))))
                
               //AND 
      id in:prductConfigSet]);
            
            system.debug('CheckMapSize=======>'+attMap.size());
            
            List<cscfga__Attribute__c > attLst = [select cscfga__Product_Configuration__c, name,cscfga__Value__c from cscfga__Attribute__c where 
                (name = 'Is_Offnet__c' or name = 'Final_Country__c' or name = 'LocalLoopPopCity__c' or name = 'G_Bandwidth_Gen_name__c' or name = 'ContractTerm_Gen__c' Or
                name = 'CompleteAddress__c' Or name = 'Postal_Code__c' Or name = 'ProductName__c' Or name = 'Shd_Carrier__c' Or
                name = 'Final_Model__c' Or name = 'Quantity__c' Or name = 'Financial_Model_radio__c' Or name = 'Final_Main_grade__c' Or
                name = 'Port_Protection__c' Or name = 'GPS_Info__c' Or name = 'MTU_Size__c' Or name = 'Shd_Interface__c' Or name = 'LocalLoopPop__c')  
            and cscfga__Product_Configuration__c IN (select id from cscfga__Product_Configuration__c where id in:prductConfigSet) ];
            
            for(cscfga__Attribute__c  attObj  : attLst )
            {
                attMap.put(attObj.cscfga__Product_Configuration__c+attObj.name,attObj);     
            }
            
            for(PricingApprovalWrapper  obj : pc)
            {
                if(obj.selectRecord)
                {
                    System.debug('Selected for loop products>>>>>>>>>'+obj.priceObj);
                    cscfga__Product_Configuration__c temp = productConfig.get(obj.priceObj.Product_Configuration__c);                 
                    System.debug('temp temp '+ temp );
                    List<case> casePclist = [select id, Product_Configuration__c, Product_Basket__c from case where Product_Configuration__c =: temp.id];
                    system.debug('Testing'+temp);
                    if(temp != null && casepclist.size()==0)
                    {
                        system.debug('Test###');                    
                        Case caseObj                         = new Case();//CustomButtonSupplierQuote.CreateCase();
                        caseObj.Origin                       = 'Web';
                        caseObj.Is_Supplier_Quote_Request__c = true;
                        caseObj.Status                       = 'New';
                        caseObj.Is_Child_Case__c             = true;
                        caseObj.ParentID                     = csid;
                        caseObj.City__c                      = attMap.get(temp.Id+'LocalLoopPopCity__c') != null ? attMap.get(temp.Id+'LocalLoopPopCity__c').cscfga__Value__c:'';
                        caseObj.Country__c                   = attMap.get(temp.Id+'Final_Country__c') != null ? attMap.get(temp.Id+'Final_Country__c').cscfga__Value__c:'';
                        caseobj.Product_Configuration__c     = temp.id;
                        caseObj.Subject                      = 'Supplier quote request for '+temp.Name; 
                        caseObj.CurrencyIsoCode              = temp.cscfga__Product_Basket__r.CurrencyIsoCode;
                        caseObj.Product_Basket__c            = temp.cscfga__Product_Basket__c;
                        caseObj.Opportunity_Name__c          = temp.cscfga__Product_Basket__r.cscfga__Opportunity__c;
                        caseObj.Opportunity_Owner__c         = temp.cscfga__Product_Basket__r.cscfga__Opportunity__r.OwnerID;
                        caseObj.accountId                    = temp.cscfga__Product_Basket__r.cscfga__Opportunity__r.accountId;
                        caseObj.CS_Service__c                = temp.csordtelcoa__Replaced_Service__c;
                        caseObj.Subscription__c              = temp.csordtelcoa__Replaced_Subscription__c;
                        caseObj.Existing_Bandwidth__c        = temp.csordtelcoa__Replaced_Service__r.Service_Speed__c;
                        caseObj.Existing_Address__c          = temp.csordtelcoa__Replaced_Service__r.CompleteAddress__c;
                        caseObj.Supplier_Circuit_Id__c       = temp.csordtelcoa__Replaced_Service__r.Supplier_Service_ID__c;
                        caseObj.Serial_Number_For_CPE__c     = temp.csordtelcoa__Replaced_Service__r.Serial_Number_For_CPE__c;
                        caseObj.Existing_Supplier_Name__c    = temp.csordtelcoa__Replaced_Service__r.Supplier_Name__c;
                        caseObj.Product__c                   = attMap.get(temp.Id+'ProductName__c') != null ? attMap.get(temp.Id+'ProductName__c').cscfga__Value__c:'';
                        caseObj.Contract_Term__c             = attMap.get(temp.Id+'ContractTerm_Gen__c') != null ? attMap.get(temp.Id+'ContractTerm_Gen__c').cscfga__Value__c:'';
                        caseObj.Bandwidth__c                 = attMap.get(temp.Id+'G_Bandwidth_Gen_name__c') != null ? attMap.get(temp.Id+'G_Bandwidth_Gen_name__c').cscfga__Value__c:'';
                        caseObj.Address__c                   = attMap.get(temp.Id+'CompleteAddress__c') != null ? attMap.get(temp.Id+'CompleteAddress__c').cscfga__Value__c:'';
                        caseObj.Postal_Code__c               = attMap.get(temp.Id+'Postal_Code__c') != null ? attMap.get(temp.Id+'Postal_Code__c').cscfga__Value__c:'';
                        caseObj.Service_Type__c              = attMap.get(temp.Id+'Shd_Carrier__c') != null ? attMap.get(temp.Id+'Shd_Carrier__c').cscfga__Value__c:'';
                        
                        if(temp.cscfga__Product_Family__c == 'GCPE'){
                            caseObj.recordtypeId = recGcpe;
                            caseObj.Quantity__c          = attMap.get(temp.Id+'Quantity__c') != null ? attMap.get(temp.Id+'Quantity__c').cscfga__Value__c:'';//temp.cscfga__Quantity__c;
                            caseObj.Model_Number__c      = attMap.get(temp.Id+'Final_Model__c') != null ? attMap.get(temp.Id+'Final_Model__c').cscfga__Value__c:'';                           
                            caseObj.Rental_Purchase__c   = attMap.get(temp.Id+'Financial_Model_radio__c') != null ? attMap.get(temp.Id+'Financial_Model_radio__c').cscfga__Value__c:'';
                            caseObj.Maintanence_Level__c = attMap.get(temp.Id+'Final_Main_grade__c') != null ? attMap.get(temp.Id+'Final_Main_grade__c').cscfga__Value__c:'';
                            caseObj.Site_Address__c      = attMap.get(temp.Id+'CompleteAddress__c') != null ? attMap.get(temp.Id+'CompleteAddress__c').cscfga__Value__c:'';
                        }
                        else if((temp.cscfga__Product_Family__c == 'Local Loop')|| (temp.cscfga__Product_Family__c == 'Local Access')){
                            caseObj.recordtypeID = localLop;
                            caseObj.Telstra_Onnet_PoP__c                     = attMap.get(temp.Id+'LocalLoopPop__c') != null ? attMap.get(temp.Id+'LocalLoopPop__c').cscfga__Value__c:'';
                            caseObj.Primary_and_backup_PoP_for_Local_loop__c = attMap.get(temp.Id+'LocalLoopPop__c') != null ? attMap.get(temp.Id+'LocalLoopPop__c').cscfga__Value__c:'';
                            caseObj.Interface_Type__c                        = attMap.get(temp.Id+'Shd_Interface__c') != null ? attMap.get(temp.Id+'Shd_Interface__c').cscfga__Value__c:'';
                            caseObj.Local_Loop_Bandwidth__c                  = attMap.get(temp.Id+'G_Bandwidth_Gen_name__c') != null ? attMap.get(temp.Id+'G_Bandwidth_Gen_name__c').cscfga__Value__c:'';
                            caseObj.MTU_Size__c                              = attMap.get(temp.Id+'MTU_Size__c') != null ? attMap.get(temp.Id+'MTU_Size__c').cscfga__Value__c:'';
                            caseObj.GPS_Information_for_UAE_Sites__c         = attMap.get(temp.Id+'GPS_Info__c') != null ? attMap.get(temp.Id+'GPS_Info__c').cscfga__Value__c:'';
                            caseObj.Protected_or_Unprotected__c              = attMap.get(temp.Id+'Port_Protection__c') != null ? attMap.get(temp.Id+'Port_Protection__c').cscfga__Value__c:'';
                            
                        }
                        
                        else
                        {
                            caseObj.recordtypeID = except;
                        } 
                 if(caseObj.Country__c == 'HK' || caseObj.Country__c == 'Hong Kong' || caseObj.Country__c == 'HongKong')
                    {
                        caseObj.OwnerID = Label.Supplier_HK;
                    }                       
                    else if(caseObj.Country__c == 'Japan')
                    {
                        caseObj.OwnerID = Label.Supplier_Japan;
                    }
                    else if(caseObj.Country__c == 'Singapore')
                    {
                        caseObj.OwnerID = Label.Supplier_Singapore;
                    }
                    else if(caseObj.Country__c == 'EMEA')
                    {
                        caseObj.OwnerID = Label.Supplier_EMEA;
                    }
                    else if(caseObj.Country__c == 'China')
                    {
                        caseObj.OwnerID = Label.Supplier_China;
                    }
                    else if(caseObj.Country__c == 'New Zealand')
                    {
                        caseObj.OwnerID = Label.Supplier_New_Zealand;
                    }
                    else if(caseObj.Country__c == 'South Korea')
                    {
                        caseObj.OwnerID = Label.Supplier_South_Korea;
                    }
                    else if(caseObj.Country__c == 'Taiwan')
                    {
                        caseObj.OwnerID = Label.Supplier_Taiwan;
                    }
                    else if(caseObj.Country__c == 'Malaysia')
                    {
                        caseObj.OwnerID = Label.Supplier_Malaysia;
                    }
                    else if(caseObj.Country__c == 'Thailand')
                    {
                        caseObj.OwnerID = Label.Supplier_Thailand;
                    }
                    else if(caseObj.Country__c == 'Philippines')
                    {
                        caseObj.OwnerID = Label.Supplier_Philippines;
                    }
                    else if(caseObj.Country__c == 'Indonesia')
                    {
                        caseObj.OwnerID = Label.Supplier_Indonesia;
                    }
                    else if(caseObj.Country__c == 'Vietnam')
                    {
                        caseObj.OwnerID = Label.Supplier_Vietnam;
                    }
                    else if(caseObj.Country__c == 'India')
                    {
                        caseObj.OwnerID = Label.Supplier_India;
                    }
                    else if(caseObj.Country__c == 'US' || caseObj.Country__c == 'USA')
                    {
                        caseObj.OwnerID = Label.Supplier_Americas;
                    }
                    else
                    {
                        caseObj.OwnerID = Label.Supplier_All_Regions;
                    }

            
                        caseLst.add(caseObj );                                         
                    }
                    else
                    {
                        for(case upcase : casePclist)
                        {
                            case updatecase      = new case();
                            updatecase.id        = upcase.id;
                            updatecase.status    = 'new';
                            updatecase.ParentID  = csid;
                            updateCaseList .add(updatecase);
                        }
                        //update updateCaseList ;
                    }
                    
                    
                }
            }
            
            
            if(caseLst.size() > 0 )
            {
                insert caseLst;
                pageRefresh = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Casess created successfully'));
                //ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.CONFIRM,'Cases are created');
            }
            else if(updateCaseList.size() >0)
            {
                update updateCaseList;
                pageRefresh = true;
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.CONFIRM,'Casess updated successfully'));
            }
            else 
            {
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Please select the record'));
            }
            return null;
    }   
    
    
    public void requestSupplierQuote()
    {
        list<case> caselst = [select id from case where id =: csid];
        system.debug('list of selected case '+caselst);
        createCase();
        
    } 
   Public boolean getisVisible(){
        ID pID = UserInfo.getProfileID();
        Boolean checkVisibility = false;
        //Bhargav -- added TI Account Manager to the query parameter below -- 28 June 2016
        list<profile> pList = [Select id from profile where name in ('TI Commercial','System Administrator','TI Account Manager')];
        for (profile p : pList) {
            if (p.id == pID)
            checkVisibility = true;
        }
        return checkVisibility;
    } */    
}