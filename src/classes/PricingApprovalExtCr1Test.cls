@istest(seealldata = false )
 Public class PricingApprovalExtCr1Test{
    @istest
    Public static void PricingApprovalExtCr1Testmtd(){
        
            P2A_TestFactoryCls.sampleTestData();

      List<Profile> Profiles_Id = [Select id, Name from Profile Where id =:userinfo.getProfileid() and (Name = 'TI Commercial' Or Name = 'System Administrator') Limit 1];
        System.assertequals(Profiles_Id[0].Name,'System Administrator');
        
        User u = new User(Alias = 'standt', Email='standarduser1233@testorg.com', 
                          EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                          LocaleSidKey='en_US', ProfileId = profiles_Id[0].id, 
                          TimeZoneSidKey='America/Los_Angeles', UserName='standarduser123as4561234789@testorg.com',EmployeeNumber='123');
        insert u;
        System.assert(U.id!=null);
       
       system.runas(u){    
    
       /* 
        cscfga__Product_Basket__c pb = new cscfga__Product_Basket__c();
        insert Pb; 
        
        Cscfga__Product_Configuration__c pc = new Cscfga__Product_Configuration__c();
        pc.Name = 'IPL';
        insert Pc; 
        
        */
        
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pcList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
        
        case cobj = new case();
        Cobj.Product_Basket__c = Products[0].id;
        insert cobj;
        System.assert(Cobj.id != null);
        
        
      Pricing_Approval_Request_Data__c Pf_Request = new Pricing_Approval_Request_Data__c();
        Pf_Request.Approved_NRC__c=0;
        Pf_Request.Approved_RC__c=0;
        Pf_Request.Offer_NRC__c=0;
        Pf_Request.Offer_RC__c =0;
        Pf_Request.Cost_RC__c = 0;
        Pf_Request.Cost_NRC__c = 0;  
        Pf_Request.Contract_Term__c = 0;
        Pf_Request.Is_offnet__c = true;
        Pf_Request.Product_Basket__c = cobj.Product_Basket__c;
        insert Pf_Request ; 
        System.assert(Pf_Request != null);
  
        ApexPages.StandardController sc = new ApexPages.standardController(Cobj);
       // PricingFinancialSummaryManishcls PFSM = new PricingFinancialSummaryManishcls(sc);   
       // PFSM.getisvisible(); 
       // PFSM.initlize();
        
       /* 
       cscfga__Product_Basket__c Cpb = new cscfga__Product_Basket__c();
        insert CPb;
        System.assert( Cpb.id!= null );
        
        Cscfga__Product_Configuration__c pcs = new Cscfga__Product_Configuration__c();
        pcs.Name = 'IPL';
        insert Pcs;  
        
        */
        
        case csobj = new case();
        Csobj.Product_Basket__c = Products[0].id;
        insert csobj; 
        System.assert(Csobj.id != null);
 
       Pricing_Approval_Request_Data__c Par_app = new Pricing_Approval_Request_Data__c();
        Par_app.Product_basket__c = Products[0].id;
        Par_app.Product_Configuration__c = pcList[0].id;
        Par_app.Rate_RC__c = 123;
        Par_app.Rate_NRC__c = 123 ;
        Par_app.Offer_RC__c = 123;
        Par_app.Last_Approved_NRC__c = 456;
        Par_app.Is_Offnet__c = false ;
        Par_app.Product_Information__c = 'Code coverage';
        Par_app.Contract_Term__c = 12;
        Par_app.Rate_Burst_Rate__c = 234;
        Par_app.Rate_China_Burst_Rate__c = 234;
        Par_app.Target_Burst_Rate__c = 156;
        Par_app.Target_China_Burst_Rate__c = 298;
        Par_app.Approved_Burst_Rate__c = 234;
        Par_app.Approved_China_Burst_Rate__c = 678;
        insert Par_app ;
       System.assert(Par_app.id != null);
         
        ApexPages.StandardController scon = new ApexPages.standardController(Csobj);
        //PricingFinancialSummaryManishcls PFSMS = new PricingFinancialSummaryManishcls(scon);   
        //PFSMS.getisvisible();  
        //PFSMS.initlize();
        
        /*cscfga__Product_Basket__c Cpbs = new cscfga__Product_Basket__c();
        cpbs.Onnet_vs_Offnet__c = 21;
        cpbs.Gross_Margin__c = 10.00;
        cpbs.Max_Contract_Term__c = 24;
        cpbs.Capex__c = 1536.67;
        insert CPbs;
        System.assert(CPbs != null);
         
         Cscfga__Product_Configuration__c pcon = new Cscfga__Product_Configuration__c();
        pcon.Name = 'IPL';
        insert Pcon;
        */
      
        case casobj = new case();
        Casobj.Product_Basket__c = Products[0].id;
        Casobj.Order_type__c='Renewal';
        insert casobj;
        System.assert(Casobj != null);
 
       Pricing_Approval_Request_Data__c Par_apps = new Pricing_Approval_Request_Data__c();
        Par_apps.Product_basket__c = casobj.Product_Basket__c ;
        Par_apps.Product_Configuration__c = pcList[0].id;
        Par_apps.Rate_RC__c = 123;
        Par_apps.Rate_NRC__c = 123 ;
        Par_apps.Offer_RC__c = 123;
        Par_apps.Last_Approved_NRC__c = 456;
        Par_apps.Is_Offnet__c = true ;
        Par_apps.Product_Information__c = 'Code coverage';
        Par_apps.Contract_Term__c = 12;
        Par_apps.Rate_Burst_Rate__c = 234;
        Par_apps.Rate_China_Burst_Rate__c = 234;
        Par_apps.Target_Burst_Rate__c = 156;
        Par_apps.Target_China_Burst_Rate__c = 298;
        Par_apps.Approved_Burst_Rate__c = 234;
        Par_apps.Approved_China_Burst_Rate__c = 678;
        insert Par_apps ;
       System.assert(Par_apps.id != null);
        

         Test.starttest();
        PricingApprovalExtCr1 Ext = new PricingApprovalExtCr1(new ApexPages.StandardController(Casobj));
        PricingApprovalExtCr1.PricingApprovalWrapper pricAppWrapObj = new PricingApprovalExtCr1.PricingApprovalWrapper(Par_apps,true);

        Ext.updateItems();
        Ext.GrandTotal();
        Ext.getisVisible();
        Ext.getIsAccessible();
        
        Ext.isLastApprovedRC = '565';
        Ext.isLastApprovedNRC = '55';
        Ext.TTargetRC = 55.5;
        Ext.TTargetNRC = 65.5;
        Ext.TDiscount = 35.3;
        Ext.selected = true;
        
        Test.stoptest();
           }
        }
     }