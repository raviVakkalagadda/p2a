@isTest(SeeAllData = false) 
public class PricingApprovalReqdataTrggrHndlerTest {  
    
   Public static testMethod void MethodTest(){
       
    List<Pricing_Approval_Request_Data__c> PriAppReqDatList; 
    PriAppReqDatList = new List<Pricing_Approval_Request_Data__c>{
        new Pricing_Approval_Request_Data__c(CurrencyIsoCode = 'INR',Cost_RC__c=5,Cost_NRC__c=6)
    };
     Map<Id, SObject> newPARDMap;
     Map<Id, SObject> oldPARDMap;
    pricingApprovalRequestDataTriggerHandler pard = new pricingApprovalRequestDataTriggerHandler();
    pard.beforeUpdate(PriAppReqDatList, newPARDMap, oldPARDMap);
    pricingApprovalRequestDataTriggerHandler.updateCurrency(PriAppReqDatList);
    pricingApprovalRequestDataTriggerHandler.muteAllTriggers(); 
    }
}