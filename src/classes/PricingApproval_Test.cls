@isTest(SeeAllData = false)
public class PricingApproval_Test{
   static testMethod void UnitTest(){
       
          P2A_TestFactoryCls.sampleTestData();
          //List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);
          List<Account> acc = P2A_TestFactoryCls.getAccounts(1);
          List<Opportunity> opp = P2A_TestFactoryCls.getOpportunitys(1,acc);
          List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasketHdlr(1,opp);
          
          List<cscfga__Product_Definition__c> pd = P2A_TestFactoryCls.getProductdef(1);       
          List<cscfga__Product_Bundle__c> bundle = P2A_TestFactoryCls.getProductBundleHdlr(1,opp);
          List<cscfga__Configuration_Offer__c> offr = P2A_TestFactoryCls.getOffers(1);
          List<cscfga__Product_Configuration__c> pclist =  P2A_TestFactoryCls.getProductonfig(2,pblist,pd,bundle,offr); 
          pclist[0].csordtelcoa__Replaced_Product_Configuration__c = pclist[1].Id;
          update pclist;
          System.assert(pclist!=null);
          List<Pricing_Approval_Request_Data__c> prd =  P2A_TestFactoryCls.getProductonfigappreq(1,pblist,pclist);
          List<cscfga__Configuration_Screen__c> screen = P2A_TestFactoryCls.getConfigScreen(1,pd);
          List<cscfga__Screen_Section__c> screenSec = P2A_TestFactoryCls.getScreenSec(1,screen);
          
          List<cscfga__Attribute_Definition__c > attDefs = P2A_TestFactoryCls.getAttributesdef(2,pclist,pd,screen,screenSec);
          attDefs[0].Is_required_for_pricing__c='Yes';
          update attDefs;
          system.assertEquals(attDefs[0].Is_required_for_pricing__c , 'Yes');
          List<cscfga__Attribute__c> attList = P2A_TestFactoryCls.getAttributes(1,pclist,attDefs);
          Set<ID> basketIds = new Set<ID>();
          Map<id,cscfga__Product_Basket__c> Approvedbasket = new Map<id,cscfga__Product_Basket__c>();
          Set<ID> pcIds = new Set<ID>();
          Map<id,String> mapProdConfigIdAtt=new Map<id,String>();
          String tempStr=  mapProdConfigIdAtt.get(attList[0].cscfga__Product_Configuration__c) + ',' + attList[0].cscfga__Value__c ;
          Map<id,Pricing_Approval_Request_Data__c>updatePARD=new Map<id,Pricing_Approval_Request_Data__c>();
          for(Pricing_Approval_Request_Data__c pard1 : prd){
              updatePARD.put(pard1.id,pard1);
          }
          //update updatePARD;
          
          for(cscfga__Product_Basket__c pbs : pblist){
              basketIds.add(pbs.id); 
              Approvedbasket.put(pbs.id,pbs);              
          }
          for(cscfga__Product_Configuration__c pcs : pclist){
              pcIds.add(pcs.id);              
          }             
          mapProdConfigIdAtt.remove(attList[0].cscfga__Product_Configuration__c);
          mapProdConfigIdAtt.put(attList[0].cscfga__Product_Configuration__c,tempStr);
             
          Test.startTest();  
          PricingApproval.setPARD(basketIds);
          futureFuntion();
          PricingApproval.getPARD(pcIds);
          //PricingApproval.setApprovedPard(Approvedbasket);
          PricingApproval.setActualConfiguration(pclist);
          PricingApproval.sleep(1);
          Test.stopTest();
          System.debug('Time####'+System.currentTimeMillis())   ;
    
    }
     @future
     public static void futureFuntion(){
         P2A_TestFactoryCls.sampleTestData();
          //List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);
          List<Account> acc = P2A_TestFactoryCls.getAccounts(1);
          List<Opportunity> opp = P2A_TestFactoryCls.getOpportunitys(1,acc);
          List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasketHdlr(1,opp);
          pblist[0].quote_status__c='Approved';
          update pblist;
           system.assertEquals(pblist[0].quote_status__c , 'Approved');
          List<cscfga__Product_Definition__c> pd = P2A_TestFactoryCls.getProductdef(1);       
          List<cscfga__Product_Bundle__c> bundle = P2A_TestFactoryCls.getProductBundleHdlr(1,opp);
          List<cscfga__Configuration_Offer__c> offr = P2A_TestFactoryCls.getOffers(1);
          List<cscfga__Product_Configuration__c> pclist =  P2A_TestFactoryCls.getProductonfig(2,pblist,pd,bundle,offr); 
          pclist[0].csordtelcoa__Replaced_Product_Configuration__c = pclist[1].Id;
          update pclist;
         System.assert(pclist!=null);
          List<Pricing_Approval_Request_Data__c> prd =  P2A_TestFactoryCls.getProductonfigappreq(1,pblist,pclist);
          List<cscfga__Configuration_Screen__c> screen = P2A_TestFactoryCls.getConfigScreen(1,pd);
          List<cscfga__Screen_Section__c> screenSec = P2A_TestFactoryCls.getScreenSec(1,screen);
          
          List<cscfga__Attribute_Definition__c > attDefs = P2A_TestFactoryCls.getAttributesdef(2,pclist,pd,screen,screenSec);
          attDefs[0].Is_required_for_pricing__c='Yes';
          update attDefs;
          system.assertEquals(attDefs[0].Is_required_for_pricing__c , 'Yes');
          
          List<cscfga__Attribute__c> attList = P2A_TestFactoryCls.getAttributes(1,pclist,attDefs);
          Set<ID> basketIds = new Set<ID>();
          Map<id,cscfga__Product_Basket__c> Approvedbasket = new Map<id,cscfga__Product_Basket__c>();
          Set<ID> pcIds = new Set<ID>();
          Map<id,String> mapProdConfigIdAtt=new Map<id,String>();
          String tempStr=  mapProdConfigIdAtt.get(attList[0].cscfga__Product_Configuration__c) + ',' + attList[0].cscfga__Value__c ;
          Map<id,Pricing_Approval_Request_Data__c>updatePARD=new Map<id,Pricing_Approval_Request_Data__c>();
          for(Pricing_Approval_Request_Data__c pard1 : prd){
              updatePARD.put(pard1.id,pard1);
          }
          //update updatePARD;
          
          for(cscfga__Product_Basket__c pbs : pblist){
              basketIds.add(pbs.id); 
              Approvedbasket.put(pbs.id,pbs);              
          }
          for(cscfga__Product_Configuration__c pcs : pclist){
              pcIds.add(pcs.id);              
          }             
          mapProdConfigIdAtt.remove(attList[0].cscfga__Product_Configuration__c);
          mapProdConfigIdAtt.put(attList[0].cscfga__Product_Configuration__c,tempStr);
             
          //Test.startTest();  
          PricingApproval.setPARD(basketIds);
          //PricingApproval.setApprovedPard(Approvedbasket);
          //Test.stopTest();
     }
}