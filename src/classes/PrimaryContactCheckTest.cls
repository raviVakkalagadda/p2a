@isTest
private class PrimaryContactCheckTest {

    static testMethod void getPrimaryContactDetails() {
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        accList=P2A_TestFactoryCls.getAccounts(2);
        conList=P2A_TestFactoryCls.getContact(2,accList);
        for(Contact con :conList){
            con.Primary_Contact__c=false;
            con.Primary_Billing_Contact__c=false;
            con.Primary_Customer_Satisfaction_Contact__c=false;
        }
        update conList;
        System.assert(conList!=null);
        system.assertEquals(conList[0].Primary_Contact__c , false);
        

        List<Account> accList1 = new List<Account>();
        List<Contact> conList1 = new List<Contact>();
        accList1=P2A_TestFactoryCls.getAccounts(2);
        conList1=P2A_TestFactoryCls.getContact(2,accList);
        for(Contact con :conList1){
            con.Primary_Contact__c=false;
            con.Primary_Billing_Contact__c=false;
            con.Primary_Customer_Satisfaction_Contact__c=false;
        }
        update conList1;
        
        for(Contact con :conList1){
            //con.Primary_Contact__c=true;
            con.Primary_Billing_Contact__c=true;
            //con.Primary_Customer_Satisfaction_Contact__c=false;
        }
        update conList1;
        
        for(Contact con :conList1){
            con.Primary_Contact__c=false;
            //con.Primary_Billing_Contact__c=true;
            con.Primary_Customer_Satisfaction_Contact__c=true;
        }
        update conList1;
        
        for(Contact con :conList1){
            //con.Primary_Contact__c=false;
            con.Primary_Billing_Contact__c=false;
            //con.Primary_Customer_Satisfaction_Contact__c=true;
        }
        update conList1;
        
        for(Contact con :conList1){
            con.Primary_Contact__c=true;
            //con.Primary_Billing_Contact__c=false;
            con.Primary_Customer_Satisfaction_Contact__c=false;
        }
        update conList1;        
    }


   static testMethod void getPrimaryContactDetails1() {
        
        List<Account> accList = new List<Account>();
        List<Contact> conList = new List<Contact>();
        accList=P2A_TestFactoryCls.getAccounts(2);
        conList=P2A_TestFactoryCls.getContact(2,accList);
        for(Contact con :conList){
            con.FirstName='test';
            con.LastName='unit';
        }
        update conList;
        
        List<Account> accList1 = new List<Account>();
        List<Contact> conList1 = new List<Contact>();
        accList1=P2A_TestFactoryCls.getAccounts(2);
        conList1=P2A_TestFactoryCls.getContact(2,accList);
        for(Contact con :conList1){
            con.FirstName='test';
            con.LastName='unit';
        }
        update conList1;
        System.assert(conList1!=null);
        system.assertEquals(conList1[0].FirstName, 'test');
    }   
}