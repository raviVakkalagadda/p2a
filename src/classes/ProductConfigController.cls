public with sharing class ProductConfigController {
    private ApexPages.StandardController sc;
    public ProductConfigController(ApexPages.StandardController sc) {
        this.sc = sc;
    }
    public cscfga__Product_Configuration__c[] pc{
        get {
            if (pc == null) {
                case r = [
                        select  Product_Basket__r.ID
                        from case
                        where id = :sc.getId()
                        ];
                Id ppmId = r.Product_Basket__r.ID;
                pc = [
                        select Name,cscfga__Description__c,cscfga_Offer_Price_MRC__c,cscfga_Offer_Price_NRC__c,
                        cost__c,margin__c,cscfga__Contract_Term__c
                        from cscfga__Product_Configuration__c
                        where cscfga__Product_Basket__c = :ppmId
                        ];
            }
            return pc;
        }
        private set;
    }
}