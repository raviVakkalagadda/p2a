@isTest
private class ProductConfigControllerTest{
    
    public static testMethod  void unittest() {
        P2A_TestFactoryCls.sampleTestData();  
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(9);  

        case c = new case();
        c.Is_Feasibility_Request__c = true;
        //c.Product_Configuration__c = pclist[0].id;
        c.Product_Basket__c = pblist[0].Id;
        c.status = 'Approved';
        c.Number_of_Child_Case__c = 2;
        c.Number_of_Child_Case_Closed__c = 1;
        c.Reason_for_selecting_one_supplier__c = 'SingleSource';
        c.Is_Resource_Reservation__c = true;
        insert c;
        System.assertEquals('SingleSource',c.Reason_for_selecting_one_supplier__c); 

        ApexPages.currentPage().getParameters().put('id',c.Id);
        ApexPages.StandardController sc = new ApexPages.StandardController(c);
        ProductConfigController  ac = new ProductConfigController(sc);     
        boolean b = true;
        String id = ApexPages.currentPage().getParameters().get('id');

        Test.startTest();    
         cscfga__Product_Configuration__c[] productconfiguration = ac.pc;
        Test.stopTest();

        System.assert(b,id==null); 
    
    }   
}