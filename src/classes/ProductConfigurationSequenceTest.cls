@isTest(SeeAllData=false) 
public class ProductConfigurationSequenceTest
{
   static testMethod void test()
    {    
    
    TriggerFlags.NoProductConfigurationTriggers=true;
    P2A_TestFactoryCls.sampleTestData();
    
    
  /*   cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
                pb.name ='IPL';
                pb.cscfga__contract_term_period__c = 12;                
                pb.Product_Name__c = 'test product';   
       insert pb;
       
       List<cscfga__Product_Configuration__c>pcList=new List<cscfga__Product_Configuration__c>(); 
       
       cscfga__Product_Configuration__c pb1 = new cscfga__Product_Configuration__c();                
                pb1.name ='IPL';
                pb1.cscfga__contract_term_period__c = 12;                
                pb1.Product_Name__c = 'test product';
                pb1.cscfga__Parent_Configuration__c= pb.id;              
      
      
      cscfga__Product_Configuration__c pb2 = new cscfga__Product_Configuration__c();                
                pb2.name ='IPL';
                pb2.cscfga__contract_term_period__c = 12;                
                pb2.Product_Name__c = 'test product';
                pb2.Master_IPVPN_Configuration__c=pb.id;
                
       pcList.add(pb1);
       pcList.add(pb2);                        
       insert pcList;*/
       
        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        // List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pcList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        system.assertEquals(true,pcList!=null); 

        List<cscfga__Product_Definition__c>prodefList = [select id, name from cscfga__Product_Definition__c where Id =:Prodef];
        
        prodefList[0].Name = 'Singlehome';
        update prodefList[0];
        system.assertEquals(true,prodefList!=null); 
        system.debug('---prodefList---'+prodefList);
 
        List<cscfga__Product_Configuration__c> pcList1 = P2A_TestFactoryCls.getProductonfig(1,Products,prodefList,pbundlelist,Offerlists);
        pcList1[0].cscfga__Parent_Configuration__c = pcList[0].id;
        pcList1[0].Parent_Configuration_For_Sequencing__c = pcList[0].id;
        pcList1[0].Row_Sequence__c=1;
        pcList1[0].Product_Id__c = 'IPTS-C';
        update pcList1[0];
        
        
        
       system.assertEquals(true,pcList1[0]!=null);
       Map<Id,String> oldmap = new Map<Id,String>();
       Map<Id,String> newmap = new Map<Id,String>();
       
       oldmap.put(pcList[0].id,'IPTS-C') ;
       newmap.put(pcList1[0].id,'IPTS-C');
           
        
       TriggerFlags.NoProductConfigurationTriggers=false;
       test.startTest();
       ProductConfigurationSequence.updateParentConfigForSequence(pcList);
       ProductConfigurationSequence.updateIPCtoSingleHome(newmap,oldmap);
       test.stopTest();        
    }
    
    
    static testMethod void test1() {
    
        P2A_TestFactoryCls.sampleTestData();
        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        //List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> pcList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
         system.assertEquals(true,pcList!=null); 
        List<cscfga__Product_Definition__c>prodefList = [select id, name from cscfga__Product_Definition__c where Id =:Prodef];
        
        prodefList[0].Name = 'Singlehome';
        update prodefList[0];
        system.assertEquals(true,prodefList!=null); 
        system.debug('---prodefList---'+prodefList);
 
        
        List<cscfga__Product_Configuration__c> pcList1 = new List<cscfga__Product_Configuration__c>();
        /*
        pcList1[0].cscfga__Parent_Configuration__c = pcList[0].id;
        pcList1[0].Parent_Configuration_For_Sequencing__c = pcList[0].id;
        update pcList1[0];
        */
        
        cscfga__Product_Configuration__c pc1=new cscfga__Product_Configuration__c(Parent_Configuration_For_Sequencing__c=pcList[0].id,cscfga__Parent_Configuration__c=pcList[0].id,name = 'XYZ1',cscfga__Product_Definition__c = prodefList[0].id,cscfga__Product_Basket__c = Products[0].Id,cscfga__Product_Family__c = 'Test1 family');
        //insert pc1;
        cscfga__Product_Configuration__c pc2=new cscfga__Product_Configuration__c(Parent_Configuration_For_Sequencing__c=pcList[0].id,cscfga__Parent_Configuration__c=pcList[0].id,name = 'XYZ2',cscfga__Product_Definition__c = prodefList[0].id,cscfga__Product_Basket__c = Products[0].Id,cscfga__Product_Family__c = 'Test1 family',row_sequence__c=1);
        //insert pc2;
        
        cscfga__Product_Configuration__c pc3=new cscfga__Product_Configuration__c(name = 'XYZ1',cscfga__Product_Definition__c = prodefList[0].id,cscfga__Product_Basket__c = Products[0].Id,cscfga__Product_Family__c = 'Test1 family');
        
        pcList1.add(pc1);
        pcList1.add(pc2);
        pcList1.add(pc3);
        insert pcList1;
        
        
        
        
        system.assertEquals(true,pcList1!=null);

            
        
        ProductConfigurationSequence.updateIPTSinglehome(pcList1);
        
        
    }
       @isTest static void testExceptions(){
         ProductConfigurationSequence alls=new ProductConfigurationSequence();
        
         try{ProductConfigurationSequence.updateParentConfigForSequence(null);}catch(Exception e){}
         try{ProductConfigurationSequence.updateIPTSinglehome(null);}catch(Exception e){}
           try{ProductConfigurationSequence.updateASBR(null);}catch(Exception e){}
             try{ProductConfigurationSequence.updateIPCtoSingleHome(null,null);}catch(Exception e){}
               try{ProductConfigurationSequence.setPcHierarchy(null);}catch(Exception e){}
                 try{ProductConfigurationSequence.setPcHierarchyBeforeUpdate(null);}catch(Exception e){}
                   
          system.assertEquals(true,alls!=null);    
         
         
     }  
}
