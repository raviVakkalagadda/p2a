@isTest(SeeAllData = false)
public class ProductDetailController_Test{ 
 public static List<cscfga__Attribute__c> attribList = new List<cscfga__Attribute__c>();
 public static List<cscfga__Attribute__c> attribList1 = new List<cscfga__Attribute__c>();
 public static List<cscfga__Attribute__c> attribList2 = new List<cscfga__Attribute__c>();
 
 //public static ApexPages.StandardController controller;
 public static list<cscfga__Product_Configuration__c> productconfiglists;
 public static List<cscfga__Attribute_Definition__c> attdefList = new List<cscfga__Attribute_Definition__c>();
 public static List<cscfga__Product_Definition__c> pdList;
 public static List<cscfga__Configuration_Screen__c> configScreenList;
 public static List<cscfga__Screen_Section__c> screenSection; 
 public static List<Account> accList;
 public static List<Opportunity> oppList;
 public static List<csord__Order__c> Orderlist;
 public static List<csord__Order_Request__c> orderReq;
 public static List<cscfga__Product_Basket__c> pbList;
 public static List<cscfga__Product_Configuration__c> pcList;
 public static List<csord__Subscription__c> sub;
 public static Integer contractTermIndex ;
 public static Integer mainContractTermIndex ;
 public static List<String> additionalFields = new List<String>{'csordtelcoa__Product_Configuration__c'};
 public static Integer Index1 = 0;
 public static Integer Index2 = 1;
 
    private static void initTestData(){  
    try{
        P2A_TestFactoryCls.sampleTestData();
        accList =  P2A_TestFactoryCls.getAccounts(1);
        oppList = P2A_TestFactoryCls.getOpportunitys(1,accList );
        productconfiglists = P2A_TestFactoryCls.getProductonfig(1);
        pdList = P2A_TestFactoryCls.getProductdef(1);
        configScreenList = P2A_TestFactoryCls.getConfigScreen(1,pdList);
        screenSection = P2A_TestFactoryCls.getScreenSec(1, configScreenList);
        
        attdefList = new List<cscfga__Attribute_Definition__c>();
        cscfga__Attribute_Definition__c  Attributesdef = new cscfga__Attribute_Definition__c();
                Attributesdef.name = 'Test Att';
                Attributesdef.cscfga__Product_Definition__c= pdList[0].id;
                Attributesdef.cscfga__Configuration_Screen__c = configScreenList[0].id;
                Attributesdef.cscfga__Screen_Section__c = screenSection[0].id;
                Attributesdef.cscfga__Column__c = 2;
                Attributesdef.cscfga__Row__c = 4;
                Attributesdef.cscfga__Label__c = 'Main Contract Term';
                attdefList.add(Attributesdef); 
                insert attdefList;
                
        attribList = P2A_TestFactoryCls.getAttributes(1,productconfiglists,attdefList);
        orderReq = P2A_TestFactoryCls.getorderrequest(1);
        Orderlist = P2A_TestFactoryCls.getorder(1,orderReq);
        pbList = P2A_TestFactoryCls.getProductBasket(1);
        pcList = P2A_TestFactoryCls.getProductonfig(1);
        sub = P2A_TestFactoryCls.getSubscription(1, orderReq);
        
        //additionalFields = new List<String>{'csordtelcoa__Product_Configuration__c'};
     }catch(Exception e){}
    }
    private static testMethod void baseTriggerHandler() {
    initTestData();
    
   /* cscfga__Attribute_Definition__c  Attributesdef1 = new cscfga__Attribute_Definition__c();
    Attributesdef1.name = 'Test Att';
    Attributesdef1.cscfga__Label__c = 'Main Contract Term';
    Attributesdef1.cscfga__Product_Definition__c= pdList[0].id;
    Attributesdef1.cscfga__Configuration_Screen__c = configScreenList[0].id;
    Attributesdef1.cscfga__Screen_Section__c = screenSection[0].id;
    Attributesdef1.cscfga__Column__c = 2;
    Attributesdef1.cscfga__Row__c = 4;
    insert Attributesdef1;
    
    */
    
    List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<cscfga__Product_Basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ProductDeflist,Pbundle,Offerlists);
    List<cscfga__Configuration_Screen__c> screen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> screensection = P2A_TestFactoryCls.getScreenSec(1,screen);
    List<cscfga__Attribute_Definition__c> Attributesdef1 = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,screen,screensection);
     List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
    List<cscfga__Attribute_Definition__c> Attributesdef2 = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,screen,screensection);
     List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,orderRequestList); 
    
       List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList); 
         
         //List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    
    
    cscfga__Attribute_Field_Definition__c attrFielddef = new cscfga__Attribute_Field_Definition__c();
    attrFielddef.name = 'Attribute';
    attrFielddef.csordtelcoa__Asset_Name__c = 'AssetName'; 
    attrFielddef.cscfga__Attribute_Definition__c = Attributesdef1[0].id;
    Insert attrFielddef;
    
    
   /* cscfga__Attribute_Definition__c  Attributesdef2 = new cscfga__Attribute_Definition__c();
    Attributesdef2.name = 'Test Att1';
    Attributesdef2.cscfga__Label__c = 'Contract Term';
    Attributesdef2.cscfga__Product_Definition__c= pdList[0].id;
    Attributesdef2.cscfga__Configuration_Screen__c = configScreenList[0].id;
    Attributesdef2.cscfga__Screen_Section__c = screenSection[0].id;
    Attributesdef2.cscfga__Column__c = 2;
    Attributesdef2.cscfga__Row__c = 4;
    insert Attributesdef2;
    
    */
    
    cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c();
    Attributes1.name = 'GCPE';
    Attributes1.cscfga__Product_Configuration__c= proconfigs[0].id;
    Attributes1.cscfga__Value__c = 'Test';
    Attributes1.cscfga__Attribute_Definition__c = Attributesdef1[0].id; 
    attribList1.add(Attributes1);
    
    cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c();
    Attributes2.name = 'GCPE';
    Attributes2.cscfga__Product_Configuration__c= proconfigs[0].id;
    Attributes2.cscfga__Value__c = 'Test1';
    Attributes2.cscfga__Attribute_Definition__c = Attributesdef2[0].id; 
    attribList2.add(Attributes2);
    
    mainContractTermIndex = 0;
    contractTermIndex = 1;
    
    csord__Service__c someServ = new csord__Service__c();
    someServ.Name = 'Test service';
    //someServ.recordtypeid = serviceRecordTypeInfo.get('Active Service').getRecordTypeId();
    someServ.Product_id__c= 'test';
    someServ.Bundle_Flag__c = false;
    someServ.Parent_Bundle_Flag__c = false;
    someServ.Inventory_Status__c = 'TestInventory';
    someServ.csordtelcoa__Service_Number__c = '21234-';
    someServ.AccountId__c = accList.get(0).id;
    someServ.Opportunity__c = oppList[0].id;
    someServ.Estimated_Start_Date__c = system.now().addDays(10);
    someServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
    someServ.Selling_Entity__c = 'Telstra ( Thailand ) Co., Limited';
    someServ.csord__Status__c = 'Service created';
    someServ.csord__Order__c = ordList[0].id;
    someServ.Z_Side_Site__c = 'ZZZZ';
    someServ.A_Side_Site__c = 'ZZZZ';
    someServ.csordtelcoa__Product_Configuration__c = proconfigs[0].Id;
    someServ.csordtelcoa__Product_Basket__c = prodBaskList[0].Id;
    someServ.csord__Subscription__c = subList[0].Id;
    someServ.csord__Order_Request__c = orderRequestList[0].Id;
    insert someServ;
    System.assertequals(someServ.name , 'Test service'); 
    
    
    
      //Start Test
    Test.startTest();  
  
     ApexPages.currentPage().getParameters().put('id',someServ.Id);
     ApexPages.StandardController controller = new ApexPages.StandardController(someServ);
     
     ProductDetailController Pdcntrl = new ProductDetailController(controller);
     Pdcntrl.serviceId = 'Hello';
     Pdcntrl.removeContractTermFromChildProduct(attribList);
     //Pdcntrl.removeContractTermFromChildProduct(attribList2);      
     List<cscfga__Attribute_Definition__c> Att1 = new List<cscfga__Attribute_Definition__c>();
     for(cscfga__Attribute_Definition__c att : attdefList){
         att.cscfga__Label__c = 'Main Contract Term';
         Att1.add(att);
     }
     update Att1;  
      Test.stopTest();  
         
     boolean b = true;
     String id = ApexPages.currentPage().getParameters().get('id');  
                            
            
        }
}