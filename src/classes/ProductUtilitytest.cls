@istest
Public class ProductUtilitytest{


Public static void ProductUtilitymethod(){
     
     List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    prodBaskList[0].CurrencyIsoCode = 'USD';
    upsert prodBaskList ;
    system.assert(prodBaskList!=null);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    proconfigs[0].cscfga__Quantity__c = 5 ;
    upsert proconfigs; 
    system.assert(proconfigs!=null);
    List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
    List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
    Attributedeflist[0].cscfga__Line_Item_Sequence__c = 2 ;
    upsert Attributedeflist; 
    system.assert(Attributedeflist!=null);
    list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
      

      AttributesList[0].cscfga__is_active__c = true;
      AttributesList[0].cscfga__Is_Line_Item__c = true;
      AttributesList[0].cscfga__Line_Item_Description__c = 'Test Data' ;
      AttributesList[0].cscfga__Line_Item_Sequence__c = 2 ;
      AttributesList[0].cscfga__Price__c = 50 ;
      AttributesList[0].cscfga__Recurring__c = true;
      AttributesList[0].cscfga__List_Price__c = 50;
      upsert AttributesList; 
      system.assert(AttributesList!=null);
      
      
    Set<String> setProductBasketId = new Set<String>();
    setProductBasketId.add(prodBaskList[0].id);
    
    Map<string,string> mapProductBasketIdPriceBookId = new Map<string,string>();
    mapProductBasketIdPriceBookId.put(prodBaskList[0].id, prodBaskList[0].id);

    AssignnPriceBookToProductBasketImpl assighnpbimpl = new AssignnPriceBookToProductBasketImpl();
    assighnpbimpl.AssignPriceBook(setProductBasketId);
   
   ProductUtility.CreateOLIs(setProductBasketId);
   //ProductUtility.CreateOLIs(mapProductBasketIdPriceBookId, AttributesList,
   // map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId, AttributesList,
    //map<string,map<string,Product>> mapProductFamilymapProduct);
  // ProductUtility.CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.CreateProducts2(AttributesList);
   //ProductUtility.DeleteHardOLIs(setProductBasketId);
   //ProductUtility.GetOLILineItemDescription(AttributesList[0]);


 
}

Public static void ProductUtilitymethod2(){
    
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    prodBaskList[0].CurrencyIsoCode = 'USD';
    upsert prodBaskList ;
    system.assert(prodBaskList!=null);
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    proconfigs[0].cscfga__Quantity__c = 5 ;
    upsert proconfigs;
    system.assert(proconfigs!=null); 
    List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
    List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
    Attributedeflist[0].cscfga__Line_Item_Sequence__c = 2 ;
    upsert Attributedeflist;
    system.assert(Attributedeflist!=null); 
    list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
      

      AttributesList[0].cscfga__is_active__c = true;
      AttributesList[0].cscfga__Is_Line_Item__c = true;
      AttributesList[0].cscfga__Line_Item_Description__c = 'Test Data' ;
      AttributesList[0].cscfga__Line_Item_Sequence__c = 2 ;
      AttributesList[0].cscfga__Price__c = 50 ;
      AttributesList[0].cscfga__Recurring__c = true;
      AttributesList[0].cscfga__List_Price__c = 50;
      upsert AttributesList;
      system.assert(AttributesList!=null); 
      
      
    Set<String> setProductBasketId = new Set<String>();
    setProductBasketId.add(prodBaskList[0].id);
    
    Map<string,string> mapProductBasketIdPriceBookId = new Map<string,string>();
    mapProductBasketIdPriceBookId.put(prodBaskList[0].id, prodBaskList[0].id);

    AssignnPriceBookToProductBasketImpl assighnpbimpl = new AssignnPriceBookToProductBasketImpl();
    assighnpbimpl.AssignPriceBook(setProductBasketId);
     
   
   //ProductUtility.CreateOLIs(setProductBasketId);
   //ProductUtility.CreateOLIs(mapProductBasketIdPriceBookId, AttributesList,
   // map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId, AttributesList,
    //map<string,map<string,Product>> mapProductFamilymapProduct);
  // ProductUtility.CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.CreateProducts2(AttributesList);
   //ProductUtility.DeleteHardOLIs(setProductBasketId);
   //ProductUtility.GetOLILineItemDescription(AttributesList[0]);


 
}
Public static void ProductUtilitymethod3(){
    
 List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    prodBaskList[0].CurrencyIsoCode = 'USD';
    upsert prodBaskList ;
    system.assert(prodBaskList!=null); 
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    proconfigs[0].cscfga__Quantity__c = 5 ;
    upsert proconfigs; 
    system.assert(proconfigs!=null); 
    List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
    List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
    Attributedeflist[0].cscfga__Line_Item_Sequence__c = 2 ;
    upsert Attributedeflist; 
    system.assert(Attributedeflist!=null); 
    list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
      

      AttributesList[0].cscfga__is_active__c = true;
      AttributesList[0].cscfga__Is_Line_Item__c = true;
      AttributesList[0].cscfga__Line_Item_Description__c = 'Test Data' ;
      AttributesList[0].cscfga__Line_Item_Sequence__c = 2 ;
      AttributesList[0].cscfga__Price__c = 50 ;
      AttributesList[0].cscfga__Recurring__c = true;
      AttributesList[0].cscfga__List_Price__c = 50;
      upsert AttributesList; 
      system.assert(AttributesList!=null); 
      
      
    Set<String> setProductBasketId = new Set<String>();
    setProductBasketId.add(prodBaskList[0].id);
    
    Map<string,string> mapProductBasketIdPriceBookId = new Map<string,string>();
    mapProductBasketIdPriceBookId.put(prodBaskList[0].id, prodBaskList[0].id);

    AssignnPriceBookToProductBasketImpl assighnpbimpl = new AssignnPriceBookToProductBasketImpl();
    assighnpbimpl.AssignPriceBook(setProductBasketId);
     
   
   //ProductUtility.CreateOLIs(setProductBasketId);
   //ProductUtility.CreateOLIs(mapProductBasketIdPriceBookId, AttributesList,
   // map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId, AttributesList,
    //map<string,map<string,Product>> mapProductFamilymapProduct);
  // ProductUtility.CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.CreateProducts2(AttributesList);
   ProductUtility.DeleteHardOLIs(setProductBasketId);
//ProductUtility.GetOLILineItemDescription(AttributesList[0]);


 
}

Public static void ProductUtilitymethod4(){
    
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
    List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    prodBaskList[0].CurrencyIsoCode = 'USD';
    upsert prodBaskList ;
    system.assert(prodBaskList!=null); 
    List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
    List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
    proconfigs[0].cscfga__Quantity__c = 5 ;
    upsert proconfigs; 
    system.assert(proconfigs!=null); 
    List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
    List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
    List<cscfga__Attribute_Definition__c > Attributedeflist= P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist,configscreen,screen);
    Attributedeflist[0].cscfga__Line_Item_Sequence__c = 2 ;
    upsert Attributedeflist; 
    system.assert(Attributedeflist!=null); 
    list<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
      
       AttributesList[0].cscfga__is_active__c = true;
      AttributesList[0].cscfga__Is_Line_Item__c = true;
      AttributesList[0].cscfga__Line_Item_Description__c = 'Test Data' ;
      AttributesList[0].cscfga__Line_Item_Sequence__c = 2 ;
      AttributesList[0].cscfga__Price__c = 50 ;
      AttributesList[0].cscfga__Recurring__c = true;
      AttributesList[0].cscfga__List_Price__c = 50;
      upsert AttributesList; 
      system.assert(AttributesList!=null); 
      
    Set<String> setProductBasketId = new Set<String>();
    setProductBasketId.add(prodBaskList[0].id);
    
    Map<string,string> mapProductBasketIdPriceBookId = new Map<string,string>();
    mapProductBasketIdPriceBookId.put(prodBaskList[0].id, prodBaskList[0].id);

    AssignnPriceBookToProductBasketImpl assighnpbimpl = new AssignnPriceBookToProductBasketImpl();
    assighnpbimpl.AssignPriceBook(setProductBasketId);

   
   //ProductUtility.CreateOLIs(setProductBasketId);
   //ProductUtility.CreateOLIs(mapProductBasketIdPriceBookId, AttributesList,
   // map<string,map<string,Product>> mapProductFamilymapProduct, map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.MakePriceBookPBEntriesMap(mapProductBasketIdPriceBookId, AttributesList,
    //map<string,map<string,Product>> mapProductFamilymapProduct);
  // ProductUtility.CreatePriceBookEntries(map<string,map<string,PBEntry>> mapPriceBookIdmapPBEntry);
   //ProductUtility.CreateProducts2(AttributesList);
   //ProductUtility.DeleteHardOLIs(setProductBasketId);
   ProductUtility.GetOLILineItemDescription(AttributesList[0]);

 
}
Public Static testmethod void Test(){
    
     Exception ee = null;

           Try{
         Test.starttest();
    Integer UserId=0;
   P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
       
         P2A_TestFactoryCls.SetupTestData();

           //ProductUtilitymethod();
           //ProductUtilitymethod4();
           //ProductUtilitymethod2();
           ProductUtilitymethod3();
           system.assert(UserId!=null);
           Test.stoptest();
           
           } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='ProductUtilitytest:Test';         
            ErrorHandlerException.sendException(e); 
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }


}

Public Static testmethod void Test0(){
    
     Exception ee = null;
     Integer UserId=0;
           Try{
         Test.starttest();
   
   P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
       
         P2A_TestFactoryCls.SetupTestData();
          system.assert(UserId!=null);
           //ProductUtilitymethod();
           //ProductUtilitymethod4();
           ProductUtilitymethod2();
           //ProductUtilitymethod3();
           
           Test.stoptest();
           
           } catch(Exception e){
            
           ee = e;
            ErrorHandlerException.ExecutingClassName='ProductUtilitytest:Test0';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }


}

Public Static testmethod void Test1(){
    integer UserId=0;
     Exception ee = null;

           Try{
         Test.starttest();
   
   P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
       
         P2A_TestFactoryCls.SetupTestData();
          system.assert(UserId!=null);
           //ProductUtilitymethod();
           ProductUtilitymethod4();
           //ProductUtilitymethod2();
           //ProductUtilitymethod3();
           
           Test.stoptest();
           
           } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='ProductUtilitytest:Test1';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }


}

Public Static testmethod void Test2(){
     integer UserId=0;
     Exception ee = null;

           Try{
         Test.starttest();
   
   P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
       
         P2A_TestFactoryCls.SetupTestData();
           system.assert(UserId!=null);
           ProductUtilitymethod();
           //ProductUtilitymethod4();
           //ProductUtilitymethod2();
           //ProductUtilitymethod3();
           
           Test.stoptest();
           
           } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='ProductUtilitytest:Test2';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }


}

}