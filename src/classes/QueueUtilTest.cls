@isTest
private class QueueUtilTest {
    @isTest static void getQueuesListForObjectTest() {
        Test.startTest();
        List<QueueSobject> queues = QueueUtil.getQueuesListForObject(Case.SObjectType);
        Test.stopTest();

        System.assertNotEquals(0, queues.size());
    }   

    @isTest static void getQueuesMapForObjectTest() {
        Test.startTest();
        Map<String, QueueSobject> queues = QueueUtil.getQueuesMapForObject(Case.SObjectType);
        Test.stopTest();

        System.assertNotEquals(0, queues.size());
    }

    @isTest static void getQueueByNameTest() {
        Test.startTest();
        QueueSobject queue = QueueUtil.getQueueByName(Case.SObjectType, 'Bid Management Queue');
        Test.stopTest();

        System.assertNotEquals(null, queue);
    }

    @isTest static void getQueueIdByNameTest() {
        Test.startTest();
        Id queueId = QueueUtil.getQueueIdByName(Case.SObjectType, 'Bid Management Queue');
        Test.stopTest();

        System.assertNotEquals(null, queueId);
    }
     @isTest static void getQueueUsersByNameTest() {
        Test.startTest();
        QueueUtil  utilObj = new QueueUtil();
        List<GroupMember> queueId = utilObj.getQueueUsersByName(Case.SObjectType, 'Bid Management Queue');
        Test.stopTest();

        System.assertNotEquals(null, queueId);
    }
}