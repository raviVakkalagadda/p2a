public class QueueableChainedJobStack {
    
    private static Boolean enqueued = false;
    
    public static List<QueueableChainedJob> qChain;
    
    public static void add(IWorker worker, Object params) {
        if (qChain == null) {
            qChain = new List<QueueableChainedJob>();
            qChain.add(new QueueableChainedJob(worker, params));
        } else {
            Integer size= qChain.size();
            qChain.add(new QueueableChainedJob(worker, params));
            if (Test.isRunningTest() == false) {
                qChain[size - 1].setNext(qChain[size]);
            }
        }
    }
    
    public static void enqueue() {
        if (qChain.size() > 0 && enqueued == false)
            System.enqueueJob(qChain[0]);
            enqueued = true;
    }
}



/* public class WorkerParamsPair implements Comparable {
    
    private IWorker m_worker;
    private Object m_params;
    private Integer m_index;
    
    WorkerParamsPair (IWorker worker, Object params, Integer index) {
        this.m_worker = worker;
        this.m_params = params;
        this.m_index = index;
    }
    
    public Integer compareTo(Object compareTo) {
        WorkerParamsPair other = (WorkerParamsPair ) compareTo;
        if (this.m_index == other.m_index) return 0;
        if (this.m_index > other.m_index) return 1;
        return -1;  
    }
} */

/* public static QueueableChainedJob chainIt() {
    workersQueue.sort();
    QueueableChainedJob next, current;
    for (Integer i = workersQueue.size() - 1; i > -1; i--) {
        if (i == workersQueue.size() - 1) {
            current = new QueueableChainedJob(workersQueue[i].m_worker, workersQueue[i].m_params, null);    
        } else {
            current = new QueueableChainedJob(workersQueue[i].m_worker, workersQueue[i].m_params, next);
        } 
        next = current; 
    }
    return current;
} */