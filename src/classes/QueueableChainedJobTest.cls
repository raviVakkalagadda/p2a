@isTest(SeeAllData=false)
public class QueueableChainedJobTest {
    
    static Integer jobCounter = 0;
    
    private static testMethod void testJobExecution() {    
        IWorker worker = new TestWorker();
        QueueableChainedJob job = new QueueableChainedJob(worker, null);
        job.setNext(null);
        system.assertEquals(true,worker!=null);

        Test.startTest();
        system.enqueueJob(job);
        Test.stopTest();
    }
    
    private class TestWorker implements IWorker {
        public void work(Object params, Object context) {
            QueueableChainedJobTest.jobCounter++;
        }
    }
}