//Generated by wsdl2apex

public class ROCTiWebservicesVo{
/*
    public class Acknowledgement {
        public Boolean Ack_xc;
        public String Error_Code;
        public String Error_Desc;
        private String[] Ack_xc_type_info = new String[]{'Ack__c','com.ti.webservices.vo',null,'1','1','false'};
        private String[] Error_Code_type_info = new String[]{'Error_Code','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Error_Desc_type_info = new String[]{'Error_Desc','com.ti.webservices.vo',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'com.ti.webservices.vo','true','false'};
        private String[] field_order_type_info = new String[]{'Ack_xc','Error_Code','Error_Desc'};
    }
    public class OrderLineItem {
        public String ProductID;
        public String BillProfileIntegrationNumber;
        public String RootProductID;
        public String Currency_x;
        public String Country;
        public Date OrderDate;
        public Date BillingCommencementDate;
        public Date StopBillingDate;
        public String OrderType;
        public String PrimaryServiceID;
        public String PinServiceID;
        public String ParentCustomerPO;
        public String ProductCode;
        public String ChargeFrequency;
        public String CostCentreintegrationnumber;
        public String ParentID;
        public String SiteAForROC;
        public String SiteBForROC;
        public String CommittedBandwidth;
        public Double ContractTerm;
        public String AccountManager;
        public Double NetNRCPrice;
        public Double NRCCost;
        public Double NetMRCPrice;
        public Double MRCCost;
        public String MRCBillText;
        public String NRCBillText;
        public String ServiceBillText;
        public String RootProductBillText;
        public String BundleLabel;
        public Double EarlyTerminationcharge;
        public String ETCBillText;
        public String Product;
        public String InAdvanceorInarrears;
        public Boolean AlignwithBillingcycle;
        public Boolean BillActivationFlag;
        public Boolean GenerateCreditFlag;
        public String ResourceID;
        public String OrderLineItemName;
        public String LastModifiedUserEmpNo;
        public String AEndZipPlus4;
        public String BEndZipPlus4;
        public String AEndCountryISOCode;
        public String BEndCountryISOCode;
        public Boolean BundleFlag;
        public Boolean BillableFlag;
        public Boolean UsageBaseFlag;
        public Boolean Parentofthebundleflag;
        public String Billing_Entity_xc;
        public String ChargeID;
        public String ParentBundleID;
        public String BundleAction;
        public String pivotdate;
        public Boolean ZeroChargeMRCFlag;
        public Boolean ZeroChargeNRCFlag;
        public String U2CMasterCode;
        public String ServiceType;
        public String PortType;
        public String DoyouwanttoassociateaUIA;
        public Boolean DisplaylineitemonInvoice;
        public String NRCChargeID;
        public Boolean ZeroChargeETCFlag;
        public Double RecurringCreditAmount;
        public Double OneOffCreditAmount;
        public String RecurringCreditBillText;
        public String OneOffCreditBillText;
        public String CreditDuration;
        public String CreditApprovedBy;
        public Boolean IsMiscellaneousCreditFlag;
        public String TypeofMiscellaneousCredit;
        public Date InstallmentNRCStartDate;
        public Date InstallmentNRCEndDate;
        public Double InstallmentNRCAmountpermonth;
        public String InstallmentNRCBillText;
        public String InstallmentNRCChargeID;
        public Double TotalInstallmentNRC;
        private String[] ProductID_type_info = new String[]{'ProductID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BillProfileIntegrationNumber_type_info = new String[]{'BillProfileIntegrationNumber','com.ti.webservices.vo',null,'0','1','false'};
        private String[] RootProductID_type_info = new String[]{'RootProductID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Currency_x_type_info = new String[]{'Currency','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Country_type_info = new String[]{'Country','com.ti.webservices.vo',null,'0','1','false'};
        private String[] OrderDate_type_info = new String[]{'OrderDate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BillingCommencementDate_type_info = new String[]{'BillingCommencementDate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] StopBillingDate_type_info = new String[]{'StopBillingDate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] OrderType_type_info = new String[]{'OrderType','com.ti.webservices.vo',null,'0','1','false'};
        private String[] PrimaryServiceID_type_info = new String[]{'PrimaryServiceID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] PinServiceID_type_info = new String[]{'PinServiceID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ParentCustomerPO_type_info = new String[]{'ParentCustomerPO','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ProductCode_type_info = new String[]{'ProductCode','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ChargeFrequency_type_info = new String[]{'ChargeFrequency','com.ti.webservices.vo',null,'0','1','false'};
        private String[] CostCentreintegrationnumber_type_info = new String[]{'CostCentreintegrationnumber','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ParentID_type_info = new String[]{'ParentID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] SiteAForROC_type_info = new String[]{'SiteAForROC','com.ti.webservices.vo',null,'0','1','false'};
        private String[] SiteBForROC_type_info = new String[]{'SiteBForROC','com.ti.webservices.vo',null,'0','1','false'};
        private String[] CommittedBandwidth_type_info = new String[]{'CommittedBandwidth','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ContractTerm_type_info = new String[]{'ContractTerm','com.ti.webservices.vo',null,'0','1','false'};
        private String[] AccountManager_type_info = new String[]{'AccountManager','com.ti.webservices.vo',null,'0','1','false'};
        private String[] NetNRCPrice_type_info = new String[]{'NetNRCPrice','com.ti.webservices.vo',null,'0','1','false'};
        private String[] NRCCost_type_info = new String[]{'NRCCost','com.ti.webservices.vo',null,'0','1','false'};
        private String[] NetMRCPrice_type_info = new String[]{'NetMRCPrice','com.ti.webservices.vo',null,'0','1','false'};
        private String[] MRCCost_type_info = new String[]{'MRCCost','com.ti.webservices.vo',null,'0','1','false'};
        private String[] MRCBillText_type_info = new String[]{'MRCBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] NRCBillText_type_info = new String[]{'NRCBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ServiceBillText_type_info = new String[]{'ServiceBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] RootProductBillText_type_info = new String[]{'RootProductBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BundleLabel_type_info = new String[]{'BundleLabel','com.ti.webservices.vo',null,'0','1','false'};
        private String[] EarlyTerminationcharge_type_info = new String[]{'EarlyTerminationcharge','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ETCBillText_type_info = new String[]{'ETCBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Product_type_info = new String[]{'Product','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InAdvanceorInarrears_type_info = new String[]{'InAdvanceorInarrears','com.ti.webservices.vo',null,'0','1','false'};
        private String[] AlignwithBillingcycle_type_info = new String[]{'AlignwithBillingcycle','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BillActivationFlag_type_info = new String[]{'BillActivationFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] GenerateCreditFlag_type_info = new String[]{'GenerateCreditFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ResourceID_type_info = new String[]{'ResourceID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] OrderLineItemName_type_info = new String[]{'OrderLineItemName','com.ti.webservices.vo',null,'0','1','false'};
        private String[] LastModifiedUserEmpNo_type_info = new String[]{'LastModifiedUserEmpNo','com.ti.webservices.vo',null,'0','1','false'};
        private String[] AEndZipPlus4_type_info = new String[]{'AEndZipPlus4','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BEndZipPlus4_type_info = new String[]{'BEndZipPlus4','com.ti.webservices.vo',null,'0','1','false'};
        private String[] AEndCountryISOCode_type_info = new String[]{'AEndCountryISOCode','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BEndCountryISOCode_type_info = new String[]{'BEndCountryISOCode','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BundleFlag_type_info = new String[]{'BundleFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BillableFlag_type_info = new String[]{'BillableFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] UsageBaseFlag_type_info = new String[]{'UsageBaseFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Parentofthebundleflag_type_info = new String[]{'Parentofthebundleflag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] Billing_Entity_xc_type_info = new String[]{'Billing_Entity__c','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ChargeID_type_info = new String[]{'ChargeID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ParentBundleID_type_info = new String[]{'ParentBundleID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] BundleAction_type_info = new String[]{'BundleAction','com.ti.webservices.vo',null,'0','1','false'};
        private String[] pivotdate_type_info = new String[]{'pivotdate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ZeroChargeMRCFlag_type_info = new String[]{'ZeroChargeMRCFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ZeroChargeNRCFlag_type_info = new String[]{'ZeroChargeNRCFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] U2CMasterCode_type_info = new String[]{'U2CMasterCode','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ServiceType_type_info = new String[]{'ServiceType','com.ti.webservices.vo',null,'0','1','false'};
        private String[] PortType_type_info = new String[]{'PortType','com.ti.webservices.vo',null,'0','1','false'};
        private String[] DoyouwanttoassociateaUIA_type_info = new String[]{'DoyouwanttoassociateaUIA','com.ti.webservices.vo',null,'0','1','false'};
        private String[] DisplaylineitemonInvoice_type_info = new String[]{'DisplaylineitemonInvoice','com.ti.webservices.vo',null,'0','1','false'};
        private String[] NRCChargeID_type_info = new String[]{'NRCChargeID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] ZeroChargeETCFlag_type_info = new String[]{'ZeroChargeETCFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] RecurringCreditAmount_type_info = new String[]{'RecurringCreditAmount','com.ti.webservices.vo',null,'0','1','false'};
        private String[] OneOffCreditAmount_type_info = new String[]{'OneOffCreditAmount','com.ti.webservices.vo',null,'0','1','false'};
        private String[] RecurringCreditBillText_type_info = new String[]{'RecurringCreditBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] OneOffCreditBillText_type_info = new String[]{'OneOffCreditBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] CreditDuration_type_info = new String[]{'CreditDuration','com.ti.webservices.vo',null,'0','1','false'};
        private String[] CreditApprovedBy_type_info = new String[]{'CreditApprovedBy','com.ti.webservices.vo',null,'0','1','false'};
        private String[] IsMiscellaneousCreditFlag_type_info = new String[]{'IsMiscellaneousCreditFlag','com.ti.webservices.vo',null,'0','1','false'};
        private String[] TypeofMiscellaneousCredit_type_info = new String[]{'TypeofMiscellaneousCredit','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InstallmentNRCStartDate_type_info = new String[]{'InstallmentNRCStartDate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InstallmentNRCEndDate_type_info = new String[]{'InstallmentNRCEndDate','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InstallmentNRCAmountpermonth_type_info = new String[]{'InstallmentNRCAmountpermonth','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InstallmentNRCBillText_type_info = new String[]{'InstallmentNRCBillText','com.ti.webservices.vo',null,'0','1','false'};
        private String[] InstallmentNRCChargeID_type_info = new String[]{'InstallmentNRCChargeID','com.ti.webservices.vo',null,'0','1','false'};
        private String[] TotalInstallmentNRC_type_info = new String[]{'TotalInstallmentNRC','com.ti.webservices.vo',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'com.ti.webservices.vo','true','false'};
        private String[] field_order_type_info = new String[]{'ProductID','BillProfileIntegrationNumber','RootProductID','Currency_x','Country','OrderDate','BillingCommencementDate','StopBillingDate','OrderType','PrimaryServiceID','PinServiceID','ParentCustomerPO','ProductCode','ChargeFrequency','CostCentreintegrationnumber','ParentID','SiteAForROC','SiteBForROC','CommittedBandwidth','ContractTerm','AccountManager','NetNRCPrice','NRCCost','NetMRCPrice','MRCCost','MRCBillText','NRCBillText','ServiceBillText','RootProductBillText','BundleLabel','EarlyTerminationcharge','ETCBillText','Product','InAdvanceorInarrears','AlignwithBillingcycle','BillActivationFlag','GenerateCreditFlag','ResourceID','OrderLineItemName','LastModifiedUserEmpNo','AEndZipPlus4','BEndZipPlus4','AEndCountryISOCode','BEndCountryISOCode','BundleFlag','BillableFlag','UsageBaseFlag','Parentofthebundleflag','Billing_Entity_xc','ChargeID','ParentBundleID','BundleAction','pivotdate','ZeroChargeMRCFlag','ZeroChargeNRCFlag','U2CMasterCode','ServiceType','PortType','DoyouwanttoassociateaUIA','DisplaylineitemonInvoice','NRCChargeID','ZeroChargeETCFlag','RecurringCreditAmount','OneOffCreditAmount','RecurringCreditBillText','OneOffCreditBillText','CreditDuration','CreditApprovedBy','IsMiscellaneousCreditFlag','TypeofMiscellaneousCredit','InstallmentNRCStartDate','InstallmentNRCEndDate','InstallmentNRCAmountpermonth','InstallmentNRCBillText','InstallmentNRCChargeID','TotalInstallmentNRC'};
    }
    public class CreateUpdateBilling_Request {
        public ROCTiWebservicesVo.OrderLineItem[] OrderLineItem;
        private String[] OrderLineItem_type_info = new String[]{'OrderLineItem','com.ti.webservices.vo',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'com.ti.webservices.vo','true','false'};
        private String[] field_order_type_info = new String[]{'OrderLineItem'};
    }*/
}