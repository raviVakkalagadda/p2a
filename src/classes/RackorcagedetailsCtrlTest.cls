@isTest
private class RackorcagedetailsCtrlTest{
    
    private static List<cscfga__Product_Configuration__c> pclist;
        
        static testMethod void UnitTest() {
            list<RR_Cage_or_Rack__c> listrcrcage = new list<RR_Cage_or_Rack__c>();        
            P2A_TestFactoryCls.sampleTestData();

            case c = new case();
            c.Is_Feasibility_Request__c = true;
            //c.Product_Configuration__c = pclist[0].id;
            c.status = 'Approved';
            c.Number_of_Child_Case__c = 2;
            c.Number_of_Child_Case_Closed__c = 1;
            c.Reason_for_selecting_one_supplier__c = 'SingleSource';
            c.Is_Resource_Reservation__c = true;
            insert c;
            System.assertEquals(c.Reason_for_selecting_one_supplier__c, 'SingleSource');

            RR_Cage_or_Rack__c rcr = new RR_Cage_or_Rack__c();
            //rcr.Product_Configuration__c = pclist[0].id;
            rcr.Cage_or_Room_ID__c = 'Cageroom';
            rcr.Type__c = 'New';
            rcr.Width_per_rack_mm__c  = 'Rackmm';
            rcr.Rack_IDs_allocated__c = 'Allocated';
            rcr.Power_per_rack_KVA__c = 'Powerrack';
            rcr.No_of_ceeforms_commando_sockets_per_rack__c = 'ceeforms';
            insert rcr;
            System.assertEquals(rcr.Cage_or_Room_ID__c, 'Cageroom');

            RR_Cage_or_Rack__c upadtercr = [select id,Product_Configuration__c,Type__c from RR_Cage_or_Rack__c where id=: rcr.id];
            listrcrcage.add(upadtercr);
            update listrcrcage;

            PageReference pageRef = Page.cagerackprivateroomdetails; // Adding VF page Name here
            pageRef.getParameters().put('id', String.valueOf(rcr.id));//for page reference
            Test.setCurrentPage(pageRef);

            ApexPages.currentPage().getParameters().put('id',c.Id);
            ApexPages.StandardController sc = new ApexPages.StandardController(c);
            rackorcagedetailsCtrl   rc = new rackorcagedetailsCtrl(sc);
            rc.updateItems();    
            boolean b = true;
            String id = ApexPages.currentPage().getParameters().get('id');
            System.assert(b,id==null); 
        }   
    }