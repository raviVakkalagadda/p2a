public with sharing class RackorcagedetailsCtrl_FS {

    public case caseObj;
    private list<case> casequery;
    public Set<ID> prodid = new Set<ID>();
    public Boolean pageRefresh {get; Set;}
    public RR_Cage_or_Rack__c priceObj{get;set;}
    public List<RR_Cage_or_Rack__c> RRcrlist{get;set;}
    
    public RackorcagedetailsCtrl_FS(ApexPages.StandardController controller) {
        this.caseObj = (case)controller.getrecord();
        casequery = [select id, Product_Basket__c, Product_Configuration__c, Product_Configuration__r.cscfga__Root_Configuration__c from
        case where id=: caseobj.id];
        initlize();
    }
    
    public void initlize()
    {   
        for(Case cs : casequery)
        {
            prodid.add(cs.Product_Configuration__c); 
        }
        RRcrlist = [select id, Product_Configuration__c, A_end_interface_type__c, B_end_interface_type__c, Rack_IDs_allocated__c,
                            A_end_Rack_ID__c, B_end_Rack_ID__c, Media_type__c  
                                            from RR_Cage_or_Rack__c where Product_Configuration__c in: prodid and Is_Cross_Connect__c = true and Is_Feasibility_Request__c = true];
    }
    public PageReference updateItems()
    {
        List<RR_Cage_or_Rack__c> priceApplst = new List<RR_Cage_or_Rack__c>();
        for( RR_Cage_or_Rack__c obj : RRcrlist) 
        {
            priceApplst.add(obj); 
        }
        update priceApplst;
        pageRefresh = true;
        return null;
    }
}