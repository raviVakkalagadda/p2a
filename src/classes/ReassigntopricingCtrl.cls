public with sharing class ReassigntopricingCtrl {
    
    private Case caseObj;
    public String callfunc{get;set;}
    
    public ReassigntopricingCtrl(ApexPages.StandardController controller) {
        caseObj = (Case) controller.getrecord();
    }
    
    public PageReference reAssigntopriceMtd(){
        List<Case> appCase = [select id, Comments_Prior__c, assigned_to__r.email, Comments__c, assigned_to__r.Name, Is_Reassign_to_Pricing__c, Opportunity_Owner__r.email, Status, Is_Reassign_to_Sales__c, Assigned_To__c, Opportunity_Name__c, Ownerid from case where id=:caseobj.id];
        //to get owner user id: to fix 10396 
        Set<Id> userIds = new Set<Id>();
        if((appCase[0].Ownerid+'').startsWith('005')) {
            userIds.add(appCase[0].Ownerid);
        } else {
            
            //List<GroupMember> lstGM = [Select UserOrGroupId From GroupMember where GroupId =:appCase[0].Ownerid];
            for(GroupMember gm :[Select UserOrGroupId From GroupMember where GroupId =:appCase[0].Ownerid]) {
                if((gm.UserOrGroupId+'').startsWith('005')) {
                    userIds.add(gm.UserOrGroupId);             
                }
            }
        }
        
        //List<User> lstUsers = [Select id, Email from user where id = :userIds];
        
        List<Case> updateCase = new List<Case>();
        Id templateId = [select Id from EmailTemplate where developername = 'Reassign_to_PricingVF'].Id;
        for(Case caseid : appcase){
            if(caseid.Is_Reassign_to_Pricing__c){
                callfunc = '<script> func(); </script>';
                return null;       
            }
            //Commented for defect QC#15030
            /*
            else if(caseid.Comments__c == caseid.Comments_Prior__c || caseid.Comments__c == null){
                callfunc = '<script> comm(); </script>';
                return null;
            }
            */
            else{
                Case upcase = new Case();
                upcase.id                        = caseid.id;
                upcase.status                    = 'Assigned';
                upcase.Is_Reassign_to_Pricing__c = true;
                upcase.Is_Reassign_to_Sales__c   = false;
                upcase.Comments_Prior__c = caseid.Comments__c;
                upcase.recordtypeID              = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Escalate'); 
                updateCase.add(upcase);
                
                EmailUtil.EmailData mailData = new EmailUtil.EmailData();               
               // mailData.addToAddress(caseid.Opportunity_Owner__r.Email);
                for(User u : [Select id, Email from user where id = :userIds]) {
                    mailData.addCCAddress(u.Email);
                }
                //mailData.addToAddress(caseid.assigned_to__r.email);
                
                mailData.message.setWhatId(upcase.Id);
                mailData.message.setSaveAsActivity(false);
                //to fix 10396 
                system.debug('caseid.assigned_to__r.email : '+caseid.assigned_to__r.email);
                if(caseid.assigned_to__c != null){
                mailData.message.setTargetObjectId(caseid.assigned_to__c);
                }
                else{
                    callfunc='<script> assignedtoemail(); </script>';
                    return null;    
                }
                /*List<User> use = [select id, email from user where email =: caseid.assigned_to__r.email limit 1];// this user will be in To address.
                List<Id> lstuserids = new List<Id>();
                if(use.size() > 0){
                    for(user u : use){
                        mailData.message.setTargetObjectId(u.id);
                    }
                }
                else{
                    callfunc='<script> assignedtoemail(); </script>';
                    return null;    
                }*/
                
                mailData.message.setTemplateId(templateId);
                system.debug('mailData : '+mailData);
                EmailUtil.sendEmail(mailData);               
            }
        }
        
        Update UpdateCase;
        return new PageReference ('/'+caseObj.id);
    }
}