public with sharing class ReassigntosalesCtrl {
    
    private Case caseObj;
    public string callfunc{get;set;}
    
    public ReassigntosalesCtrl(ApexPages.StandardController controller) {
        caseObj = (Case) controller.getrecord();
    }
    
    public PageReference reAssigntosalesMtd(){
        List<Case> AppCase = [select id, Opportunity_Name__r.OwnerId, Comments_Prior__c, assigned_to__r.email, Comments__c, Owner.email, assigned_to__r.Name, Is_Reassign_to_Pricing__c, Opportunity_Owner__r.email, Status, Is_Reassign_to_Sales__c, Assigned_To__c, Opportunity_Name__c, Ownerid, CreatedBy.Id from case where id=:caseobj.id];
        //to get owner user id: to fix 10396 
        Set<Id> userIds = new Set<Id>();
        if((appCase[0].Ownerid+'').startsWith('005')) {
            userIds.add(appCase[0].Ownerid);
        } else {
            
            //List<GroupMember> lstGM = [Select UserOrGroupId From GroupMember where GroupId =:appCase[0].Ownerid];
            for(GroupMember gm :[Select UserOrGroupId From GroupMember where GroupId =:appCase[0].Ownerid]) {
                if((gm.UserOrGroupId+'').startsWith('005')) {
                    userIds.add(gm.UserOrGroupId);             
                }
            }
        }
        
        //List<User> lstUsers = [Select id, Email from user where id = :userIds];

        List<Case> UpdateCase = new List<Case>();
		Id templateId = [select Id from EmailTemplate where developername = 'Reassign_to_salesVF'].id;
        for(Case caseid : appcase){
            if(caseid.Is_Reassign_to_Sales__c){
                callfunc='<script> func(); </script>';
                return null;       
            }
            //Commented for defect QC#15030
            /*
            else if(caseid.Comments__c == caseid.Comments_Prior__c || caseid.Comments__c == null){
                callfunc='<script> comm(); </script>';
                return null;
            }
            */
            else{   
                case upcase = new Case();
                upcase.Id = caseid.Id;
                upcase.Status = 'On Hold';
                upcase.Is_Reassign_to_Pricing__c = false;
                upcase.Is_Reassign_to_Sales__c   = true;
                upcase.recordtypeID              = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Pricing on Hold');
                upcase.Comments_Prior__c = caseid.Comments__c;
                upcase.OwnerId = caseid.CreatedBy.Id;
                updateCase.add(upcase);
                
                EmailUtil.EmailData mailData = new EmailUtil.EmailData();               
                
                if(caseid.Opportunity_Owner__r.email == null) {
                    callfunc='<script> Salesmanageremail(); </script>';
                    return null; 
                }
                //to fix 10396 
                mailData.message.setTargetObjectId(caseid.Opportunity_Owner__c);
                
                /*List<user> use = [select id, email from user where email =: caseid.Opportunity_Owner__r.email limit 1];// this user will be in To address.
                List<Id> lstuserids= new List<Id>();
                if(use.size() > 0){
                    for(user u: use){
                        mailData.message.setTargetObjectId(u.id);
                    }
                }
                else{
                    callfunc='<script> Salesmanageremail(); </script>';
                    return null;    
                }*/
                mailData.message.setWhatId(upcase.id);
                
                mailData.message.setTemplateId(templateId);
                mailData.message.saveAsActivity = false;
                
                for(User u : [Select id, Email from user where id = :userIds]) {
                    mailData.addCCAddress(u.Email);
                }            
                
                EmailUtil.sendEmail(mailData);
            }
        }        
        Update UpdateCase;
        return new PageReference ('/'+caseObj.id);
        //return null;
    }
}