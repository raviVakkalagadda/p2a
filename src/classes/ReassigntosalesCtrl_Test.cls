@istest
Public class ReassigntosalesCtrl_Test{
public static testmethod void reassigntoSalesTest(){
        List<Account> Acclist =  P2A_TestFactoryCls.getaccounts(1);
        System.assert(Acclist[0].id!=null);
        
        list<Opportunity> Opp = P2A_TestFactoryCls.getOpportunitys(1,Acclist);
        System.assert(Opp[0].id!=null);
        
        List<case> Caselist= P2A_TestFactoryCls.getcase(1, AccList);
                   System.assert(Caselist[0].id!=null);

           
        profile pr = [select id, Name from profile where name = 'System Administrator' limit 1];
         System.assertequals(Pr.Name,'System Administrator');
        
        List<User> userList = new List<User>();
        User ur = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = pr.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12356@testorg.com',EmployeeNumber='123');
        userList.add(ur);
        insert userList;
        System.assert(Userlist[0].id!=null);
        
        case Cs= new case();     
        Cs.Comments_Prior__c = 'high';
        Cs.Comments__c = 'Margin is high'; 
        Cs.Is_Reassign_to_Pricing__c = false;
        Cs.Status ='High';
        Cs.Is_Reassign_to_Sales__c = true; 
        Cs.Assigned_To__c = ur.id;
        Cs.Opportunity_Name__c = Opp[0].id;
        Cs.Opportunity_Owner__c = Userlist[0].id;
        
        insert Cs;
        System.assert(Cs.id!=null);

        Group csgroupmember = new Group();
        csgroupmember.name = 'Test';
        csgroupmember.DeveloperName = cs.OwnerId;
        insert csgroupmember;
        
        reassigntosalesCtrl resales = new reassigntosalesCtrl(new ApexPages.StandardController(cs));
        resales.reAssigntosalesMtd();
        
        
        list<Case> Updatecases = new List<Case>(); 
        Case upcase = new Case();
        upcase.id = cs.id;
        upcase.Comments__c = 'Margin is high'; 
        upcase.Is_Reassign_to_Pricing__c = false;
        upcase.Is_Reassign_to_Sales__c   = true;
        upcase.status='On Hold';
        upcase.recordtypeID              = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, 'Pricing on Hold');
        upcase.Comments_Prior__c = 'high';
        upcase.Assigned_To__c = ur.id;
        updatecases.add(upcase);
        update updatecases;
        System.assert(Updatecases[0].id!=null);

     
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Cs);
        reassigntosalesCtrl resales1 = new reassigntosalesCtrl(sc);
        resales1.reAssigntosalesMtd();
        Test.stopTest();
        //system.assert(updatecases[0].status=='On Hold');
    }
    
    public static testmethod void reassigntoSalesTest1(){
    
        List<Account> Acclist =  P2A_TestFactoryCls.getaccounts(1);
        System.assert(Acclist[0].id!=null);
        
        list<Opportunity> Opp = P2A_TestFactoryCls.getOpportunitys(1,Acclist);
        System.assert(Opp[0].id!=null);
        
        
        
        List<case> Caselist= P2A_TestFactoryCls.getcase(1, AccList);
                   System.assert(Caselist[0].id!=null);

           
        profile pr = [select id, Name from profile where name = 'System Administrator' limit 1];
         System.assertequals(Pr.Name,'System Administrator');
        
        List<User> userList = new List<User>();
        User ur = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                           EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                           LocaleSidKey='en_US', ProfileId = pr.Id, 
                           TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12356@testorg.com',EmployeeNumber='123');
        userList.add(ur);
        insert userList;
        System.assert(Userlist[0].id!=null);
        
        
        case Cs= new case();     
        Cs.Comments_Prior__c = 'high';
        Cs.Comments__c = 'Margin is high'; 
        Cs.Is_Reassign_to_Pricing__c = false;
        Cs.Status ='High';
        Cs.Is_Reassign_to_Sales__c = false; 
        Cs.Assigned_To__c = ur.id;
        Cs.Opportunity_Name__c = Opp[0].id;
        Cs.Opportunity_Owner__c = Userlist[0].id;
        
        //Cs.ownerid=group1.id;
        insert Cs;
        
   
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(Cs);
        reassigntosalesCtrl resales11 = new reassigntosalesCtrl(sc);
        resales11.reAssigntosalesMtd();
        Test.stopTest();
    }
   /* static testMethod void send(){ 
       P2A_TestFactoryCls.disableAll(UserInfo.getUserId());     
       P2A_TestFactoryCls.sampleTestData();
       
       List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        oppList[0].Name = 'Opp1';
        oppList[0].CloseDate = system.today()+10;
        oppList[0].stagename = 'Identify & Define';
        oppList[0].Probability = 30;
        oppList[0].csordtelcoa__Change_Type__c = 'Upgrade';
        oppList[0].Product_Type__c = 'CPE';
        oppList[0].Quote_Simplification__c = false;
        oppList[0].Win_Loss_Reasons__c = 'Cancelled';
        oppList[0].Sales_Status__c = 'Lost';
        oppList[0].UIFlag__c = 256;
        oppList[0].Stage__c = 'InFlight';
        oppList[0].Internet_Product__c = false;
        upsert oppList[0];
        integer i= [Select count() from Opportunity];
        system.assertNotEquals(i,0);
        system.assert(i>0);
        
        list<cscfga__Product_Basket__c> pblist  = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Pdlist1 = P2A_TestFactoryCls.getProductdef(1);
        Pdlist1[0].name = 'TWI Singlehome';
        upsert  Pdlist1[0];
        system.assertEquals(Pdlist1[0].name , 'TWI Singlehome');
        
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1,pblist,Pdlist1,pbundlelist,Offerlists);
        proconfig[0].name ='TWI Singlehome';
        upsert proconfig[0];
        system.assertEquals(proconfig[0].name , 'TWI Singlehome');
        
        List<Pricing_Approval_Request_Data__c> papprovalist = P2A_TestFactoryCls.getProductonfigappreq(1,pblist,proconfig);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<csord__service__c> services = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
        
        Test.startTest();
        List<case> caselist = P2A_TestFactoryCls.getcaseTrgHandler(1,AccList,opplist,pblist,orders,users,services);
        caselist[0].Comments__c = 'Margin is high'; 
        caselist[0].Is_Reassign_to_Pricing__c = false;
        caselist[0].Is_Reassign_to_Sales__c   = false;
        caselist[0].status='Assigned';
        caselist[0].Assigned_To__c = users[0].id;
        caselist[0].Comments_Prior__c = 'high'; 
        caselist[0].Opportunity_Name__c = oppList[0].id;
        caselist[0].Opportunity_Owner__c= users[0].id;
        update caselist[0];
        system.assertEquals(caselist[0].status,'Assigned');
        
        
        Case upcase = new Case();
        upcase.Comments__c = 'Margin is high'; 
        upcase.Is_Reassign_to_Pricing__c = false;
        upcase.Is_Reassign_to_Sales__c   = false;
        upcase.status='Assigned';
        upcase.Assigned_To__c = users[0].id;
        upcase.Comments_Prior__c = 'high'; 
        upcase.Opportunity_Name__c = oppList[0].id;
        upcase.Opportunity_Owner__c= users[0].id;
        insert upcase;
        system.assertEquals(upcase.Comments_Prior__c ,'high');
        
        Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Case; 
        Map<String,Schema.RecordTypeInfo> caseTypeInfo = cfrSchema.getRecordTypeInfosByName(); 
        Id rtId = caseTypeInfo.get('Pricing on Hold').getRecordTypeId();
        system.debug('@@@'+rtId);
         
        case upcase1 = new Case();
        upcase1.Id = upcase.Id;
        upcase1.Status = 'On Hold';
        upcase1.Is_Reassign_to_Pricing__c = false;
        upcase1.Is_Reassign_to_Sales__c   = true;
        upcase1.recordtypeid  = caseTypeInfo.get('Pricing on Hold').getRecordTypeId();
        upcase1.Comments_Prior__c = upcase.Comments__c;
        update upcase1;
        system.assertEquals(upcase1.Status , 'On Hold');
        
        ApexPages.StandardController controller = new ApexPages.StandardController(caselist[0]);
        reassigntosalesCtrl reascntrl = new reassigntosalesCtrl(controller);
        reascntrl.reAssigntosalesMtd();
        Test.stopTest();    
    }*/
    
}