@isTest
private class RecordTypeUtilTest {
	
	@isTest static void getRecordTypeIdByNameTest() {
		RecordType caseRecordType = [select Id, Name, SobjectType from RecordType where SobjectType = 'Case' and Name = 'Default Close Case'];

		Test.startTest();
		Id returnedRecTypeId = RecordTypeUtil.getRecordTypeIdByName(Case.SObjectType, caseRecordType.Name);
		Test.stopTest();

		System.assertEquals(caseRecordType.Id, returnedRecTypeId);
	}
	
	@isTest static void getRecordTypeNameByIdTest() {
		RecordType caseRecordType = [select Id, Name, SobjectType from RecordType where SobjectType = 'Case' and Name = 'Default Close Case'];
		
		Test.startTest();
		String returnedRecTypeName = RecordTypeUtil.getRecordTypeNameById(Case.SObjectType, caseRecordType.Id);
		Test.stopTest();

		System.assertEquals(caseRecordType.Name, returnedRecTypeName);
	}

	@isTest static void getRecordTypeByNameTest() {
		RecordType caseRecordType = [select Id, Name, SobjectType from RecordType where SobjectType = 'Case' and Name = 'Default Close Case'];
		
		Test.startTest();
		Schema.RecordTypeInfo returnedRecType = RecordTypeUtil.getRecordTypeByName(Case.SObjectType, caseRecordType.Name);
		Test.stopTest();

		System.assertEquals(caseRecordType.Name, returnedRecType.getName());
	}

	@isTest static void getRecordTypeByIdTest() {
		RecordType caseRecordType = [select Id, Name, SobjectType from RecordType where SobjectType = 'Case' and Name = 'Default Close Case'];
		
		Test.startTest();
		Schema.RecordTypeInfo returnedRecType = RecordTypeUtil.getRecordTypeById(Case.SObjectType, caseRecordType.Id);
		Test.stopTest();

		System.assertEquals(caseRecordType.Id, returnedRecType.getRecordTypeId());
	}
	
}