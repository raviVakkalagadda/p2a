global with sharing class ReferenceDataRemoteUtil {
	global ReferenceDataRemoteUtil(ApexPages.StandardController stdController) {
		//Just ignore it - no extension finctionality associated with this
	}
	
	@RemoteAction
	global static GIAAS_Plan_Description__c getGiaasDescription(String planName) {
		return GIAAS_Utils.getDescription(planName); 
	}
	
	@RemoteAction
	global static GIAAS_Rate_Card__c getGiaasStoragePrice(Map<String , String> pricingParameters) {
		return GIAAS_Utils.getPrice(pricingParameters); 
	}
    
}