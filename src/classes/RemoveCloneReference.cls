public class RemoveCloneReference{

    /**
     * Description: The below method will remove the reference of replaced subscription,replaced service and replaced PC
     * For a new configuration inside a change order
     * Solution was proposed by Henrick and Manish
     * @author: Ritesh
     * Date:17-dec-2016
     * As part of winter 17(CR26) logic of this method has been refactored. //Sapan
     */
    public static void setCloneReferenceForChangeOrder(List<cscfga__Product_Configuration__c> newProductConfigs){
        /** First step:get all the PCid's in the basket **/
        /** Logic to get all the PC in the basket **/

        /** Declaring set of basketID **/
        Set<Id> basketID = new Set<Id>();
        Map<Id, Id> replacedPCs = new Map<Id, Id>();

        for(cscfga__Product_Configuration__c PC :newProductConfigs){

        if(PC.cscfga__Product_Basket__c != null
           && (!basketID.contains(PC.cscfga__Product_Basket__c)
           && (PC.Cloned_PC__c == null)
           && (PC.csordtelcoa__Replaced_Subscription__c != null 
           || PC.csordtelcoa__Replaced_Service__c != null
           || PC.csordtelcoa__Replaced_Product_Configuration__c != null
           || PC.Inflight_Primary_Service_Id__c != null))){
               
               basketID.add(PC.cscfga__Product_Basket__c);
          } 
            
        }

        if(!basketID.IsEmpty()){
            Map<Id, cscfga__Product_Configuration__c> prodConfigMap = new Map<Id, cscfga__Product_Configuration__c>([Select Id, Cloned_PC__c,
                csordtelcoa__Replaced_Subscription__c, csordtelcoa__Replaced_Product_Configuration__c, csordtelcoa__Replaced_Service__c 
                    From cscfga__Product_Configuration__c 
                        Where cscfga__Product_Basket__c IN :basketID]);

            for(cscfga__Product_Configuration__c PC :prodConfigMap.Values()){
                    
                if(PC.csordtelcoa__Replaced_Product_Configuration__c != null && !replacedPCs.containsKey(PC.csordtelcoa__Replaced_Product_Configuration__c)){
                    replacedPCs.put(PC.csordtelcoa__Replaced_Product_Configuration__c, PC.Id);
                }
                if(PC.csordtelcoa__Replaced_Subscription__c != null && !replacedPCs.containsKey(PC.csordtelcoa__Replaced_Subscription__c)){
                    replacedPCs.put(PC.csordtelcoa__Replaced_Subscription__c, PC.Id);
                }
                if(PC.csordtelcoa__Replaced_Service__c != null && !replacedPCs.containsKey(PC.csordtelcoa__Replaced_Service__c)){
                    replacedPCs.put(PC.csordtelcoa__Replaced_Service__c, PC.Id);
                }       
            }
            
            for(cscfga__Product_Configuration__c PC :newProductConfigs){
                
                if(PC.Id == null){

                    if(PC.csordtelcoa__Replaced_Product_Configuration__c != null && replacedPCs.containsKey(PC.csordtelcoa__Replaced_Product_Configuration__c)){
                        PC.Cloned_PC__c = replacedPCs.get(PC.csordtelcoa__Replaced_Product_Configuration__c);
                    }
                    if(PC.csordtelcoa__Replaced_Subscription__c != null && replacedPCs.containsKey(PC.csordtelcoa__Replaced_Subscription__c)){
                        if(PC.Cloned_PC__c == null){PC.Cloned_PC__c = replacedPCs.get(PC.csordtelcoa__Replaced_Subscription__c);}
                    }
                    if(PC.csordtelcoa__Replaced_Service__c != null && replacedPCs.containsKey(PC.csordtelcoa__Replaced_Service__c)){
                        if(PC.Cloned_PC__c == null){PC.Cloned_PC__c = replacedPCs.get(PC.csordtelcoa__Replaced_Service__c);}
                    }       
                }
            }
        }
    }

    public static void removeCloneReferenceForChangeOrder(List<cscfga__Product_Configuration__c> newProductConfigs){

        for(cscfga__Product_Configuration__c PC :newProductConfigs){

            if(PC.Cloned_PC__c != null){
                PC.csordtelcoa__Replaced_Product_Configuration__c = null;
                PC.csordtelcoa__Replaced_Subscription__c = null;
                PC.csordtelcoa__Replaced_Service__c = null;
                PC.Inflight_Primary_Service_Id__c = null;
                PC.Cloning_Parent__c = PC.Cloned_PC__c;
            }
        }
    }
}