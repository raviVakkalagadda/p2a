@IsTest
public class ReplacementServiceUpdaterTest {
    Private static List<csord__Service__c> Listservicerelated;
  
      Public static testMethod void unittest() {   
         P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
         setupCustomSettings();
         setupCustomSettingsoffer();
         P2A_TestFactoryCls.sampleTestData();
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1 , accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<cscfga__Product_Definition__c>ListProductdef =  P2A_TestFactoryCls.getProductdef(1);
         List<cscfga__Product_Bundle__c> pbundlelist =  P2A_TestFactoryCls.getProductBundleHdlr(1,oppList);
         List<cscfga__Configuration_Offer__c> Listconfigoffer = P2A_TestFactoryCls.getOffers(1);
         List<cscfga__Product_Configuration__c> Listconfig = P2A_TestFactoryCls.getProductonfig(1,prodBaskList,ListProductdef ,pbundlelist,Listconfigoffer);
         
         
         set<Id>serviceIds = new set<Id>();
             for(csord__Service__c ser:serList)
              {
                serviceIds.add(ser.id);
             }
         
         Listservicerelated = new List<csord__Service__c>();
         csord__Service__c ser = new csord__Service__c();
                ser.Name = 'Test Service'; 
                ser.csord__Identification__c = 'Test-Catlyne-4238362';
                ser.csord__Order_Request__c = orderRequestList[0].id;
                ser.csord__Subscription__c = subList[0].id;
                ser.Billing_Commencement_Date__c = System.Today();
                ser.Stop_Billing_Date__c = System.Today();
                ser.RAG_Status_Red__c = false ; 
                ser.RAG_Reason_Code__c = '';                            
                ser.csordtelcoa__Product_Basket__c = prodBaskList[0].id;  
                ser.Product_Id__c = 'IPVPN';   
                ser.csordtelcoa__Product_Configuration__c = Listconfig[0].id;
                ser.Path_Instance_ID__c = 'test';                 
                Listservicerelated.add(ser);
          insert Listservicerelated;      
          List<csord__Service__c> serlist1 =[select id,name from csord__Service__c where name = 'Test Service'];
          system.assert(serlist1!=null);
          system.assertEquals(Listservicerelated[0].name, serlist1[0].name);

         cscfga__Product_Configuration__c inflightConfig = [SELECT Id, csordtelcoa__Replaced_Service__c
                                                           FROM cscfga__Product_Configuration__c
                                                           WHERE Id=:Listservicerelated[0].csordtelcoa__Product_Configuration__c
                                                           LIMIT 1];
        
        inflightConfig.csordtelcoa__Replaced_Service__c = serList.get(0).Id;
        update inflightConfig; 
        system.assert(inflightConfig!=null);
       
       Test.startTest();
         ReplacementServiceUpdater  repserv = new ReplacementServiceUpdater();
         ReplacementServiceUpdater.LinkUpdateResult repservlinkupres = new ReplacementServiceUpdater.LinkUpdateResult(serList,serList);
         repservlinkupres  = repserv.fixLinks(serviceIds);
         repservlinkupres  = repserv.fixLinks(serList);
         List<csord__Service__c> findreplserviIds = repserv.findReplacedServicesById(serviceIds);
         List<csord__Service__c> Listservice = repserv.findServicesWithMissingLinksById(serviceIds);
         List<csord__Service__c> modifiedreplacementserv = repservlinkupres.getModifiedReplacementsServices();
         List<csord__Service__c> modifiedreplacedserv = repservlinkupres.getModifiedReplacedServices();
         List<csord__Service__c> modifiedservices = repservlinkupres.getAllModifiedServices();
       Test.stopTest();
    }
   
   private static void setupCustomSettings() {
        insert new Product_Definition_Id__c(Name='Master_IPVPN_Service_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='Master_VPLS_Service_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='IPC_Definition_Id', Product_Id__c=null);        
        insert new Product_Definition_Id__c(Name='IPVPN_Port_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='VPLS_Transparent_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='VPLS_VLAN_Port_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='ASBR_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='VLANGroup_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='SMA_Gateway_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='Master_IPVPN_Service_Offer_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='Master_VPLS_Transparent_Offer_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='Master_VPLS_VLAN_Offer_Id', Product_Id__c=null);
        
       }
        
    private static void setupCustomSettingsoffer() {     
        insert new Offer_Id__c(Name='Master_IPVPN_Service_Offer_Id', Offer_Id__c =null);
        insert new Offer_Id__c(Name='Master_VPLS_Transparent_Offer_Id', Offer_Id__c =null);
        insert new Offer_Id__c(Name='Master_VPLS_VLAN_Offer_Id', Offer_Id__c =null);

    }
   
  
   /* @IsTest
    public static void testLinks() {
        setupCustomSettings();
        csord__Service__c original = createService('original');
        csord__Service__c macd     = createService('macd');
        csord__Service__c inflight = createService('inflight');

        original.csordtelcoa__Replaced_Service__c = null;
        original.csordtelcoa__Replacement_Service__c = macd.Id;
        update original;
        System.assert(original.Id != null, 'original was not updated');

        macd.csordtelcoa__Replaced_Service__c = original.Id;
        original.csordtelcoa__Replacement_Service__c = null;
        update macd;

        cscfga__Product_Configuration__c inflightConfig = [SELECT Id, csordtelcoa__Replaced_Service__c
                                                           FROM cscfga__Product_Configuration__c
                                                           WHERE Id=:inflight.csordtelcoa__Product_Configuration__c
                                                           LIMIT 1];
        
        inflightConfig.csordtelcoa__Replaced_Service__c = original.Id;
        update inflightConfig;

        inflight.csordtelcoa__Replaced_Service__c = null;        
        update inflight;
        
        // refresh and verify
        inflight = [SELECT Id, csordtelcoa__Replaced_Service__c FROM csord__Service__c WHERE Id=:inflight.id LIMIT 1];
        System.assertEquals(original.Id, inflight.csordtelcoa__Replaced_Service__c, 'Inflight service should point to original service');
        
        original = [SELECT Id, csordtelcoa__Replacement_Service__c FROM csord__Service__c WHERE Id=:original.id LIMIT 1];
        System.assertEquals(inflight.Id, original.csordtelcoa__Replacement_Service__c, 'Original service should point to inflight service');
        
        macd = [SELECT Id, csordtelcoa__Replacement_Service__c FROM csord__Service__c WHERE Id=:macd.id LIMIT 1];
        System.assert(macd.csordtelcoa__Replacement_Service__c == null, 'MacD replacement should be null');
    }
    
    private static void setupCustomSettings() {
        insert new Product_Definition_Id__c(Name='Master_IPVPN_Service_Definition_Id', Product_Id__c=null);
        insert new Product_Definition_Id__c(Name='Master_VPLS_Service_Definition_Id', Product_Id__c=null);
    }
    
    private static csord__Service__c createService(String name) {
        String description = name;
        cscfga__Product_Definition__c productDefinition = 
            TestDataFactory.createProductDefinition(name, description, true);
        cscfga__Product_Configuration__c productConfiguration = 
            TestDataFactory.createProductConfiguration(name, productDefinition, '', true);
        csord__Order_Request__c orderRequest = 
            TestDataFactory.createOrderRequest(name, '1', true);
        csord__Subscription__c subscription = 
            TestDataFactory.createSubscription(name, name, orderRequest, true);
        
        return TestDataFactory.createService(name, name, productConfiguration, subscription, name, null, orderRequest, null, null, true);
    } */

}