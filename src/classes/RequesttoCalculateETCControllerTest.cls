/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */

@isTest(seealldata=true)
private class RequesttoCalculateETCControllerTest { 
    /*
     *@name testSave_Scenario1()
     *@return void 
     *@description This method for save () In CLASS RequesttoCalculateETCController.cls
     */
    
    static Account acc;
    
     static testMethod void  testSave_Scenario1(){ 
        ConfirmPopUpController confirmPopup=new ConfirmPopUpController();
        User stdUser = UtilNovaUnitTest.getUser();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
        AI.OwnerId=UserInfo.getUserId();
         // IR 215
         AI.Status__c='Sales User in Progress';
         AI.Subject__C='Request to Calculate Early Termination Charge';
         //BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Pacnet Limited');
         List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Country_Lookup__c> ListCountry  = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> ListSite  = P2A_TestFactoryCls.getsites(1,ListAcc,ListCountry);
        List<Contact> ListContact = P2A_TestFactoryCls.getContact(1, ListAcc);
        List<BillProfile__c> billProfObjList  = P2A_TestFactoryCls.getBPs(1,ListAcc,ListSite,ListContact);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        billProfObjList[0].Billing_Entity__c='Pacnet Limited';
         //insert   billProfObj;
         
         Order__c odr=getOrder();
        insert odr;
        Account a=getAccount();
        a.Ownerid=UserInfo.getUserId();
        insert a;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
         serviceObj.Bill_profile__c = billProfObjList[0].Id;// IR 215
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Order_Line_Item__c oli=getOrderLineItem();
        oli.Service__c=serviceObj.Id;
        oli.ParentOrder__c = odr.Id;
        
        insert oli;
         AI.Order_Action_Item__c=odr.Id;
          AI.Order_Line_Item__c=oli.id;
          AI.Feedback__c='test';
          AI.Account__c=a.id;
        system.debug('====UserInfo.getUserId()=='+UserInfo.getUserId());
        insert AI;
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
        RequesttoCalculateETCController requesttoCalculateETCControllerTest  =  new  RequesttoCalculateETCController();
        
        System.runAs(stdUser) {
            PageReference pagereferenceTest = requesttoCalculateETCControllerTest.save();
            requesttoCalculateETCControllerTest .UserEmailUpdation();
            Test.stopTest();
             System.assert(pagereferenceTest!=null);
        }
    }
    
    
    static testMethod void  testSave_Scenario2(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Account a=getAccount();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
        AI.Request_to_Waive_ETC__c=true;
        AI.Status__c='Billing User in Progress';
        AI.Account__c=a.id;
        AI.OwnerId=UserInfo.getUserId();
        insert AI;
        system.assert(AI!=null);
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
        RequesttoCalculateETCController requesttoCalculateETCControllerTest  =  new  RequesttoCalculateETCController();
        
        System.runAs(stdUser) {
            PageReference pagereferenceTest = requesttoCalculateETCControllerTest.save();
            Test.stopTest();
            
        }
        
        
    }
    
    
    static testMethod void  testSave_Scenario3(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
         Order__c odr=getOrder();
        insert odr;
        Account a=getAccount();
        a.Ownerid=UserInfo.getUserId();
        insert a;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Order_Line_Item__c oli=getOrderLineItem();
        oli.Service__c=serviceObj.Id;
        oli.ParentOrder__c = odr.Id;
        
        insert oli;
        
         AI.Order_Action_Item__c=odr.Id;
        AI.Request_to_Waive_ETC__c=true;
        AI.Status__c='Billing User in Progress';
        AI.Comments__c='test';
        AI.OwnerId=UserInfo.getUserId();
        AI.Order_Line_Item__c=oli.id;
        AI.Account__c=a.id;
        AI.Feedback__c='test';
        insert AI;
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
        RequesttoCalculateETCController requesttoCalculateETCControllerTest  =  new  RequesttoCalculateETCController();
        
        System.runAs(stdUser) {
            PageReference pagereferenceTest = requesttoCalculateETCControllerTest.save();
            Test.stopTest();
             System.assert(pagereferenceTest!=null);
        }
    }
    
    
    static testMethod void  testSave_Scenario4(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
        Order__c odr=getOrder();
        insert odr;
        Account a=getAccount();
        a.Ownerid=UserInfo.getUserId();
        insert a;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Order_Line_Item__c oli=getOrderLineItem();
        oli.Service__c=serviceObj.Id;
        oli.ParentOrder__c = odr.Id;
        
        insert oli;
        AI.Request_to_Waive_ETC__c=true;
        AI.Comments__c='test';
        AI.Status__c='Commercial User in Progress';
        AI.Waiver_Request__c='Fully Waived';
        AI.Order_Action_Item__c=odr.Id;
        AI.Order_Line_Item__c=oli.id;
        AI.Feedback__c='test';
        AI.Account__c=a.id;
        AI.OwnerId=UserInfo.getUserId();
        insert AI;
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
        RequesttoCalculateETCController requesttoCalculateETCControllerTest  =  new  RequesttoCalculateETCController();
        RequesttoCalculateETCController.EmailVO e=new RequesttoCalculateETCController.EmailVO();
        
        e.sender='test';
        e.senderDispName='test';
        e.cc=new String[]{'test','test'};
        e.bcc=new String[]{'test','test'};
        e.header='test';
        e.footer='test';
        e.module='test';
        e.originator='test';
        e.replaceCharStart='test';
        e.replaceCharEnd='test';
        e.isCIC=true;
    
        System.runAs(stdUser) {
            PageReference pagereferenceTest = requesttoCalculateETCControllerTest.save();
            requesttoCalculateETCControllerTest.cancel();
            Test.stopTest();
             System.assert(pagereferenceTest!=null);
        }
    }
    
    static testMethod void  testSave_Scenario5(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
        
        Account a=getAccount();
        insert a;
        Order__c odr=getOrder();
        odr.Account__c=a.id;
        insert odr;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Order_Line_Item__c oli=getOrderLineItem();
        oli.Service__c=serviceObj.Id;
        oli.ParentOrder__c = odr.Id;
        
        insert oli;
        AI.Request_to_Waive_ETC__c=true;
        AI.Comments__c='test';
        AI.Status__c='Commercial User in Progress';
        AI.Waiver_Request__c='Fully Waived';
        AI.Order_Action_Item__c=odr.Id;
        //AI.Order_Line_Item__c=oli.id;
         AI.OwnerId=UserInfo.getUserId();
        insert AI;
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
    
        
        System.runAs(stdUser) {
            CreateETCActionItem.createEachAI(odr.Id);
            Test.stopTest();
            // System.assert(pagereferenceTest!=null);
        }
    }
    
    
  public static Order_Line_Item__c getOrderLineItem(){
        Order_Line_Item__c oli = new Order_Line_Item__c();
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'Terminate';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.CPQItem__c = '1';
        oli.Product__c=getProduct('GCPE').Id;
        oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli.MRC_Bill_Text__c='rgtetertetee';
        oli.Parent_Bundle_ID__c='123456';
        return oli;
    }     
     public static Order__c getOrder(){
         
        Order__c ordr = new Order__c();
      
        return ordr;
    }
     public static Service__c getServiceObj(){
      Service__c ser = new Service__c();
        ser.name='serv1';
        ser.Path_Instance_ID__c='1467000';
        ser.Parent_Path_Instance_id__c = '';
        ser.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
        ser.is_this_data_from_PSL__c = True;
        ser.Parent_Charge_Id__c='123456';
      
      return ser;
 }
 public static Account getAccount(){
       
     if(acc == null){
            acc = new Account();
            acc.Name = 'Test Account Test 1';
            acc.Customer_Type__c = 'MNC';
            acc.Customer_Legal_Entity_Name__c = 'Sample Name';
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_ID__c = '5555';
            acc.Account_Status__c = 'Active';
     }
        
        return acc;
    }
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        return p;   
    }
    
private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='GCPE';
        
        insert prod;
        return prod;
    }
    
    
private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2 = getProduct(prodName);
        p.Product2Id =  p.Product2.Id;
       
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p;
         
        system.debug('======in pricebook'+p.Product2.Id);
        return p;    
    }
    
        @isTest static void testExceptions(){
        
           ConfirmPopUpController confirmPopup=new ConfirmPopUpController();
        User stdUser = UtilNovaUnitTest.getUser();
        Action_Item__c AI=UtilNovaUnitTest.getAction_Item_c();
        AI.OwnerId=UserInfo.getUserId();
         // IR 215
         AI.Status__c='Sales User in Progress';
         AI.Subject__C='Request to Calculate Early Termination Charge';
         //BillProfile__c billProfObj = new BillProfile__c(Billing_Entity__c='Pacnet Limited');
         List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Country_Lookup__c> ListCountry  = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> ListSite  = P2A_TestFactoryCls.getsites(1,ListAcc,ListCountry);
        List<Contact> ListContact = P2A_TestFactoryCls.getContact(1, ListAcc);
        List<BillProfile__c> billProfObjList  = P2A_TestFactoryCls.getBPs(1,ListAcc,ListSite,ListContact);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        billProfObjList[0].Billing_Entity__c='Pacnet Limited';
         //insert   billProfObj;
         
         Order__c odr=getOrder();
        insert odr;
        Account a=getAccount();
        a.Ownerid=UserInfo.getUserId();
        insert a;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
         serviceObj.Bill_profile__c = billProfObjList[0].Id;// IR 215
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Order_Line_Item__c oli=getOrderLineItem();
        oli.Service__c=serviceObj.Id;
        oli.ParentOrder__c = odr.Id;
        
        insert oli;
         AI.Order_Action_Item__c=odr.Id;
          AI.Order_Line_Item__c=oli.id;
          AI.Feedback__c='test';
          AI.Account__c=a.id;
        system.debug('====UserInfo.getUserId()=='+UserInfo.getUserId());
        insert AI;
        Test.startTest();
        PageReference pageTest2 = Page.RequesttoCalculateETCPage;
        pageTest2.getParameters().put('Id',AI.id);
        Test.setCurrentPage(pageTest2);
        
         RequesttoCalculateETCController alls=new RequesttoCalculateETCController();
        
         try{alls.insertDynDataInBody(null,null);}catch(Exception e){}
         try{alls.sendMail(null);}catch(Exception e){}
          system.assertEquals(true,alls!=null);    
         
         
     }
}