/**
    @author - Accenture
    @date - 18-April-2013
    @version - 1.0
    @description - This is the test class for OrderLineItemAfterUpdate Trigger.
*/
@isTest(SeeAllData=false)
private class ResourceHierarchyBusinessClassTest {

   /* static TestMethod void getResources(){
    
    Test.startTest();
    List<csord__Order_Request__c> ordReqList = P2A_TestFactoryCls.getorderrequest(1);    
    List<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,ordReqList);
    List<csord__Order_Line_Item__c> OrderLineItemlist = P2A_TestFactoryCls.getOrderlineItem(1,ordList,ordReqList);
    List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1, ordReqList);
    List<csord__Service__c> serList = P2A_TestFactoryCls.getService(1, ordReqList,subList);
    
    
       getOrderlineItem(Integer totalRecords, List<csord__Order__c> OrdersLists, List<csord__Order_Request__c> OredrRequetslists){
        List<Task> testTask= new List<Task>();
        csord__Order_Line_Item__c orderLineItem = getOrderLineItem();
        Test.startTest();
        insert orderLineItem;
        Test.stopTest();
        
        orderLineItem.OrderType__c = 'Cancel';
        update orderLineItem;
          
        
       testTask = [SELECT Subject,Status,Priority,WhatId,Description FROM Task WHERE WhatId =: orderLineItem.ParentOrder__c];
        
       System.assert(testTask.size() == 1);
        
        orderLineItem.OrderType__c ='Terminate';
        orderLineItem.Service_Assurance_Assigned_Date__c=date.newinstance(2012, 11, 28);
       orderLineItem.Service_Assurance_Contact__c = getcontact().Id;
        orderLineItem.Service_Assurance_Contact__c = '';
        update orderLineItem;
        
        testTask = [SELECT Subject,Status,Priority,WhatId,Description FROM Task WHERE WhatId =: orderLineItem.ParentOrder__c];
        
        System.assert(testTask.size() == 2);
      
    }
    
    
     
    private static csord__Order_Line_Item__c getOrderLineItem(){
        csord__Order_Line_Item__c oli = new csord__Order_Line_Item__c();
        csord__Order__c ord = ordList.get(0).Id;
        oli.ParentOrder__c = ord.Id;
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'New Provide';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        Contact con = getcontact();
        oli.Service_Assurance_Contact__c = con.Id;
        oli.Service_Assurance_Contact_Number__c = con.phone;
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.CPQItem__c = '1';
        oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli.Is_shared_with_multiple_services__c = 'Yes';
        oli.Product__c = getProduct1().id;
        
        return oli;
    }
     private static Contact getcontact(){
        Contact con = new Contact();
        con.Lastname ='TestName';
        con.contact_Type__c = 'sales';
        con.phone = '998877665';
        insert con;
        return con;
    }
    private static Order__c getOrder(){
        
        Order__c ordr = new Order__c();
        ordr.SD_PM_Contact__c = getUser().Id;
        Opportunity o = getOpportunity();
        ordr.Opportunity__c = o.Id;
        insert ordr;
        return ordr;
    }
    private static Opportunity getOpportunity(){
        Opportunity opp = new Opportunity();
        Account a = getAccount();
         opp.Name = 'Test Opportunity';
         opp.AccountId = a.Id;
         opp.StageName = 'Identify & Define';
         opp.Stage__c='Identify & Define';
         opp.CloseDate = System.today();
         opp.Estimated_MRC__c=800;
         opp.Estimated_NRC__c=1000;
         opp.ContractTerm__c='10';
         opp.TigFin_PR_Actions__c = 'Processed';
        insert opp;
        return opp;
    }
     private static Account getAccount(){
        Account acc = new Account();
        Country_Lookup__c cl = getCountry();
        acc.Country__c = cl.Id;
        acc.Name = 'Test Account Test 4';
        acc.Customer_Type__c = 'MNC'; 
        acc.account_Id__c = 'Test123';    
        acc.Selling_Entity__c = 'Telstra INC';
        acc.activated__c = true;
        acc.Customer_Legal_Entity_Name__c='Test';
        
        acc.Account_Status__c ='Active';
        insert acc;
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'IND';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
        return cl;
    } 
    
    private static User getUser(){
        User u = new User();
        u.FirstName = 'suraj';
        u.LastName = 'Talreja';
        u.Alias = 'stalr';
        u.Email = 'suraj.talreja@accenture.com';
        u.Username = 'suraj.talreja@accenture.com';
        u.CommunityNickname = 'suraj.talreja';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.EmployeeNumber='1023';
        u.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u;
        return u;
    }
    
    private static Product2  getProduct1(){
            
            Product2 prod = new Product2();
            prod.name = 'Test Product';
            prod.CurrencyIsoCode='EUR';
            prod.Product_ID__c='OFFPOP';      
            prod.Create_Resource__c= false;
            insert prod;
            return prod;
            
     }*/
    

  


}