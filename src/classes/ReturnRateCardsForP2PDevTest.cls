@isTest(SeeAllData = false)
public class ReturnRateCardsForP2PDevTest {
/**
* Disables triggers, validations and workflows for the given user
* @param userId Id
*/
     
    private static voId disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_ValIdations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_ValIdations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
/**
* Enables triggers, valIdations and workflows 
* @param userId Id
*/
    private static voId enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
//Initializing lists

    private static List<cspmb__Price_Item__c> priceItemList;
    private static List<CS_Country__c> countryList;
    private static List<CS_City__c> cityList;
    private static List<CS_Pop__c> popList;
    private static List<CS_Bandwidth__c> bandwidthList;
    private static List<CS_Bandwidth_Product_Type__c> bandwidthProductTypeList;
    private static Account act;
    private static List<CS_Route_Segment__c> rtList;

//creating test data
    static voId createTestData()
    {  
        disableAll(UserInfo.getUserId());
        //Creating Country object records
        countryList = new List<CS_Country__c>{
            new CS_Country__c(Name = 'UNITED KINGDOM Test'),
            new CS_Country__c(Name = 'Croatia Test')
                };
         insert countryList;
         List<CS_Country__c> country = [select name from CS_Country__c where name = 'UNITED KINGDOM Test'];
         system.assert(country!=null);
         system.assertEquals(countryList[0].name, country[0].name);        
        
       //Creating City object records
       cityList = new List<CS_City__c>{
            new CS_City__c(Name = 'London Test', CS_Country__c = countryList[0].Id),
            new CS_City__c(Name = 'Zagreb Test', CS_Country__c = countryList[1].Id)
        };
        
        insert cityList;
         List<CS_City__c> city = [select name,CS_Country__c  from CS_City__c where name = 'London Test'];
         system.assert(city!=null);
         system.assertEquals(cityList[0].name, city[0].name); 
        
    //Creating POP object records
        popList = new List<CS_POP__c>{
            new CS_POP__c(Name='LondonZZZ Test',CS_Country__c = countryList[0].Id,CS_City__c = cityList[0].Id),
            new CS_POP__c(Name='ZagrebZZZ Test',CS_Country__c = countryList[0].Id,CS_City__c = cityList[0].Id)
        };
        insert poplist;
        system.assertEquals(poplist[0].name, 'LondonZZZ Test');
    
    //Inserting CS Bandwidth object records
        bandwidthList = new List<CS_Bandwidth__c>{
            new CS_Bandwidth__c(Name='1.5M',Bandwidth_Value_MB__c=1.5),
            new CS_Bandwidth__c(Name='10M',Bandwidth_Value_MB__c=10),
            new CS_Bandwidth__c(Name='100M',Bandwidth_Value_MB__c=100)   
        };
    insert bandwidthList;
    system.assertEquals(bandwidthList[0].name, '1.5M');
    
    //Inserting CS Bandwidth Product Type object records
        bandwidthProductTypeList = new List<CS_Bandwidth_Product_Type__c>{
            new CS_Bandwidth_Product_Type__c(Name='1.5MTest',Product_Type__c='ICBS',CS_Bandwidth__c= bandwidthList[0].Id),
            new CS_BandwIdth_Product_Type__c(Name='10MTest',Product_Type__c='EVPL',CS_BandwIdth__c= bandwidthList[1].Id),
            new CS_BandwIdth_Product_Type__c(Name='100MTest',Product_Type__c='IPL',CS_BandwIdth__c= bandwidthList[2].Id)    
         };
    insert bandwidthProductTypeList;
    system.assertEquals(bandwidthProductTypeList[0].name, '1.5MTest');
    
        //Creating Account object records
        act = new Account(Name='Test112233M',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated');
        insert act;
        system.assert(act!=null);
      
        //Creating CS_Route_Segment__c object records
        rtList =  new List<CS_Route_Segment__c>{
        new CS_Route_Segment__c(Product_Type__c='ICBS',Type__c='Route',/*CS_Country__c=countryList[0].Id,Z_End_Country__c=countryList[1].Id,*/A_End_City__c=cityList[0].Id,Z_End_City__c=cityList[1].Id,Pop_A__c=popList[0].Id,Pop_Z__c=popList[1].Id),
        new CS_Route_Segment__c(Product_Type__c='EVPL',Type__c='Route',/*CS_Country__c=countryList[0].Id,Z_End_Country__c=countryList[1].Id,*/A_End_City__c=cityList[0].Id,Z_End_City__c=cityList[1].Id,Pop_A__c=popList[0].Id,Pop_Z__c=popList[1].Id),
        new CS_Route_Segment__c(Product_Type__c='IPL',Type__c='Route',/*CS_Country__c=countryList[0].Id,Z_End_Country__c=countryList[1].Id,*/A_End_City__c=cityList[0].Id,Z_End_City__c=cityList[1].Id,Pop_A__c=popList[0].Id,Pop_Z__c=popList[1].Id)
            };
        insert rtList;
        system.assertEquals(rtList[0].Product_Type__c, 'ICBS');
    
    //Insert PriceItem records
        priceItemList= new List<cspmb__Price_Item__c>{
        new cspmb__Price_Item__c(Name = 'priceItemWithAccount',cspmb__Account__c=act.Id,Country__c=countryList[0].Id,Z_Country__c=countryList[1].Id,City__c=cityList[0].Id,Z_City__c=cityList[1].Id,Pop__c=popList[0].Id,Z_Pop__c=popList[1].Id,CS_Bandwidth_Product_Type__c = bandwidthProductTypeList[0].Id,cspmb__Is_Active__c=true),
        new cspmb__Price_Item__c(Name = 'priceItemWithAccount1',cspmb__Account__c=act.Id,Country__c=countryList[0].Id,Z_Country__c=countryList[1].Id,City__c=cityList[0].Id,Z_City__c=cityList[1].Id,Pop__c=popList[0].Id,Z_Pop__c=popList[1].Id,CS_Bandwidth_Product_Type__c = bandwidthProductTypeList[1].Id,cspmb__Is_Active__c=true),
        new cspmb__Price_Item__c(Name = 'priceItemWithAccount2',cspmb__Account__c=null,Country__c=countryList[0].Id,Z_Country__c=countryList[1].Id,City__c=cityList[0].Id,Z_City__c=cityList[1].Id,Pop__c=popList[0].Id,Z_Pop__c=popList[1].Id,CS_Bandwidth_Product_Type__c = bandwidthProductTypeList[2].Id,cspmb__Is_Active__c=true)
        };    
    insert priceItemList;
         List<cspmb__Price_Item__c> priceitem = [select id,name from cspmb__Price_Item__c where name = 'priceItemWithAccount'];
         system.assert(priceitem!=null);
         system.assertEquals(priceItemList[0].name, priceitem[0].name); 
    }
    
    private static testmethod void  returnRateCardsTestMethod()
    {
        createTestData();
        enableAll(UserInfo.getUserId());
        
        Set<String> productType = new Set<String> {'ICBS','EVPL','IPL'};    
    List<String> bandwIdthNames = new List<String>{'1.5M','10M','100M'};
        Test.startTest();
        
         List<CS_PTP_Rate_Card_Dummy__c> returnRateCards = ReturnRateCardsForP2PDev.returnRateCards(productType,countryList[0].Id,countryList[1].Id,cityList[0].Id,cityList[1].Id,popList[0].Id,popList[1].Id,bandwidthNames,act.Id);    
         system.assertEquals(true,returnRateCards!=null);
        Test.stopTest();
    }   

}