global with sharing class RoutingTableWrapper implements Comparable {
    //declaration
   public String productID;   
   Public Decimal MRC;
   public String CS_A_Country;
   public String CS_A_POP;
   public String CS_Cable_Path;
   public String CS_Route_Segment;
   public String CS_Z_Country;
   public String CS_Z_POP;
   public String Price_Item;
   public String A_End_Country_Name;
   public String Bandwidth_Name;
   public String Cable_Path_Name;
   public String Circuit_Type;
   public String Network_Technology;
   public String Z_End_Country_Name;
   public String A_POP_Name;
   public String Z_POP_Name;
   public Decimal NRC;
   public String Partner_Name;
   public String NNI_Service_ID;
   public String CS_Resilience;
   public String Resilience_Name;
   public String NNI_Location1;
   public String Segment;
   public String AEndPartnerName;
   public String ZEndPartnerName;
   
   //contructor
   public RoutingTableWrapper(CS_PTP_Rate_Card_Dummy__c r){
                
                this.productID=r.Product__c;
                this.MRC=r.MRC__c;
                this.CS_A_Country=r.CS_A_Country__c;
                this.CS_A_POP=r.CS_A_POP__c;
                this.CS_Cable_Path=r.CS_Cable_Path__c;
                this.CS_Route_Segment=r.CS_Route_Segment__c;
                this.CS_Z_Country=r.CS_Z_Country__c;
                this.CS_Z_POP=r.CS_Z_POP__c;
                this.Price_Item=r.Price_Item__c;
                this.A_End_Country_Name=r.A_End_Country_Name__c;
                this.Bandwidth_Name=r.Bandwidth_Name__c;
                this.Cable_Path_Name=r.Cable_Path_Name__c;
                this.Circuit_Type=r.Circuit_Type__c;
                this.Network_Technology=r.Network_Technology__c;
                this.Z_End_Country_Name=r.Z_End_Country_Name__c;
                this.A_POP_Name=r.A_POP_Name__c;
                this.Z_POP_Name=r.Z_POP_Name__c;
                this.NRC=r.NRC__c;
                this.Partner_Name=r.Partner_Name__c;
                this.NNI_Service_ID=r.NNI_Service_ID__c;
                this.CS_Resilience=r.CS_Resilience__c;
                this.Resilience_Name=r.Resilience_Name__c;
                this.NNI_Location1=r.NNI_Location1__c;
                this.Segment=r.Segment__c;
                this.AEndPartnerName=r.A_End_Partner_Name__c;
                this.ZEndPartnerName=r.Z_End_Partner_Name__c;
        }
     
    

  

    global Integer compareTo(Object compareTo) 
    {
        
        RoutingTableWrapper jobsWrapper = (RoutingTableWrapper) compareTo;        
            
            integer a=productID.compareTo(jobsWrapper.productID);
            if(a!=0){return a;}
            else{
            if (MRC > jobsWrapper.MRC) {
                return 1;
            }        
            else if(MRC < jobsWrapper.MRC) {
                return -1;
            }
            }
            return 0;        
            
    }
    
    public static List<CS_PTP_Rate_Card_Dummy__c> getSortedRoutongTable(List<CS_PTP_Rate_Card_Dummy__c>rateCardDummyList){
        
        List<RoutingTableWrapper>routingList=new List<RoutingTableWrapper >();
        List<CS_PTP_Rate_Card_Dummy__c>rateCardDummyList1=new List<CS_PTP_Rate_Card_Dummy__c >();
        
        for(CS_PTP_Rate_Card_Dummy__c r:rateCardDummyList){
            routingList.add(new RoutingTableWrapper(r));
        }
        routingList.sort();
        
        
        
        for(RoutingTableWrapper r:routingList){ 
        
        CS_PTP_Rate_Card_Dummy__c ro=new CS_PTP_Rate_Card_Dummy__c();
        
        ro.Product__c=r.productID;
        ro.MRC__c=r.MRC;
        ro.CS_A_Country__c=r.CS_A_Country;
        ro.CS_A_POP__c=r.CS_A_POP;
        ro.CS_Cable_Path__c=r.CS_Cable_Path;
        ro.CS_Route_Segment__c=r.CS_Route_Segment;
        ro.CS_Z_Country__c=r.CS_Z_Country;
        ro.CS_Z_POP__c=r.CS_Z_POP;
        ro.Price_Item__c=r.Price_Item;
        ro.A_End_Country_Name__c=r.A_End_Country_Name;
        ro.Bandwidth_Name__c=r.Bandwidth_Name;
        ro.Cable_Path_Name__c=r.Cable_Path_Name;
        ro.Circuit_Type__c=r.Circuit_Type;
        ro.Network_Technology__c=r.Network_Technology;
        ro.Z_End_Country_Name__c=r.Z_End_Country_Name;
        ro.A_POP_Name__c=r.A_POP_Name;
        ro.Z_POP_Name__c=r.Z_POP_Name;
        ro.NRC__c=r.NRC;
        ro.Partner_Name__c=r.Partner_Name;
        ro.NNI_Service_ID__c=r.NNI_Service_ID;
        ro.CS_Resilience__c=r.CS_Resilience;
        ro.Resilience_Name__c=r.Resilience_Name;
        ro.NNI_Location1__c=r.NNI_Location1;
        ro.Segment__c=r.Segment;
        ro.A_End_Partner_Name__c=r.AEndPartnerName;
        ro.Z_End_Partner_Name__c=r.ZEndPartnerName;

        system.debug('ro ki value'+ro);
        rateCardDummyList1.add(ro);
        }
        system.debug('rateCardDummyList1 ki value'+rateCardDummyList1);
        
    
    return rateCardDummyList1;
    
    }
}