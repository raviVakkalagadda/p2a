@istest
Public class SObjectFactorytest{
public static testmethod void SObjectFactory_test(){

Schema.DescribeSObjectResult cfrSchema = Schema.SObjectType.Case; 

  FieldValueFactory fdvg = new FieldValueFactory();  
  List<Map<String, Object>> ListMapsobj = new List<Map<String, Object>>();
SObjectFactory sof = new SObjectFactory();
sof.create(cfrSchema);
sof.setFieldValueFactory(fdvg);
system.assertEquals(true,fdvg!=null);

}
}