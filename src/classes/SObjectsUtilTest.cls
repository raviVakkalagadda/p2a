@IsTest
public class SObjectsUtilTest {
    
    @IsTest(seeAllData=true)
    public static void testGetIds() {
        List<Account> accounts = [SELECT Id, Name FROM Account];
        Set<Id> ids = SObjectsUtil.getIds(accounts);
        System.assert(ids.size() > 0, 'Expected a non-empty set of ids');
    }
    
    @IsTest(seeAllData=true)
    public static void testGetIdsForSingleField() {
        List<Account> accounts = [SELECT Id, Name, OwnerId FROM Account];
        Set<Id> ids = SObjectsUtil.getIds(accounts, 'OwnerId');
        System.assert(ids.size() > 0, 'Expected a non-empty set of ids');
    }
    
    @IsTest(seeAllData=true)
    public static void testGetIdsForAFieldHierarchy() {
        List<Account> accounts = [SELECT Id, Name, Owner.Profile.Id FROM Account];        
        Set<Id> ids = SObjectsUtil.getIds(accounts, 'Owner.Profile.Id');
        System.assert(ids.size() > 0, 'Expected a non-empty set of ids');
    }
    
    public static void testGetIdsWithNullId() {
        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < 5; i++) {
            accounts.add(new Account());
        }
        Set<Id> ids = SObjectsUtil.getIds(accounts);
        System.assert(ids.isEmpty(), 'Null values should have been removed from the retured Set.');
    }
}