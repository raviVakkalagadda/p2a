@istest
Public Class SamplesolutionsummaryTest{

@testSetup static void setup(){
    
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);   
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   
   
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    ord1.solutions__c=sol[0].id;
    //ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;    
    
    
    List<id>orderListVal = new List<Id>{ord1.id};
    set<Id>NonParallelOrderSet=new Set<Id>{ord1.id};
       Test.starttest(); 
       
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));   
       SubmitOrderBatch bc = new SubmitOrderBatch(orderListVal, NonParallelOrderSet, false, false);
       Database.executeBatch(bc,1); 
       
       
       samsolsum.SolutionList=new List<String>{'SOL-123'};
       samsolsum.OrderList=new List<csord__Order__c>{ord1};
       samsolsum.orderListForValidation=new List<csord__Order__c>{ord1};
      
       
       Test.stoptest();
    }

@isTest static void TestMethod1(){
    
   //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
   accList[0].Region__c='region1';
   update accList; 
   
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   sol[0].batch_count__c=2;
   update sol;
   
   
   
   //Create Order and it's orchestration
    CSPOFA__Orchestration_Process_Template__c ot1=new CSPOFA__Orchestration_Process_Template__c(name='order_new');
    insert ot1;
    CSPOFA__Orchestration_Process_Template__c ot2=new CSPOFA__Orchestration_Process_Template__c(name='servicenew');
    insert ot2;            
    
    //List of Orders
    List<csord__Order__c>Orderlist=new List<csord__Order__c>();
    
    //*****creation of Orders*******//
    //first Order
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    //ord1.Max_CRD__c=System.Today();
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;
    
    
    //Second Order
    csord__Order__c ord2 = new csord__Order__c();
    ord2.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord2.csord__Order_Request__c =OrdReqList[0].id;
    ord2.Order_Submitted_to_SD__c=true;
    ord2.Subscription__c=SUBList[0].id; 
    //ord2.Max_CRD__c=System.Today();
    ord2.SD_PM_Contact__c=userList[0].id;
    insert ord2;
    
    
    //Third Order
    csord__Order__c ord3 = new csord__Order__c();
    ord3.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord3.csord__Order_Request__c =OrdReqList[0].id;
    ord3.Order_Submitted_to_SD__c=true;
    ord3.Subscription__c=SUBList[0].id;
    //ord3.Is_Order_Submitted__c=true;  
    //ord3.Max_CRD__c=System.Today();
    ord3.SD_PM_Contact__c=userList[0].id;
    insert ord3;
    
    //Fourth Order
    csord__Order__c ord4 = new csord__Order__c();
    ord4.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord4.csord__Order_Request__c =OrdReqList[0].id;
    ord4.Order_Submitted_to_SD__c=true;
    ord4.Subscription__c=SUBList[0].id;
    ord4.Is_Order_Submitted__c=true;    
    ord4.SD_PM_Contact__c=userList[0].id;
    insert ord4;
    
    //*****creation of Service*******//
    csord__Service__c ser = new csord__Service__c();
    ser.AccountId__c=accList[0].id;
    ser.Product_Id__c = 'IPVPN'; 
    ser.csord__Identification__c = 'Test-Catlyne-4238362';
    ser.csord__Order_Request__c =OrdReqList[0].id;
    ser.csord__Subscription__c =SUBList[0].id;
    ser.Billing_Commencement_Date__c = System.Today();
    ser.Stop_Billing_Date__c = System.Today();
    ser.Customer_Required_Date__c = System.Today();     
    ser.solutions__c=sol[0].id; 
    insert ser;
    ser.primary_port_service__c=null;
    ser.csord__order__c=ord1.id;
    ser.Root_Service_ID__c=ser.id;
    ser.Product_Configuration_Hierarchy__c='001';
    update ser;
    
    csord__Service__c ser1 = new csord__Service__c();
    ser1.Product_Id__c = 'IPVPN PORT';
    ser1.AccountId__c=accList[0].Id;    
    ser1.csord__Identification__c = 'Test-Catlyne-4238362';
    ser1.csord__Order_Request__c =OrdReqList[0].id;
    ser1.csord__Subscription__c =SUBList[0].id;
    ser1.Billing_Commencement_Date__c = System.Today();
    ser1.Stop_Billing_Date__c = System.Today();
    ser1.Customer_Required_Date__c = System.Today();        
    ser1.solutions__c=sol[0].id;
    ser1.primary_port_service__c=ser.id;
    ser1.csord__order__c=ord2.id;
    ser1.Root_Service_ID__c=ser.id;
    ser1.Product_Configuration_Hierarchy__c='001.001';
    ser1.Service_Type__c='SE';
    ser1.Product_Code__c='IPVPN PORT';
    ser1.Selling_Entity__c='ABC';   
    ser1.Estimated_Start_Date__c=System.Today();
    //ser1.Product_Configuration_Type__c='ABC';
    ser1.A_City_Code__c='other1';
    ser1.Z_City_Code__c='other1';
    ser1.A_Side_Site__c='other1';
    ser1.Z_Side_Site__c='other';
    insert ser1;
    
    csord__Service__c ser2 = new csord__Service__c();
    ser2.Product_Id__c = 'IPVPN';
    ser2.AccountId__c=accList[0].Id;    
    ser2.csord__Identification__c = 'Test-Catlyne-4238362';
    ser2.csord__Order_Request__c =OrdReqList[0].id;
    ser2.csord__Subscription__c =SUBList[0].id;
    ser2.Billing_Commencement_Date__c = System.Today();
    ser2.Stop_Billing_Date__c = System.Today();
    ser2.Customer_Required_Date__c = System.Today();        
    ser2.solutions__c=sol[0].id;    
    insert ser2;
    ser2.primary_port_service__c=null;
    ser2.csord__order__c=ord3.id;
    ser2.Root_Service_ID__c=ser2.id;
    ser2.Product_Configuration_Hierarchy__c='002';
    update ser2;
    
    
    
    csord__Service__c ser3 = new csord__Service__c();
    ser3.Product_Id__c = 'NID';     
    ser3.csord__Identification__c = 'Test-Catlyne-4238362';
    ser3.csord__Order_Request__c =OrdReqList[0].id;
    ser3.csord__Subscription__c =SUBList[0].id;
    ser3.Billing_Commencement_Date__c = System.Today();
    ser3.Stop_Billing_Date__c = System.Today();
    ser3.Customer_Required_Date__c = System.Today();        
    ser3.solutions__c=sol[0].id;
    ser3.primary_port_service__c=ser2.id;
    ser3.csord__order__c=ord2.id;
    ser3.Root_Service_ID__c=ser.id;
    ser3.Product_Configuration_Hierarchy__c='001.002';
    ser3.Service_Type__c='SE';
    ser3.Product_Code__c='NID';
    ser3.Selling_Entity__c='ABC';   
    ser3.Estimated_Start_Date__c=System.Today();    
    ser3.Order_Type_Final__c='New Provide'; 
    ser3.A_City_Code__c='other1';
    ser3.Z_City_Code__c='other1';
    ser3.A_Side_Site__c='other1';
    ser3.Z_Side_Site__c='other';    
    ser3.Cease_Service_Flag__c=true;
    ser3.AccountId__c=accList[0].Id;
    upsert ser3;    
    
    system.assertEquals(ser3.Cease_Service_Flag__c,true);
    system.assertEquals(ser3.Order_Type_Final__c,'New Provide');    
    system.assertEquals(ser3.Customer_Required_Date__c!=null,true);
    system.assertEquals(ser3.Service_Type__c!=null,true);
    system.assertEquals(ser3.Product_Code__c!=null,true);
    system.assertEquals(ser3.Selling_Entity__c!=null,true);
    system.assertEquals(ser3.Estimated_Start_Date__c!=null,true);
    //system.assertEquals(ser3.Product_Configuration_Type__c,'ABCD');
    //system.assertEquals(ser3.region__c!=null,true);   
    
    csord__Service__c ser4 = new csord__Service__c();
    ser4.Product_Id__c = 'GMNS';
    ser4.AccountId__c=accList[0].Id;    
    ser4.csord__Identification__c = 'Test-Catlyne-4238362';
    ser4.csord__Order_Request__c =OrdReqList[0].id;
    ser4.csord__Subscription__c =SUBList[0].id;
    ser4.Billing_Commencement_Date__c = System.Today();
    ser4.Stop_Billing_Date__c = System.Today();
    ser4.Customer_Required_Date__c = System.Today();        
    ser4.solutions__c=sol[0].id;
    ser4.primary_port_service__c=ser2.id;
    ser4.csord__order__c=ord4.id;
    ser4.Root_Service_ID__c=ser.id;
    ser4.Product_Configuration_Hierarchy__c='001.002';
    ser4.Service_Type__c='SE';  
    ser4.Product_Code__c='GMNS';
    ser4.Selling_Entity__c='ABC';
    ser4.Estimated_Start_Date__c=System.Today();
    insert ser4;
    
    
    List<id>orderListVal = new List<Id>{ord1.id,ord2.id,ord3.id};
    //*****Data Creation completed***/
    
       Test.starttest();               
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));
       samsolsum.custTest='true';      
       samsolsum.expand();
       samsolsum.parentServiceIdForCheckBox=ser.id;
       samsolsum.parentServiceId=ser.id;
       samsolsum.expand();         
       samsolsum.checkBoxValueMap=new Map<Id,Boolean>();
       samsolsum.checkBoxValueMap.put(ser.id,true);
       samsolsum.checkBoxValueMap.put(ser1.id,true);
       samsolsum.checkBoxValueMap.put(ser2.id,true);
       samsolsum.checkBoxValueMap.put(ser3.id,true);
       samsolsum.checkBoxValueMap.put(ser4.id,true);           
       samsolsum.selectdeselectAll();
       samsolsum.check=false;
       samsolsum.selectdeselectAll();
       samsolsum.cascadeDetail();
       samsolsum.next();
       samsolsum.previous();
       samsolsum.first();
       samsolsum.last();
       samsolsum.getPaginationRecords(1);
       samsolsum.currentlevel=1;
       samsolsum.level=3;
       samsolsum.BatchComplete=3;   
       samsolsum.checkBoxValueMap=new Map<Id,Boolean>();
       samsolsum.checkBoxValueMap.put(ser.id,true);
       samsolsum.checkBoxValueMap.put(ser1.id,true);
       samsolsum.checkBoxValueMap.put(ser2.id,true);
       samsolsum.checkBoxValueMap.put(ser3.id,true);
       samsolsum.checkBoxValueMap.put(ser4.id,true);
       samsolsum.SubmitibleOrderMap=new Map<Integer,Set<Id>>();
       samsolsum.SubmitibleOrderMap.put(1,new Set<Id>{ord1.id});
       samsolsum.SubmitibleOrderMap.put(2,new Set<Id>{ord2.id});
       samsolsum.SubmitibleOrderMap.put(3,new Set<Id>{ord3.id});
       samsolsum.SubmitibleOrderMap.put(4,new Set<Id>{ord4.id});
       samsolsum.BulkSubmit();
       samsolsum.saveAction();
       samsolsum.MasterOrderSet=new Set<Id>{ord1.id,ord2.id,ord3.id};
       Map<Id,String>OrderNumberMap=new Map<Id,String>();
       OrderNumberMap.put(ord1.id,'111');
       OrderNumberMap.put(ord2.id,'222');
       OrderNumberMap.put(ord3.id,'333');
       OrderNumberMap.put(ord4.id,'444');
       
       //for batch status          
       //case1:
       samsolsum.checkBatchStatus();
       
       samsolsum.currentlevel=2;
       samsolsum.level=3;
       samsolsum.checkBatchStatus();
       
       samsolsum.currentlevel=3;
       samsolsum.level=3;
       samsolsum.checkBatchStatus();
       //case2:
       sol[0].batch_count__c=0;
       update sol;
       samsolsum.checkBatchStatus();
       //case3:
       samsolsum.BatchComplete=40;
       samsolsum.checkBatchStatus();       
       samsolsum.MasterChildRelationshipCheck(samsolsum.MasterOrderSet,OrderNumberMap);
       samsolsum.validationMethod(orderListVal);       
       samsolsum.removeExtraComma('Test,');
       samsolsum.modifySelectAllCheckboxValue();
       Test.stoptest();
    
    
   
}

@isTest static void TestMethod2(){
    
   //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
   accList[0].Region__c='region1';
   update accList; 
   
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   sol[0].batch_count__c=2;
   update sol;
   
   
   
   //Create Order and it's orchestration
    CSPOFA__Orchestration_Process_Template__c ot1=new CSPOFA__Orchestration_Process_Template__c(name='order_new');
    insert ot1;
    CSPOFA__Orchestration_Process_Template__c ot2=new CSPOFA__Orchestration_Process_Template__c(name='servicenew');
    insert ot2;            
    
    //List of Orders
    List<csord__Order__c>Orderlist=new List<csord__Order__c>();
    
    //*****creation of Orders*******//
    //first Order
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    //ord1.Max_CRD__c=System.Today();
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;
    
    
    //Second Order
    csord__Order__c ord2 = new csord__Order__c();
    ord2.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord2.csord__Order_Request__c =OrdReqList[0].id;
    ord2.Order_Submitted_to_SD__c=true;
    ord2.Subscription__c=SUBList[0].id; 
    //ord2.Max_CRD__c=System.Today();
    ord2.SD_PM_Contact__c=userList[0].id;
    insert ord2;
    
    
    //Third Order
    csord__Order__c ord3 = new csord__Order__c();
    ord3.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord3.csord__Order_Request__c =OrdReqList[0].id;
    ord3.Order_Submitted_to_SD__c=true;
    ord3.Subscription__c=SUBList[0].id;
    //ord3.Is_Order_Submitted__c=true;  
    //ord3.Max_CRD__c=System.Today();
    ord3.SD_PM_Contact__c=userList[0].id;
    insert ord3;
    
    //Fourth Order
    csord__Order__c ord4 = new csord__Order__c();
    ord4.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord4.csord__Order_Request__c =OrdReqList[0].id;
    ord4.Order_Submitted_to_SD__c=false;
    ord4.Subscription__c=SUBList[0].id;
    ord4.Is_Order_Submitted__c=true;    
    ord4.SD_PM_Contact__c=userList[0].id;
    insert ord4;
    
    //*****creation of Service*******//
    csord__Service__c ser = new csord__Service__c();
    ser.AccountId__c=accList[0].id;
    ser.Product_Id__c = 'IPVPN'; 
    ser.csord__Identification__c = 'Test-Catlyne-4238362';
    ser.csord__Order_Request__c =OrdReqList[0].id;
    ser.csord__Subscription__c =SUBList[0].id;
    ser.Billing_Commencement_Date__c = System.Today();
    ser.Stop_Billing_Date__c = System.Today();
    ser.Customer_Required_Date__c = System.Today();     
    ser.solutions__c=sol[0].id;
    ser.Service_Type__c='SE';
    ser.Product_Code__c='IPVPN';
    ser.Selling_Entity__c='ABC';    
    ser.Estimated_Start_Date__c=System.Today(); 
    insert ser;
    ser.primary_port_service__c=null;
    ser.csord__order__c=ord1.id;
    ser.Root_Service_ID__c=ser.id;
    ser.Product_Configuration_Hierarchy__c='001';
    update ser;
    
    csord__Service__c ser1 = new csord__Service__c();
    ser1.Product_Id__c = 'IPVPN PORT';
    ser1.AccountId__c=accList[0].Id;    
    ser1.csord__Identification__c = 'Test-Catlyne-4238362';
    ser1.csord__Order_Request__c =OrdReqList[0].id;
    ser1.csord__Subscription__c =SUBList[0].id;
    ser1.Billing_Commencement_Date__c = System.Today();
    ser1.Stop_Billing_Date__c = System.Today();
    ser1.Customer_Required_Date__c = System.Today();        
    ser1.solutions__c=sol[0].id;
    ser1.primary_port_service__c=ser.id;
    ser1.csord__order__c=ord2.id;
    ser1.Root_Service_ID__c=ser.id;
    ser1.Product_Configuration_Hierarchy__c='001.001';
    ser1.Service_Type__c='SE';
    ser1.Product_Code__c='IPVPN PORT';
    ser1.Selling_Entity__c='ABC';   
    ser1.Estimated_Start_Date__c=System.Today();
    //ser1.Product_Configuration_Type__c='ABC';
    ser1.A_City_Code__c='other1';
    ser1.Z_City_Code__c='other1';
    ser1.A_Side_Site__c='other1';
    ser1.Z_Side_Site__c='other';
    insert ser1;
    
    csord__Service__c ser2 = new csord__Service__c();
    ser2.Product_Id__c = 'IPVPN';
    ser2.AccountId__c=accList[0].Id;    
    ser2.csord__Identification__c = 'Test-Catlyne-4238362';
    ser2.csord__Order_Request__c =OrdReqList[0].id;
    ser2.csord__Subscription__c =SUBList[0].id;
    ser2.Billing_Commencement_Date__c = System.Today();
    ser2.Stop_Billing_Date__c = System.Today();
    ser2.Customer_Required_Date__c = System.Today();        
    ser2.solutions__c=sol[0].id;    
    insert ser2;
    ser2.primary_port_service__c=null;
    ser2.csord__order__c=ord3.id;
    ser2.Root_Service_ID__c=ser2.id;
    ser2.Product_Configuration_Hierarchy__c='002';
    update ser2;
    
    
    
    csord__Service__c ser3 = new csord__Service__c();
    ser3.Product_Id__c = 'NID';     
    ser3.csord__Identification__c = 'Test-Catlyne-4238362';
    ser3.csord__Order_Request__c =OrdReqList[0].id;
    ser3.csord__Subscription__c =SUBList[0].id;
    ser3.Billing_Commencement_Date__c = System.Today();
    ser3.Stop_Billing_Date__c = System.Today();
    ser3.Customer_Required_Date__c = System.Today();        
    ser3.solutions__c=sol[0].id;
    ser3.primary_port_service__c=ser2.id;
    ser3.csord__order__c=ord3.id;
    ser3.Root_Service_ID__c=ser.id;
    ser3.Product_Configuration_Hierarchy__c='001.002';
    ser3.Service_Type__c='SE';
    ser3.Product_Code__c='NID';
    ser3.Selling_Entity__c='ABC';   
    ser3.Estimated_Start_Date__c=System.Today();    
    ser3.Order_Type_Final__c='New Provide'; 
    ser3.A_City_Code__c='other1';
    ser3.Z_City_Code__c='other1';
    ser3.A_Side_Site__c='other1';
    ser3.Z_Side_Site__c='other';    
    ser3.Cease_Service_Flag__c=true;
    ser3.AccountId__c=accList[0].Id;
    upsert ser3;    
    
    system.assertEquals(ser3.Cease_Service_Flag__c,true);
    system.assertEquals(ser3.Order_Type_Final__c,'New Provide');    
    system.assertEquals(ser3.Customer_Required_Date__c!=null,true);
    system.assertEquals(ser3.Service_Type__c!=null,true);
    system.assertEquals(ser3.Product_Code__c!=null,true);
    system.assertEquals(ser3.Selling_Entity__c!=null,true);
    system.assertEquals(ser3.Estimated_Start_Date__c!=null,true);
    //system.assertEquals(ser3.Product_Configuration_Type__c,'ABCD');
    //system.assertEquals(ser3.region__c!=null,true);   
    
    csord__Service__c ser4 = new csord__Service__c();
    ser4.Product_Id__c = 'GMNS';
    ser4.AccountId__c=accList[0].Id;    
    ser4.csord__Identification__c = 'Test-Catlyne-4238362';
    ser4.csord__Order_Request__c =OrdReqList[0].id;
    ser4.csord__Subscription__c =SUBList[0].id;
    ser4.Billing_Commencement_Date__c = System.Today();
    ser4.Stop_Billing_Date__c = System.Today();
    ser4.Customer_Required_Date__c = System.Today();        
    ser4.solutions__c=sol[0].id;
    ser4.primary_port_service__c=ser2.id;
    ser4.csord__order__c=ord4.id;
    ser4.Root_Service_ID__c=ser.id;
    ser4.Product_Configuration_Hierarchy__c='001.002';
    ser4.Service_Type__c='SE';  
    ser4.Product_Code__c='GMNS';
    ser4.Selling_Entity__c='ABC';
    ser4.Estimated_Start_Date__c=System.Today();
    insert ser4;
    
    
    List<id>orderListVal = new List<Id>{ord1.id,ord2.id,ord3.id};
    //*****Data Creation completed***/
    
       Test.starttest();               
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));
       samsolsum.custTest='true'; 
       
       samsolsum.checkBoxValueMap=new Map<Id,Boolean>();
       samsolsum.checkBoxValueMap.put(ser.id,true);
       samsolsum.checkBoxValueMap.put(ser1.id,true);
       samsolsum.checkBoxValueMap.put(ser2.id,true);
       samsolsum.checkBoxValueMap.put(ser3.id,true);
       samsolsum.checkBoxValueMap.put(ser4.id,true);           
       samsolsum.saveAction();
       samsolsum.BulkSubmit();
       samsolsum.servList=new List<csord__service__c>();
       samsolsum.servList.add(ser2);
       samsolsum.servList.add(ser3);
       samsolsum.servList.add(ser4);
       samsolsum.MasterOrderSet=new Set<Id>{ord1.id,ord2.id,ord3.id};
       Map<Id,String>OrderNumberMap=new Map<Id,String>();
       OrderNumberMap.put(ord1.id,'111');
       OrderNumberMap.put(ord2.id,'222');
       OrderNumberMap.put(ord3.id,'333');
       OrderNumberMap.put(ord4.id,'444');          
       samsolsum.MasterChildRelationshipCheck(samsolsum.MasterOrderSet,OrderNumberMap);
       
       Test.stoptest();
    
    
   
}

@isTest static void TestMethod3(){
    
   //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
   List<Site__c>siteList= P2A_TestFactoryCls.getsites(1,accList,countrylist);
   siteList[0].Customer_Specific_Site_Code__c='abcd';
   update siteList;
   
   
   
   
   //Create Order and it's orchestration
    CSPOFA__Orchestration_Process_Template__c ot1=new CSPOFA__Orchestration_Process_Template__c(name='order_new');
    insert ot1;
    CSPOFA__Orchestration_Process_Template__c ot2=new CSPOFA__Orchestration_Process_Template__c(name='servicenew');
    insert ot2;            
    
    //List of Orders
    List<csord__Order__c>Orderlist=new List<csord__Order__c>();
    
    //*****creation of Orders*******//
    //first Order
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    //ord1.Max_CRD__c=System.Today();
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;
    
    Set<ID> OrderId = new Set<Id>();
    OrderId.add(Ord1.id);
    
    
    
    //*****creation of Service*******//
    csord__Service__c ser = new csord__Service__c();
    ser.AccountId__c=accList[0].id;
    ser.Product_Id__c = 'IPVPN'; 
    ser.csord__Identification__c = 'Test-Catlyne-4238362';
    ser.csord__Order_Request__c =OrdReqList[0].id;
    ser.csord__Subscription__c =SUBList[0].id;
    ser.Billing_Commencement_Date__c = System.Today();
    ser.Stop_Billing_Date__c = System.Today();
    ser.Customer_Required_Date__c = System.Today();     
    ser.solutions__c=sol[0].id;
    ser.Service_Type__c='SE';
    ser.Product_Code__c='IPVPN PORT';
    ser.Selling_Entity__c='ABC';    
    ser.Estimated_Start_Date__c=System.Today();
    ser.A_City_Code__c='aaa';
    ser.Z_City_Code__c='zzz';       
    insert ser;
    ser.primary_port_service__c=null;
    ser.csord__order__c=ord1.id;
    ser.Root_Service_ID__c=ser.id;
    ser.Product_Configuration_Hierarchy__c='001';
    ser.Acity_Side__c=siteList[0].id;
    ser.Zcity_Side__c=siteList[0].id;
    update ser; 
    
    System.assertEquals('abcd',siteList[0].Customer_Specific_Site_Code__c);
    System.assertEquals(siteList[0].id,ser.Acity_Side__c);
    System.assertEquals(siteList[0].id,ser.Zcity_Side__c);
    System.assertEquals('abcd',siteList[0].Customer_Specific_Site_Code__c);
    //System.assertEquals('abcd',ser.A_Side_Site__c);
    //System.assertEquals('abcd',ser.Z_Side_Site__c);
    
       Test.starttest();               
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));       
       
       samsolsum.checkBoxValueMap=new Map<Id,Boolean>();
       samsolsum.checkBoxValueMap.put(ser.id,true);                
       samsolsum.validateCCMS_SNOWResponse(OrderId);
       samsolsum.executeParallelOrderSubmission();
       samsolsum.BulkSubmit();
       samsolsum.identifyParallelOrder(OrderId);
       samsolsum.monitorBatchJobExecution();
       
       Test.stoptest();
    
    
   
}
  @isTest static void BulkSubmitNonParallelOrder(){
    
   //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);   
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
   List<Site__c>siteList= P2A_TestFactoryCls.getsites(1,accList,countrylist);
   siteList[0].Customer_Specific_Site_Code__c='abcd';
   update siteList;
   
   
   
   
   //Create Order and it's orchestration
    CSPOFA__Orchestration_Process_Template__c ot1=new CSPOFA__Orchestration_Process_Template__c(name='order_new');
    insert ot1;
    CSPOFA__Orchestration_Process_Template__c ot2=new CSPOFA__Orchestration_Process_Template__c(name='servicenew');
    insert ot2;            
    
    //List of Orders
    List<csord__Order__c>Orderlist=new List<csord__Order__c>();
    
    //*****creation of Orders*******//
    //first Order
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    ord1.solutions__c=sol[0].id;
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;
    
    
    
    Set<ID> OrderId = new Set<Id>();
    OrderId.add(Ord1.id);
    
    
    
    
    //*****creation of Service*******//
    csord__Service__c ser = new csord__Service__c();
    ser.AccountId__c=accList[0].id;
    ser.Product_Id__c = 'IPVPN'; 
    ser.csord__Identification__c = 'Test-Catlyne-4238362';
    ser.csord__Order_Request__c =OrdReqList[0].id;
    ser.csord__Subscription__c =SUBList[0].id;
    ser.Billing_Commencement_Date__c = System.Today();
    ser.Stop_Billing_Date__c = System.Today();
    ser.Customer_Required_Date__c = System.Today();     
    ser.solutions__c=sol[0].id;
    ser.Service_Type__c='SE';
    ser.Product_Code__c='NID';
    ser.Selling_Entity__c='ABC';    
    ser.Estimated_Start_Date__c=System.Today();
    ser.A_City_Code__c='aaa';
    ser.Z_City_Code__c='zzz';       
    insert ser;
    ser.primary_port_service__c=null;
    ser.csord__order__c=ord1.id;
    ser.Root_Service_ID__c=ser.id;
    ser.Product_Configuration_Hierarchy__c='001';
    ser.Acity_Side__c=siteList[0].id;
    ser.Zcity_Side__c=siteList[0].id;
    ser.Primary_Service_ID__c ='';
    ser.ServiceItemNumber__c=0;
    update ser; 
    
    
    
    csord__service__c checkSer=[select Site_A_Code__c,Site_B_Code__c,A_Side_Site__c,Z_Side_Site__c from csord__Service__c where Id=:ser.id];
    System.assertEquals('abcd',siteList[0].Customer_Specific_Site_Code__c);
    System.assertEquals(siteList[0].id,ser.Acity_Side__c);
    System.assertEquals(siteList[0].id,ser.Zcity_Side__c);
    System.assertEquals('abcd',siteList[0].Customer_Specific_Site_Code__c);
    //System.assertNotEquals(checkSer.A_City_Generic_Code__c,null);
    //System.assertNotEquals(checkSer.Z_City_Generic_Code__c,null);
    System.assert(checkSer.Site_A_Code__c!=null && checkSer.Site_A_Code__c!='other');
    System.assert(checkSer.Site_B_Code__c!=null && checkSer.Site_B_Code__c!='other');
    System.assert(checkSer.A_Side_Site__c!=null && checkSer.A_Side_Site__c!='other');
    System.assert(checkSer.Z_Side_Site__c!=null && checkSer.Z_Side_Site__c!='other');
    //System.assertEquals('abcd',ser.A_Side_Site__c);
    //System.assertEquals('abcd',ser.Z_Side_Site__c);
    
       Test.starttest();               
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));       
       
       samsolsum.checkBoxValueMap=new Map<Id,Boolean>();
       samsolsum.checkBoxValueMap.put(ser.id,true);
           
       samsolsum.BulkSubmit();
       
       
       Test.stoptest(); 
 
}

@isTest static void MonitorBatchJobExecutionTest(){
    //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);   
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    ord1.solutions__c=sol[0].id;
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;    
    
    
    List<id>orderListVal = new List<Id>{ord1.id};
    set<Id>NonParallelOrderSet=new Set<Id>{ord1.id};
       Test.starttest(); 
       
       
        
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));  

       
       SubmitOrderBatch bc = new SubmitOrderBatch(orderListVal, NonParallelOrderSet, false, false);
       Database.executeBatch(bc,1); 
           

       List<AsyncApexJob> Alljobs = [Select Id, Status, JobItemsProcessed, NumberOfErrors, TotalJobItems From AsyncApexJob limit 100];     
       samsolsum.batchJobIdSet=new set<id>();
       id abortableJobId;
       for(AsyncApexJob j:Alljobs){
         samsolsum.batchJobIdSet.add(j.id); 
         if(j.status!='Completed'){abortableJobId=j.id;}         
       }
        samsolsum.MonitorBatchJobExecution();
        samsolsum.BatchProcessCompleted=false;
        samsolsum.BatchComplete=1;
        pagereference pg2=samsolsum.checkBatchStatus();
        samsolsum.bulkUpdate=true;
        pagereference pg3=samsolsum.checkBatchStatus();
        
        Boolean flag=samsolsum.executeParallelOrderSubmission();
        
        if(abortableJobId!=null){
        System.abortJob(abortableJobId);
        
        samsolsum.batchJobIdSet=new set<id>{abortableJobId};
        samsolsum.MonitorBatchJobExecution();
        pagereference pg4=samsolsum.checkBatchStatus();
        
        }
 
       
      
       Test.stoptest(); 
}

@isTest static void CheckBatchStatusTest(){
    P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);   
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    ord1.solutions__c=sol[0].id;
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;    
    
    
        List<id>orderListVal = new List<Id>{ord1.id};
        set<Id>NonParallelOrderSet=new Set<Id>{ord1.id};
       
       Test.starttest(); 
       List<AsyncApexJob> Alljobs = [Select Id, Status, JobItemsProcessed, NumberOfErrors, TotalJobItems From AsyncApexJob limit 1];     
       
       
       sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));  
     
       samsolsum.BatchProcessCompleted=false;
       samsolsum.MasterOrderSet=new set<Id>{ord1.id};
       samsolsum.BatchComplete=1;
       samsolsum.batchJobIdSet=new set<id>{Alljobs[0].id};
       samsolsum.SubmitibleOrderMap=new Map<Integer,set<Id>>();
       samsolsum.SubmitibleOrderMap.put(0,new set<Id>{ord1.id});
       samsolsum.SubmitibleOrderMap.put(1,new set<Id>{ord1.id});
       samsolsum.SubmitibleOrderMap.put(2,new set<Id>{ord1.id});
       samsolsum.SubmitibleOrderMap.put(3,new set<Id>{ord1.id});
       samsolsum.orderListVal=new List<Id>{ord1.id};
       samsolsum.NonParallelOrderSet=new set<Id>{ord1.id};
       //samsolsum.CurrentFailedOrderSet=new set<Id>{ord1.id};
       //samsolsum.CurrentUnsuccessfulOrderSet=new set<Id>{ord1.id};
       samsolsum.OverAllFailedOrderSet=new set<Id>();
       
        
       samsolsum.currentlevel=0;
       samsolsum.level=2;
       pagereference pg1=samsolsum.checkBatchStatus();
       
       //samsolsum.currentlevel=2;
       //samsolsum.level=3;
       //pagereference pg2=samsolsum.checkBatchStatus();
       
       Test.stoptest(); 
}

@isTest static void ExecuteParallelOrderSubmissionTest(){
    //Creation of Data   
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);   
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    ord1.solutions__c=sol[0].id;
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;    
    
    
    Test.starttest();
    sampleSolutionSummary samsolsum = new sampleSolutionSummary(new ApexPages.StandardController(sol[0])); 
    samsolsum.orderListVal = new List<Id>{ord1.id};
    samsolsum.NonParallelOrderSet=new Set<Id>{ord1.id};
    samsolsum.ParallelOrderSet=new Set<Id>{ord1.id};
    samsolsum.NonParallelSubmissionStarted=false;
        
       
    Boolean flag1=samsolsum.executeParallelOrderSubmission();  
    
    samsolsum.NonParallelSubmissionStarted=true;
    samsolsum.allOrderscompleted=true;
    Boolean flag2=samsolsum.executeParallelOrderSubmission();
       
      
       Test.stoptest(); 
}



   @isTest static void testExceptions(){
   P2A_TestFactoryCls.sampletestdata();
   List<Account> accList = P2A_TestFactoryCls.getAccounts(1); 
   accList[0].Region__c='region1';
   update accList;    
   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
   List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
   List<solutions__c> sol = P2A_TestFactoryCls.getsolutions(1,acclist);
   List<user>userList=P2A_TestFactoryCls.get_Users(1);
   List<Country_Lookup__c> countrylist=P2A_TestFactoryCls.getcountry(1);
   List<Site__c>siteList= P2A_TestFactoryCls.getsites(1,accList,countrylist);
   siteList[0].Customer_Specific_Site_Code__c='abcd';
   update siteList;
   
   
   
   
   //Create Order and it's orchestration
    CSPOFA__Orchestration_Process_Template__c ot1=new CSPOFA__Orchestration_Process_Template__c(name='order_new');
    insert ot1;
    CSPOFA__Orchestration_Process_Template__c ot2=new CSPOFA__Orchestration_Process_Template__c(name='servicenew');
    insert ot2;            
    
    //List of Orders
    List<csord__Order__c>Orderlist=new List<csord__Order__c>();
    
    //*****creation of Orders*******//
    //first Order
    csord__Order__c ord1 = new csord__Order__c();
    ord1.csord__Identification__c = 'Test-JohnSnow-4238362';    
    ord1.csord__Order_Request__c =OrdReqList[0].id;
    ord1.Order_Submitted_to_SD__c=true;
    ord1.Subscription__c=SUBList[0].id; 
    //ord1.Max_CRD__c=System.Today();
    ord1.SD_PM_Contact__c=userList[0].id;   
    insert ord1;
    
    Set<ID> OrderId = new Set<Id>();
    OrderId.add(Ord1.id);
    
    
    
    //*****creation of Service*******//
    csord__Service__c ser = new csord__Service__c();
    ser.AccountId__c=accList[0].id;
    ser.Product_Id__c = 'IPVPN'; 
    ser.csord__Identification__c = 'Test-Catlyne-4238362';
    ser.csord__Order_Request__c =OrdReqList[0].id;
    ser.csord__Subscription__c =SUBList[0].id;
    ser.Billing_Commencement_Date__c = System.Today();
    ser.Stop_Billing_Date__c = System.Today();
    ser.Customer_Required_Date__c = System.Today();     
    ser.solutions__c=sol[0].id;
    ser.Service_Type__c='SE';
    ser.Product_Code__c='IPVPN PORT';
    ser.Selling_Entity__c='ABC';    
    ser.Estimated_Start_Date__c=System.Today();
    ser.A_City_Code__c='aaa';
    ser.Z_City_Code__c='zzz';       
    insert ser;
    ser.primary_port_service__c=null;
    ser.csord__order__c=ord1.id;
    ser.Root_Service_ID__c=ser.id;
    ser.Product_Configuration_Hierarchy__c='001';
    ser.Acity_Side__c=siteList[0].id;
    ser.Zcity_Side__c=siteList[0].id;
    update ser; 
    
         sampleSolutionSummary alls =new sampleSolutionSummary(new ApexPages.StandardController(sol[0]));
        
         try{alls.modifySelectAllCheckboxValue();}catch(Exception e){}
         try{alls.expand();}catch(Exception e){}
          try{alls.getPaginationRecords(null);}catch(Exception e){}
           try{alls.removeExtraComma('a');}catch(Exception e){}
           try{pagereference pg=alls.validationMethod(null);}catch(Exception e){}
        
          system.assertEquals(true,alls!=null);    
         
         
     }
   
 
}