public with sharing class ScreenFlowUtilities {
    public static void updateScreenFlowField(List<cscfga__Product_Configuration__c> productConfigs){
        for(cscfga__Product_Configuration__c item : productConfigs){
            item.cscfga__Screen_Flow__c = null;
        }
    }
}