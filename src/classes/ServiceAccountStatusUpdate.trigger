trigger ServiceAccountStatusUpdate on Service__c (after insert, after update) { 
  /*  Set<Id> accountIds = new Set<Id>(); 
    Map<Id,List<Service__c>> accountService = new Map<Id,List<Service__c>>();// Map which stores Accounts with List of its Services
    Set<Id> accounIdtoUpdateforNewService = new Set<Id>();
    Set<id> accountIdtoUpdateforTerminatedService = new Set<Id>();
    for (Service__c ser : Trigger.new)
    {
        // Get all the Service Account Names and Store them Along with Service Id       
        accountIds.add(ser.AccountId__c);
    }   
    List<Service__c> allServicestoAccount = [Select Id,Name,Status__c,AccountId__c from Service__c where AccountId__c in : accountIds];
    for (Service__c serv: allServicestoAccount){
        if (accountService.keySet().contains(serv.AccountId__c)){
            accountService.get(serv.AccountId__c).add(serv);
        }else{
            List<Service__c> serv2 = new List<Service__c>();
            serv2.add(serv);
            accountService.put(serv.AccountId__c,serv2);
        }
    }
    
if (Trigger.isInsert){
    for (Service__c ser: Trigger.new){
        if (ser.Status__c=='Active'){
            if (accountService.get(ser.AccountId__c)!=null){
                //System.debug('The Size of the '+accountService.get(ser.AccountId__c).size());
                if (accountService.get(ser.AccountId__c).size()==1){
                    accounIdtoUpdateforNewService.add(ser.AccountId__c);    
                }
            }else{
                accounIdtoUpdateforNewService.add(ser.AccountId__c);
            }
        }
    }
}
if(Trigger.isUpdate){
    for (Service__c ser: Trigger.new){
    if (ser.Status__c=='Active')
    {
            if (accountService.get(ser.AccountId__c)!=null)
            {
                if (accountService.get(ser.AccountId__c).size()==2)
                {
                    accounIdtoUpdateforNewService.add(ser.AccountId__c);    
                }
            }
    }
    // Check if All the services are made Cancelled or Terminated
    if (ser.Status__c=='Cancelled'||ser.Status__c=='Terminated'){
        if(checkAllServiceTerminated(accountService.get(ser.AccountId__c),ser.Id)){
            accountIdtoUpdateforTerminatedService.add(ser.AccountId__c);
        }
    }
        
}
}
boolean checkAllServiceTerminated(List<Service__c> services,Id currServiceId){
    boolean status = false;
    for (Service__c serv: services){
        //if (serv.Id!= currServiceId){
            if (serv.Status__c=='Cancelled'||serv.Status__c=='Terminated'){
                status = true;
            }else{
                status=false;
                break;
            }
        //}
    }
    return status;
}
    List<Account> newServiceUpdateList = [select Id,Name,type,BillingCountry from Account where Id in : accounIdtoUpdateforNewService];
    for (Account acc : newServiceUpdateList){
        acc.type='Customer';
        acc.BillingCountry ='US';
    }
    update newServiceUpdateList;
    
    List<Account> terminatedServiceUpdateList = [select Id,Name,type from Account where Id in : accountIdtoUpdateforTerminatedService];
    for (Account acc : terminatedServiceUpdateList){
        acc.type='Former Customer';
        acc.BillingCountry ='US';
    }
    
    update terminatedServiceUpdateList;*/
}