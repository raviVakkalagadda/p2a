/**
    @author - Accenture
    @date - 18-April-2012
    @version - 1.0
    @description - This is the class to generate the Service delivery XML from Order
*/
global public class ServiceDelivery {
     /* NC comment
     webservice public static OrderClass getOrderDetails(String ordrId) {
        system.debug('*********** '+ordrId);
       
        // Creating the OrderClass Object
        OrderClass ordrObj = new OrderClass();
        
        // Creating OrderLineItemClass List Object
        List<OrderLineItemClass> retXML = new List<OrderLineItemClass>();
        
        // query to get Order fields Object based on request Order ID
        String odrQuery = 'select ' + System.Label.SD_Order_Query+ ' from Order__c where id = '+'\''+ordrId+'\'';
        System.debug('Qryyyyyyyyyyyyyyyyy = '+odrQuery);
        Order__c o = Database.query(odrQuery);// database query to get the Order fields
        ordrObj.order = o;
        
        // query Order Line Item fields
        String oliQuery = 'select ' + System.Label.SD_Order_Line_Item_Query+ System.Label.SD_Order_Line_Item_Query1 + System.Label.SD_Order_Line_Item_Query2 + System.Label.GIAAS_Order_Line_Item_Query + System.Label.SD_Order_Line_Item_Query_3 +System.Label.SD_Order_Line_Item_Query4 + System.Label.SD_Order_Line_Item_Query5 + System.Label.SD_Order_Line_Item_Query6 +' From Order_Line_Item__c where ParentOrder__c = '+'\''+ordrId+'\' order by CPQItem__c';
        System.debug('Qryyyyyyyyyyyyyyyyy = '+oliQuery);
        
        List<Order_Line_Item__c> oliLst = Database.query(oliQuery);// database query to get the opportunity fields
        // Creating a set for Order_Line_Item object to store IDs
        List<OPPOLISortingWrapper> OppolWrap = new List<OPPOLISortingWrapper>();       
        Set<ID> oliIdSet = new Set<ID>();       
        for(Order_Line_Item__c oli : oliLst){
            OPPOLITreeSorting sor = new OPPOLITreeSorting(oli.CPQItem__c, oli);
            OPPOLISortingWrapper  wrap = new OPPOLISortingWrapper(sor); 
            OppolWrap.add(wrap);
           }    
       OppolWrap.sort();
        
        // Adding the Order_Line_Item object IDs into the set
        for(OPPOLISortingWrapper wrap: OppolWrap ){
            OPPOLITreeSorting  sor = wrap.pac;
            Order_Line_Item__c oli = sor.oOLI;
            oliIdSet.add(oli.id);
        }
        String oliIDStr = '';
        for(ID oliID : oliIdSet)
            oliIDStr+=',\''+oliID+'\'';
            
        // query Product Configuration fields
        String pcQuery = 'select ' + System.Label.SD_Product_Configuration_Query+System.Label.SD_Product_Configuration3+' From Product_Configuration__c where Order_Line_Item__c in ('+oliIDStr.substring(1)+') order by CPQItem__c';
        System.debug(' pcQuery Qryyyyyyyyyyyyyyyyy = '+pcQuery);
        List<Product_Configuration__c> pcLst = Database.query(pcQuery);
        // Map to store CPQItem and Product_Configuration object mapping
        Map<String, Product_Configuration__c> pcMap = new Map<String, Product_Configuration__c>();
        
        // Adding the values Product_Configuration__c object into the map
        for(Product_Configuration__c pc : pcLst)
            pcMap.put(pc.CPQItem__c, pc);
        
        //Added as part of the CR-321
        //Call new sorting wrapper class and sort this list view array
       /* List<SortingWrapper> SortedList = new List<SortingWrapper>();
        for(Order_Line_Item__c soli: oliLst){
            SortedList.add(new SortingWrapper(new TreeSorting(soli.CPQItem__c,soli)));
        }
        SortedList.sort();*/
        
        /*NC comment
        // Map to store OrderLineItemClass object 
        Map<String, OrderLineItemClass> oliLevlMap = new Map<String, OrderLineItemClass>();
        boolean productFlag;
       // for(SortingWrapper olisort : SortedList){
            // TreeSorting p = olisort.pac;
         for(OPPOLISortingWrapper wrap: OppolWrap ){
            OPPOLITreeSorting  sor = wrap.pac;
            Order_Line_Item__c oli = sor.oOLI;   
             Order_Line_Item__c newoli = new Order_Line_Item__c();
             newoli = oli;
            productFlag = true;
            OrderLineItemClass ordrLinItm = new OrderLineItemClass();
            ordrLinItm.LineItemDetails = oli;
            ordrLinItm.Configuration = pcMap.get(oli.CPQItem__c);
            
            ordrLinItm.OrderLineItemComponents = new List<OrderLineItemClass>();
            if(oli.ParentCPQItem__c!=null && oli.ParentCPQItem__c!=''){
                productFlag = false;
                //System.debug('!!!!!!!!!!!!!!! '+oli);
                if(oliLevlMap.containsKey(oli.ParentCPQItem__c)){
                    //System.debug('!!!!!!!!!!!!!!! key is thr');
                    OrderLineItemClass parntOli = oliLevlMap.get(oli.ParentCPQItem__c);
                    List<OrderLineItemClass> parntOlilst = parntOli.OrderLineItemComponents;
                    parntOlilst.add(ordrLinItm);
                }
            }
               oliLevlMap.put(oli.CPQItem__c,ordrLinItm) ;
               if(productFlag)
                retXML.add(ordrLinItm);
            }
        ordrObj.orderLineItems = retXML;
        return ordrObj;
     }
     
     
    global class OrderLineItemClass{
     webservice List<OrderLineItemClass> OrderLineItemComponents {get;set;}
     webservice Order_Line_Item__c  LineItemDetails {get;set;}
     webservice Product_Configuration__c  Configuration {get;set;}
     }
     
     global class OrderClass{
        webservice Order__c  order {get;set;}
        webservice List<OrderLineItemClass>  orderLineItems {get;set;}
     }
    NC Comment*/
}