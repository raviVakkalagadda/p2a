@istest (seealldata = false)
public class SetPricingStatusTest{
    
   /* private static List<csord__Subscription__c> subscriptionList = new List<csord__Subscription__c>(); 
    private static List<csord__Order_Request__c> orderReqList = new List<csord__Order_Request__c>();
    private static cscfga__Product_Basket__c basket = null;
    private static List<cscfga__Product_Configuration__c> prodConfigs = null;*/
    /**
* Disables triggers, validations and workflows for the given user
* @param userId Id
*/
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);
        
        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        
        upsert globalMute;
    }
    
    /**
* Enables triggers, validations and workflows 
* @param userId Id
*/
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);
        
        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        
        upsert globalMute;
    }
    
    public static Map<Id,cscfga__Product_Basket__c> prodBaskMap = new Map<Id,cscfga__Product_Basket__c>();
    public static Map<id,List<cscfga__Product_Configuration__c>> productmap = new Map<id,List<cscfga__Product_Configuration__c>>();
    public static Map<Id,cscfga__Product_Basket__c> oldproduct = new Map<Id,cscfga__Product_Basket__c>();
    public static Map<Id,cscfga__Product_Basket__c> newproduct = new Map<Id,cscfga__Product_Basket__c>();
    public static   List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
    
    public static   List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1,accList);
    public static   list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
    public static   List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
    public static  List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
    public static   List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
     
    public static   List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
    public static  List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
    public static   List<User> users = P2A_TestFactoryCls.get_Users(1);
    public static  List<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
   // public static  List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList, proconfigs);
     
    
    public static testmethod void setPricingStatusTestmethod() {
        P2A_TestFactoryCls.disableAll(UserInfo.getUserId()); 
        P2A_TestFactoryCls.sampleTestData();
        
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<Pricing_Approval_Request_Data__c> prd =  P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,proconfigs);
        cscfga__Product_Basket__c pb1 = new cscfga__Product_Basket__c();
        pb1.csordtelcoa__Synchronised_with_Opportunity__c = true;
        pb1.Name = 'Test basket';
        pb1.csbb__Account__c= acclist[0].id;
        pb1.csordtelcoa__Basket_Stage__c = 'Prospecting';
        pb1.csordtelcoa__Order_Generation_Batch_Job_Id__c = '';
        //pb1.Account_Customer_Type__c = 'ISO';
        insert pb1;
        system.assert(pb1!=null);
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        acclist[0].Customer_Type_New__c= 'ISO';
        upsert acclist;
        Map<ID,ID> ActionOppMapping = new Map<ID,ID>();
        Map<id,Pricing_Approval_Request_Data__c> approval = new Map<id,Pricing_Approval_Request_Data__c>();
        
        for(cscfga__Product_Basket__c prodBaskObj : prodBaskList) {
            prodBaskMap.put(prodBaskObj.Id,prodBaskObj);
        }
        
        cscfga__Product_Basket__c oldpb = new cscfga__Product_Basket__c ();
        oldpb.cscfga__Opportunity__c = oppList.get(0).id;
        oldpb.csordtelcoa__Synchronised_with_Opportunity__c =true;
        oldpb.csbb__Synchronised_With_Opportunity__c =true;
        oldpb.Quote_Status__c ='Rejected'; 
        insert oldpb; 
        system.assert(oldpb!=null);
        
        cscfga__Product_Basket__c newpb = new cscfga__Product_Basket__c ();
        newpb.cscfga__Opportunity__c = oppList.get(0).id;
        newpb.csordtelcoa__Synchronised_with_Opportunity__c =true;
        newpb.csbb__Synchronised_With_Opportunity__c =true;
        newpb.Quote_Status__c ='Pending Pricing Approval'; 
        insert newpb ;
        system.assert(newpb!=null);
        newpb.Quote_Status__c ='Draft';
        upsert newpb;
        system.assert(newpb!=null);
        
        
        
        
          List<Pricing_Approval_Request_Data__c> ListPricingApproval = new  List<Pricing_Approval_Request_Data__c>();
Pricing_Approval_Request_Data__c PricingApprovalData = new Pricing_Approval_Request_Data__c();
PricingApprovalData.Product_Configuration__c = caselist[0].Product_Configuration__c;
PricingApprovalData.Rate_RC__c=0.00;
PricingApprovalData.Rate_NRC__c=0.00;
PricingApprovalData.Offer_RC__c=0.00;
PricingApprovalData.Offer_NRC__c=0.00;
PricingApprovalData.Approved_RC__c=120.00;
PricingApprovalData.Approved_NRC__c=50.00;
PricingApprovalData.Cost_RC__c=100.00;
PricingApprovalData.Cost_NRC__c=0.00;
PricingApprovalData.Contract_Term__c=12;
//PricingApprovalData.Margin__c=41.18;
PricingApprovalData.CurrencyIsoCode= 'USD';   
PricingApprovalData.Product_Information__c = 'PEN, 12, AUS SITE';
ListPricingApproval.add(PricingApprovalData);
Insert ListPricingApproval; 
        
        
        Map<Id,List<cscfga__Product_Configuration__c>> productmap1 = new Map<Id,List<cscfga__Product_Configuration__c>>();
        productmap1.put(proconfigs[0].id, proconfigs); 
        
        productmap.put(proconfigs[0].Id, proconfigs);
        
        Map<Id,cscfga__Product_Basket__c> newBasketsMap = new Map<Id,cscfga__Product_Basket__c>();
        newBasketsMap.put(newpb.id, newpb);
        
        
        Map<Id,cscfga__Product_Basket__c> oldBasketsMap = new Map<Id,cscfga__Product_Basket__c>();
        newBasketsMap.put(oldpb.id, oldpb);
        
        Map<id,id>newPC = new Map<id,id>();
        newPC.put(oldpb.id, oldpb.id);
        
        Map<id,Pricing_Approval_Request_Data__c> pardDataMap = new Map<id,Pricing_Approval_Request_Data__c>();
        pardDataMap.put(PricingApprovalData.id, PricingApprovalData );
          Set<Id> openOpportunityBaskets = new Set<Id>();
          for(cscfga__Product_Basket__c b:prodBaskList )
          {
          openOpportunityBaskets.add(prodBaskList[0].id);
          }
     
     Map<id,List<cscfga__Product_Configuration__c>> productcon = new Map<id,List<cscfga__Product_Configuration__c>>();
        setPricingStatus setprice = new setPricingStatus();
        Test.starttest();
        try{
            setPricingStatus.getpricingStatus(prodBaskMap,prodBaskMap,null,true);
            setPricingStatus.getpricingStatus(prodBaskMap,null,proconfigs,false);
            //setPricingStatus.isPendingPricingRequiredcr(productcon,pb1,newproduct,oldproduct,ActionOppMapping,approval);
            setPricingStatus.isQuoteAcceptedISO(productcon,pb1,newproduct,oldproduct);
            //setPricingStatus.isSupplierQuoteRequired(productcon,pb1);
            setPricingStatus.isIncomplete(productcon,newpb);
            
            setPricingStatus.isQuoteRejected(productcon,newpb,newBasketsMap);
            setPricingStatus.isQuoteAccepted(productcon,newpb,newBasketsMap,oldBasketsMap);
            setPricingStatus.isPendingPricingRequired(productcon,newpb);
            
           
        }catch(exception e)
        {
         ErrorHandlerException.ExecutingClassName='setPricingStatusTest:setPricingStatusTestmethod';         
         ErrorHandlerException.sendException(e); 
        }
        Test.stopTest(); 
    }
    @istest
    public static void testisQuoteAcceptedISO(){
        
        P2A_TestFactoryCls.sampleTestData();
       
      
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       accList[0].Customer_Type_New__c = 'ISO';
       update accList;
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        products[0].Quote_Status__c = 'Accepted';
        update Products ; 
        products[0].Quote_Status__c = 'Approved';
        update Products ; 
        products[0].CountOfProductConfiguration__c = 23;
        update Products ;
    
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        
        
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
       // List<cscfga__Product_Configuration__c> prodconfig = P2A_TestFactoryCls.getProductonfig(1,Products,Prodef,pbundlelist,Offerlists);
    
    List<cscfga__Product_Configuration__c>   ProductConfiglist1 = new List<cscfga__Product_Configuration__c>();
    cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
        pb.cscfga__Product_basket__c = Products[0].id ;
        pb.cscfga__Product_Definition__c = Prodef[0].id ;
        pb.name ='IPL';
        pb.cscfga__contract_term_period__c = 12;
        pb.Is_Offnet__c = 'yes';
        pb.cscfga_Offer_Price_MRC__c = 200;
        pb.cscfga_Offer_Price_NRC__c = 300;
        pb.cscfga__Product_Bundle__c = pbundlelist[0].id;
        pb.Rate_Card_NRC__c = 200;
        pb.Rate_Card_RC__c = 300;
        pb.cscfga__total_contract_value__c = 1000;
        pb.cscfga__Contract_Term__c = 12;
        pb.CurrencyIsoCode = 'USD';
        pb.cscfga_Offer_Price_MRC__c = 100;
        pb.cscfga_Offer_Price_NRC__c = 100;
        pb.Child_COst__c = 100;
        pb.Cost_NRC__c = 100;
        pb.Cost_MRC__c = 100;
        pb.Product_Name__c = 'test product';
        pb.Added_Ports__c = null ;
        Pb.cscfga__Product_Family__c = 'Point to Point';
        
        ProductConfiglist1.add(pb);
        insert ProductConfiglist1;
        
        Set<Id> openOpportunityBaskets = new Set<Id>();
        for(cscfga__Product_Basket__c prodbaset:Products)
        {
          openOpportunityBaskets.add(Products[0].id);
        }
        
     
      
        Map<Id, List<cscfga__Product_Configuration__c>> productConfigByBasketMap = new  Map<Id, List<cscfga__Product_Configuration__c>>();
        for(cscfga__Product_Configuration__c pcc : ProductConfiglist1)
        {
          //productConfigByBasketMap.put(ProductConfiglist1[0].id,ProductConfiglist1[0]);
          productConfigByBasketMap.put(pb.cscfga__Product_basket__c = Products[0].id,ProductConfiglist1);
           
        }
        
        Map<Id,cscfga__Product_Basket__c> newBasketsMap = new  Map<Id,cscfga__Product_Basket__c>();
        for(cscfga__Product_Basket__c pbcc : Products )
        {
          newBasketsMap.put(Products[0].id,Products[0]); 
        }
        Map<Id, cscfga__Product_Basket__c> oldBasketsMap = new Map<Id, cscfga__Product_Basket__c>();
        
        for(cscfga__Product_Basket__c pcdd : Products)
        {
          oldBasketsMap.put(Products[0].id,Products[0]); 
        }
        
      Test.starttest();
        Boolean isQuoteAccepted = True;
        boolean setQuoteStatus = True;
        setPricingStatus setprice = new setPricingStatus();
        setPricingStatus.isQuoteAcceptedISO(productConfigByBasketMap,Products[0],newBasketsMap,oldBasketsMap);
        setPricingStatus.getpricingStatus(newBasketsMap,oldBasketsMap,ProductConfiglist1,setQuoteStatus );
        Test.stoptest();
    }
    
     
}