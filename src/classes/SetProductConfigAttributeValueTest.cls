@isTest(SeeAllData= false)
Public class SetProductConfigAttributeValueTest{
    @testVisible
    Public static testMethod void SetProductConfigAttributeValueTestMethd() {
        
        P2A_TestFactoryCls.sampleTestData();
        
        string status= 'True';
        //List<custom_object_to_product_configuration__c> mcs = custom_object_to_product_configuration__c.getall().values();
        
        custom_object_to_product_configuration__c mcs1 = new custom_object_to_product_configuration__c(Name='GCPE Supplier Name',Production_Configuration_Attribute_Name__c='Supplier_Quote_name_shd__c',Product_Name__c='GCPESupplierApproval',Source_Field__c='Supplier_Formula__c',Source_Object__c='VendorQuoteDetail__c');

        insert mcs1;
        system.assertEquals(mcs1.Name, 'GCPE Supplier Name');
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__Product_Basket__c>  prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,opplist);
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlist = P2A_TestFactoryCls.getOffers(1);            
        List<cscfga__Product_Configuration__c> proconfig = P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,pbundlelist,Offerlist);
        List<cscfga__Configuration_Screen__c> configscreen = P2A_TestFactoryCls.getConfigScreen(1,ProductDeflist);
        List<cscfga__Screen_Section__c> screen = P2A_TestFactoryCls.getScreenSec(1,configscreen);
        List<cscfga__Attribute_Definition__c > Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfig ,ProductDeflist,configscreen,screen);
        List<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfig ,Attributedeflist);
        List<Supplier__c> supplierlist=P2A_TestFactoryCls.getsuppliers(1);
        List<VendorQuoteDetail__c> vendorList=P2A_TestFactoryCls.getvendorQuote(1,supplierlist);
        cscfga.API_1.ApiSession apiSession = cscfga.API_1.getApiSession();
        apiSession.setConfigurationToEdit(proconfig[0]);
        cscfga.ProductConfiguration config = apiSession.getConfiguration();  
        SetProductConfigAttributeValue spca = new SetProductConfigAttributeValue();  
         cscfga__Product_Configuration__c pb = new cscfga__Product_Configuration__c();
            
            apiSession.executeRules();
            apiSession.updateLineItems();
            apiSession.persistConfiguration();
            apiSession.close();
    
        try{
            string s = spca.SetValue(proconfig[0].id, vendorList[0].id,'GCPESupplierApproval');
        }
        catch(Exception ex){
            
            ErrorHandlerException.ExecutingClassName='SetProductConfigAttributeValueTest:SetProductConfigAttributeValueTestMethd';         
            ErrorHandlerException.sendException(ex); 
            system.debug('======s====='+Ex);
        
        }
    }
}