@isTest(SeeAllData = False )
public class SolutionNameTest{
    static testMethod void SolutionName(){
    P2A_TestFactoryCls.sampleTestData();
    List<Account> acclist=P2A_TestFactoryCls.getAccounts(1);
    List<Contact> conList=P2A_TestFactoryCls.getContact(1,acclist);
    
    Solutions__c sol=new Solutions__c();
    //sol.name='test';
    sol.Solution_Name__c='test';
    sol.Account_Name__c=acclist[0].id;
    insert sol;
    list<Solutions__c> lstsolutions= new list<Solutions__c>();    
       lstsolutions.add(sol);
    Test.startTest();
    SolutionSequenceController SolutionObj = new SolutionSequenceController();
    SolutionObj.AppendSolutionNameAccountId(lstsolutions);
    ImportReferenceData ird = new ImportReferenceData();
     ird.ReadFile();
     ird.getContentFile();
     ird.getNameFile();
    Test.stopTest();
    }
    
     private static Account getAccount(){
        Account acc = new Account();
        //Country_Lookup__c cl = getCountry();
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC';
        //acc.Country__c = cl.Id;
        acc.Selling_Entity__c = 'Telstra INC';
        acc.Activated__c= true;
        acc.Account_Id__c ='12123';
        insert acc;
        return acc;
    }
   private static Solutions__c getSolutions(){
   Solutions__c sol=new Solutions__c();
  //sol.name='test';
   //sol.Account_Name__c=acclist[0].id;
   insert sol;
   return sol;
   } 

}