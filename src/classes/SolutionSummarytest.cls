@istest
Public class SolutionSummarytest
{
  @istest
  Public static void samplesolutionsummary1()
  {
  
  Map<ID ,cscfga__Product_Basket__c> newbasketmap = new Map<ID ,cscfga__Product_Basket__c>(); 
  Map<ID ,cscfga__Product_Basket__c> oldbasketmap = new Map<ID ,cscfga__Product_Basket__c>(); 
  List<Account> acclist1 = new List<Account>();
  List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
  acclist[0].Customer_Type_New__c = 'ISO';
  acclist1.add(acclist[0]);
  upsert acclist1;
  system.assert(acclist1!=null);
  List<solutions__c> sols = P2A_TestFactoryCls.getsolutions(1,acclist);
 
  List<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
  Products[0].csordtelcoa__Account__c = acclist1[0].id;
  Products[0].Quote_Status__c = 'Approved';
  upsert Products;
  List<cscfga__Product_Basket__c> p = [Select id,Quote_Status__c from cscfga__Product_Basket__c where Quote_Status__c = 'Approved'];
  system.assertequals(products[0].Quote_Status__c, p[0].Quote_Status__c);
 
  for(cscfga__Product_Basket__c pb :Products)
  {
   newBasketMap.put(pb.id, pb);
  }
  
  SolutionSummary sol = new SolutionSummary();
  SolutionSummary.EnableDisableTrigger(false);
  sol.cascadeDetail();
  SolutionSummary.setSolutionIdForISO(newBasketMap,oldBasketMap);
  SolutionSummary.RemovePreviousReferenceToSolutionBeforeCaseClosure(sols[0].id);
  }

 @istest
 Public static void solsummary2(){
   
  List<Account> accList = P2A_TestFactoryCls.getAccounts(1);  
  List<solutions__c> sols = P2A_TestFactoryCls.getsolutions(1,acclist);
  List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
  List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
  List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
  List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
  List<User> users = P2A_TestFactoryCls.get_Users(1);
  list<case> caselist = P2A_TestFactoryCls.getcases(1,accList,opplist,prodBaskList,Orders,Users);
  system.assert(caselist!=null);
    SolutionSummary.createSolution(caselist[0].id);
    
    }
  @istest
 Public static void solsummary3(){    
    List<Account> acclist1 = new List<Account>();
  List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
  acclist[0].Customer_Type_New__c = 'ISO';
  acclist1.add(acclist[0]);
  upsert acclist1;
  List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
  List<solutions__c> sols = P2A_TestFactoryCls.getsolutions(1,acclist);
  List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
  sublist[0].solutions__c = sols[0].id;
  upsert sublist;
  system.assert(sublist!=null);
  List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
  orders[0].solutions__c = sols[0].id;
  upsert Orders;
  List<csord__Service__c> services = P2A_TestFactoryCls.getService(1,OrdReqList,SUBList);
  services[0].solutions__c = sols[0].id;
  upsert services;
 
  List<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
  Products[0].csordtelcoa__Account__c = acclist1[0].id;
  Products[0].Quote_Status__c = 'Approved';
  Products[0].solutions__c= sols[0].id;
  upsert Products;
  system.assert(Products!=null);
  
   SolutionSummary.RemovePreviousReferenceToSolutionBeforeCaseClosure(sols[0].id);

  
  }
 @istest
 Public static void solsummary4(){
   
  List<Account> accList = P2A_TestFactoryCls.getAccounts(1);  
  List<solutions__c> sols = P2A_TestFactoryCls.getsolutions(1,acclist);
  List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
  opplist[0].solutions__c = sols[0].id;
  upsert opplist;
  List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
  List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
  List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
  List<User> users = P2A_TestFactoryCls.get_Users(1);
  list<case> caselist = P2A_TestFactoryCls.getcases(1,accList,opplist,prodBaskList,Orders,Users);
  system.assert(users!=null);
    SolutionSummary.createSolution(caselist[0].id);
    
    } 
 
      @isTest static void testExceptions(){
         SolutionSummary alls=new SolutionSummary();
        
         try{SolutionSummary.createSolution(null);}catch(Exception e){}
        // try{SolutionSummary.setSolutionIdForISO(null);}catch(Exception e){}
           try{SolutionSummary.RemovePreviousReferenceToSolutionBeforeCaseClosure(null);}catch(Exception e){}
          system.assertEquals(true,alls!=null);    
         
         
     }
    
}