global class StepWrapper implements Comparable {

    public CSPOFA__Orchestration_Step__c m_step;
    
    // Constructor
    public StepWrapper(CSPOFA__Orchestration_Step__c step) {
        m_step = step;
    }
    
    // Compare steps based on the milestone order.
    global Integer compareTo(Object compareTo) {
        // Cast argument to OpportunityWrapper
        StepWrapper compareToStep = (StepWrapper)compareTo;
        
        // The return value of 0 indicates that both elements are equal.
        Integer returnValue = 0;
        String thisLabel = m_step.CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c;
        String otherLabel = compareToStep.m_step.CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c;
        if (thisLabel == null && otherLabel == null)
        {
            return 0;
        }
        if (otherLabel == null)
        {
            return -1;
        }
        if (thisLabel == null)
        {
            return 1;
        }
        Integer thisNumber = Integer.valueOf(thisLabel);
        Integer otherNumber = Integer.valueOf(otherLabel);
        //HACK TODO: Convert string to integer
        if (thisNumber > otherNumber) {
            // Set return value to a positive value.
            returnValue = 1;
        } else if (thisNumber < otherNumber) {
            // Set return value to a negative value.
            returnValue = -1;
        }
        
        return returnValue;       
    }
}