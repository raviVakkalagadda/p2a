@isTest(SeeAllData=False)
public class StepWrapperTest{   
     
    @isTest static void teststepWrapper(){
        test.startTest(); 
        List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists =  P2A_TestFactoryCls.getOrchestrationProcess(5);
         List<CSPOFA__Orchestration_Process__c> orchProcessList = P2A_TestFactoryCls.getOrchestrationProcesss(5,ProcessTemplatesLists);
         List<CSPOFA__Orchestration_Step__c> orchProcessStepList = P2A_TestFactoryCls.getOrchestrationStep(5,orchProcessList);        
         List<CSPOFA__Orchestration_Step_Template__c> orchprocessstepTemplist = P2A_TestFactoryCls.getOOrchestration_Step_Template(5,ProcessTemplatesLists); 
            
        //List<CSPOFA__Orchestration_Step_Template__c> orchprocessstepTemplist=[select id,CSPOFA__Milestone_Label__c from CSPOFA__Orchestration_Step_Template__c where id!='']
        //List<CSPOFA__Orchestration_Step__c> orchProcessStepList=[select id,CSPOFA__Orchestration_Step_Template__c from CSPOFA__Orchestration_Step__c where id!=''];
        Map<integer,id>stepMap=new Map<integer,id>();
        List<CSPOFA__Orchestration_Step_Template__c> updatetempList=new List<CSPOFA__Orchestration_Step_Template__c>();
        for(integer i=0;i<orchprocessstepTemplist.size();i++){
            CSPOFA__Orchestration_Step_Template__c s=orchprocessstepTemplist[i];
            if(i==0){
                s.CSPOFA__Milestone_Label__c='1';
            }
            else if(i==1){
                s.CSPOFA__Milestone_Label__c='2';
            }
            else if(i==2){
                s.CSPOFA__Milestone_Label__c='3';
            }
            else if(i==3){
                s.CSPOFA__Milestone_Label__c='4';
            }
            else {
                s.CSPOFA__Milestone_Label__c='5';
            }            
            stepMap.put(i,s.id);
            updatetempList.add(s);
        }
       
        update updatetempList;
        
        List<CSPOFA__Orchestration_Step__c>orchlist=new List<CSPOFA__Orchestration_Step__c>();
        for(integer j=0;j<orchProcessStepList.size();j++){
        CSPOFA__Orchestration_Step__c orchstep =  orchProcessStepList[j];
         orchstep.CSPOFA__Orchestration_Step_Template__c=stepMap.containsKey(j)?stepMap.get(j):null;
         orchlist.add(orchstep);         
        }        
        update orchlist;        
        system.debug(updatetempList);
        System.debug(orchlist);
        System.assertEquals(true,orchlist[0].CSPOFA__Orchestration_Step_Template__c!=null);
        //List<CSPOFA__Orchestration_Step__c>orchlist1=[select id,CSPOFA__Orchestration_Step_Template__c,CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c from CSPOFA__Orchestration_Step__c where id in:orchlist];
        //System.assertEquals(true,orchlist[0].CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c!=null);
        List<StepWrapper> sortableSteps = new List<StepWrapper>();
        for (CSPOFA__Orchestration_Step__c step : [select id,CSPOFA__Orchestration_Step_Template__c,CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c from CSPOFA__Orchestration_Step__c where id in:orchlist])
        {
            sortableSteps.add(new StepWrapper(step));
        }

        sortableSteps.sort();
        
         
        List<StepWrapper> sortableSteps1 = new List<StepWrapper>();
        for (CSPOFA__Orchestration_Step__c step : orchlist)
        {
            sortableSteps1.add(new StepWrapper(step));
        }

        sortableSteps1.sort();
        
        
                
        test.stopTest();
        
   }   
}