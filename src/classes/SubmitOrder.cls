global class SubmitOrder {
    public ID orderId {get;set;}
  
    @future (callout=true)
    public static void submitOrderToFOM(ID OrderId){
        TibcoServiceOrderCreator orderCreator = new TibcoServiceOrderCreator();
        TibcoServiceOrder serviceOrder = orderCreator.CreateOrderFromServiceId(orderId);
        TibcoController controller1 = new TibcoController();
        String result = controller1.SubmitServiceOrder(serviceOrder,Label.SubmitOrder,'SubmitOrderResponse','SubmitOrderRequest', Label.SubmitOrderRequest);
    }
    
    //will be called from batch class
     public static void submitOrderToFOMFromBatch(ID OrderId){
        TibcoServiceOrderCreator orderCreator = new TibcoServiceOrderCreator();
        TibcoServiceOrder serviceOrder = orderCreator.CreateOrderFromServiceId(orderId);
        TibcoController controller1 = new TibcoController();
        String result = controller1.SubmitServiceOrder(serviceOrder,Label.SubmitOrder,'SubmitOrderResponse','SubmitOrderRequest', Label.SubmitOrderRequest);
    }
}