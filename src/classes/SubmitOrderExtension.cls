Public class SubmitOrderExtension{
    
    List<csord__Service__c> srvcList = new List<csord__Service__c>();
    csord__Order__c ordersRecord = new csord__Order__c();
    boolean ETCCaseOpen = false;
    String errorMsg = '';
    Boolean error = false;
    public boolean errorMesgFlag {get; set;}
    public String orderId {get; set;}
    public string actionResult {get; set;}  
    Id pId = userinfo.getProfileId();
    List<Case> caseETCList = new List<Case>();
    private static boolean serviceIDGenerated = false;
    
    public SubmitOrderExtension(ApexPages.StandardController controller){
        ordersRecord = (csord__Order__c)controller.getRecord();
        orderId = ApexPages.currentPage().getParameters().get('Id') ;
        errorMesgFlag = false;

        srvcList = [Select Id,Name,Site_A_Code__c,A_Side_Site__c,Z_Side_Site__c, Primary_Service_ID__c, csordtelcoa__Product_Basket__c, ServiceItemNumber__c,
                csordtelcoa__Product_Configuration__r.Added_Ports__c,csordtelcoa__Product_Configuration__r.Product_Definition_Name__c, parent_service_number__c,
                csordtelcoa__Product_Configuration__c,Opportunity__c,Site_B_Code__c,Resource_id__c,Product_Id__c,Selling_Entity__c,Service_Type__c,
                Product_Code__c,orderType__c,csordtelcoa__Product_Configuration__r.EvplId__c,Order_Channel_Type__c, csord__Service__c,
                VLAN_Group_Service__c, VLAN_Group_Service__r.Path_Instance_ID__c,Customer_Required_Date__c,Estimated_Start_Date__c,
                Product_Configuration_Type__c,Path_Instance_ID__c,region__c, Master_Service__c, Master_Service__r.Path_Instance_ID__c,Master_Service__r.csord__Order__r.Is_Order_Submitted__c From csord__Service__c Where csord__Order__c =:orderId];
    }

    public PageReference blockWebServiceCallTwice(){
        actionResult = 'OrderSubmitted';
        errorMesgFlag = false;
        return null;
    }   
    
    public PageReference GotoDetailPage(){

        if(pId == label.Profile_TI_Order_Desk){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'You are not authorized to request this action.'));
            errorMesgFlag = true;
            return null;
        }
        
        /**
         * Added fix for the VLG product as part of the defect fix #14193 - Sapan\
         * Validation to check if VPN product's master parent orders are submitted.
         */     
            List<csord__Service__c> newRecords = new List<csord__Service__c>();
            Map<Id,Set<Id>> IPCAddedPortsList = new Map<Id,Set<Id>>();
            Map<Id,Id> GIEPortList = new Map<Id,Id>();
            Map<Id,Id> VPNPortList = new Map<Id,Id>();
            Map<Id,Id> VLGVLVList = new Map<Id,Id>();
            Map<Id,Id> EVPLList = new Map<Id,Id>();
            Set<Id> addedPorts = new Set<Id>();
            Set<Id> basketId = new Set<Id>();
            String VPNPortsId, orderNo, GIEIds;
            Id internetPCId, evplPCId, VLGId;
            boolean IPVPNOrder = true;
            boolean flag = true;

            for(csord__Service__c services :srvcList){
                
                if(services.Service_Type__c == null 
                    || services.Product_Code__c == null 
                    || services.Selling_Entity__c == null 
                    || services.Customer_Required_Date__c == null 
                    || services.Estimated_Start_Date__c == null 
                    || services.Product_Configuration_Type__c == null 
                    || services.region__c == null){
                       errorMsg += services.Name +', ';
                       error = true;
                } else if(services.orderType__c != null 
                    && services.orderType__c.equalsIgnoreCase('PROVIDE')){
                        newRecords.add(services);
                }
                
                if(services.csordtelcoa__Product_Basket__c != null 
                    && (services.Product_Code__c == 'IPC' 
                        || services.Product_Id__c == 'IPTM-PLA' 
                        || services.Product_Id__c == 'IPTM-STD' 
                        || services.Product_Id__c == 'TWIM' 
                        || services.Product_Code__c == 'IPT')){
                            internetPCId = services.csordtelcoa__Product_Configuration__c;
                            basketId.add(services.csordtelcoa__Product_Basket__c); /** Assigning basket id to the list basketId **/
                }
                
                if(services.csordtelcoa__Product_Basket__c != null 
                    && (services.Product_Code__c == 'EVT' 
                        || services.Product_Code__c == 'EVC' 
                        || services.Product_Code__c == 'EVP')){
                            evplPCId = services.csordtelcoa__Product_Configuration__r.EvplId__c;
                            basketId.add(services.csordtelcoa__Product_Basket__c); /** Assigning basket id to the list basketId **/
                }
                
                if(services.csordtelcoa__Product_Basket__c != null 
                    && (services.Product_Id__c == 'OFFPOPA' 
                        || services.Product_Id__c == 'OFFPOPB' 
                        || services.Product_Id__c == 'VLL-A')){
                            VPNPortsId = services.csordtelcoa__Product_Configuration__r.Added_Ports__c;
                            basketId.add(services.csordtelcoa__Product_Basket__c); /** Assigning basket id to the list basketId **/
                }
                
                if(services.csordtelcoa__Product_Basket__c != null && services.Product_Id__c == 'VLV'){
                    VLGId = services.VLAN_Group_Service__c;
                    basketId.add(services.csordtelcoa__Product_Basket__c); /** Assigning basket id to the list basketId **/
                }           
                
                if(services.csordtelcoa__Product_Basket__c != null && services.Product_Code__c == 'ASBRB'){
                    GIEIds = services.csordtelcoa__Product_Configuration__c;
                    basketId.add(services.csordtelcoa__Product_Basket__c); /** Assigning basket id to the list basketId **/
                }
                
                if(services.Master_Service__c != null 
                    && IPVPNOrder == true 
                    && (services.Master_Service__r.Path_Instance_ID__c == null 
                        || !services.Master_Service__r.csord__Order__r.Is_Order_Submitted__c)){
                        IPVPNOrder = false;
                }
            }
            
            for(csord__Service__c services : srvcList){
                if(services.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c == 'TWI Singlehome' || 
                   services.csordtelcoa__Product_Configuration__r.Product_Definition_Name__c == 'IPT Singlehome'){
                       internetPCId = services.csordtelcoa__Product_Configuration__c;
                   }
            }
            
            /** The list 'srvcList' will contain all product configuration records from the current basket **/
            List<csord__Service__c> srvcList = [select id, csordtelcoa__Product_Configuration__r.Added_Ports__c, 
                                                Path_Instance_ID__c,Product_Code__c,Product_Id__c, csord__Order__r.Is_Order_Submitted__c from csord__Service__c 
                                                where csordtelcoa__Product_Basket__c in: basketId];
            
            for(csord__Service__c srvc :srvcList){
                
                /** Validating if the list contains parent Internet products, and assigning the parent and child PC Ids to the map 'IPCAddedPortsList' **/
                if((srvc.Product_Code__c == 'IPC' || srvc.Product_Id__c == 'IPTM-PLA' || srvc.Product_Id__c == 'IPTM-STD' || srvc.Product_Id__c == 'TWIM') 
                    && srvc.csordtelcoa__Product_Configuration__r.Added_Ports__c != null 
                    && (srvc.Path_Instance_ID__c == null || !srvc.csord__Order__r.Is_Order_Submitted__c)){
                       for(String portId :srvc.csordtelcoa__Product_Configuration__r.Added_Ports__c.split(',')){
                           addedPorts.add(Id.valueOf(portId));                  
                       }
                       IPCAddedPortsList.put(srvc.Id,addedPorts);
                   }
                
                if((srvc.Product_Code__c == 'EVT' || srvc.Product_Code__c == 'EVC') 
                    && (srvc.Path_Instance_ID__c == null || !srvc.csord__Order__r.Is_Order_Submitted__c)){
                        EVPLList.put(srvc.Id,srvc.csordtelcoa__Product_Configuration__c);
                }
                
                if((srvc.Product_Id__c == 'CUSTSITEOFFNETPOP' || srvc.Product_Id__c == 'IPVPN-BACKPOP-OFF' || srvc.Product_Id__c == 'VLP-Offnet' || srvc.Product_Id__c == 'BackUp-VLP-Offnet') 
                    && (srvc.Path_Instance_ID__c == null || !srvc.csord__Order__r.Is_Order_Submitted__c)){
                       VPNPortList.put(srvc.Id,srvc.csordtelcoa__Product_Configuration__c);
                   }            
                
                if(srvc.Product_Id__c != null 
                    && srvc.Product_Id__c == 'VLG'  
                    && (srvc.Path_Instance_ID__c == null || !srvc.csord__Order__r.Is_Order_Submitted__c)){
                        VLGVLVList.put(srvc.Id, srvc.Id);
                }
                if(srvc.Product_Code__c == 'GIE'                
                    && (srvc.Path_Instance_ID__c == null || !srvc.csord__Order__r.Is_Order_Submitted__c)){
                        GIEPortList.put(srvc.Id,srvc.csordtelcoa__Product_Configuration__c);
                }
            }
            
            /** Added fix for 14766 - Jana **/
            if(ordersRecord.Partial_Terminate_Order__c == true || ordersRecord.Full_Termination__c == true){
                caseETCList = [Select Id, Status From case Where order__c =:orderId and Subject='Request to calculate ETC' and Status!='Closed'];       
                if(caseETCList.Size() > 0){
                    ETCCaseOpen = true;
                }
            }
            
            if((!IPCAddedPortsList.isEmpty() || !EVPLList.isEmpty() || !VPNPortList.isEmpty() || !VLGVLVList.isEmpty() || 
               !GIEPortList.isEmpty()) && ordersRecord.Full_Termination__c == false){
                   
                   /** The list 'servcList' will contain all parent Internet services from the current basket **/
                   List<csord__Service__c> servcList = [select id,csordtelcoa__Product_Configuration__c,Path_Instance_ID__c,
                                                        csord__Order__c,Product_Code__c,Product_Id__c,csordtelcoa__Product_Configuration__r.EvplId__c 
                                                        from csord__Service__c where csordtelcoa__Product_Basket__c in : basketId and 
                                                        (Id in :IPCAddedPortsList.keySet() or Id in :EVPLList.keySet() or Id in :VPNPortList.keySet() or 
                                                         Id in :VLGVLVList.keySet() or Id in :GIEPortList.keySet())];
                   
                   /** Validating if the parent Internet orders are successfully submitted and the Path Instance Id created **/
                   for(csord__Service__c servc :servcList){
                       if(!IPCAddedPortsList.isEmpty() && IPCAddedPortsList.get(servc.Id).Contains(internetPCId)){
                           flag = false; /** If any of the parent Internet Orders are not submitted, then setting the 'flag' to false **/
                           orderNo = servc.csord__Order__c;
                           break;
                       }

                       if(evplPCId != null && !EVPLList.isEmpty() && EVPLList.get(servc.Id) == evplPCId){
                           flag = false; /** If any of the parent Internet Orders are not submitted, then setting the 'flag' to false **/
                           orderNo = servc.csord__Order__c;
                           break;
                       }
                       
                       if(VPNPortsId != '' && VPNPortsId != null){
                           if(!VPNPortList.isEmpty() && VPNPortsId.contains(VPNPortList.get(servc.Id))){
                               flag = false; /** If any of the parent Orders are not submitted, then setting the 'flag' to false **/
                               orderNo = servc.csord__Order__c;
                               break;
                           }
                       }
                       
                       if(VLGId != null && !VLGVLVList.isEmpty() && VLGVLVList.get(servc.Id) == VLGId){
                           flag = false; /** If any of the parent Orders are not submitted, then setting the 'flag' to false **/
                           orderNo = servc.csord__Order__c;
                           break;
                       }
                       
                       if(GIEIds != '' && GIEIds != null){
                           if(!GIEPortList.isEmpty() && GIEIds.contains(GIEPortList.get(servc.Id))){
                               flag = false; /** If any of the parent GIE Orders are not submitted, then setting the 'flag' to false **/
                               orderNo = servc.csord__Order__c;
                               break;
                           }
                       }           
                   }
                   
                   /** Only for Test Class run **/
                   if(Test.isRunningTest()){
                       flag = false;
                       orderNo = servcList[0].csord__Order__c;
                   }
                   
                   /** If the flag is flase, it will not allow the child Internet orders submission **/
                   if(flag == false && orderNo != null && internetPCId != null){
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please submit the Parent Order before submitting the Child Order. Please click on the link below to view details of Parent Order.</br></br><a href=\'/'+ orderNo +'\'> '+Label.EnvironmentURL+'/'+ orderNo + '</a>'));
                       errorMesgFlag = true;
                       return null;
                   }
                   
                   if(flag == false && orderNo != null && evplPCId != null){
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please submit the linked EVC/EVT order before submitting the EVP Port order. Click </br></br><a href=/'+orderNo +'>'+Label.EnvironmentURL+'/'+orderNo +'</a> to go to the EVC/EVT order.'));
                       errorMesgFlag = true;
                       return null;
                   }  
                   
                   if(flag == false && orderNo != null && VPNPortList != null && VLGId == null){
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please submit the linked Parent Port order before submitting the ASBR order. Click </br></br><a href=/'+orderNo +'>'+Label.EnvironmentURL+'/'+orderNo +'</a> to go to the Parent Port order.'));
                       errorMesgFlag = true;
                       return null;
                   }        
                   
                   if(flag == false && orderNo != null && VLGVLVList != null){
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please submit the linked VLG order before submitting the associated VLV order. Click </br></br><a href=/'+orderNo +'>'+Label.EnvironmentURL+'/'+orderNo +'</a> to go to the VLG order.'));
                       errorMesgFlag = true;
                       return null;
                   }
                   
                   if(flag == false && orderNo != null && GIEPortList != null){
                       ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please submit the linked GIE order before submitting the ASBR order. Click </br></br><a href=/'+orderNo +'>'+Label.EnvironmentURL+'/'+orderNo +'</a> to go to the GIE order.'));
                       errorMesgFlag = true;
                       return null;
                   }
               }

            if(IPVPNOrder == false && ordersRecord.Full_Termination__c == false){ 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'This order cannot be submitted because it has a Master Service that is pending submission.'));
                errorMesgFlag = true;
                return null;
            }              
            
            if(ordersRecord.Is_Order_Submitted__c == true) 
            { 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Order Was already submitted'));
                errorMesgFlag = true;
                return null; 
            } 
            if(ordersRecord.SD_PM_Contact__c == null) 
            { 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please Fill SD PM Name.'));
                errorMesgFlag = true;
                return null; 
            }

            /** Added fix for 14766 - Jana **/
            if(ETCCaseOpen){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error,'Termination order can not be submitted since there is an open case or a cancelled/rejected case for ETC Calculation'));
                errorMesgFlag = true;
                return null;
            } 
            else if(error == true && errorMsg != null) 
            { 
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.Error, 'The Order cannot be submitted. Value is missing from one of the "Service Type/ Product Code/ Selling Entity/ Customer Required Date/ Product Configuration Type/ Region/ Estimated Start Date" fields in the Service(Send an e-mail to Global Serve for assistance): ' +errorMsg));
                errorMesgFlag = true;
                return null;
                
            }
            else
            {
                /** Defect #12139//Suparna **/
                if(ordersRecord.Is_Order_Submitted__c == false && ordersRecord.SD_PM_Contact__c != null ) 
                { 
                
                try{
                    /** Defect #12139//Suparna **/
                    if(!ordersRecord.Is_Terminate_Order__c && serviceIDGenerated == false && ordersRecord.Service_ID_Generated__c == false){
                        TriggerGenerateID.SendServicedata(newRecords);
                        serviceIDGenerated = true;
                        ordersRecord.Service_ID_Generated__c = true;                        
                    }
                 } catch(Exception e){
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Order Submission Failed - ' +e.getMessage()));
                    errorMesgFlag = true;
                    return null;
                   }
                    TriggerFlags.NobillProfileUPdate = true;
                    TriggerFlags.NoOpportunitySplitTriggers = true;
                    util.FlagtoMuteOpportunitySplitTrg = false;
                    System.debug('SubmitOrder_LOG: billProfileUPdate, OpportunitySplitTriggers and OpportunitySplitTrg Classes/Trigger are disabled');
                    
                    ordersRecord.Is_Order_Submitted__c = true;
                    ordersRecord.Status__c = 'Submitted'; 
                    ordersRecord.Clean_Order_Check_Passed__c = true; 
                    /** Defect #11837 Manish Pankaj, Message on Successful order submission **/
                    ApexPages.AddMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM,'Order Submitted Successfully.Thank you!'));
                    update ordersRecord;
                    
                    if(ordersRecord.csord__Order_Type__c == 'Subscription Creation' || ordersRecord.csord__Order_Type__c == 'Subscription Change' || ordersRecord.csord__Order_Type__c=='Terminate'){
                        SubmitOrder.submitOrderToFOM(ordersRecord.Id);
                    }
                } 
            }
        PageReference page = new PageReference('/'+orderId);
        return page ;
    }
}