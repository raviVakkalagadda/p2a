@isTest(SeeAllData=false)
private class SupplierCaseConfirmedBatchTest {
    
    private static cscfga__Product_Basket__c basket = null;
    private static List<cscfga__Product_Configuration__c> prodConfigs = null;
    private static Case testQuoteCase = null;
    
    /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
    private static void prepareTestData() {
           
        basket = new cscfga__Product_Basket__c();
        insert basket;
        
        testQuoteCase = new Case(Subject = 'Supplier quote request test', Status = 'Confirmed', Product_Basket__c = basket.Id);
        insert testQuoteCase;
        
        cscfga__Product_Category__c productCategory = new cscfga__Product_Category__c(Name = 'Test category');
        insert productCategory;
        
        List<cscfga__Product_Definition__c> prodDefintions = new List<cscfga__Product_Definition__c> {
            new cscfga__Product_Definition__c (Name = 'Test root definition'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test root definition'), 
            new cscfga__Product_Definition__c (Name = 'Test child definition'
                , cscfga__Product_Category__c = productCategory.Id
                , cscfga__Description__c = 'Test child definition')};
        insert prodDefintions;
        
        List<cscfga__Attribute_Definition__c> attrDefinitions = new List<cscfga__Attribute_Definition__c> { 
            new cscfga__Attribute_Definition__c (Name = 'Test attr'
                , cscfga__Product_Definition__c = prodDefintions[0].Id
                , cscfga__Data_Type__c = 'Decimal'
                , cscfga__Type__c = 'Display Value'),
            new cscfga__Attribute_Definition__c (Name = 'Test attr'
                , cscfga__Product_Definition__c = prodDefintions[1].Id
                , cscfga__Data_Type__c = 'Decimal'
                , cscfga__Type__c = 'Display Value')
        }; 
        insert attrDefinitions;
        
        prodConfigs = new List<cscfga__Product_Configuration__c> {
            new cscfga__Product_Configuration__c( Name = 'Test root config'
                , cscfga__Product_Definition__c = prodDefintions[0].Id
                , cscfga__Product_Family__c = 'Test family'
                , cscfga__Product_Basket__c = basket.Id), 
            new cscfga__Product_Configuration__c( Name = 'Test root config'
                , cscfga__Product_Definition__c = prodDefintions[1].Id
                , cscfga__Product_Family__c = 'Test family'
                , cscfga__Product_Basket__c = basket.Id) 
        };
        insert prodConfigs;
        
        cscfga__Product_Configuration__c rootConfig = prodConfigs[0];
        cscfga__Product_Configuration__c childConfig = prodConfigs[1];
        childConfig.cscfga__Root_Configuration__c = rootConfig.Id;
        update childConfig;
        
        List<cscfga__Attribute__c> attrs = new List<cscfga__Attribute__c>{
            new cscfga__Attribute__c(Name = 'Test attribute'
                , cscfga__Product_Configuration__c = prodConfigs[0].Id
                , cscfga__Line_Item_Description__c = 'LI description'
                , cscfga__Value__c = '0.0'
                , cscfga__Display_Value__c = '0.0'
                , cscfga__Price__c = 0.0
                , cscfga__Is_Required__c = false
                , cscfga__Attribute_Definition__c = attrDefinitions[0].Id), 
            new cscfga__Attribute__c(Name = 'Test attribute'
                , cscfga__Product_Configuration__c = prodConfigs[1].Id
                , cscfga__Line_Item_Description__c = 'LI description'
                , cscfga__Value__c = '0.0'
                , cscfga__Display_Value__c = '0.0'
                , cscfga__Price__c = 0.0
                , cscfga__Is_Required__c = false
                , cscfga__Attribute_Definition__c = attrDefinitions[1].Id)
        };
        insert attrs;
        
        List<csbb__Product_Configuration_Request__c> prodRequests = new List<csbb__Product_Configuration_Request__c> {
            new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basket.Id
                , csbb__Product_Category__c = productCategory.Id
                , csbb__Product_Configuration__c = prodConfigs[0].Id
                , csbb__Optionals__c = '{}'),
            new csbb__Product_Configuration_Request__c(csbb__Product_Basket__c = basket.Id
                , csbb__Product_Category__c = productCategory.Id
                , csbb__Product_Configuration__c = prodConfigs[1].Id
                , csbb__Optionals__c = '{}')
        };
        insert prodRequests; 
        system.assert(prodRequests!=null);
    
        List<custom_object_to_product_configuration__c> custToProdMaps = new List<custom_object_to_product_configuration__c> {
            new custom_object_to_product_configuration__c (Name = 'Test NRC cost'
                , Source_Object__c= 'VendorQuoteDetail__c'
                , Source_Field__c = 'Non_recurring_Cost__c '
                , Production_Configuration_Attribute_Name__c = attrs[0].Name
                , Product_Name__c = 'Test familySupplierApproval')
        };
        insert custToProdMaps;
    }
    
    private static void prepareVendorQuoteData(Integer prodConfigIndex) {
        Exception ee = null;
        try {
            disableAll(UserInfo.getUserId());
            prepareTestData();

            testQuoteCase.Product_Configuration__c = prodConfigs[prodConfigIndex].Id;
            update testQuoteCase;
            
            Supplier__c supplier = new Supplier__c (Name = 'TEST'
                , Approved__c = true
                , Contact_Type__c = 'TEST'
                , Email__c = 'test@gmail.com'
                , Services__c = 'TEST'
                , Landline__c = '852 21728886');
            insert supplier;
            
            VendorQuoteDetail__c vendorQuote = new VendorQuoteDetail__c(Recurring_Cost__c = 1.1
                , Non_recurring_Cost__c =  1.1
                , Related_3PQ_Case_Record__c = testQuoteCase.Id
                , Product_Type__c = 'TEST'
                , Interface_Type__c = 'TEST'
                , Expiration_date__c = System.Today()
                , Supplier_Name__c = supplier.Id
                , Winning_Quote__c = true
                , Win_Reason__c = 'Win win');
            insert vendorQuote;
            system.assert(vendorQuote!=null);
            
        } catch(Exception e) {
            ee = e;
            ErrorHandlerException.ExecutingClassName='SupplierCaseConfirmedBatchTest :prepareVendorQuoteData';         
            ErrorHandlerException.sendException(e);
        } finally {
            enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
    private static testMethod void testPreparedData() {
        Exception ee = null;
        Integer userid = 0;
        try {
            disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            Test.startTest();
            prepareTestData();
        } catch(Exception e) {
            ee = e;
            ErrorHandlerException.ExecutingClassName='SupplierCaseConfirmedBatchTest :testPreparedData';         
            ErrorHandlerException.sendException(e); 
        } finally {
            Test.stopTest();
            enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    
    private static testMethod void testSupplierCaseBatchOnRoot() {
        // prepare test data
        prepareVendorQuoteData(0);
        // create the bacth
        SupplierCaseConfirmedBatch bc = new SupplierCaseConfirmedBatch(prodConfigs[0].Id);
        system.assert(bc!=null);
        // perform test
        Test.startTest();
        Database.executeBatch(bc);  
        Test.stopTest();
    }    
    
    private static testMethod void testSupplierCaseBatchOnChild() {
        // prepare test data
        prepareVendorQuoteData(1);
        // create the bacth
        SupplierCaseConfirmedBatch bc = new SupplierCaseConfirmedBatch(prodConfigs[1].Id);
        system.assert(bc!=null);
        // perform test
        Test.startTest();
        Database.executeBatch(bc);  
        Test.stopTest();
    } 
}