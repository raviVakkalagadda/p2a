@isTest(SeeAllData = false)
private class SupplierQuoteQueryTest {
private static Map<String, String> searchFields = new Map<String, String>();
private static String productDefinitionId;
private static List<CS_Route_Segment__c> routeSegmentList;

 private static void initTestData(){
        routeSegmentList = new List<CS_Route_Segment__c>{
            new CS_Route_Segment__c(Name = 'Route Segment 1', Product_Type__c = 'IPL'),
            new CS_Route_Segment__c(Name = 'Route Segment 2', Product_Type__c = 'IPL')
        };
        
        insert routeSegmentList;
        list<CS_Route_Segment__c> prod = [select id,Name from CS_Route_Segment__c where Name = 'Route Segment 1'];
        system.assertEquals(routeSegmentList[0].Name , prod[0].Name);
        System.assert(routeSegmentList!=null);
        
        CurrencyValue__c Currencyobj = new CurrencyValue__c();
        Currencyobj.currencyisocode= 'USD';
        Currencyobj.name = 'USD';
        currencyobj.Currency_Value__c = 1;
        insert Currencyobj;
        System.assert(Currencyobj.id!= null);
        system.assertEquals(Currencyobj.name , 'USD');
        
        Supplier__c s = new Supplier__c();
        s.CurrencyIsoCode = 'USD';
        s.name = 'Airtel';
        s.Approved__c= true;
        s.Services__c = 'GCPE';
        insert s;
        System.assert(s.id!= null); 
        system.assertEquals(s.name , 'Airtel');
        
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);  
        case c = new case();
        c.Status ='Unassigned';
        c.product_basket__c=pblist[0].id;        
        insert c;
        
        system.assertEquals(c.status , 'Unassigned');
        System.assert(c!=null);
        

        list<VendorQuoteDetail__c> caseVendorDetailsList  = new list<VendorQuoteDetail__c>();
        VendorQuoteDetail__c vqobj = new VendorQuoteDetail__c();
        vqobj.currencyisocode= 'USD';
        vqobj.supplier_name__c = s.id;
        vqobj.Name = '2345';
        vqobj.recurring_Cost__c = 20;
        vqobj.Non_Recurring_Cost__c = 50;
        vqobj.Expiration_date__c = system.today();
        vqobj.Interface_Type__c = '2M';
        vqobj.Product_Type__c = 'GCPE';
        vqobj.Winning_Quote__c = true;
        vqobj.Win_Reason__c = 'Single source';
        vqobj.Related_3PQ_Case_Record__c = c.id;
        vqobj.Basket_Exchange_Rate__c = 1;
        caseVendorDetailsList.add(vqobj);
        insert caseVendorDetailsList ;
         list<VendorQuoteDetail__c> prod3 = [select id,Name from VendorQuoteDetail__c where Name = '2345'];
        system.assertEquals(caseVendorDetailsList[0].Name , prod3[0].Name);
        System.assert(caseVendorDetailsList!=null);
        
   }     
    private static testMethod void doDynamicLookupSearchTest() {

    Exception ee = null;
   
        try{
            CS_TestUtil.disableAll(UserInfo.getUserId());
            Test.startTest();
            
            initTestData();
            
            searchFields.put('ProductType', routeSegmentList[0].Id);
            searchFields.put('searchValue', '');
            Id[] excludeIds; 
            //Integer pageOffset, pageLimit;
            //String productDefinitionId;
            Integer i1 = 1;
            Integer i2 = 2;
           

            SupplierQuoteQuery squery = new SupplierQuoteQuery();
            Object[] data = squery.doDynamicLookupSearch(searchFields, productDefinitionId);
          
            String reqAtts = squery.getRequiredAttributes();
           // Object[] datas = squery .doLookupSearch(searchFields, productDefinitionId); 
            //System.debug('*******Data: ' + data);
            System.assert(data.size() > 0, '');   
             
            
        } catch(Exception e){
            ee = e;
            ErrorHandlerException.ExecutingClassName='SupplierQuoteQueryTest :doDynamicLookupSearchTest';         
ErrorHandlerException.sendException(e);
        } finally {
            Test.stopTest();
            //enableAll(UserInfo.getUserId());
            if(ee != null){
                throw ee;
            }
        }
  }
 
 }