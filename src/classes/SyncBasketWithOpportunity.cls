global class SyncBasketWithOpportunity {
    
 webservice static string SynchronizeBasket(String ProductBasketId, String OppId){
            
        ProductBasketId = (string) (Id) ProductBasketId;
        OppId = (string) (Id) OppId;
        system.debug('****ProductBasketId=' + ProductBasketId);

        List<cscfga__Product_Basket__c> CurrentBasket = [select Id, csordtelcoa__Synchronised_with_Opportunity__c,csbb__Synchronised_With_Opportunity__c,cscfga__Opportunity__c
            from cscfga__Product_Basket__c where cscfga__Opportunity__c =:OppId];
        
        system.debug('****CurrentBasket before update=' + CurrentBasket);
        
        for(cscfga__Product_Basket__c bskt :CurrentBasket){
            if(bskt.Id == ProductBasketId){
                bskt.csbb__Synchronised_With_Opportunity__c=true;
                bskt.csordtelcoa__Synchronised_with_Opportunity__c = true;
            } else{
                bskt.csbb__Synchronised_With_Opportunity__c=false;
                bskt.csordtelcoa__Synchronised_with_Opportunity__c = false;             
            }
        }
        update CurrentBasket;
        system.debug('****CurrentBasket after update=' + CurrentBasket);
                
        return OppId;
    
} 
}