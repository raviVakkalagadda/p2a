/**
    @author - Accenture
    @date - 19- Jun-2012
    @version - 1.0
    @description - This is the test class for TIUtils class
*/

@isTest
private class TIUtilsTest{

     static testMethod void getAllFieldsSOQLTest() {
        TIUtils.getAllFieldsSOQL('Product2', 'IsActive=true');
     }
     static testMethod void getCreatableFieldsSOQLTest() {
        TIUtils.getCreatableFieldsSOQL('Product2', 'IsActive=true');
     }
}