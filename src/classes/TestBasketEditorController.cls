@isTest
public class TestBasketEditorController {
    
    static testmethod void controllerTest() {
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c(
            Name = 'Test',
            cscfga__Description__c = 'Test',
            Add_On__c = 'Test',
            Feature__c = 'Test',
            Rate_Card__c = 'Test',
            Price_Item_Attribute__c = 'Test'
        );
        insert pd;
        list<cscfga__Product_Definition__c> prod = [select id,Name from cscfga__Product_Definition__c where Name = 'Test'];
        system.assertEquals(pd.Name , prod[0].Name);
        System.assert(pd!=null);
        
        Add_On_Widget_Settings__c addOnSettings = new Add_On_Widget_Settings__c(
            Parent_Attribute__c = 'Parent',
            Add_On_Attribute__c = 'AddOn',
            Name = 'Test'
        );
        insert addOnSettings;
        list<Add_On_Widget_Settings__c> prod1 = [select id,Name from Add_On_Widget_Settings__c where Parent_Attribute__c = 'Parent'];
        system.assertEquals(addOnSettings.Name , prod1[0].Name);
        System.assert(addOnSettings!=null);
        
        // Test constructor
        ApexPages.StandardController sc = new ApexPages.StandardController(pd);
        BasketEditorController ctrl = new BasketEditorController();
        ctrl = new BasketEditorController(sc);
        
        // Category and Price Item data retrieval
        String str = BasketEditorController.getCategoriesAndPriceItemAttributeName(pd.Id);
    }
}