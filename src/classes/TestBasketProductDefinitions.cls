@isTest
Public class TestBasketProductDefinitions{
/**
* Disables triggers, validations and workflows for the given user
* @param userId Id
*/
     
    private static voId disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }
    
/**
* Enables triggers, valIdations and workflows 
* @param userId Id
*/
    private static voId enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_ValIdations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
    }
//Initializing 
    private static Account act;
    private static Opportunity oppObj;
    //private static cscfga__Product_Basket__c pbasket;
    private static cscfga__Product_Definition__c pd;
    private static cscfga__Product_Configuration__c pc;

//creating test data
    static void createTestData()
    {       
        //Creating Account object records
        act = new Account(Name='Test112233M',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '3333',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        //Creating Opportunity object records
         oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',Product_Type__c = 'EPL',
                                            Order_Type__c= 'New', ContractTerm__c = '24');
        insert oppObj; 
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        //insert Product Basket object record
       /* pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
        insert pbasket;*/
        
        List<cscfga__Product_Basket__c> pbasket = P2A_TestFactoryCls.getProductBasket(1);
        
        //insert product definition record
        pd = new cscfga__Product_Definition__c();
        pd.Name ='IPVPN Port';
        pd.cscfga__Description__c= 'GCPEbdkjdvbhe;ghwfguwefkjwf';
        insert pd;
        
        //insert product configuration record
        pc= new cscfga__Product_Configuration__c();
        pc.cscfga__Product_Definition__c = pd.Id;
        pc.cscfga__Product_Basket__c = pbasket[0].Id;
        insert pc;
            
    }
    
    private static testmethod void  basketProdDefTestWithoutScreenFlow()
    {
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'test@telstra.com',
            email = 'test@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='123456789',Region__c='Australia');
            insert userObj;
            system.assertEquals(true,userObj!=null); 
        System.RunAs(userObj)
        {
            disableAll(userObj.Id);
            createTestData();
            enableAll(userObj.Id);
            Test.startTest();
            PageReference pageTest = Page.ProductChooser;
            Test.setCurrentPage(pageTest);
            //pageTest.getParameters().put('basketId',pBasket.Id);
            BasketProductDefinitions basketProdDef=new BasketProductDefinitions();
            basketProdDef.getItems();
            basketProdDef.selectListValue = pd.Id;
            PageReference p = basketProdDef.setProductDefinitionName();
            basketProdDef.save();
            Test.stopTest();
        }   
    }
    private static testmethod void  basketProdDefTestWithScreenFlow()
    {
        Profile profObj = [select Id from Profile  where name ='System Administrator'];
        User userObj = new User(profileId = profObj.id, username = 'test@telstra.com',
            email = 'test@telstra.com', emailencodingkey = 'UTF-8',
            localesidkey = 'en_US',languagelocalekey = 'en_US', timezonesidkey = 'America/Los_Angeles',
            alias='nuser1',  lastname='lastname1',EmployeeNumber='123456789',Region__c='Australia');
            insert userObj;
        System.RunAs(userObj)
        {
            disableAll(userObj.Id);
            createTestData();
            //insert screenflow
            cscfga__Screen_Flow__c sf = new cscfga__Screen_Flow__c();
            sf.Name = 'IPVPN MLE';
            sf.MLEFlow__c =true;
            sf.cscfga__Template_Reference__c = 'cscfga__StandardOnlineTemplate';
            insert sf;
            
            //insert screenflow product assosciation
            cscfga__Screen_Flow_Product_Association__c spa = new cscfga__Screen_Flow_Product_Association__c();
            spa.cscfga__Screen_Flow__c =sf.Id;
            spa.cscfga__Product_Definition__c=pd.Id;
            
            insert spa;
            
            enableAll(userObj.Id);
            Test.startTest();
            PageReference pageTest = Page.ProductChooser;
            Test.setCurrentPage(pageTest);
            //pageTest.getParameters().put('basketId',pBasket.Id);
            BasketProductDefinitions basketProdDef=new BasketProductDefinitions();
            basketProdDef.getItems();
            basketProdDef.selectListValue = pd.Id;
            String prodDefSelected = basketProdDef.selectListValue;
            System.assertEquals(pd.Id,prodDefSelected);
            PageReference p = basketProdDef.setProductDefinitionName();
            basketProdDef.save();
            Test.stopTest();
        }   
    }
 
 }