/**
    @author - Accenture
    @date - 26- Jun-2012
    @version - 1.0
    @description - This is the test class for trg_biu_Opportunity Trigger.
*/
@isTest(SeeAllData=true)
private class TestBeforeInsertUpdateOpportunity {

    static Account acc;
    static Opportunity opp;
    static Country_Lookup__c country;
    static Site__c site;
    static City_Lookup__c ci;
    
    static testMethod void getOppLineItemDetails() {
        List<OpportunityLineItem> oli = new List<OpportunityLineItem>();
        oli = getOpportunityLineItem();
       try{
        insert oli;   
        system.assert(oli!=null);     
        } Catch(Exception e){
        
            system.debug('Null point exception'+e.getmessage());
        }
        Test.startTest();
        opp = [SELECT billProfileComplete__c FROM Opportunity WHERE Id =: opp.Id];
        //system.assertEquals(opp.billProfileComplete__c, true);
        
                
        Test.stopTest();
                     
    }
    
    private static List<OpportunityLineItem> getOpportunityLineItem(){
        
        List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Is_GCPE_shared_with_multiple_services__c='YES';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        system.assert(lst!=null);
        lst.add(oli);
        
        OpportunityLineItem oppLineItem = new OpportunityLineItem();
        oppLineItem.OpportunityId = opp.Id;
        oppLineiTem.IsMainItem__c = 'No';
        oppLineItem.OrderType__c = 'Renew';
        oppLineItem.Quantity = 5;
        oppLineItem.UnitPrice = 10;
        oppLineItem.BillProfileId__c = null;
        oppLineItem.Is_GCPE_shared_with_multiple_services__c='YES';
        oppLineItem.PricebookEntryId = getPriceBookEntry('EPL').Id;
        
        lst.add(oppLineItem);
        
        OpportunityLineItem oppLineItem1 = new OpportunityLineItem();
        BillProfile__c b1 = getBillProfile();
        oppLineItem1.OpportunityId = opp.Id;
        oppLineiTem1.IsMainItem__c = 'Yes';
        oppLineItem1.OrderType__c = 'New Provide';
        oppLineItem1.Quantity = 5;
        oppLineItem1.UnitPrice = 10;
        oppLineItem1.BillProfileId__c = b1.Id;
        oppLineItem1.Is_GCPE_shared_with_multiple_services__c='YES';
        oppLineItem1.PricebookEntryId = getPriceBookEntry('IPL').Id;
        
        lst.add(oppLineItem1);
        
        return lst;
    }
    private static Opportunity getOpportunity(){
        if(opp == null) {
            opp = new Opportunity();
            acc = getAccount();
            opp.Name = 'Test Opportunity';
            opp.AccountId = acc.Id;
             //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
            opp.StageName = 'Identify & Define';
            opp.Stage__c='Identify & Define';
            opp.CloseDate = System.today();
              opp.Estimated_MRC__c=800;
            opp.Estimated_NRC__c=1000;
            opp.ContractTerm__c='10';
          //  opp.Approx_Deal_Size__c = 9000;
            insert opp;
            system.assert(opp!=null);
            }
        return opp;
    }
    private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        system.assert(p!=null);
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='EPL';
        insert prod;
        system.assert(prod!=null);
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        p.UseStandardPrice = false;
        insert p; 
        system.assert(p!=null);
        return p;    
    }
    
    
    
    private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        b.Start_Date__c = date.today();        
        b.Invoice_Breakdown__c = 'Summary Page';        
        b.First_Period_Date__c = date.today();
        site = getSite();
        b.Bill_Profile_Site__c = site.Id;

        insert b;
        system.assert(b!=null);
        return b;
    }
    private static Account getAccount(){
        if(acc == null) {
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account Test 6';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_ID__c = '9090';
            acc.Account_Status__c = 'Active';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
            system.assert(acc!=null);
        }
        return acc;
    }
    
    private static Country_Lookup__c getCountry(){
        if(country == null) {
            country = new Country_Lookup__c();
            country.CCMS_Country_Code__c = 'HK';
            country.CCMS_Country_Name__c = 'India';
            country.Country_Code__c = 'HK';
            insert country;
            system.assert(country!=null);
        }
        return country;
    }
    
private static Site__c getSite(){
    if(site== null){
        site= new Site__c();
        acc = getAccount();
        site.Name = 'Test_site';
        site.Address1__c = '43';
        site.Address2__c = 'Bangalore';
        country = getCountry();
        site.Country_Finder__c = country.Id;
        ci = getCity();
        site.City_Finder__c = ci.Id;
        site.AccountId__c =  acc.Id; 
        site.Address_Type__c = 'Billing Address';
        insert site;
        system.assert(site!=null);
    }
    return site;
}  

private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
        system.assert(ci!=null);
    }
    return ci;
}

}