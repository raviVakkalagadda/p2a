/**
    @author - Accenture
    @date - 26- Jun-2012
    @version - 1.0
    @description - This is the test class for trg_bi_Account Trigger
*/
@isTest
private class TestBeforeUpdateAccount {
    static Account acc;
    static Country_Lookup__c cl;
    static Account a;
    
    static testMethod void getAccountDetails() {
        List<Account> lst = new List<Account>();
        String accountOwner = Userinfo.getFirstName() + ' ' +Userinfo.getLastName();
        lst = getAccount();
        insert lst;
        system.assert(lst!=null);
        lst[1].Account_Manager__c = 'ABC';
        update lst;   
        acc = [SELECT Account_Manager__c FROM Account WHERE Id =: lst[0].Id];
        a = [SELECT Account_Manager__c FROM Account WHERE Id =: lst[1].Id];
        
        
        
        
    }
    private static List<Account> getAccount(){
        List<Account> accList= new List<Account>();
        if(acc == null){    
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Account_Manager__c = null;
            acc.Customer_Legal_Entity_Name__c='Test';
            accList.add(acc);
        }
        if(a == null){    
            a = new Account();
            Country_Lookup__c country = getCountry();
            a.Name = 'Test Account';
            a.Customer_Type__c = 'MNC';
            a.Country__c = country.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Account_Manager__c = null;
            a.Customer_Legal_Entity_Name__c='Test';
            accList.add(a);
            system.assert(country !=null);
        }
   
        
        return accList;
    }
    
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'SG';
            cl.CCMS_Country_Name__c = 'India';
            cl.Country_Code__c = 'SG';
            insert cl;
            system.assert(cl!=null);
        }
        return cl;
    } 
}