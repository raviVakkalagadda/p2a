/**
    @author - Accenture
    @date - 3-Aug-2012
    @version - 1.0
    @description - This is the test class for BeforeUpdateOpportunity Trigger
*/
@isTest(SeeAllData = false)
private class TestBeforeUpdateOpportunity {
  /*  static testmethod void TestBeforeUpdateOpp(){
         Test.startTest();
        
       // / Country_Lookup__c Object instance and Mandatory attributes with values
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
         
        // Account Object instance and Mandatory attributes with values
        Account accObj = new Account(name = 'efgh',Customer_Type_New__c ='NE',
                                    Selling_Entity__c = 'Telstra SFDC',Country__c =cntryLkupObj.Id,
                                    Industry = 'Kpo',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='test',
                                    Account_ORIG_ID_DM__c = 'testcase', Customer_Legal_Entity_Name__c = 'teststreet');  
        insert accObj;
         Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                                            CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Identify & Define',Stage__c='Identify & Define',
                                           QuoteStatus__c = 'Approved', Sales_Status__c = 'Won', Win_Loss_Reasons__c='Product', 
                                           Order_Type__c ='New',ContractTerm__c = '24',Total_NRC__c=23,Total_MRC__c=34);
        insert oppobj;
        City_Lookup__c cityobj=new City_Lookup__c(City_Code__c ='Mcs',Name='Madurai');
        insert cityobj;
        Site__C  siteobj = new Site__c( Name = 'sitetest',Address1__c = '44', Address2__c = 'bas',Country_Finder__c = cntryLkupObj.Id,City_Finder__c = cityObj.Id,  
                                        AccountId__c =  accobj.Id,Address_Type__c = 'Billing Address' );

        insert siteobj;   
      
       PriceBook2 pricebook2Obj = new PriceBook2(Name='Testcase');
       insert pricebook2obj;

        Product2 product2obj = new Product2(Name = 'GFTpath',ProductCode = 'GFTS-e',Product_ID__c ='IPl',
                                           Installment_NRC_Bill_Text__c='test',IsActive=true,ETC_Bill_Text__c='testingcode',
                                           MRC_Bill_Text__c='test123',NRC_Bill_Text__c='workingjours',
                                           Root_Product_Bill_Text__c='Dell',Service_Bill_Text__c='serviceline',
                                           RC_Credit_Bill_Text__c='credit',NRC_Credit_Bill_Text__c='NRcreditbill'); 
        insert product2obj;   
        PricebookEntry pricebookobj = new PricebookEntry(Pricebook2Id = Test.getStandardPricebookId(),Product2Id =  product2obj.Id,
                                                        IsActive = true, CurrencyIsoCode='USD',
                                                        UseStandardPrice=false, UnitPrice=0.01);
                
        insert pricebookobj; 
         OpportunityLineItem   opplineobj = new OpportunityLineItem(OpportunityId = oppobj.Id,IsMainItem__c = 'Yes',
                              Quantity = 5,UnitPrice = 10,PricebookEntryId = pricebookobj.Id,ContractTerm__c = '12',
                              OffnetRequired__c = 'Yes',Is_GCPE_shared_with_multiple_services__c = 'NA',CPQItem__c = '1', 
                              Installment_NRC_Amount_per_month__c= 12.23,OrderType__c ='Renewal',Parent_Bundle_Flag__c =false
                              ,is_bundle__c =true,Purchase_Requisition_Number__c='222',Parent_Charge_ID__c=null,Site__c=siteobj.Id,
                               Site_B__c=siteobj.Id) ;
        insert opplineobj;
       
        Contact contactobj=new Contact( AccountId = accobj.Id,Contact_Type__c = 'Technical',Country__c = cntryLkupObj.Id,
                                        Primary_Contact__c = true,MobilePhone = '123333',Phone = '1223',
                                        LastName = 'Talreja',FirstName = 'Dinesh');
        insert contactobj;  
         BillProfile__c bileprofileobj = new BillProfile__c( Billing_Entity__c = 'Telstra Corporation',CurrencyIsoCode = 'GBP',Payment_Terms__c = '30 Days',
                                        Start_Date__c = Date.today(),Invoice_Delivery_Method__c = 'Postal',Postal_Delivery_Method__c = 'First Class',
                                        Invoice_Breakdown__c = 'Summary Page',Billing_Contact__c = contactobj.id,Status__c = 'Approved',
                                        Payment_Method__c = 'Manual',Minimum_Bill_Amount__c = 10.20,First_Period_Date__c = date.today(),
                                        Rating_Rounding_Level__c = 2, Bill_Decimal_Point__c = 0,FX_Group__c = 'Corporate',
                                        FX_Rule__c = 'FX Rate at end of period',Account__c=accobj.Id);
        insert bileprofileobj;

        test.stopTest();           
        
    }
    */
}