/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class TestBillUpdateExtension {
   /* private static Account a;
    private static CostCentre__c c;
    private static BillProfile__c b;
    private static Billing_Update__c buParent;
    private static Billing_Update__c buChild;
    private static Order__c ordr;
    static testMethod void BillUpdateExtensionTest() {
        // TO DO: implement unit test
        
        BillUpdateExtension billUpEx=new BillUpdateExtension();
        
        a=getAccount();
        Opportunity opp=getOpportunity();
        opp.AccountId = a.Id;
        opp.StageName='Identify & Define';
        opp.Stage__c='Identify & Define';
        insert opp;
        b=getBillProfile();
        b.Account__c=a.Id;
        insert b;
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=a.id;
        serviceObj.Opportunity__c=opp.Id;
        serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        c=getCostCentre();
        c.BillProfile__c = b.Id;
        c.Cost_Centre_Status__c='Active';
        insert c;
        Resource__c res=getResourceObj();
        res.AccountId__c=a.id;
        res.CostCentre_Id__c=c.Id;
        res.Bill_Profile_Id__c=b.Id;
        insert res;
        billUpEx.accId=a.id;
        
        ordr=getOrder();
        ordr.Opportunity__c = opp.Id;
        insert ordr;
        Order_Line_Item__c orderLineItem=getOrderLineItem();
        orderLineItem.Service__c=serviceObj.Id;
        orderLineItem.ParentOrder__c = ordr.Id;
        orderLineItem.Opportunity_Name__c=opp.Id;
        insert orderLineItem;
        BillUpdateExtension.BillUpdateVO bilObj=new BillUpdateExtension.BillUpdateVO();
        billUpEx.billingUpdateList=new List<BillUpdateExtension.BillUpdateVO>();
        buParent=getBillUpdateparentObj();
        buParent.Bill_Update_Against__c=serviceObj.Id;
        buParent.Account__c=a.Id; 
        insert buParent;
        buChild=getBillUpdateChildObj();
        buChild.Bill_Update_Service__c=buParent.Id;
        buChild.Bill_Update_Against__c=buParent.Bill_Update_Against__c;
        buChild.Resource_Service_Id__c=serviceObj.Id;
        buChild.Cost_Centre_integration_number__c=c.Cost_Centre_integration_number__c;
        insert buChild;
        PageReference pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('id',a.id);
        Test.setCurrentPage(pageTest2);
        try{
        test.startTest();
        bilObj.CustomerPONumber=buChild.Customer_PO_Number__c;
        bilObj.BillProfileIntegrationNumber=buChild.Bill_Profile_Integration_Number__c;
        //billUpdateObj.Bill_Update_Against__c=billupdateServiceObj.Bill_Update_Against__c;
        bilObj.ETCBillText=buChild.ETC_Bill_Text__c;
        bilObj.MRCBillText=buChild.MRC_Bill_Text__c;
        bilObj.NRCBillText=buChild.NRC_Bill_Text__c;
        bilObj.rootProductBillText=buChild.Root_Product_Bill_Text__c;
        bilObj.ServiceStatus=buChild.Service_Status__c;
        bilObj.RCCreditText=buChild.RC_Credit_Bill_Text__c;
        bilObj.NRCBillText=buChild.NRC_Credit_Bill_Text__c;
        bilObj.serviceBillText=buChild.Service_Bill_Text__c;
        bilObj.parentOfthebundleflag=true;
        bilObj.bundleFlag=true;
        //billUpdateObj.Service__c=buVO.parentService;
        bilObj.bundleLabel=buChild.Bundle_Label__c;
        bilObj.recurringCreditBillText=buChild.Recurring_Credit_Bill_Text__c;
        bilObj.OneOffCreditBillText=buChild.One_Off_Credit_Bill_Text__c;
        bilObj.RootProductID=buChild.Root_Product_ID__c;
        billUpEx.billingUpdateList.add(bilObj);
       
        billUpEx.billUpdateObj=new Billing_Update__c();
        billUpEx.ServiceID=serviceObj.Id;
        billUpEx.billUpdateObj.Service__c=serviceObj.Id;
        billUpEx.billUpdatevo=new BillUpdateExtension.BillUpdateVO();
        billUpEx.insertIntoBillingUpdate();
        billUpEx.cancel();
        Apexpages.StandardController stdController = new Apexpages.StandardController(a);
        BillUpdateExtension BillExt1=new BillUpdateExtension(stdController);
        billUpEx.getServiceDetails();
            
        }catch(Exception ex){
        system.debug('=======Exception===='+ex);    
            
            
        } try{
        
        bilObj.CustomerPONumber=buChild.Customer_PO_Number__c;
        bilObj.BillProfileIntegrationNumber=buChild.Bill_Profile_Integration_Number__c;
        //billUpdateObj.Bill_Update_Against__c=billupdateServiceObj.Bill_Update_Against__c;
        bilObj.ETCBillText='';
        bilObj.MRCBillText='';
        bilObj.NRCBillText='';
        bilObj.rootProductBillText='';
        bilObj.ServiceStatus=buChild.Service_Status__c;
        bilObj.RCCreditText='';
        bilObj.NRCBillText='';
        bilObj.serviceBillText='';
        bilObj.parentOfthebundleflag=true;
        bilObj.bundleFlag=true;
        //billUpdateObj.Service__c=buVO.parentService;
        bilObj.bundleLabel=buChild.Bundle_Label__c;
        bilObj.recurringCreditBillText=buChild.Recurring_Credit_Bill_Text__c;
        bilObj.OneOffCreditBillText=buChild.One_Off_Credit_Bill_Text__c;
        bilObj.RootProductID=buChild.Root_Product_ID__c;
        billUpEx.billingUpdateList.add(bilObj);
       
        billUpEx.billUpdateObj=new Billing_Update__c();
        billUpEx.ServiceID=serviceObj.Id;
        billUpEx.billUpdateObj.Service__c=serviceObj.Id;
        billUpEx.billUpdatevo=new BillUpdateExtension.BillUpdateVO();
        billUpEx.insertIntoBillingUpdate();
        billUpEx.cancel();
        
        pageTest2 = Page.TestCustomerCreditPage;
        pageTest2.getParameters().put('id',buParent.Id);
        Test.setCurrentPage(pageTest2);
        billUpEx.getBillUpdateDetails();
        List<String> strLst=new List<String>();
        strLst.add(buParent.Id);
        BillUpdateExtension.sendBillingUpdateToROC(strLst,false);
        billUpEx.getServiceDetails();
        
        test.stopTest();        
        }catch(Exception ex){
        system.debug('=======Exception===='+ex);    
            
            
        }
    }
     public static Billing_Update__c getBillUpdateparentObj(){
    Billing_Update__c billupdateServiceObj=new Billing_Update__c();
    billupdateServiceObj.static_flag__c = True;
        
    return billupdateServiceObj;
    }
    
     public static Billing_Update__c getBillUpdateChildObj(){
    Billing_Update__c billUpdateObj=new Billing_Update__c();
   // billUpdateObj.Bill_Update_Service__c=billupdateServiceObj.Id;
    billUpdateObj.Customer_PO_Number__c='423423';
    billUpdateObj.Bill_Profile_Integration_Number__c='25252';
    //billUpdateObj.Bill_Update_Against__c=billupdateServiceObj.Bill_Update_Against__c;
    billUpdateObj.ETC_Bill_Text__c='tretettterte';
    billUpdateObj.MRC_Bill_Text__c='tretettterte';
    billUpdateObj.NRC_Bill_Text__c='tretettterte';
    billUpdateObj.Root_Product_Bill_Text__c='tretettterte';
    billUpdateObj.Service_Status__c='Active';
    billUpdateObj.RC_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.NRC_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.Service_Bill_Text__c='tretettterte';
    billUpdateObj.Parent_of_the_bundle_flag__c=true;
    billUpdateObj.Bundle_Flag__c=true;   
    //billUpdateObj.Service__c=buVO.parentService;
    billUpdateObj.Bundle_Label__c='Test Bundle';
    billUpdateObj.Recurring_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.One_Off_Credit_Bill_Text__c='tretettterte';
    billUpdateObj.Root_Product_ID__c='test root product Id';
   /* billUpdateObj.Billing_Entity__c=buVO.BillingEntity;
    billUpdateObj.Charge_Currency__c=buVO.ChargeCurrency;
    billUpdateObj.Primary_Service_ID__c=buVO.PrimaryServiceID;
    billUpdateObj.Parent_ID__c=buVO.ParentServiceID;
    billUpdateObj.Charge_id__c=buVO.ChargeID;
    billUpdateObj.NRC_Charge_Id__c=buVO.NRCChargeID;
    billUpdateObj.Pin_Service_ID__c=buVO.PINServiceID;
    billUpdateObj.Usage_Base_Flag__c=buVO.UsageFlag;
    billUpdateObj.Order_Line_Item_name__c=buVO.Name;
    billUpdateObj.Service_Resource__c=buVO.serviceResource;
    billUpdateObj.Resource_Service_Id__c=buVO.ResourceServiceId;
    billUpdateObj.isService__c=buVO.isService;
    billUpdateObj.Parent_Bundle_ID__c =buVO.parentBundleID;
    billUpdateObj.Bundle_Action__c =buVO.bundleAction;*/
        
  /*  return billUpdateObj;
    }
     public static Service__c getServiceObj(){
      Service__c ser = new Service__c();
        ser.name='serv1';
        ser.Path_Instance_ID__c='1467000';
        ser.Parent_Path_Instance_id__c = '';
        ser.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
        ser.is_this_data_from_PSL__c = True;
        ser.Parent_Charge_Id__c='123456';
      
      return ser;
 }
  public static Account getAccount(){
        if(a == null){
            a = new Account();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Customer_Legal_Entity_Name__c = 'Sample Name';
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = True;
            a.Account_ID__c = '5555';
            a.Account_Status__c = 'Active';
            insert a;
        }
        return a;
    }
    public static CostCentre__c getCostCentre(){
    if(c == null) {
        c = new CostCentre__c();
        c.Name= 'test cost';
        
   
    }
        return c;
}
public static BillProfile__c getBillProfile(){
        b = new BillProfile__c();
        b.Billing_Entity__c = 'Telstra Limited';
        b.Status__c='Active';
        
        return b;
    }
 public static Resource__c getResourceObj(){
      Resource__c res = new Resource__c();
        res.name='resource';
        res.Path_Instance_ID__c='1467000';
        res.Parent_Path_Instance_id__c = '';
      return res;
 }
 
  public static Order_Line_Item__c getOrderLineItem(){
        Order_Line_Item__c oli = new Order_Line_Item__c();
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'New Provide';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.CPQItem__c = '1';
        oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        oli.MRC_Bill_Text__c='rgtetertetee';
        oli.Parent_Bundle_ID__c='123456';
        return oli;
    }     
     private static Order__c getOrder(){
    if(ordr == null){        
        ordr = new Order__c();
      }
        return ordr;
    }
    public static Opportunity getOpportunity(){
            
                Opportunity opp = new Opportunity();
                opp.Name = 'Test Opportunity';
                 opp.StageName = 'Unqualified Prospect';
                 opp.Stage__c='Unqualified Prospect';
                opp.CloseDate = System.today();
                opp.Estimated_MRC__c=800;
                opp.Estimated_NRC__c=1000;
                opp.ContractTerm__c='10';
                opp.SOF_signed_by_customer__c = True;
                opp.QuoteStatus__c = 'Order';
                return opp;
        }   
        */    
}