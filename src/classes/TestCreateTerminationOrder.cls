/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /**
 * Name : CreateTerminationOrderExt_Nova_Test
 * Author : NovaCop Unit Test Generator
 * Description : Test class used for testing the CreateTerminationOrderExt
 * Date : 5/12/15 3:07 PM 
 * Version : <intial Draft> 
 * TODO : This code is auto-generated. Developer should update the inputs for unit tests as well as expected results in assertions
 */
@isTest(seealldata=true)

private class TestCreateTerminationOrder {
/*
     *@name testCreateTerminationOrder_Scenario1()
     *@return void 
     *@description This method for createTerminationOrder () In CLASS CreateTerminationOrderExt.cls
     */
   /*  static testMethod void  testCreateTerminationOrder_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Account aa=getAccount();
        Test.startTest();
        insert aa;
        
        PageReference pageTest2 = Page.CreateTerminationOrderPage;
        pageTest2.getParameters().put('Id',aa.id);
        Service__c serviceObj= new Service__c();
        serviceObj=getServiceObj();
        serviceObj.AccountId__c=aa.id;
        //serviceObj.Opportunity__c=opp.Id;
        //serviceObj.Bundle_Flag__c=true;
        insert serviceObj;
        Test.setCurrentPage(pageTest2);
        
        System.runAs(stdUser) {
             ApexPages.StandardController conTest = new ApexPages.StandardController(aa);
            //PageReference pagereferenceTest = createTerminationOrderExtTest.createTerminationOrder();
            Test.stopTest();
             System.assert(conTest!=null);
        }
    }
    /*
     *@name testCreateActionItemToBillingTeam_Scenario1()
     *@return void 
     *@description This method for createActionItemToBillingTeam () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testCreateActionItemToBillingTeam_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            PageReference pagereferenceTest = createTerminationOrderExtTest.createActionItemToBillingTeam();
            Test.stopTest();
             System.assert(pagereferenceTest!=null);
        }
    }
    /*
     *@name testCancel_Scenario1()
     *@return void 
     *@description This method for cancel () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testCancel_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            PageReference pagereferenceTest = createTerminationOrderExtTest.cancel();
            Test.stopTest();
             System.assert(pagereferenceTest!=null);
        }
    }*/
    /*
     *@name testGetAllFieldQuery_Scenario1()
     *@return void 
     *@description This method for getAllFieldQuery () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testGetAllFieldQuery_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Set<string> IdSetTest1 = new Set<string>{'testString'};

        String ObjectNameTest = 'testString';

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            String stringTest = createTerminationOrderExtTest.getAllFieldQuery(ObjectNameTest,IdSetTest1);
            Test.stopTest();
             System.assert(stringTest!=null);
        }
    }
    /*
     *@name testGetAllFieldQuery_Scenario2()
     *@return void 
     *@description This method for getAllFieldQuery () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testGetAllFieldQuery_Scenario2(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Set<string> IdSetTest1 = new Set<string>{'testString'};

        String ObjectNameTest = 'Related_ParentId_s_to_Service__c';

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            String stringTest = createTerminationOrderExtTest.getAllFieldQuery(ObjectNameTest,IdSetTest1);
            Test.stopTest();
             System.assert(stringTest!=null);
        }
    }
    /*
     *@name testGetAllFieldQuery_Scenario3()
     *@return void 
     *@description This method for getAllFieldQuery () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testGetAllFieldQuery_Scenario3(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Set<string> IdSetTest1 = new Set<string>{'testString'};

        String ObjectNameTest = 'Product_Configuration__c';

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            String stringTest = createTerminationOrderExtTest.getAllFieldQuery(ObjectNameTest,IdSetTest1);
            Test.stopTest();
             System.assert(stringTest!=null);
        }
    }
    /*
     *@name testGetAllFieldQuery_Scenario4()
     *@return void 
     *@description This method for getAllFieldQuery () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testGetAllFieldQuery_Scenario4(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        Set<string> IdSetTest1 = new Set<string>{'testString'};

        String ObjectNameTest = 'Product_Configuration__c1';

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            String stringTest = createTerminationOrderExtTest.getAllFieldQuery(ObjectNameTest,IdSetTest1);
            Test.stopTest();
             System.assert(stringTest!=null);
        }
    }
    /*
     *@name testInsertDynDataInBody_Scenario1()
     *@return void 
     *@description This method for insertDynDataInBody () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testInsertDynDataInBody_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        List<Action_Item__c> actionItemListTest1 = new List<Action_Item__c>{new Action_Item__c()};

        String bodyTest = 'testString';

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            createTerminationOrderExtTest.insertDynDataInBody(actionItemListTest1,bodyTest);
            Test.stopTest();
             System.assert(true);
        }
    }
    /*
     *@name testSendMail_Scenario1()
     *@return void 
     *@description This method for sendMail () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testSendMail_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        CreateTerminationOrderExt.EMailVO prETest = new CreateTerminationOrderExt.EMailVO();

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            Boolean booleanTest = createTerminationOrderExtTest.sendMail(prETest);
            Test.stopTest();
             System.assert(booleanTest!=false);
        }
    }
    /*
     *@name testGetEmailList_Scenario1()
     *@return void 
     *@description This method for getEmailList () In CLASS CreateTerminationOrderExt.cls
     */
    /* static testMethod void  testGetEmailList_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
        List<Action_Item__c> actionItemListTest1 = new List<Action_Item__c>{new Action_Item__c()};

                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            createTerminationOrderExtTest.getEmailList(actionItemListTest1);
            Test.stopTest();
             System.assert(true);
        }
    }
    /*
     *@name testGetEmailAddess_Scenario1()
     *@return void 
     *@description This method for getEmailAddess () In CLASS CreateTerminationOrderExt.cls
     */
     /*static testMethod void  testGetEmailAddess_Scenario1(){ 
        User stdUser = UtilNovaUnitTest.getUser();
                ApexPages.StandardController conTest = new ApexPages.StandardController();
        CreateTerminationOrderExt createTerminationOrderExtTest  =  new  CreateTerminationOrderExt(conTest);
        Test.startTest();
        Test.setCurrentPage(Page.CreateTerminationOrderPage);
        System.runAs(stdUser) {
            String stringTest = createTerminationOrderExtTest.getEmailAddess();
            Test.stopTest();
             System.assert(stringTest!=null);
        }
    }
    */
     /*  public static Service__c getServiceObj(){
      Service__c ser = new Service__c();
        ser.name='serv1';
        ser.Path_Instance_ID__c='1467000';
        ser.Parent_Path_Instance_id__c = '';
        ser.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
        ser.is_this_data_from_PSL__c = True;
        ser.Parent_Charge_Id__c='123456';
      
      return ser;
 }
     public static Account getAccount(){
       
            Account a = new Account();
            a.Name = 'Test Account Test 1';
            a.Customer_Type__c = 'MNC';
            a.Customer_Legal_Entity_Name__c = 'Sample Name';
            a.Selling_Entity__c = 'Telstra INC';
            a.Activated__c = True;
            a.Account_ID__c = '5555';
            a.Account_Status__c = 'Active';
           
        
        return a;
    }
    */
}