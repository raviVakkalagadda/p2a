/**
@author - Accenture
@date - 27-Mar-2013
@version - 1.0
@description - This is the test class on CreateUpdateServiceOrResource Trigger.**/

@isTest(seeAllData = false)
public class TestCreateUpdateServiceOrResource{
 /*   public static Country_Lookup__c cl;
    public static Account a;
    public static City_Lookup__c city;
    public static Site__c s1;
    public static Order__c ordr;
    public static Opportunity o;
    
    /*static testMethod void createUpdateServiceOrResource1() {
        
       Test.StartTest();  
       // User user = [Select Id from User where isActive=true limit 1];
       
        List<Order_Line_Item__c> olilist1 = new List<Order_Line_Item__c>();
      //  List<Order_Line_Item__c> olilist2 = new List<Order_Line_Item__c>();
      //  List<Order_Line_Item__c> olilist3 = new List<Order_Line_Item__c>();
        
        Service__c ser1 = new Service__c();
        ser1.name='serv1';
        ser1.AccountId__c=getAccount().id; 
        ser1.Path_Instance_ID__c='1467000';
        ser1.Parent_Path_Instance_id__c = '213';
        ser1.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
       // ser1.Is_shared_with_multiple_services__c = 'Yes-New';
      
       // Test.StartTest();
        insert ser1;
        //Test.stopTest();
          
        Service__c ser2 = new Service__c();
        ser2.name='serv2';
        ser2.AccountId__c=getAccount().id; 
        ser2.Path_Instance_ID__c='213';
        ser2.Is_GCPE_shared_with_multiple_services__c  = 'NA';
       // ser2.Is_shared_with_multiple_services__c = 'NA';
       
       // insert ser2;
        
         Resource__c res = new Resource__c();
         res.name='res1';
         res.AccountId__c=getAccount().id;
         res.Path_Instance_ID__c='313';
         res.Parent_Path_Instance_ID__c='serv1';
         insert res;
       
     
        
        Order_Line_Item__c oli1 = getOrderLineItem();
        oli1.Path_Instance_Id__c = '213';
        oli1.Path_Status__c = 'Build&Test';
        oli1.Path_Type__c = 'testpathtype';
        
        
        oli1.Path_Status__c = 'IN Operation';
        oli1.Service__c = ser2.id;
        olilist1.add(oli1);
      
       
       Order_Line_Item__c olill = getOrderLineItem2();
       olill.Parent_Path_instance_id__c = '1234';
       olill.path_instance_id__c = '466';
      
       olilist1.add(olill);
      
       
     // Order_Line_Item__c oli3 = getOrderLineItem4();
       
     //  oli3.Path_Instance_Id__c ='1467000';
     //  oli3.Parent_path_instance_id__c ='213';
       
      Order_Line_Item__c oligcpe = getOrderLineItem1();
       
       
       oligcpe.Path_Instance_Id__c ='1467000';
       oligcpe.Parent_path_instance_id__c ='213';
       system.debug('oligcpe***'+oligcpe.OLIUpdateRequired__c);
   
   //  update oligcpe; 
     Test.StopTest();
              
   }
 
   
     private static Related_ParentId_s_to_Service__c getRelatedObj(Service__c childservice, Service__c parentService){
     
         Related_ParentId_s_to_Service__c relObj = new Related_ParentId_s_to_Service__c();
         relObj.Link_to_Child_Service__c = childservice.id;
         relObj.Link_to_Parent_Service__c = parentService.id;
         relObj.Main_Parent_Service_Id__c = 'MainParent';
         
         
         insert relObj;
         return relObj;
     
     
     }
     private static Opportunity getOpportunity(){
         if(o == null){
          
          o = new Opportunity();
          o.Name = 'UnitTest Opp';
          o.AccountId = getAccount().Id;
          o.StageName = 'Identify & Define';
          o.Stage__c='Identify & Define';
          o.CloseDate = System.today();
          o.Estimated_MRC__c=4;
          o.Estimated_NRC__c=1;
          insert o;
        }
          return o;
     
     
     }    
        
    private static Country_Lookup__c getCountry(){
        if(cl == null){ 
            cl = new Country_Lookup__c();
            cl.CCMS_Country_Code__c = 'US';
            cl.CCMS_Country_Name__c = 'USA';
            cl.Country_Code__c = 'US';
            insert cl;
        }
    return cl;
    } 
        
    private static Account getAccount(){
        RecordType rt = [SELECT Id,Name FROM RecordType WHERE SobjectType='Account' LIMIT 1];
        if(a == null){    
            a = new Account();
            cl = getCountry();
            a.Name = 'Test Account 1234567890';
            a.Customer_Type__c = 'MNC';
            a.Country__c = cl.Id;
            a.Selling_Entity__c = 'Telstra INC';
            a.Type='Customer';
            a.Account_Manager__c = 'Albert Thomas';
            a.Activated__c = true;
            a.RecordTypeId = rt.Id;
            a.Account_Status__c ='Active';
            a.Account_ID__c ='1234';
            a.Customer_Legal_Entity_Name__c = 'Telstra Incorporation';
            a.Is_updated__c = true;
            a.Postal_Code__c = '123456789';
            insert a;
        }
     return a;
     }
     private static Order__c getOrder(){
     
         if(ordr == null){
            
            ordr = new Order__c();
            
            ordr.Opportunity__c = getOpportunity().Id;
            ordr.Account__c = getAccount().Id;
            ordr.OLIUpdateRequired__c = true;
            insert ordr;
            
          }
            return ordr;
            
            
     }
     private static Product2  getProduct1(){
            
            Product2 prod = new Product2();
            prod.name = 'Test Product';
            prod.CurrencyIsoCode='EUR';
            prod.Product_ID__c='GCPE';      
            prod.Create_Resource__c= false;
            insert prod;
            return prod;
            
     }
     private static Product2  getProduct2(){
        
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPVPN';        
            prod1.Create_Resource__c=false;
            insert prod1;
            return prod1;
        
     }
     private static Product2  getProduct3(){
        
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='LLOOP';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            return prod1;
        
     }
     private static Product2 getProduct4(){
            
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPLSHC';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            return prod1;
        
     
     }
     private static Product2 getProduct5(){
            
            Product2 prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPLSHC-STRM';        
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            return prod1;
        
     
     }
     
     private static City_Lookup__c getCity(){
        if(city == null){ 
            city = new City_Lookup__c();
            city.Name = 'Bangalore';
            city.City_Code__c= 'BNG1';
            insert city;
        }
     return city;
     } 
       
     private static Site__c getSite()
     {
        if( s1== Null)
        {
          s1 = new Site__c();
          
          //Account acc = getAccount();
          s1.Name='Site 1';
          s1.AccountId__c=getAccount().Id; 
          s1.Address1__c='Address1';
          s1.Address2__c='Address2';
          s1.City_Finder__c = getCity().Id;
          s1.Country_Finder__c = getCountry().Id;
          s1.Address_Type__c='Sale Address';
          s1.Valid__c = true;
          s1.isOppUpdate__c = true;
          insert s1;
        }
      return s1; 
      } 
        
      private static Order_Line_Item__c getOrderLineItem(){
            
            Order_Line_Item__c oli1 = new Order_Line_Item__c();
            oli1.ParentOrder__c = getOrder().id;
            oli1.Product__c = getProduct2().id;
            oli1.Opportunity_Line_Item_ID__c= '1111';
            oli1.OrderType__c ='New Provide';
            oli1.CPQItem__c = '1';
            oli1.ContractTerm__c='30';
            oli1.Is_GCPE_shared_with_multiple_services__c = 'NA';
          //  oli1.Is_shared_with_multiple_services__c = 'Yes-New';
            
            
            insert oli1;
            return oli1;
      }
      private static Order_Line_Item__c getOrderLineItem1(){
            
            Order_Line_Item__c oli2 = new Order_Line_Item__c();
            oli2.ParentOrder__c = getOrder().id;
            oli2.Product__c = getProduct1().id;
            oli2.Opportunity_Line_Item_ID__c= '1111';
            oli2.OrderType__c ='New Provide';
            oli2.CPQItem__c = '1.1.1';
            oli2.ContractTerm__c='30';
            oli2.ParentCPQItem__c ='1';
            oli2.Is_GCPE_shared_with_multiple_services__c  = 'Yes-New';
          // oli2.Is_shared_with_multiple_services__c = 'Yes-New';
            insert oli2;
            return oli2;
      }
      private static Order_Line_Item__c getOrderLineItem2(){
            
            Order_Line_Item__c oli2 = new Order_Line_Item__c();
            oli2.ParentOrder__c = getOrder().id;
            oli2.Product__c = getProduct3().id;
            oli2.Opportunity_Line_Item_ID__c= '1111';
            oli2.OrderType__c ='Termination';
            oli2.CPQItem__c = '1.1.1.1';
            oli2.ParentCPQItem__c ='1';
            oli2.ContractTerm__c='30';
            oli2.Is_GCPE_shared_with_multiple_services__c = 'NA';
           // oli2.Is_shared_with_multiple_services__c = 'NA';
            insert oli2;
            return oli2;
      }*/
      
          
          
}