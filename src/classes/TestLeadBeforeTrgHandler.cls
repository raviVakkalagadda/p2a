@isTest(SeeAllData = false)
public class TestLeadBeforeTrgHandler 
{
    static testmethod void TestLeadBeforeTrgHandler1()
    {
        Test.startTest();
         Lead leadObj = new Lead(LastName='Test', Company='Testing', Status='Marketing Qualified Lead (MQL)',
                               Disqual_Reason__c='NA', LeadSource = 'eDM', Industry='BPO',
                               Country_Picklist__c= 'BELGIUM', Email='test@test1234.com');
        insert leadObj;
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
        List<Country_Lookup__c> c = [Select id,name from Country_Lookup__c where name = 'HK'];
        system.assertequals(cntryLkupObj.name,c[0].name);
        system.assert(cntryLkupObj!=null);
   
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='34987',
                                    Account_ORIG_ID_DM__c = '34987', Customer_Legal_Entity_Name__c = 'test'); 
        insert accObj;
        Contact contObj = new Contact(Country__c=cntryLkupObj.id,AccountId=accObj.Id,LastName='tech',
                                     Email ='mohantesh1256@telstra.com');
        insert contObj;
        
       
        
        Test.stopTest();
        
    }
    
    static testmethod void TestLeadBeforeTrgHandler2()
    {
        Test.startTest();
        
        Lead leadObj = new Lead(LastName='Test', Company='Testing', Status='Marketing Qualified Lead (MQL)',
                               Disqual_Reason__c='NA', LeadSource = 'eDM', Industry='BPO',
                               Country_Picklist__c= 'UNITED STATES OF AMERICA', Email='test@test1234.com', 
                               City= 'NewYork', PostalCode ='123456789' ,Request_for_Conversion__c= true);
        insert leadObj;
        
         system.assert(leadObj!=null);       
        
        Test.stopTest();
    } 
    
    static testmethod void TestLeadBeforeTrgHandler3()
    {
        Test.startTest();
        
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='60');
        insert cntryLkupObj;
    
        Account accObj = new Account(name = 'abcd',Customer_Type_New__c ='GW',
                                    Selling_Entity__c = 'Telstra International Limited',Country__c =cntryLkupObj.Id,
                                    Industry = 'BPO',Activated__c= true, Account_Status__c= 'Active', Account_ID__c='12345',
                                    Account_ORIG_ID_DM__c = '12345', Customer_Legal_Entity_Name__c = 'test'); 
        insert accObj;
        system.assert(accObj!=null);
        Lead leadObj = new Lead(LastName='Test', Company='Testing', Status='Marketing Qualified Lead (MQL)',
                               Disqual_Reason__c='NA', LeadSource = 'eDM', Industry='BPO',
                               Email='test@test1234.com', Country_Picklist__c='ALBANIA',
                               City= NULL, Lead_Account_Id__c = '12345');
        insert leadObj;
        system.assert(leadObj!=null);
      
                
        
        Test.stopTest();
    } 
    
}