@isTest(SeeAllData=false)
public with sharing class TestMasterServiceToOrdersListCtrl {
    static testmethod void TestGetSerWrapLst() {
     P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
        csord__Service__c masterServ = new csord__Service__c(); 
        List<csord__Service__c> childServs = new List<csord__Service__c>(); 
        csord__Service__c childServ = new csord__Service__c();
        List<CSPOFA__Orchestration_Process__c> thisOrchProcLst = new List<CSPOFA__Orchestration_Process__c>();
      //  Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
           csord__Order_Request__c ordReqId = new csord__Order_Request__c(Name = 'Test request'
            , csord__Module_Version__c = 'dummy'
            , csord__Module_Name__c = 'dummy');
        insert ordReqId;
                CSPOFA__Orchestration_Process_Template__c orchprocessTemplist = new CSPOFA__Orchestration_Process_Template__c(name = 'Template',CSPOFA__Associated_Profile__c = 'Profile');
                            insert orchprocessTemplist;
        List<csord__Order__c> ordLst = new List<csord__Order__c>();
        List<csord__Order__c> newOrdLst = [select Id from csord__Order__c ORDER BY CreatedDate DESC limit 3];
        List<csord__Subscription__c> subLst = new List<csord__Subscription__c>();
        
        csord__Order__c order = new csord__Order__c(
                                                    name='Test Order', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order);
        csord__Order__c order2 = new csord__Order__c(
                                                    name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order2);

        csord__Order__c order3 = new csord__Order__c(
                                                    name='Test Order3', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2016,08,01));
        ordLst.add(order3);
        insert ordLst;

        csord__Subscription__c sub = new csord__Subscription__c(csord__order__c = ordLst.get(0).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub);
        
        csord__Subscription__c sub2 = new csord__Subscription__c(csord__order__c = ordLst.get(1).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub2);
        csord__Subscription__c sub3 = new csord__Subscription__c(csord__order__c = ordLst.get(2).Id,
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReqId.Id,
                                                    name = 'Some Sub');
        subLst.add(sub3);
        insert subLst;
        CSPOFA__Orchestration_Process__c thisOrchProc;

        masterServ.csordtelcoa__Service_Number__c = '21234-';// + xx;
        masterServ.Name = 'Test Master Serv 1234';
        //masterServ.Master_Service__c = '';
        masterServ.csord__Subscription__c = sub.Id;
        masterServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG';
        masterServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
        insert masterServ;
        
        for(integer xx=0;xx<3;xx++){
            childServ = new csord__Service__c();
            childServ.csordtelcoa__Service_Number__c = '1234-' + xx;
            childServ.Name = 'Test Serv 1234-' + xx;
            childServ.Master_Service__c = masterServ.Id;
            childServ.csord__Subscription__c = subLst.get(xx).Id;
            childServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
            childServ.csord__Order_Request__c = ordReqId.Id;//'a2bO00000018nkO';
            //childServ.csord__Order__c = newOrdLst.get(xx).Id;
            childServs.add(childServ);
        }
        insert childServs;

        

        /*for(csord__Service__c thisServ : childServs){
            thisServ.csord__Order__c = ordLst.get(0).Id;
            update thisServ;
        }*/

        for(integer xx=0;xx<3;xx++){
            thisOrchProc = new CSPOFA__Orchestration_Process__c();
            thisOrchProc.Order__c = ordLst.get(xx).Id;
            thisOrchProc.CSPOFA__Progress__c = '25%';
            thisOrchProc.CSPOFA__Status__c = 'In Progress';
            thisOrchProc.CSPOFA__State__c = 'ACTIVE';
          //  thisOrchProc.CSPOFA__Orchestration_Process_Template__c = [select Id from CSPOFA__Orchestration_Process_Template__c
          //                                                             where Name ='Order_New'].get(0).Id;
            thisOrchProc.CSPOFA__Orchestration_Process_Template__c = orchprocessTemplist.Id; 
            thisOrchProcLst.add(thisOrchProc);
        }

        Test.startTest();
        MasterServiceToOrdersListCtrl mastCtrl = new MasterServiceToOrdersListCtrl(new ApexPages.StandardController(masterServ));
        List<MasterServiceToOrdersListCtrl.ServiceDetailWrapper> mmWrap = mastCtrl.getserWrapLst();
        System.assert(mmWrap != null);
        //System.assertEquals(mmWrap.get(0).Progress, '25%');
        Test.stopTest();
    }
}