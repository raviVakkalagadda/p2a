@isTest(SeeAllData = true)
public with sharing class TestNewLogoWinBackHandler 
{
  static testmethod void TestNewLogoWinBackHandler1()
    {
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
                
        Test.startTest();   
                       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type ='Prospect';
        accObj.Account_Type__c = 'Prospect';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34567';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';
        
        insert accObj;
        //list<Country_Lookup__c> prod = [select id,Name from Country_Lookup__c where Name = 'abcd'];
        //system.assertEquals(accObj.Name , prod[0].Name);
        System.assert(accObj!=null); 
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;
               
               
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true);
        
        List<Opportunity> OppList = new List<Opportunity>();
        OppList.add(oppObj);
        insert OppList;      
        
        
        Test.stopTest();
    }
    
    static testmethod void TestNewLogoWinBackHandler2()
    {
        NewLogoWinBackHandler newLogoWinBackHandler = new NewLogoWinBackHandler();
                
        Test.startTest();   
                       
        Country_Lookup__c cntryLkupObj = new Country_Lookup__c(Name='HK',Country_Code__c ='34');
        insert cntryLkupObj;
        
        Account accObj = new Account();
        accObj.Name = 'abcd';
        accObj.Type ='Former Customer';
        accObj.Account_Type__c = 'Former Customer';
        accObj.Account_Status__c = 'Active';
        accObj.Account_ID__c = '34568';
        accObj.Activated__c = true;
        accObj.Account_Verified__c = true;
        accObj.Customer_Legal_Entity_Name__c ='test';
        accObj.Industry = 'Education';
        accObj.Country__c = cntryLkupObj.Id;
        accObj.Customer_Type_New__c ='GW';
        accObj.Selling_Entity__c = 'Telstra International Limited';
        accObj.Account_ORIG_ID_DM__c ='test';
        
        insert accObj;
        //list<Country_Lookup__c> prod1 = [select id,Name from Country_Lookup__c where Name = 'abcd'];
        //system.assertEquals(accObj.Name , prod1[0].Name);
        System.assert(accObj!=null); 
        
        accObj.Type ='Customer';
        accObj.Account_Type__c = 'Customer';
        update accObj;
               
        List<Opportunity> oppList = new List<Opportunity>();
        Opportunity oppObj = new Opportunity(name = 'GFTS Ph 2',AccountID =accObj.ID, Opportunity_Type__c='Simple',
                 CurrencyIsoCode ='USD', CloseDate = Date.today(),StageName ='Closed Won',Stage__c='Identify & Define',
                 QuoteStatus__c = 'Order', Sales_Status__c = 'Open', Win_Loss_Reasons__c='Product', 
                 Order_Type__c ='Renewal',ContractTerm__c = '24',Action_Item_Created_SD__c=true);
        
        OppList.add(OppObj);
        insert OppList;
        
        
        
        Test.stopTest();
    }
    
}