@isTest 
public class TestOrderAndServiceFactory {
    private static List<CSPOFA__Orchestration_Process__c> orchprocesslist;
    private static List<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist;
    
   @testSetup
      static void SetupTestData(){
            //TestDataFactory.createProductDefinitionIdCustomSettings(TestDataFactory.createProductDefinition('Test Prod', 'Test Desc', true));
    
    }  
    static void TestmakeOrderAndServicesMapFromOrderAndServicesIdSet(){
           
      List<NewLogo__c> NewLogolist = new List<NewLogo__c>{
                    new NewLogo__c(name = 'Active',Action_Item__c = 'Active'),
                    new NewLogo__c(name = 'Action_Item__c',Action_Item__c = 'Action_Item__c'),
                    new NewLogo__c(name = 'Logo_Approved',Action_Item__c = 'Logo_Approved'),
                    new NewLogo__c(name = 'Logo_Customer',Action_Item__c = 'Logo_Customer'),
                    new NewLogo__c(name = 'Logo_Former_Customer',Action_Item__c = 'Logo_Former_Customer'),
                    new NewLogo__c(name = 'Logo_New_Customer',Action_Item__c = 'Logo_New_Customer'),
                    new NewLogo__c(name = 'Logo_New_Logo',Action_Item__c = 'Logo_New_Logo'),
                    new NewLogo__c(name = 'Logo_Prospect',Action_Item__c = 'Logo_Prospect'),
                    new NewLogo__c(name = 'Logo_Rejected',Action_Item__c = 'Logo_Rejected')  
       };
       Insert NewLogolist;
       system.assertEquals(NewLogolist[0].name,'Active');
       system.assert(NewLogolist!=null);
       
      List<cscfga__Product_Basket__c> pblist = P2A_TestFactoryCls.getProductBasket(9);    
       //Creating Account object records
        Account act = new Account(Name='Test112233M1',Customer_Type__c='MNC',Selling_Entity__c ='Telstra Incorporated',cscfga__Active__c='Yes',
                Activated__c= True,
                Account_ID__c = '33331',
                Account_Status__c = 'Active',
                Customer_Legal_Entity_Name__c = 'Test');
        insert act;
        system.assert(act!=null);
        system.assertEquals(act.Name,'Test112233M1');
        
        cscfga__Configuration_Offer__c csfa = new cscfga__Configuration_Offer__c(name='Master  VPLS VLAN Service',cscfga__Active__c =true,cscfga__Template__c=false);
        insert csfa;
        
        List<Offer_Id__c> offIdlist = new List<Offer_Id__c>{
                 new Offer_Id__c(name ='Master_IPVPN_Service_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_Transparent_Offer_Id',Offer_Id__c = csfa.id),
                 new Offer_Id__c(name ='Master_VPLS_VLAN_Offer_Id',Offer_Id__c = csfa.id)
       };
       insert offIdlist;
       //integer i=[Select count() from Offer_Id__c];
       system.assert(offIdlist!=null);
       
        //Creating Opportunity object records
        Opportunity oppObj = new Opportunity(Name='GFTS Ph 2',AccountID=act.Id,Opportunity_Type__c='Simple',
                                           CurrencyIsoCode = 'USD', CloseDate = Date.today(),StageName='Identify & Define',Stage__c='Identify & Define',
                                            QuoteStatus__c ='Approved',Sales_Status__c= 'Won',Win_Loss_Reasons__c ='Product',
                                            Order_Type__c= 'New', ContractTerm__c = '24',Estimated_MRC__c=2.00,Product_Type__c='test');
        insert oppObj; 
        system.assertEquals(oppObj.Name,'GFTS Ph 2');
        //insert contact object records
         Contact contObj = new Contact(AccountId=act.Id,LastName='tech',
                                  email='test@gmail.com');
        insert contObj; 
        //insert Product Basket object record
        cscfga__Product_Basket__c pbasket = new cscfga__Product_Basket__c();
        pbasket.csbb__Account__c = act.id;
        pbasket.csordtelcoa__Account__c = act.id;
        pbasket.cscfga__Opportunity__c = oppObj.id;
       
        insert pbasket;
        system.assert(pbasket!=null);
        
        //insert product definition object record
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN Service';
        pd.cscfga__Description__c ='112312331';
        insert pd;
        system.assertEquals(pd.name , 'Master IPVPN Service');
        
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'Master VPLS Service';
        pd1.cscfga__Description__c ='Test master VPLS';
        insert pd1;
        system.assertEquals(pd1.name , 'Master VPLS Service');
        Product_Definition_Id__c setting = new Product_Definition_Id__c();
        setting.Name = 'VLANGroup_Definition_Id';
        setting.Product_Id__c = pd.ID;
        insert setting;
        //insert custom setting records
        Product_Definition_Id__c pd6 = new Product_Definition_Id__c();
        pd6.name = 'IPVPN_Port_Definition_Id';
        pd6.Product_Id__c =pd1.ID;
        insert pd6;
        Product_Definition_Id__c pd6ad = new Product_Definition_Id__c();
        pd6ad.name = 'ASBR_Definition_Id';
        pd6ad.Product_Id__c =pd1.ID;
        insert pd6ad;
        Product_Definition_Id__c pd6n = new Product_Definition_Id__c();
        pd6n.name = 'IPC_Definition_Id';
        pd6n.Product_Id__c =pd1.ID;
        insert pd6n;
        Product_Definition_Id__c pd7 = new Product_Definition_Id__c();
        pd7.name = 'SMA_Gateway_Definition_Id';
        pd7.Product_Id__c =pd.id;
        insert pd7;
        Product_Definition_Id__c pd8 = new Product_Definition_Id__c();
        pd8.name = 'VPLS_VLAN_Port_Definition_Id';
        pd8.Product_Id__c =pd1.id;
        insert pd8;
        Product_Definition_Id__c pd9 = new Product_Definition_Id__c();
        pd9.name = 'VPLS_Transparent_Definition_Id';
        pd9.Product_Id__c =pd.id;
        insert pd9;
        Product_Definition_Id__c pd2 = new Product_Definition_Id__c();
        pd2.name = 'Master_IPVPN_Service_Definition_Id';
        pd2.Product_Id__c =pd1.id;
        insert pd2;
        Product_Definition_Id__c pdk = new Product_Definition_Id__c();
        pdk.name = 'Master_VPLS_Service_Definition_Id';
        pdk.Product_Id__c =pd.id;
        insert pdk;
        
        //insert product configuration record for Master IPVPN Service
       List<cscfga__Product_Configuration__c> pclist= new List<cscfga__Product_Configuration__c>{
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd.id,cscfga__Product_Basket__c = pbasket.Id),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='IPVPN'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='Transparent Mode'),
            new cscfga__Product_Configuration__c(cscfga__Product_Definition__c = pd1.id,cscfga__Product_Basket__c = pbasket.Id,Solution_Type__c='VLAN Mode')

           };
        insert pclist;
        system.assert(pclist!=null);
     
        csord__Order_Request__c ordReq = new csord__Order_Request__c(name='Test Ord Req', csord__Module_Version__c='1.0', 
                                            csord__Module_Name__c = 'Test Module');
        insert ordReq;
        
        Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        

        CSPOFA__Orchestration_Process_Template__c thisProcTempl = new CSPOFA__Orchestration_Process_Template__c(name='Test Proc Templ',
                                                                  CSPOFA__Processing_Mode__c = 'Background');
        insert thisProcTempl;
        system.assertEquals(thisProcTempl.name,'Test Proc Templ');
        
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 2.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '1');
        insert thisStepTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl2 = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template2',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 4.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '2');
        insert thisStepTempl2;
        

        csord__Order__c order = new csord__Order__c(name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2017,01,01));
        insert order;

        csord__Subscription__c sub = new csord__Subscription__c(Name = 'Service from to on - SN-000004106', csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,csord__Order__c = order.Id);
        insert sub;
        
        List<Account> listAcco = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity>ListOppty = P2A_TestFactoryCls.getOpportunitys(1, listAcco);
        
        csord__Service__c someServ = new csord__Service__c();
        someServ.Name = 'Test service';
        someServ.Product_id__c= 'test';
        someServ.csordtelcoa__Service_Number__c = '21234-';
        someServ.AccountId__c = listAcco[0].id;
        someServ.Opportunity__c = ListOppty[0].id;
        someServ.Estimated_Start_Date__c = system.now().addDays(10);
        someServ.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
        someServ.Selling_Entity__c = 'Telstra ( Thailand ) Co., Limited';
        someServ.csord__Status__c = 'Service created';
        someServ.csord__Order__c = order.id;
        someServ.Z_Side_Site__c = 'ZZZZ';
        someServ.A_Side_Site__c = 'ZZZZ';
        someServ.csordtelcoa__Product_Configuration__c = pclist[0].Id;
        someServ.csordtelcoa__Product_Basket__c = pblist[0].Id;
        someServ.csord__Subscription__c = sub.Id;
        someServ.csord__Order_Request__c = ordReq.Id;
        someServ.IsIP006Enabled__c=true;        
        someServ.Bundle_Label_name__c ='test';
        insert someServ;
        system.assertEquals(someServ.Z_Side_Site__c , 'ZZZZ');
        
        csord__Service__c someServ1 = new csord__Service__c();
        someServ1.Name = 'Test service';
        someServ1.Product_id__c= 'test';
        someServ1.csordtelcoa__Service_Number__c = '21234-';
        someServ1.AccountId__c = listAcco[0].id;
        someServ1.Opportunity__c = ListOppty[0].id;
        someServ1.Estimated_Start_Date__c = system.now().addDays(10);
        someServ1.csord__Identification__c = 'Order_a24O0000000OZ7aIAG'; 
        someServ1.Selling_Entity__c = 'Telstra ( Thailand ) Co., Limited';
        someServ1.csord__Status__c = 'Service created';
        someServ1.csord__Order__c = order.id;
        someServ1.Z_Side_Site__c = 'ZZZZ';
        someServ1.A_Side_Site__c = 'ZZZZ';
        someServ1.csordtelcoa__Product_Configuration__c = pclist[0].Id;
        someServ1.csordtelcoa__Product_Basket__c = pblist[0].Id;
        someServ1.csord__Subscription__c = sub.Id;
        someServ1.csord__Order_Request__c = ordReq.Id;
        someServ1.Bundle_Label_name__c ='test';
        insert someServ1;
        system.assertEquals(someServ1.Z_Side_Site__c , 'ZZZZ');
        
        csord__Service_Line_Item__c newServiceLine = TestOrderandServiceFactory.createServiceLineItem('LineItem', someServ1, 'SLI2', ordReq , false);
        newServiceLine.Net_MRC_Price__c = 52;
        newServiceLine.Net_NRC_Price__c = 62; 
        newServiceLine.csord__Total_Price__c = 72;
        newServiceLine.csord__Service__c = someServ.id;
        newServiceLine.MISC_Charge_Amount__c = 82;
        newServiceLine.Is_Miscellaneous_Credit_Flag__c=false;
        insert newServiceLine; 
        system.assertEquals(newServiceLine.MISC_Charge_Amount__c , 82);
        
       orchprocessTemplist = new List<CSPOFA__Orchestration_Process_Template__c>{
                          new CSPOFA__Orchestration_Process_Template__c(name = 'Template',CSPOFA__Associated_Profile__c = 'Profile'),
                          new CSPOFA__Orchestration_Process_Template__c(name = 'Template1',CSPOFA__Associated_Profile__c = 'Profile1')  
                          };
        insert orchprocessTemplist;
        system.assertEquals(orchprocessTemplist[0].name , 'Template');
       
       orchprocesslist = new List<CSPOFA__Orchestration_Process__c>{
                          new CSPOFA__Orchestration_Process__c(name = 'Orchestration',CSPOFA__Orchestration_Process_Template__c = orchprocessTemplist[0].id),
                          new CSPOFA__Orchestration_Process__c(name = 'Orchestration1',CSPOFA__Orchestration_Process_Template__c = orchprocessTemplist[1].id)
       }; 
       insert orchprocesslist;
       system.assertEquals(orchprocesslist[0].name , 'Orchestration');
       
       CSPOFA__Orchestration_Step__c Csos = new CSPOFA__Orchestration_Step__c();
       Csos.name = 'OrchestrationStep';
       Csos.CSPOFA__Orchestration_Process__c = orchprocesslist[0].id; 
       Csos.CSPOFA__Status__c = 'NotCompleted';
       insert Csos;
       system.assertEquals(Csos.name,'OrchestrationStep');
        
    
        //csord__Order__c order = [select Id, Name, Customer_Required_Date__c from csord__Order__c].get(0);
        //csord__Service__c someServ = [select Id, Name from csord__Service__c].get(0);
        
        Test.startTest();
        OrderAndServiceFactory.makeOrderAndServicesMapFromOrderIdSet(new Set<Id>{order.Id});
        OrderAndServiceFactory.makeOrderAndServicesFromServicesBase(new List<Id>{someServ.Id});
        OrderAndServiceFactory.makeOrderAndServicesFromOrdersFuture(new Set<Id>{order.Id});
        OrderAndServiceFactory.makeOrderAndServicesFromServices(new List<Id>{someServ.Id});
        //Database.batchableContext bc;
        //new OrderAndServiceFactory.OrderAndServicesFromServicesWorker().work((Object)new List<Id>{someServ.Id},(Object)bc);
        //OrderAndServiceFactory.makeOrderAndServicesFromServices(new List<Id>{someServ.Id}); 
        Test.stopTest();
    }
  
  private static testMethod void doDynamicLookupSearchTest() {
        Integer userid = 0;
            disableAll(UserInfo.getUserId());
            
        TestmakeOrderAndServicesMapFromOrderAndServicesIdSet();
        system.assert(userid!=null);    
       
  }
  
  /**
     * Disables triggers, validations and workflows for the given user
     * @param userId Id
     */
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }
        else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
    }
    
    /**
     * Enables triggers, validations and workflows 
     * @param userId Id
     */
    private static void enableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }
        else {
            globalMute.Mute_Triggers__c = false;
            globalMute.Mute_Validations__c = false;
            globalMute.Mute_Workflows__c = false;
        }

        upsert globalMute;
        system.assert(globalMute!=null);
        
    }
     public static csord__Service_Line_Item__c createServiceLineItem(String name, csord__Service__c service, String identification, csord__Order_Request__c orderRequest, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        Country_Lookup__c country = new Country_Lookup__c(Name='XYZA',Region__c='AXYZA',Country_Code__c='XYA',CCMS_Country_Code__c='CCXA',CCMS_Country_Name__c='CCAX');
        insert country; 
        Account accobj= new Account(Name='Acc1',Customer_Type_New__c='GW',Selling_Entity__c='Telstra Corporation Limited',Account_Status__c='Active',Type='Customer',Country__c=country.Id,Activated__c=true,Account_ID__c='123',Customer_Legal_Entity_Name__c=name);       
        insert accobj;
        Site__c s=TestOrderandServiceFactory.createSite('Test Site', accobj, country, true);
       
        //insert s;
        Contact c=new Contact(LastName='SNOW',AccountId=accobj.id,Contact_Type__c='Billing',Country__c=country.id);
        insert c;
        BillProfile__c BP=new BillProfile__c(name='BP1',Bill_Profile_Site__c=s.id,Billing_Contact__c=c.id,Billing_Entity__c='Telstra Incorporated',Status__c='Active',Account__c=accobj.id);
        insert BP;
        CostCentre__c CS=new CostCentre__c(name='CS1',Cost_Centre_Code__c='4567',BillProfile__c=BP.id);
        insert CS;
        csord__Order_Request__c orderReq=new csord__Order_Request__c(name='OrderName',csord__Module_Name__c='SansaStark123683468Test',csord__Module_Version__c='YouknownothingJhonSwon',csord__Process_Status__c='Requested',csord__Request_DateTime__c = System.now());
        insert orderReq;
        csord__Subscription__c sub = new csord__Subscription__c(name='sub1',csord__Identification__c='Test-Catlyne-4238362',csord__Order_Request__c=orderReq.id);
        insert sub;  
        csord__Order__c ord = new csord__Order__c(csord__Identification__c='Test-Catlyne-4238362',RAG_Order_Status_RED__c=false,RAG_Reason_Code__c='',Jeopardy_Case__c=null,csord__Order_Request__c=orderReq.id);
         insert ord;

        //working with custom settings//
        
        cscfga__Product_Definition__c pd = new cscfga__Product_Definition__c();
        pd.name = 'Master IPVPN port';
        pd.cscfga__Description__c ='Test master IPVPN port';
        insert pd; 
        
        cscfga__Product_Definition__c pd1 = new cscfga__Product_Definition__c();
        pd1.name = 'ASBR';
        pd1.cscfga__Description__c ='Test master ASBR';
        insert pd1; 
        
        cscfga__Product_Definition__c pd2 = new cscfga__Product_Definition__c();
        pd2.name = 'IPC';
        pd2.cscfga__Description__c ='Test master IPC';
        insert pd2;

        cscfga__Product_Definition__c pd3 = new cscfga__Product_Definition__c();
        pd3.name = 'Trans';
        pd3.cscfga__Description__c ='Test master Trans';
        insert pd3;

        cscfga__Product_Definition__c pd4 = new cscfga__Product_Definition__c();
        pd4.name = 'VLan';
        pd4.cscfga__Description__c ='Test master VLan';
        insert pd4;         
        
        
        Product_Definition_Id__c pdIdCustomSetting = new Product_Definition_Id__c();
        pdIdCustomSetting.Name = 'IPVPN_Port_Definition_Id';
        pdIdCustomSetting.Product_Id__c =pd.Id;
        insert pdIdCustomSetting;
        
        Product_Definition_Id__c pdIdCustomSetting1 = new Product_Definition_Id__c();
        pdIdCustomSetting1.Name = 'ASBR_Definition_Id';
        pdIdCustomSetting1.Product_Id__c =pd1.Id;
        insert pdIdCustomSetting1; 
        
        Product_Definition_Id__c pdIdCustomSetting2 = new Product_Definition_Id__c();
        pdIdCustomSetting2.Name = 'IPC_Definition_Id';
        pdIdCustomSetting2.Product_Id__c =pd2.Id;
        insert pdIdCustomSetting2;
        
        Product_Definition_Id__c pdIdCustomSetting3 = new Product_Definition_Id__c();
        pdIdCustomSetting3.Name = 'VPLS_Transparent_Definition_Id';
        pdIdCustomSetting3.Product_Id__c =pd3.Id;
        insert pdIdCustomSetting3;
        
        Product_Definition_Id__c pdIdCustomSetting4 = new Product_Definition_Id__c();
        pdIdCustomSetting4.Name = 'VPLS_VLAN_Port_Definition_Id';
        pdIdCustomSetting4.Product_Id__c =pd4.Id;
        insert pdIdCustomSetting4;
        //custom setting code ends      
        csord__Service__c ser=new csord__Service__c();
        ser.Name='serv1';
        ser.csord__Order_Request__c=orderReq.id;
        ser.csord__Identification__c='Test-Catlyne-4238362';
        ser.csord__Subscription__c=sub.id;
        //ser.Acity_Side__c='';
        //ser.Zcity_Side__c='';
        ser.Product_Id__c='abc';
        ser.csord__Order__c=ord.id;
        ser.csordtelcoa__Replaced_Service__c=null;
        //ser.csordtelcoa__Product_Configuration__c='';
        insert ser; 
        system.assert(ser!=null);
        defaultValues.put('Name', name);
        defaultValues.put('csord__Service__c', ser.Id);
        defaultValues.put('Account_ID__c',accobj.id);
        defaultValues.put('Net_MRC_Price__c', 0);
        defaultValues.put('Net_NRC_Price__c', 0);
        defaultValues.put('MISC_Charge_Amount__c', 1);
        defaultValues.put('csord__Total_Price__c', 0);
        defaultValues.put('csord__Identification__c', identification);
        defaultValues.put('csord__Order_Request__c', orderRequest.Id);
        defaultValues.put('Is_Miscellaneous_Credit_Flag__c', true);
        defaultValues.put('Bill_Profile__c ', BP.id);
        //defaultValues.put('Cost_Centre__c', CS.id);
        
        //defaultValues.put('Account_ID__c', accobj.id);

        csord__Service_Line_Item__c sli = (csord__Service_Line_Item__c)createObject(Schema.SObjectType.csord__Service_Line_Item__c, defaultValues, persist);
       // delete country;
        return sli;
    }
     public static SObject createObject(Schema.DescribeSObjectResult objDesc, Map<String, Object> defaultValues, Boolean persist){
        SObject obj;

        if (objDesc.isCreateable()) {
            SObjectFactory factory = new SObjectFactory();
            obj = factory.create(objDesc, defaultValues);
            system.assert(factory!=null);
            if(persist){
                 Database.SaveResult result = Database.insert(obj, false);
                System.assert(result.isSuccess(), errorMessage('insert', result.getErrors(), obj));
            }
        }

        return obj;
    }
     private static String errorMessage(String operation, List<Database.Error> errors, SObject obj) {
        String objectName = String.valueOf(obj).split(':')[0];
        String msg = operation + ' operation failed for ' +  objectName;
        /*for (Database.Error error: errors) {
            msg += '\n  [' + String.join(error.getFields(), ', ') + ']: ' + error.getMessage();
        }*/
        system.assert(msg!=null);
        return msg;
    }
    public static Site__c createSite(String name, Account acc, Country_Lookup__c country, Boolean persist){
        Map<String, Object> defaultValues = new Map<String, Object>();
        defaultValues.put('Name', name);
        defaultValues.put('AccountId__c', acc.Id);
        defaultValues.put('Address1__c', '1 Main Street');
        defaultValues.put('Country_Finder__c', country.Id);
    defaultValues.put('Address_Type__c','Billing Address');
         
        Site__c site = (Site__c)createObject(Schema.SObjectType.Site__c, defaultValues, persist);
        system.assert(site!=null);
        return site;
    }

}