/**
    @author - Accenture
    @date - 20- Jun-2012
    @version - 1.0
    @description - This is the test class for OrderLineItemAfterUpdate Trigger.
*/

@isTest
private class TestOrderLineItemAfterUpdate {

   /* static testMethod void getOrderLineItemDetailsTest() {
        List<Task> testTask= new List<Task>();
        Order_Line_Item__c orderLineItem = getOrderLineItem();
        insert orderLineItem;
        
        Test.startTest();
        orderLineItem.OrderType__c = 'Cancel';
        update orderLineItem;
        UpdateChildLineItem.ischildOLIComplete=false;
        testTask = [SELECT Subject,Status,Priority,WhatId,Description FROM Task WHERE WhatId =: orderLineItem.ParentOrder__c];
        
       // System.assert(testTask.size() == 1);
        
        orderLineItem.OrderType__c ='Terminate';
        orderLineItem.Service_Assurance_Assigned_Date__c=date.newinstance(2012, 11, 28);
        orderLineItem.Service_Assurance_Contact__c = getcontact().Id;
        //orderLineItem.OrderType__c ='Terminate';
        orderLineItem.OrderType__c ='Complete';
        orderLineItem.Cascade_Line_Item_Completion__c  =True;
        //orderLineItem.Service_Assurance_Contact__c = '';
        update orderLineItem;
            
        testTask = [SELECT Subject,Status,Priority,WhatId,Description FROM Task WHERE WhatId =: orderLineItem.ParentOrder__c];
        
        //System.assert(testTask.size() == 2);
        
        Test.stopTest();
    }
    
    Private static User u;
    private static Country_Lookup__c cl;
    private static Product2 prod1;
    private static Account acc; 
    private static Contact con;
    private static Order__c ordr;
    private static Opportunity opp;
    
    private static Order_Line_Item__c getOrderLineItem(){
        Order_Line_Item__c oli = new Order_Line_Item__c();
        Order__c ord = getOrder();
        oli.ParentOrder__c = ord.Id;
        oli.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        oli.OrderType__c = 'New Provide';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        con = getcontact();
        //oli.Service_Assurance_Contact__c = con.Id;
        oli.Service_Assurance_Contact_Number__c = con.phone;
    oli.Service_Assurance_Assigned_Date__c = System.now();
    oli.Billing_Contact_Assigned_Date__c = System.now();
       // oli.Service_Assurance_Contact__c = '';
        oli.Service_Assurance_Assigned_Date__c = System.now();
        oli.Billing_Contact_Assigned_Date__c = System.now();
        oli.CPQItem__c = '1';
       oli.product__c = getProduct().id;
       oli.Is_GCPE_shared_with_multiple_services__c = 'NA';
        return oli;
    }


     private static Contact getcontact(){
     if(con == null){
        con = new Contact();
        con.Lastname ='TestName';
        con.contact_Type__c = 'Assurance';
        con.phone = '998877665';
        insert con;
        }
        return con;
    }
    
   
    private static Order__c getOrder(){
    if(ordr == null){        
        ordr = new Order__c();
        ordr.SD_PM_Contact__c = getUser().Id;
        Opportunity o = getOpportunity();
        ordr.Opportunity__c = o.Id;
        insert ordr;
        }
        return ordr;
    }
    
   
    private static Opportunity getOpportunity(){
    if(opp == null){
        opp = new Opportunity();
        Account a = getAccount();
         opp.Name = 'Test Opportunity';
         opp.AccountId = a.Id;
          //updated Stage name UnQualified prospect  as Identify & Define as per SOMP requirement
         opp.StageName = 'Identify & Define';
         opp.Stage__c='Identify & Define';
         opp.CloseDate = System.today();
         opp.Estimated_MRC__c=800;
         opp.Estimated_NRC__c=1000;
         opp.ContractTerm__c='10';
         opp.TigFin_PR_Actions__c = 'Processed';
        insert opp;
        }
        return opp;
    }
    
     private static Account getAccount(){
     if(acc == null){
        acc = new Account();
        cl = getCountry();
        acc.Country__c = cl.Id;
        acc.Name = 'Test Account';
        acc.Customer_Type__c = 'MNC'; 
        acc.account_Id__c = 'Test123';    
        acc.Selling_Entity__c = 'Telstra INC';
        acc.activated__c = true;
        acc.Account_Status__c ='Active';
        acc.Customer_Legal_Entity_Name__c = 'Telstra Incorporation';
        insert acc;
        }
        return acc;
    }
    
    private static Country_Lookup__c getCountry(){
    if(cl == null){
        cl = new Country_Lookup__c();
        cl.CCMS_Country_Code__c = 'IND';
        cl.CCMS_Country_Name__c = 'India';
        cl.Country_Code__c = 'IND';
        insert cl;
        }
        return cl;
    }
    
    private static Product2 getProduct(){
    if(prod1 == null){
            
            prod1 = new Product2();
            prod1.name = 'Test Product';
            prod1.CurrencyIsoCode='EUR';
            prod1.Product_ID__c='IPL';     
            prod1.ProductCode = 'GMNS';   
            prod1.Create_Resource__c=TRUE;
            insert prod1;
            }
            return prod1;
        
     
     }
    private static User getUser(){
     if(u == null)
     {
        u = new User();
        u.FirstName = 'Test';
        u.LastName = 'London1';
        u.Alias = 'Tlon';
        u.Email = 'Test.London@accenture.com1';
        u.Username = 'Test.London@accenture.com1.1.1.1';
        u.CommunityNickname = 'Test.London';
        u.TimeZoneSidKey = 'Australia/Sydney';
        u.LanguageLocaleKey = 'en_US';
        u.LocaleSidKey = 'en_AU';
        u.EmailEncodingKey = 'ISO-8859-1';
        u.EmployeeNumber = 'D779858';
        u.ProfileId = [SELECT Id, Name FROM Profile Where Name='System Administrator' limit 1].Id;
        insert u;
        }
        return u;
    }
    */
 
 
}