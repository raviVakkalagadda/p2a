@isTest(SeeAllData = true)
private class TestOrderLineItemBeforeUpdate {
    /*static Order__c ord;
    static Opportunity opp;
    static Order_Line_Item__c olit;
    static Order_Line_Item__c olit1;
    static Product2 prod;
    static Contact con;
    static Account acc;
    static testMethod void myUnitTest() {
        olit = getOrderLineItem();
        olit.Line_Item_Status__c = 'Complete';
        olit1 = getOrderLineItem1();
        olit1.Line_Item_Status__c = 'Complete';
        Test.StartTest();
         
         try{
            update olit1;   
         }
         catch(DMLException e){
            system.debug('Error' +e.getMessage());
         }   
         try{
            update olit;    
         }
         catch(DMLException e){
            system.debug('Error' +e.getMessage());
         }   
         Test.StopTest();
    }
    private static void getOpportunityLineItem(){
        
        //List<OpportunityLineItem> lst = new List<OpportunityLineItem>();
        
        OpportunityLineItem oli = new OpportunityLineItem();
        opp = getOpportunity();
        BillProfile__c b = getBillProfile();
        oli.OpportunityId = opp.Id;
        oli.IsMainItem__c = 'Yes';
        oli.OrderType__c = 'New Provide';
        oli.Quantity = 5;
        oli.UnitPrice = 10;
        oli.PricebookEntryId = getPriceBookEntry('Global IPVPN').Id;
        oli.ContractTerm__c = '12';       
        
        insert oli;
    }
    
    
     private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 LIMIT 1];
        return p;   
    }
    
    private static Product2 getProduct(String prodName){
        if(prod == null){
            prod = new Product2();
            prod.Name = prodName;
            prod.ProductCode = prodName;
            prod.Key_Milestone__c = true;
            prod.Supplier_Milestone__c = true;
            prod.Product_ID__c = 'GIP';
            insert prod;
        }
        return prod;
    }
    
    private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2Id =  getProduct(prodName).Id;
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p; 
        return p;    
    }
     private static BillProfile__c getBillProfile(){
        BillProfile__c b = new BillProfile__c();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = acc.Id;
        insert b;
        return b;
    }
      private static Order_Line_Item__c getOrderLineItem(){
        if(olit == null){
        olit = new Order_Line_Item__c();
        prod = getProduct('Global IPVPN');
        ord = getOrder();
        olit.ParentOrder__c = ord.Id;
        olit.Product__c = prod.Id;
        olit.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        olit.Order_Placed_with_Supplier__c = system.today();
        olit.Supplier_Bill_Start_Date__c = system.today();
        olit.Supplier_Cancel_Date__c = system.today();
        olit.Supplier_Commitment_Date__c = system.today();
        olit.Standard_Delivery_Date__c = system.today();
        olit.Supplier_Order_Accepted_Date__c = system.today();
        olit.Actual_Supply_Date__c = system.today();
        olit.OrderType__c = 'New Provide';
        olit.CPQItem__c = '1';
        olit.Line_Item_Status__c = 'New';
         olit.Is_GCPE_shared_with_multiple_services__c = 'NA';
        //oli.Key_Milestone__c ='';
      
        insert olit;
        }
        system.debug('Milestone *' +olit.Supplier_Milestone__c);
        system.debug('Product Id' +olit.Product__c);
        
        return olit;
    }
    private static Order_Line_Item__c getOrderLineItem1(){
        if(olit1 == null){
        olit1 = new Order_Line_Item__c();
        prod = getProduct('Global IPVPN');
        ord = getOrder();
        olit1.ParentOrder__c = ord.Id;
        olit1.Product__c = prod.Id;
        olit1.Opportunity_Line_Item_ID__c = 'Opp Line Item 001';
        olit1.Order_Placed_with_Supplier__c = system.today();
        olit1.Supplier_Bill_Start_Date__c = system.today();
        olit1.Supplier_Cancel_Date__c = system.today();
        olit1.Supplier_Commitment_Date__c = system.today();
        olit1.Standard_Delivery_Date__c = system.today();
        olit1.Supplier_Order_Accepted_Date__c = system.today();
        olit1.Actual_Supply_Date__c = system.today();
        olit1.OrderType__c = 'Terminate';
        olit1.CPQItem__c = '1';
        olit1.Line_Item_Status__c = 'New';
         olit1.Is_GCPE_shared_with_multiple_services__c = 'NA';
        //oli.Key_Milestone__c ='';
      
        insert olit1;
        }
        system.debug('Milestone *' +olit1.Supplier_Milestone__c);
        system.debug('Product Id' +olit1.Product__c);
        
        return olit1;
    }
    private static Order__c getOrder(){
        if(ord == null){
            ord = new Order__c();
            Opportunity o = getOpportunity();
            acc = getAccount();
            ord.Opportunity__c = o.Id;
            ord.Account__c = acc.Id;
            insert ord; 
            
        }
        return ord;
    }
    private static Contact getContact(){
        if(con == null){
            acc = getAccount();
            Country_Lookup__c c = getCountry();
            con = new Contact();
            con.LastName = 'Tech';
            con.AccountId = acc.Id;
            con.Contact_Type__c = 'Technical';
            con.Country__c = c.Id;
            con.Primary_Contact__c = false;
            con.MobilePhone = '123333';
            insert con;
        }
        return con;
    }
    private static Opportunity getOpportunity(){
        if(opp == null){
        opp = new Opportunity();
        acc= getAccount();
        opp.Name = 'Test Opportunity';
        opp.AccountId = acc.Id;
        opp.StageName = 'New';
        opp.CloseDate = System.today();
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10';
        opp.Contract_Signed_Date__c =system.today();
        opp.Signed_Contract__c = 'Yes';
        opp.Signed_Contract_Order_form_Recieved_Date__c = system.today();
        opp.Upload_signed_order_Contract__c = true;
        opp.Win_Loss_Reasons__c='Price';
        
        insert opp;
        }
        return opp;
    }
     private static Account getAccount(){
        if(acc == null){
            acc = new Account();
            Country_Lookup__c cl = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = cl.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Activated__c = True;
            acc.Account_ID__c = '6666';
            acc.Customer_Legal_Entity_Name__c='Test';
            insert acc;
        }
        return acc;
    }
    private static Country_Lookup__c getCountry(){
        Country_Lookup__c cl = new Country_Lookup__c();
       // cl.CCMS_Country_Code__c = 'IND';
       // cl.CCMS_Country_Name__c = 'ARMENIA';
        cl.Name = 'ARMENIA';
        cl.Country_Code__c = 'AM';
        insert cl;
        return cl;
    } */
}