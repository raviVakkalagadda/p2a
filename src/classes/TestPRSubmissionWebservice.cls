@isTest(seealldata=true)
public with sharing class TestPRSubmissionWebservice{
  /* static Opportunity opp;
   static Opportunity opp2;
   static Account acc;
   static Country_Lookup__c country;
   static Site__c s;
   static Site__c sb;
   static City_Lookup__c ci;
   static OpportunityLineItem oli;
   static AccountNatureMapper__c an;
   static BillProfile__c b;
   static Country_Lookup__c country1;
   static Id ProductId;
   static testMethod void testUpdatePRFeilds(){
   getAccountNature();
   getOpportunityLineItem();
   getOpportunity();
  // getPriceBookEntry('GCPE');
   system.debug('===============Account Nature==============='+getAccountNature());
   system.debug('========='+getPurchasingEntityMapper());
   system.debug('========='+getSellingEntityMapper());
   PRSubmissionWebservice.LineItemDetails innerLineItemDetailObj = new PRSubmissionWebservice.LineItemDetails();
   PRSubmissionWebservice.fetchLineItemDetails(opp.id); 
   // PRSubmissionWebservice.fetchLineItemDetails(oli.id);   

    }


List<Id> opplist = new List<Id>();   
 private static Opportunity getOpportunity(){  
        if(opp ==null){               
        opp = new Opportunity();            
        acc = getAccount();
        opp.QuoteStatus__c='Budgetary Quote';           
        opp.Name = 'Test Opportunity';            
        opp.AccountId = acc.Id;  
        //updated Stage name Qualified prospect  as Qualify as per SOMP requirement          
        opp.Stagename='Identify & Define' ; 
        opp.Stage__c='Identify & Define' ;        
        opp.CloseDate = System.today();   
        opp.Opportunity_Type__c = 'Simple';
        //opp.Win_Loss_Reasons__c='Cancelled';  
        opp.Estimated_MRC__c=800;
        opp.Estimated_NRC__c=1000;
        opp.ContractTerm__c='10'; 
        opp.Sent_to_TIGFIN_for_Cancel__c=false;
        opp.Sent_to_TIGFIN__c=false;
       // opp.Selling_Entity__c='Telstra Telecommunications Private Limited';
       // opp.Owner=getUser(); 
        insert opp;
      //  opplist.add(opp); 
        }             
        return opp;
            
       }
       
       private static Account getAccount(){
        if(acc== null){                
        acc = new Account();            
        country = getCountry();            
        acc.Name = 'Test Account1';            
        acc.Customer_Type__c = 'MNC';            
        acc.Country__c = country.Id;            
        acc.Selling_Entity__c = 'Telstra INC'; 
        acc.Customer_Legal_Entity_Name__c = 'xyz'; 
        acc.Activated__c=true; 
        acc.Account_ID__c='5474575474' ;   
        acc.account_Status__c = 'Active';
        insert acc;
        }       
        return acc;
            
      }
       private static Country_Lookup__c getCountry(){
        if(country == null){            
            
         country = new Country_Lookup__c();            
         country.Name ='UNITED KINGDOM';         
         country.Country_Code__c = 'IN';            
         insert country;
        }        
         return country;  
        
         }      
private static City_Lookup__c getCity(){
    if(ci == null){
        ci = new City_Lookup__c();
        ci.City_Code__c ='MUM';
        ci.Name = 'MUMBAI';
        insert ci;
    }
    return ci;
}
private static OpportunityLineItem getOpportunityLineItem(){ 
    if(oli == null){
    opp = getOpportunity();
  //  oli = [SELECT Id,GIE_router_model__c,Power_per_rack__c,Unit_of_Power__c,Utility_model__c,Type_of_space__c,Specify_the_additional_items_required__c,Media_type__c,Sub_Product_Type__c,Video_device_financing_model__c,Video_hardware_name_and_model__c,Overall_Video_Service_Management__c,Concierge_Service__c,Level_of_Supplier_management__c,GMNS_PS_Type__c,Financing_Model__c,Upstream_speed__c,Downstream_speed__c,Internet_Access_Technology__c,Product_id2__c,Type__c,COSVoice__c,Other_NNI_Location__c,COS_Low_Priority__c,Site__c,Site_B__c,Site_B_State__c,Type_of_circuit__c,EMC_Type__c,Number_of_E1_T1_Cables_Required__c,NID_Port_Speed__c,Model__c,NID_Rack_Mount_Size__c,NID_WAN_Interface_SFP_Type__c,Type_of_Installation__c,GCPE_type_associated_with_GMNS__c,Hardware_Size__c,DCoS_required__c,Maintenance_MRC__c,Bill_of_Material_BoM_attached__c,Local_Loop_Speed__c,COSStandardData__c,COSInteractiveData__c,COSVideo__c,COSCriticalData__c,NNI_Provider__c,Interface_type__c,MTU_Size__c,Installation_after_business_hours_only__c,GCPE_Type__c,GCPE_Hardware_Manufacturer__c,GCPE_Hardware_Model__c,Port_speed__c,Line_Description__c,Line_Description_for_TIFGIN__c,POP_A_Code__c,POP_Z_Code__c,Site_B_City__c,Customer_Site_A_Address_2__c,Site_A_City__c,Site_A_State__c,Customer_Site_B_Address_1__c,Product_Id__c,Service_Type__c,ProductCode__c,Existing_PR_Number__c,Existing_PO_Number__c,OrderType__c,PricebookEntry.product2.Name,PricebookEntry.Product2.Product_ID__c,Purchase_Requisition_Number__c,Contract_term__c,Need_By_Date__c,Specific_Quote_Requirements__c,CurrencyIsoCode,CPQItem__c,CartItemGuid__c,Customer_Site_A_Address_1__c,Customer_Site_B_Address_2__c,Type_of_NNI_Required__c,Type_A_NNI_Port_CoS_mix__c FROM OpportunityLineItem WHERE OpportunityId ='006N0000002PVsp' and OffnetRequired__c ='Yes' and Trigger_Required__c=true LIMIT 1];
        system.debug('OliID+++++++'+oli);
         oli = new OpportunityLineItem();
            
            b = getBillProfile();
            oli.OpportunityId = opp.Id;
            oli.IsMainItem__c = 'Yes';
            oli.OrderType__c = 'New Provide';
            oli.Quantity = 5;
            oli.UnitPrice = 10;
            oli.PricebookEntryId =getPriceBookEntry('GCPE').Id;
            //oli.PricebookEntry=getPriceBookEntry('GCPE').Id;
            oli.ContractTerm__c = '12';
            oli.OffnetRequired__c = 'Yes';
            oli.Location__c = 'Bengaluru';
            oli.BillProfileId__c = b.Id;
            oli.Site__c=getSite().Id;
            oli.Site_B__c=getSiteb().Id;
            oli.PR_Created__c=false;
            oli.Purchase_Requisition_Number__c=null;
            oli.OffnetRequired__c='Yes';
            oli.CPQItem__c='1';
            oli.ServiceId__c= null;
            oli.Resource__c=null;
            oli.Email_Sent_To_TIGFIN__c=false; 
            oli.GCPE_Type__c='test';
            oli.Installation_after_business_hours_only__c='Yes';
            oli.MTU_Size__c='1';
            oli.GCPE_Hardware_Manufacturer__c='1';
             oli.GCPE_Hardware_Model__c='test';
             oli.Trigger_Required__c=false;
             oli.Line_Description__c='test'; 
             oli.Supplier_Flag__c=false;
             oli.Site_A_Country__c='test';  
             oli.Site_B_country__c='test';  
             oli.offnetRequired__c='Yes'; 
             oli.PR_Created__c=false;
             oli.Purchase_Order_Number__c=null;
             oli.Line_Description_for_TIFGIN__c='test data';
             oli.Operating_Unit_TIGFIN__c='test data';
             oli.GMNS_PS_Type__c='test';
             oli.Financing_Model__c='Sale';
             oli.Upstream_speed__c='122'; 
             oli.Downstream_speed__c='2343'; 
             oli.Internet_Access_Technology__c='test';
             oli.Type__c='test';
             oli.COSVoice__c=100;
             oli.Other_NNI_Location__c='test';
             oli.Type_of_circuit__c='test';
             oli.EMC_Type__c='test';
             oli.Number_of_E1_T1_Cables_Required__c='test'; 
             oli.NID_Port_Speed__c='test';
             oli.Model__c='tset'; 
             oli.NID_Rack_Mount_Size__c='test';
             oli.NID_WAN_Interface_SFP_Type__c='test';
             oli.Type_of_Installation__c='test';
             oli.GCPE_type_associated_with_GMNS__c='test';
             oli.Hardware_Size__c='test';
             oli.DCoS_required__c='Yes';
             oli.Maintenance_MRC__c='test';
             
             oli.Local_Loop_Speed__c='12';
             oli.COSStandardData__c=100;
             oli.COSInteractiveData__c=100; 
             oli.COSVideo__c=100;
             oli.COSCriticalData__c=100;
             oli.NNI_Provider__c='test';
             oli.Interface_type__c='test';
             oli.GIE_router_model__c='test'; 
             oli.Type_of_space__c='test';
             oli.Unit_of_Power__c='tets';
             oli.Utility_model__c='test'; 
             oli.Power_per_rack__c='test';
             oli.Specify_the_additional_items_required__c='test'; 
             oli.Media_type__c='test';
             oli.Sub_Product_Type__c='test';
             oli.Video_device_financing_model__c='test';
             oli.Video_hardware_name_and_model__c= 'test';
             oli.Overall_Video_Service_Management__c='tets'; 
             oli.Concierge_Service__c='tets';
             oli.Level_of_Supplier_management__c='tets'; 
             oli.Port_speed__c='tets';  
             oli.POP_A_Code__c='test'; 
             oli.POP_Z_Code__c='test';  
             oli.Service_Type__c='test';
             oli.OrderType__c='test';
             oli.Contract_term__c='12'; 
             oli.Need_By_Date__c=system.today();
             oli.Specific_Quote_Requirements__c='I am fine'; 
             oli.Type_of_NNI_Required__c='test'; 
             oli.Type_A_NNI_Port_CoS_mix__c='test';
             //OLI.Product_id2__c='GCPE';
             oli.Trigger_Required__c=true;
     insert oli; 
    }
    return oli;
    
}



private static Pricebook2 getPriceBook(String prodName){
        Pricebook2 p = [SELECT Id FROM Pricebook2 where IsStandard = true LIMIT 1];
        return p;   
    }
    
private static Product2 getProduct(String prodName){
        Product2 prod = new Product2();
        prod.Name = prodName;
        prod.ProductCode = prodName;
        prod.Product_ID__c ='GCPE';
        
        insert prod;
        return prod;
    }
    
    
private static PricebookEntry getPriceBookEntry(String prodName){
        PricebookEntry p = new PricebookEntry();
        p.Pricebook2Id = getPriceBook(prodName).Id;
        p.Product2 = getProduct(prodName);
        p.Product2Id =  p.Product2.Id;
       
        p.UnitPrice = 2000;
        p.IsActive = true;
        insert p;
        ProductId=p.Product2Id ; 
        system.debug('======in pricebook'+p.Product2.Id);
        return p;    
    }
    private static BillProfile__c getBillProfile(){
    if(b == null) {
        b = new BillProfile__c();
        s = getSite();
        acc = getAccount();
        //b.Bill_Profile_Number__c = 'Test Bill Profile';
        b.Billing_Entity__c = 'Telstra Limited';
        b.Account__c = s.AccountId__c;
        b.Bill_Profile_Site__c = s.Id;
        b.Invoice_Breakdown__c = 'Summary Page';
        b.First_Period_Date__c = date.today();
        b.Start_Date__c = date.today();
        //b.Operating_Unit__c = 'TI_HK_I52S';
        insert b;
    }
        return b;
}
private static Site__c getSite(){
    if(s == null){
        s = new Site__c();
        acc = getAccount();
        s.Name = 'Test_site';
        s.Address1__c = '43';
        s.Address2__c = 'Bangalore';
        country = getCountry();
        s.Country_Finder__c = country.Id;
        ci = getCity();
        s.City_Finder__c = ci.Id;
        s.AccountId__c =  acc.Id; 
        s.Address_Type__c = 'Billing Address';
        insert s;
    }
    return s;
} 


private static Site__c getSiteb(){
    if(sb == null){
        sb = new Site__c();
        acc = getAccount();
        sb.Name = 'Test_site_1';
        sb.Address1__c = '431';
        sb.Address2__c = 'Bangalore1';
        country = getCountry();
        sb.Country_Finder__c = country.Id;
        ci = getCity();
        sb.City_Finder__c = ci.Id;
        sb.AccountId__c =  acc.Id; 
        sb.Address_Type__c = 'Billing Address';
        insert sb;
    }
    return sb;
} 


    // Creating the Test Data
    private static User getUser()
    {
       
       User userObj1 = new User();
       userObj1.FirstName = 'Test';
       userObj1.LastName = 'SFDC';
       userObj1.IsActive  = true;
       userObj1.Username = 'Test.SFDC@team.telstra.com.evolution';
       userObj1.Email = 'Test.SFDC@team.telstra.com';
       userObj1.Region__c = 'US';
       userObj1.TimeZoneSidKey = 'Australia/Sydney';
       userObj1.LocaleSidKey = 'en_AU';
       userObj1.EmailEncodingKey = 'ISO-8859-1';
       userObj1.LanguageLocaleKey = 'en_US';
       userObj1.Alias = 'TSFDC';
       Profile p = [select id from Profile where name = 'TI Finance'];
       
       userObj1.ProfileId = p.id;
       
              
       return userObj1;
       
    }
    
    private static User_Group_Mapper__c getGroupMapper(){        
        User_Group_Mapper__c usrgroup = new User_Group_Mapper__c();           
         usrgroup.Group_Name__c = 'Test Group';       
         usrgroup.User_Region__c = 'US';        
         usrgroup.Profile_Name__c = 'TI Legal/Contracts Mgt';         
         insert usrgroup;        
         return usrgroup;    
         }
    private static Group getGroup(){        
        Group groupm = new Group();           
         groupm.Name = 'Legal/ Contract Management ';       
         groupm.Type = 'Regular';       
         insert groupm;        
         return groupm; 
    }
 private static PurchasingEntityMapper__c getPurchasingEntityMapper(){
    
    PurchasingEntityMapper__c PEMapper=new PurchasingEntityMapper__c();
    country1 = getCountry();
    PEMapper.Country_Name__c=country1.Id;
    PEMapper.Direct_Purchasing_PU__c='TI_UK_I61S';
    PEMapper.Purchasing_Entity__c='TI_UK_I61S';
    PEMapper.Other__c='';
    insert PEMapper;
    return PEMapper;
    
 }
  private static Country_Lookup__c getCountry1(){
        if(country == null){            
            
         country = new Country_Lookup__c();            
         //  country.CCMS_Country_Code__c = 'HK';            
         //  country.CCMS_Country_Name__c = 'India';   
         country.Name ='INDIA';         
         country.Country_Code__c = 'IN';            
         insert country;
        }        
         return country;  
        
         }      
 private static SellingEntityPurchasingEntityMapper__c getSellingEntityMapper(){
    SellingEntityPurchasingEntityMapper__c SEMapper=new SellingEntityPurchasingEntityMapper__c();
    country = getCountry1();
    SEMapper.Country__c=country.Id;
    SEMapper.Direct_Purchasing_PU__c='TI_IN_TTCS';
    SEMapper.Purchasing_Entity__c='TI_HK_IGSS';
    SEMapper.Region__c='';
    SEMapper.Selling_Entity__c='Telstra INC';
    insert SEMapper;
    return SEMapper;
    
 }
 Private static AccountNatureMapper__c getAccountNature(){
       //an = [Select Id from AccountNatureMapper__c limit 1];
       
       an=new AccountNatureMapper__c();
       system.debug('++++AN+++'+an);
       an.Account_Nature__c='CPE';
       an.Product_Code__c='GCPE';
       an.Product_ID__c='GCPE';
       an.Product_Name__c=ProductId;
       an.Type__c='';
       insert an;
       system.debug('++++AN+++'+an);
   return an;
  } */

}