@isTest
public class TestProcessStepTimeManager {
    private static List<NewLogo__c> NewLogolist; 
    private static list<cscfga__Product_Configuration__c> pclist;
  
   
    static void TestUpdateStepTimes(){
        //Id stepId = 'a2zO00000002mPm';
        P2A_TestFactoryCls.SetupTestData();
        CSPOFA__Orchestration_Process_Template__c thisProcTempl = [select Id, Name 
                                                                  from CSPOFA__Orchestration_Process_Template__c
                                                                  where name = 'Test Proc Templ'].get(0);
        
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = [select Id, Name, CSPOFA__Orchestration_Process_Template__c,
                                                                Estimated_Time_To_Complete__c, CSPOFA__OLA_Unit__c, 
                                                                CSPOFA__Milestone_Label__c from CSPOFA__Orchestration_Step_Template__c
                                                                where name = 'Test Step Template'].get(0);
        
        CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, order__c, order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process'].get(0);
        
        cspofa__Orchestration_Step__c stepToUpdate = [SELECT Id, CSPOFA__Orchestration_Process__r.Id FROM 
                                                        CSPOFA__Orchestration_Step__c 
                                                        WHERE CSPOFA__Orchestration_Process__r.Id = :thisProc.Id].get(0);
        stepToUpdate.Start_Date_Time__c = system.now();
        stepToUpdate.End_Date_Time__c = system.now().addDays(30);
        update stepToUpdate;

        DateTime dt = DateTime.newInstance(2016,11,1,12,0,0);
        Id processId = stepToUpdate.CSPOFA__Orchestration_Process__r.Id;
        List<cspofa__Orchestration_Step__c> orderSteps = [SELECT 
                                                              Id, 
                                                              CSPOFA__Orchestration_Process__r.Id,
                                                              CSPOFA__Orchestration_Step_Template__r.Name,
                                                              CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__OLA_Unit__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                              Start_Date_Time__c,
                                                              End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c
                                                          FROM 
                                                              cspofa__Orchestration_Step__c 
                                                          WHERE 
                                                              CSPOFA__Orchestration_Process__r.Id = :processId];
        system.assert(orderSteps.size() > 0);
        orderSteps = ProcessStepTimeManager.SortSteps(orderSteps, false);
        Test.startTest();
        for (cspofa__Orchestration_Step__c step : orderSteps)
        {
            if (ProcessStepTimeManager.IncludeInPathView(step))
            {
                System.Debug('Step name: ' + 
                             step.CSPOFA__Orchestration_Step_Template__r.Name.rightPad(40) + 
                             ' starts at ' + 
                             step.Start_Date_Time__c.day() + '-' + step.Start_Date_Time__c.month()
                            );
            }
        }

        System.assert(ProcessStepTimeManager.StepOLAInMinutes(orderSteps.get(0)) != null);
        System.assert(ProcessStepTimeManager.GetLongestProcessLength(new List<CSPOFA__Orchestration_Process__c>{thisProc}) != 0);

        //ProcessStepTimeManager.SetProcessEndAndCheckCRD(thisProc, system.now().addDays(40));
        Test.stopTest();
    } 

   static void TestSetProcessEndAndCheckCRD(){
       P2A_TestFactoryCls.SetupTestData();
      CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, Order__c,
                                                      order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process'].get(0);
        system.assertNotEquals(null,thisProc );                                              
        Test.startTest();
        ProcessStepTimeManager.SetProcessEndAndCheckCRD(thisProc, system.now().addDays(200));
        Test.stopTest();
    }
    
    
    
   static void TestSetDatesForProcesses() {
        P2A_TestFactoryCls.SetupTestData();
        List<CSPOFA__Orchestration_Process__c> L1 = new List<CSPOFA__Orchestration_Process__c>();
        List<cspofa__Orchestration_Step__c> L2 = new List<cspofa__Orchestration_Step__c>();
        Map<Id, List<CSPOFA__Orchestration_Step__c>> L3 = new Map<Id, List<CSPOFA__Orchestration_Step__c>>();
        CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, order__c, order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c, Start_Date_Time__c 
                                                     
                                                      from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process2'].get(0);
        L1.add(thisProc);
        
        system.assert(thisProc!=null);
        cspofa__Orchestration_Step__c stepToUpdate = [SELECT Id, Start_Date_Time__c, CSPOFA__Orchestration_Process__r.Id, 
                                                        CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                        CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                        Estimated_Time_To_Complete__c
                                                        FROM 
                                                        CSPOFA__Orchestration_Step__c
                                                        WHERE CSPOFA__Orchestration_Process__r.Id = :thisProc.Id].get(0);
        L2.add(stepToUpdate);
        //SetDatesForProcesses(List<CSPOFA__Orchestration_Process__c> processes, Map<Id, List<CSPOFA__Orchestration_Step__c>> processToStepList, DateTime startOrEndDate, boolean dateIsStart)
        L3.put(L2[0].id,L2);
        
        Test.startTest();
        ProcessStepTimeManager.SetDatesForProcesses(L1, L3, system.now().addDays(10),false);
        Test.stopTest();
   
   }  
    
   static void TestUpdatePathWithTimeBulkified(){
       P2A_TestFactoryCls.SetupTestData();
        map<Id, CSPOFA__Orchestration_Process__c> processesToUpdateMap = 
                              new Map<Id, CSPOFA__Orchestration_Process__c>([select Id, Name, Estimated_Time_To_Complete__c, 
                                                                          Order__c, CSPOFA__Orchestration_Process_Template__c
                                                                          from CSPOFA__Orchestration_Process__c
                                                                          where Name='Test Process']);

        map<Id, cspofa__Orchestration_Step__c> stepsToUpdateMap = 
                              new map<Id, cspofa__Orchestration_Step__c>([select Id, Name, Start_Date_Time__c,End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c, CSPOFA__Orchestration_Process__c
                                                              from cspofa__Orchestration_Step__c
                                                              where CSPOFA__Orchestration_Process__c != null]);
        system.assert(stepsToUpdateMap!=null);                                                      
        Test.startTest();
        ProcessStepTimeManager.UpdatePathWithTimeBulkified(processesToUpdateMap, stepsToUpdateMap, stepsToUpdateMap.values().get(0), system.now().addDays(-10), true);
        Test.stopTest();
    }
  
   static void TestUpdateStartEnd(){
       P2A_TestFactoryCls.SetupTestData();
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        
        List<CSPOFA__Orchestration_Process__c> L1 = new List<CSPOFA__Orchestration_Process__c>();
        List<cspofa__Orchestration_Step__c> L2 = new List<cspofa__Orchestration_Step__c>();

        CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, order__c, order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c, Start_Date_Time__c,csordtelcoa__Service__r.Estimated_Start_Date__c
                                                      from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process2'].get(0);
        L1.add(thisProc);
        cspofa__Orchestration_Step__c stepToUpdate = [SELECT Id, Start_Date_Time__c, CSPOFA__Orchestration_Process__r.Id, 
                                                        CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                        CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                        Estimated_Time_To_Complete__c
                                                        FROM 
                                                        CSPOFA__Orchestration_Step__c
                                                        WHERE CSPOFA__Orchestration_Process__r.Id = :thisProc.Id].get(0);
        system.assert(stepToUpdate!=null);
        
        L2.add(stepToUpdate);
        Test.startTest();
        ProcessStepTimeManager.UpdateStart(L1, L2, system.now().addDays(10));
        ProcessStepTimeManager.UpdateEnd(L1,L2,system.now().addDays(150));
        
        Test.stopTest();
    }
   
   /*static testmethod void TestSetInitialETCForOrders(){
    SetupTestData();
        csord__Order__c order = [select Id, Name, Customer_Required_Date__c from csord__Order__c
                                  where name='Test Order2'].get(0);
                                        
        Test.startTest();
        ProcessStepTimeManager.SetInitialETCForOrders(new Set<Id>{order.Id}, true);
        ProcessStepTimeManager.SetInitialETCForOrders(new Set<Id>{order.Id}, false);
        Test.stopTest();
    }
    */
   
    static void TestSetDatesForProcessAndSteps(){
        P2A_TestFactoryCls.SetupTestData();
        CSPOFA__Orchestration_Process__c thisProc = [select Id, name, CSPOFA__Orchestration_Process_Template__c, order__c, order__r.Customer_Required_Date__c,
                                                      Estimated_Time_To_Complete__c, Start_Date_Time__c, CSPOFA__Status__c,
                                                      csordtelcoa__Service__r.Estimated_Start_Date__c, End_Date_Time__c
                                                      from CSPOFA__Orchestration_Process__c
                                                      where name = 'Test Process2'].get(0);
        List<cspofa__Orchestration_Step__c> stepToUpdate = [SELECT Id, Start_Date_Time__c, CSPOFA__Orchestration_Process__r.Id, 
                                                        CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                        CSPOFA__Orchestration_Step_Template__r.Estimated_Time_To_Complete__c,
                                                        Estimated_Time_To_Complete__c, CSPOFA__Status__c
                                                        FROM 
                                                        CSPOFA__Orchestration_Step__c
                                                        WHERE CSPOFA__Orchestration_Process__r.Id = :thisProc.Id];                                            
        system.assert(stepToUpdate!=null);
        Test.startTest();
        ProcessStepTimeManager.SetDatesForProcessAndSteps(thisProc, stepToUpdate, system.now().addDays(100), false);
        ProcessStepTimeManager.SetDatesForProcessAndSteps(thisProc, stepToUpdate, system.now().addDays(50), true);
        Test.stopTest();
    }
    
    static void TestSetInitialETCForOrderWS(){
        P2A_TestFactoryCls.SetupTestData();
        csord__Order__c order = [select Id, Name, Customer_Required_Date__c from csord__Order__c].get(0);
        Test.startTest();
        System.assertEquals(ProcessStepTimeManager.SetInitialETCForOrder(order.Id), 'Success');
        System.assertEquals(ProcessStepTimeManager.ForceSetInitialETCForOrder(order.Id), 'Success');
        Test.stopTest();
    }
    
 private static testMethod void Test1() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestUpdateStepTimes();
            } catch(Exception e) {
            ee = e;
            ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test1';         
            ErrorHandlerException.sendException(e); 
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
  private static testMethod void Test2() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestSetProcessEndAndCheckCRD();
            } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test2';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
   private static testMethod void Test3() {
   Integer userid = 0;
        Exception ee = null;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestSetDatesForProcesses();
            } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test3';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
   private static testMethod void Test4() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestUpdatePathWithTimeBulkified();
            } catch(Exception e) {
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test4';         
            ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }             
  private static testMethod void Test5() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestUpdateStartEnd();
            } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test5';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
 private static testMethod void Test6() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestSetDatesForProcessAndSteps();
            } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test6';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    } 
  private static testMethod void Test7() {
        Exception ee = null;
        Integer userid = 0;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            TestSetInitialETCForOrderWS();
            } catch(Exception e) {
            ee = e;
             ErrorHandlerException.ExecutingClassName='TestProcessStepTimeManager :Test7';         
            ErrorHandlerException.sendException(e);
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
    private static testMethod void Test8() {        
        List<Account> ListAcc  = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> ListOpp = P2A_TestFactoryCls.getOpportunitys(1,ListAcc);
        List<cscfga__Product_basket__c> Products =  P2A_TestFactoryCls.getProductBasketHdlr(1,ListOpp); 
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        
        list<CSPOFA__Orchestration_Process_Template__c> orchprocessTemplist = P2A_TestFactoryCls.getOrchestrationProcess(5);    
        List<CSPOFA__Orchestration_Process__c>processList = P2A_TestFactoryCls.getOrchestrationProcesss(5,orchprocessTemplist);    
        List<CSPOFA__Orchestration_Step__c>stepList=P2A_TestFactoryCls.getOrchestrationStep(5,processList);
        List<csord__Subscription__c> subscriptionList = P2A_TestFactoryCls.getSubscription(1, OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(5,OrdReqList);    
        
        processList[0].Order__c = Orders[0].id;
        update processList;
        
        map<Id, CSPOFA__Orchestration_Process__c> processesToUpdateMap = 
                              new Map<Id, CSPOFA__Orchestration_Process__c>([select Id, Name, Estimated_Time_To_Complete__c, 
                                                                          Order__c, CSPOFA__Orchestration_Process_Template__c
                                                                          from CSPOFA__Orchestration_Process__c
                                                                          where Name='Test Process']);

        map<Id, cspofa__Orchestration_Step__c> stepsToUpdateMap = 
                              new map<Id, cspofa__Orchestration_Step__c>([select Id, Name, Start_Date_Time__c,End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c, CSPOFA__Orchestration_Process__c
                                                              from cspofa__Orchestration_Step__c
                                                              where CSPOFA__Orchestration_Process__c != null]);
        
            Test.startTest();
            ProcessStepTimeManager.UpdateOrchStepsForOrder(Orders[0]);
            //ProcessStepTimeManager.SetInitialETCForOrdersAndService(orderAndServices);                    
            ProcessStepTimeManager.UpdatePathWithTimeBulkified(processesToUpdateMap, stepsToUpdateMap, stepsToUpdateMap.values().get(0), system.now().addDays(-10), true,processList[0],processList,stepList,stepList);
            Test.stopTest();
    }
    
}