@istest(seealldata=false)
public class TibcoComBillingactivateTest {
     
    static testMethod void AllOrderTriggertest() {
    
    TibcoComSchemasXsdgenerationBill.Service_element se =new TibcoComSchemasXsdgenerationBill.Service_element();
        se.AccountManager = 'AccountManager';
        se.BundleLabel = 'BundleLabel';
        se.BundleFlag = 'BundleFlag';
        se.ParentBundleID = 'ParentBundleID';
        se.BundleAction = 'BundleAction';
        se.OLIType = 'OLIType';
        se.BillableFlag = 'BillableFlag';
        se.ProductID = 'ProductID';
        se.RootProductID = 'RootProductID';
        se.OrderDate = 'OrderDate';
        se.ContractStartDate = date.parse('04/01/2017');
        se.OrderType = 'OrderType';
        se.PrimaryServiceID = 'PrimaryServiceID';
        se.ParentCustomerPO = 'ParentCustomerPO';
        se.ProductCode = 'ProductCode';
        se.ParentServiceID = 'ParentServiceID';
        se.ContractDuration = 'ContractDuration';
        se.RootProductBillText = 'RootProductBillText';
        se.ServiceBillText = 'ServiceBillText';
        se.ProductName = 'ProductName';
        se.BillActivationFlag = 'BillActivationFlag';
        se.GenerateCreditFlag = 'GenerateCreditFlag';
        se.ResourceId = 'ResourceId';
        se.OliId = 'OliId';
        se.UserName = 'UserName';
        
    list<TibcoComSchemasXsdgenerationBill.Service_element> tibcomsxdbilserv = new list<TibcoComSchemasXsdgenerationBill.Service_element>();
    tibcomsxdbilserv.add(se);
    TibcoComBillingactivate.BillingActivatePortTypeEndpoint1  Tibcombilactivate = new TibcoComBillingactivate.BillingActivatePortTypeEndpoint1();
      
     Tibcombilactivate.endpoint_x = 'http://localhost:-1/Service.serviceagent/BillingActivateEndpoint';
     Tibcombilactivate.inputHttpHeaders_x = new Map<String,String>();
     Tibcombilactivate.outputHttpHeaders_x = new Map<String,String>();
     Tibcombilactivate.clientCertName_x = 'Tibcombilactivate';
     Tibcombilactivate.clientCert_x = 'Tibcombilactivate';
     Tibcombilactivate.clientCertPasswd_x = 'Tibcombilactivate';
     Tibcombilactivate.timeout_x = 4578;
    
    
    TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element TibSchXsdActRes = new TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
    TibcoComBillingactivate TibcoComBilling = new TibcoComBillingactivate();
    Test.startTest();
        Test.setMock(WebServiceMock.class, new WebServiceMockImpl());
        Tibcombilactivate.BillingActivateOperation(tibcomsxdbilserv);
        system.assertEquals(true,TibSchXsdActRes !=null);
    Test.stopTest();
    }
 }