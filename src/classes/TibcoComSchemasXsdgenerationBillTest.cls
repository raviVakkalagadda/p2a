@istest(seealldata=false)
public class TibcoComSchemasXsdgenerationBillTest{
     
    static testMethod void AllOrderTriggertest() {
    
      TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element tb1 = new TibcoComSchemasXsdgenerationBill.BillingActivateResponse_element();
      TibcoComSchemasXsdgenerationBill.Service_element tb2 = new TibcoComSchemasXsdgenerationBill.Service_element();
      TibcoComSchemasXsdgenerationBill.ServiceLineItem_element tb3 = new TibcoComSchemasXsdgenerationBill.ServiceLineItem_element();
      TibcoComSchemasXsdgenerationBill.UDF_element tb4 = new TibcoComSchemasXsdgenerationBill.UDF_element();
      TibcoComSchemasXsdgenerationBill.Subscription_element tb5 = new TibcoComSchemasXsdgenerationBill.Subscription_element();
      system.assertEquals(true,tb1 !=null);
      system.assertEquals(true,tb2 !=null);
      system.assertEquals(true,tb3 !=null);
      system.assertEquals(true,tb4 !=null);
      system.assertEquals(true,tb5 !=null);
    }
 }