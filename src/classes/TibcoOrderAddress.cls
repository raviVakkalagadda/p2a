public class TibcoOrderAddress {
    public String line1='';

    public String line2='';

    public String line3='';

    public String locality='';

    public String region='';

    public String country='';

    public String postCode='';

    public String supplementaryLocation='';
    
    public TibcoOrderAddress() {
    }

    public TibcoOrderAddress(
           String line1,
           String line2,
           String line3,
           String locality,
           String region,
           String country,
           String postCode,
           String supplementaryLocation)
           {
           this.line1 = line1;
           this.line2 = line2;
           this.line3 = line3;
           this.locality = locality;
           this.region = region;
           this.country = country;
           this.postCode = postCode;
           this.supplementaryLocation = supplementaryLocation;
    }
}