public class TibcoOrderCharacteristic {
    public String name='';

    public String description='';

    public TibcoOrderCharacteristicValue[] value=null;

    public TibcoOrderCharacteristic () {
    }

    public TibcoOrderCharacteristic (
           String name,
           String description,
           TibcoOrderCharacteristicValue[] value) {
           this.name = name;
           this.description = description;
           this.value = value;
    }

}