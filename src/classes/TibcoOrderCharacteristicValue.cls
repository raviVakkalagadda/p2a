public class TibcoOrderCharacteristicValue{
    public String name = '';

    public String type = '';

    public String value = '';

    public String valueFrom = '';

    public String valueTo = '';

    public TibcoOrderCharacteristicValue() {
    }

    public TibcoOrderCharacteristicValue(
           String name,
           String type,
           String value,
           String valueFrom,
           String valueTo) {
           this.name = name;
           this.type = type;
           this.value = value;
           this.valueFrom = valueFrom;
           this.valueTo = valueTo;
    }
}