public class TibcoServiceOrder {
    public String ExternalBusinessTransactionID = '';
    public String orderRef = '';
    public String orderType = '';
    public TibcoOrderHeader header;
    public TibcoOrderLine[] lines;
     public TibcoServiceOrder()
     {
     }
    public TibcoServiceOrder(String ExternalBusinessTransactionID, String orderRef, TibcoOrderHeader header, TibcoOrderLine[] lines)
    {
        this.ExternalBusinessTransactionID = ExternalBusinessTransactionID;
        this.orderRef = orderRef;
        this.header = header;
        this.lines=lines;
    }
    public TibcoServiceOrder(String ExternalBusinessTransactionID, String orderRef, String orderType)
    {
        this.ExternalBusinessTransactionID = ExternalBusinessTransactionID;
        this.orderRef = orderRef;
        this.orderType = orderType;
    }
}