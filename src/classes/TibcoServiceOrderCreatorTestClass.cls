@isTest (seealldata = false)
public class TibcoServiceOrderCreatorTestClass {
    

   
    //create test data
    public static void initTestMethod(){
        Test.startTest();
        P2A_TestFactoryCls.sampleTestData();
        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<Country_Lookup__c> countrylist =  P2A_TestFactoryCls.getcountry(1);
        List<City_Lookup__c> cityList =  P2A_TestFactoryCls.getCity(1);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        
        List<cscfga__Product_Definition__c> ProductDeflist= P2A_TestFactoryCls.getProductdef(1);
        List<cscfga__Product_Bundle__c> Pbundle = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
        List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> proconfigs= P2A_TestFactoryCls.getProductonfig(1, prodBaskList,ProductDeflist,Pbundle,Offerlists);
        List<cscfga__Configuration_Screen__c> configLst = P2A_TestFactoryCls.getConfigScreen(1, ProductDeflist);
        List<cscfga__Screen_Section__c> ScreenSec = P2A_TestFactoryCls.getScreenSec(1, configLst);      
        List<cscfga__Attribute_Definition__c> Attributedeflist = P2A_TestFactoryCls.getAttributesdef(1,proconfigs,ProductDeflist, configLst, ScreenSec);
        List<cscfga__Attribute__c> AttributesList = P2A_TestFactoryCls.getAttributes(1,proconfigs,Attributedeflist);
        List<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        List<csord__Subscription__c> subscriptions = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        List<csord__Order__c> Orders = P2A_TestFactoryCls.getorder(1,OrdReqList);
        Orders[0].Customer_Turn_Up_Test_Date__c = system.today() + 10;
        Orders[0].GCC_Reseller_Account_ID__c = 'ResellerAccount';
        Orders[0].csord__Account__c = accList[0].id;
        Orders[0].csord__Order_Type__c = 'New Provide';
        upsert Orders[0];
        system.assert(Orders[0].id!=null);
       /* List<cscfga__Product_Configuration__c> PcList = new List<cscfga__Product_Configuration__c>();
        cscfga__Product_Configuration__c Pc = new cscfga__Product_Configuration__c();
        Pc.name = 'pc1';
        Pc.cscfga__Product_Basket__c = prodBaskList[0].id;
        PcList.add(Pc);
        insert PcList;*/
        
        list<csord__Order_Request__c> ordReq =   P2A_TestFactoryCls.getorderrequest(1);
        // List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
       //  List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        list<cscfga__Product_Basket__c> Products = P2A_TestFactoryCls.getProductBasket(1);
        List<cscfga__Product_Definition__c> Prodef = P2A_TestFactoryCls.getProductdef(1);
        List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,ordReq);
        List<csord__Service__c> servList = P2A_TestFactoryCls.getService(1,ordReq,SUBList);
        List<cscfga__Product_Bundle__c> pbundlelist = P2A_TestFactoryCls.getProductBundleHdlr(1,OppList);
       //  List<cscfga__Configuration_Offer__c> Offerlists = P2A_TestFactoryCls.getOffers(1);
        List<cscfga__Product_Configuration__c> PcList = P2A_TestFactoryCls.getProductonfig(1,Products,prodef,pbundlelist,Offerlists);
        
        cscfga__Product_Configuration__c pcroot = new cscfga__Product_Configuration__c(name='Root',cscfga__Product_Family__c ='Custom TWI',
        cscfga__Product_basket__c =Products[0].id,cscfga__Product_Definition__c=Prodef[0].id);
        insert pcroot;
        
        cscfga__Product_Configuration__c pc1 = new cscfga__Product_Configuration__c(name='TWI',cscfga__Product_Family__c ='Custom TWI',
        cscfga__Product_basket__c =Products[0].id,cscfga__Product_Definition__c=Prodef[0].id,cscfga__Root_Configuration__c=pcroot.id);
        insert pc1;
        
        cscfga__Product_Configuration__c pc2 = new cscfga__Product_Configuration__c(name='GVOIP',cscfga__Product_Family__c ='Custom GVOIP',
        cscfga__Product_basket__c =Products[0].id,cscfga__Product_Definition__c=Prodef[0].id,cscfga__Root_Configuration__c=pcroot.id);
        insert pc2;
        
        cscfga__Attribute__c Attributes = new cscfga__Attribute__c();
        Attributes.name = 'a1';
        Attributes.cscfga__Product_Configuration__c=pc1.id;
        Attributes.cscfga__Value__c = 'Test';
        Attributes.cscfga__Attribute_Definition__c = Attributedeflist[0].id; 
        
        cscfga__Attribute__c Attributes1 = new cscfga__Attribute__c();
        Attributes1.name = 'a1';
        Attributes1.cscfga__Product_Configuration__c=pc1.id;
        Attributes1.cscfga__Value__c = 'Test';
        Attributes1.cscfga__Attribute_Definition__c = Attributedeflist[0].id;
        
        cscfga__Attribute__c Attributes2 = new cscfga__Attribute__c();
        Attributes2.name = 'a1';
        Attributes2.cscfga__Product_Configuration__c=pc2.id;
        Attributes2.cscfga__Value__c = 'Test';
        Attributes2.cscfga__Attribute_Definition__c = Attributedeflist[0].id;
        
        cscfga__Attribute__c Attributes3 = new cscfga__Attribute__c();
        Attributes3.name = 'a1';
        Attributes3.cscfga__Product_Configuration__c=pc2.id;
        Attributes3.cscfga__Value__c = 'Test';
        Attributes3.cscfga__Attribute_Definition__c = Attributedeflist[0].id;
        
        List<cscfga__Attribute__c>att1List=new List<cscfga__Attribute__c>{Attributes,Attributes1,Attributes2,Attributes3};
        insert att1List;
        
        List<Site__c> Site = new List<Site__c>();
        Site__c sites = new Site__c();
        sites.name = 'pc1';
        sites.AccountId__c = accList[0].id;
        sites.Address1__c = 'abc';
        sites.Country_Finder__c = countrylist[0].id;
        sites.City_Finder__c = cityList[0].id;
        sites.Address_Type__c = 'Site Address';
        Site.add(sites);
        insert Site;
        system.assert(Site!=null);
        
        Customer_Activation_Test__c cat=new Customer_Activation_Test__c();
        cat.Order__c=Orders[0].id;
        cat.Service_Activation_Queue__c='GSDI - Technical Delivery';
        cat.Activation_appointment_Date__c=System.today();
        cat.Start_time_for_activation_activity__c=system.now();
        cat.Time_zone_for_appointment__c='London';
        cat.Request_Summary__c='Some request summary';
        cat.Scope_of_work__c='some scope';
        cat.Customer_contact_details__c='some contact names';
        cat.Customer_Name__c='some value';
        insert cat;
        
        List<csord__service__c> Services = new List<csord__service__c>();
        csord__service__c ser = new csord__service__c();
        ser.name = 'abc';
        ser.AccountId__c = accList[0].id;
        ser.csord__Order__c = Orders[0].id;
        ser.csord__Order_Request__c = OrdReqList[0].id;
        ser.csord__Subscription__c = subscriptions[0].id;
        ser.csordtelcoa__Product_Configuration__c = proconfigs[0].id;
        ser.csord__Identification__c = 'abc';
        ser.Product_Id__c = 'ID';
        ser.Line_Number__c = 123;
        //ser.Technical_Customer_Contact_Phone_Number__c = Orders[0].id;
        ser.acity_side__c = Site[0].id;
        ser.zcity_side__c = Site[0].id;
        
        csord__service__c ser1 = new csord__service__c();
        ser1.name = 'abc';
        ser1.AccountId__c = accList[0].id;
        ser1.csord__Order__c = Orders[0].id;
        ser1.csord__Order_Request__c = OrdReqList[0].id;
        ser1.csord__Subscription__c = subscriptions[0].id;
        ser1.csordtelcoa__Product_Configuration__c = pc1.id;
        ser1.csord__Identification__c = 'abc';
        ser1.Product_Id__c = 'ID';
        ser1.Line_Number__c = 123;        
        ser1.acity_side__c = Site[0].id;
        ser1.zcity_side__c = Site[0].id;
        
        csord__service__c ser2 = new csord__service__c();
        ser2.name = 'abc';
        ser2.AccountId__c = accList[0].id;
        ser2.csord__Order__c = Orders[0].id;
        ser2.csord__Order_Request__c = OrdReqList[0].id;
        ser2.csord__Subscription__c = subscriptions[0].id;
        ser2.csordtelcoa__Product_Configuration__c = pc2.id;
        ser2.csord__Identification__c = 'abc';
        ser2.Product_Id__c = 'ID';
        ser2.Line_Number__c = 123;        
        ser.acity_side__c = Site[0].id;
        ser.zcity_side__c = Site[0].id;
        
        Services.add(ser);
        Services.add(ser1);
        Services.add(ser2);
        
        insert Services;
        
        system.assert(Services!=null);
        
        
        //List<csord__Subscription__c> SUBList = P2A_TestFactoryCls.getSubscription(1,OrdReqList);
        //List<User> users = P2A_TestFactoryCls.get_Users(1);
        //List<Pricing_Approval_Request_Data__c> Pricingapp = P2A_TestFactoryCls.getProductonfigappreq(1,prodBaskList,proconfigs);
        //list<case> caselist = P2A_TestFactoryCls.getcases(1,AccList,Opplist,prodBaskList,Orders,Users);
    //    List<csord__Service__c> services = P2A_TestFactoryCls.getService(1, OrdReqList, SUBList);
        
        String order = Orders[0].id;
        TibcoServiceOrderCreator t = new TibcoServiceOrderCreator();
        t.CreateOrderFromServiceId(order);
        TibcoOrderLine[]tempOrderLine=t.CreateOrderFromCATserviceId(order,1);
        Test.stopTest();
    }
    
  private static testMethod void Test() {
        Exception ee = null;
        integer userid = 0;
           try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid!=null);
            initTestMethod();
            } catch(Exception e) {
            ee = e;
            ErrorHandlerException.ExecutingClassName='TibcoServiceOrderCreatorTestClass:Test';         
            ErrorHandlerException.sendException(e); 
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    } 
  
}