/**
    @author - Accenture
    @date - 07-JUNE-2012
    @version - 1.0
    @description - This trigger is used for creating an Action Item on opportunity 
                   to check rework in Action Item Object.
*/
trigger TrgCheckRework on Action_Item__c (before insert) 
{

   /* // Creating the Set to store the IDs
    Set<Id> optyIdSet = new Set<Id>();
    
    // Mapping the Opportunity IDs with Action Items IDs
    Map<Id,Set<Id>> oppActionItemsMap = new Map<Id,Set<Id>>();
   
    // Action Item is created
    for(Action_Item__c ai : trigger.new)  
    {
        optyIdSet.add(ai.Opportunity__c);
    }
    
    // query Action_Item to get Action_Item Object based on request Opportunity ID
    List<Action_Item__c> actionItemsList=[select Id,RecordTypeId,Opportunity__c  from Action_Item__c where Opportunity__c in :optyIdSet];
    
    if(actionItemsList.size()!=0) 
    {
    
    // creating map of Opportunity Ids and Action Item's Record Type Ids
    
        for(Action_Item__c ai:actionItemsList) 
        {        
            if(oppActionItemsMap.keyset().contains(ai.Opportunity__c )) 
            {
                oppActionItemsMap.get(ai.Opportunity__c ).add(ai.RecordTypeId);
            }
            else 
            {
                oppActionItemsMap.put(ai.Opportunity__c, new Set <Id>{ai.RecordTypeId});
            }
        }
     }*/
    
    /*
    Verifying that an Action Item exists with same Record Type Id
    if yes set rework as true
    */
    
   /* for(Action_Item__c acit : trigger.new) 
    {  
        Set<Id> recTypeIds=oppActionItemsMap.get(acit.Opportunity__c );
        if(recTypeIds!=null && recTypeIds.contains(acit.RecordTypeId)) 
        {
            acit.Rework__c='Y';
        }
    }  */
       
}