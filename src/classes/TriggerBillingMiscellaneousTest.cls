@isTest(seeAllData = false)
public class TriggerBillingMiscellaneousTest{
    Private static List<Product_Definition_Id__c> PdIdlist1;

     static testmethod void unittest() {
     
     p2a_testfactorycls.disableall(userinfo.getuserid());
         
          PdIdlist1 = new List<Product_Definition_Id__c>{
                new Product_Definition_Id__c(name = 'ASBR_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'A_End_Port_Definition_Id',Product_Id__c = null),              
                new Product_Definition_Id__c(name = 'IPC_Definition_Id',Product_Id__c = null), 
                new Product_Definition_Id__c(name = 'IPVPN_Port_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'Master_IPVPN_Service_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'Master_VPLS_Service_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'Point_To_Point_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'SMA_Gateway_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'Standalone_ASBR_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'VLANGroup_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'VPLS_Transparent_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'VPLS_VLAN_Port_Definition_Id',Product_Id__c = null),
                new Product_Definition_Id__c(name = 'Z_End_Port_Definition_Id',Product_Id__c = null)
          };
          insert PdIdlist1;
          List<Product_Definition_Id__c> pd = [select name,id from Product_Definition_Id__c where name = 'ASBR_Definition_Id'];
          system.assert(pd!=null);
          system.assertEquals(PdIdlist1[0].name,pd[0].name);
         
         List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
         List<Contact> conList = P2A_TestFactoryCls.getContact(1, accList);
         List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
         list<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
         List<csord__Order_Request__c> orderRequestList = P2A_TestFactoryCls.getorderrequest(1);
         List<csord__Subscription__c> subList = P2A_TestFactoryCls.getSubscription(1,orderRequestList);
         List<csord__Service__c> serList = P2A_TestFactoryCls.getServiceHdlr(1,orderRequestList, subList,prodBaskList);
         List<csord__Service_Line_Item__c> serLineItemList = P2A_TestFactoryCls.getSerLineItem(1,serList,orderRequestList);
         List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
         List<Site__c> Listsites = P2A_TestFactoryCls.getsites(1,accList,countrylist);
         
         BillProfile__c BPS = new BillProfile__c();
         BPS.Account__c = accList[0].id;
         BPS.Name = 'TestBP';
         Bps.Status__c = 'Active';
         Bps.In_Arrear_In_Advance__c = 'In Arrears';
         Bps.Align_with_Billing_Cycle__c = true;
         Bps.Pivot_Date__c = '01 February';
         bps.company__c = null;
         BPS.Billing_Contact__c = conList[0].id;
         BPS.Bill_Profile_Site__c = Listsites[0].id;
         BPS.Billing_Entity__c = 'PT. Reach Network Services Indonesia';
         insert BPS;
         system.assert(BPS!=null);
        
         CostCentre__c c = new CostCentre__c();
         c.Cost_Centre_Code__c = 'Test code';
         c.BillProfile__c = BPS.id;
         c.Name = 'TestBillprofile';
         insert c;
         system.assert(c!=null);
         
         csord__Service_Line_Item__c  servlineItem  = new csord__Service_Line_Item__c();
         servlineItem.MISC_Charge_Amount__c = 72;
         servlineItem.Type_of_Charge__c = 'RCH - Recurring charge';
         servlineItem.RC_Credit_Bill_Text__c = 'Credit BIll';
         servlineItem.MISC_Cost__c = 65;
         servlineItem.csord__Service__c = serList[0].id;
         servlineItem.Is_Miscellaneous_Credit_Flag__c = true;
         servlineItem.Transaction_Status__c = 'Approved';
         servLineItem.Is_ETC_Line_Item__c= false;
         servlineItem.csord__Order_Request__c = orderRequestList[0].id;
         servlineItem.Installment_NRC_Start_Date__c = system.today();
         servlineItem.Installment_NRC_End_Date__c = system.today() + 1;
         servlineItem.Billing_MISC_Commencement_Date__c = system.today().addDays(10);
         servlineItem.Billing_MISC_Commencement_Date__c = system.today().addDays(12);
         servlineItem.Billing_End_Date__c = system.today();
         servlineItem.Frequency__c = 'Annual';
         servlineItem.csord__Identification__c ='hnerkfwfefwrjge';
         servlineItem.CurrencyISOCode = 'USD';       
         servlineItem.Approved_Rejected__c = 'Approved';
         servlineItem.Zero_MISC_Charge_Flag__c = true;
         servlineItem.U2C_Master_Code__c = 'U2cMasterCode';
         Insert servlineItem;  
         system.assert(servlineItem!=null);
        
       Test.startTest();
        Test.setMock(WebServiceMock.class, new MainMockClass.SuspendOrderResponse_BillingActivateResponse()); 
         TriggerBillingMiscellaneous.SendBillingdata(serList[0].id);
      
       p2a_testfactorycls.enableall(userinfo.getuserid());
  
       Test.stopTest();
         
      }
 
}