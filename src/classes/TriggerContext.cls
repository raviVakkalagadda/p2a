public class TriggerContext{

	public static Set<String>MethodcontextSet = new Set<String>();

	public static boolean IsMethodAlreadyExecuted(String MethodName){

		boolean flag = false;    
		if(MethodcontextSet.contains(MethodName)){
			flag = true;    
		} else{
			MethodcontextSet.add(MethodName);
			flag = false;   
		}
		System.debug('***MethodcontextSet='+MethodcontextSet);
		System.debug('***trigger context flag='+flag);
		system.debug('***MethodName='+MethodName);

		return flag;
	}
}