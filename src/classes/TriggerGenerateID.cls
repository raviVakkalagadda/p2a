global class TriggerGenerateID {
    
    webservice static void SendServicedata(List<csord__Service__c> serviceList){
            List<csord__Service__c> updatelist = new List<csord__Service__c>();
            Map<Id, String> PrimaryServiceID = new Map<Id, String>();
            //TriggerFlags.NobillProfileUPdate = true;
            //TriggerFlags.NoOpportunitySplitTriggers = true;
            //TriggerFlags.NoServiceTriggers=true;
            //util.FlagtoMuteOpportunitySplitTrg = false;
            
            if(serviceList.Size() > 0){
                Integer count = 0;
                TigComSchemasIdGeneratorresponse.TSID_element ResponseObj = null;
                TigComWsdlGenerateidGenerateidim.IDGeneratorSOAPEventSource serObj = new TigComWsdlGenerateidGenerateidim.IDGeneratorSOAPEventSource();
                serObj.timeout_x = 40000;

                for(csord__Service__c servcobj :serviceList){
                    count++;
                    servcobj.Primary_Service_ID__c = servcobj.Primary_Service_ID__c != null? servcobj.Primary_Service_ID__c: serObj.GenerateID(servcobj.Product_Code__c, servcobj.Site_A_Code__c,servcobj.Site_B_Code__c,'9',servcobj.Service_Type__c);
                    servcobj.ServiceItemNumber__c = count;
                    
                    /**
                     * Added as part of Winter 17(CR26) to fix the order submission issue where in the resource id was not getting updated with the parent service id's last digits.
                     */
                    if(servcobj.Primary_Service_ID__c == '' && servcobj.csord__Service__c != null && PrimaryServiceID.get(servcobj.csord__Service__c) != null){
                        PrimaryServiceID.put(servcobj.Id, PrimaryServiceID.get(servcobj.csord__Service__c));
                    } else{
                        PrimaryServiceID.put(servcobj.Id, servcobj.Primary_Service_ID__c);
                    }

                    updatelist.add(servcobj);
                }
                
                /**
                 * @Author: Accenture
                 * @Added as part of Winter 17(CR26)
                 * @Description: Update parent_service_number__c field on the service record with the parent service name's last 7 digit number.
                 * This field is used to udpate resource ids of the products Local Loop, GIAAS, GCC, GVOIP, GMNS and IPT
                 * Date: 07-August-2017
                 */
                 
                for(csord__Service__c srvc :updatelist){
                    
                    if((srvc.primary_service_id__c==null||srvc.primary_service_id__c=='') && srvc.csord__Service__c != null && PrimaryServiceID.get(srvc.csord__Service__c) != null && srvc.Service_Type__c=='None'||srvc.Resource_ID__c!=null){
                        srvc.primary_service_id__c=PrimaryServiceID.get(srvc.csord__Service__c);
                    }
                    if(srvc.csord__Service__c != null && PrimaryServiceID.get(srvc.csord__Service__c) != null){
                       
                        List<String> temp = PrimaryServiceID.get(srvc.csord__Service__c).split(' ');
                        srvc.parent_service_number__c = temp.Size() > 0? temp[temp.size()-1]: '';
                       
                    }
                    else if(srvc.csord__Service__c != null && srvc.Product_Configuration_Type__c == 'New Provide' && PrimaryServiceID.get(srvc.csord__Service__c) == null){
                       
                       
                        List<String> temp = PrimaryServiceID.get(srvc.Id).split(' ');
                        srvc.parent_service_number__c = temp.Size() > 0? temp[temp.size()-1]: '';
                       
                    }
                }
                update updatelist;
                
            }
           //TriggerFlags.NoServiceTriggers=false;
     }
}