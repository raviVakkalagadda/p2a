/**
@author - Accenture
@date - 21-JUL-2012
@version - 1.0
@description - This is the test class on Trigger_BIU_Case Trigger.
*/
@isTest
public class Trigger_BIU_CaseTest {

    static testMethod void Trigger_BIU_Case_New() {
        
        Account acc = UnitTestHelper.getAccount();
        Service__c ser = new Service__c(Name='ser1', Short_Service_Name__c = 'ser1', AccountId__c=acc.id,Is_GCPE_shared_with_multiple_services__c = 'No');
        system.debug('Service Name**'+ ser.Name);
        insert ser;
        system.assert(ser!=null);
        system.assertEquals(ser.name, 'ser1'); 
        system.debug('id and name is '+ser.id+' '+ser.name);
        Service__c service_new = [select Id,name,AccountId__c from Service__c where id =:ser.Id ];
        
        //Case cas= new Case(Customer_Account_ID__c=acc.Account_ID__c,Service__c=service_new.id ,Telstra_Service_ID__c= service_new.name); 
       // insert cas;
       
    }
 }