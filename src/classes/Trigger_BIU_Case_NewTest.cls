/**
@author - Accenture
@date - 21-JUL-2012
@version - 1.0
@description - This is the test class on Trigger_BIU_Case Trigger.
*/
@isTest
public class Trigger_BIU_Case_NewTest {

    static testMethod void Trigger_BIU_Case_New() {
        Account acc = UnitTestHelper.getAccount();
        
        Service__c ser = new Service__c(Name='AUD 1234',Primary_service_Id__c ='AUD 1234',  AccountId__c=acc.id,Is_GCPE_shared_with_multiple_services__c = 'No');
        insert ser;

       Case cas= new Case(Customer_Account_ID__c=acc.Account_ID__c,Telstra_Service_ID__c='AUD 1234'); 
        insert cas;
    }

}