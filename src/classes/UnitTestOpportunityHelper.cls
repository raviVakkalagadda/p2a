@isTest
/*  UnitTestHelper
    Provide static methods for frequently created objects in UnitTests
*/
public  with sharing class UnitTestOpportunityHelper 
{
    static Country_Lookup__c country;
    static Site__c site;
    static City_Lookup__c city;
    static Account acc;

    public static Opportunity getValidDatesOpportunity() {
      String stageName;
      Opportunity opp = new Opportunity();
      opp.Name = 'Test Approval Opp';
      //opp.mh_Associated_Blue_Sheet__c = true;
      /*Getting the record type id for simple type
        modified by Ramya (SFDC Bangalore)
      */
      
      RecordType rtype=[SELECT Id FROM RecordType where SobjectType='Opportunity' and Name= 'Simple' limit 1];
      opp.RecordTypeId = rtype.Id;
      opp.Contract_Signed_Date__c =system.Today();
      opp.CloseDate =date.valueOf('2012-12-06');
      opp.StageName ='Identify & Define';
      opp.Stage__c ='Identify & Define';
      opp.Estimated_MRC__c=800;
      opp.Estimated_NRC__c=1000;
      opp.ContractTerm__c='10';
      opp.AccountId=getAccount().id;
      insert opp;
      getOppQuote(opp);
      opp.External_Id__c='999999';
      opp.QuoteStatus__c ='Firm';
      //opp.SOF_signed_by_customer__c = true;
      update opp;
      getValidDatesOppLineItem(opp, getProduct());
      
      return opp;
    }
     public static Quote__c getOppQuote(Opportunity o) {
        Quote__c quote= new Quote__c();
        quote.Opportunity__c = o.id;
        quote.Name ='Test0001';
        quote.Status__c='Firm';
        quote.Primary__c = true;     
        insert quote;
        
        return quote;
    }
     private static OpportunityLineItem getValidDatesOppLineItem(Opportunity o, PricebookEntry pbe) {
        OpportunityLineItem oli = new OpportunityLineItem();
        oli.OpportunityId = o.Id;
        oli.Quantity = 1;
        oli.TotalPrice = 100;
        oli.PricebookEntryId = pbe.Id;
        oli.Description = 'Unit Test 001';
        oli.BillProfileId__c= getBillProfile().id;
        //oli.BillProfileId__c = null;
        oli.ProductLeadTime__c = 0;
        oli.OrderType__c = 'New Provide';
        oli.IsMainItem__c = 'Yes';
        oli.CPQItem__c = '1';
        oli.Feasibility_Expiry_Date__c ='12/29/12';
        oli.QuoteExpiry__c ='12/29/12';
        oli.QuoteBExpiry__c ='12/18/12';
       // insert oli;
        
        return oli;
    }
    private static PricebookEntry getProduct() {
        
        Product2 p = new Product2();
        p.Name = 'IP VPN';
        p.ProductCode = 'G-VPN';
        p.Product_ID__c ='GVPN';
        p.IsActive = true;

        insert p;
        
      // Pricebook2 pb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true][0];
        
        PricebookEntry pbe = new PricebookEntry();
        pbe.Pricebook2Id = '01s90000000KWfFAAW';
        pbe.Product2Id = p.Id;
        pbe.IsActive = true;
        pbe.UseStandardPrice = false;
        pbe.UnitPrice = 999999;
        
        insert pbe;
        
        return pbe;
    }   
    
    private static BillProfile__c getBillProfile() {
        
        BillProfile__c bp = new BillProfile__c();
        bp.Name = 'Test BP';
        bp.Start_date__c = system.today();
        bp.Billing_Frequency__c = 'Monthly';
        bp.CurrencyIsoCode= 'GBP';
        /* Approved by Finanace and Legal are given the true value
           modified by Ramya (SFDC Bangalore)
        */
        bp.Approved_by_Finance__c = True;
        bp.Approved_by_Legal__c = True;
        bp.Account__c = getAccount().id;
         bp.Bill_Profile_Site__c = getSite().id;
        insert bp;
        return bp;
    }   
 
    private static Country_Lookup__c getCountry(){
        if(country == null){ 
            country = new Country_Lookup__c();
            country.CCMS_Country_Code__c = 'SG';
            country.CCMS_Country_Name__c = 'India';
            country.Country_Code__c = 'SG';
            insert country;
        }
        return country;
    } 
private static Site__c getSite(){
    if(site== null){
        site= new Site__c();
        acc = getAccount();
        site.Name = 'Test_site';
        site.Address1__c = '43';
        site.Address2__c = 'Bangalore';
        country = getCountry();
        site.Country_Finder__c = country.Id;
        city = getCity();
        site.City_Finder__c = city.Id;
        site.AccountId__c =  acc.Id; 
        site.Address_Type__c = 'Billing Address';
        insert site;
    }
    return site;
}  

private static City_Lookup__c getCity(){
    if(city == null){
        city = new City_Lookup__c();
        city.City_Code__c ='MUM';
        city.Name = 'MUMBAI';
        insert city;
    }
    return city;
}
    private static Account getAccount(){
        if(acc == null){    
            acc = new Account();
            Country_Lookup__c c = getCountry();
            acc.Name = 'Test Account';
            acc.Customer_Type__c = 'MNC';
            acc.Country__c = c.Id;
            acc.Selling_Entity__c = 'Telstra INC';
            acc.Customer_Legal_Entity_Name__c='test';
            acc.Activated__c= true;
            acc.Account_status__C='Active';
            acc.Account_Id__c='123466';
            insert acc;
        }
        return acc;
    }

}