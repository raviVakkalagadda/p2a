/**
    @author - Accenture
    @date - 09-May-2012
    @version - 1.0
    @description - This test class is used to test the UpdateBillProfile trigger.
*/
@isTest
private class UpdateBillProfileTest {
    
    static Account acc1;

    static testMethod void test_billprofiletrigger() {
        
       
       BillProfile__c billProfile = getBillProfile();
       Site__c site = [select Id,Address1__c from Site__c where Id =: billprofile.Bill_Profile_Site__c];
       
       
       // Update the site with Address1
       site.Address1__c ='58';
       update site;
       system.assert(site!=null);
       // system.debug('Updated Site Name:**'+ site.id);
       // system.debug('Date***' + billProfile.Last_Address_Change_On_Site__c);
      
       // putting the value of "Last_Address_Change_On_Site" in local variable of date
      // Date modifieddate = billProfile.Last_Address_Change_On_Site__c;
       // system.debug('SiteName******'+billProfile.Bill_Profile_Address__c);
       // system.debug('Modified Date:*******'+ modifieddate);
       //system.debug('Date*********'+billProfile.Last_Address_Change_On_Site__c);
           billProfile = [SELECT Id,Last_Address_Change_On_Site__c from BillProfile__c WHERE Id =:billProfile.Id];
       //Now Test that the update trigger has set the modifieddate correctly
        // system.assertEquals(billProfile.Last_Address_Change_On_Site__c,date.today());
    }
    
 // Inserting the values in Site Object for testing  
 private static Site__c getSite(){
    Site__c newSite = new Site__c();
    newSite.Name = 'Test_site';
    newSite.Address1__c = '43';
    newSite.Address2__c = 'Bangalore';
    Country_Lookup__c country = getCountry();
    newSite.Country_Finder__c = country.Id;
    City_Lookup__c city = getCity();
    newSite.City_Finder__c = city.Id;
    
    //Country_Lookup__c country = getCountry();
    // newSite.Country_Formula__c = getCountry().Id;
    // Account acc = getAccount();
    system.debug('AccountId************'+getAccount().Id);
    newSite.AccountId__c =  getAccount().Id;
    
    newSite.Address_Type__c = 'Billing Address';
    insert newSite;
    system.assert(newSite!=null);
    system.assertEquals(newSite.Name , 'Test_site'); 
    return newSite;
 }   
    
   // Inserting the values in Account Object for testing
   private static Account getAccount(){
       if(acc1 == null){
            acc1 = new Account();
            acc1.Name = 'L&T Test';
            acc1.Customer_Type__c = 'MNC';
            acc1.CurrencyIsoCode ='GBP';
            acc1.Customer_Legal_Entity_Name__c = 'Sample Entity Name';
            Country_Lookup__c acc2 = getCountry();
            acc1.Country__c = acc2.Id;
            acc1.Selling_Entity__c ='Telstra Limited';       
            insert acc1;
            system.assert(acc1!=null);
            system.assertEquals(acc1.Name , 'L&T Test');
       }
         system.debug('actID**********'+acc1.Id);
        return acc1;
      }                          
 
 // Inserting the values in Bill Profile Object for testing                  
 private static BillProfile__c getBillProfile(){
    
    BillProfile__c bp = new BillProfile__c();
    bp.Name = 'L&T Test Account';
    
   
    Site__c siteId = getSite(); 
   
    system.debug('siteId:*********'+siteId);
    bp.Bill_Profile_Site__c = siteId.Id ; 
    
    //system.debug('Site Name inside BP****::::' + siteId.Id);  
    bp.CurrencyIsoCode = 'GBP';
    bp.Account__c = siteId.AccountId__c;
    system.debug('BpAccountId*******'+bp.Account__c);
    bp.Approved_by_Finance__c = true;
    bp.Approved_by_Legal__c = true;
    bp.First_Period_Date__c =  system.today();
    bp.Invoice_Breakdown__c ='Cost Center';
    bp.Start_Date__c = system.today();
    insert bp;
    system.assert(bp!=null);
    return bp;
    
 }      
 
 // Inserting the values in Country_Lookup Object for testing       
 private static Country_Lookup__c getCountry(){
                    
    Country_Lookup__c co = new Country_Lookup__c();
    co.Name = 'INDIA';
    co.Country_Code__c = 'IN';
    insert co;
    system.assert(co!=null);
    system.assertEquals(co.Name , 'INDIA');
    return co;
                    
  }
  
  private static City_Lookup__c getCity(){
    
    City_Lookup__c city = new City_Lookup__c();
    city.City_Code__c ='BGL';
    city.Name = 'BANGALORE';
    
    insert city;
    system.assert(city!=null);
    system.assertEquals(city.Name , 'BANGALORE');
    
    return city;
  }
            
           
}