@isTest
public class UpdateProcessStepEndDateTimeBatch_Test{
/*
 private static cscfga__Product_Basket__c basket = null;
    
    private static cscfga__Product_Configuration__c config = null;
    private static cscfga__Product_Configuration__c masterConfig = null;
    
    private static csord__Service__c service = null;
    private static csord__Service__c masterService = null;
    
    private static List<Offer_Id__c> offIdlist;
    private static List<csordtelcoa__Change_Types__c> changeTypeCSList;
    private static List<Product_Definition_Id__c> pdCSList;
    private static List<String> ordIList = new List<String>();
    private static List<csord__Order__c> ordList;
    private static List<csord__Order_Request__c> OrdReqList;
    private static Database.QueryLocator QL;
    private static Database.BatchableContext BC;
    private static Map<Id, List<CSPOFA__Orchestration_Step__c>> stepMap = new Map<Id, List<CSPOFA__Orchestration_Step__c>>();
    private static List<CSPOFA__Orchestration_Step__c> ostep;
    private static List<CSPOFA__Orchestration_Process__c> orcproc1;
    private static List<CSPOFA__Orchestration_Process_Template__c> ProcTemp;
    
    private static void disableAll(Id userId) {
        Global_Mute__c globalMute = Global_Mute__c.getInstance(userId);

        if(globalMute == null) {
            globalMute = new Global_Mute__c();
            globalMute.SetupOwnerId = userId;
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        } else {
            globalMute.Mute_Triggers__c = true;
            globalMute.Mute_Validations__c = true;
            globalMute.Mute_Workflows__c = true;
        }

        upsert globalMute;
    }

     private static void disableTelcoAll(Id userId) {
        csordtelcoa__Orders_Subscriptions_Options__c telcoOptions = csordtelcoa__Orders_Subscriptions_Options__c.getInstance(userId);

        if(telcoOptions == null) {
            telcoOptions = new csordtelcoa__Orders_Subscriptions_Options__c ();
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
            telcoOptions.SetupOwnerId = userId;
        } else {
            telcoOptions.csordtelcoa__Disable_Triggers__c = true;
        }

        upsert telcoOptions;
    }
    
   @TestSetup
    static void SetupTestData(){
    
        P2A_TestFactoryCls.SetupTestData();
        
        OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        ordList = P2A_TestFactoryCls.getorder(1,OrdReqList);
        ProcTemp = P2A_TestFactoryCls.getOrchestrationProcess(1);
        orcproc1 = P2A_TestFactoryCls.getOrchestrationProcesss(1, ProcTemp);
        ostep = P2A_TestFactoryCls.getOrchestrationStep(1,orcproc1);
        
        for(csord__Order__c ord : ordList){
            ordIList.add(ord.id);
        }
        
        stepMap.put(ostep[0].id, ostep);
        
    
        //TestDataFactory.createProductDefinitionIdCustomSettings(TestDataFactory.createProductDefinition('Test Prod', 'Test Desc', true));
        csord__Order_Request__c ordReq = new csord__Order_Request__c(name='Test Ord Req', csord__Module_Version__c='1.0', 
                                            csord__Module_Name__c = 'Test Module');
        insert ordReq;
        Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        
        
        CSPOFA__Orchestration_Process_Template__c thisProcTempl = new CSPOFA__Orchestration_Process_Template__c(name='Test Proc Templ',
                                                                  CSPOFA__Processing_Mode__c = 'Background');
        insert thisProcTempl;
        
        cspofa__Orchestration_Process__c orcproc = new cspofa__Orchestration_Process__c(name='Test Process', 
                                                      Order__c = ordList[0].id,                                                   
                                                      CSPOFA__State__c = 'ACTIVE'
                                                      );
        
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 2.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '1');
        insert thisStepTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl2 = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template2',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 4.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '2');
        insert thisStepTempl2;
        csord__Subscription__c sub = new csord__Subscription__c(
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    name = 'Some Sub');
        insert sub;

        csord__Order__c order = new csord__Order__c(name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2017,01,01));
        insert order;       
        sub.csord__Order__c = order.Id;
        update sub;
       
        csord__Service__c someServ = new csord__Service__c(csordtelcoa__Service_Number__c = '21234-', 
                                        Estimated_Start_Date__c = system.now().addDays(10),
                                        name = 'Test Serv', csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                        csord__Order_Request__c = ordReqId, csord__Subscription__c = sub.Id);
        
        
        insert someServ;
         
        CSPOFA__Orchestration_Process__c thisProc = new CSPOFA__Orchestration_Process__c(name='Test Process', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30,
                                                      CSPOFA__Status__c = 'Initialising',
                                                      order__c = order.Id, csordtelcoa__Service__c = someServ.Id);
        insert thisProc;

        CSPOFA__Orchestration_Process__c thisNewProc = new CSPOFA__Orchestration_Process__c(name='Test Process2', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30, 
                                                      CSPOFA__Status__c = 'In Progress',
                                                      csordtelcoa__Service__c = someServ.Id);
        insert thisNewProc;
        
        
        CSPOFA__Orchestration_Process__c thisNewPr = new CSPOFA__Orchestration_Process__c(name='Test Process2', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30, 
                                                      CSPOFA__Status__c = 'Complete',
                                                      csordtelcoa__Service__c = someServ.Id);
        insert thisNewPr;

        cspofa__Orchestration_Step__c thisStep = new cspofa__Orchestration_Step__c(name='Test Step', 
                                                    CSPOFA__Orchestration_Process__c = thisProc.Id,
                                                    CSPOFA__Status__c = 'Initializing',
                                                    CSPOFA__Orchestration_Step_Template__c = thisStepTempl.Id,
                                                    Estimated_Time_To_Complete__c = 2.0);
        insert thisStep;
        
        cspofa__Orchestration_Step__c thisNewStep = new cspofa__Orchestration_Step__c(name='Test Step2', 
                                                    CSPOFA__Orchestration_Process__c = thisProc.Id, 
                                                    CSPOFA__Status__c = 'Initializing',
                                                    CSPOFA__Orchestration_Step_Template__c = thisStepTempl2.Id,
                                                    Estimated_Time_To_Complete__c = 4.0);
        insert thisNewStep; 

       
    } 
    
    
    static testmethod void Test(){
        
        disableAll(UserInfo.getUserId());
        disableTelcoAll(UserInfo.getUserId());
        
         UpdateProcessStepEndDateTimeBatch ust = new UpdateProcessStepEndDateTimeBatch();
         List<CSPOFA__Orchestration_Process_Template__c> ProcessTemplatesLists = P2A_TestFactoryCls.getOrchestrationProcess(1);
         List<CSPOFA__Orchestration_Process__c>  orc = P2A_TestFactoryCls.getOrchestrationProcesss(1, ProcessTemplatesLists);
        
        Test.startTest();
        List<cspofa__Orchestration_Step__c> oldStepsMap = 
                              new List<cspofa__Orchestration_Step__c>([select Id, Name, Start_Date_Time__c,End_Date_Time__c,
                                                              Estimated_Time_To_Complete__c, CSPOFA__Orchestration_Process__c,
                                                              CSPOFA__Status__c, RAG_Status__c, 
                                                              CSPOFA__Orchestration_Process__r.Order__r.RAG_Status__c,
                                                              CSPOFA__Orchestration_Process__r.csordtelcoa__Service__c,
                                                              CSPOFA__Orchestration_Process__r.order__c,
                                                              CSPOFA__Orchestration_Step_Template__r.CSPOFA__Milestone_Label__c,
                                                              CSPOFA__Orchestration_Step_Template__r.Estimated_Time_to_Complete__c
                                                              from cspofa__Orchestration_Step__c
                                                              where CSPOFA__Orchestration_Process__c != null]);
                                                              
        csord__Order_Request__c ordReq = new csord__Order_Request__c(name='Test Ord Req', csord__Module_Version__c='1.0', 
                                            csord__Module_Name__c = 'Test Module');
        insert ordReq;
        Id ordReqId = [select Id from csord__Order_Request__c ORDER BY CreatedDate DESC LIMIT 1].get(0).Id;
        

        CSPOFA__Orchestration_Process_Template__c thisProcTempl = new CSPOFA__Orchestration_Process_Template__c(name='Test Proc Templ',
                                                                  CSPOFA__Processing_Mode__c = 'Background');
        insert thisProcTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 2.0, 
                                                              CSPOFA__OLA_Unit__c = 'day', 
                                                              CSPOFA__Milestone_Label__c = '1');
        insert thisStepTempl;
        CSPOFA__Orchestration_Step_Template__c thisStepTempl2 = new CSPOFA__Orchestration_Step_Template__c(name='Test Step Template2',
                                                              CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id, 
                                                              Estimated_Time_To_Complete__c = 4.0, 
                                                            CSPOFA__OLA_Unit__c = 'day', 
                                                            CSPOFA__Milestone_Label__c = '2');
        insert thisStepTempl2;
        csord__Subscription__c sub = new csord__Subscription__c(
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    name = 'Some Sub');
        insert sub;

        csord__Order__c order = new csord__Order__c(name='Test Order2', 
                                                    csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                                    csord__Order_Request__c = ordReq.Id,
                                                    Customer_Required_Date__c = Date.newInstance(2017,01,01));
        insert order;

        sub.csord__Order__c = order.Id;
        update sub;
       
        csord__Service__c someServ = new csord__Service__c(csordtelcoa__Service_Number__c = '21234-', 
                                        Estimated_Start_Date__c = system.now().addDays(10),
                                        name = 'Test Serv', csord__Identification__c = 'Order_a24O0000000OZ7aIAG', 
                                        csord__Order_Request__c = ordReqId, csord__Subscription__c = sub.Id);
        
        
        insert someServ;
         
        CSPOFA__Orchestration_Process__c thisProc = new CSPOFA__Orchestration_Process__c(name='Test Process', 
                                                      CSPOFA__Orchestration_Process_Template__c = thisProcTempl.Id,
                                                      Estimated_Time_To_Complete__c = 30,
                                                      CSPOFA__Status__c = 'Initialising',
                                                      order__c = order.Id, csordtelcoa__Service__c = someServ.Id);
        insert thisProc;                                                      
                                                              
        cspofa__Orchestration_Step__c  NewStep = new cspofa__Orchestration_Step__c(name='Test Step2', 
                                                    CSPOFA__Orchestration_Process__c = thisProc.Id, 
                                                    CSPOFA__Status__c = 'In Progress',
                                                    CSPOFA__Orchestration_Step_Template__c = thisStepTempl2.Id,
                                                    Estimated_Time_To_Complete__c = 4.0);
        insert NewStep ;                                                        
                                                              
        
        Exception ee = null;
        
        QL = ust.start(BC);
        ust.execute(null,orc);
        ust.finish(BC);
        UpdateProcessStepEndDateTimeBatch.executeIt(ordIList);
        UpdateProcessStepEndDateTimeBatch.getStepsFromWrapper(stepMap);
        try{        
        
        UpdateProcessStepEndDateTimeBatch.checkAndPostponeEndDateTime(oldStepsMap,NewStep);
        
        ust.execute(null,orc);        
      } catch(Exception e){
            ee = e;
        }

    }
*/
    static testmethod void testcase()
    {
        
        P2A_TestFactoryCls.SetupTestData();
        
       list<csord__Order_Request__c> OrdReqList = P2A_TestFactoryCls.getorderrequest(1);
        list<csord__Order__c> ordList = P2A_TestFactoryCls.getorder(1,OrdReqList);
        list<CSPOFA__Orchestration_Process_Template__c> ProcTemp = P2A_TestFactoryCls.getOrchestrationProcess(1);
       list<CSPOFA__Orchestration_Process__c> orcproc1 = P2A_TestFactoryCls.getOrchestrationProcesss(1, ProcTemp);
        for(CSPOFA__Orchestration_Process__c op:orcproc1)
        {
           op.CSPOFA__State__c='ACTIVE'; 
            op.Order__c=ordList[0].id;
        }
        update orcproc1;
        system.assert(orcproc1!=null);
        list<CSPOFA__Orchestration_Step__c> ostep = P2A_TestFactoryCls.getOrchestrationStep(1,orcproc1);
        UpdateProcessStepEndDateTimeBatch ups=new UpdateProcessStepEndDateTimeBatch();
        Map<Id, List<CSPOFA__Orchestration_Step__c>> stepMap = new Map<Id, List<CSPOFA__Orchestration_Step__c>> ();
        for(csord__Order__c ord : ordList){
           // ordIList.add(ord.id);
        }
        
        stepMap.put(ostep[0].id, ostep);
        
        stepMap.put(ostep[0].id,ostep);
        cspofa__Orchestration_Step__c stepToCheck = new cspofa__Orchestration_Step__c ();
       list<string> orderIds = new list<string>();
           Database.QueryLocator QL;
            Database.BatchableContext ctx;
            QL=ups.start(ctx);
        ups.execute(ctx, orcproc1);
        ups.finish(ctx);
        Test.Starttest();
       UpdateProcessStepEndDateTimeBatch.getStepsFromWrapper(stepMap);
        UpdateProcessStepEndDateTimeBatch.executeIt(orderIds);
       UpdateProcessStepEndDateTimeBatch.checkAndPostponeEndDateTime(ostep,stepToCheck);
        Test.stoptest();
    }
    
}