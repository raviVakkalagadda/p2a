/**
 * @name UpdateProcessStepEndDateTimeScheduler 
 * @description scheduler class for 'UpdateProcessStepEndDateTimeBatch' batch process
 * @revision
 * Boris Bjelan 25-03-2016 Created class
 */
global class UpdateProcessStepEndDateTimeScheduler implements Schedulable {
    
    public static String CRON_EXP = '0 15 * * * ?';
    
    global void execute(SchedulableContext ctx) {
        UpdateProcessStepEndDateTimeBatch bc = new UpdateProcessStepEndDateTimeBatch();  
        Database.executeBatch(bc, 2);
    }
    
    global static String scheduleIt() {
        UpdateProcessStepEndDateTimeScheduler scheduler = new UpdateProcessStepEndDateTimeScheduler ();
        return System.schedule('UpdateProcessStepEndDateTimeScheduler Job', CRON_EXP, scheduler);
    }
}