public class UpdateProdConfigCurrency{
    public static void updateProductConfigCurrency(ID opprtunity_ID, Decimal subscriptionExchangeRate, String subscriptionCurrencyIsoCode){
        String IsoCode;
        
        system.debug('This is inside updateProductConfigCurrency class');
        system.debug('Opportunity_ID  '+opprtunity_ID+ '  subscriptionExchangeRate '+subscriptionExchangeRate);
        List<cscfga__Product_Configuration__c> pcs = [select Id, Name,Rate_Card_NRC__c,Rate_Card_RC__c, CurrencyIsoCode, cscfga__Product_Basket__c,cscfga_Offer_Price_MRC__c, cscfga_Offer_Price_NRC__c, Child_COst__c, Cost_NRC__c, Cost_MRC__c, Rate_China_Burst_Rate__c, Rate_Burst_Rate__c from cscfga__Product_Configuration__c 
        where cscfga__Product_Basket__r.cscfga__Opportunity__c =: opprtunity_ID];
        
    if(pcs.size() > 0){

        IsoCode = pcs[0].CurrencyIsoCode;
        system.debug('Product Configuration List fetched from the above opportunity ID  '+pcs);
        for (cscfga__Product_Configuration__c pc: pcs) {            
            
            if(pc.cscfga_Offer_Price_MRC__c!=null){
                pc.cscfga_Offer_Price_MRC__c =  ( pc.cscfga_Offer_Price_MRC__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.cscfga_Offer_Price_NRC__c!=null){
                pc.cscfga_Offer_Price_NRC__c =  ( pc.cscfga_Offer_Price_NRC__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.Child_COst__c!=null){
                pc.Child_COst__c =  ( pc.Child_COst__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.Cost_NRC__c!=null){
                pc.Cost_NRC__c =  ( pc.Cost_NRC__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.Cost_MRC__c!=null){
                pc.Cost_MRC__c =  ( pc.Cost_MRC__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.Rate_Card_NRC__c!=null){
                pc.Rate_Card_NRC__c = (pc.Rate_Card_NRC__c / subscriptionExchangeRate).setScale(2);
            }
            if(pc.Rate_Card_RC__c!=null){
                pc.Rate_Card_RC__c = (pc.Rate_Card_RC__c / subscriptionExchangeRate).setScale(2);
            }           
        }
        update pcs;
    }
        
        List<cscfga__Attribute__c> atts = [select Id, Name, CurrencyIsoCode,cscfga__Is_rate_line_item__c, cscfga__Value__c, cscfga__Attribute_Definition__r.Calculate_Exchange_Rate__c, cscfga__Price__c, cscfga__Is_Line_Item__c, cscfga__Product_Configuration__r.cscfga__Product_Basket__c from cscfga__Attribute__c
            where (cscfga__Is_Line_Item__c = true or cscfga__Is_rate_line_item__c = true or cscfga__Attribute_Definition__r.Calculate_Exchange_Rate__c = true or Name = 'Product Basket Currency Ratio' or Name = 'Product Basket Currency')
        and cscfga__Product_Configuration__r.cscfga__Product_Basket__r.cscfga__Opportunity__c =: opprtunity_ID];
        system.debug('Attribiutes List fetched from above opportunity ID  '+atts);
        
    if(atts.size() > 0){
        try{
            for (cscfga__Attribute__c att: atts) {
                System.debug('<><> att ID == ' + att.Id);
                 /* Changes made to fix Multicurrency  QC # 050 and 051, Dev Name: Anuradha, Date changed: 28/11/2016 */
                  if(att.CurrencyIsoCode != null && IsoCode != null)
                   att.CurrencyIsoCode = IsoCode;
                   //defect End
                if (att.cscfga__Price__c != null && (att.cscfga__Is_Line_Item__c || att.cscfga__Is_rate_line_item__c)) {
                        att.cscfga__Price__c = (att.cscfga__Price__c / subscriptionExchangeRate ).setScale(2);
                        att.cscfga__Value__c = String.valueOf(att.cscfga__Price__c);
                    }  else if (att.cscfga__Value__c != null && att.cscfga__Value__c != '' && att.cscfga__Attribute_Definition__r.Calculate_Exchange_Rate__c){                    
                        att.cscfga__Value__c = String.valueOf((Decimal.valueOf(att.cscfga__Value__c) / subscriptionExchangeRate ).setScale(2));
                        att.cscfga__Price__c = Decimal.valueOf(att.cscfga__Value__c);                
                    }
                    if(att.Name=='Product Basket Currency' && IsoCode != null)
                    {
                            att.cscfga__Value__c= IsoCode;
                            system.debug('att.Name  '+att.Name+'  att.cscfga__Value__c  ' +att.cscfga__Value__c);
                            system.debug('Time printing this  in  updateProductConfigCurrency '+System.now());
                    }
                    
                    //else if(att.Name=='Product Basket Currency Ratio' && att.cscfga__Value__c != null && Decimal.valueOf(att.cscfga__Value__c) > 0)
                    else if(att.Name=='Product Basket Currency Ratio' && att.cscfga__Value__c != null && Decimal.valueOf(String.valueOf(att.cscfga__Value__c)) > 0)
                    {
                            att.cscfga__Value__c= String.valueOf(Decimal.valueOf(att.cscfga__Value__c)/subscriptionExchangeRate);
                            system.debug('att.Name  '+att.Name+'  att.cscfga__Value__c  ' +att.cscfga__Value__c);
                            system.debug('Time printing this  in  updateProductConfigCurrency '+System.now());
                    }
                    
            }
            update atts;
        } catch(exception e){
            system.debug('Exception: '+e);
			ErrorHandlerException.ExecutingClassName='updateProdConfigCurrency:updateProductConfigCurrency';
            ErrorHandlerException.objectId= opprtunity_ID;
            ErrorHandlerException.sendException(e);
        }
	}
    }
}