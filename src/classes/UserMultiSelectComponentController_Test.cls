@isTest(SeeAllData = false)
public class UserMultiSelectComponentController_Test{
 public static List<Supplier__c> supplierlist; 
 public static list<String> InitialRightList = new list<String>(); 
 public static list<String> CurrentRightList = new list<String>();
 
 public static list<String> LeftSelectedList = new list<String>();
 public static list<String> RightSelectedList= new list<String>();
 public static map<String, User> LeftOptionMap = new map<String, User>();
 public static map<String, User> RightOptionMap = new map<String, User>();
 
 
    
    private static void initTestData(){ 
       List<User> usrList =  P2A_TestFactoryCls.get_Users(1);
       LeftSelectedList.add('New');
       RightSelectedList.add('In-progress');
       LeftOptionMap.put(usrList[0].id,usrList[0]);
       RightOptionMap.put(usrList[1].id,usrList[1]);
       system.assert(usrList!=null);
    }
    private static testMethod void multiSelectComponentController()
    {
            InitialRightList.add('Hello');
            CurrentRightList.add('Hi');
            //initTestData();
            Test.startTest();
            UserMultiSelectComponentController umsc = new UserMultiSelectComponentController();
            umsc.InitialRightList = InitialRightList;
            umsc.CurrentRightList= CurrentRightList;
            umsc.ClickRight();
            umsc.ClickLeft();
            umsc.getLeftOptionList();
            umsc.getRightOptionList();
            umsc.Find(); 
            system.assert(umsc!=null);           
            Test.stopTest();                
            
    }
}