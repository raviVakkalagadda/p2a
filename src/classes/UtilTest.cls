/**
    @author - Accenture
    @date - 19-JULY-2012
    @version - 1.0
    @description - This is a test class for Util class.
*/
@isTest(SeeAllData = false)

public class UtilTest {

  
 
 public static void uTest() {
        

        List<Account> accList = P2A_TestFactoryCls.getAccounts(1);
        List<Opportunity> oppList = P2A_TestFactoryCls.getOpportunitys(1, accList);
        List<cscfga__product_basket__c> prodBaskList = P2A_TestFactoryCls.getProductBasketHdlr(1,oppList);
        List<User> users = P2A_TestFactoryCls.get_Users(1);
        List<Contact> contactList = P2A_TestFactoryCls.getContact(1,AccList);
        List<Country_Lookup__c> countrylist = P2A_TestFactoryCls.getcountry(1);
        List<Site__c> SitesList = P2A_TestFactoryCls.getsites(1,AccList,countrylist);
        List<BillProfile__c> BillProfiles = P2A_TestFactoryCls.getBPs(1,AccList,SitesList,contactList);
        
        Opplist[0].Pre_Contract_Provisioning_Required__c = 'Yes';
        upsert Opplist[0];
        System.assert(Opplist[0].id != null); 
        
        Action_Item__c aitem = new Action_Item__c();
        aitem.Account__c = acclist[0].id;
        aitem.Bill_Profile__c = BillProfiles[0].id;
        aitem.Customer_Type__c = 'MNC';
        aitem.Account_industry__c = 'Test Industry';
        aitem.Country__c = 'Hong Kong';
        aitem.Previous_NRC__c = 500;
        aitem.Opportunity_Email__c = 'test@accenture.com';
        aitem.Created_By_Region__c = 'Hong kong';
        insert aitem;
        
        System.assert(aitem.id != null); 
        
        
       id recordtype = RecordTypeUtil.getRecordTypeIdByName(Action_Item__c.SObjectType, 'Request Contract');
       
       Map<id,id> accountOppbillProfileMapId = new Map<id,id>();
       accountOppbillProfileMapId.put(opplist[0].id,BillProfiles[0].id);
       
       Map<id,Account> oldMap = new Map<id,Account>();
       oldMap.put(acclist[0].id,acclist[0]);
       
       profile pr = [select id from profile where name='System administrator'];
                User u = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1203456@testorg.com',EmployeeNumber='123',Region__c = 'North Asia' );
            
            insert u;
            system.assert(u!=null);
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Request Contract');
        
        User ur = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser11203456@testorg.com',EmployeeNumber='123',Region__c = 'South Asia' );
            
            insert ur;
            
        System.assert(ur.id != null); 
            
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Request Contract');
        
        User urs = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser121203456@testorg.com',EmployeeNumber='123',Region__c = 'Australia' );
            
            insert urs;
        System.assert(urs.id != null); 
            
             User ures = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1291203456@testorg.com',EmployeeNumber='123',Region__c = 'EMEA' );
            
            insert ures;
        System.assert(ures.id != null); 
            
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Request Contract');
        
         User usres = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12091203456@testorg.com',EmployeeNumber='123',Region__c = 'US' );
            
            insert usres;
        System.assert(usres.id != null); 
       
         Account acc = new Account();
                acc.name= 'Test Accenture';
                acc.BillingCountry                = 'GB';
                acc.Activated__c                  = True;
                acc.Account_ID__c                 = '3333';
                acc.Account_Status__c             = 'Active';
                acc.Customer_Legal_Entity_Name__c = 'Test';
                //acc.Customer_Type_New__c = 'GSP';
                acc.Customer_Type__c = 'GSP';
                acc.Region__c = 'US';
                insert acc;
        System.assert(acc != null); 
        system.assertEquals(acc.name, 'Test Accenture');
        
        Action_Item__c aitems = new Action_Item__c();
        aitems.Account__c = acc.id;
        aitems.Bill_Profile__c = BillProfiles[0].id;
        aitems.Customer_Type__c = 'MNC';
        aitems.Account_industry__c = 'Test Industry';
        aitems.Country__c = 'Hong Kong';
        aitems.Previous_NRC__c = 500;
        aitems.Opportunity_Email__c = 'test@accenture.com';
        aitems.Created_By_Region__c = 'Hong kong';
        insert aitems;
        System.assert(aitems.id != null); 

        
        Global_Constant_Data__c GC = new Global_Constant_Data__c();
        gc.Name = 'Action Item Status';
        gc.value__c = 'Assigned';
        insert gc;
        System.assert(gc.id != null); 
        system.assertEquals(gc.name, 'Action Item Status');
       
        Global_Constant_Data__c GC1 = new Global_Constant_Data__c();
        gc1.Name = 'Action Item Priority';
        gc1.value__c = 'High';
        insert gc1;
        System.assert(gc1.id != null); 
        system.assertEquals(gc1.name, 'Action Item Priority');
       
        PACNET_Entities__c p = new PACNET_Entities__c();
        p.name = 'Test Pacnet';
        p.PACNET_Entity__c = 'Test Pacnet Entity';
        insert p;
        System.assert(p.id != null);
        system.assertEquals(p.name, 'Test Pacnet'); 
        
        Mute_SD_Order_Date__c MSD = new Mute_SD_Order_Date__c();
        MSD.Name = 'Test Date';
        MSD.MuteCreateUpdateServiceOrResource__c = true;
        MSD.MuteOrderLineItemAfterUpdate__c = true;
        MSD.MuteOrderLineItemBeforeInsertUpdate__c = true;
        MSD.MuteOrderLineItemBeforeUpdate__c = true; 
        MSD.Mute_SD_dates__c = true ;
        insert MSD;
        System.assert(MSD.id != null); 
       
                User u1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1a1203456@testorg.com',EmployeeNumber='123',Region__c = System.Label.NORTH_ASIA );
            
            insert u1;
        System.assert(u1.id != null); 
        
        User ur1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser101203456@testorg.com',EmployeeNumber='123',Region__c = System.Label.SOUTH_ASIA );
            
            insert ur1;
         System.assert(ur1.id != null); 
       
        User urs1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser1212034516@testorg.com',EmployeeNumber='123',Region__c = System.Label.AUSTRALIA );
            
            insert urs1;
        System.assert(urs1.id != null); 
            
             User ures1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser12291203456@testorg.com',EmployeeNumber='123',Region__c = System.Label.EMEA );
            
            insert ures1;
        System.assert(ures1.id != null); 
        
         User usres1 = new User(Alias = 'standt', Email='standarduser@testorg.com', 
                EmailEncodingKey='UTF-8', LastName='Testing1', LanguageLocaleKey='en_US', 
                LocaleSidKey='en_US', ProfileId = pr.Id, 
                TimeZoneSidKey='America/Los_Angeles', UserName='standarduser132091203456@testorg.com',EmployeeNumber='123',Region__c = System.Label.US );
            
            insert usres1;
        System.assert(usres1.id != null); 

       
       Test.starttest();
       
      Util ut = new Util();
      Boolean AT = Util.muteAllTriggers();
      Boolean CUSOR = Util.MuteCreateUpdateServiceOrResource();
      Boolean Orderlineitem = Util.MuteOrderLineItemAfterUpdate();
      Boolean MOrderlineitem = Util.MuteOrderLineItemBeforeInsertUpdate();
      Boolean MuteOrderLineItemB = Util.MuteOrderLineItemBeforeUpdate();
      Boolean muteDateUpdateOnOrder = Util.muteDateUpdateOnOrder();
      Action_Item__c a = Util.createActionItem('Testname',accountOppbillProfileMapId,oppList[0].id ,BillProfiles[0].id); 
      Util.assignOwnersToRegionalQueue(users[0] ,aitem ,recordtype);
      Boolean compareOldwithNewRollUpData = Util.compareOldwithNewRollUpData(Acclist,oldMap);
      
              Util.assignOwnersToRegionalQueue(urs ,aitem ,'Request Contract');
        
                Util.assignOwnersToRegionalQueue(usres ,aitem ,'Request Contract');
        
        
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Request for Bill Profile Approval');
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Request for Bill Profile Approval');
        Util.assignOwnersToRegionalQueue(urs ,aitem ,'Request for Bill Profile Approval');
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Request for Bill Profile Approval');
        Util.assignOwnersToRegionalQueue(usres ,aitem ,'Request for Bill Profile Approval');
        
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Adhoc Request');
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Adhoc Request');
        Util.assignOwnersToRegionalQueue(urs ,aitem ,'Adhoc Request');
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Adhoc Request');
        Util.assignOwnersToRegionalQueue(usres ,aitem ,'Adhoc Request');
        
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Request Credit Check');
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Request Credit Check');
        Util.assignOwnersToRegionalQueue(urs ,aitem ,'Request Credit Check');
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Request Credit Check');
        Util.assignOwnersToRegionalQueue(usres ,aitem ,'Request Credit Check');
        
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(urs ,aitem ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(usres ,aitem ,'Request Feasibility Study');
        
        Util.assignOwnersToRegionalQueue(u ,aitem ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(ur ,aitem ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(urs ,aitem ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(ures ,aitem ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(usres ,aitem ,'Request Pricing Approval');
        
   
        Util.assignOwnersToRegionalQueue(u ,aitems ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(ur ,aitems ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(urs ,aitems ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(ures ,aitems ,'Request Feasibility Study');
        Util.assignOwnersToRegionalQueue(usres ,aitems ,'Request Feasibility Study');
        
        Util.assignOwnersToRegionalQueue(u ,aitems ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(ur ,aitems ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(urs ,aitems ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(ures ,aitems ,'Request Pricing Approval');
        Util.assignOwnersToRegionalQueue(usres ,aitems ,'Request Pricing Approval');
        
        
        
        Util.assignOwnersToRegionalQueue(u1 ,aitems , System.Label.REQUEST_SERVICE_DETAILS);
        Util.assignOwnersToRegionalQueue(ur1 ,aitems , System.Label.REQUEST_SERVICE_DETAILS);
        Util.assignOwnersToRegionalQueue(urs1 ,aitems , System.Label.REQUEST_SERVICE_DETAILS);
        Util.assignOwnersToRegionalQueue(ures1 ,aitems , System.Label.REQUEST_SERVICE_DETAILS);
        Util.assignOwnersToRegionalQueue(usres1 ,aitems , System.Label.REQUEST_SERVICE_DETAILS);
        
        Util.assignOwnersToRegionalQueue(u1,aitems,'Request to Modify Opportunity Split');
        Util.assignOwnersToRegionalQueue(ur1 ,aitems , 'Request to Modify Opportunity Split');
        Util.assignOwnersToRegionalQueue(urs1 ,aitems , 'Request to Modify Opportunity Split');
        Util.assignOwnersToRegionalQueue(ures1 ,aitems , 'Request to Modify Opportunity Split');
        Util.assignOwnersToRegionalQueue(usres1 ,aitems ,'Request to Modify Opportunity Split');
        
                
        
               
        Test.stoptest();
      
        }
        
         private static testMethod void Test1() {
        integer userid = 0;
        Exception ee = null;
        try {
            P2A_TestFactoryCls.disableAll(UserInfo.getUserId());
            system.assert(userid !=null);
            uTest();
            } catch(Exception e) {
            ErrorHandlerException.ExecutingClassName='TestUtil:Test1';         
            ErrorHandlerException.sendException(e);
            ee = e;
        } finally {
            P2A_TestFactoryCls.enableAll(UserInfo.getUserId());
            if (ee != null) {
                throw ee;
            } 
        }  
    }
          }