public with sharing class VendorQuoteTriggerHandler extends BaseTriggerHandler{
    public override void afterInsert(List<SObject> newVendorQuotes, Map<Id, SObject> newVendorQuotesMap) {
        vendorQuoteDetails((List<VendorQuoteDetail__c>)newVendorQuotes, null);
    }

    public override void afterUpdate(List<SObject> newVendorQuotes, Map<Id, SObject> newVendorQuotesMap, Map<Id, SObject> oldVendorQuotesMap) {
        vendorQuoteDetails((List<VendorQuoteDetail__c>)newVendorQuotes, (Map<Id, VendorQuoteDetail__c>)oldVendorQuotesMap);
    }

    /*
    *   Moved in from VendorQuoteDetails trigger
    */
    @TestVisible
    Private void vendorQuoteDetails(List<VendorQuoteDetail__c> newVendorQuotes, Map<Id, VendorQuoteDetail__c> oldVendorQuotesMap){
         system.debug('VendorCheck');
        Set<ID> caseIDSet = new Set<ID>();
        Set<ID> propQuote= new Set<ID>();
        Set<ID> winningQuote= new Set<ID>();    
        Set<ID> isPropQuoteChange  = new Set<ID>();    
        Set<ID> isWinQuoteChange  = new Set<ID>();        
        Set<ID> winQuoteChangeCaseID = new Set<ID>();
        Set<ID> caseUpddate = new Set<ID>();
        
        for(VendorQuoteDetail__c venObj : newVendorQuotes){
            if((venObj.Proposed_Quote__c && oldVendorQuotesMap == null) || (oldVendorQuotesMap != null && venObj.Proposed_Quote__c && venObj.Proposed_Quote__c != oldVendorQuotesMap.get(venObj.id).Proposed_Quote__c)){
                propQuote.add(venObj.ID);
                caseIDSet.add(venObj.Related_3PQ_Case_Record__c);
                isPropQuoteChange.add(venObj.Related_3PQ_Case_Record__c);  
            }            
            
            if((venObj.Winning_Quote__c  && oldVendorQuotesMap == null) || (oldVendorQuotesMap != null && venObj.Winning_Quote__c  && venObj.Winning_Quote__c != oldVendorQuotesMap.get(venObj.id).Winning_Quote__c)){
                winningQuote.add(venObj.ID);
                caseIDSet.add(venObj.Related_3PQ_Case_Record__c);
                system.debug('vendorcheck'+venObj.Related_3PQ_Case_Record__c);
                isWinQuoteChange.add(venObj.Related_3PQ_Case_Record__c);  
                winQuoteChangeCaseID.add(venObj.Related_3PQ_Case_Record__c); 
            }            
            if(venObj.Winning_Quote__c == true){   
                caseUpddate.add(venObj.Related_3PQ_Case_Record__c);
            }
        }                 
        List<VendorQuoteDetail__c> venderQuoteLst = [Select Related_3PQ_Case_Record__c, Winning_Quote__c, Proposed_Quote__c from VendorQuoteDetail__c  where Related_3PQ_Case_Record__c in:caseIDSet];        
        system.debug('VendorCheck'+caseIDset);
        for(VendorQuoteDetail__c obj : venderQuoteLst ){
            if(!winningQuote.contains(obj.ID) &&isWinQuoteChange.contains(obj.Related_3PQ_Case_Record__c) ){
                obj.Winning_Quote__c = false;
                system.debug('vendorcheck'+obj.Winning_Quote__c);
            }           
        }
        update venderQuoteLst;
    }
}