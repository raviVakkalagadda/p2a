@isTest
public class VendorQuoteTriggerHandlerTest 
{      
    static testmethod void TestVenQuoteTrgHdlrMethod()
    {
        
        
        VendorQuoteTriggerHandler venHdlrDtls = new VendorQuoteTriggerHandler();
       // VendorQuoteDetail__c venQuote = new VendorQuoteDetail__c()
        Map<Id,VendorQuoteDetail__c> venMap = new Map<Id,VendorQuoteDetail__c>();
        Map<Id,VendorQuoteDetail__c> venNewMap = new Map<Id,VendorQuoteDetail__c>();        
        Test.startTest();
        venHdlrDtls.isDisabled();
        venHdlrDtls.getName();
        List<Supplier__c> Listsupplier =  P2A_TestFactoryCls.getsuppliers(1);
        List<VendorQuoteDetail__c> venList = P2A_TestFactoryCls.getvendorQuote(1,Listsupplier);
        List<VendorQuoteDetail__c> venNewList = P2A_TestFactoryCls.getvendorQuote(1,Listsupplier);
        venNewList.get(0).Proposed_Quote__c = true;
        venNewList.get(0).Winning_Quote__c = true;
        update venNewList;
        
        for(VendorQuoteDetail__c venObj : venList){
            venMap.put(venObj.Id, venObj);
        }
        
        venHdlrDtls.beforeInsert(venList);
        venHdlrDtls.afterInsert(venList, venMap);
        venHdlrDtls.beforeUpdate(venList, venMap, venMap);
        venHdlrDtls.afterUpdate(venList, venMap, venMap);
        venHdlrDtls.beforeDelete(venMap);
        venHdlrDtls.afterDelete(venMap);
       // venHdlrDtls.afterUndelete(venMap);
        
        System.assertEquals(venList.get(0).Proposed_Quote__c, true);
        
        Test.stopTest(); 
    }
}