@istest
Public class Vendorquotecurrency_Test{
    @istest
    Public static void vendortmethod(){
        CurrencyValue__c Currencyobj = new CurrencyValue__c();
        Currencyobj.currencyisocode= 'USD';
        Currencyobj.name = 'USD';
        currencyobj.Currency_Value__c = 1;
        insert Currencyobj;
        System.assert(Currencyobj.id!= null);
        
        Supplier__c s = new Supplier__c();
        s.CurrencyIsoCode = 'USD';
        s.name = 'Airtel';
        s.Approved__c= true;
        s.Services__c = 'GCPE';
        insert s;
        System.assert(s.id!= null); 
        
        List<cscfga__Product_Basket__c> pblist =  P2A_TestFactoryCls.getProductBasket(1);  
        case c = new case();
        c.Status ='Unassigned';
        c.product_basket__c=pblist[0].id;        
        insert c;
        System.assert(c.id!= null);
        
        list<VendorQuoteDetail__c> caseVendorDetailsList  = new list<VendorQuoteDetail__c>();
        VendorQuoteDetail__c vqobj = new VendorQuoteDetail__c();
        vqobj.currencyisocode= 'USD';
        vqobj.supplier_name__c = s.id;
        vqobj.Name = '2345';
        vqobj.recurring_Cost__c = 20;
        vqobj.Non_Recurring_Cost__c = 50;
        vqobj.Expiration_date__c = system.today();
        vqobj.Interface_Type__c = '2M';
        vqobj.Product_Type__c = 'GCPE';
        vqobj.Winning_Quote__c = true;
        vqobj.Win_Reason__c = 'Single source';
        vqobj.Related_3PQ_Case_Record__c = c.id;
        vqobj.Basket_Exchange_Rate__c = 1;
        caseVendorDetailsList.add(vqobj);
        insert caseVendorDetailsList ;
        System.assert(caseVendorDetailsList[0].id != null);  
        
        List<VendorQuoteDetail__c> caseVendorDetailsList1 = [SELECT Basket_Exchange_Rate__c, CurrencyIsoCode, Expiration_date__c, non_rec_new__c, Non_recurring_Charge__c, Product_Basket__c, Reccharge__c, Recurring_Charge__c, Vendor_Quote_Exchange__c FROM VendorQuoteDetail__c WHERE Related_3PQ_Case_Record__c =: c.id];
        
        
        vendorquotecurrency ven= new  vendorquotecurrency();
        ven.Vendorquocurrency(caseVendorDetailsList1);
        
        
    }
    
}