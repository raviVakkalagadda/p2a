/**
    @author - Accenture
    @date - 12-July-2013
    @version - 1.0
    @description - This class acts as a Controller for "ViewUpdateCCMSWO" and populates the Work Order Details by fetching it from the CCMS.
*/

public class ViewUpdateCCMSWOController {


   /* String objectType;
    String pathInsId;
    String route;
    private String workOrderInsId;
    public GetServiceNowParamDetails.WorkOrder woVO {get; set;}
    public Integer selectedTask {get; set;}
    public Map<Integer,GetServiceNowParamDetails.Task> tasksMap = new Map<Integer,GetServiceNowParamDetails.Task>();

    //task details
    public GetServiceNowParamDetails.Task customTask {get; set;}
    public String Priority {get; set;}
    public String SiteHumID {get; set;}
    public String TaskNumber {get; set;}
    public String ElementStatus {get; set;}
    public String ElementName {get; set;}
    public String ElmComplStatus {get; set;}
    public String JeopardyStatus {get; set;}
    public String AllocatedTime {get; set;}
    public String ActualTime {get; set;}
    public String Description {get; set;}
    public Boolean checkOutFlag {get; set;}
    public Boolean checkinFlag {get; set;}
    public Boolean updateFlag {get; set;}


    public ViewUpdateCCMSWOController() {

        Map<String,String> urlParams = ApexPages.currentPage().getParameters();
        objectType = urlParams.containsKey('objectType') ? urlParams.get('objectType') : null;
        pathInsId = urlParams.containsKey('pathInsId') ? urlParams.get('pathInsId') : null;
        this.route = urlParams.containsKey('route') ? urlParams.get('route') : null;
        this.workOrderInsId = urlParams.containsKey('workOrderInsId') ? urlParams.get('workOrderInsId') : null;
        
        woVO = new GetServiceNowParamDetails.WorkOrder();
        customTask = new GetServiceNowParamDetails.Task();
    }

    public PageReference pageAction() {

        GetServiceNowInventoryDetails.InventoryDetailsServicePortSoapPort port = new GetServiceNowInventoryDetails.InventoryDetailsServicePortSoapPort();
        port.timeout_x = 120000;
        GetServiceNowParamDetails.InvDetailsOutputVO output;

        try {
            output = port.getInventoryDetails(objectType,pathInsId,workOrderInsId,route);
            woVO=output.WorkOrder;
            //System.debug('woVO.TaskList --->'+woVO.TaskList);

            if(woVO.TaskList!=null && !woVO.TaskList.isEmpty())
                for(GetServiceNowParamDetails.Task task:woVO.TaskList) {
                    tasksMap.put(task.Task_Inst_Id,task);
                }
            //System.debug('output--->'+output);
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Occurred: ' + ex.getMessage()));
            return null;
        }
        return null;
    }

    public PageReference openTask() {
        customTask = tasksMap.get(selectedTask);
        Priority = String.valueOf(customTask.Priority);
        PageReference pg = Page.ViewUpdateTaskStatus;
        checkOutFlag = true;
        return pg;
    }

    public PageReference checkIn() {
        callUpdateTask('Check In');
        return null;
    }


    public PageReference updateTask() {
        callUpdateTask('Update');
        return null;
    }


    public PageReference checkout() {
        callUpdateTask('Check Out');
        return null;
    }

    public void callUpdateTask(String action) {
        UpdateTaskService.UpdateTaskServicePortSoapPort port = new UpdateTaskService.UpdateTaskServicePortSoapPort();
        port.timeout_x = 120000;
        try {
            Integer prio = Priority!=null&&Priority.trim()!=''?Integer.valueOf(Priority.trim()):0;
            UpdateTaskParams.WOUpdateTaskOutputVO updateResult = port.updateTask(
                                                                            customTask.TaskName,
                                                                            prio,
                                                                            customTask.TaskOperation.toUpperCase(),
                                                                            customTask.Site_hum_id,
                                                                            customTask.Elm_compl_status,
                                                                            customTask.LockElemonCo,
                                                                            customTask.Jeopardy_status,
                                                                            Description,
                                                                            action,
                                                                            customTask.WoInstId,
                                                                            customTask.Task_Inst_Id,
                                                                            customTask.Status,
                                                                            Userinfo.getLastName().length()>20?Userinfo.getLastName().substring(0,20):Userinfo.getLastName());
            if(updateResult.Status == true){
                if(action == 'Check Out' ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Task is checked Out Successfully'));
                    updateFlag = true;
                    checkOutFlag = false;
                    checkinFlag = false;
                    }
                else if(action == 'Check In' ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Task is Checked In Successfully'));
                    updateFlag = false;
                    checkOutFlag = false;
                    checkinFlag = false;
                    }
                else if(action == 'Update' ){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Task Update is submitted to CCMS'));
                    updateFlag = false;
                    checkOutFlag = false;
                    checkinFlag = true;
                    }
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'Result Status is False'));
            }
        } catch(Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Error Occurred: ' + ex.getMessage()));
        }

    }*/
}