@isTest
global class WebServiceConnectorMock implements HttpCalloutMock {
    global HTTPResponse respond(HTTPRequest req) {
        HTTPResponse response = new HTTPResponse();
        response.setStatusCode(200);

        String body = '';

        if(req.getHeader('SoapAction').contains('service/soap/SubmitOrder')){
            body = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ord="http://www.tibco.com/aff/orderservice">'+
                    '<soap:Body>'+
                        '<ord:SubmitOrderResponse>'+
                            '<ord:orderId>123</ord:orderId>'+
                            '<ord:orderRef>456</ord:orderRef>'+
                        '</ord:SubmitOrderResponse>'+
                    '</soap:Body>'+
                '</soap:Envelope>';
        }
        else if(req.getHeader('SoapAction').contains('service/soap/AmendOrder')){
            body = '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ord="http://www.tibco.com/aff/orderservice">'+
                    '<soap:Body>'+
                        '<ord:AmendOrderResponse>'+
                            '<ord:orderId>123</ord:orderId>'+
                            '<ord:orderRef>456</ord:orderRef>'+
                        '</ord:AmendOrderResponse>'+
                    '</soap:Body>'+
                '</soap:Envelope>';
        }

        response.setBody(body);
        
        return response;  
    }
}