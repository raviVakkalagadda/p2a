//Author: Sanchit
//Created Date - 10/04/2017
//Comments - Test class for wwwTibcoComAffCommontypes because owner did not create one :)

@istest(seealldata=false)
public class WwwTibcoComAffCommontypesTest{
    
    static testMethod void unittest() {
        
        wwwTibcoComAffCommontypes.paginationReplyType wwwTibcoComAff1 = new wwwTibcoComAffCommontypes.paginationReplyType();
        wwwTibcoComAffCommontypes.noteType wwwTibcoComAff2 = new wwwTibcoComAffCommontypes.noteType();
        wwwTibcoComAffCommontypes.ascending_element wwwTibcoComAff3 = new wwwTibcoComAffCommontypes.ascending_element();
        wwwTibcoComAffCommontypes.field_element wwwTibcoComAff4 = new wwwTibcoComAffCommontypes.field_element();
        wwwTibcoComAffCommontypes.sortType  wwwTibcoComAff5 = new wwwTibcoComAffCommontypes.sortType();
        wwwTibcoComAffCommontypes.addressType   wwwTibcoComAff6 = new wwwTibcoComAffCommontypes.addressType();
        wwwTibcoComAffCommontypes.extensionType    wwwTibcoComAff7 = new wwwTibcoComAffCommontypes.extensionType();
        wwwTibcoComAffCommontypes.batchType     wwwTibcoComAff9 = new wwwTibcoComAffCommontypes.batchType();
        wwwTibcoComAffCommontypes.CriteriaType    wwwTibcoComAff10 = new wwwTibcoComAffCommontypes.CriteriaType();
        wwwTibcoComAffCommontypes.paginationRequestType    wwwTibcoComAff11 = new wwwTibcoComAffCommontypes.paginationRequestType();
        wwwTibcoComAffCommontypes.descending_element wwwTibcoComAff12 = new wwwTibcoComAffCommontypes.descending_element();
        system.assertEquals(true,wwwTibcoComAff1!=null);
        system.assertEquals(true,wwwTibcoComAff2!=null);
        system.assertEquals(true,wwwTibcoComAff3!=null);
        system.assertEquals(true,wwwTibcoComAff4!=null);
        system.assertEquals(true,wwwTibcoComAff5!=null);
        system.assertEquals(true,wwwTibcoComAff6!=null);
        system.assertEquals(true,wwwTibcoComAff7!=null);
        system.assertEquals(true,wwwTibcoComAff9!=null);
        system.assertEquals(true,wwwTibcoComAff10!=null);
        system.assertEquals(true,wwwTibcoComAff11!=null);
        system.assertEquals(true,wwwTibcoComAff12!=null);

    }
}