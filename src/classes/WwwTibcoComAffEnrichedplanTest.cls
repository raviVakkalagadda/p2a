//Author: Sanchit
//Created Date - 11/04/2017
//Comments - Test class for wwwTibcoComAffEnrichedplan because owner forgot to create one :)

@istest(seealldata=false)
public class WwwTibcoComAffEnrichedplanTest{
    
    static testMethod void unittest() {
        
        wwwTibcoComAffEnrichedplan.PlanSdfNotification_element wwwTibcoComAffEnriched1 = new wwwTibcoComAffEnrichedplan.PlanSdfNotification_element();
        wwwTibcoComAffEnrichedplan.enrichedPlanSchedule_element wwwTibcoComAffEnriched2= new wwwTibcoComAffEnrichedplan.enrichedPlanSchedule_element();
        wwwTibcoComAffEnrichedplan.section wwwTibcoComAffEnriched3= new wwwTibcoComAffEnrichedplan.section();
        wwwTibcoComAffEnrichedplan.milestoneSchedule wwwTibcoComAffEnriched4= new wwwTibcoComAffEnrichedplan.milestoneSchedule();
        wwwTibcoComAffEnrichedplan.planItemSchedule wwwTibcoComAffEnriched5= new wwwTibcoComAffEnrichedplan.planItemSchedule();
        wwwTibcoComAffEnrichedplan.enrichedPlan_element wwwTibcoComAffEnriched6= new wwwTibcoComAffEnrichedplan.enrichedPlan_element();
        wwwTibcoComAffEnrichedplan.jeopardyHeaderType wwwTibcoComAffEnriched7= new wwwTibcoComAffEnrichedplan.jeopardyHeaderType();
        wwwTibcoComAffEnrichedplan.predictedEndType wwwTibcoComAffEnriched8= new wwwTibcoComAffEnrichedplan.predictedEndType();
        wwwTibcoComAffEnrichedplan.path wwwTibcoComAffEnriched9= new wwwTibcoComAffEnrichedplan.path();
        wwwTibcoComAffEnrichedplan.planScheduleType wwwTibcoComAffEnriched10= new wwwTibcoComAffEnrichedplan.planScheduleType();
        wwwTibcoComAffEnrichedplan.predictedReleaseType wwwTibcoComAffEnriched12 = new wwwTibcoComAffEnrichedplan.predictedReleaseType();
        wwwTibcoComAffEnrichedplan.sectionSchedule wwwTibcoComAffEnriched13 = new wwwTibcoComAffEnrichedplan.sectionSchedule();
        wwwTibcoComAffEnrichedplan.planItemDateType wwwTibcoComAffEnriched14 = new wwwTibcoComAffEnrichedplan.planItemDateType();
        wwwTibcoComAffEnrichedplan.milestoneDateType  wwwTibcoComAffEnriched15 = new wwwTibcoComAffEnrichedplan.milestoneDateType();
        wwwTibcoComAffEnrichedplan.planItemSection_element wwwTibcoComAffEnriched16 = new wwwTibcoComAffEnrichedplan.planItemSection_element();
        system.assertEquals(true,wwwTibcoComAffEnriched1!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched2!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched3!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched4!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched5!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched6!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched7!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched8!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched9!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched10!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched12!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched13!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched14!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched15!=null);
        system.assertEquals(true,wwwTibcoComAffEnriched16!=null);

        
    }
}