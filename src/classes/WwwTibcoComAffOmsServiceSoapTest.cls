@istest(seealldata=false)
public class WwwTibcoComAffOmsServiceSoapTest{
     
    static testMethod void unitTest() {
    
    string orderId = 'orderId';
    String orderRef = 'orderRef';
    String customerID = 'customerID';
    String subscriberID = 'subscriberID';
    String status = 'status';
    Long count = 178946452;
    Boolean orderSummary = false;
    Boolean rollback_x = false;
    
    Boolean includejeopardyHeader = true;
    Boolean includePlanFragments = false;
    
    String action = 'action';
    String[] orderIDList = new List<string>();
    String[] orderRefList = new List<string>();
    
    Test.startTest();
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SuspendOrderResponse_element());
        wwwTibcoComAffOmsServiceSoap.OrderServicePort Orderservice = new wwwTibcoComAffOmsServiceSoap.OrderServicePort();
        wwwTibcoComAffOrderservice.SuspendOrderResponse_element Suspndrder = Orderservice.SuspendOrder('orderID','orderRef');
        system.assertEquals(false,Suspndrder !=null);
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_elementGenerator());
        wwwTibcoComAffOrderservice.SyncSubmitOrderResponse_element SyncSubmit = Orderservice.SyncSubmitOrder();
        system.assertEquals(true,SyncSubmit !=null);

        Test.setMock(WebServiceMock.class, new MainMockClass.GetOrdersRequest_element());
        wwwTibcoComAffOrderservice.sortCriteria sortCriteria = new wwwTibcoComAffOrderservice.sortCriteria();
        wwwTibcoComAffOrderservice.dateRange_element dateRange = new wwwTibcoComAffOrderservice.dateRange_element();
        wwwTibcoComAffOrderservice.headerUDF_element[] headerUDF = new List<wwwTibcoComAffOrderservice.headerUDF_element>();        
        wwwTibcoComAffOrderservice.orderLineUDF_element[] orderLineUDF = new List<wwwTibcoComAffOrderservice.orderLineUDF_element>(); 
        wwwTibcoComAffOrderservice.pagination_element pagination = new wwwTibcoComAffOrderservice.pagination_element(); 
        wwwTibcoComAffOrderservice.GetOrdersResponse_element GetOrdrs  = Orderservice.GetOrders(sortCriteria,orderId,orderRef,customerID,subscriberID,dateRange,status,headerUDF,orderLineUDF,count,pagination,orderSummary);
        
        
        Test.setMock(WebServiceMock.class, new MainMockClass.PerformBulkOrderActionRequest_element());
        wwwTibcoComAffOrderservice.PerformBulkOrderActionResponse_element PerformBulkActResponse = Orderservice.PerformBulkOrderAction(action,orderIDList,orderRefList); 
        system.assertEquals(false,PerformBulkActResponse!=null);
        
       /* Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_GetOrderDetails());
        wwwTibcoComAffOrder.orderType ordertype = Orderservice.GetOrderDetails();
        wwwTibcoComAffOrder.orderType ordertype1 = */
        
        Orderservice.AmendOrder(); 
        Orderservice.SubmitOrder();
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_CancelOrderResponse_element());
        wwwTibcoComAffOrderservice.CancelOrderResponse_element CancelOrderResponse = Orderservice.CancelOrder(orderID,orderRef,rollback_x);
        
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_GetOrderExecutionPlan());
        
      /*  Test.setMock(WebServiceMock.class, new MainMockClass.GetOrderExecutionPlanResponse_element());
        wwwTibcoComAffPlan.planType Plantype = Orderservice.GetOrderExecutionPlan(orderID,orderRef);
        system.assertEquals(true,Plantype!=null); */
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_WithdrawOrderResponse_element());
        wwwTibcoComAffOrderservice.WithdrawOrderResponse_element  WithdrwaResponse = Orderservice.WithdrawOrder(orderID,orderRef);
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_GetEnrichedExecutionPlanResponse_element());
        wwwTibcoComAffOrderservice.GetEnrichedExecutionPlanResponse_element GetEnrichmnetPlan = Orderservice.GetEnrichedExecutionPlan(orderID,orderRef,includePlanFragments,includejeopardyHeader);
        
        
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_ActivateOrderResponse_element());
        wwwTibcoComAffOrderservice.ActivateOrderResponse_element ActivateResponseElement = Orderservice.ActivateOrder(orderID,orderRef); 
           
        
    Test.stopTest(); 
    
    
   }
   @istest
   public static void GetOrderDetailstest()
   {
        string orderId = 'orderId ';
        string orderRef = 'orderRef ';
        wwwTibcoComAffOmsServiceSoap.OrderServicePort Orderservice1 = new wwwTibcoComAffOmsServiceSoap.OrderServicePort();
        Test.starttest();
        try{
        Test.setMock(WebServiceMock.class, new MainMockClass.SyncSubmitOrderResponse_GetOrderDetails());
        wwwTibcoComAffOrder.orderType ordertype = Orderservice1.GetOrderDetails(orderId,orderRef );
        }catch(Exception e){}
        Test.stoptest();
   }
   @istest
   public static void GetOrderDetailstest1()
   {
        string orderId = 'orderId ';
        string orderRef = 'orderRef ';
        wwwTibcoComAffOmsServiceSoap.OrderServicePort Orderservice1 = new wwwTibcoComAffOmsServiceSoap.OrderServicePort();
        Test.starttest();
        try{
        Test.setMock(WebServiceMock.class, new MainMockClass.GetOrderExecutionPlanResponse_element());
         wwwTibcoComAffPlan.planType Plantype = Orderservice1.GetOrderExecutionPlan(orderID,orderRef);
        }catch(Exception e){}
        Test.stoptest();
   }
    
 }