//Generated by wsdl2apex

public class WwwTibcoComBillingactivate {
    public class BillingActivatePortTypeEndpoint1 {
        public String endpoint_x = 'http://localhost:-1/Service.serviceagent/BillingActivateEndpoint';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://www.tibco.com/BillingActivate', 'wwwTibcoComBillingactivate', 'http://www.tibco.com/schemas/XSDGeneration/BillingActivateResponse.xsd', 'wwwTibcoComSchemasXsdgenerationBill', 'http://www.tibco.com/schemas/XSDGeneration/BillingActivateRequest.xsd', 'wwwTibcoComSchemasXsdgenerationBill'};
        public wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element BillingActivateOperation(wwwTibcoComSchemasXsdgenerationBill.Service_element[] Service) {
            wwwTibcoComSchemasXsdgenerationBill.Subscription_element request_x = new wwwTibcoComSchemasXsdgenerationBill.Subscription_element();
            request_x.Service = Service;
            wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element response_x;
            Map<String, wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element> response_map_x = new Map<String, wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
              this,
              request_x,
              response_map_x,
              new String[]{endpoint_x,
              'BillingActivate',
              'http://www.tibco.com/schemas/XSDGeneration/BillingActivateRequest.xsd',
              'Subscription',
              'http://www.tibco.com/schemas/XSDGeneration/BillingActivateResponse.xsd',
              'BillingActivateResponse',
              'wwwTibcoComSchemasXsdgenerationBill.BillingActivateResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}