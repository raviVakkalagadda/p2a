global class ZEndPOPLookup extends cscfga.ALookupSearch {
    public override Object[] doDynamicLookupSearch(Map<String, String> searchFields, String productDefinitionID){
        // NOT IMPLEMENTED
        System.Debug('doDynamicLookupSearch');
        List <Additional_Data__c> data = [SELECT Name, Type__c FROM Additional_Data__c WHERE Type__c = 'Country'];
        return data;
    }
    public override String getRequiredAttributes(){ 
        return '[ "A-End Country", "Z-End Country", "A-End POP/CLS 1" ]';
    }
    public override Object[] doLookupSearch(Map<String, String> searchFields, String productDefinitionId, Id[] excludeIds,
        Integer pageOffset, Integer pageLimit){
            try {
            List <String> POPNames = new List<String>();
            String Aend_Country = searchFields.get('A-End Country');
                String Zend_Country = searchFields.get('Z-End Country');
                String Aend_POP = searchFields.get('A-End POP/CLS 1');
            System.Debug('Search field = ' + Aend_Country + ', ' + Zend_Country + ', ' + Aend_POP);
            String query = 'SELECT Z_End_POP_Name__c FROM IPL_Rate_Card_Item__c';
            if (Aend_Country != null)
            {
                query = query + ' WHERE Aend_Country__c = :Aend_Country AND Zend_Country__c = :Zend_Country AND Aend_POP__c = :Aend_POP';
            }
            System.Debug('query = '+query);
            for (IPL_Rate_Card_Item__c rateCard : Database.query(query))
            {
                POPNames.add(rateCard.Z_End_POP_Name__c);
            }
            System.Debug('POPNames = '+POPNames);
            String searchValue = searchFields.get('searchValue') +'%';
         List <CloudSense_POP__c> data = [SELECT Name, Address__c, City__c FROM CloudSense_POP__c WHERE Name in :POPNames AND Name LIKE :searchValue ORDER BY Name];
            System.Debug('data = ' + data);
       return data;
            }
            catch (Exception e)
            {
                System.debug('Exception: ' + e);
                return null;
            }
    }

}