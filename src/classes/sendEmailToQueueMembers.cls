public class sendEmailToQueueMembers implements Database.Batchable<SObject>, Database.Stateful{
    
    public final Id groupOwnerId;
    public final Case caseRecord;
    public final String Query;    
    
    public sendEmailToQueueMembers(Id groupOwnerId, Case caseRecord){
        this.caseRecord = caseRecord;
        this.groupOwnerId = groupOwnerId;
        Query = 'Select UserOrGroupId From GroupMember Where GroupId =:groupOwnerId';
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc){

        /** Return the iterable data, that will be processed by the steps of the batch.*/
        return Database.getQueryLocator(Query);
    }
    
    public void execute(Database.BatchableContext bc, List<GroupMember> GroupMemberList){
        for(GroupMember userId :GroupMemberList){
            User activeUser = null;
            if(userId.UserOrGroupId != null){activeUser = [Select Id, isActive From User Where Id =:userId.UserOrGroupId];}
            if(activeUser != null && activeUser.isActive == true && caseRecord.Order__c != null){
                sendEmail(caseRecord, activeUser.Id);
            }
        }
    }
    
    public static void sendEmail(Case caseRecord, Id activeUserId){
        csord__Order__c order = [Select Id, csord__Order_Number__c From csord__Order__c Where Id =:caseRecord.Order__c];
        Messaging.SingleEmailMessage mailToOrderOwner = new Messaging.SingleEmailMessage();
        mailToOrderOwner.setTargetObjectId(activeUserId);
        mailToOrderOwner.setSaveAsActivity(false);
        mailToOrderOwner.setSubject('ETC Waiver Request: ' +caseRecord.CaseNumber +' for ' +caseRecord.Service_Name__c +' has been ' +caseRecord.Status+' for ' +order.csord__Order_Number__c);

        /** Setting the email body -- initial information **/
        String mailBody='Hi,'
        +'<br/>'
        +'<br/>'
        +'A Request to waive the ETC for Service ' +caseRecord.Service_Name__c +' has been ' +caseRecord.Status+' by the Commercial & Billing Team.';
        
        //Removing this scenaio as per request by Kylie
        //Developer Sumit Suman
        /*if(caseRecord.Waiver_Request_Status__c != 'Fully waived'){
            mailBody = mailBody +' You can raise another ETC waiver request with specific/additional comments if you still want the ETC to be waived.';
        }
        */
        mailBody = mailBody
        +'<br/>'
        +'<br/>'
        +'Account: ' +caseRecord.Account.Name
        +'<br/>'
        +'Service: ' +caseRecord.Service_Name__c
        +'<br/>'
        +'Requested Termination Date: ' +caseRecord.Requested_Termination_Date__c.format()
        +'<br/>'
        +'Order Number: '
        +'<td class="tg-031e"><a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+order.Id+'>'+order.csord__Order_Number__c+'</a></td>'
        +'<br/>'
        +'Contract Expiry Date: ' +caseRecord.Contract_Expiry_Date__c.format()
        +'<br/>'
        +'<br/>'
        +'Please click on the link below to view the details of the Request.'
        +'<br/>'
        +'<td class="tg-031e"><a href='+URL.getSalesforceBaseUrl().toExternalForm()+'/'+caseRecord.Id+'>'+caseRecord.CaseNumber+'</a></td>'
        +'<br/>'
        +'<br/>'
        +'<br/>'
        +'Please ignore the email if the activity has already been completed.'
        +'<br/>'
        +'<br/>'
        +'Thanks!'
        +'<br/>'
        +'<br/>'
        +'<br/>'
        +'***This is an auto-generated notification, please do not reply to this email***';
        mailToOrderOwner.setHtmlBody(mailBody);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mailToOrderOwner});
    }
             
    public void finish(Database.BatchableContext bc){

    }
}