<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <fields>
        <fullName>City_Name_Only__c</fullName>
        <externalId>false</externalId>
        <formula>City_Finder__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>City Name Only</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Country_Finder__c</fullName>
        <deleteConstraint>Restrict</deleteConstraint>
        <externalId>false</externalId>
        <label>Country</label>
        <referenceTo>Country_Lookup__c</referenceTo>
        <relationshipLabel>Sites</relationshipLabel>
        <relationshipName>Sites</relationshipName>
        <required>true</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Full_Address__c</fullName>
        <externalId>false</externalId>
        <formula>Address1__c + 
IF(ISBLANK(Address1__c) &amp;&amp; !ISBLANK(Address2__c), 
   Address2__c, 
   IF(!ISBLANK(Address1__c) &amp;&amp; !ISBLANK(Address2__c), 
      &apos;, &apos; + Address2__c, 
      &apos;&apos;
   ) + 
   IF((!ISBLANK(Address1__c) || !ISBLANK(Address2__c)) &amp;&amp; !ISBLANK(Address_3__c), 
      &apos;, &apos; + Address_3__c, 
      &apos;&apos;
   ) +
   IF((!ISBLANK(Address1__c) || !ISBLANK(Address2__c) || !ISBLANK(Address_3__c)) &amp;&amp; !ISBLANK(Address_4__c), 
      &apos;, &apos; + Address_4__c, 
      &apos;&apos;
   )
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Full Address</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Generic_Site_Code__c</fullName>
        <externalId>false</externalId>
        <formula>City_Finder__r.Generic_Site_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Generic Site Code</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
	<validationRules>
        <fullName>Zip_Code_mandatory_for_US_Sites</fullName>
        <active>true</active>
        <description>This validation rule to make sure that ZIp code is populated for the sites having country as US , this is to make sure that there is IP006 failures due to missing zip code</description>
        <errorConditionFormula>AND(Country_Formula__c = &apos;US&apos;, ISBLANK(PostalCode__c), ISPICKVAL(Address_Type__c, &apos;Billing Address&apos;))</errorConditionFormula>
        <errorMessage>Postal/Zip code is mandatory for US sites. Please enter a Postal code.</errorMessage>
    </validationRules>
	 <validationRules>
        <fullName>Restriction_to_Billing_SME</fullName>
        <active>true</active>
        <description>Billing SME is restricted to create the Site record with address type as &quot;Site Address&quot;.</description>
        <errorConditionFormula>And($User.Profile_Name__c  = &apos;TI Billing&apos;,  ISPICKVAL(Address_Type__c, &apos;Site Address&apos;))</errorConditionFormula>
        <errorDisplayField>Address_Type__c</errorDisplayField>
        <errorMessage>Only Sales/Non-Billing users are allowed to create provisioning sites</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Restriction_to_Sales_User</fullName>
        <active>false</active>
        <description>Sales user will be restricted to create the site with Billing Address.</description>
        <errorConditionFormula>And(OR($User.Profile_Name__c = &apos;TI Account Manager&apos;, $User.Profile_Name__c = &apos;TI Sales Admin&apos;,$User.Profile_Name__c = &apos;TI Sales Manager&apos;),ISPICKVAL(Address_Type__c, &apos;Billing Address&apos;))</errorConditionFormula>
        <errorDisplayField>Address_Type__c</errorDisplayField>
        <errorMessage>Only Billing users are allowed to create billing sites</errorMessage>
    </validationRules>
    <validationRules>
        <fullName>Validate_for_Other_City</fullName>
        <active>true</active>
        <description>If City lookup is populated with OTHER, then other city text field should have a value populated.</description>
        <errorConditionFormula>IF(City_Finder__r.Name == &quot;OTHER&quot; &amp;&amp; Other_City__c == &quot;&quot;, true, false)</errorConditionFormula>
        <errorDisplayField>Other_City__c</errorDisplayField>
        <errorMessage>You have selected the city lookup as &quot;OTHER&quot;, so please make sure that other city name is populated in the &quot;Other City&quot; textbox below the city lookup.</errorMessage>
    </validationRules>
    <listViews>
        <fullName>All</fullName>
        <columns>OBJECT_ID</columns>
        <columns>NAME</columns>
        <columns>Site_Code__c</columns>
        <columns>External_Id__c</columns>
        <columns>Country_name__c</columns>
        <columns>City_Code__c</columns>
        <columns>AccountId__c</columns>
        <columns>Account_ID__c</columns>
        <columns>State__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
</CustomObject>
