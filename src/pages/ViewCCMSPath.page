<apex:page controller="ViewCCMSPathController" sidebar="false" showHeader="false">    

  <!--page messages-->
  <apex:pageMessages id="msg"></apex:pageMessages>  

  <!--status-->
  <apex:outputPanel >
        <apex:actionStatus id="stat">
            <apex:facet name="start">
                <div class="waitingSearchDiv" id="el_loading" style="background-color: #fbfbfb;
                       height: 150%;opacity:0.65;width:100%;"> 
                    <div class="waitingHolder" style="top: 74.2px; width: 91px;">
                        <img class="waitingImage" src="/img/loading.gif" title="Please Wait..." />
                        <span class="waitingDescription">Please wait...</span>
                    </div>
                </div>
            </apex:facet>
        </apex:actionStatus>
    </apex:outputPanel>

  <!--action function called on window load-->
  <apex:form >
    <apex:actionFunction name="pageAction" action="{!pageAction}" status="stat" reRender="pd,msg"/>
  </apex:form>
  <!--page block start-->
  <apex:pageBlock title="Path Details" id="pd"> 

    <!--View Path Section start-->
    <apex:pageBlockSection title="View Path">
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="ID"/>
        <apex:outputText value="{!pathVO.CircPathHumId}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Rev."/>
        <apex:outputText value="{!pathVO.PathRevNbr}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Category"/>
        <apex:outputText value="{!pathVO.Type_x}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Bandwidth"/>
        <apex:outputText value="{!pathVO.Bandwidth}"/>
      </apex:pageBlockSectionItem>
    
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="# of Members"/>
        <apex:outputText value="{!pathVO.NbrMembers}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Topology"/>
        <apex:outputText value="{!pathVO.Topology}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Member BW"/>
        <apex:outputText value="{!pathVO.MemberBw}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Status"/>
        <apex:outputText value="{!pathVO.Status}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Role"/>
        <apex:outputText value="{!pathVO.Role}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Direction"/>
        <apex:outputText value="{!pathVO.Direction}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="A-Side Site"/>
        <apex:outputText value="{!pathVO.aSideSiteName}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Z-Side Site"/>
        <apex:outputText value="{!pathVO.zSideSiteId}"/>
      </apex:pageBlockSectionItem>

    </apex:pageBlockSection>
    <!--View Path Section end-->    


    <!--Proviosing Details Section start-->
    <apex:pageBlockSection title="Provisioning Details">
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Purchase Order Number"/>
        <apex:outputText value="{!pathVO.PurchaseOrderNumber}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Supplier Commitment Date"/>
        <apex:outputText value="{!pathVO.SupplierCommitmentDate}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Purchase Requisition Number"/>
        <apex:outputText value="{!pathVO.PurchaseReqNumbers}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Actual Supply Date"/>
        <apex:outputText value="{!pathVO.ActualSupplyDate}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Order Placed With Supplier"/>
        <apex:outputText value="{!pathVO.OrderPlacedWithSupplier}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Supplier Termination Date"/>
        <apex:outputText value="{!pathVO.SupplierTermination}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Supplier Order Accepted Date"/>
        <apex:outputText value="{!pathVO.SupplierOrderAccepted}"/>
      </apex:pageBlockSectionItem>    

    </apex:pageBlockSection>
    <!--Proviosing Details Section end-->


    <!--IP Service Common Section start-->
    <apex:pageBlockSection title="IP Service Common">
          
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Management Bandwidth"/>
        <apex:outputText value="{!pathVO.ManagementBw}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Customer AS Number"/>
        <apex:outputText value="{!pathVO.CustomerASNumber}"/>
      </apex:pageBlockSectionItem>

    </apex:pageBlockSection>
    <!--IP Service Common Section end-->    


    <!--Customer Information start. chnage this section name after confirmation-->
    <apex:pageBlockSection title="Customer Information">
          
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Ordering Customer"/>
        <apex:outputText value="{!pathVO.CustomerId}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="A-Side Customer"/>
        <apex:outputText value="{!pathVO.aSideCustomer}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Z-Side Customer"/>
        <apex:outputText value="{!pathVO.zSideCustomer}"/>
      </apex:pageBlockSectionItem>

    </apex:pageBlockSection>
    <!--Placeholder section end-->
    

    <!--Containers/Timeslots section start-->
    <apex:pageBlockSection title="Containers/Timeslots">
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Total BPS"/>
        <apex:outputText value="{!pathVO.BpsAvailable}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="BPS Allocated"/>
        <apex:outputText value="{!pathVO.ChanWarnThreshhold}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Utilization"/>
        <apex:outputText value="{!pathVO.PctUtil}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Start Channel Range"/>
        <apex:outputText value="{!pathVO.StartChanNbr}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="End Channel Range"/>
        <apex:outputText value="{!pathVO.EndChanNbr}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Assignments"/>
        <apex:outputText value="{!pathVO.VirtualChans}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Assigned"/>
        <apex:outputText value="{!pathVO.NbrChanAssigned}"/>
      </apex:pageBlockSectionItem>

      <!--update with the mapping variable-->
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Available"/>
        <apex:outputText value=""/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Warning Threshold"/>
        <apex:outputText value="{!pathVO.WarnThreshPct}"/>
      </apex:pageBlockSectionItem>
      
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Over Subscription"/>
        <apex:outputText value="{!pathVO.MaxPctOversubscription}"/>
      </apex:pageBlockSectionItem>

    </apex:pageBlockSection>
    <!--Containers/Timeslots section end-->
    

    <!--Dates section start-->
    <apex:pageBlockSection title="Dates">
    
      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Order No."/>
        <apex:outputText value="{!pathVO.OrderNum}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="In Service"/>
        <apex:outputText value="{!pathVO.InService}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Ordered"/>
        <apex:outputText value="{!pathVO.Ordered}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Scheduled"/>
        <apex:outputText value="{!pathVO.ShcedDate}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Due"/>
        <apex:outputText value="{!pathVO.Due}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Decommision"/>
        <apex:outputText value="{!pathVO.Decommision}"/>
      </apex:pageBlockSectionItem>

      <apex:pageBlockSectionItem >
        <apex:outputlabel value="Installed"/>
        <apex:outputText value="{!pathVO.Installed}"/>
      </apex:pageBlockSectionItem>

    </apex:pageBlockSection>
    <!--Dates section end-->    

  </apex:pageBlock>
  <!--page block end-->

  <script type="text/javascript">
      function load() {
        pageAction();
      }
      window.onload=load;
  </script>

</apex:page>