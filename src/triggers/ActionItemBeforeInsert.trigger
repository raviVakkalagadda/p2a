trigger ActionItemBeforeInsert on Action_Item__c (before Update, before insert) {
/*
 if(Util.muteAllTriggers()) return;
 Map<ID,ID> ActionOppMapping = new Map<ID,ID>();
       
        Map<Id,String> OppMap = new Map<Id,String>();
        Map<String,String> rtMap = new Map<String, String>();       
        Map<ID,Opportunity> objOppMap = new Map<ID,Opportunity>();
        Set<ID> OppIds = new Set<ID>();
        
        Opportunity objOpportunity; 
         if(label.MuteSPT != 'TRUE'){
    for(Action_Item__c ObjActionItem : (List<Action_Item__c>)trigger.new){
            
            ActionOppMapping.put(ObjActionItem.Id,ObjActionItem.Opportunity__c);
           
        }
        
        
        for(Opportunity opp : [select Id,Name from Opportunity where Id IN : ActionOppMapping.Values()])
        {
             OppMap.put(opp.Id,opp.Name);            
        }
        rtMap = Util.getRecordTypeMap('Action_Item__c');
        
        for(Action_Item__c ObjActionItem : (List<Action_Item__c>)trigger.new){
            if(rtMap.get('Request to Modify Opportunity Split') == ObjActionItem.RecordTypeId && ObjActionItem.Comments__c!=''){
            List<User> usrLst =[Select Name, Region__c,Role_Name__c from User where id= :UserInfo.getUserId()];
            User usr = usrLst[0];
             Util.assignOwnersToRegionalQueue(usr,objActionItem, 'Request to Modify Opportunity Split');
             
             
             
             }
             
            
        }
        
        //Changes as part of Opportunity Split Modification AI enhancement -- Bhargav
        system.debug('entered trigger ai before insert -- Bhargav');
        for(Action_Item__c ObjActionItem : (List<Action_Item__c>)trigger.new){
            if(ObjActionItem.Subject__c=='Request to Modify Opportunity Split' && ObjActionItem.Modification_of_existing_Split__c && ObjActionItem.Opportunity_Split_Owner__c!=null){
                Oppids.add(ObjActionItem.Opportunity__c);
            }
        }
        
        if(Oppids.size()>0){
            List<OpportunitySplit> oslist = [select id, SplitOwnerID, SplitPercentage, SplitPercentage_Product_SOV__c from OpportunitySplit where OpportunityID IN : Oppids ];
            for(Action_Item__c AIObj : (List<Action_Item__c>)trigger.new){
                for(OpportunitySplit o : oslist){
                    if(o.SplitOwnerID == AIObj.Opportunity_Split_Owner__c){
                        AIObj.Reported_Total_SOV_Perc__c = o.SplitPercentage;
                        AIObj.Product_SOV_Perc__c = o.SplitPercentage_Product_SOV__c;
                    }
                }
            }
         }}
    */
}