/**
    @author - Accenture
    @date - 30-June-2012
    @version - 1.0
    @description - This trigger is used to Aggregate the MRC, NRC Fields Specific on Opportunity
*/
trigger AfterInsertUpdatePopulateRecurring on Quote__c (after insert, after update) {
/*
  if(Util.muteAllTriggers()) return;
// First find the Corresponding Quotes Specific Opportunity
    Set<Id> oppIds = new set<Id>();
    Map<Id,Quote__c> quoteMap= new Map<Id,Quote__c>();
try{    
 for (Quote__c quote: Trigger.new){
    if(quote.Primary__c == true){
        oppIds.add(quote.Opportunity__c);   
        quoteMap.put(quote.Opportunity__c,quote);
    }
 }
 
 List<Opportunity> oppList = [Select Id,Total_MRC__c, Total_NRC__c,Total_Contract_Value__c,ContractTerm__c from Opportunity where Id in :oppIds];
 List<Opportunity> opptoUpdate = new List<Opportunity>();
 
  for (opportunity opp : oppList){
        if (quoteMap.containsKey(opp.Id)){
            opp.Total_MRC__c = quoteMap.get(opp.Id).Total_NetMRC_Price__c;
            opp.Total_NRC__c = quoteMap.get(opp.Id).TotalNetPrice__c;
            opp.Total_Contract_Value__c = quoteMap.get(opp.Id).Total_NetMRC_Price__c * integer.ValueOf(opp.ContractTerm__c) +quoteMap.get(opp.Id).TotalNetPrice__c;
            //opp.Total_Contract_Value__c = opp.Total_MRC__c+opp.Total_NRC__c;
            opptoUpdate.add(opp);
        }
  }
 if (opptoUpdate.size()>0)  
    update opptoUpdate;
    
}catch(Exception e){
    
}
*/                
}