trigger AllOrderTriggers on csord__Order__c (before insert, before update, before delete, after update, after delete, after undelete){
        TriggerDispatcher.run(new AllOrderTriggersHandler());
        
        if (Trigger.isBefore && Trigger.isUpdate){
            new AllOrderTriggersHandler().setCustomerRequiredDate((List<csord__Order__c>)Trigger.New);
        }
        
        if (Trigger.isAfter && Trigger.isUpdate){
            AllOrderTriggersHandler.updateOrderOrchestrationProcess((Map<Id, csord__Order__c>)Trigger.newMap, (Map<Id, csord__Order__c>)Trigger.oldMap);
        }
}