trigger AllProductBasketTrigger on cscfga__Product_Basket__c (before insert, before update, before delete, after insert, after update, after delete){
            TriggerDispatcher.run(new AllProductBasketTriggerHandler());
}