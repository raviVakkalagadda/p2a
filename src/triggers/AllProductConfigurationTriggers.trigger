trigger AllProductConfigurationTriggers on cscfga__Product_Configuration__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {

    public static Boolean muteAllTriggers(){        
        try {
            if(Global_Mute__c.getInstance(UserInfo.getUserId()) != null){
                return Global_Mute__c.getInstance(UserInfo.getUserId()).Mute_Triggers__c;
            } else {
                return Global_Mute__c.getOrgDefaults().Mute_Triggers__c;
            }
            
        } catch (Exception e){
            return false;
          }
    }

    if(Trigger.isAfter && Trigger.isUpdate){
        updateReationshipInMACD((Map<Id, cscfga__Product_Configuration__c>)Trigger.newMap, (Map<Id, cscfga__Product_Configuration__c>)Trigger.oldMap);
        if(!TriggerContext.IsMethodAlreadyExecuted('RunProductConfigurationEVPPorts')){AddEVPLPort.EVPPorts(Trigger.new);}
    } 
    
    if(Trigger.isBefore && Trigger.new!=null){
        ProductConfigurationSequence.updateParentConfigForSequence(Trigger.new);      
    }
    
    if(Trigger.isBefore && Trigger.isUpdate) {       
        ScreenFlowUtilities.updateScreenFlowField(Trigger.newMap.values());
    }

    TriggerDispatcher.run(new AllProductConfigurationTriggerHandler());
    
    private static void updateReationshipInMACD(Map<Id, cscfga__Product_Configuration__c> newPCmap,Map<Id, cscfga__Product_Configuration__c> oldPCmap){
        Set<Id> pcIds = new Set<Id>();
        for(cscfga__Product_Configuration__c pc :newPCmap.values()){
            system.debug('===== oldPCmap: ' + oldPCmap.get(pc.Id).csordtelcoa__Replaced_Product_Configuration__c + ' newPC: ' + pc.csordtelcoa__Replaced_Product_Configuration__c);
            if((oldPCmap.get(pc.Id).csordtelcoa__Replaced_Product_Configuration__c != pc.csordtelcoa__Replaced_Product_Configuration__c) && (pc.csordtelcoa__Replaced_Product_Configuration__c != null)){
                pcIds.add(pc.Id);
            }
        }   
        system.debug('pcIds size: ' + pcIds.size());
        if(!pcIds.isEmpty()){
            MACDUtilities.LinkProductConfigurationsInMACD(pcIds);
        }
    }
}