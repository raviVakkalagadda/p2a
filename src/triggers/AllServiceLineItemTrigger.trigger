trigger AllServiceLineItemTrigger on csord__Service_Line_Item__c (before insert, before update, before delete, after insert, after update, after delete, after undelete){
		TriggerDispatcher.run(new AllServiceLineItemTriggerHandler());
}