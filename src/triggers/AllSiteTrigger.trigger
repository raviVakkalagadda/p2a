trigger AllSiteTrigger on Site__c (before insert, before update, before delete,after update, after delete, after undelete) {
        TriggerDispatcher.run(new AllSiteTriggerHandler());
}