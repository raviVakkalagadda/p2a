/**
    @author - Accenture/Sanjeev
    @date - 04-Oct-2017
    @version - 1.0
    @description - This trigger change the record type to closed when status is 
                   Completed. (This Trigger is a replacement of changeRecordTypeToClosed and setAssociatedGroupManual Trigger.
*/

trigger AllTaskTrigger on Task (before insert, before update, before delete,after update, after delete, after undelete) {
TriggerDispatcher.run(new AllTaskTriggerHandler());
}