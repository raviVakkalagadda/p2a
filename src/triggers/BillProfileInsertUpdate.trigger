/**
    @author - Accenture
    @date - 13-FEB-2013
    @version - 1.0
    @description - This trigger is used to check duplicate BillProfile Numbers.
*/
trigger BillProfileInsertUpdate on BillProfile__c (before Insert, before update) {
	/*
 if(Util.muteAllTriggers()) return;
//CR1018:Suparna
for(BillProfile__c bill : Trigger.new){
   if(bill.Display_Data_Itemisation__c==false && bill.Itemisation_Display_Level__c==null){
   bill.Itemisation_Display_Level__c=0;
   
   }
  
  }
//CR1018:Suparna
//Initializing Map to collect  bill profiles
 Map<String, BillProfile__c> BillMap = new Map<String, BillProfile__c>();
 Map<String,BillProfile__c> accMap = new Map<String,BillProfile__c>();
 //Iterating through all the newly created bill profiles
   RecordType rt = [select Id,Name from RecordType where Name='ISO Bill Profile RT']; 
    for (BillProfile__c bill : System.Trigger.new) { 
              accMap.put(bill.Account__c,bill); 
       }
       List<Account> acc = [select Id,Name,Customer_Type__c,OwnerId from Account where Id in :accMap.keyset()];
    for (BillProfile__c bill : System.Trigger.new) { 
        if ((bill.Bill_Profile_ORIG_ID_DM__c != null) &&
                (System.Trigger.isInsert ||
                (bill.Bill_Profile_ORIG_ID_DM__c != 
                    System.Trigger.oldMap.get(bill.Id).Bill_Profile_ORIG_ID_DM__c))) {
            //To check,if new bill profiles have duplicates.  
            if (BillMap.containsKey(bill.Bill_Profile_ORIG_ID_DM__c)) {
                bill.Bill_Profile_ORIG_ID_DM__c.addError('Duplicate value of bill profile number are present within this new Bill Profile list.');                      
            } else {
                BillMap.put(bill.Bill_Profile_ORIG_ID_DM__c, bill);
            }
       }
       for(Account ac : acc){
        if (System.Trigger.isInsert && bill.RecordTypeId == rt.id && ac.Customer_Type__c == 'ISO'){
        
            bill.Status__c = 'Active';
            bill.Activated__c = True;
            bill.Approved__c = True;
            bill.Approved_By__c = ac.ownerId;
            }
        }      
             
    }
   
    
    // Using a single query, to fetch all BillProfiles in the database that have the same bill profile number as any of them being inserted or updated.      
    List<BillProfile__c> bpn= [SELECT Bill_Profile_ORIG_ID_DM__c,E_bill_Email_Billing_Address__c,CDR_Email_Address__c,CDR_Email_Address_CC__c,E_bill_Email_Billing_Address_CC__c FROM BillProfile__c WHERE Bill_Profile_ORIG_ID_DM__c IN :BillMap.KeySet()];
       System.debug('---Before the For Loop ----'+bpn.size());
                     if(bpn.size()>0){
    for (BillProfile__c bill :bpn ) {
       System.debug('---Inside For Loop ----');
        
        BillProfile__c newBill = BillMap.get(bill.Bill_Profile_ORIG_ID_DM__c); 
        System.debug('Sent to Tibco'+newBill.Sent_To_Tibco__c);
        if(BillProfileUpdateCount.getcount()==0)
         {     
        newBill.addError('The bill profile number is already consumed by another bill profile. Could you please try other combination?');
        }
      }
     // System.debug('---End of For Loop ----');
   }
     if(Trigger.isBefore){
    string[] input = new string[]{};
    String PatternStr = '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@' + '[A-Za-z0-9-]+(\\.[A-Za-z0-9-A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$';
    
    for(BillProfile__c BP:trigger.new){
         If(BP.E_bill_Email_Billing_Address_CC__c!=null){        
        input = BP.E_bill_Email_Billing_Address_CC__c.split('\\,');
 //Validating email address format      
        for(integer i=0; i<input.size();i++){
        if(!(pattern.Matches(PatternStr,input[i]))){
            BP.E_bill_Email_Billing_Address_CC__c.addError('Email addresses entered in this field are not valid');
            }
            }
        }       
        If(BP.CDR_Email_Address__c!=null){
        input = BP.CDR_Email_Address__c.split('\\,');
       
        for(integer i=0; i<input.size();i++){
        if(!(pattern.Matches(PatternStr,input[i]))){
            BP.CDR_Email_Address__c.addError('Email addresses entered in this field are not valid');
        }
        }
      } 
      If(BP.CDR_Email_Address_CC__c!=null){
        input = BP.CDR_Email_Address_CC__c.split('\\,');
       
        for(integer i=0; i<input.size();i++){
        if(!(pattern.Matches(PatternStr,input[i]))){
            BP.CDR_Email_Address_CC__c.addError('Email addresses entered in this field are not valid');
        }
        }
      } 
      If(BP.E_bill_Email_Billing_Address__c!=null){
        input = BP.E_bill_Email_Billing_Address__c.split('\\,');
       
        for(integer i=0; i<input.size();i++){
        if(!(pattern.Matches(PatternStr,input[i]))){
            BP.E_bill_Email_Billing_Address__c.addError('Email addresses entered in this field are not valid');
        }
        }
      } 
    }

  }
  
   // System.debug('out of Trigger');
    BillProfileUpdateCount.doAdd();
//CR-271 Validation rule on US postal code it shuould be before insert,before update
//Iterating and checking postal code pattern and throwing error message in case patttern doesnot match
for(BillProfile__c b : trigger.new){
     if(b.Country__c == 'US' && b.Billing_Entity__c == 'Telstra Incorporated' && b.Postal_Code__c!=null){
            system.debug('billing_entity' + b.Billing_Entity__c + '' + b.Country__c + b.Postal_Code__c);
            if(!(Pattern.matches('[0-9]{5} [0-9]{4}',b.Postal_Code__c)||Pattern.matches('[0-9]{5}-[0-9]{4}',b.Postal_Code__c)||   Pattern.matches('[0-9]{5}[0-9]{4}',b.Postal_Code__c))){
            
                b.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team');         
            }
             
        }else if(b.Postal_Code__c==null && b.Country__c == 'US' && b.Billing_Entity__c == 'Telstra Incorporated'){ 
        system.debug('Inside the billing_entity for null values' + b.Billing_Entity__c + '' + b.Country__c + b.Postal_Code__c);
           b.addError('The postal code entered for country United States of America is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789). If have filled in the appropriate data and you are still getting this error message then please contact support team'); 
            }
        
        
        


}
//IR116 change for Auto population of profile type based on Billing entity
  if(Trigger.isInsert){
    for(BillProfile__c b : trigger.new){
        if(b.Billing_Entity__c != null && b.Billing_Entity__c == 'Telstra International (AUS) Limited - Taiwan Branch'){
           b.Profile_type__c = 'Statement';
        }
        else{
         b.profile_type__c = 'Invoice';
        }                 
    }
  }
 */   
}