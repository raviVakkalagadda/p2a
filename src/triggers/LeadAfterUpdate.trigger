/**
    @author - Accenture
    @date - 06-JUNE-2012
    @version - 1.0
    @description - This trigger updates the lead status when the lead is assigned from Marketing to Sales Team".
*/
trigger LeadAfterUpdate on Lead (after update) {
/*
if(Util.muteAllTriggers()) return;
LeadSequenceController Lead = new LeadSequenceController();
Lead.afterUpdate();
*/
}