trigger LeadConversionRequiredAction on Lead (before update) {
    
   /* // When a Lead is sent for conversion Create an Action Item for the Sales Admin of the Region
    
      try{
      List<Action_Item__c> action = new List<Action_Item__c>();
      Map<String, ID> regionAdminMap = new Map<String, ID>();
      Map<ID, Lead> usrRegnMap = new Map<ID, Lead>();
      Set<Id> leadOwnerIds  = new Set<Id>();
      List <Lead> leadLst = [Select Id,OwnerId from Lead where id in :Trigger.newMap.keySet()];
      
       // Check for the region of the Sales user who has requested for Lead Conversion and find the respective regional Sales Admin.
       
      for(Lead ld : leadLst){
        usrRegnMap.put(ld.ownerId, ld);
        if (((String)ld.OwnerId).startswith('005')){
        leadOwnerIds.add(ld.OwnerId);   
        }
        
      }
      List<User> leadOwnerUser=[Select Id, FirstName,Region__c from User where id in : leadOwnerIds ];
      Map<Id,String> leadOwnerRegion = new Map<Id,String>();
      for (User u:leadOwnerUser){
        leadOwnerRegion.put(usrRegnMap.get(u.Id).Id,u.Region__c);
      }
        Profile profil = [Select Id from Profile Where Name='TI Sales Admin'];
      List<User> usrLst = [Select Id, Region__c from User Where ProfileId= :profil.Id];
      for(User usr : usrLst){
        regionAdminMap.put(usr.Region__c, usr.id);
      }
      
      // Fetch The Record Type of Adhoc Request Action Item for Triggering an Action Item.
      
        RecordType rt1 = [select Id,Name from RecordType where Name='Adhoc Request' and SobjectType = 'Action_Item__c' limit 1];
  for (Lead lead : Trigger.new)
  {
        //Creating an action item once the request for lead conversion button is clicked.
        
      if (lead.Request_for_Conversion__c == TRUE)
      {
        Action_Item__c ai = new Action_Item__c();              
              ai.Lead__c = lead.Id;
              ai.Due_Date__c = System.Today() + 5 ;  
              ai.RecordTypeId = rt1.Id;
              String region = leadOwnerRegion.containsKey(lead.Id)==FALSE?'Australia' : leadOwnerRegion.get(lead.Id);
              
              // Checks if the region is Null, consider it's region as 'Australia' by default
              if (region==null){
                region='Australia';
              }else if (region.trim().length()==0){ // Might not be needed but added for additional Security via DL (Implicit Loading)
                region='Australia';
              }
              ai.Region__c = region;
              ai.Subject__c = 'Lead Conversion Required';
              ai.Status__c = 'Assigned';
              ai.Priority__c = 'Medium';
              ai.OwnerId = regionAdminMap.get(region);
             
              action.add(ai); 
      }
    }
  insert action;
  }catch(Exception ex){
    System.debug('Exception in trigger LeadConversionRequiredAction '+ex);
  }
*/
}