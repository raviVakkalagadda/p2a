/**
    @author - Accenture
    @date - 04-MAY-2012
    @version - 1.0
    @description - This is trigger creates an action item when a owner changed from
                   Marketing to Sales Queue.
*/
trigger LeadQueueChangeAction on Lead (before update) {

    /*if(Util.muteAllTriggers()) return;
    List<Action_Item__c> action = new List<Action_Item__c>();
    //Fetches the queues with the lead object
    List<QueueSobject> leadQueues =[Select Id, SobjectType, QueueId, Queue.Name  From QueueSobject where SobjectType='Lead'];
    Map<String,QueueSobject> queueMap = new Map<String,QueueSobject>();
    
    //Seperating the queues based on their names
    for (QueueSobject leadQueue : leadQueues){
        queueMap.put(leadQueue.Queue.Name, leadQueue);
    }
    
    //Gets the queue based on the name of the queue
    QueueSobject marketingQueue = queueMap.get('Marketing');
    QueueSobject gspLeadQueue = queueMap.get('GSP Lead Queue');
    QueueSobject entSalesLeads = queueMap.get('Enterprise Sales Leads');
    
    //fetches the adhoc request record type 
    RecordType rt1 = [select Id,Name from RecordType where Name='Adhoc Request' and SobjectType = 'Action_Item__c' limit 1];
    for (Lead ld : Trigger.new)
    {
        // If Trigger Old Owner is a Queue and New Owner is also a Queue        
        if(((String)(ld.OwnerId)).startswith('00G')&& ((String)(Trigger.OldMap.get(ld.Id).OwnerId)).startswith('00G')){
            //checks whether the queue owner is sales queue 
            if ((ld.OwnerId==gspLeadQueue.QueueId ||ld.OwnerId==entSalesLeads.QueueId)&& Trigger.OldMap.get(ld.Id).OwnerId==marketingQueue.QueueId){
                //Creates an action item
                Action_Item__c ai = new Action_Item__c();               
                ai.Lead__c = ld.Id;
                ai.Due_Date__c = System.Today();  //Expected this SLA to be defined
                ai.RecordTypeId = rt1.Id;
                ai.Region__c = ld.Region__c;
                ai.Subject__c = 'Lead is Moved From Marketing to Sales';
                ai.Status__c = 'Assigned';
                ai.Priority__c = 'High';
                action.add(ai);   
            }
            
        }
    }
    insert action;*/
}