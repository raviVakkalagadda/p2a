trigger NotifyBillingUpdate on Billing_Update__c (after insert, before update) {
    /* Boolean isBundleService;
    List<String> BUObjIdList=new List<String>();
    Set<Id> resourceServicIdSet=new Set<Id>();
    Map<Id,Billing_Update__c> mapBu=new Map<Id,Billing_Update__c>();
    if(trigger.isInsert){
        
        for(Billing_Update__c buObj:trigger.new){
            
            isBundleService=buObj.Bundle_Flag__c;
            break;
            
        }
    
    
        if(isBundleService){
            
            // ip109 one time;
            for(Billing_Update__c buObj:trigger.new){
            
                BUObjIdList.add(buObj.Id);  
            
            }
            if(BUObjIdList.size()> 0){
                BillUpdateExtension.sendBillingUpdateToROC(BUObjIdList,isBundleService);
            }
        }else{
            
            for(Billing_Update__c buObj:trigger.new){
                if(!buObj.static_flag__c){
                    BUObjIdList.add(buObj.Id); 
                }   
        
            }  
            if(BUObjIdList.size()>0){
                BillUpdateExtension.sendBillingUpdateToROC(BUObjIdList,isBundleService);
            }
            
        }
        
    }
    if(Trigger.isUpdate){
            List<String> costCentreIntList = new List<String>();
            Map<String,CostCentre__c> ccIntCCObjMap = new Map<String,CostCentre__c>();
             //Defect #7647 Fix Part A by Bhargav
            Order_Submission_Failures__c failedBU = null;
            List<Order_Submission_Failures__c> BillingUpdateFailures = new List<Order_Submission_Failures__c>();
            //Defect #7647 Fix A ends here
            for(Billing_Update__c buObj:trigger.new){
            
                 //Defect #7647 Fix Part B by Bhargav
                if( buObj.ROC_Line_Item_Status__c == 'Error' || ( buObj.ROC_Line_Item_Status__c == 'Success' && buObj.ROC_Line_Item_Error__c.contains('not updated') ) ){
                    failedBU = new Order_Submission_Failures__c();
                    failedBU.Failed_Billing_Update__c = buObj.Id;
                    failedBU.Error_Description__c = buObj.ROC_Line_Item_Error__c;
                    BillingUpdateFailures.add(failedBU);
                }
                //Defect #7647 Fix Part B ends here
                resourceServicIdSet.add(buObj.Resource_Service_Id__c);
                mapBu.put(buObj.Resource_Service_Id__c,buObj);
                costCentreIntList.add(buObj.Cost_Centre_integration_number__c);
            }
            
            //Defect #7647 Fix Part C by Bhargav
            if(BillingUpdateFailures.size()>0){
                insert BillingUpdateFailures;
            }
            //Defect #7647 Fix Part C ends here
            
            List<CostCentre__c> costCentreList = [Select id,name,Cost_Centre_Integration_Number__c from CostCentre__c where Cost_Centre_Integration_Number__c in:costCentreIntList];
            List<Service__c> servList=[Select Id,Cost_centre__c,CostCentre_Id__c,Customer_PO__c,MRC_Bill_Text__c,NRC_Bill_Text__c,ETC_Bill_Text__c,Service_Bill_Text__c,Root_Product_Bill_Text__c,NRC_Credit_Bill_Text__c,RC_Credit_Bill_Text__c from Service__c where Id IN:resourceServicIdSet];
            List<Resource__c> resList=[Select Id,Cost_centre__c,CostCentre_Id__c,Customer_PO__c,MRC_Bill_Text__c,NRC_Bill_Text__c,ETC_Bill_Text__c,Service_Bill_Text__c,Root_Product_Bill_Text__c,NRC_Credit_Bill_Text__c,RC_Credit_Bill_Text__c from Resource__c where Id IN:resourceServicIdSet];
            
            //This map contains costcenterintegration number as Key and it's object as value
            for(CostCentre__c ccObj : costCentreList){
                ccIntCCObjMap.put(ccObj.Cost_Centre_Integration_Number__c ,ccObj);
            }
            System.debug('cost center Map size is '+ccIntCCObjMap.size() +' '+ccIntCCObjMap);
            
            
            //Updating Service Object with bill text rules which are flown down as part of IP109 interface
            for(Service__c serviceObj:servList){
                if(mapBu!=null){ 
                    if(mapBu.containsKey(serviceObj.Id)){
                    
                        if(!(mapBu.get(serviceObj.Id).ROC_Line_Item_Status__c=='Error')){
                             
                            if(mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c.contains('RC Bill Text not updated')){
                            
                            }else{
                             
                                serviceObj.MRC_Bill_Text__c = mapBu.get(serviceObj.Id).MRC_Bill_Text__c;
                                
                            }
                            
                            if(mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c.contains('NRC Bill Text not updated')){
                          
                            }else{
                              
                                serviceObj.NRC_Bill_Text__c = mapBu.get(serviceObj.Id).NRC_Bill_Text__c;
                            }
                            if(mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c.contains('ETC Bill Text not updated')){
                           
                            }else{
                                
                                serviceObj.ETC_Bill_Text__c=mapBu.get(serviceObj.Id).ETC_Bill_Text__c;
                            }
                            
                            
                            if(mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c.contains('RC Credit Text not updated')){
                          
                            }else{
                                 
                                serviceObj.RC_Credit_Bill_Text__c=mapBu.get(serviceObj.Id).RC_Credit_Bill_Text__c;
                                
                            }
                            
                            if(mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(serviceObj.Id).ROC_Line_Item_Error__c.contains('NRC Credit Text not updated')){
                           
                            }else{
                               
                                serviceObj.NRC_Credit_Bill_Text__c=mapBu.get(serviceObj.Id).NRC_Credit_Bill_Text__c;
                            }
                            
                            //Populating cost centre name and corresponding CostCentre object to Service
                            
                            if(ccIntCCObjMap.get(mapBu.get(serviceObj.Id).Cost_Centre_integration_number__c) != null){
                                
                                CostCentre__c CostCentreObj = ccIntCCObjMap.get(mapBu.get(serviceObj.Id).Cost_Centre_integration_number__c);
                                serviceObj.Cost_centre__c = CostCentreObj.Cost_Centre_Integration_Number__c;
                                serviceObj.CostCentre_Id__c = CostCentreObj.id;
                            }
                            System.debug('inside service loop'+serviceObj.Cost_centre__c);
                            
                            serviceObj.Customer_PO__c = mapBu.get(serviceObj.Id).Customer_PO_Number__c;
                            serviceObj.Service_Bill_Text__c = mapBu.get(serviceObj.Id).Service_Bill_Text__c;
                            serviceObj.Root_Product_Bill_Text__c = mapBu.get(serviceObj.Id).Root_Product_Bill_Text__c;                    
                        }
                    }
                }
            }
            if(servList.size()>0)update servList;
            
            //updating Service object ends here
            
            //updating Resource object with Bill text rules which are flown down as part of IP109
            for(Resource__c resourceObj:resList){
                if(mapBu!=null){ 
                    if(mapBu.containsKey(resourceObj.Id)){
                        if(!((mapBu.get(resourceObj.Id).ROC_Line_Item_Status__c) == 'Error')){
                                
                                if(mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c.contains('RC Bill Text not updated')){
                                
                                }else{
                                    
                                    resourceObj.MRC_Bill_Text__c = mapBu.get(resourceObj.Id).MRC_Bill_Text__c;
                                    
                                }
                                
                                if(mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c.contains('NRC Bill Text not updated')){
                                
                                }else{
                                
                                    resourceObj.NRC_Bill_Text__c = mapBu.get(resourceObj.Id).NRC_Bill_Text__c;
                                }
                                
                                if(mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c.contains('ETC Bill Text not updated')){
                                
                                }else{
                                    
                                    resourceObj.ETC_Bill_Text__c=mapBu.get(resourceObj.Id).ETC_Bill_Text__c;
                                }
                                
                                
                                if(mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c.contains('RC Credit Text not updated')){
                                
                                }else{
                                  
                                    resourceObj.RC_Credit_Bill_Text__c=mapBu.get(resourceObj.Id).RC_Credit_Bill_Text__c;
                                    
                                }
                                
                                if(mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c != null && mapBu.get(resourceObj.Id).ROC_Line_Item_Error__c.contains('NRC Credit Text not updated')){
                                
                                }else{
                                    
                                    resourceObj.NRC_Credit_Bill_Text__c=mapBu.get(resourceObj.Id).NRC_Credit_Bill_Text__c;
                                }
                                
                                //Populating cost centre name and corresponding CostCentre object to Service
                            
                                if(ccIntCCObjMap.get(mapBu.get(resourceObj.Id).Cost_Centre_integration_number__c) != null){
                                    
                                    CostCentre__c CostCentreObj = ccIntCCObjMap.get(mapBu.get(resourceObj.Id).Cost_Centre_integration_number__c);
                                    resourceObj.Cost_centre__c = CostCentreObj.Cost_Centre_Integration_Number__c;
                                    resourceObj.CostCentre_Id__c = CostCentreObj.id;
                                }
                                
                                
                                resourceObj.Customer_PO__c = mapBu.get(resourceObj.Id).Customer_PO_Number__c;
                                resourceObj.Service_Bill_Text__c = mapBu.get(resourceObj.Id).Service_Bill_Text__c;
                                resourceObj.Root_Product_Bill_Text__c = mapBu.get(resourceObj.Id).Root_Product_Bill_Text__c;
                        
                            
                        }
                    }
                }
            }
            if(resList.size()>0) update resList;
   }
   */
}