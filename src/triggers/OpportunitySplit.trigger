//I will remove

trigger OpportunitySplit on OpportunitySplit (before insert, before update,after update) {
	
        /*
		if(Util.muteAllTriggers() || util.FlagtoSkipOpportunitysplitTrigger==false || util.FlagtoMuteOpportunitySplitTrg==false || !label.MuteSPT.equalsignorecase('FALSE') ) return;
    system.debug('======Oppo Split in begining====='+trigger.new);
    Util.FlagtoMuteOpportunityDeleteSplitTrg=false;
    //OpportunityID variable assignment
    ID OpportunityID = Trigger.new[0].OpportunityID;
    Set<ID> OpportunityIDs = new Set<ID>();
    for(OpportunitySplit os : Trigger.new){
        OpportunityIDs.add(os.OpportunityId);
    }
    
    //Check whether the Opportunity's Sales Status is Won
    //If Yes, return with an error
    List<Opportunity> olist = [select sales_status__c,NewOppSplit__c,OwnerID,StageName from opportunity where id =: OpportunityID];
    
    ID ProfileID = UserInfo.getProfileID();
    List<Profile> ProfileName = [select name from Profile where id=:ProfileID];
    Map<Id,OpportunitySplit> mapOppSplit=new Map<Id,OpportunitySplit>();
if(!System.Label.ProfileNotForSplitOpp.contains(ProfileName[0].name)){
    if(olist[0].StageName=='Closed Won' && olist[0].StageName!='InFlight' && !System.Label.ProfileForSplitOpp.contains(ProfileName[0].name) && !Util.IsOppOwnerchange)
        Trigger.new[0].addError('Opportunity Split can not be updated after Opportunity is closed won. Please use the “Request to Modify Opportunity Split” button for raising the request to TI Sales Admin.');
    
    //Query list of Opportunity Splits already present under the Opportunity
    List<OpportunitySplit> OpportunitySplits = [Select id,SplitOwner.UserRole.Name,Team_Role__c, SplitAmount, SplitNote, SplitTypeID, SplitPercentage, SplitOwnerID, OpportunityID, SplitAmount_Product_SOV__c, SplitPercentage_Product_SOV__c, Total_Product_SOV__c, isApproved__c, isRejected__c from OpportunitySplit where OpportunityID =: OpportunityID and splittype.isactive=true];
    
    for(OpportunitySplit oppSplit:OpportunitySplits){
        mapOppSplit.put(oppSplit.splitownerId,oppSplit);
    }
    
        //Update Opp Once the AI is approved
    
      Util.restrictRecurPerfCallFlag=false;
      if(Trigger.isUpdate && trigger.isAfter){
        for(OpportunitySplit os : trigger.new){
            if(os.Team_Role__c=='Cross Country Application' && (os.isApproved__c || os.isRejected__c)){
                Util.skipOpportunitySplitTeamTrgFlag=false;
                update olist;
                break;
            }
            //flag to update opportunity when modification AI is completed -- Bhargav -- 2 June 2016
            if( Util.OpportunityUpdateOnAIUpdate == false){
                Util.skipOpportunitySplitTeamTrgFlag=false;
                update olist;
                Util.OpportunityUpdateOnAIUpdate = true;
                break;
            }
        }
    }
    
    //Query list of Team Members of Opportunity based on Opportunity ID
    List<OpportunityTeamMember> OpportunityTeamMembers = [Select id,User.UserRole.Name, User.UserRole.DeveloperName, UserID, TeamMemberRole, OpportunityID from OpportunityTeamMember where OpportunityId =: OpportunityID];
    
    if(Trigger.isBefore){
        List<SalesSpecialistRoleOppTeam__c> SSRoleOppTeamList = SalesSpecialistRoleOppTeam__c.getAll().values();
        Map<String,String> mapSSRoleOppTeam=new Map<String,String>();
        for(SalesSpecialistRoleOppTeam__c ssROleOppTeam:SSRoleOppTeamList){
            mapSSRoleOppTeam.put(ssROleOppTeam.SSRole__c,ssROleOppTeam.OpportunityTeam__c);
        }
        
        //Changes to Sales Specialist Opportunity Splits based on manual updates -- Bhargav
        system.debug('Util.skipOpportunitySplitManualCodeFlag '+Util.skipOpportunitySplitManualCodeFlag);
        if(Trigger.isUpdate && trigger.isBefore && Util.skipOpportunitySplitManualCodeFlag){
            List<OpportunitySplit> SplitsToBeUpdated = new List<OpportunitySplit>();
            Decimal SalesSpecialistTotalPercentage = Trigger.new[0].SplitPercentage_Product_SOV__c;
            system.debug('====SalesSpecialistTotalPercentage111==='+SalesSpecialistTotalPercentage);
             system.debug('====Trigger.new[0].Team_Role__c==='+Trigger.new[0].Team_Role__c);
            for(OpportunitySplit os : OpportunitySplits){
                if(Trigger.new[0].Team_Role__c!=null && ( Trigger.new[0].Team_Role__c.contains('Specialist') || Trigger.new[0].Team_Role__c.contains('FSI')) ){
                    if(Trigger.oldmap.get(Trigger.new[0].id).SplitPercentage_Product_SOV__c < Trigger.newmap.get(Trigger.new[0].id).SplitPercentage_Product_SOV__c){
                        if(Trigger.new[0].SplitPercentage_Product_SOV__c == 100){
                            if(os.Team_Role__c == Trigger.new[0].Team_Role__c && os.id != Trigger.new[0].id){
                                os.SplitPercentage_Product_SOV__c = 0;
                                os.SplitAmount_Product_SOV__c = 0;
                                SplitsToBeUpdated.add(os);
                            }
                        }else{
                            if(os.Team_Role__c == Trigger.new[0].Team_Role__c && os.id != Trigger.new[0].id)
                                SalesSpecialistTotalPercentage+=os.SplitPercentage_Product_SOV__c;
                        }
                    }
                }
            }
            system.debug('========SalesSpecialistTotalPercentage===='+SalesSpecialistTotalPercentage);
            if(SalesSpecialistTotalPercentage>100){
                Trigger.new[0].addError('To proceed, review this page for all error messages and make corrections.'+
                'The total recognition of a product split cannot exceed 100% at product sales specialist level, unless cross country application is submitted.');
            }
            for(Integer i=0; i<SplitsToBeUpdated.size(); i++){
                if(SplitsToBeUpdated.get(i).id==Trigger.new[0].id){
                    SplitsToBeUpdated.remove(i);
                    //system.debug('id'+o.id+'role'+o.team_role__c+'percent'+o.SplitPercentage_Product_SOV__c);
                }
            }
            system.debug('==SplitsToBeUpdated=='+SplitsToBeUpdated);
            if(SplitsToBeUpdated.size()>0)
                update SplitsToBeUpdated;
            Util.skipOpportunitySplitTeamTrgFlag = false;
        }
        //Changes to Sales Specialist Opportunity Splits based on manual updates -- Bhargav -- changes end here
        
        //Changes to Account Manager Opportunity Splits based on manual updates -- Bhargav
        system.debug('Util.skipOpportunitySplitManualCodeFlag '+Util.skipOpportunitySplitManualCodeFlag);
        if(Trigger.isUpdate && trigger.isBefore && Util.skipOpportunitySplitManualCodeFlag){
            Decimal AccountManagerTotalPercentage = 0;
            for(OpportunitySplit ops : (List<OpportunitySplit>)trigger.new){
                if((ops.Team_Role__c == 'Opportunity Owner' || ops.Team_Role__c == 'Account Manager') && ops.OppSplitType__c){
                    AccountManagerTotalPercentage = AccountManagerTotalPercentage + ops.SplitPercentage;
                }
            }
            if(AccountManagerTotalPercentage>100){
                Trigger.new[0].addError('The total recognition of an opportunity can not exceed 100% at account manager level, unless cross country application is submitted.');
            }
        }
        //Changes to Account Manager Opportunity Splits based on manual updates -- Bhargav -- changes end here
  if(Trigger.isUpdate && trigger.isBefore){
  
   for(Opportunitysplit opps:trigger.new){
    if(mapOppSplit!=null && Util.IsOppOwnerchange && Util.newOpportunityOwnerId!=null && opps.splitownerid==Util.newOpportunityOwnerId && mapOppSplit.containskey(opps.splitownerid) ){
        opps.splitpercentage=mapOppSplit.get(opps.splitownerid).splitpercentage;
        opps.Team_Role__c='Opportunity Owner';
        opps.SplitNote=opps.Team_Role__c;
    }
   }
   
  }
        
    }
    
    if(Util.skipOpportunitySplitFlag==false ) return;
       //Update Opp Once the AI is approved
    
    
    if(!trigger.isafter){
        //if(trigger.oldmap!=null && trigger.oldmap.get(trigger.new[0].id).Team_Role__c!='Opportunity Owner' && trigger.newmap!=null && trigger.newmap.get(trigger.new[0].id).Team_Role__c=='Opportunity Owner'){
        //}else{
            
          ID splittypeidactive = [select id from opportunitysplittype where isactive=:true][0].id;
          if(olist[0].NewOppSplit__c){
               Trigger.new[0].SplitTypeId = splittypeidactive;
            }
        //}
   
   

        
              
           
      //22 feb
      set<String> userRoleSet=new Set<String>();
     Map<String,List<OpportunitySplit>> mapRoleCount=new map<String,List<OpportunitySplit>>();
     List<OpportunitySplit> oppSplitLst=new List<OpportunitySplit>();
     for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
      
      userRoleSet.add(OppTeamMem.User.UserRole.Name);
     }
     for(String userRole:userRoleSet){
      integer count=0;
      oppSplitLst=new List<OpportunitySplit>();
      for(OpportunitySplit OppSplit:OpportunitySplits){
      
      if(OppSplit.SplitOwner.UserRole.Name==userRole){
      
      oppSplitLst.add(OppSplit);
      }
      }
      mapRoleCount.put(userRole,oppSplitLst);
     }
      
      
      //22feb     
               
      for(Opportunitysplit oppsplit:trigger.new){
        if(oppsplit.Total_Product_SOV__c!=null && oppsplit.SplitPercentage_Product_SOV__c!=null){
            oppsplit.SplitAmount_Product_SOV__c=(oppSplit.SplitPercentage_Product_SOV__c*oppsplit.Total_Product_SOV__c)/100;
        }
      }
        
    

    //Channels and Alliances and Pre Sales -- Bhargav -- Enhancement 10098
    List<Role_Mapping_Object__c> RoleMapList = Role_Mapping_Object__c.getAll().values();
    Set<String> overlayroles = new Set<String>();
    for(Role_Mapping_Object__c rmo : RoleMapList)
        overlayroles.add(rmo.Parent_Role__c );
    for(OpportunitySplit o : trigger.new){
        for(OpportunityTeamMember otm : OpportunityTeamMembers){
            if(o.SplitOwnerID == otm.UserID && overlayroles.contains(otm.User.UserRole.DeveloperName) && otm.TeamMemberRole!='Cross Country Application'){
                o.SplitPercentage=0;
            }
        }
    }
      
    }  
  //Util.restrictRecurPerfCallFlag=false;
    if(olist[0].StageName=='Closed Won' && !Util.IsOppOwnerchange && trigger.isafter){
        system.debug('=====trigger==='+trigger.new);
       //Util.restrictRecurPerfCallFlag=false;
       update olist;
        
        
    }
    //
    
    
}    
    system.debug('======Oppo Split in begining====='+trigger.new); */
}