trigger OpportunitySplitBeforeDelete on OpportunitySplit (before delete,before update) {
/**
//i will remove this trigger
if(Util.FlagtoSkipOpportunityAfterTrigger==false || Util.FlagtoMuteOpportunityDeleteSplitTrg==false) return;
try{
Set<Id> opIdsSet=new Set<Id>();
List<OpportunitySplit> oppSplitList=new List<OpportunitySplit>();
Map<id,OpportunitySplit> mapOppSplit=new Map<id,OpportunitySplit>();
if(trigger.isdelete){oppSplitList=trigger.old;mapOppSplit=Trigger.oldmap;}else{oppSplitList=trigger.new;mapOppSplit=Trigger.newmap;}
List<UserRole> userRoleList=[select Id,parentRoleId from UserRole];
List<OpportunitySplit> oppSplit=[select id,CurrUserProfileName__c,Opportunity.Owner.userRoleId,Opportunity.StageName from Opportunitysplit where id in:mapOppSplit.keyset()];
for(OpportunitySplit opp:oppSplit){
if(!System.Label.ProfileNotForSplitOpp.contains(opp.CurrUserProfileName__c)){
if(opp.Opportunity.StageName=='Closed Won' && !System.Label.ProfileForSplitOpp.contains(opp.CurrUserProfileName__c)){
mapOppSplit.get(opp.id).adderror('Opportunity Split Update is restricted to users other than Administrator');
return;
}
}
}

Set<Id> ownerRoleSet=new Set<Id>();

for(OpportunitySplit opp:oppSplit){

ownerRoleSet.add(opp.Opportunity.Owner.userRoleId);
}


        Id tempid=null;
    for(Id rId:ownerRoleSet){
       tempid=rId;
      // system.debug('=====tempid1===='+tempid);
        for(integer i=0;i<userRoleList.size();i++){ 
        
            if(userRoleList[i].Id==tempid){
           //system.debug('=====tempid2===='+tempid);
            ownerRoleSet.add(userRoleList[i].ParentRoleID);
            tempid=userRoleList[i].ParentRoleID;
            i=0;
           //system.debug('=====tempid3===='+tempid);
            }
        
            }   
        
    } 
system.debug('===ownerRoleSet==='+ownerRoleSet);
system.debug('===UserInfo.getUserRoleId()==='+UserInfo.getUserRoleId());
system.debug('====frferree'+!ownerRoleSet.contains(UserInfo.getUserRoleId()));

for(Opportunitysplit opp:oppSplitList){
if(!System.Label.ProfileNotForSplitOpp.contains(opp.CurrUserProfileName__c)){
if(!ownerRoleSet.contains(UserInfo.getUserRoleId()) && !System.Label.ProfileForSplitOpp.contains(opp.CurrUserProfileName__c)){
opp.adderror('Opportunity Split Update is restricted to users other than Opportunity Owner and Administrator.');
}
}
}
}catch(exception ex){
system.debug('ex'+ex.getmessage());
}
*/
}