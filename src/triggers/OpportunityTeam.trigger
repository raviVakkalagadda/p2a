trigger OpportunityTeam on OpportunityTeamMember (after insert,after update) {
    /*
    if(Util.muteAllTriggers() || Util.skipOpportunitySplitTeamTrgFlag==false) return;
      Util.FlagtoMuteOpportunityDeleteSplitTrg=false;
    try{
    //Capture the OpportunityIDs
    ID OpportunityID = Trigger.new[0].OpportunityID;
    system.debug('OpportunityID '+OpportunityID);
    
    //Check whether the Opportunity's Sales Status is Won
    //If Yes, return with an error
    List<Opportunity> olist = [select sales_status__c,StageName,OwnerId from opportunity where id =: OpportunityID];
    ID ProfileID = UserInfo.getProfileID();
    List<Profile> ProfileName = [select name from Profile where id=:ProfileID];
if(!System.Label.ProfileNotForSplitOpp.contains(ProfileName[0].name)){
    if(util.FlagtoMuteOpportunitySplitTrg && olist[0].sales_status__c=='Won' && !System.Label.ProfileForSplitOpp.contains(ProfileName[0].name))
        Trigger.new[0].addError('Opportunity Split can not be updated after Opportunity is closed won. Please use the “Request to Modify Opportunity Split” button for raising the request to TI Sales Admin.');
    
    List<OpportunityTeamMember> OpportunityTeamMembers = [Select ID, UserID, User.UserRole.Name, OpportunityID, TeamMemberRole from OpportunityTeamMember where OpportunityID = : OpportunityID];
    system.debug('OTM Users '+OpportunityTeamMembers);
        
    //Create a new OpportunitySplit Object
    OpportunitySplit o = null;
       
    //Create a new OpportunitySplit Object List
    List<OpportunitySplit> OppSplits = new List<OpportunitySplit>();
        
    //Query list of existing Opportunity Splits
    List<OpportunitySplit> OppSplitsExisting = [Select OppSplitType__c,SplitType.Isactive,Opportunity.OwnerId,Total_Product_SOV__c,Team_Role__c,OpportunityID, SplitPercentage_Product_SOV__c,SplitPercentage, SplitOwnerID, SplitTypeId, isApproved__c, isRejected__c from OpportunitySplit where OpportunityID = : OpportunityID];
    system.debug('ExistingSplits '+OppSplitsExisting);
    //changes as part of defect #10034 fix - Bhargav
    Map<ID,OpportunitySplit> existingSplitsMap = new Map<ID,OpportunitySplit>();
        Map<String,decimal> existingNewRefSplitsMap = new Map<String,decimal>();

    for(OpportunitySplit opspt : OppSplitsExisting){
        existingSplitsMap.put(opspt.SplitOwnerID,opspt);
        system.debug('======='+opspt.Team_Role__c+'===='+opspt.SplitPercentage+'====='+opspt.OppSplitType__c+'===='+opspt.Opportunity.OwnerId+'==='+Util.OldOwnerId);
                if(opspt.Team_Role__c=='Opportunity Owner' && opspt.OppSplitType__c){
        existingNewRefSplitsMap.put('Opportunity Owner',opspt.SplitPercentage);
        }

     }   
    //Declaration of a new set for capturing the existing Opportunity Split's Owner IDs
    Set<ID> existingSplitIDs = new Set<ID>();
           
        //Adding the SplitOwnerIDs to the set
        for(OpportunitySplit ops : OppSplitsExisting){
            existingSplitIDs.add(ops.SplitOwnerID);
        }
        
        ID splittypeidactive = [select splittypeid from opportunitysplit where splittype.isactive=:true][0].splittypeid;
        system.debug('OverlaySplitTypeID '+splittypeidactive);
        
        //List used to add oppsplits to be updated
        List<OpportunitySplit> osplits = new List<OpportunitySplit>();
        
        for(OpportunityTeamMember otm : OpportunityTeamMembers){
            o = new OpportunitySplit();
            String TeamMemberUserName = null;
            if(otm.User.UserRole.Name!=null)
                TeamMemberUserName=otm.User.UserRole.Name;
                 
            if(!existingSplitIDs.contains(otm.UserID)){
                
                //Put in anew condition to auto correct splits if and only if splits need autocorrecting -- Bhargav -- QC#9713 -- 31 March
                if(otm.TeamMemberRole == 'Opportunity Owner' || otm.TeamMemberRole == 'Cross Country Application' || (TeamMemberUserName!=null && TeamMemberUserName.contains('Account Manager') )){
                    //changes as part of restricting duplicate data for CCA - Bhargav
                    if( (otm.TeamMemberRole == 'Cross Country Application' && TeamMemberUserName!=null && !TeamMemberUserName.contains('Sales Specialist') && !TeamMemberUserName.contains('FSI Specialist'))){
                        o.SplitPercentage = 100;
                        o.OpportunityID = OpportunityID;
                        o.Team_Role__c=otm.TeamMemberRole;
                        o.SplitNote=o.Team_Role__c;
                        o.SplitOwnerID = otm.UserID;
                        o.SplitTypeid=splittypeidactive;
                        system.debug('Bhargav Debug 1');
                        OppSplits.add(o);
                    }else{
                        //changes as part of enhancements 10040/9914/10098 -- Bhargav
                        if(otm.TeamMemberRole == 'Opportunity Owner' && !existingNewRefSplitsMap.containskey('Opportunity Owner')){
                            o.SplitPercentage=100;
                            o.Team_Role__c='Opportunity Owner';
                            o.SplitNote=o.Team_Role__c;
                            o.SplitOwnerID = otm.UserID;
                            o.SplitTypeid=splittypeidactive;
                            o.OpportunityID = OpportunityID;
                            OppSplits.add(o);
                        }else if(otm.TeamMemberRole != 'Opportunity Owner' && TeamMemberUserName!=null && TeamMemberUserName.contains('Account Manager')){
                            o.SplitPercentage=0;
                            o.Team_Role__c='Account Manager';
                            o.SplitNote=o.Team_Role__c;
                            o.SplitOwnerID = otm.UserID;
                            o.SplitTypeid=splittypeidactive;
                            o.OpportunityID = OpportunityID;
                            OppSplits.add(o);
                        }
                    }
                }
            }
            //Updating Opportunity Splits based on new value of Opportunity Team Member Role
            //changes as part of enhancements 10040/9914/10098 -- Bhargav
          
            else if(existingSplitIDs.contains(otm.UserID) && (otm.TeamMemberRole == 'Opportunity Owner' || otm.TeamMemberRole == 'Cross Country Application' || ( TeamMemberUserName!=null && TeamMemberUserName.contains('Account Manager') ) )){
                //changes as part of restricting duplicate data for CCA - Bhargav
                //changes as part of defect #10034 fix - Bhargav
                 system.debug('TeamMemberUserName 1'+TeamMemberUserName);
                if( ( otm.TeamMemberRole == 'Cross Country Application' && TeamMemberUserName!=null && !TeamMemberUserName.contains('Sales Specialist') && !TeamMemberUserName.contains('FSI Specialist') && existingSplitsMap.containskey(otm.UserID) && !existingSplitsMap.get(otm.UserID).isApproved__c && !existingSplitsMap.get(otm.UserID).isRejected__c )){
                    o.SplitPercentage = 100;
                    system.debug('Bhargav Debug 2');
                    
                }else{
                    if(otm.TeamMemberRole == 'Opportunity Owner' && otm.UserID == Trigger.new[0].UserID ){
                            o.SplitPercentage = 100;
                            o.Team_Role__c='Opportunity Owner';
                            o.SplitNote=o.Team_Role__c;
                           
                    }else if(TeamMemberUserName!=null && TeamMemberUserName.contains('Account Manager')){
                         system.debug('TeamMemberUserName 2'+TeamMemberUserName);
                            o.Team_Role__c='Account Manager';
                            o.SplitNote=o.Team_Role__c;
                            o.SplitPercentage=0;
                            
                        }
                }
            }
        }
        
        system.debug('Splits to be inserted/updated '+OppSplits);
       Map<String,Decimal> MapCat_ProductSov=PerformanceProductSOVCalculationCls.getMapOnSSCatProductSov(new Set<Id>{OpportunityID});
        // new change   
     //Suparna i will be modifying soon
      List<SalesSpecialistRoleOppTeam__c> SSRoleOppTeamList = SalesSpecialistRoleOppTeam__c.getAll().values();
       Map<String,String> mapSSRoleOppTeam=new Map<String,String>();
       for(SalesSpecialistRoleOppTeam__c ssROleOppTeam:SSRoleOppTeamList){
       
       mapSSRoleOppTeam.put(ssROleOppTeam.SSRole__c,ssROleOppTeam.OpportunityTeam__c);
       }
     set<String> userRoleSet=new Set<String>();
     Map<String,integer> mapRoleCount=new map<String,integer>();
     for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
      
      userRoleSet.add(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name));
     }
     for(String userRole:userRoleSet){
      system.debug('UserRole Bhargav '+ userRole);
      integer count=0;
      integer crosscountrycount=0;
      for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
      system.debug('=====OppTeamMem.User.UserRole.Name==='+OppTeamMem.User.UserRole.Name);
       system.debug('=====mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)==='+mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name));
      //Put in anew condition to auto correct splits if and only if splits need autocorrecting -- Bhargav -- QC#9713 -- 31 March
      if(mapSSRoleOppTeam.containskey(OppTeamMem.User.UserRole.Name) && mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)==userRole && OppTeamMem.TeamMemberRole!='Cross Country Application'){
      
      count=count+1;
      system.debug('Bhargav Debug 3');
      }
      }
      mapRoleCount.put(userRole,count);
     }
     system.debug('====mapRoleCount===='+mapRoleCount);
     OpportunitySplit oppSplit=null;
     List<Opp_Product_Type__c> oppProductTypeList=[select id,Sales_Specialist_Category__c,Product_SOV__c from Opp_Product_Type__c where Opportunity__c=:OpportunityID];
     Map<Id,OpportunitySplit> MapOwneridOppSplit=new Map<Id,OpportunitySplit>();
     for(OpportunitySplit oppSplitEx:OppSplitsExisting){
        MapOwneridOppSplit.put(oppSplitEx.SplitOwnerID,oppSplitEx);
        
     }
     
   for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
      //changes as part of defect #10034 fix - Bhargav
      if(OppTeamMem.UserID!=null && MapOwneridOppSplit.containskey(OppTeamMem.UserID) && OppTeamMem.TeamMemberRole=='Cross Country Application' && existingSplitsMap.containskey(OppTeamMem.UserID) && !existingSplitsMap.get(OppTeamMem.UserID).isApproved__c && !existingSplitsMap.get(OppTeamMem.UserID).isRejected__c && !mapSSRoleOppTeam.containskey(OppTeamMem.User.UserRole.Name)){
        oppSplit=new OpportunitySplit();
        oppSplit=MapOwneridOppSplit.get(OppTeamMem.UserID);
        oppSplit.Team_Role__c='Cross Country Application';
        oppSplit.SplitNote=oppSplit.Team_Role__c;
        oppSplit.SplitPercentage=100;
         OppSplits.add(oppSplit);
        system.debug('Bhargav Debug 4');
      }
      }
     system.debug('===MapOwneridOppSplit=='+MapOwneridOppSplit);
  
     
      Map<id,OpportunityTeamMember> mapOpportunityTeam=new Map<id,OpportunityTeamMember>();
      for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
        mapOpportunityTeam.put(OppTeamMem.UserId,OppTeamMem);
      
      }
       Set<String> oldOpportunityTeamStr=new Set<String>();
   
     for(OpportunityTeamMember oppTeam:trigger.new){
         
        if(mapOpportunityTeam.containskey(oppTeam.UserId) && mapSSRoleOppTeam.containskey(mapOpportunityTeam.get(oppTeam.UserId).User.UserRole.Name)){
            
            oldOpportunityTeamStr.add(mapSSRoleOppTeam.get(mapOpportunityTeam.get(oppTeam.UserId).User.UserRole.Name));
            
        }
       
     }
     
   
     List<OpportunityTeamMember> OpportunityTeamMemberSSList=new List<OpportunityTeamMember>();
     for(OpportunityTeamMember OppTeamMem:OpportunityTeamMembers){
        
        if( mapSSRoleOppTeam.containskey(OppTeamMem.User.UserRole.Name) && oldOpportunityTeamStr.contains(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name))){
            
            OpportunityTeamMemberSSList.add(OppTeamMem);
            
        }
        
     }   
       
     system.debug('MapCat_ProductSov====='+MapCat_ProductSov);
     system.debug('OpportunityTeamMemberSSList====='+OpportunityTeamMemberSSList);
     
   
     for(OpportunityTeamMember OppTeamMem:OpportunityTeamMemberSSList){
     
    
      for(String SSCatStr:MapCat_ProductSov.keyset()){
     //Put in anew condition to auto correct splits if and only if splits need autocorrecting -- Bhargav -- QC#9713 -- 31 March
     if(SSCatStr!=null && ( OppTeamMem.User.UserRole.Name.containsIgnoreCase(SSCatStr) || ( OppTeamMem.User.UserRole.Name.contains('Cloud') && !OppTeamMem.User.UserRole.Name.contains('PreSales') && SSCatStr=='GiaaS' ) )){
      if(OppTeamMem.UserID!=null && !MapOwneridOppSplit.containskey(OppTeamMem.UserID)){
       oppSplit=new OpportunitySplit();
        oppSplit.SplitOwnerID = OppTeamMem.UserID;
        oppSplit.SplitTypeid=splittypeidactive;
        oppSplit.OpportunityID = OpportunityID;
        
      }else{
        oppSplit=new OpportunitySplit();
        oppSplit=MapOwneridOppSplit.get(OppTeamMem.UserID);
      }
      //changes as part of defect #10034 and 10082 fix - Bhargav
      if(OppTeamMem.TeamMemberRole=='Cross Country Application' && ((existingSplitsMap.containskey(OppTeamMem.UserID) && !existingSplitsMap.get(OppTeamMem.UserID).isApproved__c && !existingSplitsMap.get(OppTeamMem.UserID).isRejected__c) || !existingSplitsMap.containskey(OppTeamMem.UserID))){
       // if(Util.NoTeamUpdateRequired){oppSplit.SplitPercentage_Product_SOV__c=existingSplitsMap.containskey(OppTeamMem.UserID) && existingSplitsMap.get(OppTeamMem.UserID)!=null && existingSplitsMap.get(OppTeamMem.UserID).Team_Role__c=='Cross Country Application'?existingSplitsMap.get(OppTeamMem.UserID).SplitPercentage_Product_SOV__c:100;}
       if(Util.NoTeamUpdateRequired){oppSplit.SplitPercentage_Product_SOV__c=100;}
         oppSplit.SplitAmount_Product_SOV__c=(oppSplit.SplitPercentage_Product_SOV__c*(MapCat_ProductSov.containskey(SSCatStr)?MapCat_ProductSov.get(SSCatStr):0))/100;
        oppSplit.Team_Role__c='Cross Country Application';
         oppSplit.SplitPercentage=0;
      oppSplit.SplitNote=oppSplit.Team_Role__c;
      system.debug('Bhargav Debug 5');
      }else{  
        decimal perc=100.00;
        system.debug('===========perc==============='+mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)));
         //changes made as part of defect 10082 fix -- Bhargav -- added another condition
        if(mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name))!=0 && OppTeamMem.UserID == Trigger.new[0].UserID){
             if(Util.NoTeamUpdateRequired){ oppSplit.SplitPercentage_Product_SOV__c =(perc/ mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)));}
       oppSplit.SplitAmount_Product_SOV__c=(oppSplit.SplitPercentage_Product_SOV__c*(MapCat_ProductSov.containskey(SSCatStr)?MapCat_ProductSov.get(SSCatStr):0))/100;
        //changes made as part of defect 10082 fix -- Bhargav -- added another condition
        if(OppSplitsExisting.size()>0){
            for(OpportunitySplit exop : OppSplitsExisting){
                if(exop.Team_Role__c==mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)){
                    if(Util.NoTeamUpdateRequired) {exop.SplitPercentage_Product_SOV__c=(perc/ mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)));}
                    exop.SplitAmount_Product_SOV__c=(exop.SplitPercentage_Product_SOV__c*(MapCat_ProductSov.containskey(SSCatStr)?MapCat_ProductSov.get(SSCatStr):0))/100;
                }
            }
        }
      }else{
      //Issue:
      if(mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name))>0){
           if(Util.NoTeamUpdateRequired){oppSplit.SplitPercentage_Product_SOV__c =(perc/ mapRoleCount.get(mapSSRoleOppTeam.get(OppTeamMem.User.UserRole.Name)));}
          oppSplit.SplitAmount_Product_SOV__c=(oppSplit.SplitPercentage_Product_SOV__c*(MapCat_ProductSov.containskey(SSCatStr)?MapCat_ProductSov.get(SSCatStr):0))/100;
      }
     //Issue:when add 
        }
        
     if(OppTeamMem.TeamMemberRole!='Cross Country Application') {
      oppSplit.Team_Role__c=SSCatStr.containsignorecase('FSI')?'FSI':'Sales Specialist - '+SSCatStr;
      oppSplit.SplitNote=oppSplit.Team_Role__c;
     }else if(OppTeamMem.TeamMemberRole=='Cross Country Application'){
       oppSplit.Team_Role__c='Cross Country Application';
       oppSplit.SplitNote=oppSplit.Team_Role__c;
     }
      }
       oppSplit.Total_Product_SOV__c=(MapCat_ProductSov.containskey(SSCatStr)?MapCat_ProductSov.get(SSCatStr):0);
      system.debug('====oppSplitperc==='+oppSplit.Team_Role__c);
       oppSplit.SplitPercentage=0;
      
      OppSplits.add(oppSplit);
     }
     
     
          }
         
     }
     //   
     
     Set<OpportunitySplit> oppSLst = new Set<OpportunitySplit>();
List<OpportunitySplit> result = new List<OpportunitySplit>();

oppSLst.addAll(OppSplits);
oppSLst.addAll(OppSplitsExisting);
result.addAll(oppSLst);
system.debug('=====OppSplits===before'+result);
  for(OpportunitySplit oppSpli:result){
    if(oppSpli.SplitOwnerID==olist[0].OwnerId){
      oppSpli.Team_Role__c='Opportunity Owner';
      oppSpli.SplitNote=oppSpli.Team_Role__c;
            
    }
 }

Integer j = 0;
while (j < result.size())
{
    if(olist[0].sales_status__c=='Open' && result.get(j).SplitOwnerId!=olist[0].OwnerId && result.get(j).Team_Role__c=='Opportunity Owner'){
    
    result.get(j).SplitPercentage=0;
    result.remove(j);
  }else
  {
    j++;
  }
 
}
system.debug('=====Util.OldOwnerId==='+Util.OldOwnerId); 
j=0;  
while (j < result.size())
{
       
    if(olist[0].StageName=='Closed Won' && Util.OldOwnerId!=null && result.get(j).SplitOwnerId!=Util.OldOwnerId && result.get(j).Team_Role__c=='Opportunity Owner'){
    system.debug('====result.get(j).SplitOwnerIdafter==='+result.get(j).SplitOwnerId);
    system.debug('====1====');
    result.get(j).SplitPercentage=0;
    result.remove(j);
  }else
  {
    j++;
  }
 
} 
 
Util.FlagtoMuteOpportunityDeleteSplitTrg=false;
 
 
 
for(OpportunitySplit oppSpli:result){
    if(oppSpli.SplitOwnerId==olist[0].OwnerId && oppSpli.Team_Role__c=='Opportunity Owner' && olist[0].sales_status__c=='Open' && oppSpli.OppSplitType__c && util.IsOppOwnerchange){
  oppSpli.SplitPercentage=Util.mapsplitPercenatgeOfAcc.containskey(oppSpli.OpportunityID)?Util.mapsplitPercenatgeOfAcc.get(oppSpli.OpportunityID):0;
     system.debug('====1====');
    }
  
}
 Boolean flag1=false;
 Boolean flag2=false;
 for(OpportunitySplit oppSpli:result){
  if(oppSpli.SplitOwnerId==olist[0].OwnerId && oppSpli.Team_Role__c=='Opportunity Owner' && olist[0].sales_status__c=='Open' && !oppSpli.OppSplitType__c){
   flag1=true;
    system.debug('====2====');
  }
 if(oppSpli.SplitOwnerId==olist[0].OwnerId && oppSpli.Team_Role__c=='Opportunity Owner' && olist[0].sales_status__c=='Open' && oppSpli.OppSplitType__c){
   flag2=true;
    system.debug('====3====');
  }
  
  if(Util.ISOppSizeisZero==false && Util.splitPercentageForOwner!=null && Util.FlagNeedReopen==false && oppSpli.SplitOwnerId==olist[0].OwnerId && oppSpli.Team_Role__c=='Opportunity Owner' && olist[0].sales_status__c=='Open' && oppSpli.OppSplitType__c){
   oppSpli.SplitPercentage=Util.splitPercentageForOwner;
    system.debug('====4====');
  }
  
  
 }
 if(flag1 && !flag2 &&  Util.FlagNeedReopen==false){
 OpportunitySplit oppSplit1=new OpportunitySplit();
   oppSplit1.SplitOwnerID = olist[0].OwnerId;
   oppSplit1.SplitTypeid=splittypeidactive;
   oppSplit1.OpportunityID = OpportunityID;
   oppSplit1.SplitPercentage=Util.splitPercentageForOwner;
     system.debug('====5====');
   result.add(oppSplit1);
 
 }
   for(OpportunitySplit oppSpli:result){
    if(oppSpli.SplitOwnerID==olist[0].OwnerId){
      oppSpli.Team_Role__c='Opportunity Owner';
      oppSpli.SplitNote=oppSpli.Team_Role__c;
            
    }
 }
 
 

   system.debug('=====OppSplits===after'+result);
         if(result.size()>0){
            Util.FlagtoSkipOpportunityAfterTrigger=false;
            Util.skipOpportunitySplitManualCodeFlag=false;
         upsert result;
         }
       Util.skipOpportunitySplitTeamTrgFlag=false;    
}            
    }catch(Exception e){
         System.debug('Exception======='+e.getCause()+','+e.getMessage()+','+e.getLineNumber()); 
    }    
    */
}