/**
@author - Accenture
@date - 19-JUNE-2012
@version - 1.0
@description - This trigger creates the task when the SD PM Contact is Assigned and when the Order is Submitted and In Flight set to false.
*/
trigger OrderAfterUpdate on Order__c (after update) {

/*    if(Util.muteAllTriggers()) return;
    
    System.debug('OrderAfterUpdate Starting used in the Apex Trigger: ' + Limits.getQueries());
    List<Task> newTask = new List<Task>();
    List<Order_Line_Item__c> updOliLst = new List<Order_Line_Item__c>();
    List<Id> ordIDs_list = new List<Id>();
    List<Order_Line_Item__c> updOliLst1 = new List<Order_Line_Item__c>();
    List<Id> ordIDs_Rejlist = new List<Id>();
    Boolean checkTask1 = false;
    Boolean checkTask = false;
    Set<Id> OrderSubmittedSet = new Set<Id>();
    Set<Id> OrderRejectedSet = new Set<Id>();
    for(Order__c ord: Trigger.new) {
        system.debug('Owner' +ord.OwnerId);
        system.debug('PANDA'+ord.Is_Order_Submitted__c);
        //added as change
        if(ord.SD_PM_Contact__c != null){
            ordIDs_list.add(ord.id);
        }
        System.debug('Countreject----' + ord.Count_of_rejected_line_item__c);
        if(ord.Is_reject_button_clicked__c && ord.Status__c!='Reject' && ord.Count_of_rejected_line_item__c !=0){
            ordIDs_Rejlist.add(ord.id);
            
        }
        //ends here
        
        // Condition Added to check all Orders which are submitted
        System.debug('Calling the before addition class');
        //Added checkTask flag to true and checking false while entering the loop. (Defect 7532)-- Narsi
        if (Ord.Is_Order_Submitted__c && ! Trigger.oldMap.get(Ord.Id). Is_Order_Submitted__c && checkTask == false)
        {
            System.debug('Inside addition');
            OrderSubmittedSet.add(Ord.Id);
            checkTask = true; //added part of  (Defect 7532)-- Narsi
        }
        //PSM Change to call the scheduler
        System.debug('Calling the Notifier');
        if(OrderSubmittedSet.size()>0 && checkTask){   //added part of  (Defect 7532)-- Narsi
            OrderEmailNotifier ordEmN= new  OrderEmailNotifier();
            ordEmN.notifyOrder(OrderSubmittedSet);
        }
        system.debug('PandaDebug1'+Util.skipOrderAfterUpdateFlag+Util.orderSubmitFlag);
        if( Util.skipOrderAfterUpdateFlag==false) return;
        /*  if (!Ord.Is_Order_Submitted__c && ((ord.Status__c== 'Reject' &&  Trigger.oldMap.get(Ord.Id). Status__c!='Reject') || (ord.Is_reject_button_clicked__c &&  !Trigger.oldMap.get(Ord.Id). Is_reject_button_clicked__c)) && checkTask1 == false)
{
System.debug('Inside addition');
OrderRejectedSet.add(Ord.Id);
checkTask1 = true; //added part of  (Defect 7532)-- Narsi
}
//PSM Change to call the scheduler
System.debug('Calling the Notifier');
if(OrderRejectedSet.size()>0 && checkTask1){  
OrderEmailNotifier ordEmN1= new  OrderEmailNotifier();
ordEmN1.notifyOrder(OrderRejectedSet); //change
}*/
       /* if(ord.SD_PM_Contact__c != null && Trigger.oldMap.get(ord.Id).SD_PM_Contact__c == null && ord.OrderType__c !=0){
            checkTask = true;
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Acknowledgement Email',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Acknowledgement Email'));
            
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Regular/Weekly Status Update Email',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Regular/Weekly Status Update Email'));
        }
        if(ord.Is_Order_Submitted__c && !Trigger.oldMap.get(ord.Id).Is_Order_Submitted__c && !ord.Is_In_Flight__c && ord.SD_PM_Contact__c != null && ord.OrderType__c !=0) {        
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Test Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Test Letter'));
            
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Activation Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Activation Letter'));
            
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Welcome Pack',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Welcome Pack'));
            
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Billing Date Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Billing Date Letter'));
            
            newTask.add(new Task(OwnerId = ord.SD_PM_Contact__c,
                                 Subject = 'Intimation to Billing/SA Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Intimation to Billing/SA Letter'));                           
        }
        if(ord.Is_Order_Submitted__c && !Trigger.oldMap.get(ord.Id).Is_Order_Submitted__c && !ord.Is_In_Flight__c && ord.SD_PM_Contact__c == null && ord.OrderType__c !=0) {        
            newTask.add(new Task(OwnerId = ord.OwnerId,
                                 Subject = 'Test Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Test Letter'));
            
            newTask.add(new Task(OwnerId = ord.OwnerId,
                                 Subject = 'Activation Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Activation Letter'));
            
            newTask.add(new Task(OwnerId = ord.OwnerId,
                                 Subject = 'Welcome Pack',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Welcome Pack'));
            
            newTask.add(new Task(OwnerId = ord.OwnerId,
                                 Subject = 'Billing Date Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Billing Date Letter'));
            
            newTask.add(new Task(OwnerId = ord.OwnerId,
                                 Subject = 'Intimation to Billing/SA Letter',
                                 Status = 'Not Started',
                                 Priority = 'High',
                                 IsReminderSet = true,
                                 ReminderDateTime = System.now() + 5,
                                 WhatId = ord.Id,
                                 Description = 'Please Send Intimation to Billing/SA Letter'));                           
        }
    }
    /*IR088*/
  /*  Set<Id> queueIdSet=new Set<Id>();
    List<Action_Item__c> AIList=null;
    List<Action_Item__c> actionItemList=null;
    Map<Id,List<Action_Item__c>> mapQueueIdAIList=new Map<Id,List<Action_Item__c>>();
    Map<Id,Name> mapQIdName=new Map<Id,Name>();
    if(trigger.new[0].Status__c=='Cancelled'){
        
        actionItemList=[select id,Name,Ownerid,Service__c,Status__c,Resource__c,Service__r.Name,Resource__r.Name,Order_Action_Item__r.CreatedBy.Name,Order_Action_Item__r.Name,Order_Action_Item__r.CreatedBy.Email,Owner.Name from Action_Item__c where Order_Action_Item__c=:trigger.new[0].Id];
        for(Action_Item__c aiObj:actionItemList){
            if(aiObj.Status__c!='Completed') {
                queueIdSet.add(aiObj.Ownerid);  
            } 
            
        }
        
        for(Id qId:queueIdSet){
            AIList=new List<Action_Item__c>();
            for(Action_Item__c aiObj:actionItemList){
                if(aiObj.Ownerid==qId && aiObj.Status__c!='Completed'){
                    AIList.add(aiObj);
                    
                }
                mapQueueIdAIList.put(qId,AIList);
            }
            
            
            
        }
        for(Action_Item__c aiObj:actionItemList){
            
            aiObj.status__c='Cancelled';
        }
        update actionItemList;
        system.debug('===mapQueueIdAIList=='+mapQueueIdAIList);
        EmailGenerationClass em=new EmailGenerationClass();
        em.getEmailList(mapQueueIdAIList);
    }
    /*IR088 */
    
    //Cr111 Order Rejection
    /*system.debug('======trigger.new[0].Is_reject_button_clicked__c'+trigger.new[0].Is_reject_button_clicked__c);
    system.debug('======trigger.old[0].Is_reject_button_clicked__c'+trigger.old[0].Is_reject_button_clicked__c);
    if(trigger.new[0].Is_reject_button_clicked__c!=trigger.old[0].Is_reject_button_clicked__c && trigger.new[0].Is_reject_button_clicked__c && Util.orderRejectionFlag){
        
        Util.orderRejectionFlag=false;
        
    }
    
    if(trigger.new[0].Status__c!=trigger.old[0].Status__c && trigger.new[0].Status__c=='Cancelled' && Util.orderCancelationFlag){
        
        Util.orderCancelationFlag=false;
        
    }
    //CR111 Order Rejection
    
    //if reject list is not empty update list updOliLst1 : IR111 : Anshu
    if(ordIDs_Rejlist.size()>0){
        Map<Id,Order__c> idOrder1_Map = Trigger.newMap;
        
        List<Order_Line_Item__c> oliObj_list1 = [ Select id,Name,Line_Item_Status__c,Reason_for_Rejection__c,Additional_Information_for_Rejection__c,ParentOrder__r.Account_Owner__c,AccountId__c,ParentOrder__c,OrderType__c,ParentOrder__r.Name,Product_Name__c from Order_Line_Item__c where ParentOrder__c in: ordIDs_Rejlist];
        
        
        
        for(Order_Line_Item__c olilst1:oliObj_list1){
            updOliLst1.add(olilst1);
            
        }
    }
    //added as change
    // if reject list is empty then only query will run : IR111 Anshu
    if(ordIDs_list.size()>0 && trigger.new[0].Count_of_rejected_line_item__c==0){
        Map<Id,Order__c> idOrder_Map = Trigger.newMap;
        /*IR088-Manage Terminate Order:Suparna:Added Requested_Termination_Date__c to the query*/
        /*List<Order_Line_Item__c> oliObj_list = [ Select id,IsStatusChanged__c,Line_Item_Status__c,AccountName__c,Stop_Billing_Date__c,ParentOrder__r.Account_Owner__c,Name,Primary_Service_ID__c,Master_Service_ID__c,Path_Id__c,Path_Status__c,AccountId__c,Customer_Required_Date__c,Termination_Reason__c,ParentOrder__c,OrderType__c,SD_PM_Contact__c,Bundle_Flag__c,Parent_of_the_bundle_flag__c, Parent_Customer_PO__c from Order_Line_Item__c where ParentOrder__c in: ordIDs_list limit 10];
        List<Task> taskObj_List = [select id,subject,WhatId from Task where WhatId in:idOrder_Map.keyset()];
        Boolean checkDecommSubFlag = false;
        Boolean checkAckSubFlag = false;
        // Added as part of CR 533
        
        for(Order_Line_Item__c olilst:oliObj_list){
            if(((idOrder_Map.get(olilst.ParentOrder__c).Customer_PO__c) != null) || ((idOrder_Map.get(olilst.ParentOrder__c).Requested_Termination_Date__c) != null) || ((idOrder_Map.get(olilst.ParentOrder__c).Termination_Reason__c) != null)){
                olilst.Parent_Customer_PO__c = (idOrder_Map.get(olilst.ParentOrder__c)).Customer_PO__c;
                /*Added this statement to cascade requested Termination date to it's child line items*/
                //olilst.Customer_Required_Date__c = (idOrder_Map.get(olilst.ParentOrder__c)).Requested_Termination_Date__c;
                //olilst.Termination_Reason__c = (idOrder_Map.get(olilst.ParentOrder__c)).Termination_Reason__c;
                //system.debug('TESR%%%%'+(idOrder_Map.get(olilst.ParentOrder__c)).Customer_PO__c );
                //updOliLst.add(olilst);
            //}
        //}
        
        
        
        /*for(integer i = 0; i < oliObj_list.size(); i++){
            
            if(oliObj_list[i].SD_PM_Contact__c != null && (oliObj_list[i].OrderType__c == 'Cancel' || oliObj_list[i].OrderType__c == 'Terminate')){
                for( integer j = 0; j<taskObj_List.size(); j++){
                    if(taskObj_List[j].WhatId == oliObj_list[i].ParentOrder__c){
                        
                        if(taskObj_List[j].subject == 'Decommissioning/Termination Letter'){
                            checkDecommSubFlag = true;
                            
                        }
                        if(taskObj_List[j].subject == 'Acknowledgement Email'){
                            checkAckSubFlag  = true;
                        }
                    }
                    
                } 
                if(!checkDecommSubFlag){
                    checkDecommSubFlag = true;
                    
                    newTask.add(new Task(OwnerId = idOrder_Map.get(oliObj_list[i].ParentOrder__c).SD_PM_Contact__c,
                                         Subject = 'Decommissioning/Termination Letter',
                                         Status = 'Not Started',
                                         Priority = 'High',
                                         IsReminderSet = true,
                                         ReminderDateTime = System.now() + 5,
                                         WhatId = idOrder_Map.get(oliObj_list[i].ParentOrder__c).Id,
                                         Description = 'Please Send Decommissioning/Termination Letter'));
                    if(!checkTask && !checkAckSubFlag){
                        newTask.add(new Task(OwnerId = idOrder_Map.get(oliObj_list[i].ParentOrder__c).SD_PM_Contact__c,
                                             Subject = 'Acknowledgement Email',
                                             Status = 'Not Started',
                                             Priority = 'High',
                                             IsReminderSet = true,
                                             ReminderDateTime = System.now() + 5,
                                             WhatId = idOrder_Map.get(oliObj_list[i].ParentOrder__c).Id,
                                             Description = 'Please Send Acknowledgement Email'));
                    }
                    
                }
                
            }
        }
        
        
        
    }
    
    //ends here
    
    
    try {
        if(!newTask.isEmpty())insert newTask;
    } 
    catch(Exception e) {
        CreateApexErrorLog.insertHandleException('Trigger','OrderAfterUpdate',e.getMessage(),'Order',UserInfo.getUserName());
    }
    /*  Set<Id> ordList = new Set<Id>();
for (Order__c o: Trigger.new){
if(o.Status__c == 'Complete')
ordList.add(o.Id);
}
List<Order_Line_Item__c> updOliLst = new List<Order_Line_Item__c>();

for(Order_Line_Item__c oli : [SELECT Path_Instance_Id__c, Work_Order_Instance_Id__c FROM Order_Line_Item__c WHERE ParentOrder__c IN: ordList]){

if(oli.Path_Instance_Id__c!=null|| oli.Path_Instance_Id__c!=''||oli.Work_Order_Instance_Id__c!=null||oli.Work_Order_Instance_Id__c!=''){
oli.Path_Instance_Id__c = null;
oli.Work_Order_Instance_Id__c = null;
updOliLst.add(oli);
}
}  */
    
    //Added as part Of CR313..
    //OrderActionItemGenerateClass.createActionItem(Trigger.newMap,Trigger.oldMap);
    //Added as part Of CR533  
    // if reject list is empty then only query will run : IR111 Anshu  
    /*if(!updOliLst.isEmpty() && trigger.new[0].Status__c!='Complete'&& trigger.new[0].Status__c!='Reject' && trigger.new[0].Count_of_rejected_line_item__c==0){
        Util.orderLineItemUpdateFlag=false;
        if(trigger.new[0].Customer_PO__c!=trigger.old[0].Customer_PO__c || trigger.new[0].Requested_Termination_Date__c!=trigger.old[0].Requested_Termination_Date__c){
            if(checkRecursiveOrderlineitem1.runthree()){  
                update updOliLst;
            }
        }
    }
    // if reject list is having value then only query will run : IR111 Anshu
    //Commented as it's not required for IR111
    /*  if(!updOliLst1.isEmpty() && trigger.new[0].Count_of_rejected_line_item__c!=0){
update updOliLst1;
}*/
    //*Boolean statusCheckFlag=false;
   /* system.debug('=======updOliLst======'+updOliLst);
    for(Order_Line_item__c oli:updOliLst){
        
        if(oli.Line_Item_Status__c!='New'){
            statusCheckFlag=true;
            break;
        }
        
        
    }
    for(Order_Line_item__c oli:updOliLst){
        
        if(oli.IsStatusChanged__c){
            statusCheckFlag=true;
            break;
            
        }
    }
    for(Order__c odr:trigger.new){
        system.debug('======statusCheckFlag==='+statusCheckFlag);
        system.debug('condition===1'+odr.Full_Termination_Order__c + odr.Is_Order_Submitted__c + updOliLst.size() + odr.Requested_Termination_Date__c + odr.Status__c);
        system.debug('condition===2'+odr.Full_Termination_Order__c + odr.Is_Order_Submitted__c + updOliLst.size() + odr.Status__c + odr.Requested_Termination_Date__c);
        if((odr.Full_Termination_Order__c && odr.Is_Order_Submitted__c && updOliLst.size()>0 && odr.Requested_Termination_Date__c!=null && odr.Status__c!='Complete' && !statusCheckFlag) || (odr.Full_Termination_Order__c && odr.Is_Order_Submitted__c && updOliLst.size()>0 && odr.Status__c=='Complete' && odr.Requested_Termination_Date__c!=null )){
            system.debug('getemaillistentry');
            getEmailList(updOliLst,odr);
            system.debug('getemaillistentryy');
            break;
        }
    }
    for(Order__c odr1:trigger.new){
        system.debug('trueorfalse======'+(trigger.new[0].Is_reject_button_clicked__c!=Trigger.oldMap.get(Odr1.Id).Is_reject_button_clicked__c));
        if(odr1.Is_reject_button_clicked__c && odr1.Status__c!='Reject' & odr1.Count_of_rejected_line_item__c !=0 && (trigger.new[0].Is_reject_button_clicked__c !=Trigger.oldMap.get(Odr1.Id).Is_reject_button_clicked__c)){
            getEmailList1(updOliLst1);
        }
        else if(odr1.Is_reject_button_clicked__c && odr1.Status__c=='Reject' && (trigger.new[0].Is_reject_button_clicked__c !=Trigger.oldMap.get(Odr1.Id).Is_reject_button_clicked__c)){
            insertDynDataInBody2();
        }
        // added as part of order update email :IR111 :Anshu
        if(odr1.Updated_Order__c && (((trigger.new[0].Updated_Order__c !=Trigger.oldMap.get(Odr1.Id).Updated_Order__c) && (trigger.new[0].Status__c=='New' && Trigger.oldMap.get(Odr1.Id).Status__c=='Reject'))||(trigger.new[0].Count_of_rejected_line_item__c==0 && Trigger.oldMap.get(Odr1.Id).Count_of_rejected_line_item__c!=0))){
            insertDynDataInBody3();
            System.debug('queuename'+trigger.new[0].Owner.Name);
        }   
    }
    System.debug('OrderAfterUpdate Ending used in the Apex Trigger: ' + Limits.getQueries());
    /*IR088-Manage Terminate Order/Email Notification to be sent to the billing team on Order Submittion/Completion/Suparna*/
    
    
    
    //getEmailList(updOliLst);
   /* public void insertDynDataInBody(String body,boolean flag){
        try{
            EMailVO prEMailVO= new EMailVO();
            system.debug('===flag==='+flag);
            prEMailVO.to=new String[]{'suparna.sikdar@accenture.com'};
                prEMailVO.senderDispName=trigger.new[0].Account_Owner__c;
            prEMailVO.subject='Termination Order'+(flag?' Completed-':' Created-')+trigger.new[0].Name+' for this Account-'+trigger.new[0].Account_Name__c;
            String body1 = 'Dear'+' '+'Billing -'+trigger.new[0].Billing_Region__c+','+'</br>'+'</br>';
            
            String body2 = 'This is just for your information following terminate order is '+(flag?'completed.':'created and will be sent downstream to the SubexROC billing system.')+'</br>';
            String body3=URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Id+'</br></br>';
            prEMailVO.body=body1+body2+body3+'Please see below the Account Detail and Line Item Details of this Terminate Order.'+'</br>'+'</br>'+body;
            if(prEMailVO.subject!=null && prEMailVO.subject.contains('Completed') && trigger.new[0].Status__c=='Complete'){
                sendMail(prEMailVO);
            }else if(flag==false && !statusCheckFlag){
                sendMail(prEMailVO);    
                
            }
        }catch (Exception ex) {
            System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex);
        }
    }
    public  Boolean sendMail(EMailVO prEMailVO){
        Boolean mailSent=false;
        try{
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            //mail.setReplyTo(prEMailVO.sender);
            // mail.setSenderDisplayName(prEMailVO.senderDispName);
            
            String[] addressArr=system.Label.RFQ_Team_Group_Id.split(',',0);
            String[] toAddresses = getEmailAddess().split(':', 0);
            
            system.debug('to addresses '+toAddresses );
            
            mail.setToAddresses(toAddresses);
            mail.setSubject(prEMailVO.subject);
            mail.setHtmlBody(prEMailVO.body);
            /* If(prmEMailVO.sender!=null)
{
OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: prmEMailVO.sender];
if ( owea.size() > 0 ) {
mail.setOrgWideEmailAddressId(owea.get(0).Id);
}
}
*/
            /*List<Messaging.SendEmailResult> results= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            mailSent=results.get(0).isSuccess();
            if(!mailSent){
                System.Debug('Errors while sending Mail ..'+results.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent;
    }
    public  Boolean sendMail1(EMailVO1 prEMailVO1){
        Boolean mailSent1=false;
        try{
            Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
            //mail.setReplyTo(prEMailVO.sender);
            //mail.setSenderDisplayName(prmEMailVO.senderDispName);
            system.debug('====System.Label.RFQ_Team_Group_Id===='+System.Label.RFQ_Team_Group_Id);
            String[] addressArr1=system.Label.RFQ_Team_Group_Id.split(',',0);
            String[] toAddresses1 = getEmailAddess().split(':', 0);
            //String[] toAddresses = ('durgaprasad.ayinala@accenture.com');
            system.debug('to addresses '+toAddresses1 );
            
            system.debug('======RFQ Team Group Id====='+addressArr1);
            mail1.setToAddresses(toAddresses1);
            mail1.setSubject(prEMailVO1.subject1);
            mail1.setHtmlBody(prEMailVO1.bodys);
            /* If(prmEMailVO.sender!=null)
{
OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: prmEMailVO.sender];
if ( owea.size() > 0 ) {
mail.setOrgWideEmailAddressId(owea.get(0).Id);
}
}
*/
            /*List<Messaging.SendEmailResult> results1= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
            mailSent1=results1.get(0).isSuccess();
            if(!mailSent1){
                System.Debug('Errors while sending Mail ..'+results1.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent1;
    }
    public  Boolean sendMail2(EMailVO2 prEMailVO2){
        Boolean mailSent1=false;
        try{
            Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
            system.debug('====System.Label.RFQ_Team_Group_Id===='+System.Label.RFQ_Team_Group_Id);
            String[] addressArr1=system.Label.RFQ_Team_Group_Id.split(',',0);
            String[] toAddresses1 = getEmailAddess().split(':', 0);
            //String[] toAddresses = ('durgaprasad.ayinala@accenture.com');
            system.debug('to addresses '+toAddresses1 );
            
            system.debug('======RFQ Team Group Id====='+addressArr1);
            mail1.setToAddresses(toAddresses1);
            mail1.setSubject(prEMailVO2.subject1);
            mail1.setHtmlBody(prEMailVO2.bodys);
            /* If(prmEMailVO.sender!=null)
{
OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: prmEMailVO.sender];
if ( owea.size() > 0 ) {
mail.setOrgWideEmailAddressId(owea.get(0).Id);
}
}
*/
            /*List<Messaging.SendEmailResult> results1= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
            mailSent1=results1.get(0).isSuccess();
            if(!mailSent1){
                System.Debug('Errors while sending Mail ..'+results1.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent1;
    }
    //added as part of order update email :IR111 :Anshu
    public  Boolean sendMail3(EMailVO2 prEMailVO2){
        Boolean mailSent1=false;
        try{
            Messaging.SingleEmailMessage mail1 = new Messaging.SingleEmailMessage();
            system.debug('====System.Label.RFQ_Team_Group_Id===='+System.Label.RFQ_Team_Group_Id);
            String[] addressArr1=system.Label.RFQ_Team_Group_Id.split(',',0);
            String[] toAddresses1 = getEmailAddessforupdateorder().split(':', 0);
            //String[] toAddresses = ('durgaprasad.ayinala@accenture.com');
            system.debug('to addresses '+toAddresses1 );
            
            system.debug('======RFQ Team Group Id====='+addressArr1);
            mail1.setToAddresses(toAddresses1);
            mail1.setSubject(prEMailVO2.subject1);
            mail1.setHtmlBody(prEMailVO2.bodys);
            /* If(prmEMailVO.sender!=null)
{
OrgWideEmailAddress[] owea = [select Id from OrgWideEmailAddress where Address =: prmEMailVO.sender];
if ( owea.size() > 0 ) {
mail.setOrgWideEmailAddressId(owea.get(0).Id);
}
}
*/
           /* List<Messaging.SendEmailResult> results1= Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail1 });
            mailSent1=results1.get(0).isSuccess();
            if(!mailSent1){
                System.Debug('Errors while sending Mail ..'+results1.get(0).getErrors()[0] );
            }
        }catch(Exception ex){
            System.Debug('Errors while sending Mail ..'+ex );
        }
        return mailSent1;
    }
    
    private Id getFullOrderTerminationRecordTypeId() {
        return RecordTypeUtil.getRecordTypeIdByName(Order__c.sObjectType,'Full Order Termination');
    }
    
    public void insertDynDataInBody1(String bodys){
        List<Order__c> ordername =  [select CreatedBy.Name,Opportunity__r.Owner.Name,LastModifiedBy.Name from Order__c where id =:trigger.new[0].id];
        
        for (Order__c ordernam : ordername){        
            try{
                EMailVO1 prEMailVO1= new EMailVO1();
                prEMailVO1.to1=new String[]{'anshu.ayushya@accenture.com'};
                    prEMailVO1.subject1='Order ' +trigger.new[0].Name+ ' is rejected and is not ready for submission.';
                
                if (getFullOrderTerminationRecordTypeId() == trigger.new[0].RecordTypeId) {
                    String bodys1 = 'Dear'+' '+ordernam.CreatedBy.Name+' '+','+'</br></br>';         
                    String bodys2 =  'Service Delivery ('+ordernam.LastModifiedBy.Name+') has confirmed that one or more Order Line Items, from Order ' + trigger.new[0].Name+ ', are not complete and require additional information.'+'</br>'+'</br>';
                    String bodys3 ='Please review the Order by clicking the link below.'+'</br>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].id+'</br></br>';
                    prEMailVO1.bodys=bodys1+bodys2+bodys3+'The details for rejection are given below:'+'</br>'+'</br>'+bodys;
                } else {
                    String bodys1 = 'Dear'+' '+ordernam.Opportunity__r.Owner.Name+' '+','+'</br></br>';         
                    String bodys2 =  'Service Delivery ('+ordernam.LastModifiedBy.Name+') has confirmed that one or more Order Line Items, from Order ' + trigger.new[0].Name+ ', are not complete and require additional information.'+'</br>'+'</br>';
                    String bodys3 ='Please review the Opportunity by clicking the link below.'+'</br>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Opportunity__c+'</br></br>';
                    prEMailVO1.bodys=bodys1+bodys2+bodys3+'The details for rejection are given below:'+'</br>'+'</br>'+bodys;
                }
                
                sendMail1(prEMailVO1);
                
            } catch (Exception ex1) {
                System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex1);
            }
        }  
    }
    
    public void insertDynDataInBody2(){
        List<Order__c> ordername1 =  [select CreatedBy.Name,Opportunity__r.Owner.Name,LastModifiedBy.Name from Order__c where id =:trigger.new[0].id]    ;
        
        for (Order__c ordernam1 : ordername1) {
            try{
                EMailVO2 prEMailVO2= new EMailVO2();
                
                prEMailVO2.to1=new String[]{'anshu.ayushya@accenture.com'};
                    prEMailVO2.subject1='Order ' +trigger.new[0].Name+ ' is rejected and is not ready for submission.';
                //String bodys1 = 'Dear'+' '+trigger.new[0].Opportunity_Owner__c+' '+','+'</br></br>';
                if (getFullOrderTerminationRecordTypeId() == trigger.new[0].RecordTypeId) {
                    String bodys1 = 'Hi'+' '+ordernam1.CreatedBy.Name+' '+','+'</br></br>';      
                    String bodys2 = 'Service Delivery ('+ordernam1.LastModifiedBy.Name+') has confirmed that Order ' + trigger.new[0].Name+ ' is not complete and requires additional information.'+'</br>'+'The reason provided by Service Delivery is '+trigger.new[0].Reason_for_Rejection__c+(trigger.new[0].Additional_Information_for_Rejection__c==null?'':', ')+(trigger.new[0].Additional_Information_for_Rejection__c==null?'':trigger.new[0].Additional_Information_for_Rejection__c)+'.'+'</br>'+'</br>';
                    String bodys3 ='Please review the Order by clicking the link below.'+'</br>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].id+'</br></br>';
                    prEMailVO2.bodys=bodys1+bodys2+bodys3;
                } else {
                    String bodys1 = 'Hi'+' '+ordernam1.Opportunity__r.Owner.Name+' '+','+'</br></br>';      
                    String bodys2 = 'Service Delivery ('+ordernam1.LastModifiedBy.Name+') has confirmed that Order ' + trigger.new[0].Name+ ' is not complete and requires additional information.'+'</br>'+'The reason provided by Service Delivery is '+trigger.new[0].Reason_for_Rejection__c+(trigger.new[0].Additional_Information_for_Rejection__c==null?'':', ')+(trigger.new[0].Additional_Information_for_Rejection__c==null?'':trigger.new[0].Additional_Information_for_Rejection__c)+'.'+'</br>'+'</br>';
                    String bodys3 ='Please review the Opportunity by clicking the link below.'+'</br>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].Opportunity__c+'</br></br>';
                    prEMailVO2.bodys=bodys1+bodys2+bodys3;
                }
                
                sendMail2(prEMailVO2);
                
            } catch (Exception ex1) {
                System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex1);
            }
        }
    }
    //Added as part of order update email :IR111 :Anshu
    public void insertDynDataInBody3(){
        List<Order__c> ordername1 =  [select CreatedBy.Name,Opportunity__r.Owner.Name,LastModifiedBy.Name from Order__c where id =:trigger.new[0].id]    ;
        
        for (Order__c ordernam1 : ordername1){
            try{
                EMailVO2 prEMailVO2= new EMailVO2();
                
                prEMailVO2.to1=new String[]{'anshu.ayushya@accenture.com'};
                    prEMailVO2.subject1='Order ' +trigger.new[0].Name+ ', which was previously rejected, has been updated by Sales. ';
                
                String bodys1 = 'Hi'+' '+'Service delivery Team'+' '+','+'</br></br>';      
                String bodys2 = 'Order'+ trigger.new[0].Name+', which was previously rejected, has been updated by Sales'+'.'+'</br>'+'</br>';
                String bodys3 ='Please review the Order by clicking the link below.'+'</br>'+URL.getSalesforceBaseUrl().toExternalForm() + '/'+trigger.new[0].id+'</br></br>';
                prEMailVO2.bodys=bodys1+bodys2+bodys3;
                
                sendMail3(prEMailVO2);
                
            } catch (Exception ex1) {
                System.Debug('Error in inserting Dynamic data in EmailBody ..'+ex1);
            }
        }
    }
    
    public void getEmailList(List<Order_Line_Item__c> oliObj_list,Order__c odr){
        
        List<String> dynDatas = new List<String>();
        boolean flag=false;
        
        //get list of all the ordered items
        string htmlOrderedList='';
        string htmlOrderedList1='';
        string htmlOrderedList2='';
        string htmlOrderedList3='';
        string htmlOrderedList4='';
        htmlOrderedList1='<style type="text/css">'+
            '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
            '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
            '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
            '</style>';
        htmlOrderedList2='<table class="tg">'+
            '<tr>'+
            '<th class="tg-031e"><strong>Account Name</strong></th>'+
            '<th class="tg-031e"><strong>Account ID</strong></th>'+
            '<th class="tg-031e"><strong>Account Owner</strong></th>'+
            '</tr>'+
            '<tr>'+
            '<td class="tg-031e">'+oliObj_list[0].AccountName__c+'</td>'
            +'<td class="tg-031e">'+oliObj_list[0].AccountId__c+'</td>'+
            '<td class="tg-031e">'+oliObj_list[0].ParentOrder__r.Account_Owner__c+'</td>'+
            '</tr>'+
            '</table>';
        if(odr.Status__c=='Complete'){
            
            htmlOrderedList3='<table class="tg">'+
                '<tr>'+
                '<th class="tg-031e"><strong>Order Line Item Name</strong></th>'+
                '<th class="tg-031e"><strong>Master Service ID</strong></th>'+
                '<th class="tg-031e"><strong>Primary Service ID</strong></th>'+
                '<th class="tg-031e"><strong>Resource ID</strong></th>'+
                '<th class="tg-031e"><strong>Stop Billing Date</strong></th>'+
                '</tr>';
        }else{
            htmlOrderedList3='<table class="tg">'+
                '<tr>'+
                '<th class="tg-031e"><strong>Order Line Item Name</strong></th>'+
                '<th class="tg-031e"><strong>Master Service ID</strong></th>'+
                '<th class="tg-031e"><strong>Primary Service ID</strong></th>'+
                '<th class="tg-031e"><strong>Resource ID</strong></th>'+
                '<th class="tg-031e"><strong>Customer Requested Termination Date</strong></th>'+
                '</tr>';    
            
            
        }
        for(Order_Line_Item__c oliobj:oliObj_list)
        {
            if(oliobj.Stop_Billing_Date__c!=null){
                flag=true;
                htmlOrderedList4+='<tr>'+
                    '<td class="tg-031e">'+oliobj.Name+'</td>'
                    +'<td class="tg-031e">'+(oliobj.Master_Service_ID__c==null?'N/A':oliobj.Master_Service_ID__c)+'</td>'
                    +'<td class="tg-031e">'+(oliobj.Primary_Service_ID__c==null?'N/A':oliobj.Primary_Service_ID__c)+'</td>'
                    
                    +'<td class="tg-031e">'+(oliobj.Path_Id__c==null?'N/A':oliobj.Path_Id__c)+'</td>'
                    
                    +'<td class="tg-031e">'+(oliobj.Stop_Billing_Date__c.format()==null?'N/A':oliobj.Stop_Billing_Date__c.format())+'</td>'
                    +'</tr>';
                
            }
            else{
                flag=false;
                htmlOrderedList4+='<tr>'+
                    '<td class="tg-031e">'+oliobj.Name+'</td>'
                    +'<td class="tg-031e">'+(oliobj.Master_Service_ID__c==null?'N/A':oliobj.Master_Service_ID__c)+'</td>'
                    +'<td class="tg-031e">'+(oliobj.Primary_Service_ID__c==null?'N/A':oliobj.Primary_Service_ID__c)+'</td>'
                    
                    +'<td class="tg-031e">'+(oliobj.Path_Id__c==null?'N/A':oliobj.Path_Id__c)+'</td>'
                    +'<td class="tg-031e">'+(oliobj.Customer_Required_Date__c.date().format()==null?'N/A':oliobj.Customer_Required_Date__c.date().format())+'</td>'
                    +'</tr>';
                
                system.debug('I am here also Flag===='+flag);    
                
            }
        }
        String htmlOrderedList5='</table>';
        
        
        
        system.debug('I am here Flag===='+flag);
        
        //insert dynamic data in email html body
        system.debug('insertdyndatainbodyentry');
        insertDynDataInBody(htmlOrderedList1+htmlOrderedList+htmlOrderedList2+'</br>'+htmlOrderedList3+htmlOrderedList4+htmlOrderedList5+'Thanks,'+'</br>'+'***This is an auto-generated notification, please do not reply to this email***.',flag);
        system.debug('insertdyndatainbodyentryy');
        //send email
        
        
    }
    public void getEmailList1(List<Order_Line_Item__c> oliObj_list1){
        
        List<String> dynDatas1 = new List<String>();
        
        
        //get list of all the ordered items
        string htmlOrderList='';
        string htmlOrderList1='';
        string htmlOrderList2='';
        string htmlOrderList3='';
        
        htmlOrderList1='<style type="text/css">'+
            '.tg  {border-collapse:collapse;border-spacing:0;border-color:#000000;}'+
            '.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#444;background-color:#F7FDFA;}'+
            '.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#999;color:#fff;background-color:#26ADE4;}'+
            '</style>';
        
        
        
        htmlOrderList2='<table class="tg">'+
            '<tr>'+
            '<th class="tg-031e"><strong>Order Line Item No.</strong></th>'+
            '<th class="tg-031e"><strong>Product</strong></th>'+
            '<th class="tg-031e"><strong>Rejected?</strong></th>'+
            '<th class="tg-031e"><strong>Reason for Rejection</strong></th>'+
            '<th class="tg-031e"><strong>Additional Comments from SD team</strong></th>'+'</tr>';
        
        for(Order_Line_Item__c oliobj1:oliObj_list1)
        {
            if(oliobj1.Line_Item_Status__c=='Reject'){
                
                htmlOrderList3+='<tr>'+
                    '<td class="tg-031e">'+oliobj1.Name+'</td>'
                    +'<td class="tg-031e">'+oliobj1.Product_Name__c+'</td>'
                    +'<td class="tg-031e">'+(oliobj1.Line_Item_Status__c=='Reject'?'Yes':'No')+'</td>'
                    +'<td class="tg-031e">'+oliobj1.Reason_for_Rejection__c+'</td>'
                    +'<td class="tg-031e">'+(oliobj1.Additional_Information_for_Rejection__c==null?'N/A':oliobj1.Additional_Information_for_Rejection__c)+'</td>'
                    +'</tr>';
                
            }
            
        }
        String htmlOrderList4 ='</table>';
        
        
        
        
        
        //insert dynamic data in email html body
        insertDynDataInBody1(htmlOrderList1+htmlOrderList+htmlOrderList2+htmlOrderList3+htmlOrderList4);
        //send email
        
        
    }
    public class EmailVO{
        public String sender{get;set;}
        public String senderDispName{get;set;}
        public String[] to{get;set;}
        public String[] cc{get;set;}
        public String[] bcc{get;set;}
        public String subject{get;set;}
        public String body{get;set;}
        public String header{get;set;}
        public String footer{get;set;}
        public String module{get;set;}
        public String originator{get;set;}
        public String replaceCharStart{get;set;}
        public String replaceCharEnd{get;set;}
        public Boolean isCIC{get;set;}
    }
    public class EmailVO1{
        
        public String[] to1{get;set;}
        
        public String subject1{get;set;}
        public String bodys{get;set;}
        
    }
    public class EmailVO2{
        
        public String[] to1{get;set;}
        
        public String subject1{get;set;}
        public String bodys{get;set;}
        
    }
    //added as part of Order Update email: IR111 : Anshu
    public String getEmailAddessforupdateorder(){
        // query to get all queues corresponding to Order Object
        List<QueueSobject> que = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='Order__c'];
        Map<String, Id> queMap= new Map<String, Id> ();           
        //Query the User object  to get sales User who created the Order
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        Id queid;
        String billTeam;
        String emailAddresses = '';
        String USerRegion='';
        //Setting for email attributes
        
        String subject;
        String body1;
        String body2;
        String body;
        String subject1;
        String bodys;
        String bodys2;
        String bodys1;
        
        //ends here
        System.debug('Is button clicked '+trigger.new[0].Is_reject_button_clicked__c+trigger.new[0].Opportunity_Owner_Email__c+trigger.new[0].Createdby_Email__c);
        
        //Get Billing Team Queue id corresponding to sales user region.
        for (QueueSObject q:que){
            
            
            if((q.Queue.Name).contains('Service Delivery OM - '+usr.Region__c)){
                queMap.put(q.Queue.Name,q.QueueId);
                queid = q.QueueId;
                billTeam = q.Queue.Name;
                // System.debug('queue name ..'+q.Queue.Name);
            }
            
        }
        
        String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
        
        //Getting the all Users id's  corresponding to queue
        List<GroupMember> list_GM = Database.query(groupMemberQuery);
        
        List<Id> lst_Ids = new List<ID>();
        for(GroupMember gmObj : list_GM){
            lst_Ids.add(gmObj.UserOrGroupId);
        }
        
        //Query the Billing Team Users 
        List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
        
        //Here concanating all email addresses of Users belong to Billing Queue
        for(User usrObj : lst_UserObj){
            if(emailAddresses == ''){
                emailAddresses = usrObj.Email;
                USerRegion=usrObj.Region__c;
            }else{
                emailAddresses += ':'+usrObj.Email; 
                USerRegion=usrObj.Region__c;
            }
        }
        System.debug('emailadresses..'+emailAddresses);
        return emailAddresses;
        
    }
    public String getEmailAddess(){
        // query to get all queues corresponding to Bill profile Object
        List<QueueSobject> que = [Select Id,QueueId, Queue.Name from QueueSobject where SobjectType='BillProfile__c'];
        
        //Query the User object  to get sales User who created the Order
        List<User> usrLst =[Select Name, Region__c from User where id= :UserInfo.getUserId()];
        User usr = usrLst[0];
        Id queid;
        String billTeam;
        String emailAddresses = '';
        String USerRegion='';
        //Setting for email attributes
        
        String subject;
        String body1;
        String body2;
        String body;
        String subject1;
        String bodys;
        String bodys2;
        String bodys1;
        
        //ends here
        System.debug('Is button clicked '+trigger.new[0].Is_reject_button_clicked__c+trigger.new[0].Opportunity_Owner_Email__c+trigger.new[0].Createdby_Email__c);
        
        if (!trigger.new[0].Is_reject_button_clicked__c){
            
            Map<String, Id> queMap= new Map<String, Id> ();
            
            //Get Billing Team Queue id corresponding to sales user region.
            for (QueueSObject q:que){
                if((q.Queue.Name).contains(usr.Region__c)){
                    queMap.put(q.Queue.Name,q.QueueId);
                    queid = q.QueueId;
                    billTeam = q.Queue.Name;
                    // System.debug('queue name ..'+q.Queue.Name);
                }
            }
            String groupMemberQuery = 'SELECT GroupId, Id, SystemModstamp, UserOrGroupId FROM GroupMember where GroupId =\''+queid+'\'';
            
            //Getting the all Users id's  corresponding to queue
            List<GroupMember> list_GM = Database.query(groupMemberQuery);
            
            List<Id> lst_Ids = new List<ID>();
            for(GroupMember gmObj : list_GM){
                lst_Ids.add(gmObj.UserOrGroupId);
            }
            
            //Query the Billing Team Users 
            List<User> lst_UserObj = [Select Id,name,Email,Region__c from User where id in :lst_Ids];
            
            //Here concanating all email addresses of Users belong to Billing Queue
            for(User usrObj : lst_UserObj){
                if(emailAddresses == ''){
                    emailAddresses = usrObj.Email;
                    USerRegion=usrObj.Region__c;
                }else{
                    emailAddresses += ':'+usrObj.Email; 
                    USerRegion=usrObj.Region__c;
                }
            }
        }
        else if(getFullOrderTerminationRecordTypeId() == trigger.new[0].RecordTypeId){
            emailAddresses =trigger.new[0].Createdby_Email__c;
        }
        else{
            emailAddresses = trigger.new[0].Opportunity_Owner_Email__c +':'+ trigger.new[0].Createdby_Email__c;
        }
        System.debug('emailadresses..'+emailAddresses);
        return emailAddresses;
        
    }
    
   /* Type   : Apex Managed Apex Sharing for Order__c
    * Author : Siddharth Sinha
    * Date   : 08-06-2015
    * The Order object's OWD has been made as Private
    * This is to extend the Manual Sharing of the Order to Order's related Account Owner,Created By,Opportunity Owner
    */ 
    
    /*if((trigger.isAfter && Trigger.IsInsert)||(trigger.isAfter && Trigger.IsUpdate)){
        /** Pass a list of all orders that are qualified to be shared manually **/
        /*List<Order__c> olist = new List<Order__c>();
        /** For each of the Order records being inserted, add them to the share list: **/
        /*for(Order__c t : trigger.new){
            if(t.Name!=null){
                olist.add(t);} 
        }
        /** If the list is not empty call the CustomShare method to share the Order Manually: **/
        /*if(olist.size()>0){ 
            OrderShare objOdr = new OrderShare();
            objOdr.CustomShare(olist);  
        } 
    } */    
    
}