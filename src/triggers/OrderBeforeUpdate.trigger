/**
        @author - Accenture
        @date - 07-Aug-2012
        @version - 1.0
        @description - This trigger is used to check if the Key Milestone or Supplier Milestone is Yes for Line Item and Line Item Status is Complete then Order Status can be set to Complete.
    */
    trigger OrderBeforeUpdate on Order__c (before update) {
    /* class commented for test class coverage
        System.debug('inside trigger');
        if(Util.muteAllTriggers() || Util.skipOrderAfterUpdateFlag==false) return;
        if(checkRecursiveOrderlineitem1.runtwo()){  
        System.debug('OrderBeforeUpdate Starting used in the Apex Trigger: ' + Limits.getQueries());
        Set<Id> ordList = new Set<Id>();
        Map<Id,String> errorStr = new Map<Id,String>();
        List<Order__c> orderList = new List<Order__c>();
        Set<Id> OrderAccs = new Set<Id>();
        Map<Id, Contact> accContMap = new Map<Id,Contact>();
        Set<Id> OrderCont = new Set<Id>();
        String ProjectCompletion;
        Date ProjectCompletionDate;
        Boolean flagtoCheckETC;
        String errorMsg = '';
        flagtoCheckETC=false;
        //Added as part of email sending for CR271 when order is complete and available in ROC 
        List<Order__c> NotavailableInROCOrderList = new List<Order__c>();
        List<Order__c> Ordrlst = new List<Order__c>();
        
        //ends here for email ofCr271
        for(Order__c Ordr:Trigger.new)
        {
            List<Action_Item__c> ETCactionItemList= new List<Action_Item__c>();
           ETCactionItemList=[select Id,Order_Action_Item__r.CreatedById,Status__c,ownerId from Action_Item__c where Order_Action_Item__c=:Ordr.Id and Subject__c='Request to Calculate Early Termination Charge'];
           for(Action_Item__c aiObj:ETCactionItemList){
            
            //Changes as part of IR 088 enhancements -- changing the condition to check if the action items are completed -- Bhargav
            if(aiObj.Status__c!='Completed'){
                flagtoCheckETC=true;
                System.debug('**** flagtoCheckETC=='+flagtoCheckETC);
                if(flagtoCheckETC && Ordr.Full_Termination_Order__c && Ordr.Signed_Termination_Form_Attached__c){
               Ordr.addError('Please complete Request to Calculate ETC Action Item for all line items before assigning to Service Delivery');
                    
                }}}}
        
        for (Order__c o: Trigger.new){
            if(o.Status__c == 'Complete'){
                ordList.add(o.Id);
                errorStr.put(o.Id,'');
            }
            
            if(o.Status__c == 'Complete' && !(o.Available_in_ROC__c)){ 
                NotavailableInROCOrderList.add(o);
                
            }
        }
        
        //Added as part of email sending for CR271 when order is complete and available in ROC 
            if(NotavailableInROCOrderList.size()>0){
                GenericEmailSending emailSendingObj = new GenericEmailSending();
                emailSendingObj.getRequiredEmailAddresses(NotavailableInROCOrderList);
            
            }
        
        
        //ends Here of Cr271 of email sending
        /*for(Order_Line_Item__c oli : [SELECT ParentOrder__c,CPQItem__c,Product_Name__c,Key_Milestone__c,Supplier_Milestone__c,Line_Item_Status__c FROM Order_Line_Item__c WHERE ParentOrder__c IN: ordList AND Line_Item_Status__c != 'Complete']){
            if(oli.Key_Milestone__c == 'Yes' || oli.Supplier_Milestone__c == 'Yes'){
                system.debug('Status' + oli.Line_Item_Status__c);
                errorMsg = errorStr.get(oli.ParentOrder__c);
                errorMsg += oli.CPQItem__c + ' - '+oli.Product_Name__c+'</br>';
                errorStr.put(oli.ParentOrder__c,errorMsg);
            }
        }*/

      /*  if(errorStr.size() > 0){
           for (Id ids : errorStr.keySet()){
               if(errorStr.get(ids)!=''){
                   Trigger.newMap.get(ids).addError('Order Status cannot be set to Complete as the following Line Item doesnt have Status set as Complete </br> '+ errorStr.get(ids));
               }
           }
        }
          
       for(Order__c ord: Trigger.new){

            if(ord.Status__c == 'Complete'){
                ord.Project_Completion_Date__c = ord.LastModifiedDate.date();
                orderList.add(ord);
                }            
                if(!Util.muteDateUpdateOnOrder()) 
                {
                //Fix by Siddharth 07-Jan-16
                Order__c ordOld = Trigger.oldMap.get(ord.ID); // get the id of the record

            if(ord.SD_OM_Contact__c != null && (ord.SD_OM_Contact__c != ordOld.SD_OM_Contact__c)){
            
                ord.SD_OM_Assigned_Date__c = system.today();
            }
            else if(ord.SD_OM_Contact__c == null){
                ord.SD_OM_Assigned_Date__c = null;
            }
            if(ord.Technical_Sales_Contact__c != null && (ord.SD_OM_Contact__c != ordOld.Technical_Sales_Contact__c)){
            
                ord.Technical_Sales_Assigned_Date__c = system.today();
            }
            else if(ord.Technical_Sales_Contact__c == null){
                ord.Technical_Sales_Assigned_Date__c = null;
            }
            if(ord.Technical_Specialist_Contact__c != null && (ord.SD_OM_Contact__c != ordOld.Technical_Specialist_Contact__c)){
        
                ord.Technical_Specialist_Assigned_Date__c = system.today();
            }
            else if(ord.Technical_Specialist_Contact__c != null){
                ord.Technical_Specialist_Assigned_Date__c = null;
            }
            if(ord.SD_PM_Contact__c != null && (ord.SD_OM_Contact__c != ordOld.SD_PM_Contact__c)){
            
                ord.SD_PM_Assigned_Date__c = system.today();
            }
            else if(ord.SD_PM_Contact__c != null){
                ord.SD_PM_Assigned_Date__c = null;
            }
            }
            OrderAccs.add(ord.Account__c);  
        }
        List<Contact> cont = [SELECT Id,Name,AccountId,Email,Phone,MobilePhone FROM Contact WHERE AccountId IN : OrderAccs AND Primary_Contact__c=true];
        for (Contact con: cont){
            accContMap.put(con.AccountId,con);
        }
        for(Order__c ord : Trigger.new){
            if (cont!=null){
                if (cont.size()!=0){
                    if(accContMap.size()!=0){
                        if (accContMap.containsKey(ord.Account__c)){
                            ord.Customer_Primary_Contact__c = (accContMap.get(ord.Account__c)).Id;
                            ord.Customer_Primary_Contact_Email__c = (accContMap.get(ord.Account__c)).Email;
                            ord.Customer_Primary_Contact_Mobile__c = (accContMap.get(ord.Account__c)).MobilePhone;
                            ord.Customer_Primary_Contact_Number__c = (accContMap.get(ord.Account__c)).Phone;
                        }
                    }
                }
            }
        }
        List<Contact> con = new List<Contact>(); 
      /*  for(Order__c ord : Trigger.new){
            OrderAccs.add(ord.Account__c);
            if(ord.Customer_Technical_Contact__c != trigger.oldMap.get(ord.Id).Customer_Technical_Contact__c || ord.Customer_Technical_Contact__c != null){
                OrderCont.add(ord.Customer_Technical_Contact__c);
                con = [SELECT Id,Name,AccountId,Email,Phone,MobilePhone FROM Contact WHERE AccountId IN : OrderAccs AND Id IN : OrderCont];
            }
            if(ord.Customer_Technical_Contact__c == null){
               // ord.Customer_Technical_Contact_Mobile__c = null;
               // ord.Customer_Technical_Contact_Email__c = null;
            }
            for(Contact con1 : con){
                accContMap.put(con1.AccountId,con1);
            }
            if(con != null){
                if(con.size() !=0){
                    if(accContMap.size() !=0){
                        //ord.Customer_Technical_Contact_Mobile__c = (accContMap.get(ord.Account__c)).MobilePhone;
                       // ord.Customer_Technical_Contact_Email__c = (accContMap.get(ord.Account__c)).Email;
                    }
                }
            }
        }*/
        
      //Cr-271 validation rule
        
       /* Map<String, List<Order_Line_Item__c>> MapOLI= new Map<String, List<Order_Line_Item__c>>();
        List<Order_Line_Item__c> LstOLI= new List<Order_Line_Item__c>();
        
        LstOLI=[Select Id,ParentOrder__c, Line_Item_Status__c from Order_Line_Item__c where parentOrder__c in: ordList and Line_Item_Status__c!='complete'];

        for(Order_Line_Item__c oli:LstOLI){
            
            if(MapOLI.containsKey(oli.ParentOrder__c)){
                MapOLI.get(oli.ParentOrder__c).add(oli);
                System.debug('Irshad-------->>'+MapOLI.IsEmpty());
                }
                else
                {
                    List<Order_Line_Item__c> OLiLst= new List<Order_Line_Item__c>();


                       OLiLst.add(Oli);    

                   MapOLI.put(oli.ParentOrder__c,OLiLst);
                
                }
         }
        */
          /*  for(Order__c ord:Trigger.New){
                            
            
            
             if(trigger.OLdMap.get(ord.id).Status__c=='Complete' && trigger.OLdMap.get(ord.id).Status__c != trigger.newMap.get(ord.id).Status__c)
             
                 ord.Status__c.addError('Order once set to Complete, Can not be editable');
            

            }
       
      // CR-271 Validation rule on US postal code error
     
        set<id> ordId=new Set<Id>();
        for(order__c ord: trigger.New){   
           
            ordId.add(ord.Id); 
            System.debug('**** submitted value=='+ordId);
            
        }
     

        List<Order_Line_Item__c> LstOLIPostal= new List<Order_Line_Item__c>();
        Set<Id> siteId=new set<Id>();
        Map<Id,Site__C> siteMap=new map<Id,Site__c>();

        LstOLIPostal=[Select Id,name,ParentOrder__c,Billing_Entity__c,AEndZipPlus4__c,BEndZipPlus4__c,Site_A_SDPM__c,Site_B_SDPM__c,CPQItem__c from Order_Line_Item__c where parentOrder__c in: ordId];
          
        System.debug('LstOLIPostal=='+LstOLIPostal);
        
        for(Order_Line_Item__c oli:LstOLIPostal){
            if(oli.Site_A_SDPM__c != null)
                siteId.add(oli.Site_A_SDPM__c);  
           // if(oli.Site_B_SDPM__c != null)
           //     siteId.add(oli.Site_B_SDPM__c);   
          
        }  
        
        system.debug('siteId****'+siteId); 
        
        List<Site__C> lstSite=new list<Site__C>();  
            lstSite=[Select id,Country_name__c,Country_Formula__c,PostalCode__c from Site__c where id in:siteId and Country_Formula__c='US'];
            System.debug('lstSite=='+lstSite);
            for(Site__c site: lstSite){
                
                SiteMap.put(site.id,site);
                System.debug('SiteMap=='+lstSite);
            
            }
        system.debug('SiteMap'+ SiteMap);
            
        for(order__c o:trigger.new){
            IF(o.Is_Order_Submitted__c==true){
            
                 system.debug('******order submitted value==='+o.Is_Order_Submitted__c);
                      for(order_line_item__c oli:LstOLIPostal){
                          if(oli.Billing_Entity__c=='Telstra Incorporated'){ 
                                
                                if(oli.Site_A_SDPM__c != null){                            
                                
                                if(SiteMap.get(oli.Site_A_SDPM__c) != null && SiteMap.get(oli.Site_A_SDPM__c).PostalCode__c != null){
                                    //System.debug('I am in Site A++++++++++' +oli.site_A__r.PostalCode__C);
                                if(!(Pattern.matches('[0-9]{5} [0-9]{4}',SiteMap.get(oli.Site_A_SDPM__c).PostalCode__c)|| 
                                Pattern.matches('[0-9]{5}-[0-9]{4}',SiteMap.get(oli.Site_A_SDPM__c).PostalCode__c)|| 
                                Pattern.matches('[0-9]{5}[0-9]{4}',SiteMap.get(oli.Site_A_SDPM__c).PostalCode__c)))
                                    {                            
                                        O.addError('The postal code entered for the corresponding ' +oli.name  +' service site A is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789) ');
                                        //System.debug('Updated postal Code----->' +oli.site_A__r.PostalCode__C);
                                    }
                                 }   
                                    
                                }
                                
                            if(oli.Site_B_SDPM__c !=null){
                            
                                System.debug('*** I am in Site B');
                                if(SiteMap.get(oli.Site_B_SDPM__c) != null && SiteMap.get(oli.Site_B_SDPM__c).PostalCode__c != null){
                                    if(!(Pattern.matches('[0-9]{5} [0-9]{4}',SiteMap.get(oli.Site_B_SDPM__c).PostalCode__c)|| 
                                    Pattern.matches('[0-9]{5}-[0-9]{4}',SiteMap.get(oli.Site_B_SDPM__c).PostalCode__c)|| 
                                    Pattern.matches('[0-9]{5}[0-9]{4}',SiteMap.get(oli.Site_B_SDPM__c).PostalCode__c)))
                                    {        
                                        system.debug('');
                                        O.addError('The postal code entered for the corresponding '+oli.name+' service site B is not in the appropriate format.  The Postal Code format should be XXXXXYYYY(E.g. 123456789) OR XXXXX-XXXX(E.g. 12345-6789) OR XXXXX XXXX(e.g. 12345 6789) ');
                                    }
                                }
                            }   

            //CR 271 validation on AEndZipPlus4..and BEndZipPlus4
            
                if(SiteMap.get(oli.Site_A_SDPM__c)!= null && SiteMap.get(oli.Site_A_SDPM__c).PostalCode__c != null && oli.AEndZipPlus4__c==null){
                                  O.addError('You must enter value for AEndZipPlus4 at level ' +oli.CPQItem__c);               
                               }                      
                if(SiteMap.get(oli.Site_B_SDPM__c)!= null && SiteMap.get(oli.Site_B_SDPM__c).PostalCode__c != null && oli.BEndZipPlus4__c==null)
                        O.addError('You must enter value for BEndZipPlus4 at level ' +oli.CPQItem__c);           
                         
                            }
                        }  
                }
      
            }
            System.debug('OrderBeforeUpdate Ending used in the Apex Trigger: ' + Limits.getQueries());
          }  */
         }