/**
@author - Accenture
@date - 15-JUNE-2012
@version - 1.0
@description - This trigger "rolls up" the order line detials in HTML format to Line_Item_Details__c at Order level.
*/
/** 
Changes done by PSM for Specific Requirements
date - 28-Mar-2013
Version -1.1
*/

trigger OrderLineItembeAfterInsert on Order_Line_Item__c (after insert) {
  /*  if(Util.muteAllTriggers()) return;
    if(UpdateChildLineItem.ischildOLIComplete){
        System.debug('I am in OrderLineItembeAfterInsert '+Trigger.new[0].Account_Manager__c);
        System.debug('OrderLineItembeAfterInsert Starting used in the Apex Trigger: ' + Limits.getQueries());
    Set<Id> updateOrderIds = new Set<Id>();
    String productType = '';
    String circuitType = 'None';
    for(Order_Line_Item__c oli: Trigger.new)
       updateOrderIds.add(oli.ParentOrder__c);
     
    Map<Id,Order__c> OrderMap = new Map<Id,Order__c>();
    for(Order__c ord : [select Id, Line_Item_Details__c,New_Line_Item_Details__c,Product_Type__c from Order__c where Id IN: updateOrderIds and Status__c != 'Complete'])
        OrderMap.put(ord.Id,ord);

    Map<Id,String> tBody = new Map<Id,String>();
    Map<Id, String> newtBody = new Map<Id, String>();
    String header = '<table border="1"><thead><tr><th>Line Item No.</th><th>Service Id</th><th>Link</th></tr><tbody>';
    String header2 = '<table border="1"><thead><tr><th>Line Item No.</th><th>Primary Service Id</th><th>Master Service Id</th><th>Work Order Id</th></tr><tbody>'; 
    List<Order_Line_Item__c> oliList = new List<Order_Line_Item__c>();
    //oliList = [SELECT Id, Name, Primary_Service_ID__c, Master_Service_ID__c,ParentOrder__c,Product_ID__c,Site_A__c,Site_B__c,Work_Order_Id__c FROM Order_Line_Item__c WHERE ParentOrder__c IN :updateOrderIds];
    oliList = [SELECT Id, Name,ordertype__c, Primary_Service_ID__c, Master_Service_ID__c , Product_Name__c,ParentOrder__c,Product_ID__c,Site_A__c,Site_B__c,Work_Order_Id__c FROM Order_Line_Item__c WHERE ParentOrder__c IN :updateOrderIds];
    List<OrderProducts__c> allOrderprod = OrderProducts__c.getAll().values();    
    Map<String,String> OrderProdMap = new Map<String,String>();
    Map<String,Set<String>> OrderProdAssocMap = new Map<String,Set<String>>();
    for (OrderProducts__c ordprd :  allOrderprod )
    {  
          OrderProdMap.put( ordprd.MainProductType__c,ordprd.ProdIDAssociated__c);   
           Set<String> prodAssoc = new Set<String>();               
            String[] asocprods =  ordprd.ProdIDAssociated__c.split('\\;');      
              for (String prodida: asocprods )
              {            
                  prodAssoc.add(prodida);        
                 }      
               OrderProdAssocMap.put( ordprd.MainProductType__c,prodAssoc); 
      }   
    for(Order_Line_Item__c oli : oliList){
        Id ordrid = oli.ParentOrder__c;
        String bodyStr = '';
        String bodyStr2 ='';
        if(!tbody.containsKey(ordrid)){
            tbody.put(ordrid, bodyStr);
            bodyStr = header;
            newtBody.put(ordrid, bodyStr2);
            bodyStr2 = header2;
        }
        if(oli.Primary_Service_ID__c == null){
            bodyStr+= '<tr><td>' + oli.Name + '</td><td>' + 'N/A' + '</td><td><a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oli.Id + '">Link to Line Item</a></td></tr>';
            if (oli.Master_Service_ID__c!=null){
                 bodyStr2+='<tr><td>' + oli.Name + '</td><td>' + 'N/A' + '</td><td>'+(oli.Master_Service_ID__c!=null?oli.Master_Service_ID__c:'N/A')+'</td><td>'+(oli.Work_Order_Id__c!=null?oli.Work_Order_Id__c:'N/A')+'</td></tr>';
                }
        }
        else{
            bodyStr+= '<tr><td>' + oli.Name + '</td><td>' + oli.Primary_Service_ID__c + '</td><td><a href="' + URL.getSalesforceBaseUrl().toExternalForm() + '/' + oli.Id + '">Link to Line Item</a></td></tr>';
            bodyStr2+='<tr><td>' + oli.Name + '</td><td>' + oli.Primary_Service_ID__c + '</td><td>'+(oli.Master_Service_ID__c!=null?oli.Master_Service_ID__c:'N/A')+'</td><td>'+(oli.Work_Order_Id__c!=null?oli.Work_Order_Id__c:'N/A')+'</td></tr>';
           }           
          /* if(oli.Product_ID__c =='EPLSWCS'||oli.Product_ID__c =='EPLSHC'||oli.Product_ID__c=='EPL-OSS'){
             productType+='EPL'+',';               
           }else if(oli.Product_ID__c == 'IPLSWCS'||oli.Product_ID__c =='IPLSHC'||oli.Product_ID__c =='IPL-OSS'){
               productType+='IPL'+',';                            
           }else if (oli.Product_ID__c =='IPVPN'){
               productType+='IPVPN'+','; 
           }else if (oli.Product_Name__c == 'ATM' || oli.Product_ID__c == 'ATM'||oli.Product_ID__c == 'GEN-NET'||oli.Product_ID__c == 'GDDOSP'||oli.Product_ID__c =='EVC' ||oli.Product_ID__c == 'FR'||oli.Product_ID__c =='IXM'){
    productType+='GENset1Net'+',';
           }else if(oli.Product_Name__c == 'EPLX' || oli.Product_ID__c == 'EPLX'||oli.Product_ID__c == 'GMS-ME'||oli.Product_ID__c == 'GMS-SE'){
    productType+='GENset2Net'+',';
           }else if(oli.Product_Name__c == 'SAT-SE' || oli.Product_ID__c == 'SAT-SE'||oli.Product_ID__c == 'SAT-ME'){
    productType+='GENset3Net'+',';
           }*/
        /*   for (String ordkey :  OrderProdAssocMap.keyset() )   
           {
                 set<String> prodAssoc = OrderProdAssocMap.get(ordkey);
                if (prodAssoc .contains(oli.Product_ID__c))
                {
                        productType+=ordkey+',';            
                  }        
             }  
       //productType=  productType.substring(0, productType.length()-1);
       tbody.put(ordrid, tbody.get(ordrid)+bodyStr);
       newtBody.put(ordrid, newtBody.get(ordrid)+bodyStr2); 
    }

    for(id ordrid : OrderMap.keySet())
        if(tbody.containsKey(ordrid)){
            OrderMap.get(ordrid).Line_Item_Details__c = tbody.get(ordrid)+ '</table>';
            OrderMap.get(ordrid).New_Line_Item_Details__c = newtBody.get(ordrid)+ '</table>';
            OrderMap.get(ordrid).Product_Type__c = productType;
            //OrderMap.get(ordrid).Circuit_Type__c = circuitType;
            }

    try {
        if(OrderMap.values() !=null)
            update OrderMap.values();
    } catch (Exception e) {
        CreateApexErrorLog.insertHandleException('Trigger','OrderLineItembeAfterInsert',e.getMessage(),'Order Line Item',UserInfo.getUserName());
    }
}
 System.debug('OrderLineItembeAfterInsert Ending used in the Apex Trigger: ' + Limits.getQueries());*/
}