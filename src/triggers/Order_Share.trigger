trigger Order_Share on csord__Order__c (after insert, after update) {

    /* Type   : Managed Apex Sharing for csord__Order__c
    // Author : Bhargav Suryaprakash
    // Date   : 20-06-2016
    // The Order object's OWD has been made as Private
    // This is to extend the Manual Sharing of the Order to the following 
    // Order Account Owner - done through below code
    // Order Opportunity Owner - done through below code
    // Order Owner - done through below code
    // All TI Commercial Users - to be done through Object Access Level at Profile
    // Commercial Role - to be done through Sharing Rules
    // Service Delivery Role - to be done through Sharing Rules
    // Products Role - to be done through Sharing Rules
    // Roles and Subordinates of Billing Team - to be done through Sharing Rules
    // Roles and Subordinates of Global Channel Original - to be done through Sharing Rules
    */

/** Do Not Run the code if Global Mute is Enabled **/
/*if(Util.muteAllTriggers()) return; */

 //if(trigger.isAfter){
 /** Order_Share is the "Share" table that was created when the Organization Wide Default sharing setting was set to "Private" **/
 //List<csord__Order__Share> OrderShares = new List<csord__Order__Share>();

 /** For each of the Order records being inserted, do the following: **/
 //for(csord__Order__c t : trigger.new){

 /** Create a new Order_Share record to be inserted in to the Order_Share table. **/
 //csord__Order__Share Order_LastModifiedBy = new csord__Order__Share();
 //csord__Order__Share Order_AccountOwner = new csord__Order__Share();
 //csord__Order__Share Order_OppOwner = new csord__Order__Share();
 /** Populate the Order_Share record with the ID of the record to be shared. **/
  //Order_LastModifiedBy.ParentId = t.Id;
  //Order_AccountOwner.ParentId = t.Id;
  //Order_OppOwner.ParentId = t.Id;

/* Debug Testing
//system.debug('Sid.OwnerId'+t.CreatedById);
//system.debug('Sid.Account__r.OwnerId'+t.Account__r.OwnerId);
//system.debug('Sid.Account__r.OwnerId'+t.Opportunity__r.Owner.Id);
//system.debug('Sid.AW'+t.Account_Owner_Id__c);
//system.debug('Sid.OW'+t.Opportunity_Owner_Id__c);
*/


  /** Share to - Order Last Modified By **/ 
 // If(t.LastModifiedById !=null)
   //{
     //   Order_LastModifiedBy.UserOrGroupId = t.LastModifiedById;
        /** Specify that the Apex sharing reason **/
       // Order_LastModifiedBy.RowCause = Schema.csord__Order__Share.RowCause.Last_Modified_By__c;
  // }
  /** Share to - Order Account Owner **/ 
 //If(t.Account_Owner_ID__c !=null)
   //{
     //  Order_AccountOwner.UserOrGroupId = t.Account_Owner_ID__c;
       /** Specify that the Apex sharing reason **/
       //Order_AccountOwner.RowCause = Schema.csord__Order__Share.RowCause.Account_Owner__c;
  // }

  /** Share to - AI Opportunity Owner **/ 
 //If(t.Opportunity_Owner_ID__c !=null)
   //{
     //  Order_OppOwner.UserOrGroupId = t.Opportunity_Owner_ID__c;
       /** Specify that the Apex sharing reason **/
       //Order_OppOwner.RowCause = Schema.csord__Order__Share.RowCause.Opportunity_Owner__c;
   //}
   
 /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
  //Order_AccountOwner.AccessLevel = 'Edit';
  //Order_OppOwner.AccessLevel = 'Edit';
  //Order_LastModifiedBy.AccessLevel = 'Edit';

 /** Add the new Share record to the list of new Share records. **/
 //OrderShares.add(Order_AccountOwner);
 //OrderShares.add(Order_OppOwner);
 //OrderShares.add(Order_LastModifiedBy);
 //}

 /** Insert all of the newly created Share records and capture save result **/
 //Database.SaveResult[] jobShareInsertResult = Database.insert(OrderShares,false);

 //}
}