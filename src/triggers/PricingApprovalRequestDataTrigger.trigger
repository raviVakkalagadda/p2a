trigger PricingApprovalRequestDataTrigger on Pricing_Approval_Request_Data__c (before update) {
    TriggerDispatcher.run(new PricingApprovalRequestDataTriggerHandler());
}