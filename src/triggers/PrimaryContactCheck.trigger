/**
    @author - Accenture
    @date - 11-JUNE-2012
    @version - 1.0
    @description - This is Trigger to Check on Primary Contact. Functional description States that Only Contact can have either Billing, Primary and Customer Satifaction checked for an Account.
    
*/
trigger PrimaryContactCheck on Contact (before insert, before update) 
{
/*
    if(Util.muteAllTriggers()) return;
    // Check If there is any Duplicate in the List of Contact by Using Below List
    
    // Map to Know Number of Primary Contacts Associated to a Account.
    Map<Id,Integer> primaryContactSize = new Map<Id,Integer>();
    
    // Map to Know Number of Billing Contacts Associated to a Account.
    Map<Id,Integer> billingContactSize = new Map<Id,Integer>();
    
    // Map to Know Number of Customer Satisfaction Contacts Associated to a Account.
    Map<Id,Integer> custSatisfactionContactSize = new Map<Id,Integer>();
    
    // Map to Present the Primary Contact Associated to a Account.
    Map<Id,Contact> primaryContactMap = new Map<Id,Contact>();
    
    // Map to Present the Billing Contact Associated to a Account
    Map<Id,Contact> billingContactMap = new Map<Id,Contact>();
    
    // Map to Present the Customer Satisfaction Contact Associated to a Account.
    Map<Id,Contact> custSatisfactionContactMap = new Map<Id,Contact>();
    
    Integer dupListErrorCount = 0;
    
    Set<Id> allAccountIds = new Set<Id>();
    
    // Loop through the Contact List to set the Sizes
      for (Contact cont : Trigger.New)
      {
        // Checking Primary Contact Size
        
        System.debug('The Primary Contact val is '+ cont.Primary_Contact__c);
        System.debug('The Primary Billing Contact val is '+ cont.Primary_Billing_Contact__c);
        System.debug('The Primary Customer Satisfaction Contact val is '+ cont.Primary_Customer_Satisfaction_Contact__c);
        if (cont.Primary_Contact__c==true)
        {
            if (!primaryContactSize.containsKey(cont.AccountId)){
                primaryContactSize.put(cont.AccountId,1);
            }else{
                Integer count = primaryContactSize.get(cont.AccountId);
                count = count+1;
                primaryContactSize.put(cont.AccountId,count);
            }
       }        
        
        // Checking the Billing Contact Size
        if (cont.Primary_Billing_Contact__c==true)
        {
            if (!billingContactSize.containsKey(cont.AccountId)){
                billingContactSize.put(cont.AccountId,1);
            }else{
                Integer count = billingContactSize.get(cont.AccountId);
                count= count+1;
                billingContactSize.put(cont.AccountId,count);
            }
        }
        
        //Checking the Customer Contact Size
        if (cont.Primary_Customer_Satisfaction_Contact__c==true)
        {
            if(!custSatisfactionContactSize.containsKey(cont.AccountId)){
                custSatisfactionContactSize.put(cont.AccountId,1);
            }else{
                Integer count = custSatisfactionContactSize.get(cont.AccountId);
                count=count+1;
                custSatisfactionContactSize.put(cont.AccountId,count);
            }
        }
        
    }
    
    //
    for (Contact cont: Trigger.New){
        
        // Checking Duplicates for Primary Contact and Associating the Primary Contact in the List
        if(primaryContactSize.get(cont.AccountId)>1){
            cont.addError('There are Duplicate Primary Contacts in the List Provided for this Account');
            dupListErrorCount++;
        }else{
            if (primaryContactSize.get(cont.AccountId)==1){
                System.debug('Inside the Primary Contact');
                primaryContactMap.put(cont.AccountId, cont);
            }
        }
        
        // Checking Duplicates for Billing Contact and Associating the Billing Contact in the List
        if (billingContactSize.get(cont.AccountId)>1){
            cont.addError('There are Duplicate Billing Contacts in the List Provided for this Account');
            dupListErrorCount++;
        }else{
            if (billingContactSize.get(cont.AccountId)==1){
                System.debug('Inside the Billing Contact');
                billingContactMap.put(cont.AccountId,cont);
            }
        }
        
        // Checking Duplicates for Customer Satisfaction Contact and Associating the Customer Satisfaction Contact in the List
        if (custSatisfactionContactSize.get(cont.AccountId)>1){
            cont.addError('There are Duplicate Customer Satisfaction Contacts in the List Provided for this Account');
            dupListErrorCount++;
        }else{
            if (custSatisfactionContactSize.get(cont.AccountId)==1){
                System.debug('Inside the Customer Satisfaction Contact');
                custSatisfactionContactMap.put(cont.AccountId,cont);
            }
        }
    }
    
        // Merge all the keySet of PrimaryContact, Billing Contact and Customer Satisfaction into One.
        for (Id accId : primaryContactMap.keyset()){
            if (!allAccountIds.contains(accId)){                
                allAccountIds.add(accId);
            }
        }
        for (Id accId : billingContactMap.keyset()){
                if(!allAccountIds.contains(accId)){
                    allAccountIds.add(accId);
                }
        }
        for (Id accId : custSatisfactionContactMap.keyset() ){
                if(!allAccountIds.contains(accId)){
                    allAccountIds.add(accId);
                }
        }
    
    // Processing to Next Level only if there are no Errors from the List Provided
    if (dupListErrorCount==0){
                
        List<Contact> allCheckedContacts = [Select Id,Name,Primary_Contact__c,Primary_Billing_Contact__c,Primary_Customer_Satisfaction_Contact__c,AccountId from Contact where AccountId in : allAccountIds];
        System.debug('All Checked Contacts Size '+allCheckedContacts.size());
        System.debug('Primary Contact Map Size '+ primaryContactMap.keyset().size());
        for (Contact cont: allCheckedContacts)
        {
                if (cont.Primary_Contact__c){
                    System.debug('Checking if Primary Contact Exists');
                    if (primaryContactMap.get(cont.AccountId)!=null){
                        if(primaryContactMap.get(cont.AccountId).Id != cont.Id){
                            System.debug('Checking if Primary Contact Exists in Map');  
                            primaryContactMap.get(cont.AccountId).addError('Contact '+cont.Name +' is already set as Primary Contact for this Account');
                        }
                    }
                }
                if (cont.Primary_Billing_Contact__c){
                    if (billingContactMap.get(cont.AccountId)!=null){
                        //billingContactMap.get(cont.AccountId).addError('Another Billing Contact Exist for this Account');
                        if(billingContactMap.get(cont.AccountId).Id != cont.Id){
                            billingContactMap.get(cont.AccountId).addError('Contact '+cont.Name +' is already set as Primary Billing Contact for this Account');
                        }

                    }
                }
                if (cont.Primary_Customer_Satisfaction_Contact__c){
                    if (custSatisfactionContactMap.get(cont.AccountId)!=null){
                        /custSatisfactionContactMap.get(cont.AccountId).addError('Another Customer Satisfaction Contact Exist for this Account');
                        if(custSatisfactionContactMap.get(cont.AccountId).Id != cont.Id){
                            custSatisfactionContactMap.get(cont.AccountId).addError('Contact '+cont.Name +' is already set as Primary Customer Satisfaction Contact for this Account');
                        }
                        
                    }
                }
        }
                                
    }
    */
   /* Map<Id,Account> custAccMap = new Map<Id,Account>();
    Set<id> accid = new Set<Id>();  
    Map<Id,Id> accCustidMap = new Map<Id,Id>();
    for (Contact c:Trigger.New){
        accId.add(c.Accountid);     
        accCustIdMap.put(c.Accountid,c.id);
    }
    List<Account> accList = [Select Id, Name, Is_Primary__c from Account where id=:accId];
    for (Account acc : accList){
        custAccMap.put(accCustIdMap.get(acc.Id),acc);
    }
    
    for (Contact c:Trigger.New){
        try{
        if (Trigger.isupdate){
            
            if ((c.Primary_Contact__c  ||c.Primary_Billing_Contact__c|| c.Primary_Customer_Satisfaction_Contact__c)&&!custAccMap.get(c.id).Is_Primary__c)
            {     
                    Account acc = custAccMap.get(c.id);             
                    acc.Is_primary__c=true;                
                    update acc;
             }
            else if ((!c.Primary_Contact__c &&!c.Primary_Billing_Contact__c && !c.Primary_Customer_Satisfaction_Contact__c )&& (Trigger.oldMap.get(c.id).Primary_Billing_Contact__c ||Trigger.oldMap.get(c.id).Primary_Customer_Satisfaction_Contact__c||Trigger.oldMap.get(c.id).Primary_Contact__c) && custAccMap.get(c.id).Is_Primary__c){
                Account acc = custAccMap.get(c.id);             
                    acc.Is_primary__c=false;                
                    update acc;
            }else if ((!Trigger.oldMap.get(c.id).Primary_Billing_Contact__c && !Trigger.oldMap.get(c.id).Primary_Customer_Satisfaction_Contact__c && !Trigger.oldMap.get(c.id).Primary_Contact__c) && (c.Primary_Contact__c  ||c.Primary_Billing_Contact__c|| c.Primary_Customer_Satisfaction_Contact__c)&& custAccMap.get(c.id).Is_Primary__c){
                            c.Primary_Contact__c = false;                   
                    c.addError('Primary Contact already exists for this account');                 
            }
        }
        if (Trigger.isInsert){
        if ((c.Primary_Contact__c  ||c.Primary_Billing_Contact__c|| c.Primary_Customer_Satisfaction_Contact__c)&&!custAccMap.get(c.id).Is_Primary__c)
                {     
                    Account acc = custAccMap.get(c.id);             
                    acc.Is_primary__c=true;                
                    update acc;
                }
                else if ((c.Primary_Contact__c||c.Primary_Billing_Contact__c||c.Primary_Customer_Satisfaction_Contact__c)  && custAccMap.get(c.id).Is_Primary__c){
                    // Return Error                 
                    c.Primary_Contact__c = false;                   
                    c.addError('Primary Contact already exists for this account');                 
                }
      }
        }catch(Exception e){
        CreateApexErrorLog.InsertHandleException('Trigger','PrimaryContactCheck',e.getMessage(),'contact',Userinfo.getName());
    }
    }*/   
}