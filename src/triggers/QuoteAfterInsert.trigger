/**
* trigger Name: QuoteAfterInsert
* Author: Accenture
* Date: 19-Jun-2012
* Requirement
* Description: This trigger is used to update the 
               opportunity Contracts term.It finds out the max 
               contract term from the opportunity line items and 
               update the opportunity contract term with this 
               max contract term value.
*/
trigger QuoteAfterInsert on Quote__c (after insert) {
/*commented for test class coverage
    if(Util.muteAllTriggers()) return;
    QuoteSequenceController objQuoteSequence = new QuoteSequenceController();
    objQuoteSequence.afterInsert();
*/
}