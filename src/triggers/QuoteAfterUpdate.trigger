/**
* trigger Name: QuoteAfterUpdate
* Author: Accenture
* Date: 19-Jun-2012
* Requirement
* Description: This trigger is used to update the 
               opportunity Contracts term.It finds out the max 
               contract term from the opportunity line items and 
               update the opportunity contract term with this 
               max contract term value.
*/
trigger QuoteAfterUpdate on Quote__c (after update) {
/*
    if(Util.muteAllTriggers()) return;
    QuoteSequenceController objQuoteSequence = new QuoteSequenceController();
    objQuoteSequence.afterUpdate();
   */ 
}