/**
    @author - Accenture
    @date - 10-May-2012
    @version - 1.0
    @description - This trigger creates the service for the order forms from 
                   Product and Opportunity.
*/
trigger ServiceContractAssociation on Service__c (after insert, after update) {
 /* if(Util.muteAllTriggers()) return;  
  
    if(Trigger.isAfter  && Trigger.isInsert)
    {   
        Map<Id,String> productMap = new Map<Id,String>();
        Map<Id,Id> opportunityMap = new Map<Id,Id>();

        for (Service__c service: Trigger.new)
        {
        // Map of Id and Product
            productMap.put(service.Id,service.Product__c);        
        // Map of Id and Opportunity
            opportunityMap.put(service.Id,service.Opportunity__c);        
        }
    
        FutureServiceContractAssociation.processService(productMap,opportunityMap,Trigger.New);
    }
       
  */  
}