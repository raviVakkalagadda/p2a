/**
        @author - Accenture
        @date - 28-JUNE-2012
        @description -1) This Trigger is used to update the Is Updated flag when Site changes are sent to TIBCO.
                      2) For CR#152 - This trigger is used to update the "Is Attached to Bill Profile" flag if site is associated to any Bill Profile.          
                       CR# - 152 
    */  
trigger SiteBeforeUpdate on Site__c (before update) {

     /*if(Util.muteAllTriggers()) return;
     
    List<Id> siteList = new List<Id>(); //part of CR 414
    List<BillProfile__c> billList = new List<BillProfile__c>();//part of CR 414
    
    for(Site__c s:Trigger.New){
    
       siteList.add(s.Id); // part of CR 414
      if (s.valid__c==true && Trigger.oldMap.get(s.Id).valid__c== true && s.isOppUpdate__c==true && Trigger.oldMap.get(s.id).isOppUpdate__c==true){
             s.Is_updated__c = true;
        }
  }
       
        // Checking if site is associated to bill profile or not.       
        integer i = [Select count() from BillProfile__c where Bill_Profile_Site__c IN: siteList];      
      For(Site__c st : Trigger.new){
        if (i > 0 ){            
        // if i is greater than 0 that means Bill Profile is associated to Site so here we are updating the Flag as True           
        st.Is_Attached_to_Bill_Profile__c = true;            
        system.debug('\n\n@@ [1] if part :Attached Bill Profile : '+st.Is_Attached_to_Bill_Profile__c);       
        }        
        else{            
        st.Is_Attached_to_Bill_Profile__c = false;            
        system.debug('\n\n@@ [1] else part :Attached Bill Profile : '+st.Is_Attached_to_Bill_Profile__c);        
        }    
    }
    
    // CR 414 code starts here
    billList = [Select Id, Name, Company__c,Status__c,Bill_Profile_Site__c from BillProfile__c where Bill_Profile_Site__c in :siteList];
    For(Site__c site : Trigger.new){
       for(BillProfile__c bp:billList){
           if(bp.Company__c == Null && bp.Status__c =='Active'){
             site.adderror(System.label.Site_BP_Error);
           }
       }
    }
    // CR 414 Code ends here
  */  
}