/**
        @author - Accenture/Sanjeev
        @date - 06-Oct-2017
        @description - This Trigger is created as a replacement of TIDocument_Share
    */  

trigger TIDocumentTrigger on TI_Document__c (before insert, before update, before delete,after insert, after update, after delete, after undelete){
    TriggerDispatcher.run(new TIDocumentTriggerHandler());
}