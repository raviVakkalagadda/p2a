trigger TIDocument_Share on TI_Document__c (after insert,after update) {

/* Type   : Managed Apex Sharing for TI_Document__c
// Author : Siddharth Sinha
// Date   : 30-04-2015
// The TI Document object's OWD has been made as Private
// This is to extend the Manual Sharing of the TI Document to Account Owner, Created By, Opportunity Owner
*/ 

 if(trigger.isAfter){

 /** TI_Document_Share is the "Share" table that was created when the Organization Wide Default sharing setting was set to "Private" **/
 List<TI_Document__Share> TIShares = new List<TI_Document__Share>();

 /** For each of the Action_Item records being inserted, do the following: **/
 for(TI_Document__c t : trigger.new){

 /** Create a new TI_Document_Share record to be inserted in to the Action_Item_Share table. **/
 TI_Document__Share TI_CreatedBy = new TI_Document__Share();
 TI_Document__Share TI_AccountOwner = new TI_Document__Share();
 TI_Document__Share TI_OppOwner = new TI_Document__Share();
 
 /** Populate the TI_Document__Share record with the ID of the record to be shared. **/
  TI_CreatedBy.ParentId = t.Id;
  TI_AccountOwner.ParentId = t.Id;
  TI_OppOwner.ParentId = t.Id;

/* Debug Testing
system.debug('Sid.OwnerId'+t.CreatedById);
system.debug('Sid.Account__r.OwnerId'+t.Account__r.OwnerId);
system.debug('Sid.Account__r.OwnerId'+t.Opportunity__r.Owner.Id);
system.debug('Sid.AW'+t.Account_Owner_Id__c);
system.debug('Sid.OW'+t.Opportunity_Owner_Id__c);
*/

 /** Share to - TI Doc Creator **/ 
 If(t.CreatedById !=null)
   {
        TI_CreatedBy.UserOrGroupId = t.CreatedById;
        /** Specify that the Apex sharing reason **/
        TI_CreatedBy.RowCause = Schema.TI_Document__Share.RowCause.Creator__c;
   }
  
  /** Share to - TI Doc Account Owner **/ 
 If(t.Account_Owner_ID__c !=null)
   {
       TI_AccountOwner.UserOrGroupId = t.Account_Owner_ID__c;
       /** Specify that the Apex sharing reason **/
       TI_AccountOwner.RowCause = Schema.TI_Document__Share.RowCause.Account_Owner__c;
   }

  /** Share to - AI Opportunity Owner **/ 
 If(t.Opportunity_Owner_Id__c !=null)
   {
       TI_OppOwner.UserOrGroupId = t.Opportunity_Owner_Id__c;
       /** Specify that the Apex sharing reason **/
       TI_OppOwner.RowCause = Schema.TI_Document__Share.RowCause.Opportunity_Owner__c;
   }
   
 /** Specify that the AI should have edit/read access for this particular Action_Item record. **/
  TI_CreatedBy.AccessLevel = 'Edit';
  TI_AccountOwner.AccessLevel = 'Read';
  TI_OppOwner.AccessLevel = 'Read';

 /** Add the new Share record to the list of new Share records. **/
 TIShares.add(TI_CreatedBy);
 TIShares.add(TI_AccountOwner);
 TIShares.add(TI_OppOwner);
 }

 /** Insert all of the newly created Share records and capture save result **/
 Database.SaveResult[] jobShareInsertResult = Database.insert(TIShares,false);

 }
}