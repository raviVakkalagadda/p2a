trigger TrgAfterOpportunity on Opportunity (after update) {
    
   
    
    
  /* List<Action_Item__c> aiList = new List<Action_Item__c>();
    RecordType rt = [select Id,Name from RecordType where Name=:'Stage Gate Review Request' and SobjectType =: 'Action_Item__c' limit 1];

    for (Opportunity o : trigger.new) {
        if(o.Opportunity_Type__c == 'Complex' && o.StageName== 'Qualified Prospect' && o.Stage_Gate_1_Action_Created__c ==false ){
            Action_Item__c ai = new Action_Item__c();
            ai.Opportunity__c = o.Id;
            ai.Account__c = o.AccountId;
            ai.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
            ai.RecordTypeId = rt.Id;
            ai.Subject__c = 'Gate Review - 1';
            ai.Stage_Gate_Number__c = 1;
            ai.Status__c = 'Assigned';
          //  ai.Priority__c = 'High';
            aiList.add(ai);
        }else if(o.Opportunity_Type__c == 'Complex' && o.StageName== 'Proposal Issued' && o.Stage_Gate_2_Action_Created__c == false){
            If(o.Stage_Gate_1_Action_Created__c == false){
                 Action_Item__c ai1 = new Action_Item__c();
                 ai1.Opportunity__c = o.Id;
                 ai1.Account__c = o.AccountId;
                 ai1.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
                 ai1.RecordTypeId = rt.Id;
                 ai1.Subject__c = 'Gate Review - 1';
                 ai1.Stage_Gate_Number__c = 1;
                 ai1.Status__c = 'Assigned';
               //  ai.Priority__c = 'High';
                 aiList.add(ai1);
            }
                        
            Action_Item__c ai = new Action_Item__c();
            ai.Opportunity__c = o.Id;
            ai.Account__c = o.AccountId;
            ai.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
            ai.RecordTypeId = rt.Id;
            ai.Subject__c = 'Gate Review - 2';
            ai.Stage_Gate_Number__c = 2;
            ai.Status__c = 'Assigned';
          //  ai.Priority__c = 'High';
            aiList.add(ai);
        }else if(o.Opportunity_Type__c == 'Complex' && o.StageName== 'Individual & Final Negotiations' && o.Stage_Gate_3_Action_Created__c == false){
            //Check If Gate 1, Gate 2 is created before Gate 3, if not create it
            
            If(o.Stage_Gate_1_Action_Created__c == false){
             Action_Item__c ai1 = new Action_Item__c();
             ai1.Opportunity__c = o.Id;
             ai1.Account__c = o.AccountId;
             ai1.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
             ai1.RecordTypeId = rt.Id;
             ai1.Subject__c = 'Gate Review - 1';
             ai1.Stage_Gate_Number__c = 1;
             ai1.Status__c = 'Assigned';
           //  ai.Priority__c = 'High';
             aiList.add(ai1);
            }
            
            If(o.Stage_Gate_2_Action_Created__c == false){
                 Action_Item__c ai2 = new Action_Item__c();
                 ai2.Opportunity__c = o.Id;
                 ai2.Account__c = o.AccountId;
                 ai2.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
                 ai2.RecordTypeId = rt.Id;
                 ai2.Subject__c = 'Gate Review - 2';
                 ai2.Stage_Gate_Number__c = 2;
                 ai2.Status__c = 'Assigned';
               //  ai.Priority__c = 'High';
                 aiList.add(ai2);
            }

            Action_Item__c ai = new Action_Item__c();
            ai.Opportunity__c = o.Id;
            ai.Account__c = o.AccountId;
            ai.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
            ai.RecordTypeId = rt.Id;
            ai.Subject__c = 'Gate Review - 3';
            ai.Stage_Gate_Number__c = 3;
            ai.Status__c = 'Assigned';
           // ai.Priority__c = 'High';
            aiList.add(ai);
        }else if(o.Opportunity_Type__c == 'Complex' && o.StageName== 'Verbal' && o.Stage_Gate_4_Action_Created__c == false){
            //Check If Gate 1, Gate 2, Gate 3 is created before Gate 4, if not create it
            If(o.Stage_Gate_1_Action_Created__c == false){
             Action_Item__c ai1 = new Action_Item__c();
             ai1.Opportunity__c = o.Id;
             ai1.Account__c = o.AccountId;
             ai1.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
             ai1.RecordTypeId = rt.Id;
             ai1.Subject__c = 'Gate Review - 1';
             ai1.Stage_Gate_Number__c = 1;
             ai1.Status__c = 'Assigned';
           //  ai.Priority__c = 'High';
             aiList.add(ai1);
            }
            
            If(o.Stage_Gate_2_Action_Created__c == false){
                 Action_Item__c ai2 = new Action_Item__c();
                 ai2.Opportunity__c = o.Id;
                 ai2.Account__c = o.AccountId;
                 ai2.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
                 ai2.RecordTypeId = rt.Id;
                 ai2.Subject__c = 'Gate Review - 2';
                 ai2.Stage_Gate_Number__c = 2;
                 ai2.Status__c = 'Assigned';
               //  ai.Priority__c = 'High';
                 aiList.add(ai2);
            }

            If(o.Stage_Gate_3_Action_Created__c == false){
                 Action_Item__c ai3 = new Action_Item__c();
                 ai3.Opportunity__c = o.Id;
                 ai3.Account__c = o.AccountId;
                 ai3.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
                 ai3.RecordTypeId = rt.Id;
                 ai3.Subject__c = 'Gate Review - 3';
                 ai3.Stage_Gate_Number__c = 3;
                 ai3.Status__c = 'Assigned';
               //  ai.Priority__c = 'High';
                 aiList.add(ai3);
            }

            Action_Item__c ai = new Action_Item__c();
            ai.Opportunity__c = o.Id;
            ai.Account__c = o.AccountId;
            ai.Due_Date__c = System.Today() + 2;  //Expected this SLA to be defined
            ai.RecordTypeId = rt.Id;
            ai.Subject__c = 'Gate Review - 4';
            ai.Stage_Gate_Number__c = 4;
            ai.Status__c = 'Assigned';
           // ai.Priority__c = 'High';
            aiList.add(ai);
         }

        
    }
     if(aiList.size() > 0){
         insert aiList;
     }*/

}