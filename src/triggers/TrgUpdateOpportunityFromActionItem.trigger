/**
    @author - Accenture
    @date - 27-March-2012
    @version - 1.0
    @description - Rev 0.1 Added by Ramachandran to Populate the Validity of Feasibility Study from Action Item to Opportunity
*/

trigger TrgUpdateOpportunityFromActionItem on Action_Item__c (after insert, after update) {/*
    List<Opportunity> opList = new List<Opportunity>();
    Map<Id,Integer> actionOppMap = new Map<Id, Integer>();
    
    // to map the record type ID and Name
    Map<Id,String> rtMap = new Map<Id, String>();
    
    // to map the opportunity ID and Name
    Map<Id,String> opMap = new Map<Id, String>();
    
    // Creating the set to store IDs
    Set<id> oppIds = new Set<Id>();
    
    Integer StageNumber;
	for (Action_Item__c ai : trigger.new) {
		oppIds.add(ai.Opportunity__c);
	}

     //query RecordType to get RecordType Object based on request SobjectType
    for(RecordType rt :[select Id,Name from RecordType Where SobjectType = 'Action_Item__c' ]){   
        rtMap.put(rt.Id,rt.Name);
    }
    
    //query Opportunity to get Opportunity Object 
    for(Opportunity op :[select Id,Name from Opportunity where id IN: oppIds]){   
        opMap.put(op.Id,op.Name);
    }
    
    // Action Item is created for record type of which button is clicked
    for (Action_Item__c ai : trigger.new) {
        try{
            //System.debug('Record Type :'+rt.Name);            
            if(rtMap.get(ai.RecordTypeId) == 'Request Credit Check' && ai.Status__c=='Completed'){
                   if(opMap.get(ai.Opportunity__c) != null){ 
                    // update the fields of Opportunity with Action Item values                 
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,CreditCheckStatus__c=ai.Status__c,CreditCheckValidity__c=ai.CreditLimitValidity__c,SecurityDeposit__c=ai.SecurityDeposit__c,SecurityDepositHoldingPeriod__c=ai.SecurityDepositHoldingPeriod__c,CreditCheckDoneBy__c=ai.Completed_By__c,CreditCheckCompletedDate__c=ai.Completion_Date__c,Credit_Check_Feedback__c =ai.Feedback__c);
                    opList.add(op);
                   }
             } else if(rtMap.get(ai.RecordTypeId) == 'Request Feasibility Study' && ai.Status__c=='Completed'){
                if(opMap.get(ai.Opportunity__c) != null){
                   // update the fields of Opportunity with Action Item values  
                   Opportunity op = new Opportunity(Id = ai.Opportunity__c,FeasibilityStatus__c=ai.Status__c,FeasibilityDoneBy__c=ai.Completed_By__c,FeasibilityCompletedDate__c=ai.Completion_Date__c, F_Expiration_Date__c=ai.F_Expiration_Date__c,Feasibility_Result__c =ai.FeasibilityResult__c);
                   opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == 'Request Pricing Approval' && ai.Status__c=='Completed'){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,PricingApprovalStatus__c=ai.Status__c,PricingApprovalDoneBy__c=ai.Completed_By__c,PricingApprovalCompletedDate__c=ai.Completion_Date__c,Pricing_Approval_Feedback__c =ai.Pricing_Feedback__c);
                    opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == 'Request Third Party Quote' && ai.Status__c=='Completed'){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    Opportunity op = new Opportunity(Id = ai.Opportunity__c,CarrierQuoteStatus__c=ai.Status__c,CarrierQuoteDoneBy__c=ai.Completed_By__c,CarrierQuoteCompletedDate__c=ai.Completion_Date__c, Third_Party_Feedback__c = ai.Feedback__c);
                    opList.add(op);
                }
             } else if(rtMap.get(ai.RecordTypeId) == 'Stage Gate Review Request' && ai.Status__c=='Completed'){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    if(ai.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_1_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_2_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_3_Review_Completed__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_4_Review_Completed__c = true);
                        opList.add(op);
                    }                   
                }
            } else  if(rtMap.get(ai.RecordTypeId) == 'Stage Gate Review Request' && ai.Status__c=='Assigned'){
                if(opMap.get(ai.Opportunity__c) != null){
                    // update the fields of Opportunity with Action Item values  
                    if(ai.Stage_Gate_Number__c == 1){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_1_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 2){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_2_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 3){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_3_Action_Created__c = true);
                        opList.add(op);
                    }
                    else if(ai.Stage_Gate_Number__c == 4){
                        Opportunity op = new Opportunity(Id = ai.Opportunity__c,Stage_Gate_4_Action_Created__c = true);
                        opList.add(op);
                    }
                    
                }
            }
       } catch(Exception excp) {
                System.debug('Error In Action Item Trigger');       
                CreateApexErrorLog.InsertHandleException('Trigger','TrgUpdateOpportunityFromActionItem',excp.getMessage(),'Action_Item__c',Userinfo.getName());
       }
     }

    // Bulk Update
    if(opList.size() > 0 ) {
        try{
                update opList;
        }catch(Exception excp){
            
            CreateApexErrorLog.InsertHandleException('Trigger','TrgUpdateOpportunityFromActionItem',excp.getMessage(),'Action_Item__c',Userinfo.getName());
        }
    }*/
}