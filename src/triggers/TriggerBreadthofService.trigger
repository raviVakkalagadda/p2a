/**
    @author - Accenture
    @date - 09-JUNE-2012
    @version - 1.0
    @description - This trigger is used for Breadth of Service and Unique Products Purchased Calculation .
*/
trigger TriggerBreadthofService on Service__c (after insert) {
   /* if(Util.muteAllTriggers()) return;

    Set<ID> accId = new Set<ID>();
    Map<Id, Set<String>> accserviceSet = new Map<Id, Set<String>>();
    Map<String,String> prodMap = new Map<String,String>();
    List<Account> accountList = new List<Account>();

    // Get the unique Accounts in the service
    for (Service__c s : trigger.new)
        accId.add(s.AccountId__c);
        

    List<Service__c> service = [Select Name,AccountId__c,Product_Code__c,Product_ID__c from Service__c where AccountId__c in :accId];
    List<Account> account = [SELECT Id, Name, BreadthOfService__c, Unique_Products_Purchased__c FROM Account where Id in : accId];

    Map<Id,Account> accountMap = new Map<Id,Account>();
    for(Account a : account)
        accountMap.put(a.Id,a);
        
   // system.debug('accountMap'+ accountMap);

   for(Product2 objProd : [SELECT Id, ProductCode,Product_ID__c from Product2 where Eligible_for_Breadth_of_Service__c = true and ProductCode!=''])
      prodMap.put(objProd.Product_ID__c,objProd.ProductCode);

   // system.debug('ProdMap*****'+ prodMap);

    // Create Account and unique product map
    for (Service__c s : service)
    {
         //System.debug('s.Product_ID__c******* '+s.Product_ID__c);
        if(prodMap.ContainsKey(s.Product_ID__c)){
           // System.debug('s.Product_ID__c******* '+s.Product_ID__c);
             if (accserviceSet.containsKey(s.AccountId__c))
             {
                 accserviceSet.get(s.AccountId__c).add(s.Product_Code__c);
             //   System.debug('******* has account in map...');
            }
            else
             {
               // System.debug('******* new account in map...');
                 Set <String> a = new Set<String>();
                 a.add(s.Product_Code__c);
                 accserviceSet.put(s.AccountId__c,a);
            }
        }
     }

 //system.debug('AccserviceSetMap********'+ accserviceSet);

 //Update Accounts with Breadth of Service and Unique Products Purchased
    for(Id id: accserviceSet.keySet()){
        Account acc = accountMap.get(id);
        Set<String> prodCode = accserviceSet.get(id);
        acc.BreadthOfService__c = prodCode.size();
        String strCode = '';
        for(String str : prodCode)
            strCode += ','+str ;

        //system.debug('String********'+ strCode);
        strCode = strCode.substring(1);
        acc.Unique_Products_Purchased__c = strCode;
        accountList.add(acc);
    }
// system.debug('AccountMap'+accountList);
 if(accountList.size()>0)
        update accountList;*/
}