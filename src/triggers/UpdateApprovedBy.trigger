/**
    @author - Accenture
    @date - 09-JUNE-2012
    @version - 1.0
    @description - This trigger updates the field "Approved By" who checked 
                   the "Approved" Checkbox.
*/
trigger UpdateApprovedBy on BillProfile__c (before update) {
	/*
      if(Util.muteAllTriggers()) return;
      // Build up a list of all Bill Profile that are approved
      for (BillProfile__c b : trigger.new) {
        // updating the "Approved By" field 
        if (trigger.oldMap.get(b.Id).Approved__c != b.Approved__c && b.Approved__c == TRUE) {
              b.Approved_By__c = UserInfo.getUserId();
              
        }
      }*/
}