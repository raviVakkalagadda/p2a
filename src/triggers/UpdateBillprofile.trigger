/**
    @author - Accenture
    @date - 04-MAY-2012
    @version - 1.0
    @description - This is trigger to update the Bill profile's "Last_Address_Change_On_Site__c" field
                   when the Site address fields gets updated.
*/
trigger UpdateBillprofile on Site__c (after update) {/*
      if(Util.muteAllTriggers()) return;
      List<Site__c> siteList = new List<Site__c>();
   
      for (Site__c newSite:trigger.new)
      {
         //Getting the previous details of the site object      
         Site__c oldSite = trigger.oldMap.get(newSite.id);
         
         //compares the old and new values of the site object and fetches the updated site lists
         if( (oldSite.Address1__c != newSite.Address1__c)||(oldSite.Address2__c!=newSite.Address2__c)||(oldSite.Address_Type__c!=newSite.Address_Type__c)||(oldSite.City_name__c!=newSite.City_name__c)||(oldSite.Country_Formula__c!= newSite.Country_Formula__c)||(oldSite.State__c!= newSite.State__c)||(oldSite.PostalCode__c!= newSite.PostalCode__c)||(oldSite.Org_code__c!= newSite.Org_code__c) || (oldSite.Site_Code__c!= newSite.Site_Code__c) || (oldSite.Primary_Use__c!= newSite.Primary_Use__c)|| (oldSite.Name !=newSite.Name)||(oldSite.Address_3__c !=newSite.Address_3__c)||(oldSite.Address_4__c != newSite.Address_4__c))
         {           
            siteList.add(newSite);
         }                                        
                
      }
      
      //Fetching the bill profiles related to the updated site
      List<BillProfile__c> bpList = [SELECT Id,Last_Address_Change_On_Site__c,site_updated_time__c FROM BillProfile__c WHERE Bill_Profile_Site__c  IN:siteList];
      
      system.debug('BillProfile List:****' + bpList );
      for( BillProfile__c b: bpList)
      {
        //Updating the Bill Profile date field
        b.Last_Address_Change_On_Site__c = date.today();
        b.site_updated_time__c = datetime.now();
       system.debug('DATE Field******:'+  b.Last_Address_Change_On_Site__c);
      }
      update bpList;*/
}