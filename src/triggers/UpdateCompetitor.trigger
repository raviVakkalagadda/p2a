/**
    @author - Accenture
    @date - 07-JUNE-2012
    @version - 1.0
    @description - This trigger updates competitors name in the Competitor Bid .
*/
trigger UpdateCompetitor on Competitor_Bid__c (after Insert) {
     /*if(Util.muteAllTriggers()) return;
    if(trigger.isInsert){
        // variable
        String compititorNames='';       
        String compititor='';       
 
        // Creating the Set to store IDs to map with opportunity
        Set<Id> optyIdSet= new Set<Id>(); 
        Map<Id,Opportunity> oppMap=new Map<Id,Opportunity>();
        
        // Creating the Set to store IDs to map with Competitors name 
        Set<Id> compIdSet= new Set<Id>();
        Map<Id,String> compMap=new Map<Id,String>();
        

        //Creating map of Opportunity Id and Opportunity Record.       
        for(Competitor_Bid__c compititorBid : trigger.new) {      
            optyIdSet.add(compititorBid.Opportunity__c);
        }
        
        // query Opportunity to get Opportunity Object based on request Opportunity ID
        List<Opportunity> optyList=[select id, Competitors__c from opportunity where id in :optyIdSet];       
        for(Opportunity opp:optyList) {
                oppMap.put(opp.Id,opp);
        }
        
        
        //Creating map of Competitor Id and Competitor name.
        for(Competitor_Bid__c compititorBid : trigger.new) {
            compIdSet.add(compititorBid.Competitor__c);           
        }  
        
        // query Competitor to get Competitor Object based on request Competitor ID       
        List<Competitor__c> compList=[select id , name from Competitor__c where id in :compIdSet];        
        for(Competitor__c comp:compList) {
            compMap.put(comp.Id,comp.Name);
        }
        
       
        //Updating competitors name on Opportunity. 
        for(Competitor_Bid__c compititorBid : trigger.new) {      
            compititorNames=oppMap.get(compititorBid.Opportunity__c).Competitors__c;
            
            if(compititor!=null) {                            
                if(compititorNames==null){                
                    oppMap.get(compititorBid.Opportunity__c).Competitors__c = compMap.get(compititorBid.Competitor__c);                               
                }                
                else {
                    oppMap.get(compititorBid.Opportunity__c).Competitors__c = compititorNames + ',' + compMap.get(compititorBid.Competitor__c);
                }                
            }
            
        }
        update oppMap.values();                             
     
     }*/
}