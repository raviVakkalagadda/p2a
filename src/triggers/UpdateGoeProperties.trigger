/**
    @author - Accenture
    @date - 28-April-2012
    @version - 1.0
    @description - This trigger updates the Google properties in site Object .
*/
trigger UpdateGoeProperties on Site__c (before insert, before update) {/*
  if(Util.muteAllTriggers()) return;
  system.debug(Logginglevel.ERROR,'UpdateGoeProperties geo updates');      
  List <String> sitesToUpdate = new List<String>();
  
  // When new value is updated
  for(Site__c theSite: trigger.new)
  {
    boolean needsUpdate = false;
    if(Trigger.isInsert)
    {
      needsUpdate = true;
      theSite.latitude__c = '0.000';
      theSite.longitude__c ='0.000';
    }
    else {
      // getting the value for Old ID   
      Site__c beforeUpdate = System.Trigger.oldMap.get(theSite.Id);
      needsUpdate = (
       (beforeUpdate.City_Finder__c != theSite.City_Finder__c) ||
       (beforeUpdate.State__c != theSite.State__c) ||
       (beforeUpdate.Country_Finder__c != theSite.Country_Finder__c)
      );
    }
    // If it is true, then update the new ID
    if(needsUpdate)
    {
      sitesToUpdate.add(theSite.Id);      
    } 
  }
  // Bulk Update
  if(sitesToUpdate.size() >0 )
  {
    GeoUtilities.updateSiteGeo(sitesToUpdate);
  }*/
}