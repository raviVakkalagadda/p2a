/**
    @author - Accenture
    @date - 23-May-2012
    @version - 1.0
    @description - This trigger Updates Variation Number in contracts on uploading the documents.
*/
trigger UpdateVariationNoOnContracts on Attachment (before insert, before update) {
    /*if(Util.muteAllTriggers()) return;   
       // Variable
       Integer count=0;
    
       List<Contract__c> con = new List<Contract__c>();
       
       //query ObjectPrefixIdentifiers to get ObjectPrefixIdentifiers Object based on request Name
       ObjectPrefixIdentifiers__c opi = ObjectPrefixIdentifiers__c.getInstance('Contract');       
       
       // Getting the values from Contract and Account when new document is uploaded
       for(Attachment at : trigger.new)
       {
           if(opi.ObjectIdPrefix__c ==(((String)(at.ParentId)).substring(0,3))){
             //query Contract to get Contract Object based on request Attachment ID
             Contract__c con1 = [select Id, Name, Account__c, Initial_Period__c, Variation_Number__c from Contract__c where Id=:at.ParentId Limit 1];
             
             //query Account to get Account Object based on request Contract ID
             Account acc = [Select Id from Account where Id=:con1.Account__c Limit 1];
             
             // check first three digits of Attachment ID with ObjectIDPrefix field 
             if(opi.ObjectIdPrefix__c ==(((String)(at.ParentId)).substring(0,3)))
             {
            
                 
                 if(con1.Variation_Number__c == null)
                 {
                    //System.debug('Variation Number*** '+con1.Variation_Number__c);
                     con1.Variation_Number__c = 0;
                     //System.debug('Variation Number1*** '+con1.Variation_Number__c);
                 }
                 else
                 {
                    
                    for(Integer i=0; i<=con1.Variation_Number__c; i++) 
                    {
                        
                        count++;
                    }
                    con1.Variation_Number__c = count;
                    //System.debug('Variation Number1*** '+con1.Variation_Number__c);
                 }            
             }
             con.add(con1);
        }
       }
       update con;*/
}