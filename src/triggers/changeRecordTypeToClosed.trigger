/**
    @author - Accenture
    @date - 22-FEB-2012
    @version - 1.0
    @description - This trigger change the record type to closed when status is 
                   "Completed".
*/
trigger changeRecordTypeToClosed on Task (before insert,before update) {
   
     /*if(Util.muteAllTriggers()) return;
   
   //triggerlogic of setAssociatedGroupManual trigger is moved here to merge triggers into one  
   if(trigger.isInsert){
        for (Task t : Trigger.New)
        {
            String associatedGroup = t.Associated_Group_list__c;
            
            if (associatedGroup!=NULL && associatedGroup.length()>0)
            {
                t.CS_GT__Associated_Group__c = associatedGroup;
            }
            Please fill in this once this field is enabled through package
        }
   }   
   
   //Actual logic of current trigger starts here
   if(trigger.isUpdate){ 
        for (Task t : Trigger.New)
        {
            if (t.Status == 'Completed')
            {
                / query RecordType to get RecordType Object based on request 
                    Sobjecttype
                    Name
                
                //List<RecordType> recordTypeList = [SELECT Id FROM RecordType WHERE Sobjecttype = 'Task' and Name = 'Closed Task'];
                 //There should only be one response to this so just take the first entry
                //RecordType rt = recordTypeList.get(0);
                //t.recordTypeId = rt.Id;
                t.recordTypeId = Schema.SObjectType.Task.getRecordTypeInfosByName().get('Closed Task').getRecordTypeId();
               
            }
        }
    }
*/
}