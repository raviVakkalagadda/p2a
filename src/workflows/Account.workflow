<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Send_email</fullName>
        <ccEmails>venkat.narayan@accenture.com</ccEmails>
        <description>Send email</description>
        <protected>false</protected>
        <senderType>CurrentUser</senderType>
        <template>Sales_Templates/Account_Activation</template>
    </alerts>
	<fieldUpdates>
        <fullName>AccountManagerUpdate</fullName>
        <field>Account_Manager__c</field>
        <formula>Accounts_Manager__r.FirstName + Accounts_Manager__r.LastName</formula>
        <name>AccountManagerUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Account_Activated_date</fullName>
        <field>Account_Activated_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Account Activated date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_the_Customer_Type_field_to_Custom</fullName>
        <description>Update the Type field on Account to Customer</description>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Update the Type field on Account</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_accounttype_customfield</fullName>
        <field>Account_Type__c</field>
        <literalValue>Customer</literalValue>
        <name>update accounttype customfield</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
	<fullName>Update Account on Account Manager Change</fullName>
        <actions>
            <name>AccountManagerUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Accounts_Manager__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Set the record type - Activated-Australia</fullName>
        <actions>
            <name>Send_email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>equals</operation>
            <value>Active</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Customer_Type_New__c</field>
            <operation>equals</operation>
            <value>MNC</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Selling_Entity__c</field>
            <operation>equals</operation>
            <value>Telstra Corporation Limited</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Update Account Activated date</fullName>
        <actions>
            <name>Update_Account_Activated_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Activated__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Created for TGME0021097</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update the Customer Type on Account</fullName>
        <actions>
            <name>Update_the_Customer_Type_field_to_Custom</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_accounttype_customfield</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account.Global_Master_Service_Agreement_Global_B__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Type</field>
            <operation>notEqual</operation>
            <value>Customer,Former Customer</value>
        </criteriaItems>
        <description>Update the Customer Type field on Account</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
