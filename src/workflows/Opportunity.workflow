<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Also_Set_Is_Ever_Closed_for_Won</fullName>
        <field>Is_Ever_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Also Set Is Ever Closed for Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	  <fieldUpdates>
        <fullName>update_feasibilitytest</fullName>
        <field>Feasibilitytest__c</field>
        <formula>LEFT( Feasibility_Result_Details__c , 100)</formula>
        <name>update feasibilitytest</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Closed_Sales</fullName>
        <description>Update record type to Closed Sales</description>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Sales</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Closed Sales</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Opp_Record_Type_to_Closed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Closed_Won</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Set Opp Record Type to Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateSalesStatus</fullName>
        <description>Update sales status on opportunity closed lost.</description>
        <field>Sales_Status__c</field>
        <literalValue>Lost</literalValue>
        <name>UpdateSalesStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_StageName_with_Closed_Won</fullName>
        <field>StageName</field>
        <literalValue>Closed Won</literalValue>
        <name>Update StageName with Closed Won</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_to_Close_Lost</fullName>
        <field>StageName</field>
        <literalValue>Closed Lost</literalValue>
        <name>Update to Close Lost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	
	<fieldUpdates>
        <fullName>Update_Account_Account_Type_to_Customer</fullName>
        <field>Account_Type__c</field>
        <literalValue>Customer</literalValue>
        <name>Update Account Account Type to Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Account_Type_to_Cusomer</fullName>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Update Account Type to Cusomer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Also_Set_Is_Ever_Closed_for_Customer</fullName>
        <field>Is_Ever_Closed__c</field>
        <literalValue>1</literalValue>
        <name>Also Set Is Ever Closed for Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Billing_Status_to_Active</fullName>
        <field>Billing_Status__c</field>
        <literalValue>Active</literalValue>
        <name>Billing Status to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Type_to_Customer</fullName>
        <field>Type</field>
        <literalValue>Customer</literalValue>
        <name>Type to Customer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>AccountId</targetObject>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Record_Tupe_for_Non_SFDC_Order</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Prove_Close_Non_SFDC_Order</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Tupe for Non SFDC Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Date_Update</fullName>
        <description>To Update Close date field to SYSDATE</description>
        <field>CloseDate</field>
        <formula>TODAY()</formula>
        <name>Close Date Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set Close Date to Sysdate</fullName>
        <actions>
            <name>Close_Date_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To auto stamp CloseDate=SYSDATE only if the opportunity has never been closed earlier and is being closed for 1st time</description>
        <formula>OR(AND(NOT(Is_Ever_Closed__c), ISPICKVAL(StageName,&quot;Closed Sales&quot;),OR(ISPICKVAL(Sales_Status__c,&quot;Won&quot;),ISPICKVAL(Sales_Status__c,&quot;Lost&quot;))),AND(OR(ISPICKVAL(StageName,&quot;Propose&quot;),ISPICKVAL(StageName,&quot;Qualify&quot;),ISPICKVAL(StageName,&quot;Identify &amp; Define&quot;),ISPICKVAL(StageName,&quot;Develop&quot;),ISPICKVAL(StageName,&quot;InFlight&quot;),ISPICKVAL(StageName,&quot;Closed Lost&quot;),ISPICKVAL(StageName,&quot;Prove &amp; Close&quot;)),NOT(Is_Ever_Closed__c),ISPICKVAL(Sales_Status__c,&quot;Lost&quot;)))</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>	
    <rules>
        <fullName>Closed Won Opportunity Customer Type and Billing Status</fullName>
        <actions>
            <name>Also_Set_Is_Ever_Closed_for_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Billing_Status_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Type_to_Customer</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>Update_Account_Account_Type_to_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>To auto populate Account Type &amp; Billing Status as Customer &amp; Active respectively for the acccount whose opportunity stage is updated to Closed Won</description>
        <formula>AND(AND(ISPICKVAL( StageName , &apos;Closed Won&apos;),ISPICKVAL( Sales_Status__c,&apos;Won&apos;)),
OR( ISPICKVAL(Account.Billing_Status__c, &apos;Inactive&apos;), ISPICKVAL(Account.Type ,&apos;Prospect&apos;), ISPICKVAL(Account.Type ,&apos;Former Customer&apos;) ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>	
    <rules>
        <fullName>Change Opp Record Type When Closed Won</fullName>
        <actions>
            <name>Also_Set_Is_Ever_Closed_for_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Opp_Record_Type_to_Closed</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Prove &amp; Close,Closed Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <description>Assigns the &quot;Closed Won&quot; Record Type when the Stage is &quot;Prove &amp; Close&quot; and &quot;Sales Status&quot; is &quot;Won&quot;</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set StageName to Close Lost</fullName>
        <actions>
            <name>UpdateSalesStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_to_Close_Lost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Cancelled_Order__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Revise_Opportunity_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Stage name is %22Closed Sales%22</fullName>
        <actions>
            <name>Record_Type_to_Closed_Sales</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Closed Sales</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update StageName with Closed Won</fullName>
        <actions>
            <name>Update_StageName_with_Closed_Won</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Won</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Stage__c</field>
            <operation>equals</operation>
            <value>Prove &amp; Close</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Update Account Type to Customer</fullName>
        <actions>
            <name>Update_Account_Account_Type_to_Customer</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Type_to_Cusomer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(ISPICKVAL(StageName ,&apos;Closed Won&apos;), ISPICKVAL(Sales_Status__c,&apos;Won&apos;),  Account.Id!= NULL,   OR(ISPICKVAL(Account.Type ,&apos;Former Customer&apos;),ISPICKVAL(Account.Type ,&apos;Prospect&apos;)) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Non - SFDC Order Stage Name as %22Prove %26 Close%22 and %22Sales Status%22 as Open</fullName>
        <actions>
            <name>Record_Tupe_for_Non_SFDC_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>equals</operation>
            <value>Prove &amp; Close</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Sales_Status__c</field>
            <operation>equals</operation>
            <value>Open</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.R3_Products_Opportunity__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>This Work flow sets the record type to &quot;Prove &amp; Close&quot; when the stage is &quot;Prove &amp; Close&quot; and Sales Status is &quot;Open</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
	 <rules>
        <fullName>Truncate Feasibility study detail</fullName>
        <actions>
            <name>update_feasibilitytest</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISCHANGED(Feasibility_Result_Details__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
