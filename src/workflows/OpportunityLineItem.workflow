<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Calculated_Total_SOV</fullName>
        <field>Calculated_Total_SOV__c</field>
        <formula>Calculated_New_SOV__c + Calculated_Renewal_SOV__c</formula>
        <name>Calculated Total SOV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Term</fullName>
        <field>ContractTerm__c</field>
        <formula>Text(cscfga__Attribute__r.cscfga__Product_Configuration__r.cscfga__Contract_Term__c)</formula>
        <name>Contract Term</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Contract_Value</fullName>
        <field>Contract_Value__c</field>
        <formula>IF(ISNULL(VALUE(VariationToContractTerms__c) ),
IF(ISNULL(VALUE(ContractTerm__c) ),
NetNRCPrice__c,
(VALUE(ContractTerm__c) * Net_MRC_Price__c) + NetNRCPrice__c),
(VALUE(VariationToContractTerms__c) * Net_MRC_Price__c) + NetNRCPrice__c)</formula>
        <name>Contract Value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Net_NRC_Price</fullName>
        <field>NetNRCPrice__c</field>
        <formula>cscfga__Attribute__r.cscfga__Product_Configuration__r.Offer_new_Nrc_c__c</formula>
        <name>Net NRC Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reported_New_SOV</fullName>
        <field>Reported_New_SOV__c</field>
        <formula>Calculated_New_SOV__c</formula>
        <name>Reported New SOV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Reported_Renewal_SOReported_Renewal_SOV</fullName>
        <field>Reported_Renewal_SOV__c</field>
        <formula>Calculated_Renewal_SOV__c</formula>
        <name>Reported Renewal SOReported Renewal SOV</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_NetMRC_Price</fullName>
        <field>Net_MRC_Price__c</field>
        <formula>cscfga__Attribute__r.cscfga__Price__c</formula>
        <name>Set the NetMRC Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_the_NetMRC_Price1</fullName>
        <field>Net_MRC_Price__c</field>
        <formula>0.00</formula>
        <name>Set the NetMRC Price1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_the_NetNRC_price</fullName>
        <field>NetNRCPrice__c</field>
        <formula>cscfga__Attribute__r.cscfga__Price__c</formula>
        <name>set the NetNRC price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>set_the_NetNRC_price1</fullName>
        <field>NetNRCPrice__c</field>
        <formula>0.00</formula>
        <name>set the NetNRC price1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Is_usage_line_item</fullName>
        <field>Is_usage_line_item__c</field>
        <formula>if(cscfga__Attribute__r.cscfga__Is_rate_line_item__c,&apos;Yes&apos;,&apos;No&apos;)</formula>
        <name>Update Is usage line item</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Is usage line item check</fullName>
        <actions>
            <name>Update_Is_usage_line_item</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>cscfga__Attribute__c != null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>	
    <rules>
        <fullName>Contract Term</fullName>
        <actions>
            <name>Contract_Term</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OpportunityId &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Contract Value</fullName>
        <actions>
            <name>Contract_Value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OpportunityId &lt;&gt; &apos;&apos;</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>		
    </rules>
    <rules>
        <fullName>Net NRC Price</fullName>
        <actions>
            <name>Net_NRC_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>OpportunityId  &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Opportunity Product created</fullName>
        <actions>
            <name>Reported_New_SOV</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Reported_Renewal_SOReported_Renewal_SOV</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the NetMRC Price</fullName>
        <actions>
            <name>Set_the_NetMRC_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_the_NetNRC_price1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update the NetNRC Price</fullName>
        <actions>
            <name>Set_the_NetMRC_Price1</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>set_the_NetNRC_price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>OpportunityLineItem.Recurring__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
