<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Create_Service_Name</fullName>
        <field>Name</field>
        <formula>IF(Primary_Service_ID__c !=NULL, LEFT(Primary_Service_ID__c, 80), IF(Group_Service_ID__c !=NULL,LEFT(Group_Service_ID__c, 80),IF(Master_Service_ID__c !=NULL, LEFT(Master_Service_ID__c, 80), Id) ))</formula>
        <name>Create Service Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>GIAAS_Service_Field_Update</fullName>
        <description>GIAAS Service Field Update</description>
        <field>RecordTypeId</field>
        <lookupValue>GIAAS_Service_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>GIAAS Service Field Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Layout_update</fullName>
        <field>RecordTypeId</field>
        <lookupValue>TI_Service_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Service Layout update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Old_MRC</fullName>
        <description>Set the Old MRC field with the previous value of MRC field</description>
        <field>Old_MRC__c</field>
        <formula>PRIORVALUE( MRC_Cost__c )</formula>
        <name>Update Old MRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GIAAS Service WF</fullName>
        <actions>
            <name>GIAAS_Service_Field_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>For flipping the GIAAS specific Layout</description>
        <formula>Product__r.Is_GIAAS__c</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Populate Service Name</fullName>
        <actions>
            <name>Create_Service_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate Service Name</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Service Layout Assigning</fullName>
        <actions>
            <name>Service_Layout_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Product__r.Is_GIAAS__c = False</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Old MRC</fullName>
        <actions>
            <name>Update_Old_MRC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Whenever Value on MRC field is changed old value is captured in Old MRC field.</description>
        <formula>ISCHANGED( MRC_Cost__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>