<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
<fieldUpdates>
        <fullName>Cloning_parent_field_value</fullName>
        <field>Cloning_Parent__c</field>
        <name>Cloning parent field value</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>update_cloning_Id</fullName>
        <field>Cloning_Parent__c</field>
        <formula>if(Order_Type__c!=&apos;New Provide&apos;,Id,Cloning_Parent__c)</formula>
        <name>update cloning Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_Hierarchy</fullName>
        <field>Hierarchy__c</field>
        <formula>if(Parent_Configuration_For_Sequencing__c==null,Text(Hierarchy_Product_Sequence__c), 
if(Row_Sequence__c&gt;0,Parent_Configuration_For_Sequencing__r.Hierarchy__c+&quot;.&quot;+Text(Hierarchy_Row_Sequence__c), 
Parent_Configuration_For_Sequencing__r.Hierarchy__c+&quot;.&quot;+Text(Hierarchy_Product_Sequence__c)) 
)</formula>
        <name>update Hierarchy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_root_Id</fullName>
        <field>Root_Parent_Configuration_For_Sequencing__c</field>
        <formula>If(Parent_Configuration_For_Sequencing__c==null,Id,
Parent_Configuration_For_Sequencing__r.Root_Parent_Configuration_For_Sequencing__c
)</formula>
        <name>update root Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>IsOffnet_No</fullName>
        <field>Is_Offnet__c</field>
        <formula>&quot;No&quot;</formula>
        <name>IsOffnet=No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateSequenceKey</fullName>
        <field>Sequenc_Key__c</field>
        <formula>if(Parent_Configuration_For_Sequencing__c==null,Product_Sequence__c, 
if(Row_Sequence__c&gt;0,
if(LEN(text(Row_Sequence__c))==1,Parent_Configuration_For_Sequencing__r.Sequenc_Key__c+&apos;.00&apos;+text(Row_Sequence__c),
if(LEN(text(Row_Sequence__c))==2,Parent_Configuration_For_Sequencing__r.Sequenc_Key__c+&apos;.0&apos;+text(Row_Sequence__c),
Parent_Configuration_For_Sequencing__r.Sequenc_Key__c+&quot;.&quot;+text(Row_Sequence__c))
)
, 
Parent_Configuration_For_Sequencing__r.Sequenc_Key__c+&quot;.&quot;+Product_Sequence__c) 
)</formula>
        <name>updateSequenceKey</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Map_name_identifier_to_name_for_Master_S</fullName>
        <description>Map name identifier to name for Master S</description>
        <field>Name</field>
        <formula>Name_Identifier__c</formula>
        <name>Map name identifier to name for Master S</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mark_Offnet_Product_Field_as_Yes</fullName>
        <field>Offnet_Product__c</field>
        <formula>&apos;Yes&apos;</formula>
        <name>Mark Offnet Product Field as Yes</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Product_Name_Update</fullName>
        <field>Product_Name__c</field>
        <formula>Name</formula>
        <name>Product Name Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>	
	    <fieldUpdates>
        <fullName>Update_Customer_Type</fullName>
        <field>Customer_Type__c</field>
        <formula>AccountCustomerType__c</formula>
        <name>Update Customer Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
        <fullName>Cloning parent set Null</fullName>
        <actions>
            <name>Cloning_parent_field_value</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>1==1</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>IsOffnetDefaultNo</fullName>
        <actions>
            <name>IsOffnet_No</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.Is_Offnet__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>updateParentProductSequence</fullName>
        <actions>
            <name>updateSequenceKey</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_Hierarchy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_cloning_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_root_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Map name identifier to name for Master Services</fullName>
        <actions>
            <name>Map_name_identifier_to_name_for_Master_S</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.Name_Identifier__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Map name identifier to name for Master Services</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Mark Offnet Products as Yes</fullName>
        <actions>
            <name>Mark_Offnet_Product_Field_as_Yes</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.cscfga__Product_Family__c</field>
            <operation>equals</operation>
            <value>Local Access</value>
        </criteriaItems>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.cscfga__Product_Family__c</field>
            <operation>equals</operation>
            <value>Local Loop</value>
        </criteriaItems>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.cscfga__Product_Family__c</field>
            <operation>equals</operation>
            <value>GCPE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Product Name set</fullName>
        <actions>
            <name>Product_Name_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>cscfga__Product_Configuration__c.Name</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
	    <rules>
        <fullName>Update Customer Type</fullName>
        <actions>
            <name>Update_Customer_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AccountCustomerType__c  &lt;&gt; Customer_Type__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	
</Workflow>
