<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">  
<alerts>
        <fullName>Misc_Transaction_Approval_Email_Template</fullName>
        <description>Misc. Transaction Approval Email Template</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Misc_Transaction_Approved_Rejected</template>
    </alerts>
	<fieldUpdates>
        <fullName>Update_Transaction_Status</fullName>
        <field>Transaction_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Update Transaction Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Approved_Rejected_EMP_No</fullName>
        <field>Approved_Rejected__c</field>
        <formula>$User.EmployeeNumber</formula>
        <name>Update Approved/Rejected EMP No</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approved_Rejected_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved_Rejected_Miscellaneous_Service</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Approved/Rejected Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Transaction_Rejected_Status</fullName>
        <field>Transaction_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Update Transaction Rejected Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Approved_Rejected_Date</fullName>
        <field>Approved_Rejected_Date__c</field>
        <formula>now()</formula>
        <name>Update Approved/Rejected Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
  <fieldUpdates>
        <fullName>Update_Approved_Rejected_By</fullName>
        <field>Approved_Rejected_By__c</field>
        <formula>$User.FirstName &amp; &apos; &apos; &amp; $User.LastName</formula>
        <name>Update Approved/Rejected By</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>AEndZipPlus</fullName>
        <field>AEndZipPlus4__c</field>
        <formula>csord__Service__r.Acity_Side__r.PostalCode__c</formula>
        <name>AEndZipPlus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>BEndZipPlus</fullName>
        <field>BEndZipPlus4__c</field>
        <formula>csord__Service__r.Site_B__r.PostalCode__c</formula>
        <name>BEndZipPlus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bill_Text</fullName>
        <field>Bill_Texts__c</field>
        <formula>Bill_Text__c</formula>
        <name>Bill Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bill_Texts</fullName>
        <field>Bill_Texts__c</field>
        <formula>Bill_Text__c</formula>
        <name>Bill Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChargeIDMRC</fullName>
        <field>Charge_ID__c</field>
        <formula>Chargeid_MRC__c</formula>
        <name>ChargeIDMRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ChargeIDNRC</fullName>
        <field>Charge_ID__c</field>
        <formula>ChargeId_AutoNumber__c</formula>
        <name>ChargeIDNRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Charge_Amounts</fullName>
        <field>Charge_Amounts__c</field>
        <formula>Charge_Amount__c</formula>
        <name>Charge Amounts</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Charge_Frequency</fullName>
        <field>Charging_Frequency__c</field>
        <formula>IF( CONTAINS(Text(Bill_Profile__r.Billing_Frequency__c), &apos;Bi-Annual&apos;), &apos;BiYearly&apos;, IF( CONTAINS(Text(Bill_Profile__r.Billing_Frequency__c),&apos;Monthly&apos;),&apos;Monthly&apos;,IF( CONTAINS(Text(Bill_Profile__r.Billing_Frequency__c),&apos;Quarterly&apos;),&apos;Quarterly&apos;,&apos;Annually&apos;)))</formula>
        <name>Charge Frequency</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Charge_Type_MRC_SLI</fullName>
        <field>Type_of_Charge__c</field>
        <literalValue>RCH - Recurring charge</literalValue>
        <name>Charge Type MRC SLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Charge_Type_NRC_SLI</fullName>
        <field>Type_of_Charge__c</field>
        <literalValue>OCH - One off charge</literalValue>
        <name>Charge Type NRC SLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MRC_Price</fullName>
        <field>MRC_Price__c</field>
        <formula>csord__Service__r.MRC_Cost__c</formula>
        <name>MRC Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MRC_Service_Line_item_Rec_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>MRC_Service_Line_item</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>MRC Service Line item Rec Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NRCInstallmentNRC</fullName>
        <field>Installment_NRC_Charge_ID__c</field>
        <formula>ChargeId_AutoNumber__c + &quot; - INS&quot;</formula>
        <name>NRCInstallmentNRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NRC_Price</fullName>
        <field>NRC_Price__c</field>
        <formula>csord__Service__r.NRC_Cost__c</formula>
        <name>NRC Price</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NRC_Service_Line_item_Rec_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>NRC_Service_Line_item</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NRC Service Line item Rec Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Net_MRC</fullName>
        <field>Net_MRC_Price__c</field>
        <formula>csord__Total_Price__c</formula>
        <name>Net MRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Net_NRC</fullName>
        <field>Net_NRC_Price__c</field>
        <formula>csord__Total_Price__c</formula>
        <name>Net NRC</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NewMiscellaneousService</fullName>
        <field>Is_Miscellaneous_Credit_Flag__c</field>
        <literalValue>1</literalValue>
        <name>NewMiscellaneousService</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ParentBundle</fullName>
        <field>Parent_of_Bundle__c</field>
        <literalValue>1</literalValue>
        <name>ParentBundle</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>U2cMasterCode</fullName>
        <field>U2C_Master_Code__c</field>
        <formula>csord__Service__r.U2CMasterCode__c</formula>
        <name>U2cMasterCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Mrc_cost</fullName>
        <field>MRC_Cost__c</field>
        <formula>csord__Service__r.MRC_Cost__c</formula>
        <name>Update Mrc cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Nrc_cost</fullName>
        <field>NRC_Cost__c</field>
        <formula>csord__Service__r.NRC_Cost__c</formula>
        <name>Update Nrc cost</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
		<fieldUpdates>
        <fullName>Charge_Type_Installment_NRC_SLI</fullName>
        <field>Type_of_Charge__c</field>
        <literalValue>INRC - Installment NRC</literalValue>
        <name>Charge Type Installment NRC SLI</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Installment_NRC_Start_Date</fullName>
        <field>Installment_NRC_Start_Date__c</field>
        <formula>csord__Service__r.Billing_Commencement_Date__c</formula>
        <name>Update Installment NRC Start Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Installment_end_date</fullName>
        <field>Installment_NRC_End_Date__c</field>
        <formula>DATE( 
year(Installment_NRC_Start_Date__c) 
+ floor((month(Installment_NRC_Start_Date__c) + Installment_Duration__c)/12) + if(and(month(Installment_NRC_Start_Date__c)=12,Installment_Duration__c&gt;=12),-1,0) 
, 
if( mod( month(Installment_NRC_Start_Date__c) + Installment_Duration__c, 12 ) = 0, 12 , mod( month(Installment_NRC_Start_Date__c) + Installment_Duration__c, 12 )) 
, 
min( 
day(Installment_NRC_Start_Date__c), 
case( 
max( mod( month(Installment_NRC_Start_Date__c) + Installment_Duration__c, 12 ) , 1), 
9,30, 
4,30, 
6,30, 
11,30, 
2,if(mod((year(Installment_NRC_Start_Date__c) 
+ floor((month(Installment_NRC_Start_Date__c) + Installment_Duration__c)/12) + if(and(month(Installment_NRC_Start_Date__c)=12,Installment_Duration__c&gt;=12),-1,0)),4)=0,29,28), 
31 
) 
) 
)</formula>
        <name>Update Installment end date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_MISC_Name</fullName>
        <field>Name</field>
        <formula>Name+&apos; &apos;+ Misc_Unique_Name__c</formula>
        <name>Update MISC Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Update Misc Line Item Name</fullName>
        <actions>
            <name>Update_MISC_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISNEW(),Is_Miscellaneous_Credit_Flag__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>	
    <rules>
        <fullName>AEndZipPlus</fullName>
        <actions>
            <name>AEndZipPlus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Site_A_For_ROC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>BEndZipPlus</fullName>
        <actions>
            <name>BEndZipPlus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Site_B_For_ROC__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bill Text</fullName>
        <actions>
            <name>Bill_Texts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Bill_Texts__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bundled Flags</fullName>
        <actions>
            <name>ParentBundle</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>csord__Service__c.Parent_Bundle_Flag__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Charge Amounts</fullName>
        <actions>
            <name>Charge_Amounts</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Charge_Amounts__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Charge_Amounts__c</field>
            <operation>equals</operation>
            <value>0.00</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Charge Frequency</fullName>
        <actions>
            <name>Charge_Frequency</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Bill_Profile_Integration_Number__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MRC ChargeId</fullName>
        <actions>
            <name>ChargeIDMRC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
         <formula>AND(Is_Miscellaneous_Credit_Flag__c = false, csord__Is_Recurring__c = true, csord__Service__r.csordtelcoa__Replaced_Service__c = Null, ISBLANK( csord__Service__r.Full_Load__c ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>MRC Price</fullName>
        <actions>
            <name>MRC_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_Miscellaneous_Credit_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>MRC Service Line item page Layout</fullName>
        <actions>
            <name>MRC_Service_Line_item_Rec_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_Miscellaneous_Credit_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>NRC Price</fullName>
        <actions>
            <name>NRC_Price</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_Miscellaneous_Credit_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NRC Service Line item page Layout</fullName>
        <actions>
            <name>ChargeIDNRC</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NRC_Service_Line_item_Rec_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( csord__Is_Recurring__c  = false, Is_Miscellaneous_Credit_Flag__c = false,Is_ETC_Line_Item__c=false)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NRCInstallmentNRC</fullName>
        <actions>
            <name>NRCInstallmentNRC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( csord__Is_Recurring__c  = false, Is_Miscellaneous_Credit_Flag__c = false,Installment_NRC_Amount_per_month__c &lt;&gt; 0)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Net MRC Price</fullName>
        <actions>
            <name>Net_MRC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_Miscellaneous_Credit_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
		 <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_ETC_Line_Item__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Net NRC Price</fullName>
        <actions>
            <name>Net_NRC</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_Miscellaneous_Credit_Flag__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
		 <criteriaItems>
            <field>csord__Service_Line_Item__c.Is_ETC_Line_Item__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NewMiscellaneousService</fullName>
        <actions>
            <name>NewMiscellaneousService</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Miscellaneous Service</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>U2C Master Code</fullName>
        <actions>
            <name>U2cMasterCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>csord__Service__c &lt;&gt; Null</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Charge Type for MRC SLI</fullName>
        <actions>
            <name>Charge_Type_MRC_SLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.csord__Is_Recurring__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Type_of_Charge__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
        <rules>
        <fullName>Update Charge Type for NRC SLI</fullName>
        <actions>
            <name>Charge_Type_NRC_SLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(csord__Is_Recurring__c == False &amp;&amp;   ISPICKVAL(Type_of_Charge__c,&apos;&apos;) &amp;&amp; ( Installment_NRC__c == &apos;No&apos; ||  Installment_NRC__c == &apos;&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>Update MRC Cost and NRC Cost</fullName>
        <actions>
            <name>Update_Mrc_cost</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Nrc_cost</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update cost in service line item from product configuration mrc and nrc cost.</description>
        <formula>OR(ISNULL(MRC_Cost__c) , ISNULL(NRC_Cost__c) )</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Installment workflow rule</fullName>
        <actions>
            <name>Update_Installment_NRC_Start_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updating installment details in ServiceLineItem</description>
        <formula>!(ISNULL( csord__Service__r.Billing_Commencement_Date__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Charge Type for Installment NRC SLI</fullName>
        <actions>
            <name>Charge_Type_Installment_NRC_SLI</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>(csord__Is_Recurring__c == False &amp;&amp;   ISPICKVAL(Type_of_Charge__c,&apos;&apos;) &amp;&amp; Installment_NRC__c == &apos;Yes&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>update install end date</fullName>
        <actions>
            <name>Update_Installment_end_date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service_Line_Item__c.Installment_NRC_Start_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Updating installment details in ServiceLineItem</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
