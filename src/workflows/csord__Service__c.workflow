<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Email_sent_on_supplier_price_changes_in_off_net_components</fullName>
        <description>Email sent on supplier price changes in off-net components.</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Notify_Opportunity_Owner_when_supplier_price_changes</template>
    </alerts>
    <alerts>
        <fullName>Notify_Account_Owner</fullName>
        <description>Notify Account owner on Service expire..</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Related_User__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Notify_Account_Owner_on_Service_Expiry</template>
    </alerts>
    <alerts>
        <fullName>Notify_Service_Order_Owner_if_the_Customer_Required_date_is_postponed</fullName>
        <description>Notify Service Order Owner if the Customer Required date is postponed</description>
        <protected>false</protected>
        <recipients>
            <field>SD_PM_Contact__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>P2A_Templates/Notify_Service_Order_Owner_if_the_Customer_Required_Terminationdate_is_postponed</template>
    </alerts>
	<fieldUpdates>
        <fullName>Inventory_Status_Update</fullName>
        <description>Inventory Status Update for Master</description>
        <field>Inventory_Status__c</field>
        <formula>&apos;LIVE - AS DESIGNED&apos;</formula>
        <name>Inventory Status Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_Service_Name</fullName>
        <field>Name</field>
        <formula>IF(Resource_Id__c==null ,if(Primary_Service_ID__c==null,Product_Id__c,Primary_Service_ID__c), Resource_Id__c)</formula>
        <name>Update Service Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>updatesubscriptionname</fullName>
        <field>Name</field>
        <formula>&apos;SN-&apos;+Name</formula>
        <name>updatesubscriptionname</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>        
		<targetObject>csord__Subscription__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bundle_Action_for_Provide</fullName>
        <field>Bundle_Action__c</field>
        <formula>&quot;NEW&quot;</formula>
        <name>Bundle Action for Provide</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bundle_Flags</fullName>
        <field>Bundle_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Bundle Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Bundle_label_name</fullName>
        <field>Bundle_Label_name__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Bundle_Label_name__c</formula>
        <name>Bundle label name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Service_Bill_Text</fullName>
        <description>service bill text will have the value of replaced service&apos;s service bill text for &apos;terminate&apos; or &apos;renewal&apos; order, however the user can alter the value any time at will;developer:ritesh;date:17-jan-2017;defect fix:13711
developer:sowmya defect:15084</description>
        <field>Service_Bill_Text__c</field>
        <formula>if((csord__Order__r.Order_Type__c==&apos;Termination&apos;)&amp;&amp; csordtelcoa__Replaced_Service__r.Service_Bill_Text__c!=NULL, 
if(Service_Bill_Text__c==null,csordtelcoa__Replaced_Service__r.Service_Bill_Text__c,Service_Bill_Text__c), 
Service_Bill_Text__c)</formula>
        <name>Service Bill Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Order_Type_Terminate</fullName>
        <description>Set the Order Type to Terminate if the Cease Flag is true.</description>
        <field>Order_Type_Final__c</field>
        <formula>IF(Cease_Service_Flag__c, &quot;Terminate&quot;,  Order_Type_Final__c)</formula>
        <name>Set Order Type Terminate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>	
    <fieldUpdates>
        <fullName>Charge_ID</fullName>
        <field>Charge_ID__c</field>
        <formula>IF(csordtelcoa__Replaced_Service__c = Null,  Bundle_Default_Value__c ,  csordtelcoa__Replaced_Service__r.Charge_ID__c )</formula>
        <name>Charge ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_24H</fullName>
        <field>X24_hours_has_passed_since_creation__c</field>
        <literalValue>0</literalValue>
        <name>Clear 24H</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Clear_24HB</fullName>
        <field>X24_hours_has_passed_since_creation__c</field>
        <literalValue>0</literalValue>
        <name>Clear 24H</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Bundle_Action_Name_for_Terminate</fullName>
        <field>Bundle_Action__c</field>
        <formula>&apos;TERMINATE&apos;</formula>
        <name>Default Bundle Action Name for Terminate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Bundle_Label_Name_for_Change_Ord</fullName>
        <field>Bundle_Label_name__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Bundle_Label_name__c</formula>
        <name>Default Bundle Label Name for Change Ord</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Default_Bundle_Label_Name_for_New_Provid</fullName>
        <field>Bundle_Label_name__c</field>
        <formula>Bundle_Default_Value__c</formula>
        <name>Default Bundle Label Name for New Provid</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Discount_Bill_Text</fullName>
        <field>Discount_Bill_Text__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Discount_Bill_Text__c</formula>
        <name>Discount Bill Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>ETC_bill_text</fullName>
        <field>ETC_Bill_Text__c</field>
        <formula>csordtelcoa__Replaced_Service__r.ETC_Bill_Text__c</formula>
        <name>ETC bill text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Sent_Status</fullName>
        <field>email_sent__c</field>
        <literalValue>0</literalValue>
        <name>Email Sent Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OffNet_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>OffNet_Record_Type_IP006</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>OffNet Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>OffnetYesSubscription</fullName>
        <field>HasOffnetService__c</field>
        <literalValue>1</literalValue>
        <name>OffnetYesSubscription</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <targetObject>csord__Subscription__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Parent_Bundle_Flag</fullName>
        <field>Parent_Bundle_Flag__c</field>
        <literalValue>1</literalValue>
        <name>Parent Bundle Flag</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Parent_Inventory_Id</fullName>
        <field>Parent_Inventory_Instance_ID__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Parent_Inventory_Instance_ID__c</formula>
        <name>Parent Inventory Id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    
    <fieldUpdates>
        <fullName>Prepopulate_primary_service_id</fullName>
        <field>Primary_Service_ID__c</field>
        <formula>Replaced_Primary_Service_ID__c</formula>
        <name>Prepopulate primary service id</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Replaced_Resource_ID</fullName>
        <field>Replaced_Resource_ID__c</field>
        <formula>csordtelcoa__Replaced_Service__r.ResourceId__c</formula>
        <name>Replaced Resource ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>PrimaryServiceID_Change_Order</fullName>
        <field>Primary_Service_ID__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Primary_Service_ID__c</formula>
        <name>PrimaryServiceID Change Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Primary_Service</fullName>
        <field>Primary_Service_ID__c</field>
        <formula>Replaced_Primary_Service_ID__c</formula>
        <name>Primary Service</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RecordType_Field_Update_OffNet</fullName>
        <field>RecordTypeId</field>
        <lookupValue>OffNet_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RecordType Field Update OffNet</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
	    <fieldUpdates>
        <fullName>update_master_Service_IP006_rtype</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Master_Service_IP006</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>update master Service IP006 rtype</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>	
    <fieldUpdates>
        <fullName>ResourceId_Change_Order</fullName>
        <field>ResourceId__c</field>
        <formula>csordtelcoa__Replaced_Service__r.ResourceId__c</formula>
        <name>ResourceId Change Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Resource_ID_Change_Order</fullName>
        <field>ResourceId__c</field>
        <formula>csordtelcoa__Replaced_Service__r.ResourceId__c</formula>
        <name>Resource ID Change Order</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Root_Bill_Text</fullName>
        <field>Root_Bill_Text__c</field>
        <formula>if((csord__Order__r.Order_Type__c==&apos;Termination&apos;)&amp;&amp; csordtelcoa__Replaced_Service__r.Root_Bill_Text__c!=NULL, 
if(Root_Bill_Text__c==null,csordtelcoa__Replaced_Service__r.Root_Bill_Text__c,Root_Bill_Text__c), 
Root_Bill_Text__c)</formula>
        <name>Root Bill Text</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>    
    <fieldUpdates>
        <fullName>Set_Selling_Entity</fullName>
        <field>Selling_Entity__c</field>
        <formula>TEXT(AccountId__r.Selling_Entity__c)</formula>
        <name>Set Selling Entity</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Service_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Service_IP006</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Service Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_A_Side_Site_Code</fullName>
        <field>A_Side_Site__c</field>
        <formula>IF( OR( Product_Code__c ==&apos;NID&apos;, Product_Code__c ==&apos;EMC&apos;), Acity_Side__r.Customer_Specific_Site_Code__c, A_City_Generic_Code__c)</formula>
        <name>Set A Side Site Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bundle_Action_Modify</fullName>
        <field>Bundle_Action__c</field>
        <formula>&apos;MODIFY&apos;</formula>
        <name>Set Bundle Action Modify</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bundle_Action_New</fullName>
        <field>Bundle_Action__c</field>
        <formula>&apos;New&apos;</formula>
        <name>Set Bundle Action New</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Bundle_Action_for_NP</fullName>
        <field>Bundle_Action__c</field>
        <formula>&apos;NEW&apos;</formula>
        <name>Set Bundle Action for NP</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_MES_Circuit_Type_Value_For_MES</fullName>
        <field>Circuit_Type__c</field>
        <formula>&quot;MES OSS, Z Only&quot;</formula>
        <name>Set MES Circuit Type Value For MES</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_ResourceId_from_formula_field</fullName>
        <field>ResourceId__c</field>
        <formula>Resource_Id__c</formula>
        <name>Set ResourceId from formula field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Z_Side_Site_Code</fullName>
        <field>Z_Side_Site__c</field>
        <formula>IF( OR( Product_Code__c ==&apos;NID&apos;, Product_Code__c ==&apos;EMC&apos;), Zcity_side__r.Customer_Specific_Site_Code__c, Z_City_Generic_Code__c)</formula>
        <name>Set Z-Side Site Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRootProductCode</fullName>
        <field>System_Root_Product_Code__c</field>
        <formula>Root_Product_Code__c</formula>
        <name>UpdateRootProductCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateRootProductId</fullName>
        <field>System_Root_Product_Id__c</field>
        <formula>Root_Product_ID__c</formula>
        <name>UpdateRootProductId</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_24h_has_passed2</fullName>
        <field>X24_hours_has_passed_since_creation__c</field>
        <literalValue>1</literalValue>
        <name>Update 24h has passed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Amber_Status2</fullName>
        <field>RAG_Status_Amber__c</field>
        <literalValue>1</literalValue>
        <name>Update Amber Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    
	 <fieldUpdates>
        <fullName>Service_Item_Number</fullName>
        <description>This will update the Service item number for change orders</description>
        <field>ServiceItemNumber__c</field>
        <formula>csordtelcoa__Replaced_Service__r.ServiceItemNumber__c</formula>
        <name>Service Item Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Service_Names</fullName>
        <field>Name</field>
        <formula>Primary_Service_ID__c</formula>
        <name>Update Service Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>email_sent_checkbox_to_true</fullName>
        <field>email_sent__c</field>
        <literalValue>1</literalValue>
        <name>email sent checkbox to true</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateU2CMasterCode</fullName>
        <field>U2CMasterCode__c</field>
        <formula>U2C_Master_Code_Cal__c</formula>
        <name>updateU2CMasterCode</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>GBandwidthPopulationFieldUpdate</fullName>
        <field>G_Bandwidth_Gen_name__c</field>
        <formula>if(Bandwidth__c==NULL,&apos;MANAGED SERVICE&apos;,Bandwidth__c)</formula>
        <name>GBandwidthPopulationFieldUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Instance_ID</fullName>
        <field>Path_Instance_ID__c</field>
        <formula>csordtelcoa__Replaced_Service__r.Path_Instance_ID__c</formula>
        <name>Update Inventory Instance ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Inventory_Status</fullName>
        <field>Inventory_Status__c</field>
        <formula>if(AND(LOWER(Inventory_Status__c) &lt;&gt; &apos;decommissioned&apos;, LOWER(Inventory_Status__c) &lt;&gt; &apos;live - as designed&apos;), &apos;LIVE - AS DESIGNED&apos;, Inventory_Status__c)</formula>
        <name>Update Inventory Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Inventory_Status_field_update</fullName>
        <field>Inventory_Status__c</field>
        <formula>csord__Service__r.Inventory_Status__c</formula>
        <name>Inventory Status field update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Update_OLA_Value_to_15</fullName>
        <field>OLA__c</field>
        <formula>15</formula>
        <name>Update OLA Value to 15</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<rules>
        <fullName>UpdateInventoryStatusforMaster</fullName>
        <actions>
            <name>Inventory_Status_Update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update Inventory Status for Master</description>
        <formula>AND(Product_Configuration_Type__c  &lt;&gt; &apos;New Provide&apos;, Inventory_Status__c  &lt;&gt; &apos;LIVE - AS DESIGNED&apos;,  OR( Product_Id__c  = &apos;VLM&apos;,Product_Id__c  = &apos;IPVPN&apos;,Product_Id__c  = &apos;GCC&apos;),  csord__Order__r.Is_Order_Submitted__c  = true)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Set OLA for Onnet Products</fullName>
        <actions>
            <name>Update_OLA_Value_to_15</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(ISNULL(OLA__c),OR( CONTAINS( Product_Id__c , &apos;CUSTSITEONNETPOP&apos;) ,CONTAINS( Product_Id__c , &apos;OFFPOPA&apos;),CONTAINS( Product_Id__c , &apos;OFFPOPA&apos;),CONTAINS( Product_Id__c , &apos;OFFPOPB&apos;),CONTAINS( Product_Id__c , &apos;IPVPN-BACKPOP-ON&apos;),CONTAINS( Product_Id__c , &apos;EVP&apos;),CONTAINS( Product_Id__c , &apos;EVC&apos;),CONTAINS( Product_Id__c , &apos;EVT&apos;) ,CONTAINS( Product_Id__c , &apos;BackUp-VLP-Onnet&apos;),CONTAINS( Product_Id__c , &apos;VLP-Onnet&apos;),CONTAINS( Product_Id__c , &apos;GIDSECP&apos;),CONTAINS( Product_Id__c , &apos;GIDSECO&apos;),CONTAINS( Product_Id__c , &apos;GIDSSTD&apos;),CONTAINS( Product_Id__c , &apos;ITP-BACKPOP&apos;),CONTAINS( Product_Id__c , &apos;IPTCUSTSITE&apos;),CONTAINS( Product_Id__c , &apos;IPTS-C&apos;),CONTAINS( Product_Id__c , &apos;IPTS-STD&apos;),CONTAINS( Product_Id__c , &apos;IPTS-PLA&apos;),CONTAINS( Product_Id__c , &apos;IPTM-PLA&apos;),CONTAINS( Product_Id__c , &apos;IPTM-STD&apos;),CONTAINS( Product_Id__c , &apos;IPTCUSTSITE&apos;),CONTAINS( Product_Id__c , &apos;VLV&apos;),CONTAINS( Product_Id__c , &apos;VLM&apos;),CONTAINS( Product_Id__c , &apos;VLG&apos;),CONTAINS( Product_Id__c , &apos;VLI&apos;),CONTAINS( Product_Id__c , &apos;VLL-A&apos;),CONTAINS( Product_Id__c , &apos;IPVPN&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Bundle Flag</fullName>
        <actions>
            <name>Bundle_Flags</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(csordtelcoa__Replaced_Service__r.Bundle_Flag__c = TRUE,Bundle_Flag__c = FALSE)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Change Order</fullName>
        <actions>
            <name>Bundle_label_name</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Discount_Bill_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ETC_bill_text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PrimaryServiceID_Change_Order</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>Replaced_Resource_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>ResourceId_Change_Order</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Resource_ID_Change_Order</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>Service_Bill_Text</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Root_Bill_Text</name>
            <type>FieldUpdate</type>
        </actions>        
        <active>true</active>
        <formula>csordtelcoa__Replaced_Service__c &lt;&gt; null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Charge ID Populate</fullName>
        <actions>
            <name>Charge_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(csord__Order__c &lt;&gt;&apos;&apos;, Full_Load__c &lt;&gt; &apos;&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Create tasks for Contract Expire</fullName>
        <active>false</active>
        <formula>( Contract_Expiry_Date__c - Today()) = 90</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Default Bundle Action Name for Change Order</fullName>
        <actions>
            <name>Set_Bundle_Action_Modify</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Product_Configuration_Type__c</field>
            <operation>notEqual</operation>
            <value>New Provide</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Product_Configuration_Type__c</field>
            <operation>notEqual</operation>
            <value>Terminate</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Ordertype__c</field>
            <operation>notEqual</operation>
            <value>PROVIDE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Bundle Action Name for Change Order Provide</fullName>
        <actions>
            <name>Bundle_Action_for_Provide</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>csord__Service__c.Ordertype__c</field>
            <operation>equals</operation>
            <value>PROVIDE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Bundle Action Name for New Provide</fullName>
        <actions>
            <name>Set_Bundle_Action_for_NP</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Product_Configuration_Type__c</field>
            <operation>equals</operation>
            <value>New Provide</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Ordertype__c</field>
            <operation>equals</operation>
            <value>PROVIDE</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Full_Load__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Bundle Action Name for Terminate</fullName>
        <actions>
            <name>Default_Bundle_Action_Name_for_Terminate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Product_Configuration_Type__c</field>
            <operation>equals</operation>
            <value>Terminate</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Ordertype__c</field>
            <operation>notEqual</operation>
            <value>PROVIDE</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Default Bundle Label Name for Change Order</fullName>
        <actions>
            <name>Default_Bundle_Label_Name_for_Change_Ord</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Bundle_Label_name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Subscription__c.csordtelcoa__Change_Type__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>	
    <rules>
        <fullName>Default Bundle Label Name for New Provide</fullName>
        <actions>
            <name>Default_Bundle_Label_Name_for_New_Provid</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Bundle_Label_name__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Subscription__c.csordtelcoa__Change_Type__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Full_Load__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Account Owner</fullName>
        <actions>
            <name>Notify_Account_Owner</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>email_sent_checkbox_to_true</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Notify Account Owner on Service Expiry.</description>
        <formula>AND((TaskCreated__c=true) &amp;&amp; (email_sent__c=false))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Notify Opp Owner when supplier price changes</fullName>
        <actions>
            <name>Email_sent_on_supplier_price_changes_in_off_net_components</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>workflow to be triggered on supplier price changes in off-net components.</description>
        <formula>AND( is_offnet__c  = &apos;Yes&apos;, ISCHANGED(MRC__c) || ISCHANGED(NRC__c) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>OffnetCountupdate</fullName>
        <actions>
            <name>OffnetYesSubscription</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.is_offnet__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Parent Bundle Flag</fullName>
        <actions>
            <name>Parent_Bundle_Flag</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>csordtelcoa__Replaced_Service__r.Parent_Bundle_Flag__c = TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Populate Resource Id</fullName>
        <actions>
            <name>Set_ResourceId_from_formula_field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Resource_Id__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Primary Service ID</fullName>
        <actions>
            <name>Parent_Inventory_Id</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Primary_Service</name>
            <type>FieldUpdate</type>
        </actions>
		<actions>
            <name>Service_Item_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>csordtelcoa__Replaced_Service__c  &lt;&gt; &apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RecordType Change</fullName>
        <actions>
            <name>RecordType_Field_Update_OffNet</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>csord__Service__c.is_offnet__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SNOW Time based Jeopardy 1new</fullName>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Start24h__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Update_24h_has_passed2</name>
                <type>FieldUpdate</type>
            </actions>
            <offsetFromField>csord__Service__c.Estimated_Start_Date__c</offsetFromField>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>SNOW Time based Jeopardy 2</fullName>
        <actions>
            <name>Clear_24H</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.X24_hours_has_passed_since_creation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Integration_Updates__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Site Code and Selling Entity Value</fullName>
        <actions>
            <name>Set_A_Side_Site_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Selling_Entity</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_Z_Side_Site_Code</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Order_Number__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SNOW Time based Jeopardy 3</fullName>
        <actions>
            <name>Clear_24HB</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Amber_Status2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.X24_hours_has_passed_since_creation__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Integration_Updates__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Service Name Update</fullName>
        <actions>
            <name>Update_Service_Names</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>Primary_Service_ID__c &lt;&gt; Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ServiceName</fullName>
        <actions>
            <name>Set_Order_Type_Terminate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Service_Name</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>csord__Order__c&lt;&gt;&apos;&apos;</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>SubscriptionName</fullName>
        <actions>
            <name>updatesubscriptionname</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( ISCHANGED(Name), csord__Service__c==&apos;&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TriggerIP006 Assignment</fullName>
        <actions>
            <name>Service_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR( Bundle_Flag__c = FALSE,AND(Bundle_Flag__c = TRUE, Parent_Bundle_Flag__c = TRUE )),RecordTypeId =$Label.LocalLoop, Inventory_Status__c = &apos;LIVE - AS DESIGNED&apos;, TEXT(AccountId__r.Customer_Type_New__c) &lt;&gt; &apos;ISO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>TriggerIP006 Assignment OffNet</fullName>
        <actions>
            <name>OffNet_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(Bundle_Flag__c = FALSE,AND(Bundle_Flag__c = TRUE, Parent_Bundle_Flag__c = TRUE)),RecordTypeId =$Label.OffNetRecordType, Inventory_Status__c = &apos;LIVE - AS DESIGNED&apos;, TEXT(AccountId__r.Customer_Type_New__c) &lt;&gt; &apos;ISO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	    <rules>
        <fullName>TriggerIP006 Assignment Master Service</fullName>
        <actions>
            <name>update_master_Service_IP006_rtype</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>AND(OR(Bundle_Flag__c = FALSE,AND(Bundle_Flag__c = TRUE, Parent_Bundle_Flag__c = TRUE)),RecordTypeId =$Label.Master_Service_IP006_Record_Type, Inventory_Status__c = &apos;LIVE - AS DESIGNED&apos;, TEXT(AccountId__r.Customer_Type_New__c) &lt;&gt; &apos;ISO&apos;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>

    <rules>
        <fullName>RootProduct%26Code</fullName>
        <actions>
            <name>UpdateRootProductCode</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateRootProductId</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>csord__Service__c.Order_Number__c</field>
            <operation>notEqual</operation>
            <value>null</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>UpdateU2CMasterCode</fullName>
        <actions>
            <name>updateU2CMasterCode</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>!Contains($Label.VPN_Services_ProductCode,Product_Code__c) &amp;&amp; !Contains($Label.Internet_Product_Codes,Product_Code__c) &amp;&amp; !Contains($Label.Colocation_ProductCode,Product_Code__c)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
	<rules>
        <fullName>GBandwidthPopulationWorkflow</fullName>
        <actions>
            <name>GBandwidthPopulationFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>G_Bandwidth_Gen_name__c = null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Instance ID for MACD</fullName>
        <actions>
            <name>Update_Inventory_Instance_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND(csordtelcoa__Replaced_Service__c &lt;&gt; &apos;&apos;,csordtelcoa__Replaced_Service__r.Path_Instance_ID__c &lt;&gt; Path_Instance_ID__c)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Update Inventory Status for MACD</fullName>
        <actions>
            <name>Update_Inventory_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>OR(Order_Type_Final__c = &apos;Renewal&apos;, Order_Type_Final__c = &apos;Terminate&apos;)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>	
    <rules>
        <fullName>Inventory Status update for SFDC generated resource</fullName>
        <actions>
            <name>Inventory_Status_field_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3)</booleanFilter>
        <criteriaItems>
            <field>csord__Service__c.Product_Id__c</field>
            <operation>notEqual</operation>
            <value>LLOOP</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Service_Type__c</field>
            <operation>equals</operation>
            <value>None</value>
        </criteriaItems>
        <criteriaItems>
            <field>csord__Service__c.Product_Code__c</field>
            <operation>equals</operation>
            <value>NID,EMC</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
