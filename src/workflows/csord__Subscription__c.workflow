<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Order_RT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Inflight_Subscription_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Order RT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_SUB_Service_Delivery_Australia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_Australia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A SUB Service Delivery Australia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_Sub_Service_Delivery_EMEA</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_EMEA</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A Sub Service Delivery EMEA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_Sub_Service_Delivery_North_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_North_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A Sub Service Delivery North Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_Sub_Service_Delivery_South_Asia</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_South_Asia</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A Sub Service Delivery South Asia</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>P2A_Sub_Service_Delivery_USA</fullName>
        <field>OwnerId</field>
        <lookupValue>Service_Delivery_OM_US</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>P2A Sub Service Delivery USA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RT_Inflight</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Change_Order_RT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>RT - Inflight</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SubscriptionName</fullName>
        <field>Name</field>
        <formula>Name + &apos; - &apos; +  csordtelcoa__Subscription_Number__c</formula>
        <name>SubscriptionName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
	<fieldUpdates>
        <fullName>Customertype_Subsciption</fullName>
        <description>Implemented the Sharing Rule for Subscriptions object</description>
        <field>Customer_Type__c</field>
        <formula>TEXT(csord__Account__r.Customer_Type_New__c)</formula>
        <name>Customertype_Subsciption</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>P2A Sub Service Delivery Australia</fullName>
        <actions>
            <name>P2A_SUB_Service_Delivery_Australia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>False</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c, &apos;Australia&apos;) &amp;&amp;  ISPICKVAL(csordtelcoa__Change_Type__c  , &apos;Terminate&apos;) &amp;&amp;  Signed_Termination_Form_Attached__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>P2A Sub Service Delivery EMEA</fullName>
        <actions>
            <name>P2A_Sub_Service_Delivery_EMEA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>False</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c, &apos;EMEA&apos;) &amp;&amp;  ISPICKVAL(csordtelcoa__Change_Type__c  , &apos;Terminate&apos;) &amp;&amp;  Signed_Termination_Form_Attached__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>P2A Sub Service Delivery North Asia</fullName>
        <actions>
            <name>P2A_Sub_Service_Delivery_North_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>False</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c, &apos;North Asia&apos;) &amp;&amp;  ISPICKVAL(csordtelcoa__Change_Type__c  , &apos;Terminate&apos;) &amp;&amp;  Signed_Termination_Form_Attached__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>P2A Sub Service Delivery South Asia</fullName>
        <actions>
            <name>P2A_Sub_Service_Delivery_South_Asia</name>
            <type>FieldUpdate</type>
        </actions>
        <active>False</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c, &apos;South Asia&apos;) &amp;&amp;  ISPICKVAL(csordtelcoa__Change_Type__c  , &apos;Terminate&apos;) &amp;&amp;  Signed_Termination_Form_Attached__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>P2A Sub Service Delivery USA</fullName>
        <actions>
            <name>P2A_Sub_Service_Delivery_USA</name>
            <type>FieldUpdate</type>
        </actions>
        <active>False</active>
        <formula>ISPICKVAL(csord__Account__r.Region__c, &apos;US&apos;) &amp;&amp;  ISPICKVAL(csordtelcoa__Change_Type__c  , &apos;Terminate&apos;) &amp;&amp;  Signed_Termination_Form_Attached__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RT - Change Order</fullName>
        <actions>
            <name>Change_Order_RT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Count_of_Service_not_provisioned__c =  Count_of_services__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>RT - Inflight</fullName>
        <actions>
            <name>RT_Inflight</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>Count_of_Service_not_provisioned__c &lt;&gt; Count_of_services__c</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SubscriptionName</fullName>
        <actions>
            <name>SubscriptionName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>csord__Order__c &lt;&gt; Null</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
	<rules>
        <fullName>Customertype_subsciption1</fullName>
        <actions>
            <name>Customertype_Subsciption</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Implemented for the Sharing Rule for Subscriptions</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
